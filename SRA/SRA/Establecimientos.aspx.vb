Imports System.Data.SqlClient


Namespace SRA


Partial Class Establecimientos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent() 

   End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Establecimientos
   Private mstrEstaRaza As String = SRA_Neg.Constantes.gTab_EstablecRazas
   Private mstrEstaCria As String = SRA_Neg.Constantes.gTab_EstablecCriador
   Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
   Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
   Private mstrClieDire As String = SRA_Neg.Constantes.gTab_ClientesDire
   Private mstrClieTele As String = SRA_Neg.Constantes.gTab_ClientesTele
   Private mstrClieMail As String = SRA_Neg.Constantes.gTab_ClientesMails
   Private mstrActiTele As String = SRA_Neg.Constantes.gTab_ActiTele
   Private mstrActiDire As String = SRA_Neg.Constantes.gTab_ActiDire
   Private mstrActiMail As String = SRA_Neg.Constantes.gTab_ActiMail
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Public mbooActi As Boolean
   Public mintActiDefa As Integer
   Public mstrEstaId As String

   Private Enum Columnas As Integer
      EstaEdit = 0
      EstaId = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Dim lstrFil As String
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then

            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

            Session(mSess(mstrTabla)) = Nothing

            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mMostrarPanel(False)

            mstrEstaId = Request.QueryString("id")
            If mstrEstaId <> "" Then
                mCargarDatos(mstrEstaId)
                mMostrarPanel(True)
                mSetearEditor(mstrTabla, False)
            End If

         Else
            If panDato.Visible Then
               mdsDatos = Session(mSess(mstrTabla))
               Dim x As String = Session.SessionID
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()

      clsWeb.gCargarRefeCmb(mstrConn, "tele_tipos", cmbTeleTipo, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDirePais, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDirePcia, "S", "0" + cmbDirePais.Valor.ToString)
      clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
      SRA_Neg.Utiles.gSetearRaza(cmbRaza)
      clsWeb.gCargarRefeCmb(mstrConn, "tatuajes", cmbTatu, "S")
      clsWeb.gCargarCombo(mstrConn, "criadores_cliente_cargar " & usrClie.Valor, cmbCria, "id", "descrip", "S", True)
      If Not Request.Form("cmbCria") Is Nothing Then
         cmbCria.Valor = Request.Form("cmbCria")
      End If

      clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipoFil, "T")

      'inicializar valores
      If mValorParametro(Request.QueryString("DocuTipo")) <> "" Then
         cmbDocuTipoFil.Valor = mValorParametro(Request.QueryString("DocuTipo"))
      End If

   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaTele.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaDire.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaMail.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaRaza.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mSetearMaxLength()

      Dim lstrClieLong As Object
      Dim lstrClieTeleLong As Object
      Dim lstrClieDireLong As Object
      Dim lstrClieMailLong As Object
      Dim lstrEstaCriaLong As Object
      Dim lintCol As Integer

      lstrEstaCriaLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrEstaCria)
      txtCriaObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrEstaCriaLong, "escr_obse")
      txtRespa.MaxLength = clsSQLServer.gObtenerLongitud(lstrEstaCriaLong, "escr_respa")

      lstrClieLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieLong, "clie_obse")

      lstrClieTeleLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClieTele)
      txtTeleNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_tele")
      txtTeleArea.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_area_code")
      txtTeleRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_refe")

      lstrClieDireLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClieDire)
      txtDire.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_dire")
      txtDireCP.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_cpos")
      txtDireRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_refe")

      lstrClieMailLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClieMail)
      txtMail.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieMailLong, "macl_mail")
      txtMailRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieMailLong, "macl_refe")

   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintActiDefa = clsSQLServer.gConsultarValor("exec usuarios_actividades_default " & Session("sUserId").ToString, mstrConn, 0)

      usrClieFil.Tabla = mstrClientes
      usrClieFil.AutoPostback = False
      usrClieFil.FilClaveUnica = True
      usrClieFil.ColClaveUnica = True
      usrClieFil.Ancho = 790
      usrClieFil.Alto = 510
      usrClieFil.ColDocuNume = True
      usrClieFil.ColCUIT = True
      usrClieFil.FilCUIT = True
      usrClieFil.FilDocuNume = True
      usrClieFil.FilTarjNume = True

      usrClie.Tabla = mstrClientes
      usrClie.AutoPostback = False
      usrClie.FilClaveUnica = True
      usrClie.ColClaveUnica = True
      usrClie.Ancho = 790
      usrClie.Alto = 510
      usrClie.ColDocuNume = True
      usrClie.ColCUIT = True
      usrClie.FilCUIT = True
      usrClie.FilDocuNume = True
      usrClie.FilTarjNume = True

      usrCriaFil.Tabla = mstrCriadores
      usrCriaFil.Criador = True
      usrCriaFil.AutoPostback = False
      usrCriaFil.FilClaveUnica = True
      usrCriaFil.ColClaveUnica = False
      usrCriaFil.Ancho = 790
      usrCriaFil.Alto = 510
      usrCriaFil.ColDocuNume = False
      usrCriaFil.ColCriaNume = True
      usrCriaFil.ColCUIT = True
      usrCriaFil.FilCUIT = True
      usrCriaFil.FilDocuNume = True
      usrCriaFil.FilAgru = False
      usrCriaFil.FilTarjNume = False

   End Sub

   Private Function mValorParametro(ByVal pstrPara As String) As String
      Dim lstrPara As String
      If pstrPara Is Nothing Then
         lstrPara = ""
      Else
         If pstrPara Is System.DBNull.Value Then
            lstrPara = ""
         Else
            lstrPara = pstrPara
         End If
      End If
      Return (lstrPara)
   End Function

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mValidarConsulta()

      Try

         If (cmbDocuTipoFil.Valor = 0 And txtDocuNumeFil.Text <> "") _
         Or (cmbDocuTipoFil.Valor <> 0 And txtDocuNumeFil.Text = "") Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el tipo y n�mero de documento.")
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As String

         lstrCmd = ObtenerSql(Session("sUserId").ToString(), IIf(chkBusc.Checked, 1, 0), usrClieFil.Valor, _
                              IIf(chkBaja.Checked, 1, 0), txtDescFil.Text, txtApelFil.Text, txtNombFil.Text, _
                              usrCriaFil.Valor.ToString, txtCuitFil.TextoPlano.ToString, cmbDocuTipoFil.SelectedValue, _
                              txtDocuNumeFil.Text, txtCunicaFil.Text)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

         grdDato.Visible = True

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Public Shared Function ObtenerSql(ByVal pstrUserId As String, ByVal pintBusc As Integer, ByVal pstrClieId As String, _
                                     ByVal pintBaja As Integer, ByVal pstrDesc As String, ByVal pstrApel As String, _
                                     ByVal pstrNomb As String, ByVal pstrCriaId As String, ByVal pstrCuitFil As String, _
                                     ByVal pstrDocuTipo As String, ByVal pstrDocuNume As String, ByVal pstrCunicaFil As String) As String
      Dim lstrCmd As New StringBuilder

      'esta funci�n se llama tambi�n desde clsXMLHTTP
      lstrCmd.Append("exec establecimientos_busq")
      lstrCmd.Append(" @audi_user=" + clsSQLServer.gFormatArg(pstrUserId, SqlDbType.Int))
      lstrCmd.Append(" , @buscar_en=" + pintBusc.ToString)
      lstrCmd.Append(" , @clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
      lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)

      lstrCmd.Append(" , @esbl_desc=" + clsSQLServer.gFormatArg(pstrDesc, SqlDbType.VarChar))
      lstrCmd.Append(" , @clie_apel=" + clsSQLServer.gFormatArg(pstrApel, SqlDbType.VarChar))
      lstrCmd.Append(" , @clie_nomb=" + clsSQLServer.gFormatArg(pstrNomb, SqlDbType.VarChar))

      lstrCmd.Append(" , @cria_id=" + clsSQLServer.gFormatArg(pstrCriaId, SqlDbType.Int))
      lstrCmd.Append(" , @clie_cuit=" + clsSQLServer.gFormatArg(pstrCuitFil, SqlDbType.Int))
      lstrCmd.Append(" , @clie_docu_tipo=" + clsSQLServer.gFormatArg(pstrDocuTipo, SqlDbType.Int))
      lstrCmd.Append(" , @clie_docu_nume=" + clsSQLServer.gFormatArg(pstrDocuNume, SqlDbType.Int))
      lstrCmd.Append(" , @clie_cunica=" + clsSQLServer.gFormatArg(pstrCunicaFil, SqlDbType.VarChar))

      Return (lstrCmd.ToString)
   End Function
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrClieTele
            btnBajaTele.Enabled = Not (pbooAlta)
            btnModiTele.Enabled = Not (pbooAlta)
            btnAltaTele.Enabled = pbooAlta
         Case mstrClieDire
            btnBajaDire.Enabled = Not (pbooAlta)
            btnModiDire.Enabled = Not (pbooAlta)
            btnAltaDire.Enabled = pbooAlta
         Case mstrClieMail
            btnBajaMail.Enabled = Not (pbooAlta)
            btnModiMail.Enabled = Not (pbooAlta)
            btnAltaMail.Enabled = pbooAlta
         Case mstrEstaRaza
            btnBajaRaza.Enabled = True 'Not (pbooAlta)
            btnModiRaza.Enabled = True 'Not (pbooAlta)
            btnAltaRaza.Enabled = True 'pbooAlta
         Case mstrEstaCria
            btnBajaCria.Enabled = Not (pbooAlta)
            btnModiCria.Enabled = Not (pbooAlta)
            btnAltaCria.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mCargarDatos(E.Item.Cells(Columnas.EstaId).Text)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(pstrId)

      mCrearDataSet(hdnId.Text)

      grdTele.CurrentPageIndex = 0
      grdDire.CurrentPageIndex = 0
      grdMail.CurrentPageIndex = 0
      grdRaza.CurrentPageIndex = 0

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            hdnId.Text = .Item("esbl_id")
                    txtObse.Valor = IIf(.Item("esbl_obse").ToString.Length > 0, .Item("esbl_obse"), String.Empty)
                    txtDesc.Valor = .Item("esbl_desc")
            usrClie.Valor = .Item("esbl_clie_id")
            clsWeb.gCargarCombo(mstrConn, "criadores_cliente_cargar " & usrClie.Valor, cmbCria, "id", "descrip", "S", True)
            If Not .IsNull("esbl_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("esbl_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If

            lblTitu.Text = "Datos del Establecimiento: " & .Item("esbl_desc")
            lblTitu.Visible = False
         End With

         mSetearEditor("", False)
         mSetearEditor(mstrClieTele, True)
         mSetearEditor(mstrClieDire, True)
         mSetearEditor(mstrClieMail, True)
         mSetearEditor(mstrEstaRaza, True)
         mSetearEditor(mstrEstaCria, True)
         mMostrarPanel(True)
         mShowTabs(1)

      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiarFiltros()

      hdnValorId.Text = "-1"  'para que ignore el id que vino del control
      txtDescFil.Text = ""
      txtApelFil.Text = ""
      txtNombFil.Text = ""
      txtCuitFil.Text = ""
      cmbDocuTipoFil.Limpiar()
      txtDocuNumeFil.Text = ""
      txtCunicaFil.Text = ""
      chkBusc.Checked = False
      chkBaja.Checked = False
      usrClieFil.Limpiar()
      usrCriaFil.Limpiar()
      grdDato.Visible = False

   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      lblBaja.Text = ""
      txtObse.Text = ""
      txtDesc.Text = ""
      hdnLocaPop.Text = ""
      usrClie.Limpiar()
      cmbCria.Items.Clear()

      mLimpiarTelefono()
      mLimpiarDireccion()
      mLimpiarMail()
      mLimpiarCriadores()

      mCrearDataSet("")

      lblTitu.Text = ""

      grdTele.CurrentPageIndex = 0
      grdDire.CurrentPageIndex = 0
      grdMail.CurrentPageIndex = 0
      grdRaza.CurrentPageIndex = 0

      mSetearEditor("", True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Visible = (Not panDato.Visible And Not mbooActi)

      lnkCabecera.Font.Bold = True
      lnkTele.Font.Bold = False

      panDato.Visible = pbooVisi
      panFiltros.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      panCabecera.Visible = True
      panTele.Visible = False
      panDire.Visible = False
      panMail.Visible = False
      panRaza.Visible = False

      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True
      lnkTele.Font.Bold = False
      lnkDire.Font.Bold = False
      lnkMail.Font.Bold = False
      lnkRaza.Font.Bold = False

   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      Dim lstrTitu As String
      If lblTitu.Text <> "" Then
         lstrTitu = lblTitu.Text.Substring(lblTitu.Text.IndexOf(":") + 2)
      End If

      panDato.Visible = True
      panBotones.Visible = True
      btnAgre.Visible = False
      panTele.Visible = False
      panDire.Visible = False
      panMail.Visible = False
      panRaza.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkTele.Font.Bold = False
      lnkDire.Font.Bold = False
      lnkMail.Font.Bold = False
      lnkRaza.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Visible = False
         Case 2
            panTele.Visible = True
            lnkTele.Font.Bold = True
            lblTitu.Text = "Tel�fonos del Establecimiento: " & lstrTitu
            lblTitu.Visible = True
            grdTele.Visible = True
         Case 3
            panDire.Visible = True
            lnkDire.Font.Bold = True
            lblTitu.Text = "Direcciones del Establecimiento: " & lstrTitu
            lblTitu.Visible = True
            grdDire.Visible = True
         Case 4
            panMail.Visible = True
            lnkMail.Font.Bold = True
            lblTitu.Text = "Mails del Establecimiento: " & lstrTitu
            lblTitu.Visible = True
            grdMail.Visible = True
         Case 5
            panRaza.Visible = True
            lnkRaza.Font.Bold = True
            lblTitu.Text = "Razas del Establecimiento: " & lstrTitu
            lblTitu.Visible = True
            grdRaza.Visible = True
      End Select

   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjEstabl As New SRA_Neg.Establecimientos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrClieTele, mstrClieDire, mstrClieMail, mstrEstaRaza, mstrEstaCria, mdsDatos.Copy, Server)

         lobjEstabl.Alta()

         mConsultar()

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjEstabl As New SRA_Neg.Establecimientos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrClieTele, mstrClieDire, mstrClieMail, mstrEstaRaza, mstrEstaCria, mdsDatos.Copy, Server)

         lobjEstabl.Modi()

         mConsultar()

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjEstabl As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjEstabl.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If usrClie.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el cliente.")
      End If

      Dim cantVal As Integer = 0 'cantidad default

      For Each oDataItem As DataGridItem In grdDire.Items
         If oDataItem.Cells(7).Text = "X" And oDataItem.Cells(8).Text = "Activo" Then
            cantVal += 1
         End If
      Next

      If cantVal = 0 Then
         Throw New AccesoBD.clsErrNeg("No ha ingresado ninguna direcci�n activa.")
      End If
      If cantVal > 1 Then
         Throw New AccesoBD.clsErrNeg("Debe haber una �nica direcci�n Default y Activa.")
      End If
   End Sub
   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer

      mValidarDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("esbl_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("esbl_desc") = txtDesc.Valor
         .Item("esbl_obse") = txtObse.Valor
         .Item("esbl_clie_id") = usrClie.Valor
      End With

      Return mdsDatos
   End Function
   Public Sub mCrearDataSet(ByVal pstrId As String)

      Dim lstrOpci As String

      lstrOpci = "@acti=3"
      lstrOpci = lstrOpci & ",@audi_user=" & Session("sUserId").ToString
      mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, lstrOpci)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrClieTele
      mdsDatos.Tables(2).TableName = mstrClieDire
      mdsDatos.Tables(3).TableName = mstrClieMail
      mdsDatos.Tables(4).TableName = mstrEstaRaza
      mdsDatos.Tables(5).TableName = mstrEstaCria

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
      grdTele.DataBind()

      grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
      grdDire.DataBind()

      grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
      grdMail.DataBind()

      grdRaza.DataSource = mdsDatos.Tables(mstrEstaRaza)
      grdRaza.DataBind()

      grdCria.DataSource = mdsDatos.Tables(mstrEstaCria)
      grdCria.DataBind()

      Session(mSess(mstrTabla)) = mdsDatos

   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub lnkRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRaza.Click
      mShowTabs(5)
   End Sub
   Private Sub lnkTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTele.Click
      mShowTabs(2)
   End Sub
   Private Sub lnkDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDire.Click
      mShowTabs(3)
   End Sub
   Private Sub lnkMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkMail.Click
      mShowTabs(4)
   End Sub
   Private Sub btnLimpDire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDire.Click
      mLimpiarDireccion()
   End Sub
   Private Sub btnLimpMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpMail.Click
      mLimpiarMail()
   End Sub
   Private Sub btnLimpTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpTele.Click
      mLimpiarTelefono()
   End Sub
   Private Sub btnBajaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaTele.Click
      Try
         With mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & hdnTeleId.Text)(0)
            If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("tecl_gene")) Then
               Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
            End If
            .Delete()
         End With
         grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
         grdTele.DataBind()
         mLimpiarTelefono()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnBajaRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaRaza.Click
      Try
         With mdsDatos.Tables(mstrEstaRaza).Select("esra_id=" & hdnRazaId.Text)(0)
            .Item("esra_baja_fecha") = System.DateTime.Now.ToString
            .Item("_estado") = "Inactivo"
         End With
         grdRaza.DataSource = mdsDatos.Tables(mstrEstaRaza)
         grdRaza.DataBind()
         mLimpiarRaza()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnBajaDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDire.Click
      Try
         With mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & hdnDireId.Text)(0)
            If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("dicl_gene")) Then
               Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
            End If
            .Item("dicl_baja_fecha") = System.DateTime.Now.ToString
            .Item("_estado") = "Inactivo"
         End With
         grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
         grdDire.DataBind()
         mLimpiarDireccion()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnBajaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaMail.Click
      Try
         With mdsDatos.Tables(mstrClieMail).Select("macl_id=" & hdnMailId.Text)(0)
            If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("macl_gene")) Then
               Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
            End If
            .Delete()
         End With
         grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
         grdMail.DataBind()
         mLimpiarMail()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnAltaCria_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaCria.Click
      mActualizarCriadores(True)
   End Sub
   Private Sub btnBajaCria_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaCria.Click
      Try
         With mdsDatos.Tables(mstrEstaCria).Select("escr_id=" & hdnCriaId.Text)(0)
            .Item("escr_baja_fecha") = System.DateTime.Now.ToString
            .Item("_estado") = "Inactivo"
         End With
         grdCria.DataSource = mdsDatos.Tables(mstrEstaCria)
         grdCria.DataBind()
         mLimpiarCriadores()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnModiCria_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCria.Click
      mActualizarCriadores(False)
   End Sub
   Private Sub btnLimpCria_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpCria.Click
      mLimpiarCriadores()
   End Sub
   Private Sub btnAltaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaTele.Click
      mActualizarTelefono(True)
   End Sub
   Private Sub btnAltaRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaRaza.Click
      mActualizarRaza(True)
   End Sub
   Private Sub btnAltaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaMail.Click
      mActualizarMail(True)
   End Sub
   Private Sub btnAltaDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDire.Click
      mActualizarDireccion(True)
   End Sub
   Private Sub btnModiTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiTele.Click
      mActualizarTelefono(False)
   End Sub
   Private Sub btnModiDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDire.Click
      mActualizarDireccion(False)
   End Sub
   Private Sub btnModiMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiMail.Click
      mActualizarMail(False)
   End Sub
   Private Sub btnModiRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiRaza.Click
      mActualizarRaza(False)
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mValidarConsulta()
      mConsultar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub hdnLocaPop_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdnLocaPop.TextChanged
      Try
         If (hdnLocaPop.Text <> "") Then
            cmbDireLoca.Valor = hdnLocaPop.Text
            txtDireCP.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "localidades", cmbDireLoca.Valor.ToString, "loca_cpos").ToString
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub
   Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
      Try
         If (hdnDatosPop.Text <> "") Then

            mstrCmd = "exec dbo.dire_clientes_consul "
            mstrCmd += hdnDatosPop.Text


            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            If ds.Tables(0).Rows.Count > 0 Then
               With ds.Tables(0).Rows(0)
                  txtDire.Valor = .Item("dicl_dire")
                  txtDireCP.Valor = .Item("dicl_cpos")
                  txtDireRefe.Valor = .Item("dicl_refe")
                  cmbDirePais.Valor = .Item("_dicl_pais_id")
                  clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
                  cmbDirePcia.Valor = .Item("_dicl_pcia_id")
                  clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbDirePcia.Valor, cmbDireLoca, "id", "descrip", "S", True)
                  clsWeb.gCargarCombo(mstrConn, "localidades_aux_cargar " & cmbDirePcia.Valor, cmbLocaAux, "id", "descrip", "S", True)
                  cmbDireLoca.Valor = .Item("dicl_loca_id")
               End With
            End If
            hdnDatosPop.Text = ""

         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub
#End Region

#Region "Detalle"
   Public Sub mEditarDatosTele(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrTele As DataRow
         Dim lstrActiProp As String
         'Dim lstrActiNoProp As String
         Dim lbooGene As Boolean

         hdnTeleId.Text = E.Item.Cells(1).Text
         ldrTele = mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & hdnTeleId.Text)(0)

         With ldrTele
            txtTeleArea.Valor = .Item("tecl_area_code")
            txtTeleNume.Valor = .Item("tecl_tele")
            txtTeleRefe.Valor = .Item("tecl_refe")
            cmbTeleTipo.Valor = .Item("tecl_teti_id")
            lstrActiProp = .Item("acti_prop")
            'lstrActiNoProp = .Item("acti_noprop")
            lbooGene = .Item("tecl_gene")
         End With

         If mbooActi Then
            mHabilitarTele(mintActiDefa.ToString = lstrActiProp And Not lbooGene)
         End If

         grdActiTele.Visible = True

         mSetearEditor(mstrClieTele, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosDire(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrDire As DataRow
         Dim lstrActiProp As String
         'Dim lstrActiNoProp As String
         Dim lbooGene As Boolean

         hdnDireId.Text = E.Item.Cells(1).Text
         ldrDire = mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & hdnDireId.Text)(0)

         With ldrDire
                    cmbDirePais.Valor = IIf(.Item("_dicl_pais_id") Is DBNull.Value, "", .Item("_dicl_pais_id"))
                    txtDire.Valor = IIf(.Item("dicl_dire") Is DBNull.Value, "", .Item("dicl_dire"))
                    txtDireCP.Valor = IIf(.Item("dicl_cpos") Is DBNull.Value, "", .Item("dicl_cpos"))
                    txtDireCP.ValidarPaisDefault = IIf(cmbDirePais.Valor = Session("sPaisDefaId"), "S", "N")
                    txtDireRefe.Valor = IIf(.Item("dicl_refe") Is DBNull.Value, "", .Item("dicl_refe"))
                    clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
                    cmbDirePcia.Valor = IIf(.Item("_dicl_pcia_id") Is DBNull.Value, "", .Item("_dicl_pcia_id"))
                    clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbDirePcia.Valor, cmbDireLoca, "id", "descrip", "S", True)
            clsWeb.gCargarCombo(mstrConn, "localidades_aux_cargar " & cmbDirePcia.Valor, cmbLocaAux, "id", "descrip", "S", True)
                    cmbDireLoca.Valor = IIf(.Item("dicl_loca_id") Is DBNull.Value, "", .Item("dicl_loca_id"))
                    chkDireDefa.Checked = IIf(.Item("dicl_defa") Is DBNull.Value, "", .Item("dicl_defa"))
                    lstrActiProp = .Item("acti_prop").ToString
            lbooGene = .Item("dicl_gene")
         End With

         If mbooActi Then
            mHabilitarDire(mintActiDefa.ToString = lstrActiProp And Not lbooGene)
         End If

         grdActiDire.Visible = True

         mSetearEditor(mstrClieDire, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosCriador(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrCria As DataRow

         hdnCriaId.Text = E.Item.Cells(1).Text
         ldrCria = mdsDatos.Tables(mstrEstaCria).Select("escr_id=" & hdnCriaId.Text)(0)

         With ldrCria
            cmbCria.Valor = .Item("escr_cria_id")
            cmbTatu.Valor = .Item("escr_corr_tatu")
                    txtRespa.Valor = IIf(.Item("escr_respa").ToString.Length = 0, String.Empty, .Item("escr_respa"))
            If Not .IsNull("escr_dupli_cert") Then
               chkDuplCert.Checked = clsWeb.gFormatCheck(.Item("escr_dupli_cert"), "True")
            Else
               chkDuplCert.Checked = False
            End If
                    txtCriaObse.Valor = IIf(.Item("escr_obse").ToString.Length = 0, String.Empty, .Item("escr_obse"))
         End With

         mSetearEditor(mstrEstaCria, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosRaza(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrRaza As DataRow

         hdnRazaId.Text = E.Item.Cells(1).Text
         ldrRaza = mdsDatos.Tables(mstrEstaRaza).Select("esra_id=" & hdnRazaId.Text)(0)

         With ldrRaza
            cmbRaza.Valor = .Item("esra_raza_id")
         End With

         mSetearEditor(mstrEstaRaza, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatosMail(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrMail As DataRow
         Dim lstrActiProp As String
         'Dim lstrActiNoProp As String
         Dim lbooGene As Boolean

         hdnMailId.Text = E.Item.Cells(1).Text
         ldrMail = mdsDatos.Tables(mstrClieMail).Select("macl_id=" & hdnMailId.Text)(0)

         With ldrMail
            txtMail.Valor = .Item("macl_mail")
            txtMailRefe.Valor = .Item("macl_refe")
            lstrActiProp = .Item("acti_prop")
            'lstrActiNoProp = .Item("acti_noprop")
            lbooGene = .Item("macl_gene")
         End With

         If mbooActi Then
            mHabilitarMail(mintActiDefa.ToString = lstrActiProp And Not lbooGene)
         End If

         grdActiMail.Visible = True

         mSetearEditor(mstrClieMail, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdTele_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdTele.EditItemIndex = -1
         If (grdTele.CurrentPageIndex < 0 Or grdTele.CurrentPageIndex >= grdTele.PageCount) Then
            grdTele.CurrentPageIndex = 0
         Else
            grdTele.CurrentPageIndex = E.NewPageIndex
         End If
         grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
         grdTele.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdCria_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCria.EditItemIndex = -1
         If (grdCria.CurrentPageIndex < 0 Or grdCria.CurrentPageIndex >= grdCria.PageCount) Then
            grdCria.CurrentPageIndex = 0
         Else
            grdCria.CurrentPageIndex = E.NewPageIndex
         End If
         grdCria.DataSource = mdsDatos.Tables(mstrEstaCria)
         grdCria.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdDire_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDire.EditItemIndex = -1
         If (grdDire.CurrentPageIndex < 0 Or grdDire.CurrentPageIndex >= grdDire.PageCount) Then
            grdDire.CurrentPageIndex = 0
         Else
            grdDire.CurrentPageIndex = E.NewPageIndex
         End If
         grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
         grdDire.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdRaza_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdRaza.EditItemIndex = -1
         If (grdRaza.CurrentPageIndex < 0 Or grdRaza.CurrentPageIndex >= grdRaza.PageCount) Then
            grdRaza.CurrentPageIndex = 0
         Else
            grdRaza.CurrentPageIndex = E.NewPageIndex
         End If
         grdRaza.DataSource = mdsDatos.Tables(mstrEstaRaza)
         grdRaza.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdMail_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdMail.EditItemIndex = -1
         If (grdMail.CurrentPageIndex < 0 Or grdMail.CurrentPageIndex >= grdMail.PageCount) Then
            grdMail.CurrentPageIndex = 0
         Else
            grdMail.CurrentPageIndex = E.NewPageIndex
         End If
         grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
         grdMail.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarTelefono(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosTele(pbooAlta)

         mLimpiarTelefono()
         grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
         grdTele.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarMail(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosMail(pbooAlta)

         mLimpiarMail()
         grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
         grdMail.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarCriadores(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosCriadores(pbooAlta)

         mLimpiarCriadores()
         grdCria.DataSource = mdsDatos.Tables(mstrEstaCria)
         grdCria.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarRaza(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosRaza(pbooAlta)

         mLimpiarRaza()
         grdRaza.DataSource = mdsDatos.Tables(mstrEstaRaza)
         grdRaza.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarDireccion(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosDire(pbooAlta)

         mLimpiarDireccion()
         grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
         grdDire.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosTele(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow
      Dim lstrCheck As System.Web.UI.WebControls.CheckBox
      Dim lstrProp As NixorControls.ComboBox
      Dim lstrActiProp As String
      Dim lstrActiNoProp As String
      Dim lstrActiPropDesc As String

      If txtTeleArea.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el c�digo de area.")
      End If

      If txtTeleNume.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el n�mero de tel�fono.")
      End If

      If cmbTeleTipo.SelectedValue = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el tipo de tel�fono.")
      End If

      If hdnTeleId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrClieTele).NewRow
         ldrDatos.Item("tecl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieTele), "tecl_id")
         If mbooActi Then
            ldrDatos.Item("acti_prop") = mintActiDefa
         End If
      Else
         ldrDatos = mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & hdnTeleId.Text)(0)
      End If

      With ldrDatos
         .Item("tecl_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
         .Item("tecl_tele") = txtTeleNume.Valor
         .Item("tecl_area_code") = txtTeleArea.Valor
         .Item("_tele_desc") = txtTeleArea.Valor & " " & txtTeleNume.Valor
         .Item("tecl_refe") = txtTeleRefe.Valor
         .Item("tecl_teti_id") = cmbTeleTipo.Valor
         .Item("tecl_gene") = 0
         .Item("acti_prop") = "3"
      End With

      If hdnTeleId.Text = "" Then
         mdsDatos.Tables(mstrClieTele).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosMail(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow
      Dim lstrCheck As System.Web.UI.WebControls.CheckBox
      Dim lstrProp As NixorControls.ComboBox
      Dim lstrActiProp As String
      Dim lstrActiNoProp As String
      Dim lstrActiPropDesc As String

      If txtMail.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la direcci�n de mail.")
      End If

      If hdnMailId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrClieMail).NewRow
         ldrDatos.Item("macl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieMail), "macl_id")
         If mbooActi Then
            ldrDatos.Item("acti_prop") = mintActiDefa
         End If
      Else
         ldrDatos = mdsDatos.Tables(mstrClieMail).Select("macl_id=" & hdnMailId.Text)(0)
      End If

      With ldrDatos
         .Item("macl_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
         .Item("macl_mail") = txtMail.Valor
         .Item("macl_refe") = txtMailRefe.Valor
         .Item("macl_gene") = 0
         .Item("acti_prop") = "3"
      End With

      If hdnMailId.Text = "" Then
         mdsDatos.Tables(mstrClieMail).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosDire(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow
      Dim lstrCheck As System.Web.UI.WebControls.CheckBox
      Dim lstrProp As NixorControls.ComboBox
      Dim lstrActiProp As String
      Dim lstrActiNoProp As String
      Dim lstrActiPropDesc As String
      Dim vbooDefault As Boolean

      If txtDire.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar una direcci�n.")
      End If

      If txtDireCP.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el c�digo postal.")
      End If

            If cmbDireLoca.Text.Trim().Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la localidad.")
            End If

            If chkDireDefa.Checked Then
         For Each dr As DataRow In mdsDatos.Tables(mstrClieDire).Select("dicl_defa=1")
            With dr
               .Item("dicl_defa") = False
               .Item("_defa") = ""
            End With
         Next
      End If

      vbooDefault = (mdsDatos.Tables(mstrClieDire).Select("dicl_defa=1").GetLength(0) = 0)


      If hdnDireId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrClieDire).NewRow
         ldrDatos.Item("dicl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieDire), "dicl_id")
         If mbooActi Then
            ldrDatos.Item("acti_prop") = mintActiDefa
         End If
      Else
         ldrDatos = mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & hdnDireId.Text)(0)
      End If

      With ldrDatos
         .Item("dicl_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
         .Item("dicl_dire") = txtDire.Valor
         .Item("dicl_refe") = txtDireRefe.Valor
         .Item("dicl_cpos") = txtDireCP.Valor
         .Item("_dire_desc") = txtDire.Valor
                .Item("dicl_loca_id") = IIf(cmbDireLoca.Valor.Trim.Length = 0, "", cmbDireLoca.Valor)
                .Item("_dicl_pcia_id") = IIf(cmbDirePcia.Valor.Trim.Length = 0, "", cmbDirePcia.Valor)
                .Item("_dicl_pais_id") = IIf(cmbDirePais.Valor.Trim.Length = 0, "", cmbDirePais.Valor)
                .Item("dicl_defa") = vbooDefault
         .Item("_defa") = IIf(.Item("dicl_defa"), "X", "")
         .Item("dicl_gene") = 0
         .Item("acti_prop") = "3"
      End With

      If hdnDireId.Text = "" Then
         mdsDatos.Tables(mstrClieDire).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosRaza(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow

      If cmbRaza.SelectedValue = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la raza.")
      End If

      If (mdsDatos.Tables(mstrEstaRaza).Select("esra_raza_id=" & cmbRaza.Valor)).GetUpperBound(0) <> -1 Then
         Throw New AccesoBD.clsErrNeg("La raza indicada ya se encuentra asociada al establecimiento.")
      End If

      If hdnRazaId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrEstaRaza).NewRow
         ldrDatos.Item("esra_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrEstaRaza), "esra_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrEstaRaza).Select("esra_id=" & hdnRazaId.Text)(0)
      End If

      With ldrDatos
         .Item("esra_raza_id") = cmbRaza.Valor
         .Item("_raza_desc") = cmbRaza.SelectedItem.Text
         .Item("esra_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
      End With

      If hdnRazaId.Text = "" Then
         mdsDatos.Tables(mstrEstaRaza).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosCriadores(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow

            'If cmbCria.Valor Is DBNull.Value Then
            If (cmbCria.Text.Trim().Length) = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el criador.")
            End If

            If pbooAlta Then
                If (mdsDatos.Tables(mstrEstaCria).Select("escr_cria_id=" & cmbCria.Valor)).GetUpperBound(0) <> -1 Then
                    Throw New AccesoBD.clsErrNeg("El criador indicado ya se encuentra asociado al establecimiento.")
                End If
            End If

            If hdnCriaId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrEstaCria).NewRow
         ldrDatos.Item("escr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrEstaCria), "escr_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrEstaCria).Select("escr_id=" & hdnCriaId.Text)(0)
      End If

      With ldrDatos
         .Item("escr_cria_id") = cmbCria.Valor
         .Item("_criador") = cmbCria.SelectedItem.Text
         .Item("escr_corr_tatu") = cmbTatu.Valor
         .Item("escr_respa") = txtRespa.Valor
         .Item("escr_dupli_cert") = IIf(chkDuplCert.Checked, 1, 0)
         .Item("escr_obse") = txtCriaObse.Valor
         .Item("_estado") = "Activo"
      End With

      If hdnCriaId.Text = "" Then
         mdsDatos.Tables(mstrEstaCria).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mLimpiarTelefono()
      hdnTeleId.Text = ""
      txtTeleNume.Text = ""
      txtTeleArea.Text = ""
      txtTeleRefe.Text = ""
      grdActiTele.Visible = True
      cmbTeleTipo.Limpiar()
      mSetearEditor(mstrClieTele, True)
      mHabilitarTele(True)
   End Sub

   Private Sub mHabilitarTele(ByVal pbooHabi As Boolean)
      txtTeleNume.Enabled = pbooHabi
      txtTeleArea.Enabled = pbooHabi
      txtTeleRefe.Enabled = pbooHabi
      cmbTeleTipo.Enabled = pbooHabi
   End Sub

   Private Sub mHabilitarDire(ByVal pbooHabi As Boolean)
      txtDire.Enabled = pbooHabi
      txtDireCP.Enabled = pbooHabi
      txtDireRefe.Enabled = pbooHabi
      cmbDirePais.Enabled = pbooHabi
      cmbDirePcia.Enabled = pbooHabi
      cmbDireLoca.Enabled = pbooHabi
      btnLoca.Disabled = Not pbooHabi
      chkDireDefa.Enabled = pbooHabi
   End Sub

        Private Sub mLimpiarDireccion()
            hdnDireId.Text = ""
            txtDire.Text = ""
            txtDireCP.Text = ""
            txtDireRefe.Text = ""
            chkDireDefa.Checked = False
            grdActiDire.Visible = True
            cmbDirePais.Limpiar()

            'clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDirePcia, "S", "0" + cmbDirePais.Valor.ToString)
            'clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbDireLoca, "S", "0" + cmbDirePcia.Valor.ToString)
            'clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbLocaAux, "S", "0" + cmbDirePcia.Valor.ToString)

            cmbDirePcia.Valor = String.Empty
            clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDirePcia, "S", "0" + cmbDirePcia.Valor.ToString)

            cmbDireLoca.Valor = String.Empty
            clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbDireLoca, "S", "0" + cmbDireLoca.Valor.ToString)
            cmbLocaAux.Valor = String.Empty
            clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbLocaAux, "S", "0" + cmbLocaAux.Valor.ToString)

            mSetearEditor(mstrClieDire, True)
            mHabilitarDire(True)
        End Sub

   Private Sub mLimpiarCriadores()
      hdnCriaId.Text = ""
      cmbCria.Limpiar()
      cmbTatu.Limpiar()
      txtRespa.Text = ""
      txtCriaObse.Text = ""
      chkDuplCert.Checked = False
      mSetearEditor(mstrEstaCria, True)
   End Sub

   Private Sub mLimpiarRaza()
      hdnRazaId.Text = ""
      cmbRaza.Limpiar()
      mSetearEditor(mstrEstaRaza, True)
   End Sub

   Private Sub mLimpiarMail()
      hdnMailId.Text = ""
      txtMail.Text = ""
      txtMailRefe.Text = ""
      grdActiMail.Visible = True
      mSetearEditor(mstrClieMail, True)
      mHabilitarMail(True)
   End Sub

   Private Sub mHabilitarMail(ByVal pbooHabi As Boolean)
      txtMail.Enabled = pbooHabi
      txtMailRefe.Enabled = pbooHabi
   End Sub

#End Region

   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function

   Protected Overrides Sub Finalize()
      MyBase.Finalize()
   End Sub

End Class
End Namespace
