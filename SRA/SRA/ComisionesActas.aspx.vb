Namespace SRA

Partial Class ComisionesActas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents imgCerrarPrincipal As System.Web.UI.WebControls.ImageButton
   Protected WithEvents hdnSess As System.Web.UI.WebControls.TextBox


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ComisionesActas
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mdsDatos = Session(Request("sess"))
         hdnCdocId.text = Request("crac_cdoc_id")
         usrCria.cmbCriaRazaExt.Valor = Request("Raza")
         usrCria.cmbCriaRazaExt.Enabled = False

         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mSetearMaxLength()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtTema.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "crac_tema")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mdsDatos.Tables(mstrTabla).DefaultView.RowFilter = "crac_cdoc_id=" + hdnCdocId.Text
         grdDato.DataSource = mdsDatos.Tables(mstrTabla).DefaultView
         grdDato.DataBind()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mLimpiar()
         Dim ldrActa As DataRow

         hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

         ldrActa = mdsDatos.Tables(mstrTabla).Select("crac_id=" + hdnId.Text)(0)

         With ldrActa
            usrCria.Valor = .Item("crac_cria_id")
            txtTema.Valor = .Item("crac_tema")
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      txtTema.Text = ""
      usrCria.Limpiar()
      usrCria.cmbCriaRazaExt.Valor = Request.QueryString("Raza")
      usrCria.cmbCriaRazaExt.Enabled = False
      mSetearEditor(True)
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTabla).Select("crac_id=" & hdnId.Text)(0)
         row.Delete()
         grdDato.DataSource = mdsDatos.Tables(mstrTabla)
         grdDato.DataBind()
         mLimpiar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If usrCria.Valor.ToString = "" And txtTema.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el campo Criador y/o Tema.")
      End If
   End Sub
   Private Sub mGuardarDatos()
      Dim ldrActa As DataRow

      mValidarDatos()
      If hdnId.Text = "" Then
         ldrActa = mdsDatos.Tables(mstrTabla).NewRow
         ldrActa.Item("crac_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTabla), "crac_id")
      Else
         ldrActa = mdsDatos.Tables(mstrTabla).Select("crac_id=" & hdnId.Text)(0)
      End If

      With ldrActa
         .Item("crac_cdoc_id") = hdnCdocId.Text
         .Item("crac_cria_id") = usrCria.Valor
         .Item("_criador") = usrCria.Apel
         .Item("crac_tema") = txtTema.Valor
         .Item("crac_audi_user") = Session("sUserId").ToString()
      End With

      If (hdnId.Text = "") Then
         mdsDatos.Tables(mstrTabla).Rows.Add(ldrActa)
      End If
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub imgClosePrincipal_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClosePrincipal.Click
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub
#End Region

End Class
End Namespace
