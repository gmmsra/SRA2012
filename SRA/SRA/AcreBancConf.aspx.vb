Namespace SRA

Partial Class AcreBancConf
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

    Protected WithEvents lblImpo As System.Web.UI.WebControls.Label
    Protected WithEvents lblCuba As System.Web.UI.WebControls.Label
    Protected WithEvents cmbanc As NixorControls.ComboBox
    Protected WithEvents lblIngrFecha As System.Web.UI.WebControls.Label


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ComprobPagos
   Private mstrTablaAsiento As String = "rechazo_pagos_asiento"

   Private mstrConn As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      Id = 2
      EstadoOri = 3
      Centro = 5
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         mSetearEventos()

         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            hdnEmctId.Text = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
            mCargarCombos()
            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancFil, "S", "@con_cuentas=1")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBanc, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancOrig, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConc, "S", "@conc_acti_id=1")
        clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCcos, "S")
        Me.CargarComboEmpresasCobradorasElectronicas()
    End Sub
    ' Dario 2013-06-18 metodo que se utiliza para la carge del combo de empresas cobradoras electronicas    
    Private Sub CargarComboEmpresasCobradorasElectronicas()
        Try
            ' coloco el valor por defecto del combo
            cmbEmpCobElecFil.Items.Add(New System.Web.UI.WebControls.ListItem("(Todos)", "-1"))
            ' instancio el objeto cobranzas de negocio
            Dim _CobranzasBusiness As New Business.Facturacion.CobranzasBusiness
            ' ejecuto el metodo que recupera los datos a mostar en la grilla
            Dim ds As DataSet = _CobranzasBusiness.GetEmpresasCobranzasElectronicas(Me.User.Identity.Name)
            ' controlo que tenga algo la consulta de empresas
            If (Not ds Is Nothing And ds.Tables.Count > 0) Then
                ' si tiene 0 o mas de uno agrega el seleccione sino deja solo el unico que tiene
                If (ds.Tables(0).Rows.Count > 0) Then
                    cmbEmpCobElecFil.Items.Add(New System.Web.UI.WebControls.ListItem("(Solo Acrd. Bacrarias)", "0"))
                End If
                ' recorre el resultado de la consulta y lo carga en el combo
                For Each dr As DataRow In ds.Tables(0).Rows
                    ' agrego el item 
                    cmbEmpCobElecFil.Items.Add( _
                                                New System.Web.UI.WebControls.ListItem(dr("varDesEmpresaCobElect").ToString() _
                                                                                          , dr("intIdEmprezasCobranzasElectronicas").ToString()))
                Next
            End If
            ds.Dispose()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooRech As Boolean)
        btnModi.Enabled = Not (pbooRech)
        btnModiComp.Enabled = Not (pbooRech)
    End Sub

    Private Sub mSetearEventos()
        btnConf.Attributes.Add("onclick", "if(!confirm('Confirma la autorización de las acreditaciones?')) return false;")
        btnModi.Attributes.Add("onclick", "if(!confirm('Confirma la anulación de la acreditación?')) return false;")
        btnModiComp.Attributes.Add("onclick", "if(!confirm('Confirma el rechazo de la acreditación y la generación de la ND?')) return false;")
    End Sub

    Public Sub mCargarDatos(ByVal pstrId As String)
        Dim lbooRech As Boolean
        Dim ldsEsta As DataSet = CrearDataSet(pstrId)

        With ldsEsta.Tables(0).Rows(0)
            hdnId.Text = .Item("paco_id").ToString()
            txtImpo.Valor = .Item("paco_impo")
            cmbMone.Valor = .Item("paco_mone_id")
            cmbBanc.Valor = .Item("_cuba_banc_id")
            clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuba, "", cmbBanc.Valor)
            cmbCuba.Valor = .Item("paco_cuba_id")
            txtNume.Valor = .Item("paco_nume")
            usrClie.Valor = .Item("_comp_clie_id")
            txtFecha.Fecha = .Item("_comp_fecha")
                txtDepoFecha.Fecha = .Item("paco_acre_depo_fecha").ToString
                txtIngrFecha.Fecha = .Item("_comp_ingr_fecha").ToString
                cmbCentroEmisor.Valor = .Item("_comp_emct_id").ToString
                cmbBancOrig.Valor = .Item("paco_orig_banc_id").ToString
                txtSucursal.Valor = .Item("paco_orig_banc_suc").ToString
                txtClie.Valor = .Item("_code_reci_nyap").ToString
                cmbConc.Valor = .Item("paco_conc_id").ToString
                cmbCcos.Valor = .Item("paco_ccos_id").ToString

            txtClie.Visible = (txtClie.Text <> "")
            usrClie.Visible = (txtClie.Text = "")

            If Not .IsNull("paco_acre_audi_fech") Then
                lblBaja.Text = "Acredicatión bancaria rechazada en fecha: " & CDate(.Item("paco_acre_audi_fech")).ToString("dd/MM/yyyy HH:mm")
                lbooRech = True
                cmbConc.Enabled = False
                cmbCcos.Enabled = False
            Else
                lblBaja.Text = ""
                cmbConc.Enabled = True
                cmbCcos.Enabled = True
            End If

            If Not .IsNull("paco_acre_audi_fech") Then
                lblBaja.Text = "Acredicatión bancaria rechazada en fecha: " & CDate(.Item("paco_acre_audi_fech")).ToString("dd/MM/yyyy HH:mm")
                lbooRech = True
            Else
                lblBaja.Text = ""
            End If
            ' el comprobante fue ingresado por cobro electronico, no se puede anular
            If (.Item("_intNroComprobante") > 0) Then
                lbooRech = True
            End If

        End With

        mSetearEditor("", lbooRech)
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        lblTitu.Text = ""

        txtImpo.Text = ""
        cmbBanc.Limpiar()
        cmbBancOrig.Limpiar()
        txtMoti.Text = ""
        txtNume.Text = ""
        txtMoti.Text = ""
        txtFecha.Text = ""
        txtIngrFecha.Text = ""
        cmbMone.Limpiar()
        cmbCentroEmisor.Limpiar()
        txtSucursal.Text = ""
        usrClie.Limpiar()
        txtClie.Text = ""

        mSetearEditor("", True)
    End Sub

    Private Sub mCerrar()
        mLimpiar()
        grdDato.DataSource = mdsDatos
        grdDato.DataBind()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible
        btnConf.Visible = Not panDato.Visible

        If pbooVisi Then
            grdDato.DataSource = Nothing
            grdDato.DataBind()
        End If
    End Sub

    Private Sub mLimpiarFiltros()
        txtFechaFil.Text = ""
        cmbBancFil.Limpiar()
        cmbCubaFil.Items.Clear()
        txtNumeFil.Text = ""
        chkRech.Checked = False
        chkApro.Checked = False
        usrClieFil.Limpiar()
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mConfMul()
        Dim lbooCambio As Boolean
        Dim lbooCheck As Boolean
        Dim lstrEstaOri As String
        Dim lstrRet As String
        Dim lstrCentro As String
        Dim ldsDatos As DataSet
        Dim ldrPaco As DataRow
        Dim lstrReciIds, lstrReciNumes, lstrPacoIdsError, lstrPacoNumesError, lstrMsg, lstrError As String
        Dim lstrCajaError As String = ""
        Dim lstrErrorCtros As String = ""
        Dim lobjNeg As SRA_Neg.Generica

        Try
            For Each oItem As DataGridItem In grdDato.Items
                lbooCheck = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked
                lstrEstaOri = oItem.Cells(Columnas.EstadoOri).Text

                'si se aprobó o se canceló la aprobación
                If (lbooCheck And lstrEstaOri = "-1") Or (Not lbooCheck And lstrEstaOri = "1") Then
                    lbooCambio = True
                    ldsDatos = CrearDataSet(oItem.Cells(Columnas.Id).Text)

                    'obtengo el registro a modificar
                    ldrPaco = ldsDatos.Tables(mstrTabla).Rows(0)

                    With ldrPaco
                        If lbooCheck Then
                            'actualizo con los datos nuevos
                            .Item("paco_acre_fecha") = Today
                            .Item("paco_acre_audi_fech") = Now
                            .Item("paco_acre_audi_user") = Session("sUserId")
                            .Item("paco_acre_apro") = True
                        Else
                            'cancelo la aprobación
                            .Item("paco_acre_fecha") = DBNull.Value
                            .Item("paco_acre_audi_fech") = DBNull.Value
                            .Item("paco_acre_audi_user") = DBNull.Value
                            .Item("paco_acre_apro") = DBNull.Value
                        End If
                    End With

                    If lobjNeg Is Nothing Then
                        lobjNeg = New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
                    Else
                        lobjNeg.MiDataSet = ldsDatos
                    End If

                    Try
                        lstrRet = lobjNeg.ModiRet()

                        If lstrRet <> "0" Then
                            lstrCentro = oItem.Cells(Columnas.Centro).Text
                            lstrErrorCtros = "," & lstrCajaError & ","
                            If lstrErrorCtros.IndexOf("," & lstrCentro & ",") = -1 Then
                                If lstrCajaError <> "" Then
                                    lstrCajaError += ","
                                End If
                                lstrCajaError = lstrCajaError & lstrCentro
                            End If
                        Else
                            If lstrReciIds <> "" Then
                                lstrReciIds += ","
                                lstrReciNumes += ","
                            End If
                            lstrReciIds += ldrPaco.Item("paco_id").ToString
                            lstrReciNumes += clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, ldrPaco.Item("paco_id")).Tables(0).Rows(0).Item("_comp_nume")
                        End If

                    Catch ex As Exception
                        If lstrPacoIdsError <> "" Then
                            lstrPacoIdsError += ","
                            lstrPacoNumesError += ","
                        End If
                        lstrError = ex.Message

                        lstrPacoIdsError += ldrPaco.Item("paco_id").ToString
                        lstrPacoNumesError += ldrPaco.Item("paco_nume").ToString
                    End Try
                End If
            Next

            If lbooCambio Then
                If lstrReciNumes <> "" Then
                    ' Dario 2013-07-30 Nueva funcion de mail que envia ND y recibos
                    ' instancio el objeto cobranzas de negocio
                    Dim _AcreditacionBancariaBusiness As New Business.Facturacion.AcreditacionBancariaBusiness
                    ' ejecuto el metodo que recupera los datos a mostar en la grilla
                    Dim respuesta As String = _AcreditacionBancariaBusiness.EnviarMailDeConfirmacionAcreditaciones(lstrReciIds, Me.Session("sUsuaEmail").ToString)
                    ' paso la respuesta que tiene los numeros generados de comprobantes de los recibos y notas de debitos
                    If (respuesta.Trim.Length > 0) Then
                        lstrMsg = respuesta
                    Else
                        lstrMsg = "Se generaron los recibos: " + lstrReciNumes
                    End If
                    ' Dario se envia mail de los que no salen por alerta
                    mEnviarMail(lstrReciIds, SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_CONFIRMACION)
                End If

                If lstrPacoNumesError <> "" Then
                    If lstrMsg <> "" Then
                        lstrMsg += ". "
                    End If
                    lstrMsg += "Las siguientes acreditaciones dieron error al confirmarse: " + lstrPacoNumesError + " (" + lstrError + ")"
                End If

                If lstrCajaError <> "" Then
                    If lstrMsg <> "" Then
                        lstrMsg += ". "
                    End If
                    lstrMsg += "Las acreditaciones de los siguientes centros no se pueden confirmar por estar la caja respectiva cerrada (esperar al dia siguiente): " + lstrCajaError
                End If

                If lstrMsg <> "" Then
                    clsError.gGenerarMensajes(Me, lstrMsg)
                End If
                ' Dario se comenta 
                'mEnviarMail(lstrReciIds, SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_CONFIRMACION)
                mConsultar()
            Else
                Throw New AccesoBD.clsErrNeg("Debe seleccionar una acreditación")
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mEnviarMail(ByVal pstrReciIds As String, ByVal pstrAlerTipo As String)
        Dim lstrAlerId As String
        Dim lDsAler As DataSet

        lDsAler = clsSQLServer.gObtenerEstruc(mstrConn, "alertas", "@aler_alti_id=" & pstrAlerTipo)
        If lDsAler.Tables(0).Rows.Count > 0 Then
            SRA_Neg.Alertas.EnviarMailAlertas(mstrConn, Session("sUserId").ToString(), lDsAler.Tables(0).Rows(0).Item("aler_id"), clsSQLServer.gFormatArg(pstrReciIds, SqlDbType.VarChar))
        End If
    End Sub

    Private Sub mModi()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatosRechazo()

            If txtMoti.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el motivo de la anulación de la acreditación.")
            End If

            Dim lobjNeg As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla + "_rechazo", ldsEstruc)
            lobjNeg.Alta()

            'enviar alertas
            Dim lstrReciId = clsSQLServer.gCampoValorConsul(mstrConn, "comprob_pagos_consul @paco_id=" & hdnId.Text, "paco_comp_id")
            mEnviarMail(lstrReciId, SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_ANULACION)

            'mConsultar()
            mdsDatos.Tables(0).Select("paco_id=" & hdnId.Text)(0).Delete()
            grdDato.DataSource = mdsDatos
            grdDato.DataBind()
            mLimpiar()
            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModiComp()
        Try
            Dim lstrCompId As String
            Dim ldsEstruc As DataSet = mGuardarDatosComp()

            Dim lobjNeg As New SRA_Neg.Cheques(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lstrCompId = lobjNeg.RechazarCupones(True)

            clsError.gGenerarMensajes(Me, "Se generó la ND " + clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantesX", lstrCompId, "numero"))
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mGuardarDatosRechazo(Optional ByVal pdsDatos As DataSet = Nothing) As DataSet
        Dim ldrDatos As DataRow

        If pdsDatos Is Nothing Then
            pdsDatos = New DataSet
        End If

        With pdsDatos.Tables.Add(mstrTabla + "_rechazo")
            .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_paco_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
            .Columns.Add("paco_baja_moti", System.Type.GetType("System.String"))
            ldrDatos = .NewRow
        End With

        With ldrDatos
            .Item("proc_id") = clsSQLServer.gObtenerId(.Table, "proc_id")
            .Item("proc_paco_id") = hdnId.Text
            .Item("paco_baja_moti") = txtMoti.Valor.ToString
            .Table.Rows.Add(ldrDatos)
        End With

        Return pdsDatos
    End Function

    Private Function mGuardarDatosComp() As DataSet
        Dim ldsDatos As DataSet
        Dim ldrComp, ldrCode, ldrCoco, ldrCovt, ldrAsie As DataRow
        Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
        Dim lstrHost, lstrCemiNume, lstrEmctId As String
        Dim lDrPaco As DataRow
        Dim lDrMovim As DataRow

        mValidarDatos()

        'obtengo el dataset de COMPROBANTES vacío
        ldsDatos = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, "")

        ldsDatos.Tables.Add(mstrTablaAsiento)
        With ldsDatos.Tables(mstrTablaAsiento)
            .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_paco_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_comp_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
        End With

        ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).Rows(0).Delete()

        ldrComp = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).NewRow

        'COMPROBANTES
        With ldrComp
            oFact.CentroEmisorNro(mstrConn)
            lstrCemiNume = oFact.pCentroEmisorNro
            lstrEmctId = oFact.pCentroEmisorId
            lstrHost = oFact.pHost

            .Item("comp_id") = clsSQLServer.gObtenerId(.Table, "comp_id")
            .Item("comp_fecha") = Today
            .Item("comp_ingr_fecha") = .Item("comp_fecha")
            .Item("comp_impre") = 0
            .Item("comp_clie_id") = usrClie.Valor
            .Item("comp_dh") = 0    '0 = debe
            .Item("comp_cance") = 0 ' 0 = cancelado
            .Item("comp_cs") = 1 ' cta cte
            .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.ND
            .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.AcreditacionesConfirmación
            .Item("comp_mone_id") = 1
            .Item("comp_neto") = txtImpo.Valor
            .Item("comp_letra") = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", .Item("comp_clie_id").ToString).Tables(0).Rows(0).Item("_ivap_letra")
            .Item("comp_cpti_id") = 2 ' 2 = cta.cte
            .Item("comp_acti_id") = SRA_Neg.Constantes.Actividades.Administracion
            .Item("comp_cemi_nume") = lstrCemiNume
            .Item("comp_emct_id") = lstrEmctId
            .Item("comp_host") = lstrHost

            .Table.Rows.Add(ldrComp)
        End With

        'COMPROB_DETA
        ldrCode = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobDeta).NewRow
        With ldrCode
            .Item("code_id") = clsSQLServer.gObtenerId(.Table, "code_id")
            .Item("code_comp_id") = ldrComp.Item("comp_id")
            .Item("code_coti") = 1
            .Item("code_pers") = True
            .Item("code_impo_ivat_tasa") = 0
            .Item("code_impo_ivat_tasa_redu") = 0
            .Item("code_impo_ivat_tasa_sobre") = 0
            .Item("code_impo_ivat_perc") = 0

            .Table.Rows.Add(ldrCode)
        End With

        'COMPROB_VTOS
        ldrCovt = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobVtos).NewRow
        With ldrCovt
            .Item("covt_id") = clsSQLServer.gObtenerId(.Table, "covt_id")
            .Item("covt_comp_id") = ldrComp.Item("comp_id")
            .Item("covt_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, ldrComp.Item("comp_acti_id"), ldrComp.Item("comp_fecha"))
            .Item("covt_porc") = 100
            .Item("covt_impo") = ldrComp.Item("comp_neto")
            .Item("covt_cance") = 0

            .Table.Rows.Add(ldrCovt)
        End With

        'COMPROB_CONCEPTOS
        ldrCoco = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobConcep).NewRow
        With ldrCoco
            .Item("coco_id") = clsSQLServer.gObtenerId(.Table, "coco_id")
            .Item("coco_comp_id") = ldrComp.Item("comp_id")
            .Item("coco_conc_id") = cmbConc.Valor
            .Item("coco_ccos_id") = cmbCcos.Valor
            .Item("coco_impo") = ldrComp.Item("comp_neto")
            .Item("coco_impo_ivai") = .Item("coco_impo")
            .Table.Rows.Add(ldrCoco)
        End With

        'ASIENTOS
        ldrAsie = ldsDatos.Tables(mstrTablaAsiento).NewRow
        With ldrAsie
            .Item("proc_id") = clsSQLServer.gObtenerId(.Table, "proc_id")
            .Item("proc_paco_id") = hdnId.Text
            .Table.Rows.Add(ldrAsie)
        End With

        'obtengo el registro a modificar
        lDrPaco = SRA_Neg.Utiles.gAgregarRegistro(mstrConn, ldsDatos.Tables(mstrTabla), hdnId.Text)

        'actualizo con los datos nuevos
        With lDrPaco
            .Item("paco_conc_id") = cmbConc.Valor
            .Item("paco_ccos_id") = cmbCcos.Valor
            .Item("paco_acre_fecha") = Today
            .Item("paco_acre_audi_fech") = Now
            .Item("paco_acre_audi_user") = Session("sUserId")
            .Item("paco_acre_apro") = False  'rechazada
        End With

        ldsDatos = mGuardarDatosRechazo(ldsDatos)

        Return ldsDatos
    End Function

    Private Function CrearDataSet(ByVal pstrId As String) As DataSet
        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        ldsEsta.Tables(0).TableName = mstrTabla
        Return ldsEsta
    End Function

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
        Public Sub mConsultar()
            Dim lstrCmd As New StringBuilder
            Try
                'Dim lstrCmd As New StringBuilder
                lstrCmd.Append("exec comprob_pagos_busq")
                lstrCmd.Append(" @paco_acre_depo_fecha=" & clsFormatear.gFormatFecha2DB(txtFechaFil.Fecha))
                lstrCmd.Append(",@cuba_banc_id =" + cmbBancFil.Valor.ToString)
                lstrCmd.Append(",@paco_cuba_id =" + cmbCubaFil.Valor.ToString)
                lstrCmd.Append(",@paco_nume =" + clsSQLServer.gFormatArg(txtNumeFil.Valor.ToString, SqlDbType.VarChar))
                lstrCmd.Append(",@paco_pati_id =" + CType(SRA_Neg.Constantes.PagosTipos.AcredBancaria, String))
                lstrCmd.Append(",@clie_id =" + usrClieFil.Valor.ToString)
                lstrCmd.Append(",@rechazado=" + Math.Abs(CInt(chkRech.Checked)).ToString)
                lstrCmd.Append(",@aprobada=" + Math.Abs(CInt(chkApro.Checked)).ToString)
                ' Dario 2013-06-18 se agragan dos nuevos filtros
                lstrCmd.Append(",@IntIdEmpCobElec =" + cmbEmpCobElecFil.Valor.ToString)
                lstrCmd.Append(",@varNroCompCobElect =" + clsSQLServer.gFormatArg(txtNroCompCobElectFil.Valor.ToString, SqlDbType.VarChar))

                mdsDatos = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

                grdDato.CurrentPageIndex = 0
                grdDato.DataSource = mdsDatos
                grdDato.DataBind()

                Session(mstrTabla) = mdsDatos
                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, New Exception(ex.Message + " ** " + lstrCmd.ToString))
            End Try
        End Sub

        Private Sub mGuardarChechk()
        Dim dr As DataRow
        'Mantengo los check de las paguinas de la grilla
        For Each oDataItem As DataGridItem In grdDato.Items
            If mdsDatos.Tables(0).Select("paco_id = " & oDataItem.Cells(2).Text).Length > 0 Then
                dr = mdsDatos.Tables(0).Select("paco_id = " & oDataItem.Cells(2).Text)(0)
                dr.Item("chk") = DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked
            End If
        Next
        Session(mstrTabla) = mdsDatos
    End Sub

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            mGuardarChechk()

            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If

            grdDato.DataSource = mdsDatos
            grdDato.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            mGuardarChechk()
            mCargarDatos(e.Item.Cells(Columnas.Id).Text)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub grdDato_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDato.ItemDataBound
        Try
            If e.Item.ItemIndex <> -1 Then
                Dim lbooConf As Boolean
                Dim lbooRech As Boolean

                With CType(e.Item.DataItem, DataRowView).Row
                    lbooConf = Not .IsNull("paco_acre_apro") AndAlso .Item("paco_acre_apro")
                    lbooRech = Not .IsNull("paco_acre_apro") AndAlso Not .Item("paco_acre_apro")
                End With

                With DirectCast(e.Item.FindControl("chkSel"), CheckBox)
                    .Visible = Not lbooRech
                    If Not .Checked Then .Checked = lbooConf
                End With

                DirectCast(e.Item.FindControl("LinkButton1"), LinkButton).Visible = Not lbooConf

                With e.Item.Cells(Columnas.EstadoOri)
                    If lbooConf Then
                        .Text = "1"
                    ElseIf lbooRech Then
                        .Text = "0"
                    Else
                        .Text = "-1"
                    End If
                End With
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Eventos"
    Private Sub btnConf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConf.Click
        mGuardarChechk()
        mConfMul()
    End Sub

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnModiComp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiComp.Click
        mModiComp()
    End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mCerrar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            grdDato.CurrentPageIndex = 0
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub
#End Region

End Class

End Namespace
