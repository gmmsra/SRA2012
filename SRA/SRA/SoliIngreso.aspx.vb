Namespace SRA

    Partial Class SoliIngreso
        Inherits FormGenerico

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Definición de Variables"
        Private mstrTabla As String = SRA_Neg.Constantes.gTab_SoliIngreso

        Private mstrConn As String
        Private mstrParaPageSize As Integer
#End Region

#Region "Inicialización de Variables"
        Private Sub mInicializar()
            Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
            txtNaci.Enabled = hdnPeti.Text <> "J"
            txtEman.Enabled = hdnPeti.Text <> "J"
        End Sub

#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                mInicializar()

                mSetearCate()

                If (Not Page.IsPostBack) Then
                    mSetearEventos()
                    mSetearMaxLength()

                    mCargarCombos()
                    mConsultar()

                    clsWeb.gInicializarControles(Me, mstrConn)
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mSetearEventos()
            btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        End Sub


        Private Sub mSetearMaxLength()
            Dim lstrLong As Object

            lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
            txtNroSociAnt.Valor = clsSQLServer.gObtenerLongitud(lstrLong, "soin_ante_soci")

        End Sub

        Private Sub mSetearCate()
            Dim lbooDist As Boolean
            Dim lbooTitu As Boolean
            Dim lbooPciaLoc As Boolean

            If Not cmbCate.Valor Is DBNull.Value Then
                With clsSQLServer.gObtenerEstruc(mstrConn, "categorias", cmbCate.Valor.ToString).Tables(0).Rows(0)
                    If .Item("cate_dist") Is DBNull.Value Then
                        lbooDist = False
                        lbooPciaLoc = False
                    Else
                        lbooDist = .Item("cate_dist")
                        lbooPciaLoc = lbooDist
                    End If

                    If .Item("cate_titu") Is DBNull.Value Then
                        lbooTitu = False
                    Else
                        lbooTitu = .Item("cate_titu")
                    End If


                End With
            End If

            cmbDist.Enabled = lbooDist
            cmbDirePcia.Enabled = lbooPciaLoc
            cmbDireLoca.Enabled = lbooPciaLoc
            If Not cmbDist.Enabled Then
                cmbDist.Limpiar()
                cmbDirePcia.Limpiar()
                cmbDireLoca.Limpiar()
            Else
                If Not Request.Form(cmbDist.UniqueID) Is Nothing Then
                    cmbDist.Valor = Request.Form(cmbDist.UniqueID)
                End If

                If Not Request.Form(cmbDirePcia.UniqueID) Is Nothing Then
                    cmbDirePcia.Valor = Request.Form(cmbDirePcia.UniqueID)
                End If

                If Not Request.Form(cmbDireLoca.UniqueID) Is Nothing Then
                    cmbDireLoca.Valor = Request.Form(cmbDireLoca.UniqueID)
                End If
            End If

            usrTituClie.Activo = Not lbooTitu
            clsWeb.gActivarControl(usrTituClie.txtCodiExt, Not lbooTitu)
            clsWeb.gActivarControl(usrTituClie.txtApelExt, Not lbooTitu)

            usrTituSoci.Activo = Not lbooTitu
            clsWeb.gActivarControl(usrTituSoci.txtCodiExt, Not lbooTitu)
            clsWeb.gActivarControl(usrTituSoci.txtApelExt, Not lbooTitu)

            If Not usrTituClie.Activo Then
                usrTituClie.Limpiar()
                usrTituSoci.Limpiar()
            Else
                If Request.Form(usrTituClie.txtIdExt.UniqueID) <> "" Then
                    usrTituClie.Valor = Request.Form(usrTituClie.txtIdExt.UniqueID)
                End If
                If Request.Form(usrTituSoci.txtIdExt.UniqueID) <> "" Then
                    usrTituSoci.Valor = Request.Form(usrTituSoci.txtIdExt.UniqueID)
                End If
            End If
        End Sub

        Private Sub mCargarCombos()
            clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "distritos", cmbDist, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDirePcia, "S", "0" + "350")
        End Sub
#End Region

#Region "Seteo de Controles"
        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, ByVal pbooActivo As Boolean, ByVal pbooDist As Boolean, ByVal pbooTitu As Boolean, ByVal pbooActa As Boolean, pbooPciaLoc As Boolean)
            btnBaja.Enabled = Not (pbooAlta) And pbooActivo
            btnCobra.Disabled() = (pbooAlta) And Not pbooActivo
            btnModi.Enabled = Not (pbooAlta) And pbooActivo
            btnAlta.Enabled = pbooAlta And pbooActivo

            usrClie.Activo = pbooActivo And pbooActa

            txtPresFecha.Enabled = pbooActivo

            txtVigeFecha.Enabled = pbooActivo
            txtEman.Enabled = pbooActivo
            txtNaci.Enabled = pbooActivo
            cmbCate.Enabled = pbooActivo And pbooActa
            cmbDist.Enabled = pbooActivo And pbooDist
            cmbDirePcia.Enabled = pbooActivo And pbooPciaLoc
            cmbDireLoca.Enabled = pbooActivo And pbooPciaLoc
            usrPresSoci.Activo = pbooActivo
            usrTituClie.Activo = pbooActivo And pbooTitu
            usrTituSoci.Activo = pbooActivo And pbooTitu
            txtNroSociAnt.Enabled = pbooActivo And pbooActa
        End Sub

        Public Sub mCargarDatos(ByVal pstrId As String)
            Dim lbooActivo As Boolean
            Dim lbooActa As Boolean
            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            With ldsEsta.Tables(0).Rows(0)
                hdnId.Text = .Item("soin_id").ToString()

                usrClie.Valor = .Item("soin_clie_id")
                txtNume.Valor = .Item("_soin_nume")
                txtPresFecha.Fecha = .Item("soin_pres_fecha").ToString
                txtEman.Fecha = .Item("soin_eman_fecha").ToString
                txtNaci.Fecha = .Item("soin_naci_fecha").ToString

                txtVigeFecha.Fecha = .Item("soin_vige_fecha").ToString
                cmbCate.Valor = .Item("soin_cate_id")
                lblEntid.Text = .Item("_enti")

                mSetearCate()

                cmbDist.Valor = .Item("soin_dist_id").ToString

                usrPresSoci.Valor = .Item("soin_pres_soci_id")
                usrTituClie.Valor = .Item("soin_titu_clie_id")
                usrTituSoci.Valor = .Item("soin_titu_soci_id")
                txtNroSociAnt.Valor = .Item("soin_ante_soci").ToString

                hdnEstaUlti.Text = .Item("soin_ante_esta_id").ToString
                txtMoti.Valor = .Item("soin_ante").ToString
                lblEstaUlti.Text = .Item("_ante_esta_desc")

                If Not .IsNull("soin_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("soin_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If

                lblEsta.Text = .Item("_estado").ToString

                lbooActivo = .IsNull("soin_resu")
                lblSoci.Text = .Item("_soci_nume").ToString
                lbooActa = .IsNull("_acta")

                'Busco los datos de la localidad.
                If Not .IsNull("soin_ainfluencia_loca_id") Then
                    Dim pcia_id As String = String.Empty
                    Dim dsPcia As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, "soli_provincia", .Item("soin_ainfluencia_loca_id").ToString())
                    With dsPcia.Tables(0).Rows(0)
                        pcia_id = .Item("pcia_id").ToString()
                        clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbDireLoca, "S", "0" + pcia_id)
                    End With
                    cmbDirePcia.Valor = pcia_id
                    cmbDireLoca.Valor = .Item("soin_ainfluencia_loca_id").ToString
                Else
                    cmbDirePcia.Valor = String.Empty
                End If

            End With

            If hdnEstaUlti.Text = SRA_Neg.Constantes.Estados.Socios_Eliminado Then
                txtMoti.Enabled = True
            End If

            lblTitu.Text = "Registro Seleccionado: " + txtNume.Text + " - " + usrClie.Apel

            mSetearEditor("", False, lbooActivo, cmbDist.Enabled, usrTituClie.Activo, lbooActa, cmbDirePcia.Enabled)
            mMostrarPanel(True)
        End Sub

        Private Sub mAgregar()
            mLimpiar()
            btnBaja.Enabled = False
            btnCobra.Disabled = True
            btnModi.Enabled = False
            mMostrarPanel(True)
        End Sub

        Private Sub mLimpiar()
            hdnId.Text = ""
            lblTitu.Text = ""

            usrClie.Limpiar()
            txtNume.Text = ""
            txtPresFecha.Text = ""

            txtVigeFecha.Text = ""
            hdnPeti.Text = ""
            cmbCate.Limpiar()
            usrPresSoci.Limpiar()
            usrTituClie.Limpiar()
            usrTituSoci.Limpiar()
            txtNaci.Text = ""
            txtEman.Text = ""
            lblSoci.Text = ""
            lblBaja.Text = ""
            lblEsta.Text = ""
            txtNroSociAnt.Text = ""
            lblEntid.Text = ""
            hdnEstaUlti.Text = ""
            txtMoti.Valor = ""
            lblEstaUlti.Text = ""
            txtMoti.Enabled = False
            cmbDist.Limpiar()
            cmbDireLoca.Limpiar()
            'cmbDirePcia.Limpiar()

            mSetearEditor("", True, True, True, True, True, True)
        End Sub

        Private Sub mCerrar()
            mLimpiar()
            mConsultar()
        End Sub

        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            panDato.Visible = pbooVisi
            panBotones.Visible = pbooVisi
            panFiltro.Visible = Not panDato.Visible
            btnAgre.Visible = Not panDato.Visible
            grdDato.Visible = Not panDato.Visible


            If pbooVisi Then
                grdDato.DataSource = Nothing
                grdDato.DataBind()
            End If
        End Sub


#End Region

#Region "Opciones de ABM"
        Private Sub mAlta()
            Try
                Dim lstrSoliId As String
                Dim ldsEstruc As DataSet = mGuardarDatos()

                Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
                lstrSoliId = lobjGenerico.Alta()

                mConsultar()

                hdnAltaId.Text = "La solicitud ha sido dada de alta con el número " & lstrSoliId.ToString

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mModi()
            Try
                Dim ldsEstruc As DataSet = mGuardarDatos()

                Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
                lobjGenerico.Modi()

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mBaja()
            Try
                Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
                lobjGenerico.Baja(hdnId.Text)

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Function mGuardarDatos() As DataSet
            mValidarDatos()

            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

            With ldsEsta.Tables(0).Rows(0)
                .Item("soin_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("soin_clie_id") = usrClie.Valor
                .Item("soin_pres_fecha") = txtPresFecha.Fecha
                If txtEman.Fecha.ToString.Length > 0 Then
                    .Item("soin_eman_fecha") = txtEman.Fecha
                End If
                If (txtNaci.Fecha.Trim.Length > 0) Then
                    .Item("soin_naci_fecha") = txtNaci.Fecha
                End If

                .Item("soin_vige_fecha") = txtVigeFecha.Fecha
                .Item("soin_cate_id") = cmbCate.Valor
                If cmbDist.Valor.Length > 0 Then
                    .Item("soin_dist_id") = cmbDist.Valor
                End If
                .Item("soin_pres_soci_id") = usrPresSoci.Valor
                .Item("soin_titu_clie_id") = usrTituClie.Valor
                .Item("soin_titu_soci_id") = usrTituSoci.Valor
                If txtNroSociAnt.Valor.Length > 0 Then
                    .Item("soin_ante_soci") = txtNroSociAnt.Valor
                End If

                .Item("soin_ante_esta_id") = IIf(hdnEstaUlti.Text = "", 0, hdnEstaUlti.Text)
                .Item("soin_ante") = txtMoti.Valor

                .Item("soin_baja_fecha") = DBNull.Value

                If cmbDireLoca.Valor.Length > 0 Then
                    .Item("soin_ainfluencia_loca_id") = cmbDireLoca.Valor
                Else
                    .Item("soin_ainfluencia_loca_id") = DBNull.Value
                End If

            End With

            Return ldsEsta
        End Function

        Private Sub mValidarDatos()

            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)

        End Sub
#End Region

#Region "Operacion Sobre la Grilla"
        Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.CurrentPageIndex = E.NewPageIndex
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            Try
                mCargarDatos(e.Item.Cells(1).Text)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
#End Region

        Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()
        End Sub

        Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBaja()
        End Sub

        Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi()
        End Sub

        Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar()
        End Sub

        Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
        End Sub



        Public Sub mConsultar()
            Try
                Dim lstrCmd As New StringBuilder
                lstrCmd.Append("exec " + mstrTabla + "_consul")
                lstrCmd.Append(" @apel=" + clsSQLServer.gFormatArg(txtApelFil.Valor, SqlDbType.VarChar))
                lstrCmd.Append(", @fecha_desde=" & clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha.ToString))
                lstrCmd.Append(", @fecha_hasta=" & clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha.ToString))
                lstrCmd.Append(", @soin_nume=" & txtNumeFil.Valor.ToString)

                clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
            Try
                grdDato.CurrentPageIndex = 0
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
            Try
                mAgregar()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub



    End Class

End Namespace
