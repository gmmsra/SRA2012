Namespace SRA

Partial Class CambioPadres
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblCria As System.Web.UI.WebControls.Label



    Protected WithEvents lblSolicitante As System.Web.UI.WebControls.Label
    Protected WithEvents txtSolicitante As NixorControls.TextBoxTab
    Protected WithEvents lblDatosPrdt As System.Web.UI.WebControls.Label
    Protected WithEvents lblPro As System.Web.UI.WebControls.Label
    
    Protected WithEvents panPadresDeta As System.Web.UI.WebControls.Panel
    
    Protected WithEvents grdPadresDeta As System.Web.UI.WebControls.DataGrid
    Protected WithEvents Numberbox1 As NixorControls.NumberBox

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "cambio_padres"
    Private mstrTablaDetalle As String = "cambio_padres_deta"
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
    Private mstrParaPageSize As Integer
    Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
    Const mProdTodos = 0
    Const mProdUno = 1
    Const mProdConjun = 2


#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            minicializar()

            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                mLimpiar()
                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnBajaDeta.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mDetalle()

        If cmbTipo.Valor = mProdTodos Then
            lnkDetalle.Enabled = False
        Else
            lnkDetalle.Enabled = True
        End If
    End Sub
    Public Sub mInicializarDeta()

        usrProductoDeta.Limpiar()
        usrProductoDeta.cmbProdRazaExt.Valor = usrProducto.cmbProdRazaExt.Valor
        usrProductoDeta.cmbProdRazaExt.Enabled = False
        txtAnalisiDeta.Text = ""

        If mdsDatos.Tables(mstrTablaDetalle).Rows.Count > 0 And cmbTipo.Valor <> "P" Then
            cmbTipoDeta.Enabled = False
            'cmbTipoDeta.Valor = mdsDatos.Tables(mstrTablaDetalle).Rows(0)("capd_tipo")
            cmbTipoDeta.Valor = hdnTipoDeta.Text
        Else
            cmbTipoDeta.Enabled = True
        End If

        If cmbTipo.Valor <> "P" Then
            mMostrarPanelMadre(True)
        Else
            mMostrarPanelMadre(False)
        End If

    End Sub
    Public Sub mInicializar()

        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        usrClienteFil.Tabla = mstrClientes
        usrClienteFil.AutoPostback = False
        usrClienteFil.FilClaveUnica = False
        usrClienteFil.ColClaveUnica = True
        usrClienteFil.Ancho = 790
        usrClienteFil.Alto = 510
        usrClienteFil.ColCriaNume = True
        usrClienteFil.ColDocuNume = True
        usrClienteFil.ColCUIT = True
        usrClienteFil.FilCUIT = True
        usrClienteFil.FilDocuNume = True
        usrClienteFil.FilTarjNume = True
        usrClienteFil.FilCriaNume = True

        usrCriadorFil.Tabla = mstrCriadores
        usrCriadorFil.Criador = True
        usrCriadorFil.AutoPostback = False
        usrCriadorFil.FilClaveUnica = False
        usrCriadorFil.ColClaveUnica = True
        usrCriadorFil.Ancho = 790
        usrCriadorFil.Alto = 510
        usrCriadorFil.ColDocuNume = False
        usrCriadorFil.ColCUIT = True
        usrCriadorFil.ColCriaNume = True
        usrCriadorFil.FilCUIT = True
        usrCriadorFil.FilDocuNume = True
        usrCriadorFil.FilAgru = False
        usrCriadorFil.FilTarjNume = False
        usrCriadorFil.FilClieNume = True


        usrProducto.FilSexo = True
        usrProducto.FilRpNume = True
        usrProducto.FilNumExtr = False
        usrProducto.cmbProdRazaExt.Valor = usrRazaCriador.cmbCriaRazaExt.Valor


        usrProductoDeta.FilSexo = False
        usrProductoDeta.FilRpNume = True
        usrProductoDeta.FilNumExtr = False
        usrProductoDeta.Sexo = 1


        usrProdMadre.FilSexo = False
        usrProdMadre.FilRpNume = True
        usrProdMadre.FilNumExtr = False
        usrProdMadre.Sexo = 0

        usrRazaCriador.Tabla = mstrCriadores
        usrRazaCriador.Criador = True
        usrRazaCriador.AutoPostback = False
        usrRazaCriador.FilClaveUnica = False
        usrRazaCriador.FilClieNume = True
        usrRazaCriador.ColClaveUnica = True
        usrRazaCriador.Ancho = 790
        usrRazaCriador.Alto = 510
        usrRazaCriador.ColDocuNume = False
        usrRazaCriador.ColCUIT = True
        usrRazaCriador.ColCriaNume = True
        usrRazaCriador.FilCUIT = True
        usrRazaCriador.FilDocuNume = True
        usrRazaCriador.FilAgru = False
        usrRazaCriador.FilTarjNume = False
        usrRazaCriador.ColClieNume = True

        'usrProd.CriaId = hdnCriaId.Text
        'usrProd.Raza = usrCriaPresta.cmbCriaRazaExt.Valor

    End Sub


    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        'txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "pstm_obse")
    End Sub

    Private Sub mCargarCombos()
        'clsWeb.gCargarCombo(mstrConn, "dbo.implante_ctros_cargar", cmbCtroImpla, "id", "descrip_codi", "S")

    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDetalle.EditItemIndex = -1
            If (grdDetalle.CurrentPageIndex < 0 Or grdDetalle.CurrentPageIndex >= grdDetalle.PageCount) Then
                grdDetalle.CurrentPageIndex = 0
            Else
                grdDetalle.CurrentPageIndex = E.NewPageIndex
            End If
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            'mstrCmd = "exec " + mstrTabla + "_consul "
            'mstrCmd = mstrCmd + " @pstm_hace_clie_id=" + IIf(usrCliePrestaFil.Valor.ToString = "", "0", usrCliePrestaFil.Valor.ToString)
            'mstrCmd = mstrCmd + ",@pstm_reci_clie_id=" + IIf(usrCliePrestataFil.Valor.ToString = "", "0", usrCliePrestataFil.Valor.ToString)
            'mstrCmd = mstrCmd + ",@pstm_hace_cria_id=" + IIf(usrCriaPrestaFil.Valor.ToString = "", "0", usrCriaPrestaFil.Valor.ToString)
            'mstrCmd = mstrCmd + ",@pstm_reci_cria_id=" + IIf(usrCriaPrestataFil.Valor.ToString = "", "0", usrCriaPrestataFil.Valor.ToString)
            'mstrCmd = mstrCmd + ",@fecha_desde=" + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
            'mstrCmd = mstrCmd + ",@fecha_hasta=" + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)

            mstrCmd = "exec " + mstrTabla + "_consul "
            mstrCmd = mstrCmd + " @capa_clie_id=" + IIf(usrClienteFil.Valor.ToString = "", "0", usrClienteFil.Valor.ToString)
            mstrCmd = mstrCmd + ",@capa_cria_id=" + IIf(usrCriadorFil.Valor.ToString = "", "0", usrCriadorFil.Valor.ToString)
            mstrCmd = mstrCmd + ",@capa_prdt_id=" + IIf(usrProdFil.Valor.ToString = "", "0", usrProdFil.Valor.ToString)

            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta

        txtSoliFecha.Enabled = pbooAlta
        usrRazaCriador.Activo = pbooAlta
        usrCliente.Activo = pbooAlta
        usrProducto.Activo = pbooAlta
        txtAnalisis.Enabled = pbooAlta
        cmbTipo.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
        btnBajaDeta.Enabled = Not (pbooAlta)
        btnModiDeta.Enabled = Not (pbooAlta)
        btnAltaDeta.Enabled = pbooAlta
        If cmbTipo.Valor <> "P" Then
            mMostrarPanelMadre(True)
        Else
            lblDetalleDePadres.Visible = True
            cmbTipoDeta.Visible = False
        End If
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                txtSoliFecha.Text() = .Item("capa_soli_fecha")
                usrRazaCriador.Valor = .Item("capa_cria_id")
                usrCliente.Valor = .Item("capa_clie_id")
                usrProducto.Valor = .Item("capa_prdt_id")
                txtAnalisis.Text = .Item("capa_anal_nume")
                cmbTipo.Valor = .Item("capa_tipo")

                If Not .IsNull("capa_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("capa_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If


            End With

            mSetearEditor(False)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub
    Public Sub mEditarPadre()
        Dim ldrDeta As DataRow

        ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("capd_id=" & hdnDetaId.Text)(0)

        With ldrDeta
            If .Item("capd_tipo") Then
                cmbTipoDeta.Visible = False
                lblDetalleDePadres.Visible = True
            Else
                cmbTipoDeta.Valor = .Item("capd_tipo")
                lblDetalleDePadres.Visible = False
            End If

            usrProductoDeta.Valor = .Item("capd_prdt_id")
            If Not .IsNull("capd_anal_nume") Then txtAnalisiDeta.Text = .Item("capd_anal_nume")

            'usrProd.Valor = .Item("pstd_prdt_id")
            If Not .IsNull("capd_baja_fecha") Then
                lblBajaDeta.Text = "Producto dado de baja en fecha: " & CDate(.Item("capd_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
                lblBajaDeta.Text = ""
            End If
        End With
    End Sub
    Public Sub mEditarPareja()
        Dim ldrDeta As DataRow
        Dim ldrDeta2 As DataRow
        ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("capd_pare=" & hdnPareId.Text & "and capd_sexo = 0")(0)
        ldrDeta2 = mdsDatos.Tables(mstrTablaDetalle).Select("capd_pare=" & hdnPareId.Text & "and capd_sexo = 1")(0)

        With ldrDeta2
            cmbTipoDeta.Valor = .Item("capd_tipo")
            usrProductoDeta.Valor = .Item("capd_prdt_id")
            If Not .IsNull("capd_anal_nume") Then txtAnalisiDeta.Text = .Item("capd_anal_nume")

            'usrProd.Valor = .Item("pstd_prdt_id")
            If Not .IsNull("capd_baja_fecha") Then
                lblBajaDeta.Text = "Producto dado de baja en fecha: " & CDate(.Item("capd_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
                lblBajaDeta.Text = ""
            End If
        End With
        With ldrDeta
            usrProdMadre.Valor = .Item("capd_prdt_id")
            txtAnalisisMadre.Text = .Item("capd_anal_nume")

            'usrProd.Valor = .Item("pstd_prdt_id")
            If Not .IsNull("capd_baja_fecha") Then
                lblBajaDeta.Text = "Producto dado de baja en fecha: " & CDate(.Item("capd_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
                lblBajaDeta.Text = ""
            End If

        End With
    End Sub
    Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            hdnDetaId.Text = E.Item.Cells(1).Text
            hdnPareId.Text = E.Item.Cells(2).Text

            If hdnPareId.Text = "0" Or hdnPareId.Text = "" Then
                mEditarPadre()
            Else
                mEditarPareja()
            End If

            mSetearEditorDeta(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosPadres(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDetalle

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        Else
            If mdsDatos.Tables(mstrTablaDetalle).Rows.Count > 0 Then
                hdnTipoDeta.Text = mdsDatos.Tables(mstrTablaDetalle).Rows(0)("capd_tipo")
            End If
        End If
        grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle).DefaultView
        grdDetalle.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""

        usrRazaCriador.Limpiar()
        usrCliente.Limpiar()
        usrProducto.Limpiar()
        txtAnalisis.Text = ""
        cmbTipo.Limpiar()

        lblBaja.Text = ""

        'hdnCriaCodi.Text = ""

        'cmbCtroImpla.Limpiar()
        'cmbTipo.Limpiar()
        'txtFechaDesde.Text = ""
        'txtFechaHasta.Text = ""
        'txtFechaDesdeFil.Text = ""
        'txtFechaHastaFil.Text = ""
        'cmbTipoReporte.Limpiar()
        'hdnCriaId.Text = ""

        'cmbTipo.Limpiar()
        'txtFechaDesde.Text = ""
        'txtFechaHasta.Text = ""


        'usrCliePresta.Limpiar()
        'usrCliePrestata.Limpiar()
        'usrCriaPresta.Limpiar()
        'usrCriaPrestata.Limpiar()

        'lblBaja.Text = ""

        'mLimpiarDeta()

        grdDato.CurrentPageIndex = 0
        grdDetalle.CurrentPageIndex = 0

        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarDeta()
        hdnDetaId.Text = ""

        usrProductoDeta.Valor = ""
        txtAnalisiDeta.Text = ""

        usrProdMadre.Valor = ""
        txtAnalisisMadre.Text = ""

        lblBajaDeta.Text = ""

        mInicializarDeta()

        mSetearEditorDeta(True)
    End Sub
    Private Sub mLimpiarFil()
        'grdDato.CurrentPageIndex = 0
        'usrCliePrestaFil.Limpiar()
        'usrCliePrestataFil.Limpiar()
        'usrCriaPrestaFil.Limpiar()
        'usrCriaPrestataFil.Limpiar()
        usrClienteFil.Limpiar()
        usrCriadorFil.Limpiar()
        usrProdFil.Limpiar()
        mConsultar()
    End Sub

    Private Sub mCerrar()

        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        btnAgre.Visible = panFiltro.Visible
        btnList.Visible = panFiltro.Visible

        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub

    Private Sub mMostrarPanelMadre(ByVal pbooMostrar As Boolean)
        Try
            If pbooMostrar Then
                usrProdMadre.cmbProdRazaExt.Valor = usrRazaCriador.cmbCriaRazaExt.Valor
                usrProdMadre.cmbProdRazaExt.Enabled = False
            End If

            lblDetalleDePadres.Visible = Not pbooMostrar
            cmbTipoDeta.Visible = pbooMostrar

            panDatosMadre.Visible = pbooMostrar

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mOcultarPanelesDeta()
        Try
            panPadresDeta.Visible = False
        Catch ex As Exception
            clsError.gManejarError(Me, ex)

        End Try
    End Sub


#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Alta()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Modi()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If usrRazaCriador.ClientID = "" Then
            Throw New AccesoBD.clsErrNeg("Debe especificar un cliente")
        End If
        'If usrCliePresta.Valor = 0 And Me.usrCriaPresta.Valor = 0 Then
        '    Throw New AccesoBD.clsErrNeg("Debe ingresar el Prestamista (por nro. de cliente o por nro. de criador).")
        'End If

        'If usrCliePrestata.Valor = 0 And Me.usrCriaPrestata.Valor = 0 Then
        '    Throw New AccesoBD.clsErrNeg("Debe ingresar el Prestatario (por nro. de cliente o por nro. de criador).")
        'End If
        'If cmbTipo.Valor = mProdConjun Then
        '    mdsDatos.Tables(mstrTablaDetalle).DefaultView.RowFilter = "pstd_baja_fecha is null"
        '    If mdsDatos.Tables(mstrTablaDetalle).DefaultView.Count < 2 Then
        '        Throw New AccesoBD.clsErrNeg("Debe ingresar mas de un producto. De lo contrario cambie el tipo de prestamo.")
        '    End If
        'End If

        mValidaCantProdUnico()
    End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("capa_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("capa_clie_id") = usrRazaCriador.ClieId
            .Item("capa_cria_id") = usrRazaCriador.Valor
            .Item("capa_soli_fecha") = txtSoliFecha.Text
            .Item("capa_tipo") = cmbTipo.Valor
            .Item("capa_audi_user") = Session("sUserId").ToString()
            .Item("capa_raza_id") = Convert.ToInt32(usrProducto.cmbProdRazaExt.Valor)
            .Item("capa_sra_nume") = Convert.ToInt32(usrProducto.txtSraNumeExt.Text)
            .Item("capa_sexo") = IIf(usrProducto.Sexo = "1", True, False)
            .Item("capa_rp") = usrProducto.txtRPExt.Text
            If txtAnalisis.Text <> "" Then .Item("capa_anal_nume") = txtAnalisis.Text
            .Item("capa_prdt_id") = usrProducto.Valor


        End With
    End Sub

    Private Sub mVerificarProducto()
        'verificar que el producto  seleccionado pueda ser prestado.
        'Dim lstrfiltro As String
        'If usrProd.Valor <> 0 Then   'And Not txtFechaDesde.Fecha Is DBNull.Value Then
        '    lstrfiltro = " @clie_id=" + clsSQLServer.gFormatArg(usrCliePresta.Valor, SqlDbType.Int)
        '    lstrfiltro = lstrfiltro + ",@cria_id=" + clsSQLServer.gFormatArg(usrCriaPresta.Valor, SqlDbType.Int)
        '    lstrfiltro = lstrfiltro + ",@prdt_id=" + clsSQLServer.gFormatArg(usrProd.Valor, SqlDbType.Int)
        '    lstrfiltro = lstrfiltro + ",@tipo='P'"

        '    'lstrfiltro = lstrfiltro + ",@fecha_ini=" + clsSQLServer.gFormatArg(txtFechaDesde.Text, SqlDbType.SmallDateTime)

        '    If SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "productos_prop_prestamos_embargos", "resul", lstrfiltro) = "S" Then
        '        Throw New AccesoBD.clsErrNeg("El producto No puede utilizarse para el prestamo.")
        '    End If
        '    'Else
        '    '  Throw New AccesoBD.clsErrNeg("Debe seleccionar un producto y la fecha de inicio del prestamo.")
        'End If
    End Sub
    Private Sub mProxiPareNume(ByVal pintTipo As Integer, ByRef pintProxiPareNume As Integer)
        'obtener el pr�ximo nro de pareja por detalle de cambio de padres
        Dim lstrfiltro As String
        If cmbTipo.Valor <> "P" Then   'Si no se trata de detalle de padres

            lstrfiltro = " @capd_capa_id=" + clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

            pintProxiPareNume = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "cambio_padres_proxi_pare_nume", "pare_nume", lstrfiltro)

            If pintProxiPareNume = 1 And hdnUltiPareNume.Text <> "" Then
                pintProxiPareNume = Convert.ToInt32(hdnUltiPareNume.Text) + 1
            End If

        Else
            pintProxiPareNume = 0
        End If
    End Sub
    'Seccion de detalle
    Private Sub mActualizarDeta()
        Try

            If (cmbTipo.Valor <> "P") Then
                mGuardarDatosPareja()
            Else
                mVerificarProducto()
                mGuardarDatosDeta()
            End If

            hdnTipoDeta.Text = IIf(cmbTipo.Valor = "P", 2, cmbTipoDeta.Valor)

            mLimpiarDeta()
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaPareja()
        Dim row1 As Data.DataRow = mdsDatos.Tables(mstrTablaDetalle).Select("capd_pare=" & hdnPareId.Text & "and capd_sexo=1")(0)
        Dim row2 As Data.DataRow = mdsDatos.Tables(mstrTablaDetalle).Select("capd_pare=" & hdnPareId.Text & "and capd_sexo=0")(0)

        row1.Delete()
        row2.Delete()
    End Sub

    Private Sub mBajaPadre()
        Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDetalle).Select("capd_id = " & hdnDetaId.Text)(0)
        row.Delete()
    End Sub

    Private Sub mBajaDeta()
        Try
            If cmbTipo.Valor <> "P" Then
                mBajaPareja()
            Else
                mBajaPadre()
            End If


            'Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDetalle).Select("capd_id=" & hdnDetaId.Text)(0)
            'row.Delete()

            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
            grdDetalle.DataBind()
            mLimpiarDeta()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosDeta()
        Dim ldrDeta As DataRow
        Dim lintProxiPareNume As Integer
        Dim lintSraNume As Integer
        Dim lintAnalNume As Integer

        mValidarDatosDeta()

        If hdnDetaId.Text = "" Then
            mProxiPareNume(cmbTipoDeta.Valor, lintProxiPareNume)
            hdnPareId.Text = lintProxiPareNume

            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
            ldrDeta.Item("capd_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "capd_id")
        Else
            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("capd_id=" & hdnDetaId.Text)(0)
        End If

        With ldrDeta
            .Item("_descrip") = IIf(usrProductoDeta.Sexo = "1", "Padre", "Madre")
            '.Item("capd_capa_id") = hdnId.Text
            .Item("capd_tipo") = IIf(cmbTipo.Valor = "P", 2, cmbTipoDeta.Valor)
            .Item("capd_prdt_id") = usrProductoDeta.Valor
            .Item("capd_sexo") = usrProductoDeta.Sexo
            If usrProductoDeta.txtSraNumeExt.Text <> "" Then .Item("capd_hba") = Convert.ToInt32(usrProductoDeta.txtSraNumeExt.Text)
            If usrProductoDeta.txtRPExt.Text <> "" Then .Item("capd_rp") = usrProductoDeta.txtRPExt.Text
            If txtAnalisiDeta.Text <> "" Then .Item("capd_anal_nume") = txtAnalisiDeta.Text
            .Item("capd_audi_user") = Session("sUserId").ToString()
            .Item("capd_pare") = hdnPareId.Text
        End With
        'ldrDeta.Item("pstd_prdt_id") = usrProd.Valor
        'ldrDeta.Item("_producto") = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "ProductoDesc", "_desc", usrProd.Valor.ToString)
        'ldrDeta.Item("_estado") = "Activo"
        If (hdnDetaId.Text = "") Then
            mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
        End If
    End Sub

    Private Sub mGuardarDatosPareja()
        Dim ldrDeta As DataRow

        mValidarDatosDeta()

        mGuardarDatosDeta()

        If hdnDetaId.Text = "" Then
            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
            ldrDeta.Item("capd_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "capd_id")
        Else
            ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("capd_id=" & hdnDetaId.Text)(0)
        End If

        With ldrDeta
            .Item("_descrip") = IIf(usrProdMadre.Sexo = "1", "Padre", "Madre")
            '.Item("capd_capa_id") = hdnId.Text
            .Item("capd_tipo") = IIf(cmbTipo.Valor = "P", 2, cmbTipoDeta.Valor)
            .Item("capd_prdt_id") = usrProdMadre.Valor
            .Item("capd_sexo") = usrProdMadre.Sexo
            If usrProdMadre.txtSraNumeExt.Text <> "" Then .Item("capd_hba") = Convert.ToInt32(usrProdMadre.txtSraNumeExt.Text)
            If usrProdMadre.txtRPExt.Text <> "" Then .Item("capd_rp") = usrProdMadre.txtRPExt.Text
            If txtAnalisisMadre.Text <> "" Then .Item("capd_anal_nume") = txtAnalisisMadre.Text
            .Item("capd_audi_user") = Session("sUserId").ToString()
            .Item("capd_pare") = hdnPareId.Text
        End With
        'ldrDeta.Item("pstd_prdt_id") = usrProd.Valor
        'ldrDeta.Item("_producto") = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "ProductoDesc", "_desc", usrProd.Valor.ToString)
        'ldrDeta.Item("_estado") = "Activo"
        If (hdnDetaId.Text = "") Then
            mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
        End If
        hdnUltiPareNume.Text = hdnPareId.Text
    End Sub

    Private Sub mValidarDatosDeta()
        If usrProductoDeta.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar un producto como Padre.")
        End If

        'If txtAnalisiDeta.Text = "" Then
        '    Throw New AccesoBD.clsErrNeg("Debe ingresar el detalle del an�lisis.")
        'End If

        'If cmbTipoDeta.SelectedValue <> 2 Then
        If cmbTipo.Valor <> "P" Then
            If usrProdMadre.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar un producto como Padre.")
            End If

            'If txtAnalisisMadre.Text = "" Then
            '    Throw New AccesoBD.clsErrNeg("Debe ingresar el detalle del an�lisis.")
            'End If
        End If


        'If mdsDatos.Tables(mstrTablaDetalle).Select("pstd_baja_fecha is null and pstd_prdt_id=" & usrProd.Valor.ToString & IIf(hdnDetaId.Text = "", "", " and pstd_id<>" & hdnDetaId.Text)).GetUpperBound(0) <> -1 Then
        '    Throw New AccesoBD.clsErrNeg("Producto ya ingresado.")
        'End If

        mValidarCantRegistros()
    End Sub

    Private Sub mValidarCantRegistros()

        If cmbTipo.Valor <> "P" And cmbTipoDeta.Valor = 0 Then

            If mdsDatos.Tables(mstrTablaDetalle).Rows.Count = 2 And (hdnDetaId.Text = "") Then 'And mdsDatos.Tables(mstrTablaDetalle).Select("capd_baja_fecha is null").Length = mdsDatos.Tables(mstrTablaDetalle).Rows.Count Then
                Throw New AccesoBD.clsErrNeg("El tipo de tr�mite no permite el ingreso de mas de una pareja.")
            End If
        End If
    End Sub
    Private Sub mValidaCantProdUnico()

        'If cmbTipo.Valor = mProdUno Then
        '    mdsDatos.Tables(mstrTablaDetalle).DefaultView.RowFilter = "pstd_baja_fecha is null"
        '    If mdsDatos.Tables(mstrTablaDetalle).DefaultView.Count > 1 Then
        '        Throw New AccesoBD.clsErrNeg("Solo puede ingresar un producto. De lo contrario cambie el tipo de prestamo.")
        '    End If
        'End If


    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panDato.Visible = True
            panBotones.Visible = True
            btnAgre.Visible = False

            panFiltro.Visible = False
            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            panDetalle.Visible = False
            lnkDetalle.Font.Bold = False
            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 2
                    panDetalle.Visible = True
                    lnkDetalle.Font.Bold = True

                    mLimpiarDeta()
                    mInicializarDeta()
                    'usrProd.cmbProdRazaExt.Valor = usrCriaPresta.cmbCriaRazaExt.Valor
                    'usrProd.CriaId = hdnCriaId.Text

            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub
    'Botones Generales
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    'Botones Detalle
    Private Sub btnAltaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
        mActualizarDeta()
    End Sub
    Private Sub btnBajaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
        mBajaDeta()
    End Sub
    Private Sub btnModiDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
        mActualizarDeta()
    End Sub
    Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
        mLimpiarDeta()
    End Sub
    'Solapas
    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDetalle.Click
        mShowTabs(2)
    End Sub

    Private Sub cmbTipo_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        mDetalle()
        If cmbTipo.Valor = mProdTodos Then
            mCrearDataSet("")
        End If
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String
            Dim lstrRpt As String
            Dim lintSoliFecha As Integer

            lstrRptName = "ListadoCambioPadres"


            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            If txtSoliFecha.Text = "" Then
                lintSoliFecha = 0
            Else
                lintSoliFecha = CType(clsFormatear.gFormatFechaString(txtSoliFecha.Text, "Int32"), Integer)
            End If

            '            lstrRpt += "&capa_soli_fecha=" + lintSoliFecha.ToString()
            lstrRpt += "&capa_clie_id=" + IIf(usrClienteFil.Valor.ToString = "", "0", usrClienteFil.Valor.ToString)
            lstrRpt += "&capa_cria_id=" + IIf(usrCriadorFil.Valor.ToString = "", "0", usrCriadorFil.Valor.ToString)
            lstrRpt += "&capa_prdt_id=" + IIf(usrProdFil.Valor.ToString = "", "0", usrProdFil.Valor.ToString)

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub



    Private Sub cmbTipoDeta_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbTipoDeta.SelectedIndexChanged


        Try
            mLimpiarDeta()
            
            Select Case cmbTipoDeta.Valor
                Case "0"
                    mMostrarPanelMadre(True)
                Case "1"
                    mMostrarPanelMadre(True)
                    'Case "2"
                    '    mMostrarPanelMadre(False)
            End Select


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region
End Class

End Namespace
