Namespace SRA

Partial Class ChequesVenta
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    ' Protected WithEvents cmbEmct As NixorControls.ComboBox


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Cheques
   Private mstrTablaChequesMovim As String = SRA_Neg.Constantes.gTab_ChequesMovim
   Private mstrTablaAsiento As String
   Private mstrTablaAsientoAlta As String = "venta_cheques_asiento"
   Private mstrTablaAsientoBaja As String = "venta_cheques_asiento_baja"

   Private mstrConn As String
   Private mstrParaPageSize As Integer

   Private Enum Columnas As Integer
      Id = 0
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      If hdnId.Text <> "" Then
         If btnBaja.Enabled = False Then
            mstrTablaAsiento = mstrTablaAsientoAlta
         Else
            mstrTablaAsiento = mstrTablaAsientoBaja
         End If
      End If
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            hdnEmctId.Text = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
            mCargarCombos()
            mMostrarPanel(False)
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      'clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbEmct, "@ori_emct_id=" + hdnEmctId.Text)
      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "")
      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbGstoMone, "")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancFil, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBanc, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "cheques_tipos", cmbChti, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCcos, "S", "@fina=1")
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooRech As Boolean)
      btnModi.Enabled = Not (pbooRech)
      btnBaja.Enabled = pbooRech
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim lbooRech As Boolean
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)

      With ldsEsta.Tables(0).Rows(0)
         hdnId.Text = .Item("cheq_id").ToString()
         txtImpo.Valor = .Item("cheq_impo")
         cmbMone.Valor = .Item("cheq_mone_id")
         cmbGstoMone.Valor = .Item("cheq_mone_id")
         ' cmbEmct.Valor = .Item("cheq_emct_id")
         cmbBanc.Valor = .Item("cheq_banc_id")

         usrClie.Valor = .Item("_comp_clie_id")
         txtNume.Valor = .Item("cheq_nume")
         txtReceFecha.Fecha = .Item("cheq_rece_fecha")
            '15/09/2010     txtTeorDepoFecha.Fecha = .Item("cheq_teor_depo_fecha")
            txtCheqFecha.Fecha = .Item("cheq_fecha")
            cmbChti.Valor = .Item("cheq_chti_id")

                cmbCcos.Valor = .Item("cheq_ccos_id").ToString
                txtGstoImpo.Valor = .Item("cheq_gsto_impo").ToString
                txtImpoPeso.Valor = .Item("cheq_venta_impo_pesos").ToString
                txtImpoMone.Valor = .Item("cheq_venta_impo_ori").ToString
         If .IsNull("cheq_dispo") Then
            chkDisp.Checked = True
         Else
            chkDisp.Checked = IIf(.Item("cheq_dispo") = 0, False, True)
         End If
                txtCoti.Valor = .Item("cheq_gsto_coti").ToString

         If txtCoti.Text = "" Then
            txtCoti.Valor = SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, Date.Today, 2)
         End If

         If Not .IsNull("cheq_vta_fecha") Then
            lblBaja.Text = "Cheque vendido en fecha: " & CDate(.Item("cheq_audi_fecha")).ToString("dd/MM/yyyy HH:mm")
            lbooRech = True
            cmbCcos.Enabled = False
            txtGstoImpo.Enabled = False
            txtImpoPeso.Enabled = False
            txtImpoMone.Enabled = False
            chkDisp.Enabled = False
            txtCoti.Enabled = False
         Else
            lblBaja.Text = ""
            cmbCcos.Enabled = True
            txtGstoImpo.Enabled = True
            txtImpoPeso.Enabled = True
            txtImpoMone.Enabled = True
            chkDisp.Enabled = True
            txtCoti.Enabled = True
         End If

         If cmbMone.Valor.ToString = SRA_Neg.Comprobantes.gMonedaPesos(mstrConn) Then
            lblImpoMone.Visible = False
            txtImpoMone.Visible = False
         Else
            lblImpoMone.Visible = True
            txtImpoMone.Visible = True
            lblImpoMone.Text = "Importe " & cmbMone.SelectedItem.Text.Trim() & ":"
         End If

      End With

      mSetearEditor("", lbooRech)
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""

      txtImpo.Text = ""
      cmbBanc.Limpiar()

      txtNume.Text = ""
      txtReceFecha.Text = ""
        '15/09/2010    txtTeorDepoFecha.Text = ""
        txtCheqFecha.Text = ""
      cmbChti.Limpiar()
      cmbMone.Limpiar()
      'cmbEmct.Limpiar()

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mActualizar(ByVal pbooEsAlta As Boolean)
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos(pbooEsAlta)

         Dim lobjNeg As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjNeg.Modi()

         chkVend.Checked = False
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos(ByVal pbooEsAlta As Boolean) As DataSet
      Dim ldsDatos As DataSet
      Dim ldrComp, ldrCode, ldrCoco, ldrCovt, ldrAsie As DataRow
      Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
      Dim lstrHost, lstrCemiNume, lstrEmctId As String
      Dim lDrCheq As DataRow
      Dim lDrMovim As DataRow

      mValidarDatos()

      ldsDatos = CrearDataSet(hdnId.Text)

      ldsDatos.Tables.Add(mstrTablaAsiento)
      With ldsDatos.Tables(mstrTablaAsiento)
         .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_cheq_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
      End With

      'ASIENTOS
      ldrAsie = ldsDatos.Tables(mstrTablaAsiento).NewRow
      With ldrAsie
         .Item("proc_id") = clsSQLServer.gObtenerId(.Table, "proc_id")
         .Item("proc_cheq_id") = hdnId.Text
         .Table.Rows.Add(ldrAsie)
      End With

      lDrCheq = ldsDatos.Tables(mstrTabla).Rows(0)

      'guardo los datos anteriores
      lDrMovim = ldsDatos.Tables(mstrTablaChequesMovim).NewRow
      With lDrMovim
         .Item("mchq_id") = -1
         .Item("mchq_cheq_id") = lDrCheq.Item("cheq_id")
         .Item("mchq_rece_fecha") = lDrCheq.Item("cheq_rece_fecha")
            .Item("mchq_teor_depo_fecha") = lDrCheq.Item("cheq_teor_depo_fecha")
            .Item("mchq_clea") = lDrCheq.Item("cheq_clea_id")
         .Item("mchq_emct_id") = lDrCheq.Item("cheq_emct_id")
         .Table.Rows.Add(lDrMovim)
      End With

      'actualizo con los datos nuevos
      With lDrCheq
         If pbooEsAlta Then
            .Item("cheq_ccos_id") = cmbCcos.Valor
            .Item("cheq_gsto_impo") = txtGstoImpo.Valor
            .Item("cheq_venta_impo_pesos") = txtImpoPeso.Valor
            .Item("cheq_venta_impo_ori") = txtImpoMone.Valor
            .Item("cheq_dispo") = chkDisp.Checked
            .Item("cheq_gsto_coti") = txtCoti.Valor
            .Item("cheq_vta_fecha") = Today
            .Item("cheq_real_depo_fecha") = Today
         Else
            .Item("cheq_ccos_id") = DBNull.Value
            .Item("cheq_gsto_impo") = DBNull.Value
            .Item("cheq_venta_impo_pesos") = DBNull.Value
            .Item("cheq_venta_impo_ori") = DBNull.Value
            .Item("cheq_gsto_coti") = DBNull.Value
            .Item("cheq_vta_fecha") = DBNull.Value
            .Item("cheq_real_depo_fecha") = DBNull.Value
         End If
      End With

      Return ldsDatos
   End Function

   Private Function CrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      ldsEsta.Tables(0).TableName = mstrTabla
      ldsEsta.Tables(1).TableName = mstrTablaChequesMovim
      For Each ldrDatos As DataRow In ldsEsta.Tables(mstrTablaChequesMovim).Select
         ldsEsta.Tables(mstrTablaChequesMovim).Rows.Remove(ldrDatos)
      Next
      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec cheques_busq")
         lstrCmd.Append(" @cheq_banc_id =" + cmbBancFil.Valor.ToString)
         lstrCmd.Append(",@cheq_nume =" + clsSQLServer.gFormatArg(txtNumeFil.Valor.ToString, SqlDbType.VarChar))
         lstrCmd.Append(",@clie_id =" + usrClieFil.Valor.ToString)
         lstrCmd.Append(",@cheq_plaza=0") 'extranjera
         lstrCmd.Append(",@depositado=0")
         lstrCmd.Append(",@rechazado=0")
         lstrCmd.Append(",@vendido=" + Math.Abs(CInt(chkVend.Checked)).ToString)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos"
   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      Try
          If (txtGstoImpo.Valor <> 0 And cmbCcos.Valor.ToString() = "") Or (txtGstoImpo.Valor = 0 And cmbCcos.Valor.ToString() <> "") Then
              Throw New AccesoBD.clsErrNeg("Debe indicar el importe de gastos y el centro de costo.")
          Else
              mActualizar(True)
          End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mActualizar(False)
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mCerrar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class
End Namespace
