Namespace SRA

Partial Class Alumnos_Pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      Chk = 0
      Id = 1
      Lega = 2
      Apel = 3
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If Not Page.IsPostBack Then
            Session(mSess(0)) = Nothing
            hdnInse.Text = Request("inse")
            mConsultar()
            mChequearTodos()
         Else
            mdsDatos = Session(mSess(0))
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function

#End Region

#Region "Operaciones sobre el DataGrid"
   Private Sub mGuardarTodosIds()
      Dim lstrId As New System.Text.StringBuilder
      mLimpiarSession()

      For i As Integer = 0 To grdConsulta.PageCount - 1
         lstrId.Length = 0
         grdConsulta.CurrentPageIndex = i
         grdConsulta.DataBind()

         Session("mulPag" & i.ToString) = mGuardarDatos(True)
      Next

      grdConsulta.CurrentPageIndex = 0
      grdConsulta.DataBind()
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         Dim lstrId As String

         lstrId = mGuardarDatos()

         Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) = lstrId.ToString

         grdConsulta.CurrentPageIndex = E.NewPageIndex

         mConsultar()

         mChequearGrilla()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", E.Item.Cells(Columnas.Id).Text))
      lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub

   Public Sub mConsultar()
      Dim lstrCmd As String = ""
      Dim ds As DataSet

      lstrCmd = "exec alumnos_mesas_busq"
      lstrCmd = lstrCmd & " @inse_id=" & clsSQLServer.gFormatArg(Request("inse"), SqlDbType.Int)
      lstrCmd = lstrCmd & ",@comi_id=" & clsSQLServer.gFormatArg(Request("comi"), SqlDbType.Int)
      lstrCmd = lstrCmd & ",@mate_id=" & clsSQLServer.gFormatArg(Request("mate"), SqlDbType.Int)

      ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

      Session(mSess(0)) = ds

      grdConsulta.DataSource = ds
      grdConsulta.DataBind()
      ds.Dispose()

      Session("mulPaginas") = grdConsulta.PageCount
   End Sub
#End Region

   Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
      Dim lstrId As New StringBuilder
      Dim lstrClieAnt, lstrBuqeAnt As String
      Try
         lstrId.Append(mGuardarDatos)

         For i As Integer = 0 To Session("mulPaginas")
            If Not Session("mulPag" & i.ToString) Is Nothing Then
               Dim lstrIdsSess As String = Session("mulPag" & i.ToString)
               If i <> grdConsulta.CurrentPageIndex And lstrIdsSess <> "" Then
                  If lstrId.Length > 0 Then lstrId.Append(Chr(5))
                  lstrId.Append(lstrIdsSess)
               End If
               Session("mulPag" & i.ToString) = Nothing
            End If
         Next

         grdConsulta.DataSource = Nothing
         grdConsulta.DataBind()

         Dim lsbMsg As New StringBuilder
         lsbMsg.Append("<SCRIPT language='javascript'>")
         lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", lstrId.ToString.Replace("'", "´")))
         lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
         lsbMsg.Append("window.close();")
         lsbMsg.Append("</SCRIPT>")
         Response.Write(lsbMsg.ToString)

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
      Response.Write("<Script>window.close();</script>")
   End Sub

   Private Function mGuardarDatos(Optional ByVal pbooTodos As Boolean = False) As String
      Dim lstrId As New StringBuilder

      For Each oDataItem As DataGridItem In grdConsulta.Items
            If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Or pbooTodos Then
                If lstrId.Length > 0 Then lstrId.Append(Chr(5))
                lstrId.Append(oDataItem.Cells(Columnas.Id).Text)
                lstrId.Append(Chr(6))
                lstrId.Append(oDataItem.Cells(Columnas.Lega).Text)
                lstrId.Append(Chr(6))
                lstrId.Append(oDataItem.Cells(Columnas.Apel).Text)
            End If
      Next

      Return (lstrId.ToString)
   End Function

   Private Sub mChequearGrilla()
      Dim lstrIdSess As String
      If Not (Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) Is Nothing) Then
         lstrIdSess = Session("mulPag" & grdConsulta.CurrentPageIndex.ToString)
      End If

      For Each oDataItem As DataGridItem In grdConsulta.Items
         CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = (Chr(5) & lstrIdSess).IndexOf(Chr(5) & oDataItem.Cells(Columnas.Id).Text & Chr(6)) <> -1
      Next
   End Sub

   Private Sub btnTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTodos.Click
      mConsultar()

      mChequearTodos()
   End Sub

   Private Sub btnNinguno_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNinguno.Click
      mLimpiarSession()
      mChequearGrilla()
   End Sub

   Private Sub mLimpiarSession()
      Dim k As Integer

      While Not Session("mulPag" & k.ToString) Is Nothing
         Session("mulPag" & k.ToString) = Nothing
         k += 1
      End While
   End Sub

   Private Sub mChequearTodos()
      mGuardarTodosIds()
      mChequearGrilla()
   End Sub
End Class

End Namespace
