<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.FactMasivaProf" CodeFile="FactMasivaProf.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Facturaci�n masiva de proformas</title>
		<meta content="False" name="vs_showGrid">
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/valDecimal.js"></script>
		<SCRIPT language="javascript">
		function expandir()
		{
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function chkSelCheck(pChk)
            {
                var lintFil= eval(pChk.id.replace("grdProfo__ctl","").replace("_chkSel",""));
                var lboolChek = pChk.checked;
                if (lboolChek)
                {
					dValor = eval(document.all(pChk.id.replace("chkSel", "divImporte")).innerText.replace(".",""))
					if (document.all("txtTotalSel").value != "")
						dTotalSel = eval(document.all("txtTotalSel").value);
					else	
						dTotalSel = 0;
					
					dTotalSel = dTotalSel + (dValor/100);
					document.all("txtTotalSel").value = gRedondear(dTotalSel,2);
                }
                else
                {
					dValor = eval(document.all(pChk.id.replace("chkSel", "divImporte")).innerText.replace(".",""))
					if (document.all("txtTotalSel").value != "")
						dTotalSel = eval(document.all("txtTotalSel").value);
					else	
						dTotalSel = 0;					
					dTotalSel = dTotalSel - (dValor/100);
					document.all("txtTotalSel").value = gRedondear(dTotalSel,2);
                }
            }

		</SCRIPT>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 10px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Facturaci�n masiva de Proformas</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="100%" Visible="True">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif"
																	BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
																	BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="10"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox id="cmbActi" class="combo" runat="server" Width="250px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR vAlign="top">
																		<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																			<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrClieFil" runat="server" width="100%" AceptaNull="False" FilCUIT="True" Tabla="Clientes"
																				FilLegaNume="True" FilSociNume="True" MuestraDesc="False" FilDocuNume="True"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
													<A id="editar" name="editar"></A>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD align="center" colSpan="3">
									<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid" BorderWidth="1px"
											Visible="False">
											<TABLE width="100%">
												<TR>
													<TD>
														<P align="right">
															<TABLE style="WIDTH: 100%" id="Table2" class="FdoFld" border="0" cellPadding="0" align="left">
																<TR>
																	<TD>
																		<P></P>
																	</TD>
																	<TD height="5">
																		<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
																	<TD vAlign="top" align="right">&nbsp;
																		<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" CausesValidation="False"
																			ToolTip="Cerrar"></asp:ImageButton></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 100%" colSpan="3">
																		<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																			<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																				<TR>
																					<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																				</TR>
																				<TR>
																					<TD vAlign="top" colSpan="2" align="center">
																						<asp:datagrid id="grdProfo" runat="server" width="100%" Visible="true" BorderWidth="1px" BorderStyle="None"
																							HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False">
																							<FooterStyle CssClass="footer"></FooterStyle>
																							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																							<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																							<Columns>
																								<asp:TemplateColumn HeaderText="Incl.">
																									<HeaderStyle Width="3%"></HeaderStyle>
																									<ItemTemplate>
																										<asp:CheckBox ID="chkSel" Runat="server" onclick="chkSelCheck(this);"></asp:CheckBox>
																										<DIV runat="server" id="divImporte" style="DISPLAY: none"><%#(DataBinder.Eval(Container, "DataItem.prfr_neto"))%></DIV>
																									</ItemTemplate>
																								</asp:TemplateColumn>
																								<asp:BoundColumn Visible="False" DataField="prfr_id"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_nume" HeaderText="N&#250;mero"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_ingr_fecha" HeaderText="Fecha Ing." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_fecha" HeaderText="Fecha Valor" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_clie_apel" HeaderText="Cliente"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_tipo" HeaderText="Tipo"></asp:BoundColumn>
																								<asp:BoundColumn DataField="acti_desc" HeaderText="Actividad"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_neto" HeaderText="Importe" HeaderStyle-HorizontalAlign="Right">
																									<ItemStyle HorizontalAlign="Right"></ItemStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																							</Columns>
																							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																						</asp:datagrid></TD>
																				</TR>
																				<TR height="30">
																					<TD align="left">
																						<asp:button id="btnTodos" runat="server" cssclass="boton" Width="80px" Text="Todos"></asp:button>&nbsp;
																						<asp:button id="btnNinguno" runat="server" cssclass="boton" Width="80px" Text="Ninguno"></asp:button></TD>
																					<TD align="right">
																						<asp:Label id="lblTotalSel" runat="server" cssclass="titulo">Total Seleccionado:</asp:Label>&nbsp;
																						<CC1:NUMBERBOX id="txtTotalSel" runat="server" cssclass="cuadrotextodeshab" Width="65px" Enabled="False"></CC1:NUMBERBOX>&nbsp;</TD>
																				</TR>
																				<TR height="30">
																					<TD colSpan="2" align="center">
																						<asp:button id="btnGenerar" runat="server" cssclass="boton" Width="157px" Text="Generar Facturas"></asp:button></TD>
																				</TR>
																			</TABLE>
																		</asp:panel></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 100%" colSpan="3" align="center">
																		<asp:panel id="PanFact" runat="server" cssclass="titulo" Width="85%" BorderWidth="1px" BorderStyle="Solid"
																			visible="false">
																			<TABLE style="WIDTH: 100%" id="TableFact" border="0" cellPadding="0" align="left">
																				<TR>
																					<TD vAlign="top" align="right">
																						<asp:ImageButton id="imgCloseFact" runat="server" ImageUrl="images\Close.bmp" CausesValidation="False"
																							ToolTip="Cerrar"></asp:ImageButton></TD>
																				</TR>
																				<TR>
																					<TD>
																						<asp:Label id="lblFact" runat="server" cssclass="titulo">Facturas:</asp:Label>&nbsp;</TD>
																				</TR>
																				<TR>
																					<TD vAlign="top" colSpan="2" align="center">
																						<asp:datagrid id="grdFact" runat="server" width="95%" BorderWidth="1px" BorderStyle="None" HorizontalAlign="Center"
																							CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" AllowPaging="False">
																							<FooterStyle CssClass="footer"></FooterStyle>
																							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																							<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																							<Columns>
																								<asp:BoundColumn DataField="comp_clie_id" HeaderText="Cliente"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_comp_desc" HeaderText="Apellido / Raz�n Social"></asp:BoundColumn>
																								<asp:BoundColumn DataField="comp_neto" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																									ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																							</Columns>
																							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																						</asp:datagrid></TD>
																				</TR>
																				<TR>
																					<TD>
																						<asp:Label id="lblProfErr" runat="server" cssclass="titulo">Proformas no facturadas:</asp:Label>&nbsp;<A id="editar2" name="editar2"></A></TD>
																				</TR>
																				<TR>
																					<TD vAlign="top" colSpan="2" align="center">
																						<asp:datagrid id="grdProfErr" runat="server" width="95%" Visible="true" BorderWidth="1px" BorderStyle="None"
																							HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False">
																							<FooterStyle CssClass="footer"></FooterStyle>
																							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																							<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																							<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																							<Columns>
																								<asp:BoundColumn Visible="False" DataField="prfr_id"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_nume" HeaderText="N&#250;mero"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_ingr_fecha" HeaderText="Fecha Ing." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_clie_apel" HeaderText="Cliente"></asp:BoundColumn>
																								<asp:BoundColumn DataField="_tipo" HeaderText="Tipo"></asp:BoundColumn>
																								<asp:BoundColumn DataField="acti_desc" HeaderText="Actividad"></asp:BoundColumn>
																								<asp:BoundColumn DataField="prfr_neto" HeaderText="Importe" HeaderStyle-HorizontalAlign="Right">
																									<ItemStyle HorizontalAlign="Right"></ItemStyle>
																								</asp:BoundColumn>
																								<asp:BoundColumn DataField="desc_error" HeaderText="Error"></asp:BoundColumn>
																							</Columns>
																							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																						</asp:datagrid></TD>
																				</TR>
																				<TR height="30">
																					<TD align="center">
																						<asp:button id="btnAlta" runat="server" cssclass="boton" Width="157px" Text="Grabar Facturas"></asp:button></TD>
																				</TR>
																			</TABLE>
																		</asp:panel></TD>
																</TR>
															</TABLE>
														</P>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnIds" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar2"]!= null)
			document.location='#editar2';
		</SCRIPT>
	</BODY>
</HTML>
