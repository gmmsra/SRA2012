Namespace SRA

Partial Class EnvioMails
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

    Protected WithEvents btnMailMode As System.Web.UI.HtmlControls.HtmlImage


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mInicializar()
            mEstablecerPerfil()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdCons.PageSize = Convert.ToInt32(mstrParaPageSize)
        clsWeb.gCargarCombo(mstrConn, "mails_modelos_cargar 'S'", cmbMode, "id", "descrip", "S")
        btnEnvi.Attributes.Add("onclick", "if(!confirm('Confirma el envio de los mails a los socios seleccionados?')) return false;")
        cmbMode.Attributes.Add("onchange", "mModelos(this);return(false);")
        usrSocFil.MostrarOrden = False
        usrSocFil.MostrarDatosOpcionales = False
   End Sub

   Private Sub mLimpiarFiltros()
      usrSocFil.Limpiar()
      panGrab.Visible = False
      grdCons.Visible = False
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCons.EditItemIndex = -1
         If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
            grdCons.CurrentPageIndex = 0
         Else
            grdCons.CurrentPageIndex = E.NewPageIndex
         End If
            mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Sub mConsultar(Optional ByVal pstrTop As String = "")
        Try

            hdnRptId.Text = usrSocFil.ObtenerIdFiltro.ToString()

            mstrCmd = "exec socios_general_mails_consul "
            mstrCmd += " @rptf_id=" + hdnRptId.Text
            If pstrTop = "T" Then
                mstrCmd += ",@rptf_top=1"
            End If
            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd, 3000)

            For Each dc As DataColumn In ds.Tables(0).Columns
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                dgCol.DataField = dc.ColumnName
                dgCol.HeaderText = dc.ColumnName
                If dc.Ordinal = 0 Then
                    dgCol.Visible = False
                End If
                If dc.ColumnName = "cate_id" Or dc.ColumnName = "inst_id" Then
                    dgCol.Visible = False
                End If
                grdCons.Columns.Add(dgCol)
            Next
            grdCons.DataSource = ds
            grdCons.DataBind()
            grdCons.Visible = True
            panGrab.Visible = True
            Session("ds") = DirectCast(grdCons.DataSource, DataSet)
            btnEnvi.Enabled = (grdCons.Items.Count > 0)
            ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnCons_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCons.Click
        grdCons.CurrentPageIndex = 0
        mConsultar("T")
    End Sub

    Private Sub btnEnvi_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEnvi.Click

        Try
            Dim lstrMail As String
            Dim lstrInst As String
            Dim lstrCate As String
            Dim lstrAsun As String
            Dim lstrMens As String
            Dim lstrMailMode As String = System.Configuration.ConfigurationSettings.AppSettings("conMailMode").ToString()

            lstrMailMode = lstrMailMode

            If txtAsun.Text = "" Then
                lstrAsun = clsSQLServer.gCampoValorConsul(mstrConn, "mails_modelos_consul @mamo_id=" & cmbMode.Valor.ToString, "mamo_asun")
                txtAsun.Text = lstrAsun
                If lstrAsun = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el asunto del mensaje.")
                End If
            End If

            'mstrCmd = "exec socios_general_mails_consul "
            'mstrCmd += " @rptf_id=" + usrSocFil.ObtenerIdFiltro.ToString
            'Dim ds As New DataSet
            'ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            For Each odrDeta As DataRow In Session("ds").Tables(1).Select
                lstrCate = odrDeta.Item("cate_id")
                If odrDeta.IsNull("inst_id") Then
                    lstrInst = ""
                Else
                    lstrInst = odrDeta.Item("inst_id")
                End If
                mstrCmd = "mails_mode_catego_mensaje_consul @mmca_cate_id=" & lstrCate & ",@mmca_mamo_id=" & cmbMode.Valor.ToString
                If lstrInst <> "" Then
                    mstrCmd = mstrCmd & ",@mmca_inst_id=" & lstrInst
                End If
                lstrMens = clsSQLServer.gCampoValorConsul(mstrConn, mstrCmd, "mmca_mens")
                lstrMail = mObtenerMails(Session("ds"), lstrCate, lstrInst)
                If lstrMail <> "" And lstrMens <> "" Then
                    clsMail.gEnviarMail(Trim(lstrMail), txtAsun.Text, lstrMens, lstrMailMode)
                End If
            Next

            Throw New AccesoBD.clsErrNeg("El proceso de env�o de mails ha finalizado correctamente.")

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            mConsultar()
        End Try
    End Sub

    Private Function mObtenerMails(ByVal ds As DataSet, ByVal pstrCate As String, ByVal pstrInst As String) As String
        Dim lstrMail As String
        Try
            For Each odrClag As DataRow In ds.Tables(0).Select("cate_id=" & pstrCate)
                If lstrMail <> "" Then
                    lstrMail = lstrMail & ";"
                End If
                lstrMail = lstrMail & odrClag.Item("mails")
            Next
            ds.Dispose()
            Return (lstrMail)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Function

End Class
End Namespace
