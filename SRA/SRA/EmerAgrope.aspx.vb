Namespace SRA

Partial Class EmerAgrope
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_EmerAgrope

    Private mstrConn As String
    Private mstrCmd As String
    Private mstrParaPageSize As Integer
    Private mstrSociId As String

    Private mdsDatos As DataSet

    Private Enum Columnas As Integer
        Edit = 0
        Id = 1
    End Enum
#End Region

#Region "Inicialización de Variables"
    Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        If Request.QueryString("soci_id") Is Nothing Then
            mstrSociId = ""
        Else
            mstrSociId = Request.QueryString("soci_id")
        End If
    End Sub
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()

            If (Not Page.IsPostBack) Then
                    txtFecha.Fecha = Today
                    txtFecha.Enabled = False

                Session(mstrTabla) = Nothing
                mdsDatos = Nothing

                If mstrSociId <> "" Then
                    panFiltro.Visible = False
                    btnAgre.Visible = False
                    btnAlta.Visible = False
                    btnModi.Visible = False
                    btnBaja.Visible = False
                    btnLimp.Visible = False
                End If
                mSetearMaxLength()
                mCargarCombo()
                mConsultar()
                btnDeuda.Attributes.Add("onclick", "btnDeuda_Click();return false;")

            Else
                mdsDatos = Session(mstrTabla)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


#End Region

#Region "Seteo de Controles"
    Private Sub mCargarDatos(ByVal pstrId As String)
        Dim lbooActivo As Boolean
        Dim lstrTipo As String
        Dim lstrDesde As String
        Dim lstrHasta As String
        Dim lintHastaPeri As Integer
        Dim lintHastaAnio As Integer
        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        With ldsEsta.Tables(0).Rows(0)
            hdnId.Text = .Item("emer_id").ToString()
            hdnSociId.Text = .Item("emer_soci_id")
            UsrSoci.Valor = .Item("emer_soci_id")
            txtFecha.Fecha = .Item("emer_fecha")
            If .IsNull("emer_perio_desc") Then
                lblBimDesc.Text = ""
            Else
                lblBimDesc.Text = .Item("emer_perio_desc")
            End If
            lblBimDescConf.Text = lblBimDesc.Text
            txtDesdeFecha.Fecha = .Item("emer_desde_fecha")
            txtHastaFecha.Fecha = .Item("emer_hasta_fecha")

            txtObse.Valor = .Item("emer_obse")
            chkConf.Checked = .Item("emer_conf")

            txtComp.Valor = .Item("_comp")
            txtImpo.Valor = .Item("emer_impo")

            hdnDeuda.Text = .Item("comprobantes")

            lbooActivo = Not .Item("emer_conf")
        End With

        lblTitu.Text = "Registro Seleccionado: " + UsrSoci.Codi + " - " + UsrSoci.Apel
        'btnDeuda.Enabled = True
        mCargarDeuda()

            If (txtDeudaAnterior.Text <> "0.00" And txtDeudaAnterior.Text <> "0,00") Then
                lblBimAnteDesc.Text = clsSQLServer.gCampoValorConsul(mstrConn, "deuda_social_descrip_consul " & UsrSoci.Valor.ToString, "deuda_descrip")
                If lblBimAnteDesc.Text.Length > 0 Then
                    lstrTipo = lblBimAnteDesc.Text.Trim.Substring(0, 3)
                Else
                    lstrTipo = String.Empty
                End If
                If lblBimAnteDesc.Text.Length > 0 Then
                    lstrDesde = lblBimAnteDesc.Text.Substring(0, lblBimAnteDesc.Text.IndexOf(" a")).Trim()
                End If
                If lstrTipo = "SEM" Then
                    lstrHasta = lblBimDesc.Text.Substring(0, lblBimDesc.Text.IndexOf(" a") + 1).Trim().Replace(".", " ").Replace(lstrTipo, "").Trim()
                    If lstrHasta = "" Then
                        lstrHasta = lblBimDesc.Text.Replace("Anualidad", "")
                    End If
                Else
                    If lblBimDesc.Text.Length > 0 Then
                        lstrHasta = lblBimDesc.Text.Substring(0, lblBimDesc.Text.IndexOf("a")).Trim().Replace(".", " ").Replace(lstrTipo, "").Trim()
                    End If
                End If
                lstrHasta = lstrHasta.Replace("Anualidad", "").Trim()
                If lstrTipo = "SEM" Then
                    lintHastaPeri = 1
                    lintHastaAnio = Convert.ToInt32(lstrHasta) - 1
                    lintHastaAnio = Convert.ToInt32(lintHastaAnio.ToString().Substring(2, 2))
                Else
                    lintHastaPeri = lstrHasta.Substring(lstrHasta.IndexOf("/") + 1)
                    lintHastaAnio = lstrHasta.Substring(0, lstrHasta.IndexOf("/"))
                    lintHastaPeri = lintHastaPeri - 1
                    If lintHastaPeri = 0 Then
                        If lstrTipo = "BIM" Then
                            lintHastaPeri = 6
                        Else
                            lintHastaPeri = 2
                        End If
                        lintHastaAnio = lintHastaAnio - 1
                    End If
                End If
                lblBimAnteDesc.Text = lstrDesde & " al " & lstrTipo & " " & IIf(lintHastaAnio < 9, "0", "") & lintHastaAnio.ToString() & "/" & lintHastaPeri.ToString()
                If lblBimDesc.Text <> "" Then
                    If lblBimDesc.Text.Trim.Substring(0, 3) = "SEM" Then
                        lblBimDesc.Text = mAnualidad(lblBimDesc.Text)
                    End If
                End If
                If lblBimAnteDesc.Text <> "" And lblBimAnteDesc.Text.Trim.Substring(0, 3) = "SEM" Then
                    lblBimAnteDesc.Text = mAnualidad(lblBimAnteDesc.Text)
                End If
                If lblBimAnteDesc.Text <> "" And lblBimDesc.Text.Substring(lblBimDesc.Text.LastIndexOf(" ") + 1) = lblBimAnteDesc.Text.Substring(lblBimAnteDesc.Text.IndexOf(" al ") + 4) Then
                    lblBimAnteDesc.Text = lblBimAnteDesc.Text.Substring(0, lblBimAnteDesc.Text.IndexOf(" al "))
                End If
            Else
                lblBimAnteDesc.Text = ""
                If lblBimDesc.Text <> "" Then
                    If lblBimDesc.Text.Trim.Substring(0, 3) = "SEM" Then
                        lblBimDesc.Text = mAnualidad(lblBimDesc.Text)
                    End If
                End If
            End If

            mSetearEditor("", False, lbooActivo)
            mMostrarPanel(True)
        End Sub

    Private Sub mCargarDeuda()
        Dim lstrCmd As New StringBuilder
        Dim ds As DataSet
        Dim ldecImpo As Decimal = 0
        Dim lstrTipo As String = ""
        Dim lstrDesde As String
        Dim lintDesdeMes As Integer
        Dim lstrDeuda As String = "," & hdnDeuda.Text & ","

        lstrCmd.Append("exec deuda_agropecuaria_consul")
        lstrCmd.Append(" @soci_id=")
        lstrCmd.Append(UsrSoci.Valor)
        lstrCmd.Append(", @fecha_desde=")
        'lstrCmd.Append(clsSQLServer.gFormatArg(txtDesdeFecha.Text, SqlDbType.SmallDateTime))
        If lblBimDesc.Text <> "" Then
            lstrTipo = lblBimDesc.Text.Substring(0, 3)
            If lstrTipo.ToUpper() = "ANU" Then lstrTipo = "Anualidad"
            If lblBimDesc.Text.IndexOf(" a") <> -1 Then
                lstrDesde = lblBimDesc.Text.Substring(0, lblBimDesc.Text.IndexOf(" a")).Trim()
            Else
                lstrDesde = lblBimDesc.Text
            End If
            lstrDesde = lstrDesde.Replace(lstrTipo, "").Replace(".", "").Trim()
            If lstrDesde.IndexOf("/") <> -1 Then
                lintDesdeMes = lstrDesde.Substring(lstrDesde.IndexOf("/") + 1)
            End If
            If lstrTipo = "BIM" Then
                Select Case lintDesdeMes
                    Case 1
                        lintDesdeMes = 1
                    Case 2
                        lintDesdeMes = 3
                    Case 3
                        lintDesdeMes = 5
                    Case 4
                        lintDesdeMes = 7
                    Case 5
                        lintDesdeMes = 9
                    Case Else
                        lintDesdeMes = 11
                End Select
            Else
                lintDesdeMes = 1
            End If
            If lstrTipo = "BIM" Then
                lstrDesde = "01/" & lintDesdeMes.ToString() & "/" & lstrDesde.Substring(0, lstrDesde.IndexOf("/"))
            Else
                lstrDesde = "01/01/" & lstrDesde.Substring(0, 4)
            End If
        Else
            lstrDesde = txtDesdeFecha.Text
        End If
        lstrCmd.Append(clsSQLServer.gFormatArg(lstrDesde, SqlDbType.SmallDateTime))
        lstrCmd.Append(", @fecha_hasta=")
        lstrCmd.Append(clsSQLServer.gFormatArg(txtHastaFecha.Text, SqlDbType.SmallDateTime))

        ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

        For Each dr As DataRow In ds.Tables(0).Select("")
            If lstrDeuda.IndexOf("," & dr.Item("comp_id") & ",") <> -1 Then
                ldecImpo = ldecImpo + dr.Item("cuota")
            End If
        Next
        txtTotalPagar.Text = Format(ldecImpo, "#,##0.00").ToString

        For Each dr As DataRow In ds.Tables(1).Select("")
           txtDeudaAnterior.Text = Format(dr.Item("deuda_anterior"), "#,##0.00").ToString
        Next

    End Sub

    Private Sub mCerrar()
        mConsultar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
    End Sub

    Private Sub mCargarCombo()
        clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDocuTipoFil, "S")
    End Sub
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, ByVal pbooActivo As Boolean)
        btnBaja.Enabled = Not (pbooAlta) And pbooActivo
        btnModi.Enabled = Not (pbooAlta) And pbooActivo
        'btnDeuda.Enabled = Not (pbooAlta) And pbooActivo
        btnAlta.Enabled = pbooAlta And pbooActivo

        UsrSoci.Activo = pbooActivo

        txtDesdeFecha.Enabled = pbooActivo
        txtHastaFecha.Enabled = pbooActivo

        txtObse.Enabled = pbooActivo
        chkConf.Enabled = pbooActivo
        btnDeuda.Enabled = pbooActivo
        lblBimDesc.Visible = pbooActivo
        lblBimDescConf.Visible = Not pbooActivo

    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "emer_obse")
    End Sub
#End Region

#Region "Opciones de ABM"



    Public Sub mCrearDataSet(ByVal pstrId As String)
        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)
        mdsDatos.Tables(0).TableName = mstrTabla
        'Session(mstrTabla) = mdsDatos
    End Sub

#End Region

#Region "Operacion Sobre la Grilla"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mCargarDatos(E.Item.Cells(Columnas.Id).Text)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec " + mstrTabla + "_consul")

            If mstrSociId <> "" Then
                lstrCmd.Append(" @emer_soci_id=" + mstrSociId.ToString)
            Else
                lstrCmd.Append(" @soci_apel=" + clsSQLServer.gFormatArg(txtApelFil.Text, SqlDbType.VarChar))
                lstrCmd.Append(", @soci_clie_id=" + usrClieFil.Valor.ToString)
                lstrCmd.Append(", @soci_nume=" + txtNumeFil.Valor.ToString)
                lstrCmd.Append(", @clie_cuit=" + txtCuitFil.TextoPlano.ToString)
                lstrCmd.Append(", @clie_doti_id=" + cmbDocuTipoFil.Valor.ToString)
                lstrCmd.Append(", @clie_docu_nume=" + clsSQLServer.gFormatArg(txtDocuNumeFil.Valor.ToString, SqlDbType.Int))
            End If
            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""

            txtFecha.Fecha = Today
            lblBimDesc.Text = ""
        lblBimDescConf.Text = ""
        lblBimAnteDesc.Text = ""
        UsrSoci.Limpiar()
        hdnSociId.Text = ""
        txtDesdeFecha.Text = ""
        txtHastaFecha.Text = ""
        txtObse.Text = ""
        chkConf.Checked = False

        txtComp.Text = ""
        txtImpo.Text = ""
        lblTitu.Text = ""
        hdnDeuda.Text = ""
        txtDeudaAnterior.Text = ""
        txtTotalPagar.Text = ""

        mSetearEditor("", True, True)
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        mMostrarPanel(True)
    End Sub

    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            grdDato.CurrentPageIndex = 0
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
       mAgregar()
    End Sub

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Alta()

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Modi()

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerico.Baja(hdnId.Text)

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mGuardarDatos() As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        With ldsEsta.Tables(0).Rows(0)
            .Item("emer_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("emer_soci_id") = UsrSoci.Valor
            .Item("emer_fecha") = txtFecha.Fecha
            .Item("emer_perio_desc") = lblBimDesc.Text
            .Item("emer_desde_fecha") = txtDesdeFecha.Fecha
            .Item("emer_hasta_fecha") = txtHastaFecha.Fecha

            .Item("emer_obse") = txtObse.Valor
            .Item("emer_conf") = chkConf.Checked
            .Item("comprobantes") = hdnDeuda.Text
        End With

        Return ldsEsta
    End Function

    Private Sub mValidarDatos()

        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If chkConf.Checked Then
            If txtHastaFecha.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la fecha hasta.")
            End If
            If txtDeudaAnterior.Text <> "" Then
                If Convert.ToDecimal(txtDeudaAnterior.Text) <> 0 Then
                    Throw New AccesoBD.clsErrNeg("No puede confirmar si posee deuda anterior.")
                End If
            End If
        End If


    End Sub
#End Region

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub

    Private Sub mLimpiarFiltro()
        txtApelFil.Text = ""
        txtNumeFil.Text = ""
        txtCuitFil.Text = ""
        cmbDocuTipoFil.Limpiar()
        txtDocuNumeFil.Text = ""
        usrClieFil.Limpiar()
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "EmerAgrope"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&soci_id=" + clsSQLServer.gFormatArg(mstrSociId, SqlDbType.Int)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub txtHastaFecha_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtHastaFecha.TextChanged
        'btnDeuda.Enabled = (txtDesdeFecha.Text <> "" And txtHastaFecha.Text <> "" And hdnSociId.Text <> "")
    End Sub

    Private Sub mCalcularBimestreDeuda()
        Dim lstrPerio As String = "BIM"
        If lblBimAnteDesc.Text.Substring(0, 3).ToUpper() = "SEM" Then
            lstrPerio = "SEM"
        End If
        If lblBimDesc.Text = "" Then
            If lstrPerio = "SEM" Then
                lblBimAnteDesc.Text = mAnualidad(lblBimAnteDesc.Text)
            End If
            Exit Sub
        End If
        Dim lstrDeudaDesde As String = lblBimAnteDesc.Text.Substring(3, lblBimAnteDesc.Text.ToUpper().LastIndexOf(lstrPerio) - 3).Replace("al", "").Trim()
        Dim lstrSeleDesde As String = ""
        If lblBimDesc.Text.ToUpper().LastIndexOf(lstrPerio) <> -1 Then
            lstrSeleDesde = lblBimDesc.Text.Substring(3, lblBimDesc.Text.ToUpper().LastIndexOf(lstrPerio) - 3).Replace("al", "").Trim()
        End If
        lstrSeleDesde = lstrSeleDesde.Replace(".", "")
        Dim lintAnio As Integer = Convert.ToInt32(lstrSeleDesde.Substring(0, 2))
        Dim lintBim As Integer = Convert.ToInt32(lstrSeleDesde.Substring(3, 1))
        lintBim = lintBim - 1
        If lintBim = 0 Then
            lintBim = IIf(lstrPerio = "BIM", 6, 2)
            lintAnio = lintAnio - 1
        End If
        If (lintAnio < 0) Then
            lintAnio = 99
        End If
        If (lintAnio < 10) Then
            lintAnio = "0" + lintAnio
        End If
        If (lintAnio = 0) Then
            lintAnio = "000"
        End If
        If txtDeudaAnterior.Text <> "0.00" Then
            lblBimAnteDesc.Text = lstrPerio & " " & lstrDeudaDesde & " al " & lstrPerio & " " & IIf(lintAnio < 10, "0" & lintAnio.ToString, lintAnio.ToString) & "/" & lintBim.ToString()
            If lstrPerio = "SEM" Then
                lblBimAnteDesc.Text = mAnualidad(lblBimAnteDesc.Text)
            End If
        Else
            lblBimAnteDesc.Text = ""
        End If
        If lblBimDesc.Text <> "" Then
            If lblBimDesc.Text.Trim.Substring(0, 3) = "SEM" Then
                lblBimDesc.Text = mAnualidad(lblBimDesc.Text)
            End If
        End If
    End Sub

    Private Function mAnualidad(ByVal pstrDesc As String) As String
        Dim lstrDescAnual As String = ""
        If pstrDesc = "" Then
            Return ""
        Else
            Dim lstrPrefIni As String = "20"
            Dim lstrPrefFin As String = "20"
            Dim lstrAnioDesde = pstrDesc.Replace("SEM", "")
            Dim lstrAnioHasta = lstrAnioDesde.substring(lstrAnioDesde.indexOf("/") + 3)
            lstrAnioDesde = lstrAnioDesde.substring(0, lstrAnioDesde.indexOf("/"))
            lstrAnioHasta = lstrAnioHasta.replace("SEM", "").replace("al ", "")
            lstrAnioHasta = lstrAnioHasta.substring(0, lstrAnioHasta.indexOf("/"))
            lstrAnioDesde = lstrAnioDesde.replace(" ", "").replace(".", "")
            lstrAnioHasta = lstrAnioHasta.replace(" ", "").replace(".", "")
            If (lstrAnioDesde > "50") Then
                lstrPrefIni = "19"
            End If
            If (lstrAnioHasta > "50") Then
                lstrPrefFin = "19"
            End If
            lstrDescAnual = lstrPrefIni + pstrDesc
            lstrDescAnual = lstrDescAnual.Replace("SEM", "")
            lstrDescAnual = lstrDescAnual.Replace("/1", "")
            lstrDescAnual = lstrDescAnual.Replace("/2", "")
            lstrDescAnual = lstrDescAnual.Replace("SEM", "")
            lstrDescAnual = lstrDescAnual.Replace("/1", "")
            lstrDescAnual = lstrDescAnual.Replace("/2", "")
            lstrDescAnual = lstrDescAnual.Replace("al ", "al " & lstrPrefFin)
            lstrDescAnual = lstrDescAnual.Replace(lstrPrefIni & " ", lstrPrefIni)
            lstrDescAnual = lstrDescAnual.Replace(lstrPrefFin & " ", lstrPrefFin)
            lstrDescAnual = lstrDescAnual.Replace(".", "")
            lstrDescAnual = lstrDescAnual.Replace(".", "")
            Dim lstrAnio As String = lstrDescAnual.Replace("al", "")
            If lstrAnio.IndexOf(" ") <> -1 Then
                Dim lstrDesde As String = lstrAnio.Substring(0, lstrAnio.IndexOf(" ")).Replace(" ", "")
                Dim lstrHasta As String = lstrAnio.Substring(lstrAnio.IndexOf(" ")).Replace(" ", "")
                If lstrDesde.Replace(" ", "") = lstrHasta.Replace(" ", "") Then
                    lstrDescAnual = lstrDesde.Replace(" ", "")
                End If
            End If
            Return ("Anualidad " + lstrDescAnual)
        End If
    End Function

    Private Sub hdnDatosPop_Change(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Dim lvrDatos, lvrDato As String()

        If (hdnDatosPop.Text <> "") Then
            lvrDatos = hdnDatosPop.Text.Split(Chr(6))
            hdnDeuda.Text = lvrDatos(0).ToString
            txtTotalPagar.Valor = Format(Convert.ToDecimal(lvrDatos(1)), "#,##0.00").ToString
            txtDeudaAnterior.Valor = lvrDatos(2).ToString
            lblBimDesc.Text = lvrDatos(3).ToString
            lblBimDescConf.Text = lblBimDesc.Text
            If txtDeudaAnterior.Text <> "0.00" Then
                lblBimAnteDesc.Text = clsSQLServer.gCampoValorConsul(mstrConn, "deuda_social_descrip_consul " & UsrSoci.Valor.ToString, "deuda_descrip")
                mCalcularBimestreDeuda()
            Else
                lblBimAnteDesc.Text = ""
                If lblBimDesc.Text <> "" Then
                    If lblBimDesc.Text.Trim.Substring(0, 3) = "SEM" Then
                        lblBimDesc.Text = mAnualidad(lblBimDesc.Text)
                    End If
                End If
            End If
            hdnDatosPop.Text = ""
        End If

    End Sub
End Class
End Namespace
