<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.noaccess" CodeFile="noaccess.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Access Control</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE id="tabAcce" style="Z-INDEX: 101; LEFT: 3px; WIDTH: 479px; POSITION: absolute; TOP: 3px; HEIGHT: 214px"
				cellSpacing="1" cellPadding="1" width="479" border="0">
				<TR>
					<TD style="WIDTH: 84px; HEIGHT: 42px"></TD>
					<TD style="HEIGHT: 42px"></TD>
					<TD style="HEIGHT: 42px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 84px; HEIGHT: 89px"></TD>
					<TD style="HEIGHT: 89px"><asp:label class="mensajeerror" id="lblError" runat="server">No se permite el acceso a esta opci�n.</asp:label></TD>
					<TD style="HEIGHT: 89px"></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 84px"></TD>
					<TD><asp:label class="titulo" id="lblMess" runat="server">Usted no tiene permiso para acceder a esta opci�n.</asp:label></TD>
					<TD></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 84px"></TD>
					<TD><asp:label class="titulo" id="lblMess2" runat="server">
						Por favor, contacte al administrador del sistema.</asp:label></TD>
					<TD></TD>
				</TR>
			</TABLE>
		</form>
	</body>
</HTML>
