<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Monedas" CodeFile="Monedas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Monedas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Monedas</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center"><asp:datagrid id="grdDato" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" CausesValidation="false" Text="Editar" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="mone_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="mone_abre" ReadOnly="True" HeaderText="Abreviatura">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="mone_desc" ReadOnly="True" HeaderText="Descripci&#243;n"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cuenta" HeaderText="Cuenta Contable" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn HeaderText="Estado">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" HeaderText="fecha"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
													ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
													ImageOver="btnNuev2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nueva Moneda"
													ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="center" width="50"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
													ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"
													ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD><a name="editar"></a></TD>
								<TD align="center"><asp:panel id="panDato" runat="server" width="95%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
										<TABLE style="WIDTH: 100%; HEIGHT: 100%" id="Table2" class="FdoFld" border="0" cellPadding="0"
											align="left">
											<TR>
												<TD style="WIDTH: 142px">
													<P></P>
												</TD>
												<TD height="5">
													<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="219px"></asp:Label></TD>
												<TD vAlign="top" rowSpan="2" align="right">&nbsp;
													<asp:ImageButton id="imgClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 142px" align="right">
													<asp:Label id="lblCodi" runat="server" cssclass="titulo" Width="73px">Abreviatura:</asp:Label></TD>
												<TD>
													<CC1:TEXTBOXTAB id="txtAbre" runat="server" cssclass="cuadrotexto" Width="199px"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 142px; HEIGHT: 16px" align="right">&nbsp;
													<asp:Label id="lblDesc" runat="server" cssclass="titulo" Width="90px">Descripción:</asp:Label></TD>
												<TD style="HEIGHT: 16px" height="16" colSpan="2">
													<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="250px"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 142px" align="right">
													<asp:Label id="lblCuen" runat="server" cssclass="titulo">Cuenta Contable:</asp:Label></TD>
												<TD colSpan="2">
													<TABLE border="0" cellSpacing="0" cellPadding="0">
														<TR>
															<TD>
																<cc1:combobox id="cmbCuen" class="combo" runat="server" Obligatorio="True" CssClass="combo" Filtra="True"
																	AceptaNull="False" MostrarBotones="False"></cc1:combobox></TD>
															<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																	id="btnAvanBusq" onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCuen','Cuentas Contables','');"
																	border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD colSpan="3" align="center">
													<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
											</TR>
											<TR> <!--TD style="WIDTH: 142px"></TD-->
												<TD colSpan="3" align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
													<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
														Text="Baja"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
														Text="Modificar"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
														Text="Limpiar"></asp:Button></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:Button style="Z-INDEX: 0" id="Button1" runat="server" cssclass="boton" Width="80px" Text="mail cs"></asp:Button>
				<asp:Button style="Z-INDEX: 0" id="Button2" runat="server" cssclass="boton" Width="80px" Text="mail cslmail"></asp:Button>
				<asp:Button style="Z-INDEX: 0" id="Button3" runat="server" cssclass="boton" Width="80px" Text="mail.vb"></asp:Button>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["txtAbre"]!= null)
			document.all["txtAbre"].focus();
		</SCRIPT>
	</BODY>
</HTML>
