Imports System.Data.SqlClient


Namespace SRA


Partial Class LeyendaComprobantes
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lnkCabecera As System.Web.UI.WebControls.LinkButton
   Protected WithEvents lnkDeta As System.Web.UI.WebControls.LinkButton


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "leyendas_factu"

   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String


   Private Enum Columnas As Integer
      DistId = 1
      Codigo = 2
      Descrip = 3
      

   End Enum

   
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()


         If (Not Page.IsPostBack) Then
                mCargarCombos()
            mSetearEventos()
            mEstablecerPerfil()
            mSetearMaxLength()

				mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)

         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub
    Private Sub mCargarCombos()
        Dim mdsDatos As New DataSet
		', " @actiProf=1"
		clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActividadFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "T")
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        Me.txtAbre.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "lefa_abre")
        Me.txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "lefa_desc")
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean

        'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Distritos, String), (mstrConn), (Session("sUserId").ToString()))) Then
        'Response.Redirect("noaccess.aspx")
        'End If

        'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Distritos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        'btnAlta.Visible = lbooPermiAlta

        'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Distritos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        'btnModi.Visible = lbooPermiModi

        'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
			mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.DistId).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            Me.txtAbre.Valor = .Item("lefa_abre")
                txtDesc.Valor = .Item("lefa_desc")
                cmbActi.Valor = .Item("lefa_acti_id")
                If Not .IsNull("lefa_desde_fecha") Then
                    txtFDesde.Text = .Item("lefa_desde_fecha")
                End If
                If Not .IsNull("lefa_hasta_fecha") Then
                    txtFHasta.Text = .Item("lefa_hasta_fecha")
                End If
                If Not .IsNull("lefa_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("lefa_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If
            End With

         mSetearEditor("", False)
         mMostrarPanel(True)


      End If
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()

      hdnId.Text = ""

      mCrearDataSet("")
      lblBaja.Text = ""
      txtAbre.Text = ""
      txtDesc.Text = ""
		cmbActi.Valor = ""
		txtFDesde.Text = ""
		txtFHasta.Text = ""
      lblTitu.Text = ""


      mSetearEditor("", True)

	End Sub
	Private Sub mLimpiarFiltros()
		cmbActividadFil.Valor = ""
		txtFechaDesdeFil.Text = ""
		txtFechaHastaFil.Text = ""
	End Sub

	Private Sub mCerrar()
		mMostrarPanel(False)
	End Sub
	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		If (pbooVisi) Then
			hdnPage.Text = " "
		Else
			hdnPage.Text = ""
		End If
		panDato.Visible = pbooVisi
		btnAgre.Enabled = Not (panDato.Visible)

		panDato.Visible = pbooVisi

		panCabecera.Visible = True

	End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos(True)
         Dim lobjDistritos As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjDistritos.Alta()
			mConsultar()


         mMostrarPanel(False)


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         mGuardarDatos(False)
         Dim lobjIncripciones As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjIncripciones.Modi()

			mConsultar()

         mMostrarPanel(False)



      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjIncripciones As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjIncripciones.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0


			mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidaDatos(ByVal pboolAlta As Boolean)


      With mdsDatos.Tables(0).Rows(0)
         If txtAbre.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Abreviatura.")
         End If

         If txtDesc.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Descripci�n.")
         End If

      End With


   End Sub

   Private Function mGuardarDatos(ByVal pboolAlta As Boolean) As DataSet

      mValidaDatos(pboolAlta)

      With mdsDatos.Tables(0).Rows(0)
         .Item("lefa_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("lefa_abre") = txtAbre.Valor
            .Item("lefa_desc") = txtDesc.Valor
            .Item("lefa_desde_fecha") = txtFDesde.Fecha
            .Item("lefa_hasta_fecha") = txtFHasta.Fecha
            .Item("lefa_acti_id") = cmbActi.Valor


         For i As Integer = 0 To .Table.Columns.Count - 1
            If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
               .Item(i) = DBNull.Value
            End If
         Next
      End With

      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla



      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If


      Session(mstrTabla) = mdsDatos

   End Sub

#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub



#End Region

#Region "Detalle"


	Public Sub mConsultar()
		Try
			
			mstrCmd = "exec " + mstrTabla + "_consul"
			mstrCmd += " @acti_id=" + clsSQLServer.gFormatArg(cmbActividadFil.Valor, SqlDbType.Int)
			mstrCmd += ",@fechadesde=" + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha)
			mstrCmd += ",@fechahasta=" + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha)

			clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
		Try
			Dim lstrRptName As String = "LeyendasComprobantes"
			Dim lstrperiodo As String
			Dim lintFechaH As Integer
			Dim lintFechaD As Integer

			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			lstrRpt += "&acti_id=" + clsSQLServer.gFormatArg(cmbActividadFil.Valor, SqlDbType.Int)

			If txtFechaDesdeFil.Text = "" Then
				lintFechaD = 0
			Else
				lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
			End If

			lstrRpt += "&fechadesde=" + lintFechaD.ToString

			If txtFechaHastaFil.Text = "" Then
				lintFechaH = 0
			Else
				lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
			End If

			lstrRpt += "&fechahasta=" + lintFechaH.ToString

			If txtFechaDesdeFil.Text = "" And txtFechaHastaFil.Text = "" Then
				lstrperiodo = "Todo"
			ElseIf txtFechaDesdeFil.Text <> "" And txtFechaHastaFil.Text <> "" Then
				lstrperiodo = txtFechaDesdeFil.Text & " al " & txtFechaHastaFil.Text
			ElseIf txtFechaDesdeFil.Text <> "" And txtFechaHastaFil.Text = "" Then
				lstrperiodo = "desde: " & txtFechaDesdeFil.Text
			ElseIf txtFechaDesdeFil.Text = "" And txtFechaHastaFil.Text <> "" Then
				lstrperiodo = "hasta: " & txtFechaHastaFil.Text
			End If

            lstrRpt += "&Periodo=" + lstrperiodo

			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub


	Private Sub mSetearEditorMail(ByVal pbooAlta As Boolean)
		btnBaja.Enabled = Not (pbooAlta)
		btnModi.Enabled = Not (pbooAlta)
		btnAlta.Enabled = pbooAlta
	End Sub


#End Region

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

	Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltros()

	End Sub

	Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
		mConsultar()
	End Sub
End Class
End Namespace
