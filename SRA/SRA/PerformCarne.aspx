<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.PerformCarne" CodeFile="PerformCarne.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Performance de Carne</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="javascript">
		function usrProductoFil_cmbProdRaza_onchange()
		{
			mSetearRazas('usrProductoFil:cmbProdRaza');
		}
		
		function usrCriadorFil_cmbRazaCria_onchange()
		{
			mSetearRazas('usrCriadorFil:cmbRazaCria');
		}
				
		function mSetearRazas(pstrControl)
		{	
			if (document.all(pstrControl)!=null)
			{
				if (document.all('cmbRazaFil')!=null && document.all('cmbRazaFil').value != document.all(pstrControl).value)
				{
					document.all('txtcmbRazaFil').value = document.all('txt' + pstrControl).value;
					document.all('txtcmbRazaFil').onchange();
				}	
				if (document.all('usrProductoFil:cmbProdRaza')!=null && document.all('usrProductoFil:cmbProdRaza').value != document.all(pstrControl).value)
				{
					document.all('txtusrProductoFil:cmbProdRaza').value = document.all('txt' + pstrControl).value;
					document.all('txtusrProductoFil:cmbProdRaza').onchange();
				}	
				if (document.all('usrCriadorFil:cmbRazaCria')!=null && document.all('usrCriadorFil:cmbRazaCria').value != document.all(pstrControl).value)
				{
					document.all('txtusrCriadorFil:cmbRazaCria').value = document.all('txt' + pstrControl).value;
					document.all('txtusrCriadorFil:cmbRazaCria').onchange();
				}
			}	
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="2"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Performance de Carne</asp:label></TD>
									</TR>
									<TR>
										<TD colSpan="2" height="10"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" Visible="True" BorderStyle="Solid"
												BorderWidth="0">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="17" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 25px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrCriadorFil" runat="server" Criador="True" Alto="560" Ancho="780" Saltos="1,1,1"
																						Tabla="Criadores" MuestraDesc="False" AceptaNull="False"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblRazaFil" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbRazaFil" runat="server" cssclass="cuadrotexto" Width="280px"
																						AceptaNull="False" Height="20px" filtra="true" onchange="mSetearRazas('cmbRazaFil')" NomOper="razas_cargar"
																						MostrarBotones="False"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblProductoFil" runat="server" cssclass="titulo">Producto:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<UC1:PROD id="usrProductoFil" runat="server" Ancho="800" Saltos="1,2" Tabla="productos" MuestraDesc="False"
																						AceptaNull="false" FilDocuNume="True" FilTipo="S" FilSociNume="True" AutoPostBack="False"></UC1:PROD></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD colSpan="2" height="10"></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
												HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
												AutoGenerateColumns="False">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="_pfca_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn Visible="False" DataField="_prdt_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="_Criador" ReadOnly="True" HeaderText="Criador">
														<HeaderStyle Width="40%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_Raza" ReadOnly="True" HeaderText="Raza">
														<HeaderStyle Width="30%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_Producto" ReadOnly="True" HeaderText="Producto">
														<HeaderStyle Width="30%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD align="right" colSpan="2"><CC1:BOTONIMAGEN id="btnListar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
												ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
												BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Listar"></CC1:BOTONIMAGEN></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" BorderStyle="Solid"
												BorderWidth="1px" Height="116px" width="100%">
												<TABLE class="FdoFld" id="Table6" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD vAlign="top" colSpan="3">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">
															<asp:imagebutton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right" width="30%" height="15">
															<asp:Label id="lblPesoNacer" runat="server" cssclass="titulo">Peso al nacer:</asp:Label>&nbsp;</TD>
														<TD width="15%">
															<cc1:numberbox id="txtPesoNacer" runat="server" cssclass="cuadrotexto" Width="75px" obligatorio="true"></cc1:numberbox></TD>
														<TD align="right" width="30%">
															<asp:Label id="lblDepLeche" runat="server" cssclass="titulo">Dep leche:</asp:Label>&nbsp;</TD>
														<TD width="15%">
															<cc1:numberbox id="txtDepLeche" tabIndex="9" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right" height="15">
															<asp:Label id="lblDepPesoNacer" runat="server" cssclass="titulo">Dep Peso al nacer:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtDepPesoNacer" tabIndex="1" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
														<TD align="right">
															<asp:Label id="lblExaLeche" runat="server" cssclass="titulo">Exactitud leche:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtExaLeche" tabIndex="10" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right" height="15">
															<asp:Label id="lblExaPesoNacer" runat="server" cssclass="titulo">Exactitud peso al nacer:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtExaPesoNacer" tabIndex="2" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
														<TD align="right">
															<asp:Label id="lblLecheCrece" runat="server" cssclass="titulo">Leche y crecimiento:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtLecheCrece" tabIndex="11" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right" height="15">
															<asp:Label id="lblPesoDeste" runat="server" cssclass="titulo">Peso al destete:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtPesoDeste" tabIndex="3" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
														<TD align="right">
															<asp:Label id="lblDepCircun" runat="server" cssclass="titulo">Dep circunferencia escrotal:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtDepCircun" tabIndex="12" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right" height="15">
															<asp:Label id="lblDepPesoDeste" runat="server" cssclass="titulo">Dep peso al destete:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtDepPesoDeste" tabIndex="4" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
														<TD align="right">
															<asp:Label id="lblExaCircun" runat="server" cssclass="titulo">Exactitud circunferencia escrotal:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtExaCircun" tabIndex="13" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right" height="15">
															<asp:Label id="lblExaPesoDeste" runat="server" cssclass="titulo">Exactitud peso al destete:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtExaPesoDeste" tabIndex="5" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
														<TD align="right">
															<asp:Label id="lblDepAreaOjo" runat="server" cssclass="titulo">Dep area ojo de bife:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtDepAreaOjo" tabIndex="14" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right" height="15">
															<asp:Label id="lblPesoFinal" runat="server" cssclass="titulo">Peso final:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtPesoFinal" tabIndex="6" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
														<TD align="right">
															<asp:Label id="lblExaAreaOjo" runat="server" cssclass="titulo">Exactitud area ojo de bife:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtExaAreaOjo" tabIndex="15" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right" height="15">
															<asp:Label id="lblDepPesoFinal" runat="server" cssclass="titulo">Dep peso final:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtDepPesoFinal" tabIndex="7" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
														<TD align="right">
															<asp:Label id="lblDepEngorda" runat="server" cssclass="titulo">Dep engordas grasa dorsal:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtDepEngorda" tabIndex="16" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD align="right" height="15">
															<asp:Label id="lblExaPesoFinal" runat="server" cssclass="titulo">Exactitud peso final:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtExaPesoFinal" tabIndex="8" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
														<TD align="right">
															<asp:Label id="lblExaEngorda" runat="server" cssclass="titulo">Exactitud engordas grasa dorsal:</asp:Label>&nbsp;</TD>
														<TD>
															<cc1:numberbox id="txtExaEngorda" tabIndex="17" runat="server" cssclass="cuadrotexto" Width="75px"
																obligatorio="true"></cc1:numberbox></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnPfca_prdt_id" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		
		mSetearRazas('usrProductoFil:cmbProdRaza');
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
