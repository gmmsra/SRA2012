Namespace SRA

Partial Class Contactos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblNroRef As System.Web.UI.WebControls.Label
   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub
#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Contactos
   Private mstrContMail As String = SRA_Neg.Constantes.gTab_MailsContactos
   Private mstrContTel As String = SRA_Neg.Constantes.gTab_TeleContactos
   Private mstrContEstud As String = SRA_Neg.Constantes.gTab_EstudiosContactos
   Private mstrContExplot As String = SRA_Neg.Constantes.gTab_ExplotaContactos
   Private mstrContFunc As String = SRA_Neg.Constantes.gTab_FuncionesContactos
   Private mstrContOcup As String = SRA_Neg.Constantes.gTab_OcupacionesContactos

   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
#End Region

#Region "Inicializaci�n de Variables"
   Private Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar(grdDato)
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnMailBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnTelBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnEstuBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnExplBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnFuncBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnOcupBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActiFil, "T")

      clsWeb.gCargarRefeCmb(mstrConn, "organ_contactos", cmbOrg, "N")
      clsWeb.gCargarRefeCmb(mstrConn, "clasifi_contactos", cmbClasi, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "clasifi_contactos", cmbClasiFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "docu_tipos", cmbDoti, "S")

      clsWeb.gCargarRefeCmb(mstrConn, "tele_tipos", cmbTiTel, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbExclPais, "S")


      clsWeb.gCargarRefeCmb(mstrConn, "estudios", cmbEstuEst, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "explota_tipos", cmbExplTipo, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "funciones", cmbFuncFunc, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "ocupaciones", cmbOcupOcup, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbExplPais, "S")

      If Not Request.Form("cmbExclPais") Is Nothing Then
         cmbExclPais.Valor = Request.Form("cmbExclPais")
         clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbExclPais.Valor, cmbExclPcia, "id", "descrip", "S", True)
      End If
   End Sub
   Private Sub mCargarProvincias()
      clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbExclPcia, "S", IIf(cmbExclPais.Valor Is DBNull.Value, "-1", cmbExclPais.Valor))
      mCargarLocalidades()
   End Sub
   Private Sub mCargarLocalidades()
      clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbExclLoca, "N", IIf(cmbExclPcia.Valor Is DBNull.Value, "-1", cmbExclPcia.Valor))
      clsWeb.gCargarCombo(mstrConn, "localidades_aux_cargar " & cmbExclPcia.Valor, cmbLocaAux, "id", "descrip", "S", True)
   End Sub
   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Usuarios, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Usuarios_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLong As Object

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtApelFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cotc_nyap")
      txtNyA.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cotc_nyap")
      txtDire.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cotc_dire")
      txtCpos.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cotc_cpos")
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cotc_obse")

      txtDeud.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cotc_deud")
      txtAnteNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cotc_ante_nume")
      txtDocuNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cotc_docu_nume")

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrContTel)
      txtArea.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tect_area_code")
      txtTel.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tect_tele")
      txtRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tect_refe")

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrContMail)
      txtEmail.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "mact_mail")
      txtMRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "mact_refe")

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrContExplot)
      txtExplRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "exct_refe")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If

         mConsultar(grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdMail_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdMail.EditItemIndex = -1
         If (grdMail.CurrentPageIndex < 0 Or grdMail.CurrentPageIndex >= grdMail.PageCount) Then
            grdMail.CurrentPageIndex = 0
         Else
            grdMail.CurrentPageIndex = E.NewPageIndex
         End If
         grdMail.DataSource = mdsDatos.Tables(mstrContMail)
         grdMail.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdTele_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdTelefono.EditItemIndex = -1
         If (grdTelefono.CurrentPageIndex < 0 Or grdTelefono.CurrentPageIndex >= grdTelefono.PageCount) Then
            grdTelefono.CurrentPageIndex = 0
         Else
            grdTelefono.CurrentPageIndex = E.NewPageIndex
         End If
         grdTelefono.DataSource = mdsDatos.Tables(mstrContTel)
         grdTelefono.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdEstu_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdEstudios.EditItemIndex = -1
         If (grdEstudios.CurrentPageIndex < 0 Or grdEstudios.CurrentPageIndex >= grdEstudios.PageCount) Then
            grdEstudios.CurrentPageIndex = 0
         Else
            grdEstudios.CurrentPageIndex = E.NewPageIndex
         End If

         grdEstudios.DataSource = mdsDatos.Tables(mstrContEstud)
         grdEstudios.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdOcup_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdOcupaciones.EditItemIndex = -1
         If (grdOcupaciones.CurrentPageIndex < 0 Or grdOcupaciones.CurrentPageIndex >= grdOcupaciones.PageCount) Then
            grdOcupaciones.CurrentPageIndex = 0
         Else
            grdOcupaciones.CurrentPageIndex = E.NewPageIndex
         End If

         grdOcupaciones.DataSource = mdsDatos.Tables(mstrContOcup)
         grdOcupaciones.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdFunc_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdFunciones.EditItemIndex = -1
         If (grdFunciones.CurrentPageIndex < 0 Or grdFunciones.CurrentPageIndex >= grdFunciones.PageCount) Then
            grdFunciones.CurrentPageIndex = 0
         Else
            grdFunciones.CurrentPageIndex = E.NewPageIndex
         End If

         grdFunciones.DataSource = mdsDatos.Tables(mstrContFunc)
         grdFunciones.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdExpl_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdExplotaciones.EditItemIndex = -1
         If (grdExplotaciones.CurrentPageIndex < 0 Or grdExplotaciones.CurrentPageIndex >= grdExplotaciones.PageCount) Then
            grdExplotaciones.CurrentPageIndex = 0
         Else
            grdExplotaciones.CurrentPageIndex = E.NewPageIndex
         End If

         grdExplotaciones.DataSource = mdsDatos.Tables(mstrContExplot)
         grdExplotaciones.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mLimpiar()
      hdnId.Text = ""
      txtTrat.Text = ""
      txtNyA.Text = ""
      cmbOrg.Limpiar()
      cmbClasi.Limpiar()
      cmbActi.Limpiar()

      txtDire.Text = ""
      txtCpos.Text = ""
      txtObse.Text = ""
      lblBaja.Text = ""

      txtAnteNume.Text = ""
      txtDeud.Text = ""
      txtIngrFecha.Text = ""
      txtNaciFecha.Text = ""
      cmbDoti.Limpiar()
      txtDocuNume.Text = ""

      cmbExclPais.Limpiar()
      cmbExclPcia.Items.Clear()
      cmbExclLoca.Items.Clear()
      cmbLocaAux.Items.Clear()

      cmbExplPais.Limpiar()
      cmbExplProv.Items.Clear()
      cmbExplLoca.Items.Clear()
      cmbExplLocaAux.Items.Clear()

      grdTelefono.CurrentPageIndex = 0
      grdMail.CurrentPageIndex = 0
      grdEstudios.CurrentPageIndex = 0
      grdExplotaciones.CurrentPageIndex = 0
      grdFunciones.CurrentPageIndex = 0
      grdOcupaciones.CurrentPageIndex = 0

      mCrearDataSet("")

      mLimpiarMail()
      mLimpiarTelefonos()
      mLimpiarEstudios()
      mLimpiarExplotaciones()
      mLimpiarFunciones()
      mLimpiarOcupaciones()

      mSetearEditor("", True)
      mShowTabs(1)
   End Sub
   Private Sub mLimpiarFiltros()
      cmbActiFil.Limpiar()
      txtApelFil.Text = ""
      cmbClasiFil.Limpiar()
      chkBusc.Checked = False
      mConsultar(grdDato)
   End Sub
   Private Sub mLimpiarMail()
      hdnMailId.Text = ""
      txtEmail.Text = ""
      txtMRefe.Text = ""

      mSetearEditor(mstrContMail, True)
   End Sub
   Private Sub mLimpiarTelefonos()
      hdnTeleId.Text = ""
      cmbTiTel.Limpiar()
      txtTel.Text = ""
      txtArea.Text = ""
      txtRefe.Text = ""

      mSetearEditor(mstrContTel, True)
   End Sub
   Private Sub mLimpiarEstudios()
      hdnEstuId.Text = ""
      cmbEstuEst.Limpiar()
      mSetearEditor(mstrContEstud, True)
   End Sub
   Private Sub mLimpiarExplotaciones()
      hdnExplId.Text = ""
      cmbExplTipo.Limpiar()
      cmbExplPais.Limpiar()
      cmbExplProv.Items.Clear()
      cmbExplLoca.Items.Clear()
      cmbExplLocaAux.Items.Clear()
      txtExplRefe.Text = ""
      mSetearEditor(mstrContExplot, True)
   End Sub
   Private Sub mLimpiarFunciones()
      hdnFuncId.Text = ""
      cmbFuncFunc.Limpiar()
      mSetearEditor(mstrContFunc, True)
   End Sub
   Private Sub mLimpiarOcupaciones()
      hdnOcupId.Text = ""
      cmbOcupOcup.Limpiar()
      mSetearEditor(mstrContOcup, True)
   End Sub

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrContTel
            btnTelBaja.Enabled = Not (pbooAlta)
            btnTelModi.Enabled = Not (pbooAlta)
            btnTelAlta.Enabled = pbooAlta
         Case mstrContMail
            btnMailBaja.Enabled = Not (pbooAlta)
            btnMailModi.Enabled = Not (pbooAlta)
            btnMailAlta.Enabled = pbooAlta
         Case mstrContEstud
            btnEstuBaja.Enabled = Not (pbooAlta)
            btnEstuModi.Enabled = Not (pbooAlta)
            btnEstuAlta.Enabled = pbooAlta
         Case mstrContExplot
            btnExplBaja.Enabled = Not (pbooAlta)
            btnExplModi.Enabled = Not (pbooAlta)
            btnExplAlta.Enabled = pbooAlta
         Case mstrContFunc
            btnFuncBaja.Enabled = Not (pbooAlta)
            btnFuncModi.Enabled = Not (pbooAlta)
            btnFuncAlta.Enabled = pbooAlta
         Case mstrContOcup
            btnOcupBaja.Enabled = Not (pbooAlta)
            btnOcupModi.Enabled = Not (pbooAlta)
            btnOcupAlta.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      mMostrarPanel(True)
   End Sub
   Private Sub mCerrar()
      mLimpiar()
      mConsultar(grdDato)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      Dim lbooVisiOri As Boolean = panDato.Visible

      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panSolapas.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
         mShowTabs(1)
      Else
         Session(mstrTabla) = Nothing
         mdsDatos = Nothing
      End If

      If lbooVisiOri And Not pbooVisi Then
         mLimpiarFiltros()
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjContacto As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjContacto.Alta()
         mConsultar(grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjContacto As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjContacto.Modi()

         mConsultar(grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjContacto As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjContacto.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar(grdDato)

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If txtTrat.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el tratamiento")
      End If

      If txtNyA.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el nombre y apellido")
      End If

      If cmbActi.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar una actividad")
      End If

      If cmbClasi.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar una clasificaci�n")
      End If

      If cmbExclPcia.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar una provincia")
      End If
   End Sub
   Private Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrContTel
      mdsDatos.Tables(2).TableName = mstrContMail
      mdsDatos.Tables(3).TableName = mstrContEstud
      mdsDatos.Tables(4).TableName = mstrContExplot
      mdsDatos.Tables(5).TableName = mstrContFunc
      mdsDatos.Tables(6).TableName = mstrContOcup

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      grdTelefono.DataSource = mdsDatos.Tables(mstrContTel)
      grdTelefono.DataBind()

      grdMail.DataSource = mdsDatos.Tables(mstrContMail)
      grdMail.DataBind()

      grdEstudios.DataSource = mdsDatos.Tables(mstrContEstud)
      grdEstudios.DataBind()

      grdExplotaciones.DataSource = mdsDatos.Tables(mstrContExplot)
      grdExplotaciones.DataBind()

      grdFunciones.DataSource = mdsDatos.Tables(mstrContFunc)
      grdFunciones.DataBind()

      grdOcupaciones.DataSource = mdsDatos.Tables(mstrContOcup)
      grdOcupaciones.DataBind()

      Session(mstrTabla) = mdsDatos

   End Sub
   Private Sub ActualizarTelefono()
      Try
         mGuardarTelefonos()

         mLimpiarTelefonos()
         grdTelefono.DataSource = mdsDatos.Tables(mstrContTel)
         grdTelefono.DataBind()


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub ActualizarMail()
      Try
         mGuardarMails()

         mLimpiarMail()
         grdMail.DataSource = mdsDatos.Tables(mstrContMail)
         grdMail.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub ActualizarEstudios()
      Try
         mGuardarEstudios()

         mLimpiarEstudios()
         grdEstudios.DataSource = mdsDatos.Tables(mstrContEstud)
         grdEstudios.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub ActualizarExplotaciones()
      Try
         mGuardarExplotaciones()

         mLimpiarExplotaciones()
         grdExplotaciones.DataSource = mdsDatos.Tables(mstrContExplot)
         grdExplotaciones.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub ActualizarFunciones()
      Try
         mGuardarFunciones()

         mLimpiarFunciones()
         grdFunciones.DataSource = mdsDatos.Tables(mstrContFunc)
         grdFunciones.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub ActualizarOcupaciones()
      Try
         mGuardarOcupaciones()

         mLimpiarOcupaciones()
         grdOcupaciones.DataSource = mdsDatos.Tables(mstrContOcup)
         grdOcupaciones.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Detalle"
   Public Sub mConsultar(ByVal grdDato As DataGrid)
      Try

         mstrCmd = "exec " + mstrTabla + "_consul "
         mstrCmd += "@cotc_acti_id=" + cmbActiFil.Valor.ToString
         mstrCmd += ", @cotc_nyap =" + clsSQLServer.gFormatArg(txtApelFil.Text, SqlDbType.VarChar)
         mstrCmd += ", @cotc_clcc_id =" + cmbClasiFil.Valor.ToString
         mstrCmd += ", @buscar_en=" + IIf(chkBusc.Checked, "1", "0")
         mstrCmd += ", @cotc_ante_nume = " + IIf(txtNroRefFil.Text = "", "null", txtNroRefFil.Text)
         mstrCmd += ", @formato='1'"
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(mstrTabla).Rows(0)
                    hdnId.Text = .Item("cotc_id").ToString()
                    txtTrat.Valor = .Item("cotc_trata")
                    txtNyA.Valor = .Item("cotc_nyap")
                    txtDire.Valor = .Item("cotc_dire")
                    txtCpos.Valor = .Item("cotc_cpos")
                    txtObse.Valor = .Item("cotc_obse")
                    cmbActi.Valor = IIf(.Item("cotc_acti_id").ToString.Length > 0, .Item("cotc_acti_id"), String.Empty)
                    cmbClasi.Valor = IIf(.Item("cotc_clcc_id").ToString.Length > 0, .Item("cotc_clcc_id"), String.Empty)
                    cmbOrg.Valor = IIf(.Item("cotc_orcc_id").ToString.Length > 0, .Item("cotc_orcc_id"), "0")

                    txtDeud.Valor = .Item("cotc_deud")
                    txtAnteNume.Valor = .Item("cotc_ante_nume")
                    txtIngrFecha.Fecha = .Item("cotc_ingr_fecha")
                    txtNaciFecha.Fecha = IIf(.Item("cotc_naci_fecha").ToString.Length > 0, .Item("cotc_naci_fecha"), String.Empty)
                    cmbDoti.Valor = IIf(.Item("cotc_doti_id").ToString.Length > 0, .Item("cotc_doti_id"), String.Empty)
                    txtDocuNume.Valor = .Item("cotc_docu_nume")

                    cmbExclPais.Valor = IIf(.Item("_pcia_pais_id").ToString.Length > 0, .Item("_pcia_pais_id"), "0")
                    mCargarProvincias()
                    cmbExclPcia.Valor = IIf(.Item("cotc_pcia_id").ToString.Length > 0, .Item("cotc_pcia_id"), "0")
                    mCargarLocalidades()
                    cmbExclLoca.Valor = IIf(.Item("cotc_loca_id").ToString.Length > 0, .Item("cotc_loca_id"), "0")

                    If Not .IsNull("cotc_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("cotc_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If
                End With

                lblTitu.Text = "Registro Seleccionado: " + txtNyA.Text
                mSetearEditor("", False)
                mMostrarPanel(True)
                mShowTabs(1)
            End If
        End Sub
   Public Sub mEditarDatosTele(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrTele As DataRow
         hdnTeleId.Text = E.Item.Cells(1).Text
         ldrTele = mdsDatos.Tables(mstrContTel).Select("tect_id=" & hdnTeleId.Text)(0)

         With ldrTele
            cmbTiTel.Valor = .Item("tect_teti_id")
            txtTel.Valor = .Item("tect_tele")
            txtArea.Valor = .Item("tect_area_code")
            txtRefe.Valor = .Item("tect_refe")
         End With

         mSetearEditor(mstrContTel, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosMail(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrMail As DataRow
         hdnMailId.Text = E.Item.Cells(1).Text
         ldrMail = mdsDatos.Tables(mstrContMail).Select("mact_id=" & hdnMailId.Text)(0)

         With ldrMail
            txtEmail.Valor = .Item("mact_mail")
            txtMRefe.Valor = .Item("mact_refe")
         End With

         mSetearEditor(mstrContMail, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosEstu(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrEstu As DataRow
         hdnEstuId.Text = E.Item.Cells(1).Text
         ldrEstu = mdsDatos.Tables(mstrContEstud).Select("esct_id=" & hdnEstuId.Text)(0)

         With ldrEstu
            cmbEstuEst.Valor = .Item("esct_estu_id")
         End With
         mSetearEditor(mstrContEstud, False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosExpl(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrExpl As DataRow
         hdnExplId.Text = E.Item.Cells(1).Text
         ldrExpl = mdsDatos.Tables(mstrContExplot).Select("exct_id=" & hdnExplId.Text)(0)

         With ldrExpl
            cmbExplTipo.Valor = .Item("exct_exti_id")
            cmbExplPais.Valor = .Item("_PaisId")
            clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbExplProv, "S", IIf(cmbExplPais.Valor Is DBNull.Value, "-1", cmbExplPais.Valor))
            cmbExplProv.Valor = .Item("exct_pcia_id")
            clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbExplLoca, "N", IIf(cmbExplProv.Valor Is DBNull.Value, "-1", cmbExplProv.Valor))
            clsWeb.gCargarCombo(mstrConn, "localidades_aux_cargar " & cmbExclPcia.Valor, cmbExplLocaAux, "id", "descrip", "S", True)
            cmbExplLoca.Valor = .Item("exct_loca_id")
            txtExplRefe.Valor = .Item("exct_refe")
         End With

      mSetearEditor(mstrContExplot, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosFunc(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrFunc As DataRow
         hdnFuncId.Text = E.Item.Cells(1).Text
         ldrFunc = mdsDatos.Tables(mstrContFunc).Select("fuct_id=" & hdnFuncId.Text)(0)

         With ldrFunc
            cmbFuncFunc.Valor = .Item("fuct_func_id")
         End With
         mSetearEditor(mstrContFunc, False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosOcup(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrOcup As DataRow
         hdnOcupId.Text = E.Item.Cells(1).Text
         ldrOcup = mdsDatos.Tables(mstrContOcup).Select("occt_id=" & hdnOcupId.Text)(0)

         With ldrOcup
            cmbOcupOcup.Valor = .Item("occt_ocup_id")
         End With
         mSetearEditor(mstrContOcup, False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("cotc_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("cotc_trata") = txtTrat.Valor
         .Item("cotc_nyap") = txtNyA.Valor
         .Item("cotc_dire") = txtDire.Valor
         .Item("cotc_cpos") = txtCpos.Valor
         .Item("cotc_obse") = txtObse.Valor
         .Item("cotc_clcc_id") = cmbClasi.Valor
                .Item("cotc_orcc_id") = IIf(cmbOrg.Valor.Length > 0, cmbOrg.Valor, 0)
         .Item("cotc_pcia_id") = cmbExclPcia.Valor
         .Item("cotc_loca_id") = cmbExclLoca.Valor
         .Item("cotc_acti_id") = cmbActi.Valor
         .Item("cotc_baja_fecha") = DBNull.Value

         .Item("cotc_deud") = txtDeud.Valor
         .Item("cotc_ante_nume") = txtAnteNume.Valor
         .Item("cotc_ingr_fecha") = txtIngrFecha.Fecha
                .Item("cotc_naci_fecha") = IIf(txtNaciFecha.Fecha.Length > 0, txtNaciFecha.Fecha, DBNull.Value)
         .Item("cotc_doti_id") = cmbDoti.Valor
         .Item("cotc_docu_nume") = txtDocuNume.Valor
      End With

      Return mdsDatos
   End Function
   Private Sub mGuardarTelefonos()
      Dim ldrTele As DataRow

      If (cmbTiTel.SelectedItem.Text = "(Seleccione)") Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar el Tipo de Tel�fono.")
      End If

      If txtTel.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el N�mero de Tel�fono.")
      End If

      If hdnTeleId.Text = "" Then
         ldrTele = mdsDatos.Tables(mstrContTel).NewRow
         ldrTele.Item("tect_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrContTel), "tect_id")
      Else
         ldrTele = mdsDatos.Tables(mstrContTel).Select("tect_id=" & hdnTeleId.Text)(0)
      End If

      With ldrTele
         .Item("tect_teti_id") = cmbTiTel.Valor
         .Item("tect_tele") = txtTel.Valor
         .Item("tect_area_code") = txtArea.Valor
         .Item("tect_refe") = txtRefe.Valor
         .Item("_TipoTel") = cmbTiTel.SelectedItem.Text
      End With

      If (hdnTeleId.Text = "") Then
         mdsDatos.Tables(mstrContTel).Rows.Add(ldrTele)
      End If
   End Sub
   Private Sub mGuardarMails()
      Dim ldrMail As DataRow

      If txtEmail.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la direcci�n de e-mail.")
      End If

      If hdnMailId.Text = "" Then
         ldrMail = mdsDatos.Tables(mstrContMail).NewRow
         ldrMail.Item("mact_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrContMail), "mact_id")
      Else
         ldrMail = mdsDatos.Tables(mstrContMail).Select("mact_id=" & hdnMailId.Text)(0)
      End If

      With ldrMail
         .Item("mact_mail") = txtEmail.Valor
         .Item("mact_refe") = txtMRefe.Valor
      End With

      If (hdnMailId.Text = "") Then
         mdsDatos.Tables(mstrContMail).Rows.Add(ldrMail)
      End If
   End Sub
   Private Sub mGuardarEstudios()
      Dim ldrEstud As DataRow

      If cmbEstuEst.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Estudio.")
      End If

      If hdnEstuId.Text = "" Then
         ldrEstud = mdsDatos.Tables(mstrContEstud).NewRow
         ldrEstud.Item("esct_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrContEstud), "esct_id")
      Else
         ldrEstud = mdsDatos.Tables(mstrContEstud).Select("esct_id=" & hdnEstuId.Text)(0)
      End If

      With ldrEstud
         .Item("esct_estu_id") = cmbEstuEst.Valor
         .Item("_EstuDesc") = cmbEstuEst.SelectedItem.Text
      End With

      If (hdnEstuId.Text = "") Then
         mdsDatos.Tables(mstrContEstud).Rows.Add(ldrEstud)
      End If
   End Sub
   Private Sub mGuardarExplotaciones()
      Dim ldrExpl As DataRow

      If cmbExplTipo.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Tipo de Explotaci�n.")
      End If

      If cmbExplPais.Valor.ToString <> "" And cmbExplProv.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar una Provincia.")
      End If

      If hdnExplId.Text = "" Then
         ldrExpl = mdsDatos.Tables(mstrContExplot).NewRow
         ldrExpl.Item("exct_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrContExplot), "exct_id")
      Else
         ldrExpl = mdsDatos.Tables(mstrContExplot).Select("exct_id=" & hdnExplId.Text)(0)
      End If

      With ldrExpl
         .Item("exct_exti_id") = cmbExplTipo.Valor
         .Item("exct_loca_id") = cmbExplLoca.Valor
         .Item("exct_pcia_id") = cmbExplProv.Valor
         .Item("exct_refe") = txtExplRefe.Valor
         .Item("_ExplTipoDesc") = cmbExplTipo.SelectedItem.Text
         .Item("_PciaDesc") = cmbExplProv.SelectedItem.Text
         .Item("_LocaDesc") = cmbExplLoca.SelectedItem.Text
      End With

      If (hdnExplId.Text = "") Then
         mdsDatos.Tables(mstrContExplot).Rows.Add(ldrExpl)
      End If
   End Sub
   Private Sub mGuardarFunciones()
      Dim ldrFunc As DataRow

      If cmbFuncFunc.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Funci�n.")
      End If

      If hdnFuncId.Text = "" Then
         ldrFunc = mdsDatos.Tables(mstrContFunc).NewRow
         ldrFunc.Item("fuct_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrContFunc), "fuct_id")
      Else
         ldrFunc = mdsDatos.Tables(mstrContFunc).Select("fuct_id=" & hdnFuncId.Text)(0)
      End If

      With ldrFunc
         .Item("fuct_func_id") = cmbFuncFunc.Valor
         .Item("_FuncDesc") = cmbFuncFunc.SelectedItem.Text
      End With


      If (hdnFuncId.Text = "") Then
         mdsDatos.Tables(mstrContFunc).Rows.Add(ldrFunc)
      End If
   End Sub
   Private Sub mGuardarOcupaciones()
      Dim ldrOcup As DataRow

      If cmbOcupOcup.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Ocupaci�n.")
      End If

      If hdnOcupId.Text = "" Then
         ldrOcup = mdsDatos.Tables(mstrContOcup).NewRow
         ldrOcup.Item("occt_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrContOcup), "occt_id")
      Else
         ldrOcup = mdsDatos.Tables(mstrContOcup).Select("occt_id=" & hdnOcupId.Text)(0)
      End If

      With ldrOcup
         .Item("occt_ocup_id") = cmbOcupOcup.Valor
         .Item("_OcupDesc") = cmbOcupOcup.SelectedItem.Text
      End With

      If (hdnOcupId.Text = "") Then
         mdsDatos.Tables(mstrContOcup).Rows.Add(ldrOcup)
      End If
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar(grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnTelAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelAlta.Click
      ActualizarTelefono()
   End Sub
   Private Sub btnTelBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelBaja.Click
      Try
         mdsDatos.Tables(mstrContTel).Select("tect_id=" & hdnTeleId.Text)(0).Delete()
         grdTelefono.DataSource = mdsDatos.Tables(mstrContTel)
         grdTelefono.DataBind()
         mLimpiarTelefonos()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnTelModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelModi.Click
      ActualizarTelefono()
   End Sub
   Private Sub btnTelLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelLimp.Click
      mLimpiarTelefonos()
   End Sub

   Private Sub btnMailAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMailAlta.Click
      ActualizarMail()
   End Sub
   Private Sub btnMailBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMailBaja.Click
      Try
         mdsDatos.Tables(mstrContMail).Select("mact_id=" & hdnMailId.Text)(0).Delete()
         grdMail.DataSource = mdsDatos.Tables(mstrContMail)
         grdMail.DataBind()
         mLimpiarMail()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnMailModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMailModi.Click
      ActualizarMail()
   End Sub
   Private Sub btnMailLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMailLimp.Click
      mLimpiarMail()
   End Sub

   Private Sub btnEstuAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEstuAlta.Click
      ActualizarEstudios()
   End Sub
   Private Sub btnEstuBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEstuBaja.Click
      Try
         mdsDatos.Tables(mstrContEstud).Select("esct_id=" & hdnEstuId.Text)(0).Delete()
         grdEstudios.DataSource = mdsDatos.Tables(mstrContEstud)
         grdEstudios.DataBind()
         mLimpiarEstudios()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnEstuModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEstuModi.Click
      ActualizarEstudios()
   End Sub
   Private Sub btnEstuLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEstuLimp.Click
      mLimpiarEstudios()
   End Sub

   Private Sub btnExplAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExplAlta.Click
      ActualizarExplotaciones()
   End Sub
   Private Sub btnExplBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExplBaja.Click
      Try
         mdsDatos.Tables(mstrContExplot).Select("exct_id=" & hdnExplId.Text)(0).Delete()
         grdExplotaciones.DataSource = mdsDatos.Tables(mstrContExplot)
         grdExplotaciones.DataBind()
         mLimpiarExplotaciones()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnExplModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExplModi.Click
      ActualizarExplotaciones()
   End Sub
   Private Sub btnExplLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExplLimp.Click
      mLimpiarExplotaciones()
   End Sub

   Private Sub btnFuncAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFuncAlta.Click
      ActualizarFunciones()
   End Sub
   Private Sub btnFuncBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFuncBaja.Click
      Try
         mdsDatos.Tables(mstrContFunc).Select("fuct_id=" & hdnFuncId.Text)(0).Delete()
         grdFunciones.DataSource = mdsDatos.Tables(mstrContFunc)
         grdFunciones.DataBind()
         mLimpiarFunciones()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnFuncModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFuncModi.Click
      ActualizarFunciones()
   End Sub
   Private Sub btnFuncLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFuncLimp.Click
      mLimpiarFunciones()
   End Sub

   Private Sub btnOcupAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcupAlta.Click
      ActualizarOcupaciones()
   End Sub
   Private Sub btnOcupBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcupBaja.Click
      Try
         mdsDatos.Tables(mstrContOcup).Select("occt_id=" & hdnOcupId.Text)(0).Delete()
         grdOcupaciones.DataSource = mdsDatos.Tables(mstrContOcup)
         grdOcupaciones.DataBind()
         mLimpiarOcupaciones()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnOcupModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcupModi.Click
      ActualizarOcupaciones()
   End Sub
   Private Sub btnOcupLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOcupLimp.Click
      mLimpiarOcupaciones()
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTele.Click
      mShowTabs(2)
   End Sub
   Private Sub lnkMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMail.Click
      mShowTabs(3)
   End Sub
   Private Sub lnkEstudios_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEstudios.Click
      mShowTabs(4)
   End Sub
   Private Sub lnkExplotaciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkExplotaciones.Click
      mShowTabs(5)
   End Sub
   Private Sub lnkFunciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFunciones.Click
      mShowTabs(6)
   End Sub
   Private Sub lnkOcupaciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOcupaciones.Click
      mShowTabs(7)
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True

      panCabecera.Visible = False
      panTelefono.Visible = False
      panMail.Visible = False
      panEstudios.Visible = False
      panExplotaciones.Visible = False
      panFunciones.Visible = False
      panOcupaciones.Visible = False

      lnkCabecera.Font.Bold = False
      lnkTele.Font.Bold = False
      lnkMail.Font.Bold = False
      lnkEstudios.Font.Bold = False
      lnkExplotaciones.Font.Bold = False
      lnkFunciones.Font.Bold = False
      lnkOcupaciones.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Text = "Datos del Contacto: " & txtNyA.Text
         Case 2
            panTelefono.Visible = True
            lnkTele.Font.Bold = True
            lblTitu.Text = "Tel�fonos del Contacto: " & txtNyA.Text
         Case 3
            panMail.Visible = True
            lnkMail.Font.Bold = True
            lblTitu.Text = "Mails del Contacto: " & txtNyA.Text
         Case 4
            panEstudios.Visible = True
            lnkEstudios.Font.Bold = True
            lblTitu.Text = "Estudios del Contacto: " & txtNyA.Text
         Case 5
            panExplotaciones.Visible = True
            lnkExplotaciones.Font.Bold = True
            lblTitu.Text = "Explotaciones del Contacto: " & txtNyA.Text
         Case 6
            panFunciones.Visible = True
            lnkFunciones.Font.Bold = True
            lblTitu.Text = "Cargos del Contacto: " & txtNyA.Text
         Case 7
            panOcupaciones.Visible = True
            lnkOcupaciones.Font.Bold = True
            lblTitu.Text = "Ocupaciones del Contacto: " & txtNyA.Text
      End Select
   End Sub
#End Region
End Class
End Namespace
