<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EventosSociosReporte_pop" CodeFile="EventosSociosReporte_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Configuración Reporte de Socios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">		
			function mCargarCombos_OnChange()
			{
				if (document.all["cmbEvento"].value != '')
				{
					var sFiltro = '@even_id=' + document.all["cmbEvento"].value;
					LoadComboXML("eventos_entradas_por_evento_cargar", sFiltro, "cmbReporte", "R");
					document.all["cmbReporte"].selectedIndex = 0;
					document.all["hdnReporte"].value = 0;
					document.all["hdnReporteDesc"].value = document.all["cmbReporte"].item(document.all["cmbReporte"].selectedIndex).text;
				}
			}
			function cmbReporte_OnChange()
			{
				if (document.all["cmbReporte"].value == '')
				{
					document.all["hdnReporte"].value = 0;
					document.all["trOrdenaPor"].style.display = "none";
				}
				else
				{
					document.all["trOrdenaPor"].style.display = "inline";
					document.all["hdnReporte"].value = document.all["cmbReporte"].value;
					document.all["hdnReporteDesc"].value = document.all["cmbReporte"].item(document.all["cmbReporte"].selectedIndex).text;
				}
			}
			
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
							<TR>
								<TD vAlign="top" align="right" width="100%" colSpan="6">&nbsp;
									<asp:imagebutton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes\Close3.bmp"></asp:imagebutton></TD>
							</TR>
							<tr>
								<td colSpan="6">
									<table width="100%">
										<TR>
											<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
												<asp:Label id="lblEvento" runat="server" cssclass="titulo">Evento:</asp:Label>
											</TD>
											<TD style="HEIGHT: 24px" colSpan="2" height="24">
												<cc1:combobox class="combo" id="cmbEvento" runat="server" Width="90%" AceptaNull="false" onchange="mCargarCombos_OnChange();"></cc1:combobox>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
												<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>
											</TD>
											<TD style="HEIGHT: 24px" colSpan="2" height="24">
												<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
												<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>
											</TD>
											<TD style="HEIGHT: 24px" colSpan="2" height="24">
												<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>
											</TD>
										</TR>
										<TR>
											<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
												<asp:Label id="lblSeleccion" runat="server" cssclass="titulo">Seleccione:</asp:Label>
											</TD>
											<TD style="HEIGHT: 24px" colSpan="2" height="24">
												<CC1:combobox id="cmbReporte" runat="server" cssclass="combo" Width="240px" onchange="cmbReporte_OnChange();"></CC1:combobox>
											</TD>
										</TR>
										<TR id="trOrdenaPor" style="DISPLAY: none" runat="server">
											<TD style="WIDTH: 160px; HEIGHT: 24px" align="right">
												<asp:Label id="lblOrdenaPor" runat="server" cssclass="titulo">Ordena Por:</asp:Label>
											</TD>
											<TD style="HEIGHT: 24px" colSpan="2" height="24">
												<CC1:combobox id="cmbOrdenaPor" runat="server" cssclass="combo" Width="240px">
													<asp:ListItem Selected="True" Value="0">Etiqueta Desde</asp:ListItem>
													<asp:ListItem Value="1">Nro. Socio</asp:ListItem>
													<asp:ListItem Value="2">Nombre</asp:ListItem>
												</CC1:combobox>
											</TD>
										</TR>
									</table>
								</td>
							</tr>
							<TR>
								<TD style="HEIGHT: 8px" width="70" align="right"></TD>
								<TD style="HEIGHT: 8px" width="70" align="right" onclick="cmbReporte_OnChange();">
									<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
										IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
										ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnReporte" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnReporteDesc" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
			if (document.all["cmbEvento"].value != '')
			{
				mCargarCombos_OnChange();
			}
		</SCRIPT>
	</BODY>
</HTML>
