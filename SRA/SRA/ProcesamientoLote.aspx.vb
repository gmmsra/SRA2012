Imports System.Data.SqlClient


Namespace SRA


Partial Class ProcesamientoLote
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Productos
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         clsWeb.gInicializarControles(Me, mstrConn)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            Session(mSess(mstrTabla)) = Nothing

                btnProcesar.Attributes.Add("onclick", "return(mPorcPeti());")

            mSetearMaxLength()
            mEstablecerPerfil()
            mCargarCombos()
            mMostrarPanel(False)
         Else
            mdsDatos = Session(mSess(mstrTabla))
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip", "S")
      SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)

      clsWeb.gCargarRefeCmb(mstrConn, "implante_ctros", cmbCtroFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "productos_estados", cmbEstaFil, "T")
      cmbEstaFil.Items.Remove(cmbEstaFil.Items.FindByValue(SRA_Neg.Constantes.Estados.Productos_Vigente))

      clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0", cmbAsocFil, "id", "descrip", "T")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrProdLong As Object

      lstrProdLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtNombFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_nomb")
      txtCuigFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_cuig")
      txtNumeFil.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
      txtRPNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_rp")
      txtLaboNumeFil.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
      txtAsoNumeFil.MaxLength = txtRPNumeFil.MaxLength
   End Sub
   Private Sub mEstablecerPerfil()
      btnProcesar.Enabled = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.ProcesamientoLote_Procesar, String), (mstrConn), (Session("sUserId").ToString()))
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Private Sub mGuardarChechk()
      Dim dr As DataRow
      'Mantengo los check de las paguinas de la grilla
      For Each oDataItem As DataGridItem In grdDato.Items
         dr = mdsDatos.Tables(0).Select("prdt_id = " & oDataItem.Cells(2).Text)(0)
         dr.Item("chk") = DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked
      Next
      Session(mstrTabla) = mdsDatos
   End Sub
   Private Sub mChequearGrilla(ByVal pbooValor As Boolean)
      For Each ldrData As DataRow In mdsDatos.Tables(mstrTabla).Select("chk = " & (Not pbooValor).ToString)
         ldrData.Item("chk") = pbooValor
      Next
      Session(mstrTabla) = mdsDatos
      mConsultarGrilla()
   End Sub
   Private Sub mConsultarGrilla()
      grdDato.DataSource = mdsDatos
      grdDato.DataBind()
   End Sub
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         mGuardarChechk()

         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarGrilla()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         Dim lstrCmd As String
         Dim lstrPropClie As String
         Dim lstrPropCria As String

         If optClie.Checked Then
            lstrPropClie = usrPropClieFil.Valor.ToString
         Else
            lstrPropClie = ""
         End If
         If optCria.Checked Then
            lstrPropCria = usrPropCriaFil.Valor.ToString
         Else
            lstrPropCria = ""
         End If

         lstrCmd = ObtenerSql(Session("sUserId").ToString(), IIf(chkBusc.Checked, 1, 0), usrCriaFil.Valor.ToString, _
                              IIf(chkBaja.Checked, 1, 0), txtNombFil.Text, cmbRazaFil.Valor.ToString, _
                              txtNumeFil.Text, txtRPNumeFil.Text, cmbSexoFil.Valor.ToString, txtNaciFechaDesdeFil.Text, _
                              txtNaciFechaHastaFil.Text, cmbAsocFil.Valor.ToString, txtAsoNumeFil.Text, txtLaboNumeFil.Text, _
                              usrMadreFil.Valor.ToString, usrPadreFil.Valor.ToString, usrReceFil.Valor.ToString, txtObservacionFil.Valor.ToString, _
                              txtNumeDesdeFil.Valor.ToString, txtNumeHastaFil.Valor.ToString, _
                              txtApodoFil.Text, txtDonaFil.Text, 0, txtCuigFil.Text, _
                              txtTramNumeFil.Text, txtInscFechaDesdeFil.Text, txtInscFechaHastaFil.Text, _
                              txtRPDesdeFil.Text, txtRPHastaFil.Text, lstrPropClie, lstrPropCria, _
                              cmbEstaFil.Valor, cmbNaciFil.Valor)

         lstrCmd = lstrCmd & ",@procesar_lote='S'"
         mdsDatos = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)
         mdsDatos.Tables(0).TableName = mstrTabla
         Session(mSess(mstrTabla)) = mdsDatos

         mConsultarGrilla()

         mMostrarPanel(True)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mLimpiarFiltros()
      hdnValorId.Text = "-1"  'para que ignore el id que vino del control

      txtNumeFil.Text = ""
      txtTramNumeFil.Text = ""
      txtCuigFil.Text = ""
      txtNombFil.Text = ""
      txtRPDesdeFil.Text = ""
      txtRPHastaFil.Text = ""
      txtNumeDesdeFil.Text = ""
      txtNumeHastaFil.Text = ""
      txtApodoFil.Text = ""
      txtAsoNumeFil.Text = ""
      txtRPNumeFil.Text = ""
      txtLaboNumeFil.Text = ""
      txtCtrolFactuNumeFil.Text = ""
      txtNaciFechaDesdeFil.Text = ""
      txtNaciFechaHastaFil.Text = ""
      txtInscFechaDesdeFil.Text = ""
      txtInscFechaHastaFil.Text = ""
      usrPropClieFil.Limpiar()
      usrPropCriaFil.Limpiar()
      If usrCriaFil.Activo Then usrCriaFil.Limpiar()
      txtObservacionFil.Text = ""
      usrMadreFil.Limpiar()
      usrPadreFil.Limpiar()
      usrReceFil.Limpiar()

      chkBusc.Checked = False
      chkBaja.Checked = False

      If cmbRazaFil.Enabled Then
         cmbRazaFil.Limpiar()
      End If
      cmbAsocFil.Limpiar()
      cmbSexoFil.Limpiar()
      cmbCtroFil.Limpiar()
      cmbEstaFil.Limpiar()
      cmbNaciFil.Limpiar()

      mMostrarPanel(False)

   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panBotones.Visible = pbooVisi
      grdDato.Visible = pbooVisi
   End Sub
   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function
   Public Shared Function ObtenerSql(ByVal pstrUserId As String, ByVal pintBusc As Integer, ByVal pstrCriaId As String, _
      ByVal pintBaja As Integer, ByVal pstrNomb As String, ByVal pstrRaza As String, ByVal pstrSraNume As String, _
      ByVal pstrRPNume As String, ByVal pstrSexo As String, ByVal pstrNaciDesde As String, ByVal pstrNaciHasta As String, _
      ByVal pstrAsoc As String, ByVal pstrAsocNume As String, ByVal pstrLaboNume As String, ByVal pstrMadre As String, _
      ByVal pstrPadre As String, ByVal pstrRece As String, ByVal pstrObservacion As String, ByVal pstrSraNumDesde As String, _
      ByVal pstrSraNumHasta As String, ByVal pstrApodo As String, ByVal pstrDonaFil As String, ByVal pintInscrip As Integer, _
      Optional ByVal pstrCuig As String = "", Optional ByVal pstrTram As String = "", Optional ByVal pstrInscDesde As String = "", _
      Optional ByVal pstrInscHasta As String = "", Optional ByVal pstrRPDesde As String = "", Optional ByVal pstrRPHasta As String = "", _
      Optional ByVal pstrPropClie As String = "", Optional ByVal pstrPropCria As String = "", Optional ByVal pstrEsta As String = "", Optional ByVal pstrNacio As String = "") As String

      Dim lstrCmd As New StringBuilder

        lstrCmd.Append("exec p_rg_PorcLotes_productos_busq")
      lstrCmd.Append(" @buscar_en=" + pintBusc.ToString)
      lstrCmd.Append(" , @prdt_cria_id=" + clsSQLServer.gFormatArg(pstrCriaId, SqlDbType.Int))
      lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)

      lstrCmd.Append(" , @prdt_nomb=" + clsSQLServer.gFormatArg(pstrNomb, SqlDbType.VarChar))
      lstrCmd.Append(" , @prdt_apodo=" + clsSQLServer.gFormatArg(pstrApodo, SqlDbType.VarChar))
      lstrCmd.Append(" , @prdt_sra_nume=" + clsSQLServer.gFormatArg(pstrSraNume, SqlDbType.VarChar))
      lstrCmd.Append(" , @prdt_rp=" + clsSQLServer.gFormatArg(pstrRPNume, SqlDbType.VarChar))
      lstrCmd.Append(" , @prdt_cuig=" + clsSQLServer.gFormatArg(pstrCuig, SqlDbType.VarChar))
      lstrCmd.Append(" , @rp_desde=" + clsSQLServer.gFormatArg(pstrRPDesde, SqlDbType.VarChar))
      lstrCmd.Append(" , @rp_hasta=" + clsSQLServer.gFormatArg(pstrRPHasta, SqlDbType.VarChar))
      lstrCmd.Append(" , @prop_clie_id=" + clsSQLServer.gFormatArg(pstrPropClie, SqlDbType.Int))
      lstrCmd.Append(" , @prop_cria_id=" + clsSQLServer.gFormatArg(pstrPropCria, SqlDbType.Int))
      lstrCmd.Append(" , @esta_id=" + clsSQLServer.gFormatArg(pstrEsta, SqlDbType.Int))
      If pstrNacio <> "T" Then
         lstrCmd.Append(" , @prdt_ndad=" + clsSQLServer.gFormatArg(pstrNacio, SqlDbType.VarChar))
      End If
      If pstrSexo = "" Then
         lstrCmd.Append(" , @prdt_sexo=null")
      Else
         If pstrSexo.IndexOf(",") <> -1 Then
            pstrSexo = pstrSexo.Substring(pstrSexo.IndexOf(",") + 1)
         End If
         lstrCmd.Append(" , @prdt_sexo=" + clsSQLServer.gFormatArg(pstrSexo, SqlDbType.Int))
      End If

      lstrCmd.Append(" , @prdt_naci_fecha_desde=" + clsSQLServer.gFormatArg(pstrNaciDesde, SqlDbType.SmallDateTime))
      lstrCmd.Append(" , @prdt_naci_fecha_hasta=" + clsSQLServer.gFormatArg(pstrNaciHasta, SqlDbType.SmallDateTime))
      lstrCmd.Append(" , @prdt_insc_fecha_desde=" + clsSQLServer.gFormatArg(pstrInscDesde, SqlDbType.SmallDateTime))
      lstrCmd.Append(" , @prdt_insc_fecha_hasta=" + clsSQLServer.gFormatArg(pstrInscHasta, SqlDbType.SmallDateTime))
      lstrCmd.Append(" , @prdt_asoc_id=" + clsSQLServer.gFormatArg(pstrAsoc, SqlDbType.Int))
      lstrCmd.Append(" , @prdt_asoc_nume=" + clsSQLServer.gFormatArg(pstrAsocNume, SqlDbType.VarChar))
      lstrCmd.Append(" , @prta_nume=" + clsSQLServer.gFormatArg(pstrLaboNume, SqlDbType.Int))
      lstrCmd.Append(" , @madre_prdt_id=" + clsSQLServer.gFormatArg(pstrMadre, SqlDbType.Int))
      lstrCmd.Append(" , @padre_prdt_id=" + clsSQLServer.gFormatArg(pstrPadre, SqlDbType.Int))
      lstrCmd.Append(" , @rece_prdt_id=" + clsSQLServer.gFormatArg(pstrRece, SqlDbType.Int))
      lstrCmd.Append(" , @prdt_raza_id=" + clsSQLServer.gFormatArg(pstrRaza, SqlDbType.Int))
      lstrCmd.Append(" , @prdt_dona_nume=" + clsSQLServer.gFormatArg(pstrDonaFil, SqlDbType.Int))
      lstrCmd.Append(" , @prdo_refe=" + clsSQLServer.gFormatArg(pstrObservacion, SqlDbType.VarChar))
      lstrCmd.Append(" , @inscrip = " + pintInscrip.ToString)
      lstrCmd.Append(" , @tram_nume=" + clsSQLServer.gFormatArg(pstrTram, SqlDbType.Int))
      lstrCmd.Append(" , @prdt_sra_nume_desde=" + clsSQLServer.gFormatArg(pstrSraNumDesde, SqlDbType.Int))
      lstrCmd.Append(" , @prdt_sra_nume_hasta=" + clsSQLServer.gFormatArg(pstrSraNumHasta, SqlDbType.Int))
      lstrCmd.Append(" , @prdt_esta_id_noinc=" + clsSQLServer.gFormatArg(SRA_Neg.Constantes.Estados.Productos_Vigente, SqlDbType.VarChar))

      Return (lstrCmd.ToString)
   End Function
#End Region

#Region "Eventos de Controles"
   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub mProcesar()
      Try
          Dim lstrMsgError As String
          Dim lstrProdId As String
          Dim lstrRazaId As String
          Dim lstrSexo As String
          Dim lstrId As String
            'Dim dsVali As DataSet = New DataSet
            'Dim lstrFechaVali As String
          
          mGuardarChechk()

          If mdsDatos.Tables(0).Select().GetUpperBound(0) <> -1 Then
             If mdsDatos.Tables(0).Select("chk=True").GetUpperBound(0) = -1 Then
                Throw New AccesoBD.clsErrNeg("Debe indicar los productos a reprocesar.")
                Return
             End If
          Else
             Throw New AccesoBD.clsErrNeg("Debe indicar los productos a reprocesar.")
             Return
          End If

          'PROCESAR PRODUCTOS SELECCIONADOS.
          
          For Each dr As DataRow In mdsDatos.Tables(0).Select()
            If dr.Item("chk") Then
                lstrProdId = dr.Item("prdt_id")
                lstrRazaId = dr.Item("prdt_raza_id")
                If Not dr.IsNull("prdt_sexo") Then
                    lstrSexo = dr.Item("prdt_sexo")
                Else
                    lstrSexo = "null"
                End If
                If Not dr.IsNull("prdt_pdin_id") Then
                    lstrId = dr.Item("prdt_pdin_id")
                Else
                        lstrId = "-1"
                End If
                    'If Not dr.IsNull("prdt_naci_fecha") Then
                    '    lstrFechaVali = dr.Item("prdt_naci_fecha")
                    'Else
                    '    lstrFechaVali = ""
                    'End If
                    'dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), "W", lstrFechaVali, lstrFechaVali)
                    'Limpiar errores anteriores.
                    If (lstrId <> "-1") Then
                        ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, lstrId)
                        'Aplicar Reglas.
                        lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, lstrId, lstrRazaId, lstrSexo, Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), 0, lstrId)
                    End If
                End If
            Next

          grdDato.CurrentPageIndex = 0
          mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTodos.Click
      Try
         mChequearGrilla(True)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnNinguno_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNinguno.Click
      Try
         mChequearGrilla(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnProcesar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcesar.Click
      Try
         mProcesar()
      Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
   End Sub
#End Region

End Class
End Namespace
