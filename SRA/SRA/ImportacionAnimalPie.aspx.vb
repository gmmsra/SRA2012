' Dario 2014-05-28 Cambios de nombres asignacion de campos y volar planilla 1
' Dario 2014-06-23 se agrega campo nuevo tram_apob_asoc_fecha
' Dario 2014-10-10 se agrega en el evento de cambio de producto busqueda de padre madre y snps
Imports SRA_Entidad
Imports ReglasValida.Validaciones
Imports Entities


Namespace SRA


Partial Class ImportacionAnimalPie
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents usrCriadorFil As usrClieDeriv
    Protected WithEvents usrCriaProp As usrClieDeriv
    Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    Protected WithEvents rowDivProp As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblRazaFil As System.Web.UI.WebControls.Label
    '  Protected WithEvents cmbRazaFil As NixorControls.ComboBox
    Protected WithEvents cmbRazaCria As NixorControls.ComboBox

    Protected WithEvents lblSraNumeFil As System.Web.UI.WebControls.Label
    Protected WithEvents txtSraNumeFil As NixorControls.NumberBox
    Protected WithEvents lblCriaFil As System.Web.UI.WebControls.Label


    Protected WithEvents cmbCriaComp As NixorControls.ComboBox
    Protected WithEvents hdnCriaComp As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblPorcComp As System.Web.UI.WebControls.Label
    Protected WithEvents txtPorcComp As NixorControls.NumberBox
    Protected WithEvents lblObseComp As System.Web.UI.WebControls.Label
    Protected WithEvents txtObseComp As NixorControls.TextBoxTab
    Protected WithEvents btnModiComp As System.Web.UI.WebControls.Button
    Protected WithEvents btnLimpComp As System.Web.UI.WebControls.Button


    Protected WithEvents lblImpo As System.Web.UI.WebControls.Label
    Protected WithEvents lblNombComp As System.Web.UI.WebControls.Label
    Protected WithEvents lblRecuFecha As System.Web.UI.WebControls.Label
    Protected WithEvents txtRecuFecha As NixorControls.DateBox
    Protected WithEvents btnTeDenu As System.Web.UI.WebControls.Button
    Protected WithEvents txtTeDesc As NixorControls.TextBoxTab




    Protected WithEvents lblPelaAPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaAPeli As NixorControls.ComboBox
    Protected WithEvents lblPelaBPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaBPeli As NixorControls.ComboBox
    Protected WithEvents lblPelaCPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaCPeli As NixorControls.ComboBox
    Protected WithEvents lblPelaDPeli As System.Web.UI.WebControls.Label
    Protected WithEvents cmbPelaDPeli As NixorControls.ComboBox
    Protected WithEvents lblTipoRegistro As System.Web.UI.WebControls.Label
    Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblRazaCriadorLocal As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTramiteId As String = ""

    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Tramites
    Private mstrTablaRequisitos As String = SRA_Neg.Constantes.gTab_Tramites_Deta
    Private mstrTablaDocumentos As String = SRA_Neg.Constantes.gTab_Tramites_Docum
    Private mstrTablaObservaciones As String = SRA_Neg.Constantes.gTab_Tramites_Obse
    Private mstrTablaProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrTablaAsociaciones As String = SRA_Neg.Constantes.gTab_Asociaciones
    Private mstrTablaEspecies As String = SRA_Neg.Constantes.gTab_Especies
    Private mstrTablaTramitesProductos As String = SRA_Neg.Constantes.gTab_Tramites_Productos
    Private mstrTablaAnalisis As String = SRA_Neg.Constantes.gTab_ProductosAnalisis
    Private mstrTablaProductosRelaciones As String = SRA_Neg.Constantes.gTab_ProductosRelaciones
    Private mstrTablaProductosNumeros As String = SRA_Neg.Constantes.gTab_ProductosNumeros
    Private mstrTablaProductosDocum As String = SRA_Neg.Constantes.gTab_ProductosDocum


    Private mstrParaPageSize As Integer
    Private mdsDatosPadre As DataSet
    Private mdsDatosMadre As DataSet
    Private dsVali As DataSet
    Private mstrCmd As String
    Public mintProce As Integer
    Private mdsDatos As DataSet
    Private mdsDatosProd As DataSet
    Private mdsDatosRelaciones As DataSet
    Private mdsRelacion As DataSet
    Private dtRelation As DataTable

    Private mstrConn As String
    Private mstrTrapId As String
    Public mstrTitulo As String
    Public mintTtraId As Integer

    Dim NdadProd As String = "I"
    Dim NdadPadres As String = "E"

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                clsWeb.gInicializarControles(Me, mstrConn)

                cmbSNPS.Enabled = False
                Session("mitramite") = Nothing
                Session("sessProductos") = Nothing

                mstrTramiteId = Request.QueryString("Tram_Id")

                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mMostrarPanel(False)

                lnkRequ.Font.Bold = False
                lnkDocu.Font.Bold = False
                lnkObse.Font.Bold = False

                lnkRequ.Enabled = False
                lnkDocu.Enabled = False
                lnkObse.Enabled = False

                panRequ.Visible = False
                panDocu.Visible = False
                panObse.Visible = False

                If mstrTramiteId <> "" Then
                    hdnId.Text = mstrTramiteId
                    Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
                    cmbEsta.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                    cmbEstadoFil.Valor = oTramite.GetTramiteById(mstrTramiteId, "").Rows(0).Item("tram_esta_id").ToString()
                    mEditarTramite(mstrTramiteId)
                    mMostrarPanel(True)
                End If

                If mstrTramiteId = "" Then
                    mSetearMaxLength()
                    mSetearEventos()
                    mCargarCombos()
                    mMostrarPanel(False)
                End If
            Else
                mstrTramiteId = Request.QueryString("Tram_Id")
                If mstrTramiteId <> "" Then
                    Response.Write("<Script>window.close();</script>")
                End If
            End If

            mdsDatos = Session("mitramite")
            mdsDatosProd = Session("sessProductos")
            mstrTrapId = Session("mstrTrapId")




        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mCargarCombos()
        Dim lstrSRA As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Tr�mites) + ",@defa=1")
        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEstadoFil, "T", "@esti_id=" + CStr(SRA_Neg.Constantes.EstaTipos.EstaTipo_Tr�mites) + ",@defa=1")
        clsWeb.gCargarCombo(mstrConn, "rg_requisitos_cargar", cmbRequRequ, "id", "descrip_codi", "S")
        clsWeb.gCargarCombo(mstrConn, "importadores_cargar", cmbImpo, "id", "descrip_codi", "S")
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPais, "S", "@inc_defa = null", True) ' & IIf(mintTtraId = SRA_Neg.Constantes.TramitesTipos.Exportaci�n Or mintTtraId = SRA_Neg.Constantes.TramitesTipos.Exportaci�nSemen, "null", "1")
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPaisFil, "S", "", True)
    End Sub
    Private Sub mSetearEventos()
        btnModi.Attributes.Add("onclick", "return(mPorcPeti());")
        btnAlta.Attributes.Add("onclick", "return(mPorcPeti());")
        btnBaja.Attributes.Add("onclick", "if(!confirm('Est� dando de baja el Tr�mite, el cual ser� cerrado.�Desea continuar?')){return false;} else {mPorcPeti();}")
        ' btnTeDenu.Attributes.Add("onclick", "mCargarTE();return(false);")
        btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
        btnCriaCopropiedadComp.Attributes.Add("onclick", "mCriaCopropiedadComp();return false;")
        btnCriaCopropiedadVend.Attributes.Add("onclick", "mCriaCopropiedadVend();return false;")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nume")
        txtNroControl.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tram_nro_control")

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDocumentos)
        txtDocuObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trdo_refe")

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaObservaciones)
        txtObseObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "trao_obse")

    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mHabilitarDatosTE(ByVal pbooHabi As Boolean)

        txtRecuFecha.Enabled = pbooHabi

    End Sub
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdRequ.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdDocu.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdObse.PageSize = Convert.ToInt32(mstrParaPageSize)


        mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos
        mintProce = ReglasValida.Validaciones.Procesos.ImportacionProductos

        mstrTitulo = "Importaci�n de Productos"

        rowProp.Style.Add("display", "none")
        usrProductoPadre.FilSexo = False
        usrProductoPadre.Sexo = 1
        usrProductoPadre.FilRpNume = True
        usrProductoPadre.IgnogaInexistente = True

        usrProductoMadre.FilSexo = False
        usrProductoMadre.Sexo = 0
        usrProductoMadre.FilRpNume = True
        usrProductoMadre.IgnogaInexistente = True

        btnAgre.AlternateText = "Nueva " & mstrTitulo

        lblTituAbm.Text = mstrTitulo

    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub grdDato_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            If Page.IsPostBack Then

                mConsultar(True)
            Else
                mConsultar(False)

            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdRequ_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdRequ.EditItemIndex = -1
            If (grdRequ.CurrentPageIndex < 0 Or grdRequ.CurrentPageIndex >= grdRequ.PageCount) Then
                grdRequ.CurrentPageIndex = 0
            Else
                grdRequ.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarRequ()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdDocu_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDocu.EditItemIndex = -1
            If (grdDocu.CurrentPageIndex < 0 Or grdDocu.CurrentPageIndex >= grdDocu.PageCount) Then
                grdDocu.CurrentPageIndex = 0
            Else
                grdDocu.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarDocu()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdObse_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdObse.EditItemIndex = -1
            If (grdObse.CurrentPageIndex < 0 Or grdObse.CurrentPageIndex >= grdObse.PageCount) Then
                grdObse.CurrentPageIndex = 0
            Else
                grdObse.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarObse()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mConsultar(ByVal boolMostrarTodos As Boolean)

        Try
            Dim lstrCmd As String
            Dim dsDatos As New DataSet

            lstrCmd = "exec  tramites_animalEnPie_busq "
            If Not boolMostrarTodos Then
                If txtNumeFil.Valor <> 0 Then
                    lstrCmd += " @tram_nume=" + clsSQLServer.gFormatArg(txtNumeFil.Valor, SqlDbType.Int) + ","
                End If
                If usrProdFil.RazaId.ToString() <> "" Then
                    lstrCmd += " @tram_raza_id=" + clsSQLServer.gFormatArg(usrProdFil.RazaId, SqlDbType.Int) + ","
                Else
                    lstrCmd += " @tram_raza_id=null" + ","
                End If
            End If

            If Not usrProdFil Is Nothing Then
                If usrProdFil.Valor <> 0 Then
                    lstrCmd += " @prod_id=" + clsSQLServer.gFormatArg(usrProdFil.Valor, SqlDbType.Int) + ","
                End If
                lstrCmd += " @sexo=" + IIf(usrProdFil.cmbSexoProdExt.SelectedValue.ToString = "", _
                     "null", usrProdFil.cmbSexoProdExt.SelectedValue.ToString) + ","

                lstrCmd += " @prdt_nombre=" + IIf(usrProdFil.txtProdNombExt.Valor.ToString() = "", "null", _
                  "'" + usrProdFil.txtProdNombExt.Valor.ToString() + "'") + ","
            End If

            lstrCmd += " @sra_nume=" + clsSQLServer.gFormatArg(usrProdFil.txtSraNumeExt.Valor.ToString(), SqlDbType.Int) + ","
            lstrCmd += " @cria_id=" + clsSQLServer.gFormatArg(usrProdFil.CriaOrPropId, SqlDbType.Int) + ","
            lstrCmd += " @tram_fecha_desde=" + clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha) + ","
            lstrCmd += " @tram_fecha_hasta=" + clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha) + ","
            lstrCmd += " @tram_ttra_id=" + mintTtraId.ToString + ","
            lstrCmd += " @tram_esta_id=" + cmbEstadoFil.Valor.ToString + ","
            lstrCmd += " @tram_pais_id=" + cmbPaisFil.Valor.ToString + ","
            lstrCmd += " @mostrar_vistos=" + IIf(chkVisto.Checked, "1", "0")

            dsDatos = clsWeb.gCargarDataSet(mstrConn, lstrCmd)
            grdDato.Visible = True
            grdDato.DataSource = dsDatos
            grdDato.DataBind()


            grdDato.Visible = True

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
    Private Sub mConsultarRequ()
        grdRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
        grdRequ.DataBind()
    End Sub
    Private Sub mConsultarDocu()
        grdDocu.DataSource = mdsDatos.Tables(mstrTablaDocumentos)
        grdDocu.DataBind()
    End Sub
    Private Sub mConsultarObse()
        mdsDatos.Tables(mstrTablaObservaciones).DefaultView.Sort = "trao_fecha desc,_requ_desc"
        grdObse.DataSource = mdsDatos.Tables(mstrTablaObservaciones)
        grdObse.DataBind()
    End Sub


    Private Function mCrearDataSet(ByVal pstrId As String, ByVal pSpEstructura As String, _
                    ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                    ByVal pOpcionAdd As Boolean, ByVal TramiteId As String) As Boolean


        Dim ldsDatosTransf As New DataSet
        Dim tblDatos As New DataTable(pTableName)

        ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, TramiteId)
        tblDatos = ldsDatosTransf.Tables(0)
        tblDatos.TableName = pTableName

        dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
        Session("miTramite") = dsDatosNewDataSet

        Return True
    End Function

    Private Function mCrearDataSetProd(ByVal pstrId As String, ByVal pSpEstructura As String, _
                      ByVal pTableName As String, ByRef dsDatosNewDataSet As DataSet, _
                      ByVal pOpcionAdd As Boolean, ByVal TramiteId As String) As Boolean


        Dim ldsDatosTransf As New DataSet
        Dim tblDatos As New DataTable(pTableName)

        ldsDatosTransf = clsSQLServer.gObtenerEstruc(mstrConn, pSpEstructura, TramiteId)
        tblDatos = ldsDatosTransf.Tables(0)
        tblDatos.TableName = pTableName

        dsDatosNewDataSet.Tables.Add(tblDatos.Copy())
        Session("sessProductos") = dsDatosNewDataSet

        Return True
    End Function


    Public Sub mCrearDataSet(ByVal pstrId As String)
        mdsDatos = Nothing

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaRequisitos
        mdsDatos.Tables(2).TableName = mstrTablaDocumentos
        mdsDatos.Tables(3).TableName = mstrTablaObservaciones
        mdsDatos.Tables(4).TableName = mstrTablaTramitesProductos


        grdRequ.CurrentPageIndex = 0
        grdDato.CurrentPageIndex = 0
        grdObse.CurrentPageIndex = 0


        mConsultarRequ()
        mConsultarDocu()
        mConsultarObse()



        Session("mitramite") = mdsDatos
    End Sub

    Public Sub mCrearDataSetProd(ByVal pstrId As String)
        mdsDatosProd = Nothing

        mdsDatosProd = clsSQLServer.gObtenerEstruc(mstrConn, mstrTablaProductos, pstrId)

        mdsDatosProd.Tables(0).TableName = mstrTablaProductos
        mdsDatosProd.Tables(1).TableName = mstrTablaAnalisis
        mdsDatosProd.Tables(2).TableName = mstrTablaAsociaciones
        mdsDatosProd.Tables(3).TableName = mstrTablaProductosDocum
        ' solo para trmites esta en el sp 
        'mdsDatosProd.Tables(4).TableName = mstrTablaProductosRelaciones



        Session("sessProductos") = mdsDatosProd
    End Sub
#End Region

#Region "Seteo de Controles"
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mEditarTramite(E.Item.Cells(2).Text)
    End Sub

    Public Sub mEditarTramite(ByVal pstrTram As String)
        Try
            mLimpiar()

            Dim strProdId As String
            Dim dtProducto As DataTable
            Dim dtProductos As DataTable
            Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
            Dim intClienteId As Integer
            Dim intVendClienteId As Integer
            Dim intRazaId As Integer

            intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)
            intVendClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaVend.RazaId, usrCriaVend.Valor)

            hdnId.Text = clsFormatear.gFormatCadena(pstrTram)

            mCrearDataSet(hdnId.Text)

            dtProducto = oTramite.GetProductoByTramiteId(hdnId.Text, "")
            If dtProducto.Rows.Count > 0 Then
                strProdId = dtProducto.Rows(0).Item("prdt_id")
            End If
            mCrearDataSetProd(strProdId)

            Session("mitramite") = mdsDatos
            Session("sessProductos") = mdsDatosProd

            With mdsDatos.Tables(mstrTabla).Rows(0)
                hdnTramNro.Text = "Tr�mite Nro: " & .Item("tram_nume")
                lblTitu.Text = hdnTramNro.Text
                    hdnRazaId.Text = .Item("tram_raza_id")
                    txtInicFecha.Fecha = CDate(.Item("tram_inic_fecha")) '.ToString("dd/MM/yyyy")
                    If Not .IsNull("tram_fina_fecha") Then
                        txtFinaFecha.Fecha = CDate(.Item("tram_fina_fecha")) '.ToString("dd/MM/yyyy")
                    End If
                    If Not .IsNull("tram_pres_fecha") Then
                        txtFechaTram.Fecha = CDate(.Item("tram_pres_fecha")) '.ToString("dd/MM/yyyy")
                    End If
                    If Not .IsNull("tram_liber_fecha") Then
                        txtFechaLiberacionSanitaria.Fecha = CDate(.Item("tram_liber_fecha")) '.ToString("dd/MM/yyyy")
                    End If
                    If Not .IsNull("tram_apob_asoc_fecha") Then
                        Me.txtFechaApobAsoc.Fecha = .Item("tram_apob_asoc_fecha")
                    End If
                    cmbEsta.Valor = .Item("tram_esta_id").ToString
                    cmbImpo.Valor = .Item("tram_impr_id").ToString
                    cmbPais.Valor = .Item("tram_pais_id").ToString
                    txtFechaImportacion.Fecha = .Item("tram_oper_fecha").ToString
                txtNroControl.Text = ValidarNulos(.Item("tram_nro_control"), False)
                usrClieProp.Valor = .Item("tram_prop_clie_id")
                usrClieProp.Activo = .IsNull("tram_prop_clie_id")
                '  usrProd.Tramite = hdnId.Text
                usrProd.Valor = dtProducto.Rows(0).Item("prdt_id")
                'usrProd.cmbProdRazaExt.SelectedValue = dtProducto.Rows(0).Item("prdt_raza_id")
                'usrProd.Valor = hdnId.Text

                Me.CargaComboSNPS()
                If ValidarNulos(.Item("tram_ressnpsextr"), False) <> "0" Then
                    cmbSNPS.SelectedValue = ValidarNulos(.Item("tram_ressnpsextr"), False)
                End If
                usrCriaComp.Valor = ValidarNulos(.Item("tram_comp_cria_id"), False)
                usrCriaVend.Valor = ValidarNulos(.Item("tram_vend_cria_id"), False)
                usrProd.CriaId = usrCriaVend.Valor
                hdnCompOrig.Text = ValidarNulos(.Item("tram_comp_clie_id"), False)
                hdnVendOrig.Text = ValidarNulos(.Item("tram_vend_clie_id"), False)

                If Not .IsNull("tram_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & _
                    CDate(.Item("tram_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If

                If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count = 1 Then
                    With mdsDatos.Tables(mstrTablaTramitesProductos).Rows(0)
                        hdnDetaId.Text = .Item("trpr_id")
                    End With
                Else
                    hdnDetaId.Text = "0"
                End If

                Dim cont As Integer
                If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count > 1 Then
                    usrProd.Visible = False

                    lblTranPlan.Visible = True
                    hdnMultiProd.Text = "S"
                Else
                    usrProd.txtCodiExt.Valor = dtProducto.Rows(0).Item("prdt_ori_asoc_nume")
                    ' usrProd.usrProductoExt.txtCodiExtLab.Valor = dtProducto.Rows(0).Item("prdt_ori_asoc_nume_lab")
                    usrProd.txtProdNombExt.Valor = UCase(dtProducto.Rows(0).Item("prdt_nomb"))
                    usrProd.cmbProdAsocExt.Valor = ValidarNulos(dtProducto.Rows(0).Item("prdt_ori_asoc_id"), True)
                    usrProd.txtRPExt.Text = ValidarNulos(dtProducto.Rows(0).Item("prdt_rp"), True)
                    'usrProd..txtSraNumeExt.Valor = dtProducto.Rows(0).Item("prdt_sra_nume")
                    usrProd.cmbSexoProdExt.Valor = IIf(dtProducto.Rows(0).Item("prdt_sexo"), "1", "0")
                    usrProd.cmbSexoProdExt.SelectedValue = IIf(dtProducto.Rows(0).Item("prdt_sexo"), "1", "0")
                    usrProd.Sexo = IIf(dtProducto.Rows(0).Item("prdt_sexo"), "1", "0")
                    hdnSexo.Text = IIf(dtProducto.Rows(0).Item("prdt_sexo"), "1", "0")
                    usrProd.Visible = True
                    lblTranPlan.Visible = False
                    hdnMultiProd.Text = ""
                End If

                Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())
                Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
                usrProductoMadre.Valor = ValidarNulos(oProducto.GetMadreByProductoId(usrProd.Valor), False)
                usrProductoPadre.Valor = ValidarNulos(oProducto.GetPadreByProductoId(usrProd.Valor), False)

                Dim strCriadorPadre As String
                Dim strCriadorMadre As String
                strCriadorPadre = ValidarNulos(oCriador.GetCriadorIdByProductoId(usrProductoPadre.Valor), False)
                strCriadorMadre = ValidarNulos(oCriador.GetCriadorIdByProductoId(usrProductoMadre.Valor), False)
                usrProductoPadre.CriaId = strCriadorPadre
                usrProductoMadre.CriaId = strCriadorMadre

                If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count > 0 Then
                    With mdsDatos.Tables(mstrTablaTramitesProductos).Rows(0)
                        usrProductoMadre.cmbProdAsocExt.Valor = .Item("trpr_madre_asoc_id")
                        usrProductoPadre.cmbProdAsocExt.Valor = .Item("trpr_padre_asoc_id")

                    End With
                End If
                mSetearEditor(mstrTabla, False)
                mMostrarPanel(True)
            End With
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosRequ(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDeta As DataRow

            hdnRequId.Text = E.Item.Cells(1).Text
            ldrDeta = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)

            With ldrDeta
                cmbRequRequ.Valor = .Item("trad_requ_id")
                chkRequPend.Checked = .Item("trad_pend")
                lblRequManu.Text = .Item("_manu")
            End With

            mSetearEditor(mstrTablaRequisitos, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosDocu(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDoc As DataRow

            hdnDocuId.Text = E.Item.Cells(1).Text
            ldrDoc = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)

            With ldrDoc
                If .IsNull("trdo_path") Then
                    txtDocuDocu.Valor = ""
                    imgDelDocuDoc.Visible = False
                Else
                    imgDelDocuDoc.Visible = True
                    txtDocuDocu.Valor = .Item("trdo_path")
                End If
                txtDocuObse.Valor = .Item("trdo_refe")
            End With

            mSetearEditor(mstrTablaDocumentos, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosObse(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrObse As DataRow

            hdnObseId.Text = E.Item.Cells(1).Text
            ldrObse = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)

            With ldrObse
                cmbObseRequ.Valor = .Item("_trad_requ_id")
                txtObseObse.Valor = .Item("trao_obse")
                lblObseFecha.Text = CDate(.Item("trao_fecha")).ToString("dd/MM/yyyy")
            End With

            mSetearEditor(mstrTablaObservaciones, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiarFiltros()
        txtNumeFil.Text = ""
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        cmbEstadoFil.Limpiar()
        cmbPaisFil.Limpiar()
        txtNumeFil.Text = ""
        grdDato.Visible = False
        usrProdFil.Limpiar()

    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnRazaId.Text = ""
        lblBaja.Text = ""
        hdnMultiProd.Text = ""
        txtNroControl.Text = ""
        hdnTEId.Text = ""

        txtInicFecha.Fecha = Now
        txtFechaTram.Fecha = Now
            txtFechaImportacion.Fecha = "" ' System.DBNull.Value
            Me.txtFechaLiberacionSanitaria.Fecha = "" 'System.DBNull.Value
            Me.txtFechaApobAsoc.Fecha = "" 'System.DBNull.Value
        Me.cmbSNPS.Limpiar()
        txtFinaFecha.Text = ""

        cmbEsta.Limpiar()
        usrCriaComp.Limpiar()
        usrCriaVend.Limpiar()
        usrProd.Limpiar()
        lblTranPlan.Visible = False
        usrProd.Visible = True

        hdnCompOrig.Text = ""
        hdnVendOrig.Text = ""


        usrClieProp.Limpiar()
        usrClieProp.Activo = mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos
        cmbPais.Limpiar()
        cmbImpo.Limpiar()



        lblTitu.Text = ""
        hdnTramNro.Text = ""

        grdRequ.CurrentPageIndex = 0
        grdDocu.CurrentPageIndex = 0
        grdObse.CurrentPageIndex = 0
        usrProductoPadre.Limpiar()
        usrProductoMadre.Limpiar()

        mLimpiarRequ()
        mLimpiarDocu()
        mLimpiarObse()

        mCrearDataSet("")
        mCrearDataSetProd("")

        mSetearEditor(mstrTabla, True)
        mShowTabs(1)
        mCargarPlantilla()
    End Sub
    Private Sub mLimpiarRequ()
        hdnRequId.Text = ""
        cmbRequRequ.Limpiar()
        chkRequPend.Checked = False
        lblRequManu.Text = "S�"

        mSetearEditor(mstrTablaRequisitos, True)
    End Sub
    Private Sub mLimpiarDocu()
        hdnDocuId.Text = ""
        txtDocuObse.Valor = ""
        txtDocuDocu.Valor = ""
        imgDelDocuDoc.Visible = False

        mSetearEditor(mstrTablaDocumentos, True)
    End Sub


    Private Sub mLimpiarObse()
        hdnObseId.Text = ""
        cmbObseRequ.Limpiar()
        txtObseObse.Text = ""
        txtObseFecha.Fecha = Now

        mSetearEditor(mstrTablaObservaciones, True)
    End Sub


    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTabla
                btnAlta.Enabled = pbooAlta
                btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                'btnPedigree.Disabled = pbooAlta
                btnErr.Visible = Not (pbooAlta)
            Case mstrTablaRequisitos
                btnAltaRequ.Enabled = pbooAlta
                btnBajaRequ.Enabled = Not (pbooAlta)
                btnModiRequ.Enabled = Not (pbooAlta)
            Case mstrTablaDocumentos
                btnAltaDocu.Enabled = pbooAlta
                btnBajaDocu.Enabled = Not (pbooAlta)
                btnModiDocu.Enabled = Not (pbooAlta)
            Case mstrTablaObservaciones
                btnAltaObse.Enabled = pbooAlta
                btnBajaObse.Enabled = Not (pbooAlta)
                btnModiObse.Enabled = Not (pbooAlta)

        End Select
    End Sub
    Private Sub mAgregar()
        Try
            mstrTrapId = 3 ' hdnDatosPop.Text Dario 2014-05-28
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnAlta.Enabled = True
            mMostrarPanel(True)
            hdnDatosPop.Text = ""
            Session("mstrTrapId") = mstrTrapId
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        lnkCabecera.Font.Bold = True
        lnkRequ.Font.Bold = False
        lnkDocu.Font.Bold = False
        lnkObse.Font.Bold = False
        lnkVend.Font.Bold = False
        lnkComp.Font.Bold = False

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panCabecera.Visible = True
        panRequ.Visible = False
        panDocu.Visible = False
        panObse.Visible = False

        panLinks.Visible = pbooVisi
        btnAgre.Visible = Not panDato.Visible
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim ldsDatos As DataSet
            Dim ldsDatosProd As DataSet
            Dim lstrTramiteId As String
            Dim eError As New ErrorEntity
            Dim strTramiteProdId As String
            Dim strSexo As String
            Dim strRaza As String
            Dim ldrProd As DataRow
            Dim sProd As String
            Dim intTramProdId As Integer
            Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())

            hdnOperacion.Text = "A"
            ldsDatos = Nothing
            ldsDatosProd = Nothing


            Dim ValorActual As Integer
            ValorActual = SRA_Neg.Constantes.Estados.Tramites_Vigente


            cmbEsta.Valor = ValorActual.ToString()

            ' gsz 10/03/2015 se quito porque si es una alta no debe tomar nada de la sesion
            'mdsDatos = Session("mitramite")
            'mdsDatosProd = Session("sessProductos")

            ldsDatos = mGuardarDatos()

            If ldsDatosProd Is Nothing Then
                ldsDatosProd = mGuardarDatosProd()
            End If

            If Not mdsDatosProd Is Nothing Then
                ldsDatosProd = mdsDatosProd
            End If

            If Not ldsDatosProd Is Nothing Then
                sProd = Convert.ToString(usrProd.Valor.ToString())
                hdnProdId.Text = usrProd.Valor.ToString
            End If

            mdsDatosMadre = mGuardarDatosPadre(usrProductoMadre, usrProductoMadre.Valor.ToString, False)
            mdsDatosPadre = mGuardarDatosPadre(usrProductoPadre, usrProductoPadre.Valor.ToString, False)

            ldsDatosProd.AcceptChanges()

            Dim lTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), _
            mstrTabla, ldsDatos, ldsDatosProd, context, _
            False, mintTtraId, mdsDatosMadre, mdsDatosPadre, mdsDatosRelaciones)

            lstrTramiteId = lTramites.Alta()

            lblAltaId.Text = mObtenerTramite(lstrTramiteId)
            intTramProdId = oTramite.GetTramProdIdByPK(lstrTramiteId, hdnProdId.Text)

            strRaza = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_raza_id"), True)
            strTramiteProdId = ldsDatos.Tables("tramites_productos").Rows(0).Item("trpr_prdt_id").ToString()

            ldsDatos.Tables("tramites").Rows(0).Item("tram_id") = lstrTramiteId
            ldsDatos.Tables(mstrTablaTramitesProductos).Rows(0).Item("trpr_tram_id") = lstrTramiteId
            ldsDatos.Tables(mstrTablaTramitesProductos).Rows(0).Item("trpr_id") = intTramProdId
            ldsDatos.Tables("tramites").Rows(0).Item("tram_nume") = lblAltaId.Text

            lTramites.Tramites_updateByTramiteId(lstrTramiteId, lblAltaId.Text)
            lTramites.AplicarReglasTramite(lstrTramiteId, mintProce, mstrTablaTramitesProductos, "", strRaza, eError)
            If eError.errorDescripcion <> "" Then
                lTramites.GrabarEstadoRetenidaEnTramites(lstrTramiteId)
                Throw New AccesoBD.clsErrNeg("Se detectaron errores validatorios, por favor verifiquelo.")
            End If

            mConsultar(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                mConsultar(False)
            End If
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub

    Private Sub mModi()
        Try
            Dim ldsDatos As DataSet
            Dim ldsDatosProd As DataSet
            Dim eError As New ErrorEntity
            Dim booSexo As Boolean
            Dim strRaza As String
            Dim strTramiteProdId As String
            Dim strTramiteId As String
            Dim sProd As String

            hdnOperacion.Text = "M"

            ldsDatos = mGuardarDatos()

            If ldsDatosProd Is Nothing Then
                ldsDatosProd = mGuardarDatosProd()
            End If

            mdsDatos = Session("mitramite")
            mdsDatosProd = Session("sessProductos")

            If Not mdsDatosProd Is Nothing Then
                ldsDatosProd = mdsDatosProd
            End If

            If Not ldsDatosProd Is Nothing Then
                sProd = Convert.ToString(usrProd.Valor.ToString())
                hdnProdId.Text = usrProd.Valor.ToString
            End If

            mdsDatosMadre = mGuardarDatosPadre(usrProductoMadre, usrProductoMadre.Valor.ToString, False)
            mdsDatosPadre = mGuardarDatosPadre(usrProductoPadre, usrProductoPadre.Valor.ToString, False)

            Dim lTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString(), mstrTabla, _
            ldsDatos, ldsDatosProd, context, False, _
            mintTtraId, mdsDatosMadre, mdsDatosPadre, mdsDatosRelaciones)

            booSexo = ldsDatosProd.Tables("productos").Rows(0).Item("prdt_sexo")
            strRaza = ValidarNulos(ldsDatosProd.Tables("productos").Rows(0).Item("prdt_raza_id"), False)
            strTramiteProdId = ValidarNulos(ldsDatos.Tables("tramites_productos").Rows(0).Item("trpr_id"), True)
            strTramiteId = ValidarNulos(ldsDatos.Tables("tramites").Rows(0).Item("tram_id"), True)

            lTramites.Modi()

            lTramites.AplicarReglasTramite(hdnId.Text, mintProce, mstrTablaTramitesProductos, "", strRaza, eError)
            If eError.errorDescripcion <> "" Then
                lTramites.GrabarEstadoRetenidaEnTramites(hdnId.Text)
                Throw New AccesoBD.clsErrNeg("Se detectaron errores validatorios, por favor verifiquelo.")
            End If

            mLimpiar()
            mConsultar(False)
            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            If (ex.Message.ToLower() = "se detectaron errores validatorios, por favor verifiquelo.") Then
                mConsultar(False)
            End If
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            'Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
            'lobjGenerica.Baja(hdnId.Text)
            cmbEsta.SelectedValue = SRA_Neg.Constantes.Estados.Tramites_Baja

            mModi()

            grdDato.CurrentPageIndex = 0

            mConsultar(True)

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mValidarDatos(ByVal pbooCierre As Boolean, ByVal pbooCierreBaja As Boolean)
        Dim clie_id_Prod As Int32
        Dim drTramites As DataRow
        Dim cria_id_Prod As Int32

        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        'If ValidarNulos(txtNroControl.Valor, False) = 0 Then
        '    Throw New AccesoBD.clsErrNeg("El numero de Control debe ser distinto de cero.")
        'End If

        If IsDate(txtFechaImportacion.Text) Then
            If CDate(txtFechaImportacion.Text) > DateTime.Now Then
                Throw New AccesoBD.clsErrNeg("La Fecha de Importaci�n no puede ser superior a la fecha actual.")
            End If
        End If

        If IsDate(txtFechaLiberacionSanitaria.Text) Then
            If CDate(txtFechaLiberacionSanitaria.Text) > DateTime.Now Then
                Throw New AccesoBD.clsErrNeg("La Fecha de Liberaci�n Sanitaria no puede ser superior a la fecha actual.")
            End If
        End If
        If IsDate(txtFechaTram.Text) Then
            If CDate(txtFechaTram.Text) > DateTime.Now Then
                Throw New AccesoBD.clsErrNeg("La Fecha del Tr�mite no puede ser superior a la fecha actual.")
            End If
        End If


        If usrProd.cmbProdRazaExt.SelectedValue.ToString.Length = 0 Then
            Throw New AccesoBD.clsErrNeg("No hay ninguna raza seleccionada!")
        End If

        '' Dario 2014-10-31 comentado ahora no lo quierenal control de nro de control
        '' creo el objeto de productos 
        'Dim objProductosBusiness As New Business.Productos.ProductosBusiness
        '' ejecuto la validacion que me retorna un string con el mensaje de error si existe
        'Dim msg As String = objProductosBusiness.ValidaNumeroControlTramitesPropiedad(txtNroControl.Valor, usrCriaVend.Valor, usrCriaComp.Valor, usrProd.cmbProdRazaExt.SelectedValue, 1, 0, Convert.ToInt16(Common.CodigosServiciosTiposRRGG.ImportacionAnimalesPie))
        'If (Len(msg.Trim) > 0) Then
        '    Throw New AccesoBD.clsErrNeg(msg)
        'End If

        If Not pbooCierreBaja Then
            If usrCriaComp.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el comprador.")
            Else
                If usrCriaComp.Valor = "0" Then
                    Throw New AccesoBD.clsErrNeg("El Comprador no tiene datos de Criador.")
                End If

            End If

            'El vendedor puede ser vacio
            '
            'If usrCriaVend.Valor.ToString = "" Then
            '    Throw New AccesoBD.clsErrNeg("Debe indicar el Vendedor.")
            'Else
            '    If usrCriaVend.Valor = "0" Then
            '        Throw New AccesoBD.clsErrNeg("El Vendedor no tiene datos de Criador.")
            '    End If
            'End If

            If usrProd.cmbProdRazaExt.SelectedValue.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Raza del Producto para el Tr�mite.")
            End If
            If hdnSexo.Text <> "" Then
                usrProd.cmbSexoProdExt.Valor = hdnSexo.Text
            End If
            If usrProd.cmbSexoProdExt.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Sexo del Producto para el Tr�mite.")
            End If

            If clsSQLServer.gCampoValorConsul(mstrConn, "razas_consul @raza_id=" + usrProd.cmbProdRazaExt.SelectedValue.ToString, "raza_espe_id") <> SRA_Neg.Constantes.Especies.Peliferos Then
                If usrProd.txtProdNombExt.Valor.ToString = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Nombre del Producto para el Tr�mite.")
                End If
            End If

            If cmbPais.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el Pais de Origen del Animal.")

            End If
            ' 2015-06-15 Dario control de id vacio sino se rompe todo
            If usrProd.Valor.ToString() = "" Or usrProd.Valor.ToString() = "0" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el producto, si no existe delo de alta por el mas azul.")
            End If

            If usrProductoPadre.Valor.ToString() = "" Or usrProductoPadre.Valor.ToString() = "0" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Padre del producto")
            End If

            If usrProductoMadre.Valor.ToString() = "" Or usrProductoMadre.Valor.ToString() = "0" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar el Madre del producto")
            End If

            If usrProductoMadre.cmbProdAsocExt.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Asociaci�n de la Madre del Producto a Importar")
            End If

            If usrProductoPadre.cmbProdAsocExt.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Asociaci�n del Padre del Producto a Importar")
            End If


            If ValidarNulos(usrProd.Valor, False) = ValidarNulos(usrProductoPadre.Valor, False) Then
                Throw New AccesoBD.clsErrNeg("El producto a importar  y el Padre deben ser distintos.")
            End If

            If ValidarNulos(usrProd.Valor, False) = ValidarNulos(usrProductoMadre.Valor, False) Then
                Throw New AccesoBD.clsErrNeg("El producto a importar  y la Madre deben ser distintos.")
            End If

            Dim oTramite As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
            Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())

            If usrCriaComp.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el comprador.")
            Else
                If usrCriaComp.Valor = "0" Then
                    Throw New AccesoBD.clsErrNeg("El Comprador no tiene datos de Criador.")
                End If

            End If

            clie_id_Prod = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)

            If clie_id_Prod = 0 Then
                Throw New AccesoBD.clsErrNeg("El comprador no es cliente.")
            End If

            'If ValidarNulos(txtNroControl.Valor, False) = 0 Then
            '    MessageBox(Me, "El Nro de Control debe ser distinto de 0.")
            'End If

            'If Not oTramite.ValidarPaisesImportacionByRazaCriador(usrCriaVend.cmbCriaRazaExt.Valor, _
            '                               usrCriaVend.Valor, _
            '                               usrCriaComp.cmbCriaRazaExt.Valor, _
            '                               usrCriaComp.Valor) Then
            '    Throw New AccesoBD.clsErrNeg("El Importador debe ser Argentino y el vendedor debe ser Extranjero.")
            'End If

            'Dim oTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
            'drTramites = oTramites.GetCantFacturadoByNroControlRazaCriador(txtNroControl.Valor, _
            '              usrProductoPadre.cmbProdRazaExt.SelectedValue.ToString(), _
            '             usrCriaComp.Valor)

            'If IsNothing(drTramites) Then
            '    Throw New AccesoBD.clsErrNeg("No se encontraron comprobantes ,ni proforma con el Nro.Control " & txtNroControl.Valor)
            'End If

            If pbooCierre Then
                If cmbPais.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Pa�s para cerrar el Tr�mite.")
                End If

                If cmbImpo.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Importador para cerrar el Tr�mite.")
                End If
            End If
        End If
    End Sub
    Private Function mObtenerTramite(ByVal pstrId) As String
        Dim lstrId As String = ""
        Try
            mstrCmd = "exec tramites_consul " & pstrId
            Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), mstrCmd)
            While (dr.Read())
                lstrId = dr.GetValue(dr.GetOrdinal("tram_nume")).ToString().Trim()
            End While
            dr.Close()
            Return (lstrId)
        Catch ex As Exception
            Return (lstrId)
        End Try
    End Function

    Private Function mAplicarReglas(ByVal pstrTramId As String, ByVal pstrProdId As String) As Boolean
        Try
            Dim lstrId As String = ""
            Dim lstrRaza As String = ""
            Dim oTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())


            dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, mintProce.ToString(), _
            "W", txtFechaTram.Text, txtFechaTram.Text)

            lstrRaza = usrProd.cmbProdRazaExt.SelectedValue
            'Aplicar Reglas.
            hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, _
            mstrTablaTramitesProductos, pstrProdId, lstrRaza, "", Session("sUserId").ToString(), _
            True, ReglasValida.Validaciones.Procesos.ImportacionProductos.ToString(), pstrTramId, lstrId)

            If hdnMsgError.Text <> "" Then
                AccesoBD.clsError.gGenerarMensajes(Me, hdnMsgError.Text)
                oTramites.GrabarEstadoRetenidaEnTramites(pstrTramId)
                Return False
            Else
                mstrCmd = "exec " & mstrTabla & "_aprobar @tram_id = " & pstrTramId & _
                ", @audi_user=" & Session("sUserId").ToString()
                clsSQLServer.gExecute(mstrConn, mstrCmd)
                Return True
            End If

        Catch ex As Exception

            Dim oTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
            oTramites.GrabarEstadoRetenidaEnTramites(pstrTramId)
            clsError.gManejarError(Me, ex)
            Return False
        End Try
    End Function

    Private Function mGuardarDatos() As DataSet
        Dim ldsDatosProd As DataSet
        Dim ldsDatos As DataSet
        Dim lbooCierre As Boolean = False
        Dim lbooCierreBaja As Boolean = False
        Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
        Dim intClienteId As Integer
        Dim intVendClienteId As Integer

        ldsDatos = Session("mitramite")

        intClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaComp.RazaId, usrCriaComp.Valor)
        intVendClienteId = oCliente.GetClienteIdByRazaCriador(usrCriaVend.RazaId, usrCriaVend.Valor)

        If ldsDatos.Tables(mstrTablaRequisitos).Select("trad_pend = 1").GetUpperBound(0) < 0 And _
                grdRequ.Items.Count > 0 Then
            lbooCierre = True
        End If

        If cmbEsta.Valor.ToString = SRA_Neg.Constantes.Estados.Tramites_Baja Then
            lbooCierreBaja = True
        End If

        mValidarDatos(lbooCierre, lbooCierreBaja)

        If ldsDatos.Tables(mstrTabla).Rows.Count > 0 Then
            With mdsDatos.Tables(mstrTabla).Rows(0)
                .Item("tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("tram_ttra_id") = mintTtraId
                .Item("tram_raza_id") = usrProd.cmbProdRazaExt.SelectedValue
                .Item("tram_pres_fecha") = txtFechaTram.Fecha
                    .Item("tram_esta_id") = cmbEsta.Valor
                    If (cmbImpo.Valor.Trim.Length > 0) Then
                        .Item("tram_impr_id") = Convert.ToInt32(cmbImpo.Valor)
                        '.Item("tram_impr_id") = IIf(cmbImpo.Valor = "", DBNull.Value, Convert.ToInt32(cmbImpo.Valor))
                    End If
                    If (cmbPais.Valor.Length > 0) Then
                        '.Item("tram_pais_id") = IIf(cmbPais.Valor = "", DBNull.Value, Convert.ToInt32(cmbPais.Valor))
                        .Item("tram_pais_id") = Convert.ToInt32(cmbPais.Valor)
                    End If
                    .Item("tram_prop_clie_id") = intClienteId
                .Item("tram_vend_clie_id") = intVendClienteId
                .Item("tram_comp_clie_id") = intClienteId
                .Item("tram_vend_cria_id") = usrCriaVend.Valor
                .Item("tram_comp_cria_id") = usrCriaComp.Valor
                .Item("tram_nro_control") = ValidarNulos(txtNroControl.Text, False)
                    .Item("tram_oper_fecha") = IIf(txtFechaImportacion.Fecha = "", DBNull.Value, txtFechaImportacion.Fecha)
                    .Item("tram_liber_fecha") = IIf(txtFechaLiberacionSanitaria.Fecha = "", DBNull.Value, txtFechaLiberacionSanitaria.Fecha)
                    .Item("tram_apob_asoc_fecha") = IIf(txtFechaApobAsoc.Fecha = "", DBNull.Value, txtFechaApobAsoc.Fecha)
                    .Item("tram_ressnpsextr") = IIf(cmbSNPS.SelectedValue = "", DBNull.Value, cmbSNPS.SelectedValue)

                hdnCompOrig.Text = ValidarNulos(.Item("tram_comp_clie_id"), False)
                hdnVendOrig.Text = ValidarNulos(.Item("tram_vend_clie_id"), False)

                If .IsNull("tram_inic_fecha") Then
                    .Item("tram_inic_fecha") = Today
                End If

                .Item("tram_baja_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)
                .Item("tram_fina_fecha") = IIf(lbooCierreBaja, Today, DBNull.Value)

                If mNuevoProducto(usrProd, hdnId.Text) Then
                    'hdnMultiProd.Text <> "S"
                    ' usrProd.GenerarSRANume = lbooCierre
                    mdsDatosProd = mGuardarDatosProd()
                Else
                    If hdnOperacion.Text = "A" Then
                        Throw New AccesoBD.clsErrNeg("El producto fue importado anteriormente.")
                    End If
                End If
            End With
        End If

        Dim ldrDatos As DataRow

        'GUARDA LOS DATOS DE TRAMITES_PRODUCTOS
        If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count = 0 Then
            ldrDatos = mdsDatos.Tables(mstrTablaTramitesProductos).NewRow
            ldrDatos.Item("trpr_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaTramitesProductos), "trpr_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaTramitesProductos).Select()(0)
        End If

        With ldrDatos
            .Item("trpr_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("trpr_prdt_id") = usrProd.Valor
            .Item("trpr_asoc_id") = usrProd.cmbProdAsocExt.Valor
                .Item("trpr_padre_asoc_id") = IIf(usrProductoPadre.cmbProdAsocExt.Valor.Trim().Length = 0, "", usrProductoPadre.cmbProdAsocExt.Valor)
                .Item("trpr_madre_asoc_id") = IIf(usrProductoMadre.cmbProdAsocExt.Valor.Trim().Length = 0, "", usrProductoMadre.cmbProdAsocExt.Valor)
                .Item("trpr_audi_user") = Session("sUserId").ToString()
            .Item("trpr_baja_fecha") = DBNull.Value
            .Item("trpr_sexo") = usrProd.cmbSexoProdExt.ValorBool
            If mdsDatos.Tables(mstrTablaTramitesProductos).Rows.Count = 0 Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With

        mdsDatos.AcceptChanges()
        Return mdsDatos
    End Function

    Private Function mNuevoProducto(ByVal pusrProd As SRA.usrProductoExtranjero, ByVal pstrTramId As String) As Boolean
        Dim lstrNuevo As String
        Dim dtProducto As DataTable
        Dim ProductoNombre As String

        If pstrTramId = "" Then
            If pusrProd.Valor.ToString = "" Then
                Return (True)
            Else
                dtProducto = GetProductoById(pusrProd.Valor.ToString(), "")
                If dtProducto.Rows.Count > 0 Then

                    If ValidarImportados() Then
                        Return (True)
                    Else
                        Return (False)
                    End If
                End If
                Return (False)
            End If
        Else
            If pusrProd.Valor.ToString = "" Then
                Return (True)
            Else
                mstrCmd = "producto_tramite_determinar @prdt_id=" & pusrProd.Valor.ToString & _
                ",@tram_id=" & pstrTramId

                lstrNuevo = clsSQLServer.gCampoValorConsul(mstrConn, mstrCmd, "resul")
                If lstrNuevo = "S" Then
                    Return (True)
                Else
                    Return (False)
                End If
            End If
        End If
    End Function

    Public Function mGuardarDatosPadre(ByVal pusrProd As SRA.usrProductoExtranjero, _
    ByVal pstrId As String, ByVal pbooCierre As Boolean) As DataSet

        Dim ldsDatos As DataSet
        Dim lstrId As String
        Dim ldrProd As DataRow
        Dim ldrNume As DataRow

        ldsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, SRA_Neg.Constantes.gTab_Productos, "", "")

        With ldsDatos
            .Tables(0).TableName = SRA_Neg.Constantes.gTab_Productos
            .Tables(1).TableName = SRA_Neg.Constantes.gTab_ProductosNumeros

        End With

        If ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select.GetUpperBound(0) = -1 Then
            ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).NewRow
            ldrProd.Table.Rows.Add(ldrProd)
        Else
            ldrProd = ldsDatos.Tables(SRA_Neg.Constantes.gTab_Productos).Select()(0)
        End If
        Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())


        'productos
        With ldrProd
            lstrId = clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)
            .Item("prdt_id") = lstrId
            .Item("prdt_raza_id") = ValidarNulos(pusrProd.cmbProdRazaExt.SelectedValue, True)
            .Item("prdt_sexo") = pusrProd.cmbSexoProdExt.ValorBool
            '.Item("prdt_sra_nume") = ValidarNulos(pusrProd.txtSraNumeExt.Text, True)
            If pusrProd.CriaId <> "" Then
                .Item("prdt_cria_id") = pusrProd.CriaId
            End If

            .Item("prdt_nomb") = UCase(pusrProd.txtProdNombExt.Valor)
                .Item("prdt_tran_fecha") = IIf(txtFechaImportacion.Fecha = "", DBNull.Value, txtFechaImportacion.Fecha)
                .Item("prdt_rp") = IIf(Trim(pusrProd.txtRPExt.Text) = "", DBNull.Value, Trim(pusrProd.txtRPExt.Text))
            .Item("prdt_ndad") = IIf(.Item("prdt_ndad").ToString = "", "E", .Item("prdt_ndad"))
            .Item("prdt_ori_asoc_id") = pusrProd.cmbProdAsocExt.Valor
            .Item("prdt_ori_asoc_nume") = pusrProd.txtCodiExt.Valor
            '  .Item("prdt_ori_asoc_nume_lab") = pusrProd.txtCodiExtLab.Valor
            .Item("generar_numero") = pbooCierre
        End With


        '''productos_numeros
        'If pusrProd.txtCodiExt.Text <> "" Then
        '    If mdsDatosProd.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).Select.GetUpperBound(0) = -1 Then
        '        ldrNume = mdsDatosProd.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).NewRow
        '        ldrNume.Table.Rows.Add(ldrNume)
        '    Else
        '        ldrNume = ldsDatos.Tables(SRA_Neg.Constantes.gTab_ProductosNumeros).Select()(0)
        '    End If

        '    With ldrNume
        '        .Item("ptnu_id") = clsSQLServer.gFormatArg("", SqlDbType.Int)
        '        .Item("ptnu_prdt_id") = lstrId
        '        .Item("ptnu_asoc_id") = pusrProd.cmbProdAsocExt.Valor
        '        .Item("ptnu_nume") = pusrProd.txtCodiExt.Text
        '    End With
        'End If

        Return ldsDatos
    End Function

    'REQUISISTOS
    Private Sub mActualizarRequ(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosRequ(pbooAlta)
            mLimpiarRequ()
            mConsultarRequ()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarRequ(ByVal pbooAlta As Boolean)
        If cmbRequRequ.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
        End If
        If pbooAlta Then
            If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Requisito existente.")
            End If
        End If
    End Sub
    Private Sub mValidarModiRequ(ByVal pbooAlta As Boolean)
        If grdRequ.Items.Count = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Requisito.")
        End If
        If pbooAlta Then
            If mdsDatos.Tables(mstrTablaRequisitos).Select("trad_baja_fecha is null and trad_requ_id=" & cmbRequRequ.Valor.ToString & IIf(hdnRequId.Text = "", "", " and trad_id<>" & hdnRequId.Text)).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Requisito existente.")
            End If
        End If
    End Sub
    Private Sub mGuardarDatosRequ(ByVal pbooAlta As Boolean)
        Dim ldrDatos As DataRow
        If hdnOperacion.Text = "M" Then
            mValidarModiRequ(pbooAlta)
        Else
            mValidarRequ(pbooAlta)
        End If

        If hdnRequId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
            ldrDatos.Item("trad_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaRequisitos), "trad_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0)
        End If

        With ldrDatos
            .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("trad_requ_id") = cmbRequRequ.Valor
            .Item("trad_pend") = chkRequPend.Checked
            .Item("trad_obli") = True
            .Item("trad_manu") = IIf(.IsNull("trad_manu"), True, .Item("trad_manu"))
            .Item("trad_baja_fecha") = DBNull.Value
            .Item("trad_audi_user") = Session("sUserId").ToString()
            .Item("_requ_desc") = cmbRequRequ.SelectedItem.Text
            .Item("_pend") = IIf(chkRequPend.Checked, "S�", "No")
            .Item("_manu") = "S�"
            .Item("_estado") = "Activo"

            If hdnRequId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With

    End Sub
    'DOCUMENTOS
    Private Sub mActualizarDocu()
        Try
            mGuardarDatosDocu()
            mLimpiarDocu()
            mConsultarDocu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDocu()
        If txtDocuDocu.Valor.ToString = "" And filDocuDocu.Value = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el Documento.")
        End If

        If txtDocuObse.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
        End If
    End Sub
    Private Sub mGuardarDatosDocu()
        Dim ldrDatos As DataRow
        Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_tram_docu_path")

        mValidarDocu()

        If hdnDocuId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).NewRow
            ldrDatos.Item("trdo_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocumentos), "trdo_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0)
        End If

        With ldrDatos
            .Item("trdo_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

            If filDocuDocu.Value <> "" Then
                .Item("trdo_path") = filDocuDocu.Value
            Else
                .Item("trdo_path") = txtDocuDocu.Valor
            End If
            If Not .IsNull("trdo_path") Then
                .Item("trdo_path") = .Item("trdo_path").Substring(.Item("trdo_path").LastIndexOf("\") + 1)
            End If
            .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocumentos + "_" + Session("MilSecc") + "_" + Replace(.Item("trdo_id"), "-", "m") + "__" + .Item("trdo_path")
            .Item("trdo_refe") = txtDocuObse.Valor
            .Item("trdo_baja_fecha") = DBNull.Value
            .Item("trdo_audi_user") = Session("sUserId").ToString()
            .Item("_estado") = "Activo"

            SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filDocuDocu)

            If hdnDocuId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With
    End Sub
    'OBSERVACIONES
    Private Sub mActualizarObse()
        Try
            mGuardarDatosObse()
            mLimpiarObse()
            mConsultarObse()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarObse()
        If txtObseObse.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Observaci�n.")
        End If
        If txtObseFecha.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Fecha de la Observaci�n.")
        End If
    End Sub
    Private Sub mGuardarDatosObse()
        Dim ldrDatos As DataRow
        mValidarObse()

        If hdnObseId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).NewRow
            ldrDatos.Item("trao_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaObservaciones), "trao_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0)
        End If

        With ldrDatos
            .Item("trao_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            If cmbObseRequ.Valor.ToString = "" Then
                .Item("trao_trad_id") = DBNull.Value
            Else
                .Item("trao_trad_id") = mdsDatos.Tables(mstrTablaRequisitos).Select("trad_requ_id=" + cmbObseRequ.Valor.ToString)(0).Item("trad_id")
            End If
            .Item("_requ_desc") = IIf(cmbObseRequ.Valor.ToString = "", "COMENTARIO GENERAL", cmbObseRequ.SelectedItem.Text)
            .Item("_trad_requ_id") = IIf(cmbObseRequ.Valor.ToString = "", "0", cmbObseRequ.Valor)
            .Item("trao_obse") = txtObseObse.Valor
            .Item("trao_fecha") = txtObseFecha.Fecha
            .Item("trao_audi_user") = Session("sUserId").ToString()

            If hdnObseId.Text = "" Then
                .Table.Rows.Add(ldrDatos)
            End If
        End With
    End Sub


    'PRODUCTOS
    Private Function mGuardarDatosProd() As DataSet
        Dim ldsDatosProd As DataSet
        Dim ldrDatosProd As DataRow
        Dim lbooCierre As Boolean = False
        Dim lbooCierreBaja As Boolean = False
        Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())

        'Dim intClienteId As Integer
        'Dim intVendClienteId As Integer

        mValidarDatos(lbooCierre, lbooCierreBaja)

        hdnProdId.Text = usrProd.Valor

        If mdsDatosProd.Tables(mstrTablaProductos).Select.GetUpperBound(0) = -1 Then
            ldrDatosProd = mdsDatosProd.Tables(mstrTablaProductos).NewRow
            ldrDatosProd.Table.Rows.Add(ldrDatosProd)
        Else
            ldrDatosProd = mdsDatosProd.Tables(mstrTablaProductos).Select()(0)
        End If

        With ldrDatosProd
            If hdnProdId.Text <> "" Then
                .Item("prdt_id") = clsSQLServer.gFormatArg(hdnProdId.Text, SqlDbType.Int)
            Else
                .Item("prdt_id") = clsSQLServer.gFormatArg(ldrDatosProd.ItemArray(0).ToString(), SqlDbType.Int)
            End If
            .Item("prdt_raza_id") = usrProd.cmbProdRazaExt.SelectedValue
            .Item("prdt_sexo") = usrProd.cmbSexoProdExt.ValorBool
            .Item("prdt_nomb") = usrProd.txtProdNombExt.Valor
            .Item("prdt_rp") = Trim(usrProd.txtRPExt.Text)
            ' .Item("prdt_rp_extr") = txtRPExtr.Valor
            .Item("prdt_ndad") = "I"
            .Item("prdt_ori_asoc_id") = usrProd.cmbProdAsocExt.Valor
            .Item("prdt_ori_asoc_nume") = usrProd.txtCodiExt.Valor
            '.Item("prdt_prop_clie_id") = _
            'oCliente.GetClienteIdByRazaCriador(usrProd.RazaId, usrProd.CriaOrPropId)
            .Item("generar_numero") = True
            If usrProd.CriaId <> "" Then
                .Item("prdt_cria_id") = usrProd.CriaId
            End If
        End With

        mdsDatosProd.AcceptChanges()

        ldsDatosProd = mdsDatosProd

        ldsDatosProd.AcceptChanges()

        mGuardarAnalisis(ldsDatosProd, ldrDatosProd)
        'If mbooMostrarNumeroExtranjero Then
        'mGuardarAsociaciones(ldsDatosProd, ldrDatosProd.Item("prdt_id").ToString, usrProd)
        'End If

        Dim mbooMostrarPadre As Boolean = True
        Dim mbooMostrarMadre As Boolean = True


        Dim ldrRela As DataRow 'FILA DEL PRODUCTO RELACIONADO
        Dim ldsDatos As DataSet


        mdsDatosRelaciones = mCrearDataSetRelaciones(hdnProdId.Text)

        If mdsDatosRelaciones Is Nothing Then
            mdsRelacion = mCrearDataSetRelaciones("")
        End If

        If mbooMostrarPadre Then mGuardarRelacionados(ldsDatosProd, hdnProdId.Text, CType(SRA_Neg.Constantes.ProductosRelacion.Padre, String), usrProductoPadre)
        If mbooMostrarMadre Then mGuardarRelacionados(ldsDatosProd, hdnProdId.Text, CType(SRA_Neg.Constantes.ProductosRelacion.Madre, String), usrProductoMadre)

        mdsDatosRelaciones = mdsRelacion.Copy()

        If mbooMostrarPadre Then
            If mdsRelacion.Tables(mstrTablaProductosRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Padre).GetUpperBound(0) > -1 Then
                mGuardarAsociaciones(mdsRelacion, mdsRelacion.Tables(mstrTablaProductosRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Padre)(0).Item("ptre_supe_prdt_id").ToString, usrProductoPadre)
            Else
                If ldsDatosProd.Tables(mstrTablaProductos).Select("prdt_sexo = true").Length > 0 Then
                    mGuardarAsociaciones(ldsDatosProd, ldsDatosProd.Tables(mstrTablaProductos).Select("prdt_sexo = true")(0).Item("prdt_id").ToString, usrProductoPadre)
                End If

            End If
        End If

        If mbooMostrarMadre Then
            If mdsRelacion.Tables(mstrTablaProductosRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Madre).GetUpperBound(0) > -1 Then
                mGuardarAsociaciones(mdsRelacion, mdsRelacion.Tables(mstrTablaProductosRelaciones).Select("ptre_reti_id=" & SRA_Neg.Constantes.ProductosRelacion.Madre)(0).Item("ptre_supe_prdt_id").ToString, usrProductoMadre)
            Else
                If ldsDatosProd.Tables(mstrTablaProductos).Select("prdt_sexo = false").Length > 0 Then
                    mGuardarAsociaciones(ldsDatosProd, ldsDatosProd.Tables(mstrTablaProductos).Select("prdt_sexo = false")(0).Item("prdt_id").ToString, usrProductoMadre)
                End If

            End If
        End If

        ldsDatosProd.AcceptChanges()

        Return (ldsDatosProd)
    End Function
    Private Sub mGuardarAsociaciones(ByVal pdtsDatos As DataSet, ByVal pstrPrdtId As String, _
   ByVal pusrProd As usrProductoExtranjero)
        Dim ldrDatos As DataRow
        Dim lbooAlta As Boolean = False

        'If pusrProd.cmbProdAsocExt.Valor.ToString <> "" And pusrProd.txtCodiExt.Valor.ToString <> "" Then
        '    'Borro los existentes
        '    If pusrProd.Valor.ToString <> "" Then
        '        If pdtsDatos.Tables(mstrTablaAsociaciones).Select("ptnu_prdt_id=" & pusrProd.Valor.ToString & " and ptnu_asoc_id=" & pusrProd.cmbProdAsocExt.Valor.ToString).GetUpperBound(0) > -1 Then
        '            ldrDatos = pdtsDatos.Tables(mstrTablaAsociaciones).Select("ptnu_prdt_id=" & pusrProd.Valor.ToString & " and ptnu_asoc_id=" & pusrProd.cmbProdAsocExt.Valor.ToString)(0)
        '        End If
        '    Else
        '        lbooAlta = True
        '        ldrDatos = pdtsDatos.Tables(mstrTablaAsociaciones).NewRow
        '        ldrDatos.Item("ptnu_id") = clsSQLServer.gObtenerId(pdtsDatos.Tables(mstrTablaAsociaciones), "ptnu_id")
        '    End If
        '    If Not ldrDatos Is Nothing Then
        '        With ldrDatos
        '            .Item("ptnu_asoc_id") = pusrProd.cmbProdAsocExt.Valor
        '            .Item("ptnu_prdt_id") = pstrPrdtId
        '            .Item("ptnu_nume") = pusrProd.txtCodiExt.Valor

        '            If lbooAlta Then
        '                .Table.Rows.Add(ldrDatos)
        '            End If
        '        End With
        '    End If

        'End If
    End Sub
    Private Function mCrearDataSetRelaciones(ByVal pstrId As String) As DataSet
        Dim ldsDatos As DataSet

        If pstrId = "" Then
            pstrId = "-1"
        End If

        ldsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTablaProductos, pstrId, "@tramite='S'")

        With ldsDatos
            '.Tables(0).TableName = mstrTabla
            .Tables(0).TableName = mstrTablaProductos
            .Tables(1).TableName = mstrTablaAnalisis
            .Tables(2).TableName = mstrTablaAsociaciones
            .Tables(3).TableName = mstrTablaProductosDocum
            .Tables(4).TableName = mstrTablaProductosRelaciones
        End With

        Return ldsDatos
    End Function

    Private Sub mGuardarRelacionados(ByVal pdsDatos As DataSet, ByVal pHijo As String, _
    ByVal pstrRetiId As String, ByVal pusrRela As usrProductoExtranjero)

        Dim ldrRela As DataRow 'FILA DEL PRODUCTO RELACIONADO
        Dim ldsDatos As DataSet

        Dim strRela As String
        strRela = pHijo 'pusrRela.Valor.ToString()

        'mdsDatosRelaciones = mCrearDataSetRelaciones(strRela)

        If Not pHijo Is Nothing Then
            With mdsDatosRelaciones.Tables(mstrTablaProductosRelaciones).Select("ptre_reti_id=" & pstrRetiId)
                If .GetUpperBound(0) > -1 Then
                    With mdsDatosRelaciones.Tables(mstrTablaProductosRelaciones).Select("ptre_reti_id=" & pstrRetiId)(0)
                        .Item("ptre_reti_id") = pstrRetiId
                        .Item("ptre_prdt_id") = pHijo
                        .Item("ptre_supe_prdt_id") = pusrRela.Valor.ToString
                    End With
                Else
                    ' esto para nuevo
                    Dim ldrPtre As DataRow 'FILA DE PRODUCTOS RELACIONES
                    ldrPtre = mdsDatosRelaciones.Tables(mstrTablaProductosRelaciones).NewRow
                    ldrPtre.Item("ptre_id") = clsSQLServer.gObtenerId(ldrPtre.Table, "ptre_id")
                    ldrPtre.Item("ptre_reti_id") = pstrRetiId
                    ldrPtre.Item("ptre_prdt_id") = pHijo
                    ldrPtre.Item("ptre_supe_prdt_id") = pusrRela.Valor.ToString

                    mdsDatosRelaciones.Tables(mstrTablaProductosRelaciones).Rows.Add(ldrPtre)
                    'mdsRelacion.Tables(mstrTablaProductosRelaciones).ImportRow(ldrPtre)
                End If
            End With
        End If

        mdsRelacion = mdsDatosRelaciones.Copy()
        mdsDatosProd = pdsDatos
        mdsDatos.AcceptChanges()
        mdsDatosRelaciones.AcceptChanges()
        mdsDatos.AcceptChanges()
    End Sub

    Private Sub mGuardarAnalisis(ByVal pdsDatos As DataSet, ByVal pdrHijo As DataRow)

        'If pdsDatos.Tables(mstrTablaAnalisis).Rows.Count > 0 Then
        '    'Modifico el �ltimo analisi
        '    pdsDatos.Tables(mstrTablaAnalisis).DefaultView.Sort() = "prta_fecha DESC"
        '    With pdsDatos.Tables(mstrTablaAnalisis).Select()(0)
        '        .Item("prta_nume") = txtNroAnal.Valor
        '        .Item("prta_ares_id") = cmbAres.Valor
        '        '  .Item("prta_tipo") = cmbTipoAnal.Valor
        '        .Item("prta_ndad") = IIf(NdadProd = "", DBNull.Value, NdadProd)
        '    End With
        'Else
        '    'Agrego uno nuevo
        '    Dim ldrDatos As DataRow

        '    ldrDatos = pdsDatos.Tables(mstrTablaAnalisis).NewRow
        '    ldrDatos.Item("prta_id") = clsSQLServer.gObtenerId(pdsDatos.Tables(mstrTablaAnalisis), "prta_id")

        '    With ldrDatos
        '        .Item("prta_prdt_id") = pdrHijo.Item("prdt_id")
        '        .Item("prta_nume") = txtNroAnal.Valor
        '        .Item("prta_ares_id") = cmbAres.Valor
        '        '.Item("prta_tipo") = cmbTipoAnal.Valor
        '        .Item("prta_ndad") = "I"
        '        .Item("prta_fecha") = Now
        '        pdsDatos.Tables(mstrTablaAnalisis).Rows.Add(ldrDatos)
        '    End With
        'End If
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panBotones.Visible = True
        panCabecera.Visible = False
        panRequ.Visible = False
        panDocu.Visible = False
        panObse.Visible = False

        lnkCabecera.Font.Bold = False
        lnkRequ.Font.Bold = False
        lnkDocu.Font.Bold = False
        lnkObse.Font.Bold = False

        lblTitu.Text = ""

        Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())

        If origen <> 1 And usrProd.Valor.ToString <> "" Then
            lblTitu.Text = "Producto: " & _
            oRaza.GetRazaDescripcionById(usrProd.cmbProdRazaExt.SelectedValue.ToString()) & " - " & _
            usrProd.cmbSexoProdExt.SelectedItem.Text & _
            " - " & usrProd.txtProdNombExt.Valor.ToString
        End If

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
                lblTitu.Text = hdnTramNro.Text
            Case 2
                panRequ.Visible = True
                lnkRequ.Font.Bold = True
            Case 3
                panDocu.Visible = True
                lnkDocu.Font.Bold = True
            Case 4
                mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = "trad_baja_fecha is null"

                cmbObseRequ.DataTextField = "_requ_desc"
                cmbObseRequ.DataValueField = "trad_requ_id"
                cmbObseRequ.DataSource = mdsDatos.Tables(mstrTablaRequisitos)
                cmbObseRequ.DataBind()
                cmbObseRequ.Items.Insert(0, "(Seleccione)")
                cmbObseRequ.Items(0).Value = ""
                cmbObseRequ.Valor = ""

                mdsDatos.Tables(mstrTablaRequisitos).DefaultView.RowFilter = ""

                panObse.Visible = True
                lnkObse.Font.Bold = True

        End Select
    End Sub
    Private Sub mLimpiarPersonas(ByVal pstrTabla As String, ByVal pgrdGrilla As System.Web.UI.WebControls.DataGrid, ByVal pctrHdnOrig As System.Web.UI.WebControls.TextBox)
        For Each lDr As DataRow In mdsDatos.Tables(pstrTabla).Select()
            If lDr.Item("trpe_id") > 0 Then
                lDr.Delete()
            Else
                mdsDatos.Tables(pstrTabla).Rows.Remove(lDr)
            End If
        Next

        If mdsDatos.Tables(pstrTabla).Select.GetUpperBound(0) = -1 Then pctrHdnOrig.Text = ""

        pgrdGrilla.DataSource = mdsDatos.Tables(pstrTabla)
        pgrdGrilla.DataBind()
    End Sub

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    'REQUISISTOS
    Private Sub btnAltaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaRequ.Click
        mActualizarRequ(True)
    End Sub
    Private Sub btnLimpRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpRequ.Click
        mLimpiarRequ()
    End Sub
    Private Sub btnBajaRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaRequ.Click
        Try
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaObservaciones).Select("trao_trad_id=" + hdnRequId.Text)
                odrDeta.Delete()
            Next

            mConsultarObse()
            mdsDatos.Tables(mstrTablaRequisitos).Select("trad_id=" & hdnRequId.Text)(0).Delete()
            grdRequ.CurrentPageIndex = 0
            mConsultarRequ()
            mLimpiarRequ()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiRequ.Click
        mActualizarRequ(False)
    End Sub
    'DOCUMENTOS
    Private Sub btnAltaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocu.Click
        mActualizarDocu()
    End Sub
    Private Sub btnLimpDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocu.Click
        mLimpiarDocu()
    End Sub
    Private Sub btnBajaDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocu.Click
        Try
            mdsDatos.Tables(mstrTablaDocumentos).Select("trdo_id=" & hdnDocuId.Text)(0).Delete()
            grdDocu.CurrentPageIndex = 0
            mConsultarDocu()
            mLimpiarDocu()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocu.Click
        mActualizarDocu()
    End Sub
    'OBSERVACIONES
    Private Sub btnAltaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaObse.Click
        mActualizarObse()
    End Sub
    Private Sub btnLimpObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpObse.Click
        mLimpiarObse()
    End Sub
    Private Sub btnBajaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaObse.Click
        Try
            mdsDatos.Tables(mstrTablaObservaciones).Select("trao_id=" & hdnObseId.Text)(0).Delete()
            grdObse.CurrentPageIndex = 0
            mConsultarObse()
            mLimpiarObse()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiObse.Click
        mActualizarObse()
    End Sub



    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar(False)
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkRequ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRequ.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkDocu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocu.Click
        mShowTabs(3)
    End Sub
    Private Sub lnkObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkObse.Click
        mShowTabs(4)
    End Sub

#End Region

#Region "Opciones de POP"
    Private Sub mCargarPlantilla()
        If mstrTrapId <> "" Then

            Dim lDs As New DataSet
            lDs = clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_Tramites_Plantilla, "@SinBaja=1,@trap_id=" + mstrTrapId)
            Dim ldrDatos As DataRow

            mdsDatos.Tables(mstrTablaRequisitos).Clear()

            For Each ldrOri As DataRow In lDs.Tables(1).Select
                ldrDatos = mdsDatos.Tables(mstrTablaRequisitos).NewRow
                With ldrDatos
                    .Item("trad_id") = clsSQLServer.gObtenerId(.Table, "trad_id")
                    .Item("trad_tram_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                    .Item("trad_baja_fecha") = DBNull.Value
                    .Item("trad_requ_id") = ldrOri.Item("trpd_requ_id")
                    .Item("trad_obli") = ldrOri.Item("trpd_obli")
                    .Item("trad_pend") = True
                    .Item("trad_manu") = False
                    .Item("_requ_desc") = ldrOri.Item("_requisito")
                    .Item("_obli") = ldrOri.Item("_obligatorio")
                    .Item("_pend") = "S�"
                    .Item("_manu") = "No"
                    .Item("_estado") = ldrOri.Item("_estado")

                    .Table.Rows.Add(ldrDatos)
                End With
            Next

            Dim lstrRazas As String = ""
            Dim lstrEspecies As String = ""

            For Each ldrOri As DataRow In lDs.Tables(2).Select
                With ldrOri
                    If .IsNull("trdr_raza_id") Then
                        If lstrEspecies.Length > 0 Then lstrEspecies += ","
                        lstrEspecies += .Item("trdr_espe_id").ToString
                    Else
                        If lstrRazas.Length > 0 Then lstrRazas += ","
                        lstrRazas += .Item("trdr_raza_id").ToString
                    End If
                End With
            Next

            'usrProd.usrProductoExt.FiltroRazas = "@raza_ids = " & IIf(lstrRazas = "", "null", "'" & lstrRazas & "'") & ",@raza_espe_ids = " & IIf(lstrEspecies = "", "null", "'" & lstrEspecies & "'")
            'usrProd.usrProductoExt.mCargarRazas()

            mConsultarRequ()
        End If
    End Sub
    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Try
            mstrTrapId = hdnDatosPop.Text
            mAgregar()
            hdnDatosPop.Text = ""
            Session("mstrTrapId") = mstrTrapId

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub hdnDatosTEPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosTEPop.TextChanged
        Try
            hdnTEId.Text = ""
            txtRecuFecha.Text = ""
            txtTeDesc.Text = ""

            usrProductoMadre.Valor = ""
            usrProductoPadre.Valor = ""

            If (hdnDatosTEPop.Text <> "") Then
                hdnTEId.Text = hdnDatosTEPop.Text
                mDescripTE(hdnDatosTEPop.Text)
                hdnDatosTEPop.Text = ""
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    'Private Function GetAsociacionByTramiteId(ByVal TramiteId As String) As DataSet
    '    Dim ds As New DataSet

    '    mstrCmd = "exec GetAsociacionByTramiteId "
    '    mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

    '    ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
    '    Return ds

    'End Function
    Private Function GetProductoById(ByVal ProductoId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName

        mstrCmd = "exec GetProductoById "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function
    Private Function GetProductoByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetProductoByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function
    Private Function ValidarImportados() As Boolean
        Dim boolExiste As Boolean
        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())

        Dim intRazaId As Integer
        Dim NombreProd As String
        Dim boolSexo As Boolean

        intRazaId = Convert.ToInt32(usrProd.cmbProdRazaExt.SelectedValue)
        boolSexo = IIf(usrProd.Sexo = "1", True, False)
        NombreProd = usrProd.txtProdNombExt.Text

        boolExiste = oProducto.ValidarImportacionByRazaSexoRP(intRazaId, _
       usrProd.cmbSexoProdExt.Valor.ToString(), usrProd.txtRPExt.Text)

        'todo: corregir

        If boolExiste Then
            'ya se importo anteriormente
            Return False
        Else
            If Not oProducto.ExisteProductoByRazaSexoNombre(NombreProd, boolSexo, intRazaId) Then
                'no se importo anteriormente
                Return True
            Else

                'ya se importo anteriormente
                Return False
            End If

        End If

    End Function
    Private Sub mDescripTE(ByVal pstrId As String)
        Dim lsrtFiltro As String = " @tede_id =" + clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)

        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(mstrConn, "exec dbo.te_denun_tramite_busq " & lsrtFiltro)
        For Each ldr As DataRow In ds.Tables(0).Select
            With ldr
                txtTeDesc.Text = "Denuncia TE Nro.: " + Convert.ToString(.Item("tede_nume"))

                usrProductoMadre.Valor = .Item("tede_madr_prdt_id")
                usrProductoPadre.Valor = .Item("tede_pad1_prdt_id")
                txtRecuFecha.Fecha = .Item("tede_recu_fecha")
            End With
        Next

    End Sub
    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub grdDato_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdDato.SelectedIndexChanged

    End Sub
    Function MessageBox(ByRef oPage As Page, ByVal sAviso As String)
        If sAviso.Trim.Length > 0 Then
            Dim s As String = "alert('" & sAviso & "') "
            oPage.RegisterStartupScript("OnLoad", s)
        End If
    End Function
    ' Dario 2014-06-27 carga el combo snps segun dato de raza pasado
    Private Sub CargaComboSNPS()
        If (usrProd.cmbProdRazaExt.SelectedValue Is Nothing) Then
            clsWeb.gCargarCombo(mstrConn, "rg_analisis_resul_snps_cargar @raza_id=0", cmbSNPS, "codi", "descrip_codi", "S")
            'cmbSNPS.Enabled = False
        Else
            clsWeb.gCargarCombo(mstrConn, "rg_analisis_resul_snps_cargar @raza_id=0" & usrProd.cmbProdRazaExt.SelectedValue, cmbSNPS, "codi", "descrip_codi", "S")
            'cmbSNPS.Enabled = True
        End If
        Me.PnlcmbSNPS.UpdateAfterCallBack = True
    End Sub

    ' Dario 2014-06-24 Metodo que recupera el evento del cambio de producto del control usrPord
    Private Sub usrProd_Change(ByVal sender As Object) Handles usrProd.Cambio
        If Not (sender Is Nothing) Then

            CargaComboSNPS()
            ' creo el objeto de productos  Dario 2014-10-10
            Dim objProductosBusiness As New Business.Productos.ProductosBusiness
            Dim objProductoEntity As New Entities.ProductoEntity
            ' busco datos del producto snps y padres
            objProductoEntity = objProductosBusiness.GetDatosProducto(usrProd.Valor)
            If (objProductoEntity.idMadre > 0) Then
                usrProductoMadre.Valor = objProductoEntity.idMadre
            End If
            If (objProductoEntity.idPadre > 0) Then
                usrProductoPadre.Valor = objProductoEntity.idPadre
            End If
            If (objProductoEntity.idSNPS > 0) Then
                cmbSNPS.SelectedValue = objProductoEntity.idSNPS.ToString()
            End If
        End If
    End Sub
End Class
End Namespace
