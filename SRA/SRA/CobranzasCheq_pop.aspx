<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CobranzasCheq_pop" CodeFile="CobranzasCheq_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
  <head>
    <title>SELECCIONE</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name=vs_defaultClientScript content="JavaScript">
    <meta name=vs_targetSchema content="http://schemas.microsoft.com/intellisense/ie5">
    <LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
	<script language="JavaScript" src="includes/utiles.js"></script>
  </head>
  <body  onload="CargarCombo();">
    <form id="frmABM" method="post" runat="server">
		<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
			<TR>
				<TD vAlign="middle" align="right" height="40">
					<BUTTON class="boton" id="btnReci" style="WIDTH: 100%" language="javascript" onclick="Aceptar('0');window.close();" type="button" runat="server" value="Detalles">Generar Recibo</BUTTON>
				</TD>
				<TD vAlign="middle" align="right" height="40">
					<BUTTON class="boton" id="btnFact" style="WIDTH: 100%" language="javascript" onclick="Aceptar('1');window.close();" type="button" runat="server" value="Detalles">Imprimir Factura</BUTTON>
				</TD>
			</TR>
			<TR>
				<TD align=center colspan=2>
					<select name="cmbImp" class="combo" style="width:100%"></select></TD>
			</TR>
		</TABLE>
	</form>
	<SCRIPT language="javascript">
	function CargarCombo()
	{
		var combo = document.all("cmbImp");
		
		if("<%=Session("sImpreTipo")%>"=="C")
			combo.style.display = "none";
		else
		{
			var lImprs;
			var lImpr;
			var It;
			var lstrRet;
			var lstrRet=EjecutarMetodoXML("ObtenerImpresora", "Recibo");

			if (lstrRet == "0")
			{
				It =document.createElement("option");
				It.value = "";
				It.text = "IMPRESORA INEXISTENTE";
				combo.add(It);
			}
			else
			{
				if (lstrRet.indexOf("|") == -1)
				{
					It =document.createElement("option");
					It.value = lstrRet;
					It.text = lstrRet;
					combo.add(It);
					combo.style.display = "none";
				}
				else
				{
					lImprs = lstrRet.split("|")
					for (var i=0;i<lImprs.length; i++)
					{
						lImpr = lImprs[i].split("!")
						It =document.createElement("option");
						It.value = lImpr[0];
						It.text = lImpr[1];
						combo.add(It);
					}
				}
			}
		}
		Aceptar('0');
		document.all("btnReci").focus();
	}
	
	function Aceptar(pGeneFact)
	{
		window.returnValue = pGeneFact + "," + document.all('cmbImp').value
	}
		</SCRIPT>
  </body>
</html>