Namespace SRA

Partial Class FactMasivaProf
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents btnAgre As NixorControls.BotonImagen
    Protected WithEvents hdnUltSociNume As System.Web.UI.WebControls.TextBox
    Protected WithEvents hdnTotalSolis As System.Web.UI.WebControls.TextBox

    Protected WithEvents btnAnulReactiv As System.Web.UI.WebControls.Button
    Protected WithEvents panCriador As System.Web.UI.WebControls.Panel


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrConn As String
    Private mstrParaPageSize As Integer
    Private mdsDatos As DataSet
    Private mobj As SRA_Neg.Facturacion


    Private Enum Columnas As Integer
        Id = 1
        Nro = 3
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
                mobj.CentroEmisorNro(mstrConn)

                mSetearEventos()
                mCargarCombos()

                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                mdsDatos = Session(mstrTabla)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnAlta.Attributes.Add("onclick", "if(!confirm('Confirma la grabación de las facturas?')) return false;")
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mGuardarDatos()
        Dim lbooSelec As Boolean
        Dim lstrIDS As String = ""
        Dim lobj As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
        Dim ldsErr As DataSet

        For Each oItem As DataGridItem In grdProfo.Items
            lbooSelec = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked
            If lbooSelec Then
                'proforma para facturar
                lstrIDS = lstrIDS + IIf(lstrIDS <> "", ",", "") + oItem.Cells(Columnas.Id).Text()
            End If
        Next

        If lstrIDS <> "" Then
            'proformas que no pueden facturarse(muestra en una grilla al final de la generación)
            ldsErr = lobj.ErrProformaFact(mstrConn, lstrIDS)

            'ids de las proformas que si pueden facturarse
            For Each ldr As DataRow In ldsErr.Tables(0).Select
                lstrIDS = "," + lstrIDS + ","
                lstrIDS = lstrIDS.Replace("," + ldr.Item("prfr_id").ToString + ",", ",")
            Next

            lstrIDS = lstrIDS.Replace(",,", ",")

            'crea el ds con los datos para facturar 
            mdsDatos = lobj.ProformaFact(mstrConn, lstrIDS, CInt(Session("sCentroEmisorId")), CInt(Session("sCentroEmisorNro")))
            'pendiente:
            '1)facturar las proformas (usando )
            '1.1) dar de baja las proformas facturadas
            '2)mostrar las proformas que no pudieron ser facturadas 
            '  (las que tiene en ds ldsErr + las que no se pudieron facturar en el proceso
            '   lobj.ProformaFact)

            grdFact.DataSource = mdsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes)
            grdFact.DataBind()

            grdProfErr.DataSource = ldsErr.Tables(0)
            grdProfErr.DataBind()
        Else
            mMostrarPanel(False)
            Throw New AccesoBD.clsErrNeg("Debe indicar las proformas a facturar.")
        End If

        hdnIds.Text = lstrIDS

        Session(mstrTabla) = mdsDatos

        mMostrarPanel(False, True)

        btnAlta.Enabled = mdsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).Rows.Count > 0
    End Sub

    Public Sub mConsultar()
        Try
            mValidar()

            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec proformas_fact_consul")
            lstrCmd.Append("  @prfr_fecha_desde=" & clsFormatear.gFormatFecha2DB(txtFechaDesde.Fecha))
            lstrCmd.Append(", @prfr_fecha_hasta=" & clsFormatear.gFormatFecha2DB(txtFechaHasta.Fecha))
            lstrCmd.Append(", @prfr_acti_id=" + cmbActi.Valor.ToString)
            lstrCmd.Append(", @prfr_clie_id=" + usrClieFil.Valor.ToString)
            lstrCmd.Append(", @usua_id=" + Session("sUserId").ToString())

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdProfo)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Operacion Sobre la Grilla"
    Private Sub grdProfo_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Try
            If e.Item.ItemIndex <> -1 Then
                Dim ochkSel As CheckBox = e.Item.FindControl("chkSel")

                With CType(e.Item.DataItem, DataRowView).Row
                    ochkSel.Checked = .Item("sacd_id") <> 0
                End With

            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Eventos"
    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        mConsultar()
    End Sub
    Private Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mMostrarPanel(True)
    End Sub
    Private Sub btnGenerar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerar.Click
        Try
            mGuardarDatos()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        Try

            txtFechaDesde.Text = ""
            txtFechaHasta.Text = ""
            cmbActi.Limpiar()
            usrClieFil.Valor = ""
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnNinguno_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNinguno.Click
        Try
            mChequearGrilla(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTodos.Click
        Try
            mChequearTodos()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        Try
            Dim lstrCompIds, vstrCompIds(), lstrCompNumes As String

            Dim lobj As New SRA_Neg.FacturacionABM(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
            lstrCompIds = lobj.AltaProfMasi()

            If lstrCompIds <> "" Then
                vstrCompIds = lstrCompIds.Split(",")
                For Each lstrCompId As String In vstrCompIds
                    If lstrCompNumes <> "" Then lstrCompNumes += ", "
                    lstrCompNumes += clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantesX", lstrCompId, "numero")
                Next
                clsError.gGenerarMensajes(Me, "Se generaron las facturas: " + lstrCompNumes)
            End If

            mMostrarPanel(True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub imgCloseFact_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgCloseFact.Click
        mMostrarPanel(False)
    End Sub
#End Region

#Region "Funciones/Sub"

    Private Sub mChequearTodos()
        mChequearGrilla(True)
    End Sub

    Private Sub mChequearGrilla(ByVal pboolTodo As Boolean)
        Dim ldecTotal As Double
        ldecTotal = 0
        For Each oDataItem As DataGridItem In Me.grdProfo.Items
            CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = pboolTodo
            If pboolTodo Then
                ldecTotal = ldecTotal + CType(oDataItem.Cells(8).Text, Double)
            End If
        Next
        txtTotalSel.Text = ldecTotal.ToString()
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S", " @usua_id=" + Session("sUserId").ToString() + ",@factura = 1 , @actiProf = 1")
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean, Optional ByVal pbooVisiFact As Boolean = False)
        panFiltro.Visible = pbooVisi
        panDato.Visible = Not panFiltro.Visible
        PanFact.Visible = Not panFiltro.Visible And pbooVisiFact
        'grdProfo.Visible = Not PanFact.Visible
        txtTotalSel.Text = ""
    End Sub

    Private Sub mValidar()
        If Not (txtFechaDesde.Fecha Is DBNull.Value) And Not (txtFechaHasta.Fecha Is DBNull.Value) Then
            If txtFechaDesde.Fecha > txtFechaHasta.Fecha Then
                Throw New AccesoBD.clsErrNeg("La fecha Desde no puede ser mayor a la fecha Hasta.")
            End If
        End If
    End Sub
#End Region


End Class
End Namespace
