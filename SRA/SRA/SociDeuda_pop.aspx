<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SociDeuda_pop" CodeFile="SociDeuda_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Consulta de Deuda</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0" style="margin-top: 0px">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
							<TR>
								<TD vAlign="top" align="right" width="100%" colSpan="6">&nbsp;
									<asp:imagebutton id="imgClose" runat="server" ImageUrl="imagenes\Close3.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
							</TR>
							<tr>
								<td colSpan="6">
									<table>
										<TR>
											<TD align="right" width="5%"><asp:label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:label>&nbsp;</TD>
											<TD width="10%"><cc1:datebox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="68px" autopostback="true"></cc1:datebox></TD>
											<TD align="right" width="10%"><asp:label id="lblSoci" runat="server" cssclass="titulo">Socio:</asp:label></TD>
											<TD width="85%"><UC1:CLIE id="usrSoci" runat="server" autopostback="true" width="100%" FilCUIT="True" Tabla="Socios"
													Saltos="1,1,1" FilClieNume="True" FilSociNume="True" MuestraDesc="False" FilDocuNume="True" FilClaveUnica="True"></UC1:CLIE></TD>
										</TR>
									</table>
								</td>
							</tr>
							<tr>
								<td colSpan="6">
									<table>
										<TR>
											<TD align="left"><asp:label id="lblSaldoCtaCC" runat="server" cssclass="titulo">A favor Cta.Cte.:</asp:label>&nbsp;
												<CC1:NUMBERBOX id="txtSaldoCtaCC" runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"></CC1:NUMBERBOX></TD>
											<TD align="left"><asp:label id="lblTotalCC" runat="server" cssclass="titulo">Deuda Cta.Cte.:</asp:label>&nbsp;
												<CC1:NUMBERBOX id="txtTotalCC" runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"></CC1:NUMBERBOX></TD>
											<td align="left"><asp:label id="lblTotalVencCC" runat="server" cssclass="titulo">Vencida:</asp:label>&nbsp;
												<CC1:NUMBERBOX id="txtTotalVencCC" runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"></CC1:NUMBERBOX></td>
										</TR>
										<TR>
											<TD align="left"><asp:label id="lblSaldoCtaCS" runat="server" cssclass="titulo">A favor Cta.Soc.:</asp:label>&nbsp;
												<CC1:NUMBERBOX id="txtSaldoCtaCS" runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"></CC1:NUMBERBOX></TD>
										</TR>
									</table>
								</td>
							</tr>
							<TR height="100%">
								<TD vAlign="top" colSpan="6"><asp:datagrid id="grdConsulta" runat="server" width="100%" AutoGenerateColumns="False" ItemStyle-Height="5px"
										PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										BorderWidth="1px" AllowPaging="True" BorderStyle="None">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="3%"></HeaderStyle>
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" Runat="server" onclick="chkSelCheck(this);"></asp:CheckBox>
													<DIV runat="server" id="divCuot" style="DISPLAY: none"><%#(DataBinder.Eval(Container, "DataItem.cuota"))%></DIV>
													<DIV runat="server" id="divInte" style="DISPLAY: none"><%#((DataBinder.Eval(Container, "DataItem.interes").ToString))%></DIV>
													<DIV runat="server" id="divDesc" style="DISPLAY: none"><%#(DataBinder.Eval(Container, "DataItem.descuento"))%></DIV>
													<DIV runat="server" id="divPeti" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem.peti_id")%></DIV>
													<DIV runat="server" id="divPerio" style="DISPLAY: none"><%#DataBinder.Eval(Container, "DataItem.perio")%></DIV>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="comp_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="selec_ini"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="anio"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="perio"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="peti_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="inst_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="coti_desc" HeaderText="Concepto"></asp:BoundColumn>
											<asp:BoundColumn DataField="fechaVto" HeaderText="Fecha Vto." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="cuota" HeaderText="Cuota" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="interes" HeaderText="Intereses" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="descuento" HeaderText="Descuentos Varios"  HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="total" HeaderText="Total Por Cuota" HeaderStyle-HorizontalAlign="Right" DataFormatString="{0:F2}"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="left" width="25%" colSpan="2" style="HEIGHT: 24px" rowSpan="1"><asp:label id="lblTotalCuot" runat="server" cssclass="titulo">Cuota Pura:</asp:label>&nbsp;
									<CC1:NUMBERBOX id="txtTotalCuot" runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"></CC1:NUMBERBOX></TD>
								<TD align="left" width="50%" colSpan="2" style="HEIGHT: 24px" noWrap><asp:label id="lblTotalInte" runat="server" cssclass="titulo">Intereses al :</asp:label>&nbsp;
									<CC1:NUMBERBOX id="txtTotalInte" runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"></CC1:NUMBERBOX></TD>
								<TD align="left" width="25%" colSpan="2" style="HEIGHT: 24px"><asp:label id="lblTotalDesc" runat="server" cssclass="titulo">Descuentos:</asp:label>&nbsp;
									<CC1:NUMBERBOX id="txtTotalDesc" runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"></CC1:NUMBERBOX></TD>
							</TR>
							<TR>
								<TD align="left" colSpan="6"><asp:label id="lblTotalTotal" runat="server" cssclass="titulo">Total A Pagar:</asp:label>&nbsp;
									<CC1:NUMBERBOX id="txtTotalTotal" runat="server" cssclass="cuadrotextodeshab" Width="50px" Enabled="False"></CC1:NUMBERBOX></TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="left" colSpan="2" height="40" style="WIDTH: 196px"><asp:imagebutton id="btnList" runat="server" ImageUrl="./imagenes/prints1.GIF" ToolTip="Imprimir"></asp:imagebutton></TD>
								<TD vAlign="middle" align="right" colSpan="4" height="40"><asp:button id="btnAceptar" runat="server" cssclass="boton" Width="80px" Text="Aceptar"></asp:button></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
	function chkSelCheck(pChk)
	{
		var lintPerio;
		var lintPeti;
		var dCuot=0;
		var dInte=0;
		var dDesc=0;
		var dTotal=0;
		
		var lintFil= eval(pChk.id.replace("grdConsulta__ctl","").replace("_chkSel",""));
		var lbooChecked = pChk.checked;

		lintPerio = eval(document.all(pChk.id.replace("chkSel","divPerio")).innerText);
		lintPeti = eval(document.all(pChk.id.replace("chkSel","divPeti")).innerText);

		if(lintPerio==1&&lintPeti==8)
		{
			lintFil+=1;
			document.all("grdConsulta__ctl" + lintFil.toString() + "_chkSel").checked = lbooChecked;
		}
		
		if(lintPerio==2&&lintPeti==8&&!lbooChecked)
		{
			lintFil-=1;
			document.all("grdConsulta__ctl" + lintFil.toString() + "_chkSel").checked = false;
		}
		
		if (document.all("grdConsulta").children(0).children.length > 1)
		{
			for (var fila=3; fila <= document.all("grdConsulta").children(0).children.length; fila++)
			{
				if (fila>lintFil)
					document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked = false;
				
				if((lbooChecked)&&(fila<=lintFil))
					document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked = lbooChecked;


				if ((document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked))
				{
					/*
                    dCuot += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divCuot").innerText.replace(",", ""));
                    dInte += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divInte").innerText.replace(",", ""));
                    dDesc += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divDesc").innerText.replace(",", ""));
					*/
                    if (document.all("grdConsulta__ctl" + fila.toString() + "_divCuot").innerText != "00.00") {
                        dCuot += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divCuot").innerText); //.innerText.replace(",",""));
                    }
                    if (document.all("grdConsulta__ctl" + fila.toString() + "_divInte").innerText != "00.00") {
                        dInte += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divInte").innerText); //.innerText.replace(",",""));
                    }
                    if (document.all("grdConsulta__ctl" + fila.toString() + "_divDesc").innerText != "00.00") {
                        dDesc += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divDesc").innerText); //.innerText.replace(",",""));
                    }
				}
			}
		}

		dTotal = dCuot + dInte + dDesc;
        dCuot = dCuot.toFixed(2).toString();
        dInte = dInte.toFixed(2).toString();
        dDesc = dDesc.toFixed(2).toString();
        dTotal = dTotal.toFixed(2).toString();
		
		if (dCuot == 0)
			document.all("txtTotalCuot").value = "0.00"
		else
			document.all("txtTotalCuot").value = dCuot; // .substring(0, dCuot.length - 2) + "." + dCuot.substring(dCuot.length - 2 , dCuot.length );

		if (dInte == 0)
			document.all("txtTotalInte").value = "0.00"
		else
			document.all("txtTotalInte").value = dInte; // .substring(0, dInte.length - 2) + "." + dInte.substring(dInte.length - 2 , dInte.length );

		if (dDesc == 0)
			document.all("txtTotalDesc").value = "0.00"
		else
			document.all("txtTotaldesc").value = dDesc; //.substring(0, dDesc.length - 2) + "." + dDesc.substring(dDesc.length - 2 , dDesc.length );
	
		if (dTotal == 0)
			document.all("txtTotalTotal").value = "0.00"
		else
			document.all("txtTotalTotal").value = dTotal; // .substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2 , dTotal.length );
	}
	function mCalcularTotales()
	{
		var lintPerio;
		var lintPeti;
		var dCuot=0;
		var dInte=0;
		var dDesc=0;
		var dTotal=0;
		
		if (document.all("grdConsulta").children(0).children.length > 1)
		{
			for (var fila=3; fila <= document.all("grdConsulta").children(0).children.length; fila++)
			{
				if ((document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked))
				{
					/*
                    dCuot += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divCuot").innerText.replace(",", ""));
                    dInte += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divInte").innerText.replace(",", ""));
					dDesc += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divDesc").innerText.replace(",", ""));
					*/
                    if (document.all("grdConsulta__ctl" + fila.toString() + "_divCuot").innerText != "00.00") {
                        dCuot += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divCuot").innerText); //.innerText.replace(",",""));
                    }
                    if (document.all("grdConsulta__ctl" + fila.toString() + "_divInte").innerText != "00.00") {
                        dInte += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divInte").innerText); //.innerText.replace(",",""));
                    }
                    if (document.all("grdConsulta__ctl" + fila.toString() + "_divDesc").innerText != "00.00") {
                        dDesc += eval(document.all("grdConsulta__ctl" + fila.toString() + "_divDesc").innerText); //.innerText.replace(",",""));
                    }
				}
			}
		}

		dTotal = dCuot + dInte + dDesc;
        dCuot = dCuot.toFixed(2).toString();
        dInte = dInte.toFixed(2).toString();
        dDesc = dDesc.toFixed(2).toString();
        dTotal = dTotal.toFixed(2).toString();
		
		if (dCuot == 0)
			document.all("txtTotalCuot").value = "0.00"
		else
			document.all("txtTotalCuot").value = dCuot; //.substring(0, dCuot.length - 2) + "." + dCuot.substring(dCuot.length - 2 , dCuot.length );

		if (dInte == 0)
			document.all("txtTotalInte").value = "0.00"
		else
			document.all("txtTotalInte").value = dInte; //.substring(0, dInte.length - 2) + "." + dInte.substring(dInte.length - 2 , dInte.length );

		if (dDesc == 0)
			document.all("txtTotalDesc").value = "0.00"
		else
			document.all("txtTotaldesc").value = dDesc; //.substring(0, dDesc.length - 2) + "." + dDesc.substring(dDesc.length - 2 , dDesc.length );
	
		if (dTotal == 0)
			document.all("txtTotalTotal").value = "0.00"
		else
			document.all("txtTotalTotal").value = dTotal; //.substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2 , dTotal.length );
	}
	if (document.all("grdConsulta")!=null)
		mCalcularTotales();
        </SCRIPT>
	</BODY>
</HTML>
