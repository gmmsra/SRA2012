<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ChequesVenta" CodeFile="ChequesVenta.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Venta de Cheques</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		function CargaCuentas(ComboOrigen,ComboDestino,Opc)
		{
			var sFiltro = document.all(ComboOrigen).value;
			if (sFiltro != '')
			{
				LoadComboXML("cuentas_bancos_cargar", sFiltro, ComboDestino, Opc);
			}
		}
		</SCRIPT>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Venta de Cheques</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="97%"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																	ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE class="FdoFld" cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 32px" align="right">
																			<asp:Label id="lblBancFil" runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 85%; HEIGHT: 32px">
																			<TABLE>
																				<TR>
																					<TD>
																						<cc1:combobox class="combo" id="cmbBancFil" runat="server" cssclass="cuadrotexto" Width="260px"
																							AceptaNull="false" MostrarBotones="False" filtra="true" NomOper="bancos_cargar" TextMaxLength="7"></cc1:combobox></TD>
																					<TD><IMG language="javascript" id="btnBancoBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																							onclick="BusqCombo('bancos','','banc_desc','cmbBancFil','Bancos');" alt="Busqueda avanzada"
																							src="imagenes/Buscar16.gif" border="0">
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 17px" align="right">
																			<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD>
																			<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" Tabla="Clientes" FilSociNume="True"
																				MuestraDesc="true" FilLegaNume="True" FilDocuNume="True" Ancho="800"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 17px" align="right">
																			<asp:Label id="lblNumeFil" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtNumeFil" runat="server" cssclass="cuadrotexto" Width="270"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 17px" align="right"></TD>
																		<TD>
																			<asp:checkbox id="chkVend" CssClass="titulo" Runat="server" Text="Traer Vendidos durante el d�a"></asp:checkbox>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="9px"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="cheq_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="mone_desc" HeaderText="Mon"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_impo" HeaderText="Importe $" DataFormatString="{0:F2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="paco_orig_impo" HeaderText="Importe Ori." DataFormatString="{0:F2}">
												<HeaderStyle HorizontalAlign="Right"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="cheq_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="cheq_rece_fecha" HeaderText="Fecha Recep." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="banc_desc" HeaderText="Banco"></asp:BoundColumn>
											<asp:BoundColumn DataField="clie_apel" HeaderText="Cliente"></asp:BoundColumn>
											<asp:BoundColumn DataField="cheq_nume" HeaderText="Nro"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Visible="False" BorderWidth="1px"
										BorderStyle="Solid">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD height="5">
													<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
												<TD vAlign="top" align="right">&nbsp;
													<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="2">
													<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
														<TR>
															<TD align="right">
																<asp:Label id="lblBanc" runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
															<TD>
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD>
																			<cc1:combobox class="combo" id="cmbBanc" runat="server" Width="260px" onchange="mCargarCuentas();"
																				enabled="false"></cc1:combobox></TD>
																		<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																				onclick="mBotonBusquedaAvanzada('bancos','banc_desc','cmbBanc','Bancos','');" alt="Busqueda avanzada"
																				src="imagenes/Buscar16.gif" border="0">
																		</TD>
																	</TR>
																</TABLE>
															</TD>
															<TD align="right">
																<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro.Cheque:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:textboxtab id="txtNume" runat="server" cssclass="cuadrotextodeshab" Width="100px" enabled="false"></cc1:textboxtab></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblMone" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:combobox class="combo" id="cmbMone" runat="server" cssclass="cuadrotextodeshab" Width="90px"
																	enabled="false" Obligatorio="True"></cc1:combobox></TD>
															<TD align="right">
																<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false"
																	Obligatorio="True" EsDecimal="true"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblReceFecha" runat="server" cssclass="titulo">Fecha Recepci�n:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:DateBox id="txtReceFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px" enabled="false"
																	Obligatorio="true"></cc1:DateBox></TD>
															<TD align="right">
																<asp:Label id="lblTeorDepoFecha" runat="server" cssclass="titulo">Fecha Dep.:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:DateBox id="txtCheqFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px" AceptaNull="False"
																	enabled="false" Obligatorio="true"></cc1:DateBox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblChti" runat="server" cssclass="titulo">Tipo Cheque:</asp:Label>&nbsp;</TD>
															<TD colSpan="3">
																<cc1:combobox class="combo" id="cmbChti" runat="server" Width="70px" enabled="false"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 9px" vAlign="top" align="right">
																<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
															<TD style="HEIGHT: 9px" colSpan="3">
																<UC1:CLIE id="usrClie" runat="server" AceptaNull="false" Tabla="Clientes" FilSociNume="True"
																	MuestraDesc="true" FilDocuNume="True" Ancho="800" Activo="False" Saltos="1,1"></UC1:CLIE></TD>
														</TR>
														<TR>
															<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															<TD background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="Label3" runat="server" cssclass="titulo">Datos de la Venta</asp:Label>&nbsp;</TD>
															<TD colSpan="3"></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblCcos" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
															<TD colSpan="3">
																<TABLE cellSpacing="0" cellPadding="0" border="0">
																	<TR>
																		<TD>
																			<cc1:combobox class="combo" id="cmbCcos" runat="server" cssclass="cuadrotexto" Width="300" MostrarBotones="False"
																				filtra="true" NomOper="cuentas_cargar" Obligatorio="False"></cc1:combobox></TD>
																		<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																				onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCcos','Centros de Costo','@fina=1');"
																				alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																		</TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 22px" align="right">
																<asp:Label id="lblGastoImpo" runat="server" cssclass="titulo">Importe Gastos $:</asp:Label>&nbsp;</TD>
															<TD style="HEIGHT: 22px">
																<cc1:numberbox id="txtGstoImpo" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="False"
																	Obligatorio="False" EsDecimal="true"></cc1:numberbox>&nbsp;
																<asp:Label id="lblCoti" runat="server" cssclass="titulo">Cotizaci�n $  :</asp:Label>
																<cc1:numberbox id="txtCoti" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="False"
																	Obligatorio="True" EsDecimal="true"></cc1:numberbox></TD>
															<TD style="HEIGHT: 22px" align="right"></TD>
															<TD style="HEIGHT: 22px"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 25px" align="right">
																<asp:Label id="lblImpoPeso" runat="server" cssclass="titulo">Importe $:</asp:Label>&nbsp;</TD>
															<TD style="HEIGHT: 25px">
																<cc1:numberbox id="txtImpoMone" runat="server" cssclass="cuadrotexto" Width="100px" AceptaNull="False"
																	Obligatorio="false" EsDecimal="true"></cc1:numberbox>
																<asp:Label id="lblImpoMone" runat="server" cssclass="titulo">Importe:                </asp:Label>
																<cc1:numberbox id="txtImpoPeso" runat="server" cssclass="cuadrotexto" Width="100px" AceptaNull="False"
																	Obligatorio="false" EsDecimal="true"></cc1:numberbox></TD>
															<TD style="HEIGHT: 25px" colSpan="3">&nbsp;&nbsp;&nbsp;
																<asp:Label id="lblGstoMone" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;
																<cc1:combobox class="combo" id="cmbGstoMone" runat="server" cssclass="combo" Width="90px" enabled="false"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 21px" align="right"></TD>
															<TD style="HEIGHT: 21px" colSpan="3">
																<asp:checkbox id="chkDisp" CssClass="titulo" Runat="server" Text="Disponible para depositar" Checked="True"></asp:checkbox></TD>
														</TR>
														<TR>
															<TD vAlign="middle" align="center" colSpan="4" height="5">
																<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
									<ASP:PANEL id="panBotones" Runat="server">
										<TABLE width="100%">
											<TR height="30">
												<TD align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Vender" CausesValidation="False"></asp:Button>&nbsp;
													<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="100px" Text="Anular Venta" CausesValidation="False"></asp:Button>&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Cerrar" CausesValidation="False"></asp:Button></TD>
											</TR>
										</TABLE>
									</ASP:PANEL>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnEmctId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtImpo"]!= null&&!document.all["txtImpo"].disabled)
			document.all["txtImpo"].focus();
		</SCRIPT>
	</BODY>
</HTML>
