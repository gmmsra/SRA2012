Namespace SRA

Partial Class Conceptos
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Textbox1 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox2 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Textbox3 As System.Web.UI.WebControls.TextBox
    Protected WithEvents Combobox1 As NixorControls.ComboBox


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "Conceptos"
    Private mstrTablaActi As String = "Actividades_Concep"
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object
        Dim lintCol As Integer

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

        txtCodi.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "conc_codi")
        txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "conc_desc")
        txtCodigoDesdeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "conc_codi")
        txtCodigoHastaFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "conc_codi")
        txtDescripcionFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "conc_desc")
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActividades, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActividadesFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "formulas", cmbForm, "S", "@origen=2")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuen, "S", "@CodiPrim = '1,2,4,5'")

        ' Dario 2016-04-28 para que diferencie trene fsra y sra los filtros
        Dim base As String = ConfigurationSettings.AppSettings("conBase")

        If (UCase(base) = "FSRA") Then
            clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuenOpt, "S", "@CodiPrim = '213',@CodiPrim2 = '113'")
        Else
            clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuenOpt, "S", "@CodiPrim = '2132',@CodiPrim2 = '1136'")
        End If


        clsWeb.gCargarRefeCmb(mstrConn, "formulas_desc", cmbFormAux, "S", "@origen=2")

        With cmbGravaTasa
            .Items.Insert(0, "(Seleccione)")
            .Items(0).Value = ""

            .Items.Insert(1, "Tasa Normal")
            .Items(1).Value = "1"

            .Items.Insert(2, "Tasa Reducida")
            .Items(2).Value = "2"

            .Items.Insert(3, "Sobretasa")
            .Items(3).Value = "3"
        End With
        cmbGravaTasa.Enabled = False
    End Sub
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd = mstrCmd + " @conc_desc = " + clsSQLServer.gFormatArg(txtDescripcionFil.Valor.ToString, SqlDbType.VarChar)
            mstrCmd = mstrCmd + ",@conc_codi = " + clsSQLServer.gFormatArg(txtCodigoDesdeFil.Valor.ToString, SqlDbType.VarChar)
            mstrCmd = mstrCmd + ",@conc_codiHasta = " + clsSQLServer.gFormatArg(txtCodigoHastaFil.Valor.ToString, SqlDbType.VarChar)
            mstrCmd = mstrCmd + ",@actividad = " + clsSQLServer.gFormatArg(cmbActividadesFil.Valor.ToString, SqlDbType.Int)

            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorActi(ByVal pbooAlta As Boolean)
        btnBajaActi.Enabled = Not (pbooAlta)
        btnModiActi.Enabled = Not (pbooAlta)
        btnAltaActi.Enabled = pbooAlta
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                txtCodi.Valor = .Item("conc_codi")
                txtDesc.Valor = .Item("conc_desc")
                cmbForm.Valor = .Item("conc_form_id")
                    cmbCuen.Valor = .Item("conc_cuct_id")
                    If Not (.Item("conc_secu_cuct_id") Is DBNull.Value) Then
                        cmbCuenOpt.Valor = .Item("conc_secu_cuct_id")
                    End If
                    chkUtilRecibo.Checked = IIf(.Item("conc_reci") Is DBNull.Value, False, .Item("conc_reci"))
                    chkIIBBGrava.Checked = IIf(.Item("conc_iibb_grava") Is DBNull.Value, False, .Item("conc_iibb_grava"))
                    If .Item("conc_grava") Then
                        cmbGravaTasa.Valor = .Item("conc_grava_tasa")
                        hdnHabComboIVA.Text = "0"
                        rbtIva.Checked = True
                    End If
                    If Not .IsNull("conc_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("conc_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If
                End With
            Me.cmbGravaTasa.Enabled = (Me.hdnHabComboIVA.Text = "0")
            mSetearEditor(False)
            mMostrarPanel(True)
            mShowTabs(1)
        End If

    End Sub
    Public Sub mEditarDatosActi(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrActi As DataRow

            hdnActiId.Text = E.Item.Cells(1).Text
            ldrActi = mdsDatos.Tables(mstrTablaActi).Select("acco_id=" & hdnActiId.Text)(0)

            With ldrActi
                cmbActividades.Valor = .Item("acco_acti_id")
                If Not .IsNull("acco_baja_fecha") Then
                    lblBajaActi.Text = "Actividad dada de baja en fecha: " & CDate(.Item("acco_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaActi.Text = ""
                End If
            End With
            mSetearEditorActi(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaActi

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If
        grdActi.DataSource = mdsDatos.Tables(mstrTablaActi)
        grdActi.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        txtCodi.Text = ""
        txtDesc.Text = ""
        hdnHabComboIVA.Text = "1"
        cmbGravaTasa.Limpiar()
        rbtIva.Checked = False
        rbtGrav.Checked = True
        cmbForm.Limpiar()
        cmbCuen.Limpiar()
        cmbCuenOpt.Limpiar()
        txtFormDesc.Text = ""
        lblBaja.Text = ""
        mLimpiarActi()

        grdActi.CurrentPageIndex = 0

        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarActi()
        hdnActiId.Text = ""
        cmbActividades.Limpiar()
        lblBajaActi.Text = ""
        mSetearEditorActi(True)
    End Sub
    Private Sub mLimpiarFil()
        txtDescripcionFil.Text = ""
        txtCodigoDesdeFil.Text = ""
        txtCodigoHastaFil.Text = ""
        cmbActividadesFil.Limpiar()
        mConsultar()
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        panBotones.Visible = pbooVisi
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Alta()

            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Modi()

            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos()
        Me.cmbGravaTasa.Enabled = (Me.hdnHabComboIVA.Text = "0")

        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If rbtIva.Checked And cmbGravaTasa.Valor Is DBNull.Value Then
            rbtIva.Checked = True
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la Tasa de I.V.A.")
        End If

        If grdActi.Items.Count = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe haber al menos una Actividad Relacionada.")
        End If
    End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("conc_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("conc_codi") = txtCodi.Valor
            .Item("conc_desc") = txtDesc.Valor
            .Item("conc_form_id") = cmbForm.Valor
                .Item("conc_cuct_id") = cmbCuen.Valor
                If cmbCuenOpt.Valor.ToString.Length > 0 Then
                    .Item("conc_secu_cuct_id") = cmbCuenOpt.Valor
                End If
                .Item("conc_reci") = chkUtilRecibo.Checked
                .Item("conc_grava") = rbtIva.Checked
                .Item("conc_iibb_grava") = chkIIBBGrava.Checked
                If rbtIva.Checked Then
                    .Item("conc_grava_tasa") = cmbGravaTasa.Valor
                Else
                    .Item("conc_grava_tasa") = DBNull.Value
                End If
            End With
    End Sub
    'Seccion de las Actividades
    Private Sub mActualizarActi()
        Try
            mValidaDatosActi()
            mGuardarDatosActi()

            mLimpiarActi()
            grdActi.DataSource = mdsDatos.Tables(mstrTablaActi)
            grdActi.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaActi()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaActi).Select("acco_id=" & hdnActiId.Text)(0)
            row.Delete()
            grdActi.DataSource = mdsDatos.Tables(mstrTablaActi)
            grdActi.DataBind()
            mLimpiarActi()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosActi()
        Dim ldrActi As DataRow

        If hdnActiId.Text = "" Then
            ldrActi = mdsDatos.Tables(mstrTablaActi).NewRow
            ldrActi.Item("acco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaActi), "acco_id")
        Else
            ldrActi = mdsDatos.Tables(mstrTablaActi).Select("acco_id=" & hdnActiId.Text)(0)
        End If

        With ldrActi
            .Item("acco_acti_id") = cmbActividades.Valor

            .Item("_ActiDesc") = cmbActividades.SelectedItem.Text
            .Item("_estado") = "Activo"
            .Item("acco_conc_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("acco_audi_user") = Session("sUserId").ToString()
            .Item("acco_baja_fecha") = DBNull.Value
        End With
        If (hdnActiId.Text = "") Then
            mdsDatos.Tables(mstrTablaActi).Rows.Add(ldrActi)
        End If
    End Sub
    Private Sub mValidaDatosActi()
        If cmbActividades.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Actividad.")
        End If
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            panActividades.Visible = False
            lnkActividades.Font.Bold = False
            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 2
                    panActividades.Visible = True
                    lnkActividades.Font.Bold = True
            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub lnkActividades_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkActividades.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub btnClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        Try
            Dim params As String
            Dim lstrRptName As String = "Conceptos"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&conc_codi=" + txtCodigoDesdeFil.Valor.ToString
            lstrRpt += "&conc_codiHasta=" + txtCodigoHastaFil.Valor.ToString
            lstrRpt += "&conc_desc=" + txtDescripcionFil.Valor.ToString
            lstrRpt += "&actividad=" + cmbActividadesFil.Valor.ToString
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Overloads Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    'Botones de actividades
    Private Sub btnAltaActi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaActi.Click
        mActualizarActi()
    End Sub
    Private Sub btnBajaActi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaActi.Click
        mBajaActi()
    End Sub
    Private Sub btnModiActi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiActi.Click
        mActualizarActi()
    End Sub
    Private Sub btnLimpActi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpActi.Click
        mLimpiarActi()
    End Sub
#End Region

End Class
End Namespace
