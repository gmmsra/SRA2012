Namespace SRA

Partial Class PedigreeCarga
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Productos
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mMostrarPanel(False)
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
         End If

         usrCriaFil.cmbCriaRazaExt.Valor = cmbRazaFil.Valor.ToString
         usrPadreFil.cmbProdRazaExt.Valor = cmbRazaFil.Valor.ToString
         usrMadreFil.cmbProdRazaExt.Valor = cmbRazaFil.Valor.ToString
         If cmbRazaFil.Valor.ToString <> "" Then
            usrCriaFil.cmbCriaRazaExt.Enabled = False
            usrPadreFil.cmbProdRazaExt.Enabled = False
            usrMadreFil.cmbProdRazaExt.Enabled = False
         Else
            usrCriaFil.cmbCriaRazaExt.Enabled = True
            usrPadreFil.cmbProdRazaExt.Enabled = True
            usrMadreFil.cmbProdRazaExt.Enabled = True
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      Dim lstrSRA As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")
      clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRazaFil, "T")
      SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLong As Object
      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "prdt_sra_nume")
      txtNombFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "prdt_nomb")
      txtRPNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "prdt_rp_nume")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub grdDato_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mConsultar()
      Try
         Dim lstrCmd As String

         lstrCmd = "exec " & mstrTabla & "_busq"
         lstrCmd = lstrCmd & "  @buscar_en=" + IIf(chkBusc.Checked, "1", "0")
         lstrCmd = lstrCmd & " ,@prdt_cria_id=" + clsSQLServer.gFormatArg(usrCriaFil.Valor, SqlDbType.Int)
         lstrCmd = lstrCmd & " ,@prdt_raza_id=" + IIf(cmbRazaFil.Valor.ToString = "", "0", cmbRazaFil.Valor.ToString)
         lstrCmd = lstrCmd & " ,@prdt_sexo=" + IIf(cmbSexoFil.Valor.ToString = "", "null", cmbSexoFil.Valor.ToString)
         lstrCmd = lstrCmd & " ,@prdt_sra_nume=" + clsSQLServer.gFormatArg(txtNumeFil.Valor, SqlDbType.Int)
         lstrCmd = lstrCmd & " ,@incluir_bajas=" + IIf(chkBaja.Checked, "1", "0")
         lstrCmd = lstrCmd & " ,@madre_prdt_id=" + clsSQLServer.gFormatArg(usrMadreFil.Valor, SqlDbType.Int)
         lstrCmd = lstrCmd & " ,@padre_prdt_id=" + clsSQLServer.gFormatArg(usrPadreFil.Valor, SqlDbType.Int)
         lstrCmd = lstrCmd & " ,@prdt_rp=" + clsSQLServer.gFormatArg(txtRPNumeFil.Valor, SqlDbType.VarChar)
         lstrCmd = lstrCmd & " ,@prdt_nomb=" + clsSQLServer.gFormatArg(txtNombFil.Valor, SqlDbType.VarChar)
         lstrCmd = lstrCmd & " ,@prdt_naci_fecha_desde=" + clsSQLServer.gFormatArg(txtNaciFechaDesdeFil.Text, SqlDbType.SmallDateTime)
         lstrCmd = lstrCmd & " ,@prdt_naci_fecha_hasta=" + clsSQLServer.gFormatArg(txtNaciFechaHastaFil.Text, SqlDbType.SmallDateTime)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

         grdDato.Visible = True

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub
   Public Sub mCrearDataSet(ByVal pstrId As String)
      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables.Remove(mdsDatos.Tables(3))
      mdsDatos.Tables.Remove(mdsDatos.Tables(2))
      mdsDatos.Tables.Remove(mdsDatos.Tables(1))

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      Session(mstrTabla) = mdsDatos
   End Sub
#End Region

#Region "Seteo de Controles"
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mLimpiar()

         hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

         mCrearDataSet(hdnId.Text)

         With mdsDatos.Tables(mstrTabla).Rows(0)
            usrProd.Valor = .Item("prdt_id")

            If Not .IsNull("prdt_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("prdt_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If

            mSetearEditor(mstrTabla, False)
            mMostrarPanel(True)
         End With
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiarFiltros()
      txtNumeFil.Text = ""
      cmbRazaFil.Limpiar()
      cmbSexoFil.Limpiar()
      txtNombFil.Text = ""
      txtRPNumeFil.Text = ""
      txtNaciFechaDesdeFil.Text = ""
      txtNaciFechaHastaFil.Text = ""

      usrCriaFil.cmbCriaRazaExt.Enabled = True
      usrPadreFil.cmbProdRazaExt.Enabled = True
      usrMadreFil.cmbProdRazaExt.Enabled = True
      usrCriaFil.Limpiar()
      usrMadreFil.Limpiar()
      usrPadreFil.Limpiar()

      If cmbRazaFil.Valor.ToString <> "0" Then
         usrCriaFil.cmbCriaRazaExt.Enabled = False
         usrPadreFil.cmbProdRazaExt.Enabled = False
         usrMadreFil.cmbProdRazaExt.Enabled = False
      Else
         usrCriaFil.cmbCriaRazaExt.Enabled = True
         usrPadreFil.cmbProdRazaExt.Enabled = True
         usrMadreFil.cmbProdRazaExt.Enabled = True
      End If

      chkBusc.Checked = False
      chkBaja.Checked = False

      grdDato.Visible = False
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      lblBaja.Text = ""
      usrProd.Limpiar()
      lblTitu.Text = ""

      mCrearDataSet("")

      mSetearEditor(mstrTabla, True)
   End Sub

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrTabla
            btnAlta.Enabled = pbooAlta
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
      End Select
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltros.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      panCabecera.Visible = True
      btnAgre.Visible = Not panDato.Visible
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsDatosProd As DataSet
         ldsDatosProd = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatosProd, Context)
         lobjGenerico.Alta()

         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         Dim ldsDatosProd As DataSet
         ldsDatosProd = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatosProd, Context)
         lobjGenerico.Modi()

         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
         lobjGenerica.Baja(hdnId.Text)

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If usrProd.hdnProdIdExt.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Producto.")
      End If

      If usrProd.usrPadreExt.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Padre del Producto seleccionado.")
      End If

      If usrProd.usrMadreExt.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Madre del Producto seleccionado.")
      End If
   End Sub
   Private Function mGuardarDatos() As DataSet
      Dim ldsDatosProd As DataSet
      mValidarDatos()

      ldsDatosProd = mGuardarDatosProd()
      Return ldsDatosProd
   End Function
   'PRODUCTOS
   Private Function mGuardarDatosProd() As DataSet
      Dim ldsDatosProd As DataSet
      Dim ldrDatos As DataRow

      usrProd.NdadProd = "E"

      ldsDatosProd = usrProd.GuardarDatos

      Return (ldsDatosProd)
   End Function
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
#End Region

End Class
End Namespace
