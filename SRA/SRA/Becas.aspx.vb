Namespace SRA

Partial Class Becas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents btnList As NixorControls.BotonImagen


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
   Private mintinsti As Int32

   Private Enum Columnas As Integer
      Id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()

            mCargarCombos()
            mConsultar()
        Else
                    ' mCargarCentroCostos(False)
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbMatrConc, "S", "@conc_grava=0")
      clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbCuotConc, "S", "@conc_grava=0")
      'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCcos, "S")
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "beca_desc")
      txtDescFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "beca_desc")
      txtImpo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "beca_impo")
      txtPorc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "beca_porc")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrTabla = "Becas"

      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintinsti = Convert.ToInt32(Request.QueryString("fkvalor"))
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul"
         mstrCmd += " @beca_inse_id = " + mintinsti.ToString
         mstrCmd += ",@beca_desc = " + clsSQLServer.gFormatArg(txtDescFil.Text, SqlDbType.VarChar)

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
      mstrCmd = "exec " + mstrTabla + "_consul"
      mstrCmd += " " + hdnId.Text

      Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      If ldsDatos.Tables(0).Rows.Count > 0 Then
         With ldsDatos.Tables(0).Rows(0)
            txtDesc.Valor = .Item("beca_desc")
            cmbMone.Valor = .Item("beca_mone_id")
            txtImpo.Valor = .Item("beca_impo")
            txtPorc.Valor = .Item("beca_porc")
            chkMatr.Checked = .Item("beca_matri")
            chkCuot.Checked = .Item("beca_cuota")
            cmbMatrConc.Valor = .Item("beca_matr_conc_id")
            cmbCuotConc.Valor = .Item("beca_cuot_conc_id")
            mCargarCentroCostos(True)
            cmbCcos.Valor = .Item("beca_ccos_id")
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
      End If
   End Sub
   Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
      Dim lstrCta1 As String = ""
      Dim lstrCta2 As String = ""
      If Not cmbMatrConc.Valor Is DBNull.Value and Not cmbCuotConc.Valor Is DBNull.Value  Then
        lstrCta1 = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbMatrConc.Valor)
        lstrCta1 = lstrCta1.Substring(0, 1)
        lstrCta2 = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbCuotConc.Valor)
        lstrCta2 = lstrCta2.Substring(0, 1)
      End If
      If lstrCta1 <> "" And lstrCta1 <> "1" And lstrCta1 <> "2" And lstrCta1 <> "3" and lstrCta2 <> "" And lstrCta2 <> "1" And lstrCta2 <> "2" And lstrCta2 <> "3" Then
         If Not Request.Form("cmbCCos") Is Nothing Or pbooEdit Then
            clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta1, cmbCCos, "id", "descrip", "N")
            If Not Request.Form("cmbCCos") Is Nothing Then
                cmbCCos.Valor = Request.Form("cmbCCos").ToString
            End If
         End If
      Else
         cmbCCos.Items.Clear()
      End If
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      txtDesc.Text = ""
      cmbMone.Limpiar()
      txtImpo.Text = ""
      txtPorc.Text = ""
      chkMatr.Checked = True
      chkCuot.Checked = False
      cmbMatrConc.Limpiar()
      cmbCuotConc.Limpiar()
      cmbCcos.Items.Clear 

      mSetearEditor(True)
   End Sub
   Private Sub mLimpiarFil()
      txtDescFil.Text = ""
      mConsultar()
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If (txtPorc.Valor.ToString = "0" Or txtPorc.Valor.ToString = "") And (txtImpo.Valor.ToString = "0" Or txtImpo.Valor.ToString = "") Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el importe o el porcentaje.")
      End If

      If chkMatr.Checked = False And chkCuot.Checked = False Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar a que se aplicar� la Beca.")
      End If

      If chkMatr.Checked And cmbMatrConc.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Concepto de Matricula.")
      End If

      If chkCuot.Checked And cmbCuotConc.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Concepto de Cuota.")
      End If

   End Sub
   Private Sub mAlta()
      Try
         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         Dim ldsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Function mGuardarDatos() As DataSet
      Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      mValidarDatos()

      With ldsDatos.Tables(0).Rows(0)
         .Item("beca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("beca_inse_id") = mintinsti
         .Item("beca_desc") = txtDesc.Valor
         .Item("beca_mone_id") = cmbMone.Valor
         .Item("beca_impo") = txtImpo.Valor
         .Item("beca_porc") = txtPorc.Valor
         .Item("beca_matri") = chkMatr.Checked
         .Item("beca_cuota") = chkCuot.Checked
         .Item("beca_matr_conc_id") = cmbMatrConc.Valor
         .Item("beca_cuot_conc_id") = cmbCuotConc.Valor
         .Item("beca_ccos_id") = cmbCcos.Valor
      End With
      Return ldsDatos
   End Function
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
      Try
         Dim lstrRptName As String = "Becas"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         lstrRpt += "&descripcion=" + txtDescFil.Text

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

End Class
End Namespace
