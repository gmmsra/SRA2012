Namespace SRA

Partial Class SociDocum
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrClieId As String
   Private mstrPath As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mstrClieId = Request.QueryString("clie_id")
         mstrPath = clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_foto_path")
         If (Not Page.IsPostBack) Then
            mConsultar()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As String = "exec clientes_consul @clie_id=" & mstrClieId
         Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), lstrCmd)
         While (dr.Read())
            txtFoto.Valor = dr.GetValue(dr.GetOrdinal("clie_foto"))
            txtFirma.Valor = dr.GetValue(dr.GetOrdinal("clie_firma"))
            lblTitu.Text = "Documentación Socio: " & dr.GetValue(dr.GetOrdinal("_soci_nume")) & " - " & dr.GetValue(dr.GetOrdinal("clie_nomb")) & " " & dr.GetValue(dr.GetOrdinal("clie_apel"))
         End While
         dr.Close()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mGuardarArchivos(ByVal lstrClieId As String, ByVal lstrPath As String)
      Dim lstrNombre As String
      Dim lstrCarpeta As String

      lstrCarpeta = lstrPath
      If Not (filFoto.PostedFile Is Nothing) AndAlso filFoto.PostedFile.FileName <> "" Then
         lstrNombre = filFoto.PostedFile.FileName
         lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)
         lstrNombre = lstrClieId & "_" & lstrNombre
         filFoto.PostedFile.SaveAs(server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre)
         txtFoto.Text = lstrNombre
      End If

      If Not (filFirma.PostedFile Is Nothing) AndAlso filFirma.PostedFile.FileName <> "" Then
         lstrNombre = filFirma.PostedFile.FileName
         lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)
         lstrNombre = lstrClieId & "_" & lstrNombre
         filFirma.PostedFile.SaveAs(Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre)
         txtFirma.Text = lstrNombre
      End If

   End Sub

   Private Sub mModi()
      Try
         Dim lstrCmd As String = ""

         mGuardarArchivos(mstrClieId, mstrPath)

         lstrCmd = "socios_documentacion_modi "
         lstrCmd = lstrCmd & " @clie_id=" & clsSQLServer.gFormatArg(mstrClieId, SqlDbType.Int)
         If txtFoto.Text <> "" Then
            lstrCmd = lstrCmd & ",@clie_foto=" & clsSQLServer.gFormatArg(txtFoto.Valor.ToString, SqlDbType.VarChar)
         Else
            If filFoto.Value <> "" Then
                lstrCmd = lstrCmd & ",@clie_foto=" & clsSQLServer.gFormatArg(mstrClieId & "_" & filFoto.Value.Substring(filFoto.Value.LastIndexOf("\") + 1), SqlDbType.VarChar)
            End If
         End If
         If txtFirma.Text <> "" Then
            lstrCmd = lstrCmd & ",@clie_firma=" & clsSQLServer.gFormatArg(txtFirma.Valor.ToString, SqlDbType.VarChar)
         Else
            If filFirma.Value <> "" Then
                lstrCmd = lstrCmd & ",@clie_firma=" & clsSQLServer.gFormatArg(mstrClieId & "_" & filFirma.Value.Substring(filFirma.Value.LastIndexOf("\") + 1), SqlDbType.VarChar)
            End If
         End If
         lstrCmd = lstrCmd & ",@clie_audi_user=" & clsSQLServer.gFormatArg(Session("sUserId").ToString(), SqlDbType.Int)

         clsSQLServer.gExecute(mstrConn, lstrCmd)

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

End Class

End Namespace
