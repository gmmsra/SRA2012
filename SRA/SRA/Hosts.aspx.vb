Namespace SRA

Partial Class Hosts

    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblTitu As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Public mstrTabla As String
   Public mstrParaPageSize As String
   Public mstrCmd As String
   Private mstrConn As String
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrTabla = "hosts"
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mConsultar()
            mSetearMaxLength()
            clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", Me.cmbCentroEmisor, "S")
            mSetearEventos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mEstablecerPerfil()
      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Perfiles, String), (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If
      btnAlta.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      btnModi.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      btnLimp.Visible = (btnAlta.Visible Or btnBaja.Visible Or btnModi.Visible)
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "hsts_desc")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal e As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = e.NewPageIndex
            mConsultar()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul "
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal e As DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Function CrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)
      ldsEsta.Tables(0).TableName = mstrTabla
      Return ldsEsta
   End Function
   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)

      With ldsEsta.Tables(0).Rows(0)
         txtId.Text = .Item("hsts_id").ToString()
         Me.txtDesc.Valor = .Item("hsts_desc").ToString()
         Me.cmbCentroEmisor.Valor = .Item("_emct_id").ToString()
      End With
      mSetearEditor(False)
      mMostrarPanel(True)
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      Me.cmbCentroEmisor.SelectedValue = ""
      txtDesc.Text = ""
      mSetearEditor(True)
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If pbooVisi Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      mEstablecerPerfil()
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Alta()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()

      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(txtId.Text)

         mMostrarPanel(False)
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, Me.txtId.Text)

      With ldsEsta.Tables(0).Rows(0)

         If (Trim(Me.txtDesc.Text) = "") Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la Descripci�n del Host.")
         End If
         If (Me.cmbCentroEmisor.SelectedItem.Text = "(Seleccione)") Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un Centro Emisor.")
         End If

         .Item("hsts_id") = clsSQLServer.gFormatArg(Me.txtId.Text, SqlDbType.Int)
         .Item("hsts_desc") = Me.txtDesc.Valor
         .Item("hsts_emct_id") = Me.cmbCentroEmisor.Valor
      End With

      Return ldsEsta
   End Function
#End Region

   Private Sub btnModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "hosts"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
