<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Depositos" CodeFile="Depositos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Dep�sitos</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/impresion.js"></script>
		<SCRIPT language="javascript">
		function ConsultarEfectivo(pCuenta)
		{
			if (document.all('cmbCuentaNueva')!= null)
			{
				var lstrFiltro = document.all('hdnCenEmId').value + ',' + pCuenta.value;
					
				if (document.all('lblEfectivoMuestra')!=null)
				{
					if (pCuenta.value == '' || pCuenta.value == null)
					{
						document.all('lblEfectivoMuestra').innerText='0.00';
					}
					else
					{
						var lstrValor = LeerCamposXML("depositos_cobranzas_efectivo", lstrFiltro, "importe");
						document.all('lblEfectivoMuestra').innerText = lstrValor;
						//document.all('lblEfectivoMuestra').innerText = '( ' + LeerCamposXML("depositos_cobranzas_efectivo_consul", lstrFiltro, "importe") + ' )';
					}
				}
			}
		}
		function CargaCombo(ComboOrigen,ComboDestino)
			{
			var sFiltro = document.all(ComboOrigen).value;
				if (sFiltro != '')
				{
					if (ComboOrigen=='cmbBanco')
	    				LoadComboXML("cuentas_bancos_cargar", sFiltro, ComboDestino, "T");
					else
						LoadComboXML("cuentas_bancos_cargar", sFiltro + "," + document.all("cmbMoneda").value, ComboDestino, "S");					
				}
				else
					document.all(ComboDestino).innerText='';
			}
		
		function Imprimir()
		{
			var depoIds="";
			var lstrRpt = "BoletasDeposito";
			
			if (document.all("hdnDeposId").value != "")
			{
				try
				{
					var vValParams = (document.all("hdnDeposId").value).split(",");
					
					for(i=0; i<vValParams.length; i++)
					{
						if(depoIds!="") depoIds = depoIds + ",";
							depoIds = depoIds + vValParams[i];

						var sRet = ImprimirReporte(lstrRpt,"DepoId",vValParams[i],"C","","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");

						if(sRet=="0") 
						{
							document.all("hdnImprId").value="";
							return;
						}
					}
			
				}
				catch(e)
				{
					document.all("hdnImprId").value="";
					alert("Error al intentar efectuar la impresi�n");
				}	
			}
				
			document.all("hdnDeposId").value="";				
			document.all("hdnImprId").value="";
		}
		
        </SCRIPT>
	</HEAD>
	<body class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');Imprimir();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td style="WIDTH: 9px" width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Dep�sitos</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
											Width="100%" Visible="True">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																		ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 70px">
														<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																	<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="middle" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo" Width="87px">Fecha Desde:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 25px" background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																				<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 25px" vAlign="middle" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblBanco" runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 25px" background="imagenes/formfdofields.jpg">
																				<table border=0 cellpadding=0 cellspacing=0>
																				<tr>
																				<td>															
																				<cc1:combobox class="combo" id="cmbBanco" runat="server" Width="280px" Height="28px" onchange="CargaCombo('cmbBanco','cmbCuenta');"></cc1:combobox>
																				</td>
																				<td>
																					<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																													onclick="mBotonBusquedaAvanzada('bancos','banc_desc','cmbBanco','Bancos','@con_cuentas=1');"
																													alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																				</td>
																				</tr>
																				</table>
																			</TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 24px" vAlign="middle" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCuenta" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbCuenta" runat="server" Width="280px" nomoper="cuentas_bancos_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 5px" vAlign="middle" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCentroEmisor" runat="server" cssclass="titulo" Visible="False">Centro Emisor:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 5px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbCentroEmisor" runat="server" Width="280px" nomoper="emisores_ctros_cargar"
																					visible="false"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD style="HEIGHT: 95px" width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<!---fin filtro --->
								<tr>
									<td colspan="3" height="10"></td>
								</tr>
								<TR>
									<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" Height="100%" width="100%"
											AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
											AutoGenerateColumns="False">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="10px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="2%"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="depo_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="depo_fecha" HeaderStyle-HorizontalAlign="Center" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="_banc_id" HeaderText="banc_id"></asp:BoundColumn>
												<asp:BoundColumn DataField="_banc_desc" HeaderText="Banco">
													<HeaderStyle Width="50%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="depo_cuba_id" HeaderText="cuct_id"></asp:BoundColumn>
												<asp:BoundColumn DataField="_cuba_nume" HeaderText="N&#186; Cuenta">
													<HeaderStyle Width="50%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="depo_mone_id" HeaderText="mone_id"></asp:BoundColumn>
												<asp:BoundColumn DataField="_mone_desc" HeaderText="Moneda" HeaderStyle-HorizontalAlign="Center"
													ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
												<asp:BoundColumn DataField="depo_monto" HeaderText="Importe" HeaderStyle-HorizontalAlign="Center"
													ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
												<asp:BoundColumn DataField="_Cheque" HeaderText="Cheque" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colspan="3" height="10"></td>
								</tr>
								<TR id="tdNuevo" runat="server">
									<TD style="HEIGHT: 9px" colspan="3" vAlign="middle" height="9"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
											IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif"
											ForeColor="Transparent" ToolTip="Agregar un Nuevo Dep�sito" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="2"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" Height="124%" width="100%">
											<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
															cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> Dep�sito</asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkDetalle" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
															cssclass="solapa" Width="70px" Height="21px" CausesValidation="False"> Cheques</asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" Height="116px" width="100%">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD style="WIDTH: 100%" vAlign="top" align="right">
														<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="TablaCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="HEIGHT: 16px" align="right">
																		<asp:Label id="lblFechaNueva" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="2" height="16">
																		<cc1:DateBox id="txtFechaNueva" runat="server" cssclass="cuadrotexto" Width="68px" Obligatorio="True"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 15px" align="right">
																		<asp:Label id="lblBancoNuevo" runat="server" cssclass="titulo">Banco:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 15px" colSpan="2" height="15">
																	<table border=0 cellpadding=0 cellspacing=0>
																	<tr>
																	<td>
																		<cc1:combobox class="combo" id="cmbBancoNuevo" runat="server" Width="280px" onchange="CargaCombo('cmbBancoNuevo','cmbCuentaNueva');ConsultarEfectivo(document.all('cmbCuentaNueva'));" Obligatorio="True"></cc1:combobox>
																	</td>
																	<td>
																		<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																										onclick="mBotonBusquedaAvanzada('bancos','banc_desc','cmbBancoNuevo','Bancos','@con_cuentas=1');"
																										alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</td>
																	</tr>
																	</table>
																	</TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 16px" align="right">
																		<asp:Label id="lblMoneda" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="2" height="16">
																		<cc1:combobox class="combo" id="cmbMoneda" runat="server" Width="100px" onchange="CargaCombo('cmbBancoNuevo','cmbCuentaNueva');ConsultarEfectivo(document.all('cmbCuentaNueva'));"
																			Obligatorio="True"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 16px" align="right">
																		<asp:Label id="lblCuentaNueva" runat="server" cssclass="titulo">Cuenta:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="2" height="16">
																		<cc1:combobox class="combo" id="cmbCuentaNueva" runat="server" Width="280px" nomoper="cuentas_bancos_cargar" onchange="ConsultarEfectivo(this);" Obligatorio="True"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 16px" align="right">
																		<asp:Label id="lblNumeroDeposito" runat="server" cssclass="titulo">N� Boleta:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="2" height="16">
																		<CC1:TEXTBOXTAB id="txtNumeroDeposito" runat="server" cssclass="cuadrotexto" Width="160px" Enabled="False"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 16px" align="right">
																		<asp:Label id="lblEfectivo" runat="server" cssclass="titulo">Efectivo:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="2" height="16">
																		<cc1:NUMBERBOX id="txtEfectivo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="false"
																			AceptaNull="false" EsDecimal="True"></cc1:NUMBERBOX>&nbsp;<asp:Label id="lblEfectivoMuestra" runat="server" cssclass="titulo"></asp:Label></TD>
																</TR>
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 16px" align="right">
																		<asp:Label id="Label1" runat="server" cssclass="titulo">Total Cheques:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="2" height="16">
																		<cc1:NUMBERBOX id="txtCheques" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="false" enabled = false
																			AceptaNull="false" EsDecimal="True"></cc1:NUMBERBOX>&nbsp;<asp:Label id="Label2" runat="server" cssclass="titulo"></asp:Label>
																			&nbsp;<asp:Label id="lblChequesMuestra" runat="server" cssclass="titulo"></asp:Label></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panDetalle" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE id="TablaDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 100%" colSpan="2">
																		<asp:datagrid id="grdCheques" runat="server" Visible="true" BorderStyle="None" BorderWidth="1px"
																			AutoGenerateColumns="False" OnEditCommand="mEliminarCheque" OnPageIndexChanged="DataGridCheques_Page"
																			CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True"
																			width="100%" OnDeleteCommand="mEliminarCheque" ShowFooter="false" PageSize="10">
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="10px" CssClass="header"></HeaderStyle>
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="20px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="lbtEliminarReg" runat="server" Text="Editar" CausesValidation="false" CommandName="Delete">
																							<img onclick="javascript:return(confirm('Desea eliminar el cheque de la lista.'))" src='imagenes/del.gif'
																								border="0" alt="Eliminar Cheque" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="_cheq_id" HeaderText="ID"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_banc_desc" HeaderText="Banco">
																					<HeaderStyle Width="80%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="_cheq_nume" HeaderText="N&#186;"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_cheq_impo" HeaderText="Importe">
																					<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																					<ItemStyle HorizontalAlign="Right"></ItemStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn Visible="False" DataField="_clea_id" HeaderText="clea_id"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_clea_desc" HeaderText="Clearing">
																					<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
																					<ItemStyle HorizontalAlign="Right"></ItemStyle>
																				</asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 90%" align="right">
																		<asp:Label id="lblTotalCheques" runat="server" cssclass="titulo">Total Cheques:</asp:Label>&nbsp;
																		<cc1:NUMBERBOX id="txtTotalCheques" runat="server" cssclass="textomonto" Width="100px" EsDecimal="True"
																			enabled="false"></cc1:NUMBERBOX>&nbsp;&nbsp;&nbsp;</TD>
																	<TD style="WIDTH: 10%">
																		<asp:Button id="btnConsulCheq" runat="server" cssclass="boton" Width="80px" Text="Cheques"></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
										<DIV></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td style="WIDTH: 9px" width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>			
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="hdnExiste" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnChequesOriginales" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnCenEmId" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="hdnClearingId" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnBancId" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="hdnDatosPop" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnValoresCheque" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnChequesBorrados" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnDeposId" runat="server" AutoPostBack="True"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnImprId" runat="server"></ASP:TEXTBOX>
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT></OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();
		if (document.all('hdnExiste').value=='S')
		{
			if (confirm('Ya existe una boleta de deposito con el mismo importe para la misma cuenta. Desea darlo de alta?'))
			{	
				document.all('btnAlta').disabled = true;				
				__doPostBack('btnAlta','');
			}
			else
				document.all('hdnExiste').value = '';
		}
		</SCRIPT>
	</body>
</HTML>
