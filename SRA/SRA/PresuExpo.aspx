<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.PresuExpo" CodeFile="PresuExpo.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="controles/usrSociosFiltro.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Control Presupuestario</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">
function expandir()
{
	try{ parent.frames("menu").CambiarExp();}catch(e){;}
}	
function mProcesar()
{
	if (/*document.all("cmbMesD").selectedIndex == 0 || */document.all("txtAnioD").value == '')
	{
		alert('Debe seleccionar el mes y a�o desde.');
		return(false);
	}


	var bRet;
	bRet=confirm('Confirma la generaci�n del archivo ?');
	
	if(bRet)
	{	
		document.all("divgraba").style.display ="none";
		document.all("divproce").style.display ="inline";
	}
	return(bRet); 
}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Control Presupuestario</asp:label></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3">
											<asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table4" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnCons" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																			ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																			ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent" visible="false"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			BackColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"
																			visible="false"></CC1:BotonImagen></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE id="Table6" cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 100%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg"
																					colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 100%; HEIGHT: 24px" vAlign="top" background="imagenes/formfdofields.jpg"
																					colSpan="2">&nbsp;&nbsp;&nbsp;&nbsp;
																					<asp:Label id="lblMesD" runat="server" cssclass="titulo">Mes:</asp:Label>&nbsp;
																					<cc1:combobox class="combo" id="cmbMesD" runat="server" Width="184px"></cc1:combobox>&nbsp;
																					<asp:Label id="lblAnioD" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;
																					<cc1:numberbox id="txtAnioD" runat="server" cssclass="cuadrotexto" Width="72px" MaxLength="4" MaxValor="2090"
																						CantMax="4" AceptaNull="False"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD vAlign="middle" colspan="3" align="left">
											<br>
											<asp:panel id="panGrab" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
												Width="30%" BorderColor="#003784">
												<TABLE id="Table5" cellSpacing="0" cellPadding="10" width="90%" align="center" border="0">
													<TR>
														<TD style="WIDTH: 4px" align="right" height="8"></TD>
														<TD style="WIDTH: 254px" height="8"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 4px" noWrap align="left"></TD>
														<TD style="WIDTH: 254px" vAlign="bottom" align="center">
															<DIV id="divgraba" style="DISPLAY: inline">
																<asp:panel id="pangraba" runat="server" cssclass="titulo" width="100%">
																	<CC1:BotonImagen id="btnGrab" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																		ImageOver="btnGrab2.gif" ImageBoton="btnGrab.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																		ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent" ToolTip="Grabar" ImageDisable="btnGrab0.gif"></CC1:BotonImagen>
																</asp:panel></DIV>
															<DIV id="divproce" style="DISPLAY: none">
																<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
																	<asp:Image id="Image1" runat="server" ImageUrl="imagenes/btnProce.gif"></asp:Image>
																</asp:panel></DIV>
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 4px" vAlign="top" noWrap align="left" height="30"></TD>
														<TD style="WIDTH: 254px" vAlign="bottom" height="6"></TD>
													</TR>
												</TABLE>
											</asp:panel>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO --->
						</td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnExpo" runat="server" Width="140px"></ASP:TEXTBOX>
			</DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TR></TBODY></TABLE></TR></TBODY></TABLE>
		<script language="javascript">
		if (document.all["editar"]!= null && document.all("panGrab")!=null )
			document.location='#editar';
		if (document.all("hdnExpo").value!='')
		{
			var winArch = window.open('exportaciones/'+document.all("hdnExpo").value);
			document.all("hdnExpo").value='';
		}
		</script>
		</FORM>
	</BODY>
</HTML>
