<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CuotasDeve" CodeFile="CuotasDeve.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
<title>Devengamiento de Cuotas de Socios</title>
<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
<meta content="JavaScript" name="vs_defaultgrupntScript">
<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
<script language="JavaScript" src="includes/utiles.js"></script>
<script language="JavaScript" src="includes/paneles.js"></script>
<script>
			function mDevengar()
		{
			if (document.all("cmbDeveFil").selectedIndex == 0)
			{
				alert('Debe indicar el per�odo a devengar.');
				return(false);
			}
		
			
			var bRet;
			bRet=confirm('Confirma el devengamiento para el per�odo indicado?');
			
			if(bRet)
			{	
				document.all("divgraba").style.display ="none";
				document.all("divproce").style.display ="inline";
			}
			return(bRet); 
		}
		</script>
</SCRIPT>
</HEAD>
<BODY class="pagina" leftMargin="5" topMargin="5" onload="javascript:gSetearTituloFrame();"
		rightMargin="0">
<form id="frmABM" method="post" runat="server">
<!------------------ RECUADRO ------------------->
<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
<tr>
<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
</tr>
<tr>
<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
<td vAlign="middle" align="center">
<!----- CONTENIDO ----->
<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD width="100%" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Devengamiento de Cuotas de Socios</asp:label></TD>
</TR>
<TR>
<TD width="100%" colSpan="3"></TD>
</TR>
<TR>
<TD></TD>
<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="97%"
										BorderWidth="0" Visible="True">
<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
<TR>
<TD style="WIDTH: 100%">
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD style="HEIGHT: 8px" width="24"></TD>
<TD style="HEIGHT: 8px" width="42"></TD>
<TD style="HEIGHT: 8px" width="26"></TD>
<TD style="HEIGHT: 8px"></TD>
<TD style="HEIGHT: 8px" width="26"></TD>
<TD style="HEIGHT: 8px" width="26"></TD>
</TR>
<TR>
<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" width="7" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD align="right">
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD align="right"><!-- FOMULARIO -->
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
</TR>
<TR>
<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
<asp:Label id="lblSociFil" runat="server" cssclass="titulo">Socio:</asp:Label>&nbsp;</TD>
<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
<UC1:CLIE id="usrSociFil" runat="server" PermiModi="True" FilCUIT="True" width="100%" FilDocuNume="True"
																				MuestraDesc="False"
																				FilClieNume="True"
																				FilSociNume="True"
																				Saltos="1,1,1"
																				Tabla="Socios"></UC1:CLIE></TD>
</TR>
<TR>
<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
</TR>
<TR>
<TD style="WIDTH: 20%; HEIGHT: 10px" noWrap align="right" background="imagenes/formfdofields.jpg">&nbsp;
<asp:Label id="lblCateFil" runat="server" cssclass="titulo">Per�odo a Devengar:</asp:Label>&nbsp;</TD>
<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
<cc1:combobox class="combo" id="cmbDeveFil" runat="server" Width="240px" AceptaNull="False" Obligatorio="True"></cc1:combobox>&nbsp;</TD>
</TR>
<TR>
<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
</TR>
</TABLE>
</TD>
<TD width="2"></TD>
</TR>
<TR>
<TD colSpan="3" height="10"></TD>
</TR>
</TABLE>
<DIV id="divgraba" style="DISPLAY: inline">
<asp:panel id="pangraba" runat="server" cssclass="titulo" width="100%">
<CC1:BotonImagen id="btnGene" runat="server" BorderStyle="None" ImageDisable="btnGrab0.gif" ImageUrl="imagenes/btnImpr.jpg"
																BackColor="Transparent" ImageOver="btnGrab2.gif"
																ImageBoton="btnGrab.gif" BtnImage="edit.gif"
																OutImage="del.gif" CambiaValor="False"
																ImagesUrl="imagenes/" IncludesUrl="includes/"
																ForeColor="Transparent" ToolTip="Grabar devengamiento de cuotas"></CC1:BotonImagen>
</asp:panel></DIV>
<DIV id="divproce" style="DISPLAY: none">
<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
<asp:Image id="Image1" runat="server" ImageUrl="imagenes/btnProce.gif"></asp:Image>
</asp:panel></DIV>
</TD>
</TR>
<TR>
<TD></TD>
</TR>
</TABLE>
</asp:panel></TD>
</TR>
<TR>
<TD colSpan="3" height="10"></TD>
</TR>
<TR>
<TD><a name="editar"></a></TD>
<TD align="center" colSpan="2">
<DIV>&nbsp;</DIV>
</TD>
</TR>
</TABLE>
<!--- FIN CONTENIDO ---></td>
<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
</tr>
<tr>
<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
</tr>
</table>
<!----------------- FIN RECUADRO ----------------->
<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
</form>
<SCRIPT language="javascript">
		if (document.all["editar"]!= null && document.all["panDato"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
	    
		</SCRIPT>
</BODY>
</HTML>
