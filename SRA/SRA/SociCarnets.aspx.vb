Namespace SRA

Partial Class SociCarnets
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Carnets

   Private mstrConn As String
   Private mstrSociId As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      mstrSociId = Request.QueryString("soci_id")
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mSetearMaxLength()

            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub


   Private Sub mSetearMaxLength()
      Dim lstrLong As Object

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "carn_obse")
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

        Public Sub mCargarDatos(ByVal pstrId As String)

            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            With ldsEsta.Tables(0).Rows(0)
                hdnId.Text = .Item("carn_id").ToString()

                txtSoliFecha.Fecha = IIf(.Item("carn_soli_fecha").ToString.Trim.Length > 0, .Item("carn_soli_fecha"), String.Empty)

                txtObse.Valor = .Item("carn_obse")
                txtDispFecha.Fecha = IIf(.Item("carn_disp_fecha").ToString.Trim.Length > 0, .Item("carn_disp_fecha"), String.Empty)
                txtEntrFecha.Fecha = IIf(.Item("carn_entr_fecha").ToString.Trim.Length > 0, .Item("carn_entr_fecha"), String.Empty)

            End With

            mSetearEditor("", False)
        End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""

      txtSoliFecha.Text = ""
      txtObse.Text = ""
      txtDispFecha.Text = ""
      txtEntrFecha.Text = ""

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Alta()

         mConsultar()

         mLimpiar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         If Not ldsEstruc.Tables(0).Rows(0).IsNull("carn_entr_fecha") Then
            Dim lstrRptName As String = "CarnetRecibo"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&carn_id=" + hdnId.Text

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            hdnRpt.Text = lstrRpt
         End If

         mConsultar()

         mLimpiar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         mConsultar()

         mLimpiar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

        Private Function mGuardarDatos() As DataSet

            mValidarDatos()

            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

            With ldsEsta.Tables(0).Rows(0)
                .Item("carn_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("carn_soli_fecha") = IIf(txtSoliFecha.Fecha.Trim.Length > 0, txtSoliFecha.Fecha, DBNull.Value)
                .Item("carn_obse") = txtObse.Valor
                .Item("carn_disp_fecha") = IIf(txtDispFecha.Fecha.Trim.Length > 0, txtDispFecha.Fecha, DBNull.Value)
                .Item("carn_entr_fecha") = IIf(txtEntrFecha.Fecha.Trim.Length > 0, txtEntrFecha.Fecha, DBNull.Value)
                .Item("carn_soci_id") = mstrSociId
            End With

            Return ldsEsta
        End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @carn_soci_id=" + mstrSociId)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class

End Namespace
