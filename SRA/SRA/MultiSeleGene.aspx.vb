Namespace SRA

Partial Class MultiSeleGene
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents cmbOpci As NixorControls.ComboBox
    Private mstrTabla As String
    Private mstrFiltros As String
    Private mstrSele As String
    Private mstrCtrol As String
    Private mstrConn As String
    Private mstrCmd As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mCargarCombos()
            End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#Region "Inicializacion de Variables"

   Public Sub mCargarCombos()

        Dim lstrSele As String = "," & mstrSele & ","

        mstrCmd = mstrTabla & "_multi_cargar "
        If mstrFiltros <> "" Then
           mstrCmd = mstrCmd & "@filtros=" & clsSQLServer.gFormatArg(mstrFiltros, SqlDbType.VarChar)
        End If

        clsWeb.gCargarCombo(mstrConn, mstrCmd, lstDatos, "id", "descrip", "")

        For Each loItem As ListItem In lstDatos.Items
            If lstrSele.IndexOf("," & loItem.Value & ",") <> -1 Then
               loItem.Selected = True
            Else
               loItem.Selected = False
            End If
        Next

   End Sub

   Public Sub mInicializar()
        lblTitu.Text = "Selecci�n de " & Request.QueryString("tit")
        mstrTabla = Request.QueryString("tab")
        mstrCtrol = Request.QueryString("ctrl")
        mstrFiltros = Request.QueryString("fil")
        mstrSele = Request.QueryString("sel")
   End Sub
#End Region

#Region "Selecci�n de Filtros"
    Private Sub btnSele_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSele.Click
        mSeleccionar()
    End Sub

    Public Sub mSeleccionar()

      Dim lsbMsg As New StringBuilder
      Dim lstrResu As String = ""

      lstrResu = lstDatos.ObtenerIDs()

      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.document.all['{0}'].value='{1}';", mstrCtrol, lstrResu))
      lsbMsg.Append(String.Format("window.opener.mActualizarSeleccion('{0}','{1}');", mstrCtrol, mstrTabla))
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")

      Response.Write(lsbMsg.ToString)

    End Sub

#End Region

End Class

End Namespace
