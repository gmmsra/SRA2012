Namespace SRA

Partial Class AnillosEnvio
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Anillos_Movim_Cabe
   Private mstrTablaDetalle As String = SRA_Neg.Constantes.gTab_Anillos_Movim_Deta
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtNumeroDespacho.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "amca_desp_nume")
      txtDireccion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "amca_dire")
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "amca_obse")

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDetalle)
      txtNumeDesde.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "amde_desde_nume")
      txtNumeHasta.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "amde_hasta_nume")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDetalle.EditItemIndex = -1
         If (grdDetalle.CurrentPageIndex < 0 Or grdDetalle.CurrentPageIndex >= grdDetalle.PageCount) Then
            grdDetalle.CurrentPageIndex = 0
         Else
            grdDetalle.CurrentPageIndex = E.NewPageIndex
         End If
         grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
         grdDetalle.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul"
			mstrCmd = mstrCmd & " @crdr_id=" + clsSQLServer.gFormatArg(usrCriaFil.Valor, SqlDbType.Int)
         mstrCmd = mstrCmd & ",@todo = " + IIf(chkTodo.Checked, "1", "0")
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnModi.Enabled = Not (pbooAlta)
   End Sub
   Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
      btnModiDeta.Enabled = Not (pbooAlta)
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtFechaEnvio.Fecha = .Item("amca_envi_fecha")
            txtNumeroDespacho.Valor = .Item("amca_desp_nume")
            txtDireccion.Valor = .Item("amca_dire")
            txtObse.Valor = .Item("amca_obse")

            If Not .IsNull("amca_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("amca_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
         mShowTabs(1)
      End If
   End Sub
   Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrDeta As DataRow

         hdnDetaId.Text = E.Item.Cells(1).Text
         ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("amde_id=" & hdnDetaId.Text)(0)

         With ldrDeta
            lblAnilloDeta.Text = "Anillo: " & .Item("_anillo")
            txtNumeDesde.Valor = .Item("_NumeroDesde") '.Item("amde_desde_nume")
            txtNumeHasta.Valor = .Item("amde_hasta_nume")
            hdnCanti.Text = .Item("amde_cant")
         End With
         mSetearEditorDeta(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaDetalle
      mdsDatos.Tables.Remove(mdsDatos.Tables(2))

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If
      grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
      grdDetalle.DataBind()

      Session(mstrTabla) = mdsDatos
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""

      txtFechaEnvio.Text = ""
      txtNumeroDespacho.Text = ""
      txtDireccion.Text = ""
      txtObse.Text = ""

      mLimpiarDeta()

      grdDato.CurrentPageIndex = 0
      grdDetalle.CurrentPageIndex = 0

      mCrearDataSet("")
      mShowTabs(1)
      mSetearEditor(True)
   End Sub
   Private Sub mLimpiarDeta()
      hdnDetaId.Text = ""
      lblAnilloDeta.Text = ""
      txtNumeDesde.Text = ""
      txtNumeHasta.Text = ""
      hdnCanti.Text = ""
      mSetearEditorDeta(True)
   End Sub
   Private Sub mLimpiarFil()
      usrCriaFil.Limpiar()
      chkTodo.Checked = False
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      panSolapas.Visible = pbooVisi
      panBotones.Visible = pbooVisi
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If mdsDatos.Tables(mstrTablaDetalle).Select("amde_desde_nume is null or amde_hasta_nume is null").GetUpperBound(0) > -1 Then
         Throw New AccesoBD.clsErrNeg("Debe completar todos los N�meros de Anillos.")
      End If
   End Sub
   Private Sub mGuardarDatos()
      mValidaDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("amca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("amca_envi_fecha") = txtFechaEnvio.Fecha
         .Item("amca_desp_nume") = txtNumeroDespacho.Valor
         .Item("amca_dire") = txtDireccion.Valor
         .Item("amca_obse") = txtObse.Valor
         .Item("amca_audi_user") = Session("sUserId").ToString()
      End With
   End Sub
   'Seccion de detalle
   Private Sub mActualizarDeta()
      Try
         mGuardarDatosDeta()

         mLimpiarDeta()
         grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
         grdDetalle.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosDeta()
      Dim ldrDeta As DataRow

      mValidarDatosDeta()

      If hdnDetaId.Text = "" Then
         ldrDeta = mdsDatos.Tables(mstrTablaDetalle).NewRow
         ldrDeta.Item("amde_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "amde_id")
      Else
         ldrDeta = mdsDatos.Tables(mstrTablaDetalle).Select("amde_id=" & hdnDetaId.Text)(0)
      End If

      With ldrDeta
         .Item("amde_desde_nume") = txtNumeDesde.Valor
         .Item("amde_hasta_nume") = txtNumeHasta.Valor
      End With
      If (hdnDetaId.Text = "") Then
         mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrDeta)
      End If
   End Sub
   Private Sub mValidarDatosDeta()
      If txtNumeDesde.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el N�mero Desde.")
      End If

      If txtNumeHasta.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el N�mero Hasta.")
      End If

      If (txtNumeDesde.Valor + CInt(hdnCanti.Text)) - 1 < txtNumeHasta.Valor Then
         Throw New AccesoBD.clsErrNeg("Los N�meros superan la Cantidad total de anillos.")
      End If

      If (txtNumeDesde.Valor + CInt(hdnCanti.Text)) - 1 > txtNumeHasta.Valor Then
         Throw New AccesoBD.clsErrNeg("Los N�meros no cubren la Cantidad total de anillos.")
      End If
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panGeneral.Visible = False
         lnkGeneral.Font.Bold = False
         panDetalle.Visible = False
         lnkDetalle.Font.Bold = False
         Select Case Tab
            Case 1
               panGeneral.Visible = True
               lnkGeneral.Font.Bold = True
            Case 2
               panDetalle.Visible = True
               lnkDetalle.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
      mCerrar()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      mConsultar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   'Botones Generales
   Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   'Botones Detalle
   Private Sub btnModiDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
      mActualizarDeta()
   End Sub
   Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
      mLimpiarDeta()
   End Sub
   'Solapas
   Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDetalle.Click
      mShowTabs(2)
   End Sub
#End Region

End Class

End Namespace
