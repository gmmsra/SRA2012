<%@ Reference Control="~/controles/usrsociosfiltro.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.SociosExpo" CodeFile="SociosExpo.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="controles/usrSociosFiltro.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Exportar Padr�n de Socios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">
		function mRptFiltrosLimpiar()
		{
			EjecutarMetodoXML("Utiles.RptFiltrosLimpiar",document.all("hdnRptId").value);
		}
		function expandir()
		{
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}	
		</script>
</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0"
		onunload="javascript:mRptFiltrosLimpiar();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Exportar Padr�n de Socios</asp:label></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3">
											<asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
												BorderStyle="Solid">
            <TABLE id=TableFil style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
            align=left border=0>
              <TR>
                <TD style="WIDTH: 100%">
                  <TABLE id=Table4 cellSpacing=0 cellPadding=0 width="100%" 
                  border=0>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24></TD>
                      <TD style="HEIGHT: 8px" width=42></TD>
                      <TD style="HEIGHT: 8px" width=26></TD>
                      <TD style="HEIGHT: 8px"></TD>
                      <TD style="HEIGHT: 8px" width=26>
<CC1:BotonImagen id=btnCons runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BotonImagen></TD>
                      <TD style="HEIGHT: 8px" width=26>
<CC1:BotonImagen id=btnLimpFil runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg" BackColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BotonImagen></TD></TR>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24><IMG height=25 
                        src="imagenes/formfle.jpg" width=24 border=0></TD>
                      <TD style="HEIGHT: 8px" width=42><IMG height=25 
                        src="imagenes/formtxfiltro.jpg" width=113 border=0></TD>
                      <TD style="HEIGHT: 8px" width=26><IMG height=25 
                        src="imagenes/formcap.jpg" width=26 border=0></TD>
                      <TD style="HEIGHT: 8px" background=imagenes/formfdocap.jpg 
                      colSpan=3><IMG height=25 src="imagenes/formfdocap.jpg" 
                        width=7 border=0></TD></TR></TABLE></TD></TR>
              <TR>
                <TD>
                  <TABLE id=Table6 cellSpacing=0 cellPadding=0 width="100%" 
                  border=0>
                    <TR>
                      <TD width=3 background=imagenes/formiz.jpg><IMG 
                        height=30 src="imagenes/formiz.jpg" width=3 border=0></TD>
                      <TD><!-- FOMULARIO -->
                        <TABLE id=Table6 cellSpacing=0 cellPadding=0 
                        width="100%" border=0>
                          <TR>
                            <TD style="HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg 
                          colSpan=2></TD></TR>
                          <TR>
                            <TD style="WIDTH: 100%; HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg colSpan=2>
<UC1:SOCFIL id=usrSocFil runat="server"></UC1:SOCFIL></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 100%; HEIGHT: 24px" vAlign=top 
                            background=imagenes/formfdofields.jpg colSpan=2>
<asp:label id=lblCamp runat="server" cssclass="titulo">Campos a Incluir</asp:label>&nbsp; 
<cc1:checklistbox id=lisCamp runat="server" width="100%" CellPadding="0" CellSpacing="0" CssClass="chklst" height="100px" RepeatColumns="2" MuestraBotones="True"></cc1:checklistbox></TD></TR>
                          <TR>
                            <TD align=right background=imagenes/formdivmed.jpg 
                            colSpan=2 height=2><IMG height=2 
                              src="imagenes/formdivmed.jpg" width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 25%; HEIGHT: 24px" vAlign=top 
                            align=right background=imagenes/formfdofields.jpg>
<asp:label id=lblOrde runat="server" cssclass="titulo">Ordenar por:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 75%; HEIGHT: 24px" vAlign=top 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox class=combo id=cmbOrde runat="server" Width="192px">
																						<asp:ListItem Value="clie_apel" Selected="True">Apellido</asp:ListItem>
																						<asp:ListItem Value="soci_nume">Nro. de Socio</asp:ListItem>
																					</cc1:combobox></TD></TR>
                          <TR>
                            <TD style="HEIGHT: 10px" align=right 
                            background=imagenes/formfdofields.jpg 
                          colSpan=2></TD></TR></TABLE></TD>
                      <TD width=2 background=imagenes/formde.jpg><IMG height=2 
                        src="imagenes/formde.jpg" width=2 
                  border=0></TD></TR></TABLE></TD></TR></TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD vAlign="middle" align="left" colSpan="3" height="30">
											<asp:label id="lblPrev" runat="server" cssclass="titulo" Visible="False">Vista Previa con los primeros 30 registros</asp:label></TD>
									</TR>
									<TR>
										<TD vAlign="top" colspan="3" align="center">
											<asp:datagrid id="grdCons" runat="server" BorderStyle="None" BorderWidth="1px" CellSpacing="1"
												CellPadding="1" width="100%" AllowPaging="True" HorizontalAlign="Center" GridLines="None"
												OnPageIndexChanged="DataGrid_Page" PageSize="10" ItemStyle-Height="5px" AutoGenerateColumns="False">
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<FooterStyle CssClass="footer"></FooterStyle>
												<Columns>
													<asp:TemplateColumn Visible="false">
														<HeaderStyle Width="5%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit" 
 Height="5">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid>
										</TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD vAlign="middle" colspan="3" align="left"><A id="editar" name="editar"></A>
											<asp:panel id="panGrab" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
												Width="50%" Visible="False" BorderColor="#003784">
            <TABLE id=Table5 cellSpacing=0 cellPadding=0 width="90%" 
            align=center border=0>
              <TR>
                <TD style="WIDTH: 164px" align=right height=8></TD>
                <TD style="WIDTH: 201px" height=8></TD>
                <TD height=8></TD></TR>
              <TR>
                <TD style="WIDTH: 164px" noWrap align=left>
<asp:label id=lblArch runat="server" cssclass="titulo">Nombre Archivo:</asp:label>
<cc1:TextBoxtab id=txtArch runat="server" cssclass="cuadrotexto" Width="155px"></cc1:TextBoxtab></TD>
                <TD style="WIDTH: 201px" noWrap>
<asp:label id=lblForm runat="server" cssclass="titulo">Formato:</asp:label>
<cc1:combobox class=combo id=cmbForm runat="server" Width="176px">
																<asp:ListItem Value="XLS" Selected="True">Microsoft Excel Workbook</asp:ListItem>
																<asp:ListItem Value="CSV">CSV (Comma Delimited)</asp:ListItem>
																<asp:ListItem Value="DBF">DBF (dBASE)</asp:ListItem>
															</cc1:combobox></TD>
                <TD vAlign=bottom align=center>
<CC1:BotonImagen id=btnGrab runat="server" BorderStyle="None" ImageUrl="imagenes/btnCons.gif" BackColor="Transparent" ImageOver="btnGrab2.gif" ImageBoton="btnGrab.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="../includes/" ForeColor="Transparent" ImageDisable="btnGrab0.gif"></CC1:BotonImagen></TD></TR>
              <TR>
                <TD style="WIDTH: 164px" vAlign=top noWrap align=left height=30>
<asp:checkbox id=chkTitu Visible="true" CssClass="titulo" Runat="server" Text="Incluir T�tulos"></asp:checkbox></TD>
                <TD style="WIDTH: 201px" noWrap height=6></TD>
                <TD vAlign=bottom 
        height=6></TD></TR></TABLE>
											</asp:panel>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO --->
						</td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnExpo" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnRptId" runat="server" Width="140px"></ASP:TEXTBOX>
			</DIV>
		</form></TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TR></TBODY></TABLE></TR></TBODY></TABLE>
		<script language="javascript">
		if (document.all["editar"]!= null && document.all("panGrab")!=null )
			document.location='#editar';
		if (document.all("hdnExpo").value!='')
		{
			var winArch = window.open('exportaciones/'+document.all("hdnExpo").value);
			document.all("hdnExpo").value='';
		}
		</script>
</FORM>
	</BODY>
</HTML>
