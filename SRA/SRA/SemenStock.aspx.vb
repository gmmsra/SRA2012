Imports ReglasValida.Validaciones


Namespace SRA


Partial Class SemenStock
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    '  Protected WithEvents usrClie As usrClieDeriv

    '  Protected WithEvents usrCria As usrClieDeriv
    Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblCria As System.Web.UI.WebControls.Label
    Protected WithEvents lblClie As System.Web.UI.WebControls.Label
    Protected WithEvents lblClieFil As System.Web.UI.WebControls.Label

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_SemenStock
    Private mstrTablaDenuncia As String = SRA_Neg.Constantes.gTab_TeDenun
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                btnAlta.Attributes.Add("onclick", "return(mPorcPeti());")
                btnModi.Attributes.Add("onclick", "return(mPorcPeti());")
                btnBaja.Attributes.Add("onclick", "return(mPorcPeti());")
                mSetearMaxLength()
                mSetearEventos()
                mEstablecerPerfil()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            End If
            mdsDatos = Session(mstrTabla)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mEstablecerPerfil()
            'Dim lbooPermiAlta As Boolean
            'Dim lbooPermiModi As Boolean

        'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
        '   Response.Redirect("noaccess.aspx")
        'End If

        'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        'btnAlta.Visible = lbooPermiAlta

        'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        'btnModi.Visible = lbooPermiModi

        'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object
        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtCant.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "sest_cant")
    End Sub

    Public Sub mInicializar()
        usrProdFil.FilSexo = False
        usrProdFil.Sexo = 1
        usrProdu.AutoPostback = False
        usrProdu.Ancho = 790
        usrProdu.Alto = 510
        usrProdu.FilSexo = False
        usrProdu.Sexo = 1
    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_busq "
            mstrCmd = mstrCmd + "@cria_id=" + _
            IIf(usrProdFil.CriaOrPropId.ToString = "", "0", usrProdFil.CriaOrPropId.ToString.ToString)
            mstrCmd = mstrCmd + ",@prdt_id=" + IIf(usrProdFil.Valor.ToString = "", "0", usrProdFil.Valor.ToString)
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            grdDato.Visible = True
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, E.Item.Cells(1).Text)
        ldsEsta.Tables(0).TableName = mstrTabla

        With ldsEsta.Tables(0).Rows(0)
            hdnId.Text = .Item("sest_id")
            usrProdu.Valor = .Item("sest_prdt_id")
                txtCant.Valor = .Item("sest_cant")
                If .Item("sest_recu_fecha").ToString.Length > 0 Then
                    txtRecuFecha.Fecha = .Item("sest_recu_fecha")
                End If
                If .Item("sest_pres_fecha").ToString.Length > 0 Then
                    txtPresFecha.Fecha = .Item("sest_pres_fecha")
                End If
                usrProdu.CriaOrPropId = .Item("sest_cria_id")
                usrProdu.Valor = .Item("sest_prdt_id")
                If Not .IsNull("sest_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("sest_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If
            End With

        mSetearEditor(False)
        mMostrarPanel(True)
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        usrProdu.Limpiar()
        txtRecuFecha.Text = ""
        txtCant.Text = ""
        txtPresFecha.Text = ""
        lblBaja.Text = ""

        grdDato.CurrentPageIndex = 0
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarFiltro()

        usrProdFil.Limpiar()
        grdDato.CurrentPageIndex = 0
        grdDato.Visible = False
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        trbtnAgre.Visible = Not panDato.Visible
        btnAgre.Enabled = Not (panDato.Visible)
        panFiltro.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible


        mEstablecerPerfil()
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim lTransac As SqlClient.SqlTransaction
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)
            Dim ldsDatos As DataSet = mGuardarDatos(lTransac, "A")

            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim lTransac As SqlClient.SqlTransaction
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)
            Dim ldsDatos As DataSet = mGuardarDatos(lTransac, "M")
            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub
    Private Sub mValidarDatos()
        Dim msgError As String = "Error en la validacion del producto."

        Me.panBotones.Style.Add("display", "inline")
        Me.divproce.Style.Add("display", "none")

        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        If usrProdu.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el producto.")
        End If
        Dim mstrCmd As String
        mstrCmd = "exec p_rg_RecuSemenValida "
        mstrCmd = mstrCmd + "@prdt_id=" + IIf(usrProdu.Valor.ToString = "", "0", usrProdu.Valor.ToString)
        mstrCmd = mstrCmd + ",@FechaValor='" + Convert.ToDateTime(Me.txtRecuFecha.Fecha).ToString("yyyyMMdd") + "'"

        Try
            Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd, 9999)
            Dim tienePropiedad As String
            Dim esMachoDator As String
            Dim animalVigente As String
            Dim tieneSRAnume As String
            Dim esNacional As String

            With ldsDatos.Tables(0).Rows(0)
                tienePropiedad = .Item("tienePropiedad")
                esMachoDator = .Item("esMachoDator")
                animalVigente = .Item("animalVigente")
                tieneSRAnume = .Item("tieneSRAnume")
                esNacional = .Item("esNacional")
            End With

            If tienePropiedad <> "Si" Then
                msgError = "A la fecha informada no posee la propiedad del animal."
            End If
            If esMachoDator <> "Si" Then
                msgError = "El animal no es macho dador."
            End If
            If animalVigente <> "Si" Then
                msgError = "El animal esta dado de baja."
            End If
            If tieneSRAnume <> "Si" Then
                msgError = "El animal no posee numero de inscripcion SRA."
            End If
            If esNacional <> "Si" Then
                msgError = "El animal es extranjero, no puede realizar."
            End If

            If (msgError.Length > 0) Then
                Throw New AccesoBD.clsErrNeg("Error en la validacion del producto.")
            End If

        Catch ex As Exception
            Throw New AccesoBD.clsErrNeg(msgError)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try

    End Sub

    Private Function mGuardarDatos(ByVal lTransac As SqlClient.SqlTransaction, ByVal pOperacion As String) As DataSet
        Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
        Dim ldsDenuncia As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTablaDenuncia, hdnDenunciaId.Text)
        Dim lintCpo As Integer
        Dim ldrDatos As DataRow
        Dim ldrDatosDenuncia As DataRow
        Dim intClienteId As Integer
        Dim intDenunciaId As Integer
        Dim oCliente As New SRA_Neg.Clientes(mstrConn, Session("sUserId").ToString())
        Dim oSemenStock As New SRA_Neg.SemenStock(mstrConn, Session("sUserId").ToString())
        Dim odenuncia As New SRA_Neg.Denuncias(mstrConn, Session("sUserId").ToString())
        Dim intSemenStockId As String

        Try
            mValidarDatos()

            If pOperacion = "A" Then
                ldrDatosDenuncia = ldsDenuncia.Tables(mstrTablaDenuncia).NewRow
                ldrDatosDenuncia.Item("tede_id") = clsSQLServer.gObtenerId(ldsDenuncia.Tables(mstrTablaDenuncia), "tede_id")
                intDenunciaId = ldrDatosDenuncia.Item("tede_id")
            End If

            intClienteId = oCliente.GetClienteIdByRazaCriador(usrProdu.RazaId, usrProdu.CriaOrPropId)

            ' Por inconsistencia de datos puede ser que existan trates sin denuncia, en el caso de no exisir agrega la denuncia

            If pOperacion = "A" Then
                ldrDatosDenuncia.Item("tede_recu_fecha") = txtRecuFecha.Fecha
                ldrDatosDenuncia.Item("tede_gest_dias") = DBNull.Value
                ldrDatosDenuncia.Item("tede_embr_cong") = DBNull.Value
                ldrDatosDenuncia.Item("tede_embr_fres") = DBNull.Value
                ldrDatosDenuncia.Item("tede_impl_embr_cong") = DBNull.Value
                ldrDatosDenuncia.Item("tede_impl_embr_fres") = DBNull.Value
                ldrDatosDenuncia.Item("tede_serv_fecha") = DBNull.Value
                ldrDatosDenuncia.Item("tede_pad1_prdt_id") = usrProdu.Valor
                ldrDatosDenuncia.Item("tede_cria_id") = ValidarNulos(usrProdu.CriaOrPropId, False)
                ldrDatosDenuncia.Item("tede_clie_id") = intClienteId
                ldrDatosDenuncia.Item("tede_imcr_id") = DBNull.Value
                ldrDatosDenuncia.Item("tede_auim_id") = DBNull.Value
                ldrDatosDenuncia.Item("tede_raza_id") = usrProdu.RazaId
                ldrDatosDenuncia.Item("tede_nume") = DBNull.Value
                ldsDenuncia.Tables(mstrTablaDenuncia).Rows.Add(ldrDatosDenuncia)
                intDenunciaId = clsSQLServer.gAlta(lTransac, Session("sUserId").ToString(), _
                            mstrTablaDenuncia, ldrDatosDenuncia)
            End If

            If pOperacion = "A" Then
                ldrDatos = ldsDatos.Tables(0).NewRow
                ldrDatos.Item("sest_id") = clsSQLServer.gObtenerId(ldsDatos.Tables(0), "sest_id")
            Else
                ldrDatos = ldsDatos.Tables(0).Select()(0)

                Dim dtDenuncia As DataTable

                dtDenuncia = odenuncia.GetDenunciaBySemenId(hdnId.Text, "")

                If dtDenuncia.Rows.Count > 0 Then
                    intDenunciaId = dtDenuncia.Rows(0).Item("tede_id")
                End If
            End If

            With ldsDatos.Tables(0).Rows(0)
                ldrDatos.Item("sest_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                ldrDatos.Item("sest_prdt_id") = usrProdu.Valor
                ldrDatos.Item("sest_cant") = txtCant.Valor
                ldrDatos.Item("sest_recu_fecha") = txtRecuFecha.Fecha
                ldrDatos.Item("sest_pres_fecha") = txtPresFecha.Fecha
                ldrDatos.Item("sest_clie_id") = intClienteId
                ldrDatos.Item("sest_cria_id") = usrProdu.CriaOrPropId
                ldrDatos.Item("sest_prdt_id") = usrProdu.Valor
                ldrDatos.Item("sest_auto") = 0   ' manual
                ldrDatos.Item("sest_tipo_movi") = SRA_Neg.Constantes.TipoMovimientos.DenunciaRecuperacionSemen
                ldrDatos.Item("sest_tram_id") = DBNull.Value
                ldrDatos.Item("sest_tede_id") = intDenunciaId
            End With

            If pOperacion = "A" Then
                intSemenStockId = clsSQLServer.gAlta(lTransac, Session("sUserId").ToString(), mstrTabla, ldrDatos)
            Else
                clsSQLServer.gModi(lTransac, Session("sUserId").ToString(), mstrTabla, ldrDatos)
            End If

            clsSQLServer.gCommitTransac(lTransac)
            clsSQLServer.gCloseTransac(lTransac)
        Catch ex As Exception
            ' Rollback
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
        Return ldsDatos
    End Function
#End Region

#Region "Eventos de Controles"
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
#End Region

End Class
End Namespace
