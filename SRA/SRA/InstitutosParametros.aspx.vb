Namespace SRA

Partial Class InstitutosParametros
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Public mstrTabla As String = SRA_Neg.Constantes.gTab_Institutos
   Private mstrConn As String
   Public mstrTitulo As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            hdnInstId.Text = Convert.ToInt32(Request.QueryString("fkvalor"))

            Select Case hdnInstId.Text
               Case "1"
                  mstrTitulo = "Par�metros de ISEA"
               Case "2"
                  mstrTitulo = "Par�metros de CEIDA"
               Case "3"
                  mstrTitulo = "Par�metros de EGEA"
            End Select
            lblTituAbm.Text = mstrTitulo
            mCargarCombos()
            mConsultar()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mEstablecerPerfil()
      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Perfiles, String), (mstrConn), (Session("sUserId").ToString()))) Then
      '   Response.Redirect("noaccess.aspx")
      'End If
      'btnModificar.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
   End Sub

   Private Sub mCargarCentroCostos()
      Dim lstrCta1 As String = ""
      Dim lstrCta2 As String = ""
      Dim lstrCta3 As String = ""
      Dim lstrCta4 As String = ""
      If Not cmbMatrConc.Valor Is DBNull.Value And Not cmbCuotConc.Valor Is DBNull.Value And Not cmbCertConc.Valor Is DBNull.Value And Not cmbExamConc.Valor Is DBNull.Value Then
        lstrCta1 = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbCertConc.Valor)
        lstrCta1 = lstrCta1.Substring(0, 1)
        lstrCta2 = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbCuotConc.Valor)
        lstrCta2 = lstrCta2.Substring(0, 1)
        lstrCta3 = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbExamConc.Valor)
        lstrCta3 = lstrCta3.Substring(0, 1)
        lstrCta4 = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbMatrConc.Valor)
        lstrCta4 = lstrCta4.Substring(0, 1)
      End If
      If lstrCta1 <> "" And lstrCta1 <> "1" And lstrCta1 <> "2" And lstrCta1 <> "3" And lstrCta2 <> "" And lstrCta2 <> "1" And lstrCta2 <> "2" And lstrCta2 <> "3" And lstrCta1 = lstrCta2 And lstrCta1 = lstrCta3 And lstrCta1 = lstrCta4 Then
         clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta1, cmbCentroCost, "id", "descrip", "N")
         If Not Request.Form("cmbCentroCost") Is Nothing Then
            cmbCentroCost.Valor = Request.Form("cmbCentroCost").ToString
         End If
      Else
         cmbCentroCost.Items.Clear()
      End If
   End Sub

 Public Sub mConsultar()
  Try
   Dim lstrCmd As New StringBuilder
   Dim ldsEsta As New DataSet

   lstrCmd.Append("exec " + mstrTabla + "_consul ")
   lstrCmd.Append(hdnInstId.Text)
   ldsEsta = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

   With ldsEsta.Tables(0).Rows(0)
    cmbMatrConc.Valor = .Item("inse_matr_conc_id")
                cmbCuotConc.Valor = .Item("inse_cuot_conc_id")
                cmbCertConc.Valor = .Item("inse_cert_conc_id")
                cmbExamConc.Valor = .Item("inse_exam_conc_id")
                mCargarCentroCostos()
    cmbCentroCost.Valor = .Item("inse_ccos_id")
    cmbFormaPago.Valor = .Item("inse_cpti_id")
    txtDescDeb.Valor = .Item("inse_deau_porc")
    txtCuotaDescSocio.Valor = .Item("inse_soci_porc")
    txt1CuotaDescSocio.Valor = .Item("inse_1soci_porc")
    txt2CuotaDescSocio.Valor = .Item("inse_2soci_porc")
    txtMatrDescSocio.Valor = .Item("inse_matr_soci_porc")
    txt1MatrDescSocio.Valor = .Item("inse_1matr_soci_porc")
    txt2MatrDescSocio.Valor = .Item("inse_2matr_soci_porc")
                    txtNotaAprob.Valor = IIf(.Item("inse_aprob_calif").ToString.Length > 0, .Item("inse_aprob_calif"), String.Empty)


    txtPorcAsis.Valor = .Item("inse_asis_porc")
                txtExamImpo.Valor = .Item("inse_exam_impo")
            End With
        Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub

 Private Sub mCargarCombos()
  clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbMatrConc, "S", "@conc_grava=0")
        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbCuotConc, "S", "@conc_grava=0")
        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbCertConc, "S", "@conc_grava=0")
        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbExamConc, "S", "@conc_grava=0")
        'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCentroCost, "S")
  clsWeb.gCargarRefeCmb(mstrConn, "comprob_pagos_tipos", cmbFormaPago, "S", "@factura=1")
 End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mConsultar()
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

	Private Function mGuardarDatos() As DataSet
		mValidarDatos()
		Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnInstId.Text)
		With ldsEsta.Tables(0).Rows(0)
			.Item("inse_id") = hdnInstId.Text
			.Item("inse_matr_conc_id") = cmbMatrConc.Valor
            .Item("inse_cuot_conc_id") = cmbCuotConc.Valor
            .Item("inse_cert_conc_id") = cmbCertConc.Valor
            .Item("inse_exam_conc_id") = cmbExamConc.Valor
			.Item("inse_ccos_id") = cmbCentroCost.Valor
			.Item("inse_cpti_id") = cmbFormaPago.Valor
			.Item("inse_deau_porc") = txtDescDeb.Valor
			.Item("inse_soci_porc") = txtCuotaDescSocio.Valor
			.Item("inse_1soci_porc") = txt1CuotaDescSocio.Valor
			.Item("inse_2soci_porc") = txt2CuotaDescSocio.Valor
			.Item("inse_matr_soci_porc") = txtMatrDescSocio.Valor
			.Item("inse_1matr_soci_porc") = txt1MatrDescSocio.Valor
			.Item("inse_2matr_soci_porc") = txt2MatrDescSocio.Valor
                .Item("inse_aprob_calif") = IIf(txtNotaAprob.Valor.ToString.Length > 0, txtNotaAprob.Valor, DBNull.Value)
                .Item("inse_asis_porc") = txtPorcAsis.Valor
            .Item("inse_exam_impo") = txtExamImpo.Valor
        End With
        Return ldsEsta
	End Function
#End Region

   Private Sub btnModificar_click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModificar.Click
      mModi()
   End Sub
End Class
End Namespace
