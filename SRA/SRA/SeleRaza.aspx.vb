Namespace SRA

Partial Class SeleRaza
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmbActiProp As NixorControls.ComboBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrCmd As String
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Dim lstrFil As String
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            mCargarCombos()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "especies_cargar", cmbEspe, "id", "descrip", "S")
      If Not Session("sEspeId") Is Nothing Then
        cmbEspe.Valor = Session("sEspeId").ToString
      End If
      mCargarRazas(cmbEspe.Valor.ToString)
      If Not Session("sRazaId") Is Nothing Then
        cmbRaza.Valor = Session("sRazaId").ToString
      End If
   End Sub

   Private Sub mCargarRazas(ByVal pstrEspe As String)
      If pstrEspe = "" Then
        cmbRaza.Items.Clear()
      Else
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & pstrEspe, cmbRaza, "id", "descrip_codi", "S")
      End If
   End Sub

#End Region

   Protected Overrides Sub Finalize()
      MyBase.Finalize()
   End Sub

   Private Sub btnModi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
       Session("sRazaId") = cmbRaza.Valor.ToString
       Session("sRazaDesc") = cmbRaza.SelectedItem.Text
       Session("sEspeId") = cmbEspe.Valor.ToString
       Session("sEspeDesc") = cmbEspe.SelectedItem.Text
   End Sub

End Class
End Namespace
