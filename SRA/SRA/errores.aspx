<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.errores" CodeFile="errores.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript"> 
		function mImprimir()
		{
			window.print();
		}		
		
		function mGuardarCambios_Click()
		         
		{
			//window.opener.parent.frames('main').location.href='ProductosInscripcion.aspx';
			if (window.opener.mModificar != null)
				window.opener.mModificar();
			window.close();
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG class="noprint" height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td class="noprint" background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG class="noprint" height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG class="noprint" height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD vAlign="middle" align="right">
									<DIV onclick="window.close();"><IMG class="noprint" style="CURSOR: hand" alt="Cerrar" src="imagenes/close3.bmp" border="0">
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 27px"><asp:label id="lblTituProducto" runat="server" cssclass="titulo" Font-Size="Small" Font-Italic="True"></asp:label></TD>
							</TR>
							<tr>
								<td></td>
							</tr>
							<TR>
								<TD><asp:label id="lblTituAbm" runat="server" cssclass="titulo" Font-Size="X-Small"></asp:label></TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
										ItemStyle-Height="5px" PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="5%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
														Height="5">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="Id" Visible="false" ReadOnly="True" HeaderText="Id"></asp:BoundColumn>
											<asp:BoundColumn DataField="Fecha" ReadOnly="True" HeaderText="Fecha"></asp:BoundColumn>
											<asp:BoundColumn DataField="Codigo" ReadOnly="True" HeaderText="Codigo"></asp:BoundColumn>
											<asp:BoundColumn DataField="Mensaje" ReadOnly="True" HeaderText="Mensaje"></asp:BoundColumn>
											<asp:BoundColumn Visible="false" DataField="err" ReadOnly="True" HeaderText="Error"></asp:BoundColumn>
											<asp:BoundColumn DataField="manual" ReadOnly="True" HeaderText="Manual"></asp:BoundColumn>
											<asp:BoundColumn DataField="audi_user" ReadOnly="True" HeaderText="Salvado por"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<ASP:TEXTBOX id="hdnUsua" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnErr" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnId" runat="server"></ASP:TEXTBOX></DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG class="noprint" height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<TR>
					<TD width="9" background="imagenes/reciz.jpg"></TD>
					<TD vAlign="middle" align="center">
						<div class="noprint">
							<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="96%" border="0">
								<TR>
									<TD height="40"><asp:button id="btnSalvar" runat="server" cssclass="boton" Text="Salvar/Activar" CausesValidation="False"
											Width="110px"></asp:button></TD>
									<TD height="30">&nbsp;
										<asp:label id="txtDesc" runat="server" cssclass="titulo"></asp:label></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 19px" align="right"><asp:label id="lblErr" runat="server" cssclass="titulo">Error:</asp:label>&nbsp;</TD>
									<TD style="HEIGHT: 19px"><cc1:combobox class="combo" id="cmbErr" runat="server" Width="324px"></cc1:combobox>&nbsp;
										<asp:button id="btnErr" runat="server" cssclass="boton" Text="Agregar Error" CausesValidation="False"
											Width="100px"></asp:button></TD>
								</TR>
								<TR>
									<TD height="30"></TD>
									<TD></TD>
								</TR>
							</TABLE>
						</div>
					</TD>
					<TD width="13" background="imagenes/recde.jpg"></TD>
				</TR>
				<TR>
					<TD width="9" background="imagenes/reciz.jpg"></TD>
					<TD vAlign="middle" align="right">
						<div class="noprint"><asp:button id="btnGuardarCambios" runat="server" cssclass="boton" Text="Guardar Cambios" CausesValidation="False"
								Width="125px"></asp:button>&nbsp;&nbsp;<asp:button id="btnImpri" runat="server" cssclass="boton" Text="Imprimir" CausesValidation="False"
								Width="80px"></asp:button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
					</TD>
					<TD width="13" background="imagenes/recde.jpg"></TD>
				</TR>
				<tr>
					<td width="9"><IMG class="noprint" height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td class="noprint" background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG class="noprint" height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
