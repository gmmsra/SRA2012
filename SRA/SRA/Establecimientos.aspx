<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Establecimientos" CodeFile="Establecimientos.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Establecimientos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		
		function mSelecCP()
		{
		    document.all["txtDireCP"].value = LeerCamposXML("localidades", document.all["cmbDireLoca"].value, "loca_cpos");
		}
		   
		function mCargarProvincias(pPais)
		{
		    var sFiltro = "0" + pPais.value;
		    LoadComboXML("provincias_cargar", sFiltro, "cmbDirePcia", "S");
			if (pPais.value == '<%=Session("sPaisDefaId")%>')
				document.all('hdnValCodPostalPais').value = 'S';
			else
				document.all('hdnValCodPostalPais').value = 'N';
			document.all("cmbDirePcia").onchange();
		}
		
		function mCargarLocalidades(pProv)
		{
			var sFiltro = "0" + pProv.value 
		    LoadComboXML("localidades_cargar", sFiltro, "cmbDireLoca", "S");
		    document.all("cmbDireLoca").onchange();
		}
			
		function btnSelecDire_click()
	   	{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Direciones de los integrantes&tabla=dire_clientesX&filtros='"+ document.all("hdnClieIds").value +"'", 7, "700","300");
		}
		
		function btnLoca_click()
   		{
	   	  if (document.all("cmbDirePais").value == "" || document.all("cmbDirePcia").value == "" )
		   {
		    alert('Debe seleccionar Pa�s y Provincia.')
	       }	  
		  else	
	      {
	   	   gAbrirVentanas("localidades.aspx?EsConsul=1&t=popup&pa=" + document.all("cmbDirePais").value + "&pr=" + document.all("cmbDirePcia").value,1);
		  }
		}
	   
	    function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function usrClieId_onchange()
		{
		    var sFiltro = document.all('usrClie:txtId').value;
 	        if (sFiltro == '')
 	           sFiltro = '0';
		    LoadComboXML("criadores_cliente_cargar", sFiltro, "cmbCria", "S");
		}
			
		function mOtrosDatosAbrir()
	   	{
          gAbrirVentanas("Alertas_pop.aspx?titulo=Otros Datos&amp;origen=O&amp;clieId=" + document.all("hdnId").value+"&amp;"+"FechaValor=&amp;sociId=", 7, "450","300");
	 	}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Establecimientos</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" noWrap colSpan="3">
										<asp:panel id="panFiltros" runat="server" cssclass="titulo" width="100%" BorderStyle="none"
											BorderWidth="1px">
											<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																		ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																		CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="2" background="imagenes/formiz.jpg" colSpan="3"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD>
																	<TABLE class="FdoFld" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 17px" align="right">
																				<asp:label id="lblClieNumeFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 80%">
																				<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" FilDocuNume="True" MuestraDesc="False"
																					FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="Clientes"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblCriaNumeFil" runat="server" cssclass="titulo">Criador:</asp:label>&nbsp;</TD>
																			<TD>
																				<UC1:CLIE id="usrCriaFil" runat="server" AceptaNull="true" FilDocuNume="True" MuestraDesc="False"
																					FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="Criadores" Ancho="800" AutoPostBack="True"
																					CampoVal="Criador"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblDescFil" runat="server" cssclass="titulo">&nbsp;&nbsp;Establecimiento:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtDescFil" runat="server" cssclass="cuadrotexto" Width="300px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblApelFil" runat="server" cssclass="titulo">Apellido/Raz�n Social:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtApelFil" runat="server" cssclass="cuadrotexto" Width="300px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblNombFil" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" Width="300px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right"></TD>
																			<TD>
																				<asp:checkbox id="chkBusc" Text="Buscar en..." Runat="server" CssClass="titulo"></asp:checkbox>&nbsp;&nbsp;&nbsp;
																				<asp:checkbox id="chkBaja" Text="Incluir Dados de Baja" Runat="server" CssClass="titulo"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblCuitFil" runat="server" cssclass="titulo">CUIT:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:CUITBOX id="txtCuitFil" runat="server" cssclass="cuadrotexto" Width="200px"></CC1:CUITBOX></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblDocuFil" runat="server" cssclass="titulo">Nro. Documento:</asp:label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox class="combo" id="cmbDocuTipoFil" runat="server" AceptaNull="False" Width="100px"></cc1:combobox>
																				<cc1:numberbox id="txtDocuNumeFil" runat="server" cssclass="cuadrotexto" Width="143px" MaxValor="9999999999999"
																					esdecimal="False"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblCunicaFil" runat="server" cssclass="titulo">Clave �nica:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB onkeypress="gMascaraCunica(this)" id="txtCunicaFil" runat="server" cssclass="cuadrotexto"
																					Width="161px" maxlength="13"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD vAlign="top" align="left" colSpan="3">
										<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
											OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="esbl_id" ReadOnly="True" HeaderText="Id.EstablCliente"></asp:BoundColumn>
												<asp:BoundColumn DataField="esbl_desc" HeaderText="Establecimiento"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_clie_id" HeaderText="Nro.Cliente"></asp:BoundColumn>
												<asp:BoundColumn DataField="clie_apel" ReadOnly="True" HeaderText="Apellido/Raz&#243;n Social"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="clie_nomb" HeaderText="Nombre"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="_cuit" ReadOnly="True" HeaderText="CUIT"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="_clie_docu" HeaderText="Documento"></asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="_clie_cunica" HeaderText="Clave Unica"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td colSpan="3" height="10"></td>
								</tr>
								<TR>
									<TD vAlign="middle" width="100">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
											BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
											ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ImageDisable="btnNuev0.gif" ToolTip="Agregar un Nuevo Establecimiento"></CC1:BOTONIMAGEN></TD>
									<TD align="right" colspan="2">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Establecimiento</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkTele" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" Height="21px" CausesValidation="False"> Tel�fonos</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDire" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" Height="21px" CausesValidation="False"> Direcciones</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkMail" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" Height="21px" CausesValidation="False"> Mails</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkRaza" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" Height="21px" CausesValidation="False"> Razas</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="99%" BorderStyle="Solid" BorderWidth="1px"
												Height="116px" Visible="False">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="2">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 15%" vAlign="middle" align="right">
																			<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label></TD>
																		<TD style="WIDTH: 85%">
																			<UC1:CLIE id="usrClie" runat="server" AceptaNull="true" FilDocuNume="True" MuestraDesc="False"
																				FilTipo="S" FilSociNume="True" Saltos="1,2" Tabla="Clientes" Ancho="800"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="right">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripci�n:</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="450px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="right">
																			<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label></TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="450px" Height="50px" TextMode="MultiLine"
																				EnterPorTab="False" MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="20">
																			<asp:Label id="lblCriaTitu" runat="server" cssclass="titulo">Criadores Asociados</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:datagrid id="grdCria" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
																				OnUpdateCommand="mEditarDatosCriador" OnPageIndexChanged="grdCria_PageChanged" CellPadding="1"
																				GridLines="None" CellSpacing="1" HorizontalAlign="Center" Visible="True" ShowFooter="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="15px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="escr_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_criador" ReadOnly="True" HeaderText="Criador"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<TABLE id="Table7" cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD align="right" height="21">
																						<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																					<TD>
																						<cc1:combobox class="combo" id="cmbCria" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="razas_cargar"
																							MostrarBotones="False" filtra="False"></cc1:combobox></TD>
																				</TR>
																				<TR>
																					<TD align="right" height="21">
																						<asp:Label id="lblTatu" runat="server" cssclass="titulo">Tatuaje:</asp:Label>&nbsp;</TD>
																					<TD>
																						<cc1:combobox class="combo" id="cmbTatu" runat="server" AceptaNull="False" Width="234px"></cc1:combobox></TD>
																				</TR>
																				<TR>
																					<TD align="right" height="21">
																						<asp:Label id="lblRespa" runat="server" cssclass="titulo">Respa:</asp:Label>&nbsp;</TD>
																					<TD>
																						<CC1:TEXTBOXTAB id="txtRespa" runat="server" cssclass="cuadrotexto" Width="140px"></CC1:TEXTBOXTAB></TD>
																				</TR>
																				<TR>
																					<TD height="21"></TD>
																					<TD>
																						<asp:CheckBox id="chkDuplCert" Text="Solicitud de Env�o de Doble Certificado" Runat="server" CssClass="titulo"></asp:CheckBox></TD>
																				</TR>
																				<TR>
																					<TD align="right" height="21">
																						<asp:Label id="lblCriaObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																					<TD>
																						<CC1:TEXTBOXTAB id="txtCriaObse" runat="server" cssclass="cuadrotexto" Width="450px" Height="50px"
																							TextMode="MultiLine" EnterPorTab="False" MaxLength="20"></CC1:TEXTBOXTAB></TD>
																				</TR>
																				<TR>
																					<TD align="center" colSpan="2" height="34">
																						<asp:Button id="btnAltaCria" runat="server" cssclass="boton" Width="110px" Text="Agregar Criador"></asp:Button>&nbsp;
																						<asp:Button id="btnBajaCria" runat="server" cssclass="boton" Width="110px" Text="Eliminar Criador"></asp:Button>&nbsp;
																						<asp:Button id="btnModiCria" runat="server" cssclass="boton" Width="110px" Text="Modificar Criador"></asp:Button>&nbsp;
																						<asp:Button id="btnLimpCria" runat="server" cssclass="boton" Width="110px" Text="Limpiar Criador"></asp:Button></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panDire" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="Table4" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="left" colSpan="2">
																			<asp:datagrid id="grdDire" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
																				OnPageIndexChanged="grdDire_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																				HorizontalAlign="Center" AllowPaging="True" Visible="False" OnEditCommand="mEditarDatosDire">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="dicl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_dire_desc" HeaderText="Direcci&#243;n">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="dicl_refe" ReadOnly="True" HeaderText="Referencia">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_prop" HeaderText="acti_prop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_noprop" HeaderText="acti_noprop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_prop" HeaderText="Prop."></asp:BoundColumn>
																					<asp:BoundColumn DataField="_defa" HeaderText="Default" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtDire" runat="server" cssclass="cuadrotexto" Width="440px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDirePais" runat="server" cssclass="titulo">Pais:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbDirePais" runat="server" Width="260px" onchange="mCargarProvincias(this)"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="llbDirePcia" runat="server" cssclass="titulo">Provincia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbDirePcia" runat="server" Width="260px" NomOper="provincias_cargar"
																				onchange="mCargarLocalidades(this)"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDireLoca" runat="server" cssclass="titulo">Localidad:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbDireLoca" runat="server" Width="260px" onchange="mSelecCP();"></cc1:combobox>&nbsp;<BUTTON class="boton" id="btnLoca" style="WIDTH: 87px" onclick="btnLoca_click();" type="button"
																				runat="server" value="Detalles">Localidades</BUTTON></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDireCP" runat="server" cssclass="titulo">C�digo Postal:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:CODPOSTALBOX id="txtDireCP" runat="server" cssclass="cuadrotexto" Width="77px"></CC1:CODPOSTALBOX></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDireRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtDireRefe" runat="server" cssclass="cuadrotexto" Width="440px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD></TD>
																		<TD>
																			<asp:checkbox id="chkDireDefa" Text="Default" Runat="server" CssClass="titulo"></asp:checkbox>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<% if mbooActi then %>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right"></TD>
																		<TD>
																			<asp:Label id="lblTituActiDire" runat="server" cssclass="titulo">Actividades</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<asp:datagrid id="grdActiDire" runat="server" BorderWidth="1px" BorderStyle="None" width="70%"
																				AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																				Visible="False" ShowFooter="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn HeaderText="Asociar">
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:checkbox id="chkActi" Visible="True" CssClass="titulo" Runat="server" Text=""></asp:checkbox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Actividad"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Prop.">
																						<HeaderStyle Width="70px"></HeaderStyle>
																						<ItemTemplate>
																							<cc1:combobox class="combo" enabled="false" id="cmbActiProp" runat="server" Width="50px" Visible="True">
																								<asp:ListItem Value="N">No</asp:ListItem>
																								<asp:ListItem Value="S">Si</asp:ListItem>
																							</cc1:combobox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<% end if %>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaDire" runat="server" cssclass="boton" Width="100px" Text="Agregar Direc."></asp:Button>&nbsp;
																			<asp:Button id="btnBajaDire" runat="server" cssclass="boton" Width="100px" Text="Eliminar Direc."></asp:Button>&nbsp;
																			<asp:Button id="btnModiDire" runat="server" cssclass="boton" Width="100px" Text="Modificar Direc."></asp:Button>&nbsp;
																			<asp:Button id="btnLimpDire" runat="server" cssclass="boton" Width="100px" Text="Limpiar Direc."
																				Visible="True"></asp:Button>&nbsp;&nbsp;<BUTTON class="boton" id="btnSelecDire" style="WIDTH: 100px" onclick="btnSelecDire_click();"
																				type="button" runat="server" value="Detalles">Selec. Direc.</BUTTON></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panMail" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdMail" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
																				OnPageIndexChanged="grdMail_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																				HorizontalAlign="Center" AllowPaging="True" Visible="False" OnEditCommand="mEditarDatosMail">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="macl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="macl_mail" HeaderText="Mail">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="macl_refe" ReadOnly="True" HeaderText="Referencia">
																						<HeaderStyle Width="35%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_prop" HeaderText="acti_prop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" HeaderImageUrl="acti_noprop" HeaderText="acti_noprop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_prop" HeaderText="Prop.">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblMail" runat="server" cssclass="titulo">Mail:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtMail" runat="server" cssclass="cuadrotexto" AceptaNull="False" Width="292px"
																				EsMail="True" Panel="Mail"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblMailRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtMailRefe" runat="server" cssclass="cuadrotexto" Width="440px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<% if mbooActi then %>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right"></TD>
																		<TD>
																			<asp:Label id="lblTituActiMail" runat="server" cssclass="titulo">Actividades</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<asp:datagrid id="grdActiMail" runat="server" BorderWidth="1px" BorderStyle="None" width="70%"
																				AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																				Visible="False" ShowFooter="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn HeaderText="Asociar">
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:checkbox id="Checkbox1" Visible="True" CssClass="titulo" Runat="server" Text=""></asp:checkbox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Actividad"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Prop.">
																						<HeaderStyle Width="70px"></HeaderStyle>
																						<ItemTemplate>
																							<cc1:combobox class="combo" enabled="false" id="Combobox1" runat="server" Width="50px" Visible="True">
																								<asp:ListItem Value="N">No</asp:ListItem>
																								<asp:ListItem Value="S">Si</asp:ListItem>
																							</cc1:combobox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<% end if %>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaMail" runat="server" cssclass="boton" Width="100px" Text="Agregar Mail"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaMail" runat="server" cssclass="boton" Width="100px" Text="Eliminar Mail"></asp:Button>&nbsp;
																			<asp:Button id="btnModiMail" runat="server" cssclass="boton" Width="100px" Text="Modificar Mail"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpMail" runat="server" cssclass="boton" Width="100px" Text="Limpiar Mail"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panTele" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdTele" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
																				OnPageIndexChanged="grdTele_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																				HorizontalAlign="Center" AllowPaging="True" Visible="False" OnEditCommand="mEditarDatosTele">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton8" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="tecl_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_tele_desc" ReadOnly="True" HeaderText="Tel&#233;fono">
																						<HeaderStyle Width="50%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="tecl_refe" HeaderText="Referencia">
																						<HeaderStyle Width="35%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_prop" HeaderText="acti_prop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="acti_noprop" HeaderText="acti_noprop"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_prop" HeaderText="Prop.">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTeleNume" runat="server" cssclass="titulo">Tel�fono:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtTeleArea" runat="server" cssclass="cuadrotexto" Width="80px"></CC1:TEXTBOXTAB>&nbsp;
																			<CC1:TEXTBOXTAB id="txtTeleNume" runat="server" cssclass="cuadrotexto" Width="332px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTeleRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtTeleRefe" runat="server" cssclass="cuadrotexto" Width="450px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTeleTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 10px">
																			<cc1:combobox class="combo" id="cmbTeleTipo" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<% if mbooActi then %>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right"></TD>
																		<TD style="HEIGHT: 10px">
																			<asp:Label id="lblTituActiTele" runat="server" cssclass="titulo">Actividades</asp:Label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<asp:datagrid id="grdActiTele" runat="server" BorderWidth="1px" BorderStyle="None" width="70%"
																				AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																				Visible="False" ShowFooter="True">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn HeaderText="Asociar">
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:checkbox id="Checkbox2" Visible="True" CssClass="titulo" Runat="server" Text=""></asp:checkbox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Actividad"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderText="Prop.">
																						<HeaderStyle Width="70px"></HeaderStyle>
																						<ItemTemplate>
																							<cc1:combobox class="combo" enabled="false" id="Combobox2" runat="server" Width="50px" Visible="True">
																								<asp:ListItem Value="N">No</asp:ListItem>
																								<asp:ListItem Value="S">Si</asp:ListItem>
																							</cc1:combobox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<% end if %>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaTele" runat="server" cssclass="boton" Width="100px" Text="Agregar Tel."></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaTele" runat="server" cssclass="boton" Width="100px" Text="Eliminar Tel."
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnModiTele" runat="server" cssclass="boton" Width="100px" Text="Modificar Tel."
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpTele" runat="server" cssclass="boton" Width="100px" Text="Limpiar Tel."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panRaza" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="Table6" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdRaza" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AutoGenerateColumns="False"
																				OnPageIndexChanged="grdRaza_PageChanged" CellPadding="1" GridLines="None" CellSpacing="1"
																				HorizontalAlign="Center" AllowPaging="True" Visible="True" OnEditCommand="mEditarDatosRaza">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="esra_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_raza_desc" ReadOnly="True" HeaderText="Raza">
																						<HeaderStyle Width="70%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="true" DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="25%">
																			<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 25px">
																			<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="razas_cargar"
																				MostrarBotones="False" filtra="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaRaza" runat="server" cssclass="boton" Width="100px" Text="Agregar Raza"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaRaza" runat="server" cssclass="boton" Width="100px" Text="Eliminar Raza"
																				Visible="true"></asp:Button>&nbsp;
																			<asp:Button id="btnModiRaza" runat="server" cssclass="boton" Width="100px" Text="Modificar Raza"
																				Visible="False"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpRaza" runat="server" cssclass="boton" Width="100px" Text="Limpiar Raza"
																				Visible="False"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnLocaPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnTeleId" runat="server"></asp:textbox><asp:textbox id="hdnDireId" runat="server"></asp:textbox><asp:textbox id="hdnMailId" runat="server"></asp:textbox><asp:textbox id="hdnCriaId" runat="server"></asp:textbox><asp:textbox id="hdnAgru" runat="server"></asp:textbox><asp:textbox id="hdnClagId" runat="server"></asp:textbox><asp:textbox id="hdnOrdenAnte" runat="server"></asp:textbox><asp:textbox id="hdnOrdenProx" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnClieIds" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox><cc1:combobox class="combo" id="cmbLocaAux" runat="server" AceptaNull="false"></cc1:combobox><asp:textbox id="hdnRazaId" runat="server" AutoPostBack="True"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Establecimientos');
		document.frmABM.action='establecimientos.aspx'
		</SCRIPT>
	</BODY>
</HTML>
