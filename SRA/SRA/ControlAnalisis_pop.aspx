<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ControlAnalisis_pop" CodeFile="ControlAnalisis_pop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%" id="Table1" border="0">
								<TBODY>
									<TR>
										<TD vAlign="middle" align="right"><asp:imagebutton id="Imagebutton1" runat="server" ImageUrl="imagenes/close3.bmp" ToolTip="Cerrar"
												CausesValidation="False"></asp:imagebutton>&nbsp;
										</TD>
									</TR>
									<TR>
										<TD vAlign="middle" colSpan="2" align="center"><asp:panel id="panEspeciales" runat="server" height="100%" BorderStyle="Solid" BorderWidth="1px"
												Width="100%" cssclass="titulo" Visible="true">
												<TABLE style="WIDTH: 100%" id="tblEspeciales" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 25%" colSpan="2" align="left">
															<asp:label id="lblNomb" runat="server" cssclass="titulo"></asp:label>&nbsp;
														</TD>
													</TR>
													<TR>
													<TR>
														<TD style="WIDTH: 25%" colSpan="2" align="left">
															<asp:label id="lblLeyendaFecha" runat="server" cssclass="titulo"></asp:label>&nbsp;
														</TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR height="100%">
										<TD vAlign="top"><asp:datagrid id="grdConsulta" runat="server" BorderStyle="None" BorderWidth="1px" width="100%"
												AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page"
												PageSize="15" ItemStyle-Height="5px" AutoGenerateColumns="False">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:BoundColumn DataField="DescEspecie" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" HeaderText="Especie">
														<HeaderStyle Width="20%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Cantidad" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderText="Cant.An�lisis">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="Importe" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderText="Por Importe"
														DataFormatString="{0:C}">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="ImportConcep" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Center" HeaderText="Descuentos Aplicados"
														DataFormatString="{0:C}">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
								</TBODY>
							</TABLE>
							<DIV style="DISPLAY: none">&nbsp;
								<asp:textbox id="hdnIdCliente" runat="server" ></asp:textbox>
							</DIV>
							<!--- FIN CONTENIDO ---></td>
						<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
