'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar de la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_DebiAutoEnvio_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
' a la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'DebiAutoEnvio.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
Namespace SRA

'Partial Class DebiAutoEnvio
Partial Class Migrated_DebiAutoEnvio

Inherits DebiAutoEnvio

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblPeti As System.Web.UI.WebControls.Label
    Protected WithEvents lblDeta As System.Web.UI.WebControls.Label
    Protected WithEvents lbObse As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_DebitosCabe
    Private mstrDebitosDeta As String = SRA_Neg.Constantes.gTab_DebitosDeta
    Private mstrInstituto As String
    Private mstrConn As String
    Private mstrEtapa As String
'    Public mstrTitulo As String
    Private mstrParaPageSize As Integer

    Private mdsDatos As DataSet

    Private Enum ColumnasDeta As Integer
        Id = 0
    End Enum
#End Region

#Region "Inicializaci�n de Variables"
    Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        grdDeta.PageSize = Convert.ToInt32(mstrParaPageSize)
    End Sub
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            ' 2021-01-18 1255 Dario se toma el valor con el que se llama a esta pagina para pasarlo al 
            ' que ve si esta presentado esto por instituto
            Try
                mstrInstituto = Request.QueryString("fkvalor")
            Catch ex As Exception
                mstrInstituto = ""
            End Try

            If (Not Page.IsPostBack) Then
                Session(mstrTabla) = Nothing
                mdsDatos = Nothing

                mSetearMaxLength()
                mSetearEventos()

                mCargarCombos()

                    txtGeneFecha.Fecha = Today
                    txtGeneFecha.Enabled = False
                txtEnviFecha.Enabled = False
                txtAnioFil.Valor = Today.Year
                With mObtenerPerioActual(cmbPetiFil.Valor)
                    txtAnioFil.Text = .Item("cues_anio")
                    txtPeriFil.Text = .Item("cues_perio")
                End With

                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                mdsDatos = Session(mstrTabla)
            End If

        Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
    End Sub

    Private Sub mSetearEventos()
        btnAlta.Attributes.Add("onclick", "if(!confirm('Confirma la generaci�n del d�bito?')) return false;")
        btnBajaListaTodos.Attributes.Add("onclick", "if(!confirm('Confirma la baja de los registros existentes y la re-generaci�n del d�bito?')) return false;")
        btnEnviar.Attributes.Add("onclick", "if(!confirm('Atenci�n: Si realiz� alg�n cambio en la lista de movimientos a presentar se deber� primeramente (Modificar),\nde lo contrario estos ajustes no ser�n incluidos en el archivo de env�o.\nConfirma la generaci�n del archivo de env�o?')) return false;")
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPetiFil, "", SRA_Neg.Constantes.PeriodoTipos.Bimestre)
        clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPeti, "", SRA_Neg.Constantes.PeriodoTipos.Bimestre)

        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "")
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, Optional ByVal pbooEnviado As Boolean = False)
        Select Case pstrTabla
            Case mstrDebitosDeta
                btnBajaDeta.Enabled = Not pbooAlta And Not pbooEnviado And Not (usrSoci.Valor Is DBNull.Value) And Not btnAlta.Enabled
                btnModiDeta.Enabled = Not pbooAlta And Not pbooEnviado And Not (usrSoci.Valor Is DBNull.Value) And Not btnAlta.Enabled
                btnAltaDeta.Enabled = pbooAlta And Not pbooEnviado And Not (usrSoci.Valor Is DBNull.Value)
                btnDeuda.Disabled = usrSoci.Valor Is DBNull.Value
                usrSoci.Activo = pbooAlta And grdDeta.Columns(0).Visible

            Case Else
                btnBajaListaTodos.Visible = Not pbooAlta
                btnModi.Enabled = Not pbooAlta And Not pbooEnviado
                btnEnviar.Enabled = Not pbooAlta
                btnAlta.Enabled = pbooAlta And Not pbooEnviado
                btnList.Visible = Not pbooAlta

                lblTarj.Visible = Not pbooAlta
                cmbTarj.Visible = Not pbooAlta


                grdDeta.Columns(0).Visible = Not pbooEnviado
                usrSoci.Activo = grdDeta.Columns(0).Visible
        End Select
    End Sub

    Public Overrides Sub mCargarDatos(ByVal pstrId As String)
        Dim lintComiCant As Integer
        Dim lbooEnviada As Boolean

        mCrearDataSet(pstrId)

        With mdsDatos.Tables(mstrTabla).Rows(0)
            hdnId.Text = .Item("deca_id").ToString()
                txtGeneFecha.Fecha = IIf(.Item("deca_gene_fecha").ToString.Trim.Length > 0, .Item("deca_gene_fecha"), String.Empty)

                txtEnviFecha.Fecha = IIf(.Item("deca_envi_fecha").ToString.Trim.Length > 0, .Item("deca_envi_fecha"), String.Empty)
                cmbTarj.Valor = IIf(.Item("deca_tarj_id").ToString.Trim.Length > 0, .Item("deca_tarj_id"), DBNull.Value)
                txtAnio.Valor = IIf(.Item("deca_anio").ToString.Trim.Length > 0, .Item("deca_anio"), String.Empty)
                cmbPeti.Valor = IIf(.Item("deca_peti_id").ToString.Trim.Length > 0, .Item("deca_peti_id"), String.Empty)
                txtPerio.Valor = IIf(.Item("deca_perio").ToString.Trim.Length > 0, .Item("deca_perio"), String.Empty)
                lbooEnviada = Not .IsNull("deca_envi_fecha")
        End With

        lblTitu.Text = "Registro Seleccionado: " + CDate(txtGeneFecha.Fecha).ToString("dd/MM/yyyy")
        lblBaja.Text = ""
        mSetearEditor("", False, lbooEnviada)
        mMostrarPanel(True)
    End Sub

    Private Sub mAgregar()
        mLimpiar()

        btnBajaListaTodos.Visible = False
        btnModi.Enabled = False
        btnEnviar.Enabled = False
        lblTarj.Visible = False
        cmbTarj.Visible = False

        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        lblTitu.Text = ""
            txtGeneFecha.Fecha = Today

            txtEnviFecha.Text = ""
        cmbTarj.Limpiar()

        cmbPeti.Limpiar()

        With mObtenerPerioActual(cmbPeti.Valor)
            txtAnio.Text = .Item("cues_anio")
            txtPerio.Text = .Item("cues_perio")
        End With

        lblTarj.Visible = False
        cmbTarj.Visible = False

        lblBaja.Text = mObtenerExiste()
        btnAlta.Text = IIf(lblBaja.Text = "", "Generar", "Regenerar")

        grdDeta.CurrentPageIndex = 0

        mCrearDataSet("")

        mLimpiarDeta()

        mSetearEditor("", True, False)

        mShowTabs(1)
    End Sub

    Private Function mObtenerPerioActual(ByVal pstrPetiId As String) As DataRow
        Dim lstrCmd As String
        Dim lds As DataSet

        lstrCmd += "exec cuotas_estru_x_fecha_consul"
        lstrCmd += " @fecha=" + clsFormatear.gFormatFecha2DB(Today)
        lstrCmd += ",@peti_id=" + pstrPetiId

        lds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)
        Return (lds.Tables(0).Rows(0))
    End Function

'    Public Shared Function gObtenerExiste(ByVal pstrConn As String, ByVal pstrArgs As String) As String
'        Dim lstrCmd As String
'        Dim lstrRet As String
'        Dim lds As DataSet
'        Dim lvstr() As String
'        lvstr = pstrArgs.Split(";")
'
'        lstrCmd = "exec debitos_cabe_consul"
'        lstrCmd += " @deca_anio=" + lvstr(0)
'        lstrCmd += ",@deca_perio=" + lvstr(1)
'        lstrCmd += ",@deca_peti_id=" + lvstr(2)
'        lstrCmd += ",@regeneracion=1"
'        If Len(lvstr(3)) > 0 Then 'mstrInstituto
'            lstrCmd += ",@inse_id=" + lvstr(3)
'        End If
'
'        lds = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd)
'
'        If lds.Tables(0).Rows.Count > 0 Then
'            lstrRet = "Ya se gener� el d�bito autom�tico para este per�odo. Fecha: " + CDate(lds.Tables(0).Rows(0).Item("deca_audi_fecha")).ToString("dd/MM/yyyy HH:mm")
'        Else
'            lstrRet = ""
'        End If
'        Return (lstrRet)
'    End Function

    Private Function mObtenerExiste() As String
        Return (gObtenerExiste(mstrConn, txtAnio.Valor.ToString + ";" + txtPerio.Valor.ToString + ";" + cmbPeti.Valor.ToString + ";" + mstrInstituto))
    End Function

    Private Sub mCerrar()
        mLimpiar()
        mConsultar()
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        Dim lbooVisiOri As Boolean = panDato.Visible

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        btnAgre.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        If pbooVisi Then
            mShowTabs(1)
            grdDato.DataSource = Nothing
            grdDato.DataBind()
        Else
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            mLimpiarDeta()
        End If

        If lbooVisiOri And Not pbooVisi Then
            txtAnioFil.Valor = Today.Year
        End If

        tabLinks.Visible = pbooVisi And Not btnAlta.Enabled
    End Sub

    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panBotones.Visible = True
        panDeta.Visible = False
        panCabecera.Visible = False

        lnkCabecera.Font.Bold = False
        lnkDeta.Font.Bold = False

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
            Case 2
                panDeta.Visible = True
                lnkDeta.Font.Bold = True
                grdDeta.Visible = True

                hdnAnio.Text = txtAnio.Text
                hdnPerio.Text = txtPerio.Text
                hdnFecha.Text = txtGeneFecha.Text
        End Select
    End Sub

    Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub

    Private Sub lnkDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
        mShowTabs(2)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatosMasivo()

            Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
            lobjGenericoRel.ActualizaMul()

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try
            mGuardarDatos(mdsDatos.Tables(mstrTabla).Rows(0), cmbTarj.Valor)

            Dim ldsTmp As DataSet = mdsDatos.Copy

            Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTmp)
            lobjGenericoRel.Modi()

            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosMasivo()
        Dim ldrCab As DataRow

        mValidarDatos()
        mCrearDataSet("")

        mdsDatos.Tables(mstrTabla).Rows(0).Delete()

        For Each oItem As Web.UI.WebControls.ListItem In cmbTarj.Items
            ldrCab = mdsDatos.Tables(mstrTabla).NewRow
            ldrCab.Item("deca_id") = clsSQLServer.gObtenerId(ldrCab.Table, "deca_id")
            ldrCab.Table.Rows.Add(ldrCab)
            mGuardarDatos(ldrCab, oItem.Value)
        Next
    End Sub

    Private Sub mGuardarDatos(ByVal ldrCab As DataRow, ByVal pintTarjId As Integer)
        mValidarDatos()

        With ldrCab
            .Item("deca_acti_id") = SRA_Neg.Constantes.Actividades.Socios
                .Item("deca_gene_fecha") = IIf(txtGeneFecha.Fecha.Trim.Length > 0, txtGeneFecha.Fecha, DBNull.Value)
                .Item("deca_envi_fecha") = IIf(txtEnviFecha.Fecha.Trim.Length > 0, txtEnviFecha.Fecha, DBNull.Value)
                .Item("deca_tarj_id") = IIf(pintTarjId.ToString.Trim.Length > 0, pintTarjId, DBNull.Value)
                .Item("deca_anio") = IIf(txtAnio.Valor.Trim.Length > 0, txtAnio.Valor, DBNull.Value)
                .Item("deca_peti_id") = IIf(cmbPeti.Valor.Trim.Length > 0, cmbPeti.Valor, DBNull.Value)
                .Item("deca_perio") = IIf(txtPerio.Valor.Trim.Length > 0, txtPerio.Valor, DBNull.Value)
        End With
    End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Public Overrides Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrDebitosDeta

        mUnificarDescs()
        mConsultarDeta()

        Session(mstrTabla) = mdsDatos
    End Sub

    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
        Try
            Dim lstrCmd As New StringBuilder
            Dim lstrArch As String
            Dim lstrPathTemp As String = System.Configuration.ConfigurationSettings.AppSettings("conPathTemp")
            Dim ds As New DataSet
            Dim lstrTarjNombArch, lstrTarjNombArch2, lstrArch2 As String
            Dim vstrTarjNombArch() As String

            lstrCmd.Append("exec debitos_cabe_exporta_consul")
            lstrCmd.Append(" @deca_id=" + hdnId.Text)

            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

            lstrTarjNombArch = clsSQLServer.gObtenerEstruc(mstrConn, "tarjetas", cmbTarj.Valor).Tables(0).Rows(0).Item("tarj_da_arch_soci").ToString

            If lstrTarjNombArch = "" Then lstrTarjNombArch = "ExporTarj.txt"

            If lstrTarjNombArch.IndexOf(",") > -1 Then
                vstrTarjNombArch = lstrTarjNombArch.Split(",")
                lstrTarjNombArch = vstrTarjNombArch(0)
                lstrTarjNombArch2 = vstrTarjNombArch(1)
                lstrArch2 = lstrPathTemp + lstrTarjNombArch2
                lstrArch2 = HttpContext.Current.Server.MapPath(lstrArch2)
            End If

            lstrArch = lstrPathTemp + lstrTarjNombArch
            lstrArch = HttpContext.Current.Server.MapPath(lstrArch)

            clsExporta.clsGrabarArchivo.FormatoTexto(ds, lstrArch, "", False)

            hdnExpo.Text = lstrPathTemp + lstrTarjNombArch

            If lstrTarjNombArch2 <> "" Then
                ds.Tables.Remove(ds.Tables(0))
                clsExporta.clsGrabarArchivo.FormatoTexto(ds, lstrArch2, "", False)
                hdnExpo2.Text = lstrPathTemp + lstrTarjNombArch2
            End If

                txtEnviFecha.Fecha = Today
                mModi()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub txtAnioFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnioFil.TextChanged
        Try
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec " + mstrTabla + "_consul")
            lstrCmd.Append(" @deca_anio=" + txtAnioFil.Valor.ToString)
            lstrCmd.Append(",@deca_peti_id=" + cmbPetiFil.Valor.ToString)
            lstrCmd.Append(",@deca_perio=" + txtPeriFil.Valor.ToString)
            lstrCmd.Append(",@deca_tarj_id=" + cmbTarjFil.Valor.ToString)
            lstrCmd.Append(",@deca_acti_id=" + CType(SRA_Neg.Constantes.Actividades.Socios, String))
            lstrCmd.Append(",@etapa='E'")

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
    Public Overrides Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            mCargarDatos(e.Item.Cells(1).Text)
            hdnDatosPop.Text = ""
            hdnDescuen.Text = ""

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Detalle"
    Public Overrides Sub grdDeta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDeta.EditItemIndex = -1
            If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
                grdDeta.CurrentPageIndex = 0
            Else
                grdDeta.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarDeta()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBajaDetaTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaListaTodos.Click
        Try
            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec debitos_deta_proceso_alta ")
            lstrCmd.Append(hdnId.Text)
            clsSQLServer.gExecute(mstrConn, lstrCmd.ToString)

            'remuevo todos los detalles de la tabla, para que no los agregue en el modi
            For Each oDataRow As DataRow In mdsDatos.Tables(mstrDebitosDeta).Select
                mdsDatos.Tables(mstrDebitosDeta).Rows.Remove(oDataRow)
            Next
            grdDeta.CurrentPageIndex = 0

                'cambio la fecha de generaci�n y le saco la marca de enviado
                txtGeneFecha.Fecha = Today
                txtEnviFecha.Fecha = String.Empty  ' DBNull.Value

            mModi()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mConsultarDeta(Optional ByVal pstrSociId As String = "")
        With mdsDatos.Tables(mstrDebitosDeta)
            .DefaultView.RowFilter = "dede_total_impo<>0"
            .DefaultView.Sort = "_clie_apel"
            grdDeta.DataSource = .DefaultView
            grdDeta.DataBind()
        End With
    End Sub

    Public Overrides Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDeta As DataRow

            hdnDatosPop.Text = ""
            hdnDescuen.Text = ""

            hdnDedeId.Text = E.Item.Cells(1).Text
            ldrDeta = mdsDatos.Tables(mstrDebitosDeta).Select("dede_id=" & hdnDedeId.Text)(0)

            With ldrDeta
                usrSoci.Valor = .Item("dede_soci_id")

                txtImpo.Valor = .Item("dede_envio_impo")
                txtAnteImpo.Text = txtImpo.Valor
                txtImpoOrig.Valor = .Item("dede_total_impo")

                If Not .IsNull("_tacl_nume") Then
                    txtTaclNume.Text = .Item("_tacl_nume")
                End If

                    txtObse.Valor = .Item("dede_modi_obse").ToString

                lblAuto.Text = .Item("_auto").ToString
                lblCotiDesc.Text = .Item("_coti_desc").ToString
                hdnSociId.Text = .Item("dede_soci_id").ToString
                hdnTaclId.Text = .Item("dede_tacl_id").ToString
                hdnCompId.Text = .Item("dede_comp_id").ToString
            End With

            mSetearEditor(mstrDebitosDeta, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarDeta(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosDeta(pbooAlta)

            mLimpiarDeta()
            mConsultarDeta()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosDeta(ByVal pbooAlta As Boolean, Optional ByVal pbooAuto As Boolean = False)
        Dim ldrDatos As DataRow

        If usrSoci.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el Socio.")
        End If

        If hdnDedeId.Text <> "" Then
            ldrDatos = mdsDatos.Tables(mstrDebitosDeta).Select("dede_id=" & hdnDedeId.Text)(0)
        Else
            If pbooAlta Then
                ldrDatos = mdsDatos.Tables(mstrDebitosDeta).NewRow
                ldrDatos.Item("dede_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrDebitosDeta), "dede_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrDebitosDeta).Select("dede_soci_id=" & usrSoci.Valor.ToString)(0)
            End If
        End If

        If Not pbooAuto And txtImpo.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar un importe.")
        End If
        If txtImpoOrig.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar un importe.")
        End If
        If Convert.ToDouble(txtImpo.Valor) > Convert.ToDouble(txtImpoOrig.Valor) Then
            Throw New AccesoBD.clsErrNeg("El importe no puede ser mayor al importe total.")
        End If
        If txtAnteImpo.Text <> txtImpo.Valor And txtObse.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar una observaci�n.")
        End If

        If hdnDatosPop.Text <> "" Then
            mCargarCuotaSocial(False)
            hdnDatosPop.Text = ""
            hdnDescuen.Text = ""
        Else
            Return
        End If

        If hdnDedeId.Text = "" And pbooAlta Then
            ldrDatos = mdsDatos.Tables(mstrDebitosDeta).Select("dede_soci_id=" & usrSoci.Valor.ToString, "dede_id desc")(0)
        End If

        With ldrDatos

            .Item("dede_soci_id") = usrSoci.Valor
            .Item("dede_clie_id") = usrSoci.ClieId
            .Item("_clie_apel") = usrSoci.Apel
            .Item("_soci_nume") = usrSoci.Codi
            .Item("dede_deca_id") = hdnId.Text

            If Not pbooAuto Then
                If .Item("dede_impo") <> txtImpo.Valor Or IIf(hdnDatosPop.Text = "", .Item("dede_envio_impo"), txtDedeEnvioImpo.Valor) <> txtImpo.Valor Then
                    .Item("dede_modi_obse") = txtObse.Valor
                    .Item("dede_modi_fecha") = Today
                    .Item("dede_modi_user") = Session("sUserId").ToString()
                Else
                    .Item("dede_modi_obse") = DBNull.Value
                    .Item("dede_modi_fecha") = DBNull.Value
                    .Item("dede_modi_user") = DBNull.Value
                End If
            End If

            If Not pbooAuto Then .Item("dede_envio_impo") = txtImpo.Text
            .Item("dede_auto") = IIf(lblAuto.Text = "SI", True, False)
            .Item("_auto") = IIf(lblAuto.Text = "SI", "SI", "NO")

            .Item("_coti_desc") = lblCotiDesc.Text
            .Item("dede_total_impo") = txtImpoOrig.Valor

            If hdnDedeId.Text = "" And pbooAlta Then
                .Item("dede_total_impo") = txtDedeTotalImpo.Valor
                .Item("dede_envio_impo") = txtImpo.Valor
            End If

            If hdnTaclId.Text <> "" Then .Item("dede_tacl_id") = hdnTaclId.Text
            'If hdnCompId.Text <> "" Then .Item("dede_comp_id") = hdnCompId.Text
        End With
        'If hdnDedeId.Text = "" And pbooAlta Then
        'mdsDatos.Tables(mstrDebitosDeta).Rows.Add(ldrDatos)
        'End If
    End Sub

    Private Sub mGuardarDatosSocio(ByVal dtOriDatos As DataTable)
        Dim ldrDatos As DataRow
        Dim ldrOriDatos As DataRow

        ldrDatos = mdsDatos.Tables(mstrDebitosDeta).NewRow
        ldrDatos.Item("dede_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrDebitosDeta), "dede_id")

        If Not dtOriDatos.Select("")(0) Is Nothing Then
            ldrOriDatos = dtOriDatos.Select("")(0)
        End If

        With ldrDatos
            .Item("dede_impo") = ldrOriDatos.Item("dede_impo")
            .Item("dede_cate_impo") = ldrOriDatos.Item("dede_cate_impo")
            .Item("dede_deca_id") = hdnId.Text
            .Item("dede_total_impo") = ldrOriDatos.Item("dede_impo")
            .Item("dede_envio_impo") = ldrOriDatos.Item("dede_impo")
            .Item("dede_soci_id") = ldrOriDatos.Item("dede_soci_id")
            .Item("dede_clie_id") = ldrOriDatos.Item("dede_clie_id")
            .Item("_clie_apel") = ldrOriDatos.Item("_clie_apel")
            .Item("_soci_nume") = ldrOriDatos.Item("_soci_nume")
            .Item("dede_auto") = False
            .Item("_auto") = "NO"
            .Item("dede_coti_id") = ldrOriDatos.Item("dede_coti_id")
            .Item("_coti_desc") = ldrOriDatos.Item("_coti_desc")
            .Item("dede_tacl_id") = ldrOriDatos.Item("dede_tacl_id")
            .Item("dede_comp_id") = ldrOriDatos.Item("dede_comp_id")
            .Item("dede_anio") = ldrOriDatos.Item("dede_anio")
            .Item("dede_perio") = ldrOriDatos.Item("dede_perio")
            .Item("dede_peti_id") = ldrOriDatos.Item("dede_peti_id")
            .Item("_tacl_nume") = ldrOriDatos.Item("_tacl_nume")
        End With

        mdsDatos.Tables(mstrDebitosDeta).Rows.Add(ldrDatos)

    End Sub

    Private Sub mLimpiarDeta()
        hdnDedeId.Text = ""
        hdnDatosPop.Text = ""
        hdnDescuen.Text = ""
        usrSoci.Limpiar()
        txtDedeTotalImpo.Valor = ""
        txtDedeEnvioImpo.Valor = ""
        txtImpo.Text = ""
        txtImpoOrig.Text = ""
        txtObse.Text = ""
        hdnTaclId.Text = ""
        hdnCompId.Text = ""
        hdnSociId.Text = ""
        txtAnteImpo.Text = ""
        txtTaclNume.Text = ""
        lblAuto.Text = "NO"
        lblCotiDesc.Text = ""

        mSetearEditor(mstrDebitosDeta, True, Not (txtEnviFecha.Fecha Is DBNull.Value))
    End Sub

    Private Sub btnLimpDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
        Try
            mConsultarDeta()
            mLimpiarDeta()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBajaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
        Try
            With mdsDatos.Tables(mstrDebitosDeta)
                For Each ldrDatos As DataRow In .Select("dede_soci_id=" & .Select("dede_id=" & hdnDedeId.Text)(0).Item("dede_soci_id"))
                    ldrDatos.Delete()
                Next
            End With
            mConsultarDeta()
            mLimpiarDeta()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnModiDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
        mActualizarDeta(False)
    End Sub

    Private Sub btnAltaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
        mActualizarDeta(True)
    End Sub

#End Region

    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            grdDato.CurrentPageIndex = 0
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        Try
            mAgregar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
        Try
            mListar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mListar()
        Dim lstrRptName As String = "Debitos"
        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        lstrRpt += "&deca_id=" + hdnId.Text
        lstrRpt += "&esta_id=0"
        lstrRpt += "&deca_anio=0"
        lstrRpt += "&deca_perio=0"
        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
        Response.Redirect(lstrRpt)
    End Sub

    Private Sub usrSoci_Cambio(ByVal sender As Object) Handles usrSoci.Cambio
        Try
            Dim lstrCmd As New StringBuilder
            Dim lDr As DataRow
            Dim lvDr() As DataRow

            ' cargo los datos en hiddens con un query
            ' si intenta darle alta le digo que tiene que seleccionar la deuda

            hdnSociId.Text = usrSoci.Valor.ToString

            btnDeuda.Disabled = hdnSociId.Text <> ""

            grdDeta.CurrentPageIndex = 0
            'mConsultarDeta(usrSoci.Valor.ToString)

            If usrSoci.Valor Is DBNull.Value Then
                Return
            End If

            mValidarDatos()

            lvDr = mdsDatos.Tables(mstrDebitosDeta).Select("dede_soci_id=" & usrSoci.Valor & " AND dede_total_impo<>0")

            If lvDr.GetUpperBound(0) <> -1 Then
                lDr = lvDr(0)
            Else
                lstrCmd.Append("exec debitos_deta_datos_consul")
                lstrCmd.Append(" @deca_anio=" + txtAnio.Valor.ToString)
                lstrCmd.Append(",@deca_peti_id=" + cmbPeti.Valor.ToString)
                lstrCmd.Append(",@deca_perio=" + txtPerio.Valor.ToString)
                lstrCmd.Append(",@deca_tarj_id=" + cmbTarj.Valor.ToString)
                lstrCmd.Append(",@dede_soci_id=" + usrSoci.Valor.ToString)
                lstrCmd.Append(",@deca_acti_id=" + CType(SRA_Neg.Constantes.Actividades.Socios, String))

                lDr = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString).Tables(0).Rows(0)
                If lDr.IsNull("dede_tacl_id") Then
                    usrSoci.Limpiar()
                    Throw New AccesoBD.clsErrNeg("El Socio no pertenece a la tarjeta seleccionada.")
                End If
            End If

            With lDr
                hdnDedeId.Text = .Item("dede_id").ToString

                If hdnDedeId.Text <> "" Then
                    txtImpo.Valor = .Item("dede_envio_impo")
                        txtImpoOrig.Valor = .Item("dede_total_impo")
                        If Not (.Item("dede_modi_obse") Is DBNull.Value) Then
                            txtObse.Valor = .Item("dede_modi_obse")
                        End If
                        lblCotiDesc.Text = .Item("_coti_desc").ToString
                        End If

                        txtTaclNume.Text = .Item("_tacl_nume").ToString
                lblAuto.Text = IIf(.IsNull("dede_id") OrElse Not .Item("dede_auto"), "NO", "SI")
                hdnTaclId.Text = .Item("dede_tacl_id").ToString
                hdnCompId.Text = .Item("dede_comp_id").ToString

            End With

            mSetearEditor(mstrDebitosDeta, IIf(hdnDedeId.Text = "", True, False))

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Function mCargarCuotaSocial(Optional ByVal pboolDSTemp As Boolean = False) As DataTable

        Dim lvrDatos() As String = hdnDatosPop.Text.Split(Chr(5))
        Dim lvrDato() As String
        Dim ldrDatos As DataRow
        Dim ldtDetaAUX As DataTable
        Dim lstrFiltro, lstrSociId As String
        Dim ldsDeuda As DataSet
        Dim ldrDeuda As DataRow
        Dim lbooVenc, lbooExiste As Boolean
        Dim ldecCateImpo As Decimal

        'copio la estructura de la tabla de detalle
        If pboolDSTemp Then ldtDetaAUX = mdsDatos.Tables(mstrDebitosDeta).Clone

        For i As Integer = 0 To lvrDatos.GetUpperBound(0)
            lbooVenc = False
            lvrDato = lvrDatos(i).Split(Chr(6))   'CompId(6)Anio(6)Perio(6)PetiId(6)InstId(6)ImpoCuota(6)ImpoInteres(6)SociId(6)Fecha(5)...
            lstrSociId = lvrDato(7)

            lstrFiltro = "dede_soci_id = " + lstrSociId

            If IsNumeric(lvrDato(0)) Then
                lstrFiltro += " AND dede_comp_id = " + lvrDato(0)
            Else
                lstrFiltro += " AND dede_anio = " + lvrDato(1)
                lstrFiltro += " AND dede_perio= " + lvrDato(2)
            End If
            lstrFiltro += " AND dede_deca_id <> -1"

            lbooVenc = CInt(lvrDato(1)) < txtAnio.Valor Or (CInt(lvrDato(1)) = txtAnio.Valor And CInt(lvrDato(2)) <= txtPerio.Valor)
            If mdsDatos.Tables(mstrDebitosDeta).Select(lstrFiltro).GetUpperBound(0) = -1 Then
                'AGREGO LA CUOTA SOCIAL
                If pboolDSTemp Then
                    ldrDatos = ldtDetaAUX.NewRow
                Else
                    ldrDatos = mdsDatos.Tables(mstrDebitosDeta).NewRow
                End If
                ldrDatos.Item("dede_id") = clsSQLServer.gObtenerId(ldrDatos.Table, "dede_id")

                With ldrDatos
                    .Item("dede_clie_id") = usrSoci.ClieId
                    .Item("dede_soci_id") = usrSoci.Valor
                    .Item("dede_tacl_id") = hdnTaclId.Text
                    .Item("dede_deca_id") = hdnId.Text

                    If IsNumeric(lvrDato(0)) Then .Item("dede_comp_id") = lvrDato(0)

                    .Item("dede_anio") = lvrDato(1)
                    .Item("dede_perio") = lvrDato(2)
                    .Item("dede_peti_id") = lvrDato(3)

                    If IsNumeric(lvrDato(4)) Then .Item("dede_inst_id") = lvrDato(4)

                    .Item("dede_cate_impo") = lvrDato(5)
                    .Item("dede_coti_id") = SRA_Neg.Constantes.ComprobTipos.Cuota_Social

                    .Item("dede_auto") = 0

                    .Item("_clie_apel") = usrSoci.Apel
                    .Item("_soci_nume") = usrSoci.Codi
                    .Item("_tacl_nume") = txtTaclNume.Text
                    .Item("_coti_desc") = "Cuota Social " & .Item("dede_perio") & "/" & .Item("dede_anio")
                    .Item("_auto") = "NO"

                    ldsDeuda = SRA_Neg.Comprobantes.gObtenerDatosDeudaSocio(mstrConn, .Item("dede_soci_id"), .Item("dede_anio"), .Item("dede_peti_id"), .Item("dede_perio"), 0)
                    ldrDeuda = ldsDeuda.Tables(0).Rows(0)

                    'SAP 08/2009 aunque la cuota este vencida se toman los valores de Debito Automatico
                    'If lbooVenc Then  'si est� vencido pone el importe original
                    '.Item("dede_impo") = .Item("dede_cate_impo")

                    'Else
                    .Item("dede_impo") = ldrDeuda.Item("cuca_deau_valor")
                    ldecCateImpo = ldrDeuda.Item("cuca_deau_valor")

                    'si tiene promociones
                    .Item("dede_prom_id") = ldrDeuda.Item("prom_id")
                    If Not .IsNull("dede_prom_id") Then
                        If ldrDeuda.Item("prom_impo") > 0 Then
                            .Item("dede_impo") = ldrDeuda.Item("prom_impo")
                            ldecCateImpo = ldrDeuda.Item("prom_impo")
                        Else
                            .Item("dede_impo") = .Item("dede_cate_impo") * ldrDeuda.Item("prom_porc") / 100
                        End If
                    End If
                    'End If

                    .Item("dede_intef_impo") = 0
                    .Item("dede_inted_impo") = 0
                    .Item("dede_envio_impo") = 0
                    .Item("dede_total_impo") = 0

                    If IsNumeric(lvrDato(6)) AndAlso lvrDato(6) <> 0 Then
                        'AGREGO EL INTERES (FLOTANTE O DEVENGADO)

                        If ldsDeuda.Tables.Count > 1 Then
                            .Item("dede_inted_comp_id") = ldsDeuda.Tables(1).Rows(0).Item("comp_id")

                            For Each ldrDeudaTmp As DataRow In ldsDeuda.Tables(1).Rows
                                .Item("dede_inted_impo") += ldrDeudaTmp.Item("comp_neto")
                            Next
                        End If

                        If .IsNull("dede_inted_comp_id") Then
                            .Item("dede_intef_impo") = lvrDato(6) - .Item("dede_inted_impo")
                        Else

                        End If
                    End If
                End With

                ldrDatos.Table.Rows.Add(ldrDatos)
            End If
        Next

        If pboolDSTemp Then
            Return ldtDetaAUX
        Else
            'BORRO LOS QUE AHORA NO EST�N
            SRA_Neg.Comprobantes.gBorrarCuotasSociSobrantes(mdsDatos.Tables(mstrDebitosDeta), usrSoci.Valor.ToString, "dede", lvrDatos, "Chr(6)")
        End If
    End Function
    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Dim lbooAuto As Boolean = False
        Dim lvrDatos() As String = hdnDatosPop.Text.Split(Chr(5))
        Dim lstrCompBaja As String = ""
        Dim lstrDecaId As String = ""
        Dim lvrDato() As String

        Try
            hdnDatosPop.Text = hdnDatosPop.Text.Replace("NADA", "")
            If (hdnDatosPop.Text <> "") Then
                Dim lvdrDeta(), lvdrDetaAUX() As DataRow
                Dim ldrDeta, ldrDetaPrinc As DataRow
                Dim lstrFiltro, lstrSociId As String
                Dim ldrDatos As DataRow

                Dim ldecImpo, ldecCateImpo As Decimal
                ' Dim lbooVenc, lbooExiste As Boolean
                Dim lintPerioIni, lintAnioIni, lintPerioFin, lintAnioFin, lintPetiId, lintInstId As Integer
                Dim ldtDetaAUX As DataTable
                Dim lbooExiste As Boolean

                For Each ldrDataRow As DataRow In mdsDatos.Tables(mstrDebitosDeta).Select("dede_soci_id=" + usrSoci.Valor.ToString())
                    With ldrDataRow
                        If Not ldrDataRow Is Nothing Then
                            For i As Integer = 0 To lvrDatos.GetUpperBound(0)
                                lvrDato = lvrDatos(i).Split(Chr(6))
                                lbooExiste = (.Item("dede_anio") = lvrDato(1) And .Item("dede_perio") = lvrDato(2))
                                If lbooExiste Then Exit For
                            Next
                        End If
                        If Not lbooExiste Then
                            .Item("dede_deca_id") = -1
                        Else
                            .Item("dede_deca_id") = hdnId.Text
                        End If
                    End With
                Next

                ldtDetaAUX = mCargarCuotaSocial(True)

                'UNIFICAMOS EL IMPORTE EN UN SOLO REGISTRO
                lstrFiltro = "dede_soci_id = " + usrSoci.Valor.ToString
                lvdrDeta = mdsDatos.Tables(mstrDebitosDeta).Select(lstrFiltro, "dede_anio,dede_perio")

                If lvdrDeta.GetUpperBound(0) = -1 And ldtDetaAUX.Rows.Count <> 0 Then
                    lbooAuto = True
                End If

                If lvdrDeta.GetUpperBound(0) <> -1 Or lbooAuto Then
                    If lvdrDeta.GetUpperBound(0) <> -1 Then 'Or (lbooAuto And ldtDetaAUX.Rows.Count <> 1) Then
                        For Each ldrDeta In lvdrDeta
                            With ldrDeta
                                ldrDatos = ldtDetaAUX.NewRow
                                ldrDatos.Item("dede_anio") = .Item("dede_anio")
                                ldrDatos.Item("dede_perio") = .Item("dede_perio")
                                ldrDatos.Item("dede_impo") = .Item("dede_impo")
                                ldrDatos.Item("dede_intef_impo") = .Item("dede_intef_impo")
                                ldrDatos.Item("dede_inted_impo") = .Item("dede_inted_impo")
                                ldrDatos.Item("dede_cate_impo") = .Item("dede_cate_impo")
                                ldrDatos.Item("dede_intef_impo") = .Item("dede_intef_impo")
                                ldrDatos.Item("dede_inted_impo") = .Item("dede_inted_impo")
                                ldrDatos.Item("dede_soci_id") = .Item("dede_soci_id")
                                ldrDatos.Item("dede_peti_id") = .Item("dede_peti_id")
                                ldrDatos.Item("_tacl_nume") = .Item("_tacl_nume")
                                ldrDatos.Item("dede_deca_id") = .Item("dede_deca_id")
                                ldrDatos.Item("dede_comp_id") = .Item("dede_comp_id")
                            End With
                            ldrDatos.Table.Rows.Add(ldrDatos)
                        Next
                    End If

                    lvdrDeta = ldtDetaAUX.Select(lstrFiltro, "dede_anio,dede_perio")

                    For Each ldrDeta In lvdrDeta
                        If ldrDetaPrinc Is Nothing Then
                            ldrDetaPrinc = ldrDeta
                        End If

                        With ldrDeta
                            If Not .Item("dede_deca_id") Is DBNull.Value Then
                                lstrDecaId = .Item("dede_deca_id")
                            Else
                                lstrDecaId = ""
                            End If
                            If lstrDecaId <> "-1" Then
                                ldecImpo += .Item("dede_impo") + IIf(.IsNull("dede_intef_impo"), "0", .Item("dede_intef_impo")) + IIf(.IsNull("dede_inted_impo"), "0", .Item("dede_inted_impo"))
                                ldecCateImpo += IIf(.IsNull("dede_cate_impo"), "0", .Item("dede_cate_impo")) + IIf(.IsNull("dede_intef_impo"), "0", .Item("dede_intef_impo")) + IIf(.IsNull("dede_inted_impo"), "0", .Item("dede_inted_impo"))
                            End If
                            If lintPerioIni = 0 And Not .IsNull("dede_perio") Then
                                lintPerioIni = .Item("dede_perio")
                                lintAnioIni = .Item("dede_anio")
                                lintPetiId = .Item("dede_peti_id")
                                If Not .IsNull("dede_inst_id") Then
                                    lintInstId = .Item("dede_inst_id")
                                End If
                                'lintPerioFin = .Item("dede_perio")
                                'lintAnioFin = .Item("dede_anio")
                            End If
                            lintPerioFin = .Item("dede_perio")
                            lintAnioFin = .Item("dede_anio")

                            .Item("dede_envio_impo") = 0
                            .Item("dede_total_impo") = 0
                        End With
                    Next

                    With ldrDetaPrinc

                        txtDedeTotalImpo.Valor = ldecCateImpo
                        txtDedeEnvioImpo.Valor = ldecImpo

                        txtImpo.Valor = ldecImpo
                        txtImpoOrig.Valor = ldecCateImpo

                        .Item("dede_envio_impo") = ldecImpo
                        .Item("dede_total_impo") = ldecCateImpo

                        lblCotiDesc.Text = "Bim." & lintAnioIni.ToString.Substring(2, 2) & "/" & lintPerioIni
                        lintPerioFin = lvrDatos(lvrDatos.GetUpperBound(0)).Split(Chr(6))(2)
                        If lintPerioIni <> lintPerioFin Or lintAnioIni <> lintAnioFin Then
                            lblCotiDesc.Text = lblCotiDesc.Text & "-" & lintAnioFin.ToString.Substring(2, 2) & "/" & lintPerioFin
                        End If
                    End With
                End If

                If lintAnioFin <> 0 And hdnDescuen.Text <> "" Then
                    ''mCalcultarDescuentoPagoAdelantado(lstrSociId, lintAnioFin, lintPerioFin, lintPetiId, lintInstId) ', ldrDetaPrinc)
                    txtImpo.Valor = txtImpo.Valor + CDec(hdnDescuen.Text)
                    'txtImpoOrig.Valor = txtImpoOrig.Valor + CDec(hdnDescuen.Text)
                End If

                With mdsDatos.Tables(mstrDebitosDeta)
                    If .Select("dede_deca_id = -1 and dede_auto=0 and dede_soci_id=" & usrSoci.Valor.ToString()).GetUpperBound(0) <> -1 Then
                        For Each ldrComp As DataRow In .Select("dede_deca_id = -1 and dede_auto=0 and dede_soci_id=" & usrSoci.Valor.ToString())
                            ldrComp.Item("dede_deca_id") = hdnId.Text
                        Next
                    End If
                End With

            End If

        Catch ex As Exception
            hdnDatosPop.Text = ""
            hdnDescuen.Text = ""
            clsError.gManejarError(Me, ex)

        End Try
    End Sub

    Private Sub mCalcultarDescuentoPagoAdelantado(ByVal pintSociId As Integer, ByVal pintAnio As Integer, ByVal pintPerio As Integer, ByVal pintPetiId As Integer, ByVal pintInstId As Integer) ', ByVal pdrDetaPrinc As DataRow)
        Dim lDs As DataSet

        If pintAnio <> 0 Then
            lDs = SRA_Neg.Comprobantes.gObtenerDescuentoSocioPagoAdel(mstrConn, pintSociId, pintAnio, pintPerio, pintPetiId, pintInstId, Today)

            If lDs.Tables(0).Rows.Count > 0 Then
                'With pdrDetaPrinc
                txtImpo.Valor = txtImpoOrig.Valor + +lDs.Tables(0).Rows(0).Item("cucd_valor")
                '.Item("dede_envio_impo") = .Item("dede_total_impo") + lDs.Tables(0).Rows(0).Item("cucd_valor")
                ' End With
            End If
        End If
    End Sub

    Private Sub mUnificarDescs()
        Dim ldrDetaPrinc, ldrDetaSoc As DataRow
        Dim lintPerioIni, lintAnioIni, lintPerioFin, lintAnioFin As Integer

        For Each ldrDeta As DataRow In mdsDatos.Tables(mstrDebitosDeta).Select("dede_total_impo>0")
            lintPerioIni = 0
            With mdsDatos.Tables(mstrDebitosDeta).Select("dede_soci_id = " + ldrDeta.Item("dede_soci_id").ToString, "dede_anio,dede_perio")
                If .GetUpperBound(0) > 0 Then
                    For i As Integer = 0 To .GetUpperBound(0)
                        ldrDetaSoc = .GetValue(i)
                        With ldrDetaSoc
                            If lintPerioIni = 0 Then
                                lintPerioIni = .Item("dede_perio")
                                lintAnioIni = .Item("dede_anio")
                            End If

                            lintPerioFin = .Item("dede_perio")
                            lintAnioFin = .Item("dede_anio")

                            If .Item("dede_envio_impo") > 0 Then
                                ldrDetaPrinc = ldrDetaSoc
                            End If
                        End With
                    Next

                    With ldrDetaPrinc
                        .Item("_coti_desc") = "Bim." & lintAnioIni.ToString.Substring(2, 2) & "/" & lintPerioIni

                        If lintPerioIni <> lintPerioFin Or lintAnioIni <> lintAnioFin Then
                            .Item("_coti_desc") = .Item("_coti_desc") & " - " & lintAnioFin.ToString.Substring(2, 2) & "/" & lintPerioFin
                        End If
                    End With
                End If
            End With
        Next
    End Sub

End Class
End Namespace
