<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EventosSociosCocktailRRII_pop" CodeFile="EventosSociosCocktailRRII_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Consulta Cocktail de Socios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
							<TR>
								<TD vAlign="top" align="right" width="100%" colSpan="6">&nbsp;
									<asp:imagebutton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes\Close3.bmp"></asp:imagebutton></TD>
							</TR>
							<tr>
								<td colSpan="6">
									<table width="100%">
										<TR>
											<TD style="HEIGHT: 8px" noWrap align="left"><asp:label id="lblApellidoNombre" runat="server" cssclass="titulo">Apellido y Nombre:</asp:label>&nbsp;
												<CC1:TEXTBOXTAB id="txtApellidoNomb" runat="server" cssclass="cuadrotexto" Width="350px"></CC1:TEXTBOXTAB></TD>
											<TD style="HEIGHT: 8px" width="26"><CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
													IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
													ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
										</TR>
									</table>
								</td>
							</tr>
							<TR height="100%">
								<TD vAlign="top" colSpan="6"><asp:datagrid id="grdConsulta" runat="server" width="100%" BorderStyle="None" AllowPaging="True"
										PageSize="20" BorderWidth="1px" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" ItemStyle-Height="5px"
										AutoGenerateColumns="False" OnPageIndexChanged="DataGrid_Page">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:BoundColumn DataField="rrii_nomb" HeaderText="Apellido y Nombre" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 24px" align="right" width="50%" colSpan="2" rowSpan="1">
									<BUTTON class="boton" id="btnEncontrado" style="WIDTH: 110px" type="button" runat="server"
										value="Deuda">Encontrado</BUTTON>&nbsp;</TD>
								<TD style="HEIGHT: 24px" align="left" width="50%" colSpan="2" rowSpan="1">&nbsp; <BUTTON class="boton" id="btnNoEncontrado" style="WIDTH: 110px" type="button" runat="server"
										value="Encontrado">No Encontrado</BUTTON></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">	
	
		</SCRIPT>
	</BODY>
</HTML>
