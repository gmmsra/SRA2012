Imports System.Web


Namespace SRA


Partial Class CuentasContables
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub




    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Public mstrTabla As String = "param_cuentas_contables"
    Public mstrCmd As String
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                mCargarCombos()
                mConsultar()
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mEstablecerPerfil()
        If (Not clsSQLServer.gMenuPermi(CType(Opciones.Perfiles, String), (mstrConn), (Session("sUserId").ToString()))) Then
            Response.Redirect("noaccess.aspx")
        End If
        btnGrabar.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Perfiles_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
    End Sub
    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            Dim ldsEsta As New DataSet

            lstrCmd.Append("exec " + mstrTabla + "_consul")
            ldsEsta = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

            With ldsEsta.Tables(0).Rows(0)
                    cmbDescuento.Valor = .Item("pcts_soci_dcto_cuct_id").ToString
                    cmbDeudIncob.Valor = .Item("pcts_inco_cuct_id").ToString
                    cmbEmerAgroS.Valor = .Item("pcts_soci_emer_cuct_id").ToString
                    cmbPrevDeudIncob.Valor = .Item("pcts_prev_inco_cuct_id").ToString
                    cmbRecargoMoraS.Valor = .Item("pcts_soci_mora_cuct_id").ToString
                    cmbSolicitudIngS.Valor = .Item("pcts_soci_soli_cuct_id").ToString
                    cmbIVATasaNormal.Valor = .Item("pcts_ivat_tasa_cuct_id").ToString
                    cmbIVATasaReducida.Valor = .Item("pcts_ivat_tasa_redu_cuct_id").ToString
                    cmbIVATasaSobre.Valor = .Item("pcts_ivat_tasa_sobre_cuct_id").ToString
                    cmbIVAPercepcion.Valor = .Item("pcts_ivat_perc_cuct_id").ToString
                    cmbSaldosCuenta.Valor = .Item("pcts_cobr_pend_cuct_id").ToString
                    cmbChequesDiaPesos.Valor = .Item("pcts_cheq_dia_pes_cuct_id").ToString
                    cmbChequesDiaDolares.Valor = .Item("pcts_cheq_dia_dol_cuct_id").ToString
                    cmbChequesDiferPesos.Valor = .Item("pcts_cheq_dif_pes_cuct_id").ToString
                    cmbChequesDiferDolares.Valor = .Item("pcts_cheq_dif_dol_cuct_id").ToString
                    cmbValoresCartera.Valor = .Item("pcts_val_cart_deud_cuct_id").ToString
                    cmbValoresCarteraAcreedor.Valor = .Item("pcts_val_cart_acre_cuct_id").ToString
                    cmbDifererCambio.Valor = .Item("pcts_dife_camb_cuct_id").ToString
                    cmbRecaudacionesDepositar.Valor = .Item("pcts_reca_depo_cuct_id").ToString
                    cmbGastosBancarios.Valor = .Item("pcts_banc_gast_cuct_id").ToString
                    cmbPrevDeudIncobServ.Valor = .Item("pcts_inco_serv_cuct_id").ToString
                    cmbSociBajaSoli.Valor = .Item("pcts_soci_baja_soli_cuct_id").ToString

                mCargarCentroCostos(cmbDescuento, cmbSociDctoCcos)
                mCargarCentroCostos(cmbSociBajaSoli, cmbSociBajaSoliCcos)
                mCargarCentroCostos(cmbDifererCambio, cmbDifererCambioCcos)
                mCargarCentroCostos(cmbDeudIncob, cmbDeudIncobCcos)
                mCargarCentroCostos(cmbDeudIncob, cmbSociIncoCcos)
                mCargarCentroCostos(cmbEmerAgroS, cmbEmerAgroSCCos)
                mCargarCentroCostos(cmbRecargoMoraS, cmbRecargoMoraSCcos)
                mCargarCentroCostos(cmbSolicitudIngS, cmbSociSoliCcos)

                    cmbDeudIncobCcos.Valor = .Item("pcts_inco_ccos_id").ToString
                    cmbEmerAgroSCCos.Valor = .Item("pcts_soci_emer_ccos_id").ToString
                    cmbRecargoMoraSCcos.Valor = .Item("pcts_soci_mora_ccos_id").ToString
                    cmbSociDctoCcos.Valor = .Item("pcts_soci_dcto_ccos_id").ToString
                    cmbSociSoliCcos.Valor = .Item("pcts_soci_soli_ccos_id").ToString
                    cmbSociIncoCcos.Valor = .Item("pcts_soci_inco_ccos_id").ToString
                    cmbSociBajaSoliCcos.Valor = .Item("pcts_soci_baja_soli_ccos_id").ToString
                    cmbDifererCambioCcos.Valor = .Item("pcts_dife_camb_ccos_id").ToString
                    cmbIIBBPerc.Valor = .Item("pcts_iibb_perc_cuct_id").ToString

            End With

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbChequesDiaDolares, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbChequesDiaPesos, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbChequesDiferDolares, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbChequesDiferPesos, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbDescuento, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbDeudIncob, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbDifererCambio, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbEmerAgroS, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbIVAPercepcion, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbIVATasaNormal, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbIVATasaReducida, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbIVATasaSobre, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbPrevDeudIncob, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbRecargoMoraS, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbRecaudacionesDepositar, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbSaldosCuenta, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbSolicitudIngS, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbValoresCartera, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbValoresCarteraAcreedor, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbGastosBancarios, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbSociBajaSoli, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbPrevDeudIncobServ, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbIIBBPerc, "S")

        'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbDeudIncobCcos, "S")
        'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbEmerAgroSCCos, "S")
        'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbRecargoMoraSCcos, "S")
        'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbSociDctoCcos, "S")
        'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbSociSoliCcos, "S")
        'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbSociIncoCcos, "S")
        'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbSociBajaSoliCcos, "S")
        'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbDifererCambioCcos, "S")
    End Sub
    Private Sub mCargarCentroCostos(ByVal pcmbCuen As System.Web.UI.WebControls.ListControl, ByVal pcmbCost As System.Web.UI.WebControls.ListControl)
        Dim lstrCta As String = ""
        If Not pcmbCuen.SelectedValue Is DBNull.Value Then
            lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "cuentas_ctables", "cuct_codi", "@cuct_id=" & pcmbCuen.SelectedValue)
            lstrCta = lstrCta.Substring(0, 1)
        End If
        If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
            clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, pcmbCost, "id", "descrip", "N")
        Else
            pcmbCost.Items.Clear()
        End If
    End Sub

#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnGrabar.Enabled = Not (pbooAlta)
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mModi()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Modi()

            mConsultar()
        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Private Function mGuardarDatos() As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla)

        With ldsEsta.Tables(0).Rows(0)
            .Item("pcts_soci_dcto_cuct_id") = cmbDescuento.Valor()
            .Item("pcts_inco_cuct_id") = cmbDeudIncob.Valor()
            .Item("pcts_soci_emer_cuct_id") = cmbEmerAgroS.Valor()
            .Item("pcts_prev_inco_cuct_id") = cmbPrevDeudIncob.Valor()
            .Item("pcts_soci_mora_cuct_id") = cmbRecargoMoraS.Valor()
            .Item("pcts_soci_soli_cuct_id") = cmbSolicitudIngS.Valor()
            .Item("pcts_cobr_pend_cuct_id") = cmbSaldosCuenta.Valor()
            .Item("pcts_cheq_dia_pes_cuct_id") = cmbChequesDiaPesos.Valor()
            .Item("pcts_cheq_dia_dol_cuct_id") = cmbChequesDiaDolares.Valor()
            .Item("pcts_cheq_dif_pes_cuct_id") = cmbChequesDiferPesos.Valor()
            .Item("pcts_cheq_dif_dol_cuct_id") = cmbChequesDiferDolares.Valor()
            .Item("pcts_val_cart_deud_cuct_id") = cmbValoresCartera.Valor()
            .Item("pcts_val_cart_acre_cuct_id") = cmbValoresCarteraAcreedor.Valor()
            .Item("pcts_ivat_perc_cuct_id") = cmbIVAPercepcion.Valor
            .Item("pcts_ivat_tasa_cuct_id") = cmbIVATasaNormal.Valor()
            .Item("pcts_ivat_tasa_redu_cuct_id") = cmbIVATasaReducida.Valor()
            .Item("pcts_ivat_tasa_sobre_cuct_id") = cmbIVATasaSobre.Valor()
            .Item("pcts_dife_camb_cuct_id") = cmbDifererCambio.Valor()
            .Item("pcts_reca_depo_cuct_id") = cmbRecaudacionesDepositar.Valor()
            '.Item("pcts_pago_rete_cuct_id") = cmbRetencionesCobranzas.Valor()
            .Item("pcts_banc_gast_cuct_id") = cmbGastosBancarios.Valor()

            .Item("pcts_inco_ccos_id") = cmbDeudIncobCcos.Valor()
            .Item("pcts_soci_emer_ccos_id") = cmbEmerAgroSCCos.Valor()
            '.Item("pcts_ingr_ccos_id") = cmbIngresosVariosCcos.Valor()
            .Item("pcts_soci_mora_ccos_id") = cmbRecargoMoraSCcos.Valor()
            .Item("pcts_inco_serv_cuct_id") = cmbPrevDeudIncobServ.Valor()

            .Item("pcts_soci_dcto_ccos_id") = cmbSociDctoCcos.Valor()
            .Item("pcts_soci_soli_ccos_id") = cmbSociSoliCcos.Valor()
            .Item("pcts_soci_inco_ccos_id") = cmbSociIncoCcos.Valor()
            .Item("pcts_soci_baja_soli_cuct_id") = cmbSociBajaSoli.Valor()
            .Item("pcts_soci_baja_soli_ccos_id") = cmbSociBajaSoliCcos.Valor()
            .Item("pcts_dife_camb_ccos_id") = cmbDifererCambioCcos.Valor()
            .Item("pcts_iibb_perc_cuct_id") = cmbIIBBPerc.Valor()
        End With

        Return ldsEsta
    End Function
#End Region

    Private Sub btnGrabar_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGrabar.Click
        mModi()
    End Sub

        'Private Function btnGrabar() As Object
        '    Throw New NotImplementedException
        'End Function


End Class
End Namespace
