<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Cobranzas" CodeFile="Cobranzas.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitu%>
		</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/impresion.js"></script>
		<SCRIPT language="javascript">
		
		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null);

	   	function mTarjetasAbrir()
	   	{
		 gAbrirVentanas("TarjetasHabilitadas_pop.aspx?EsConsul=1&titulo=Tarjetas Habilitadas&tabla=tarjetas_clientes&filtros=" + document.all("usrClie:txtId").value , 7, "700","300","100");
		}
		
       function mFechaValor()
		{  
		 
		  document.all("txtCotDolar").value = mTraerCotizacion(document.all("txtFechaValor").value);
		  document.all("hdnCotDolar").value = document.all("txtCotDolar").value;
		}

      function mTraerCotizacion(pFecha)
      {
		  var lstrCoti = EjecutarMetodoXML("ObtenerCotizacion", pFecha);
     	  return(lstrCoti);
      } 
      									
	   function mEstadosAbrir()
	   	{
	   	 mAbrirPop("Estado de Saldos","S");
	 	}
	  
	   function mEspecialesAbrir()
	   	{
	   	 mAbrirPop("Especificaciones Especiales","N");
	    }
	    	
      function mAbrirPop(ptitulo,popcion)
       {
         var sFiltroClie = document.all("usrClie:txtId").value;
	    if (sFiltroClie != '')
         {
         
          mAbrirVentanas("Alertas_pop.aspx?titulo="+ptitulo+"&amp;"+"origen="+popcion+"&amp;"+"clieId=" + document.all("usrClie:txtId").value+"&amp;"+"FechaValor=" + document.all("txtFechaValor").value+"&amp;"+"sociId="+document.all("hdnSocioId").value, 7);
	     }else
	     {
	      alert("Debe ingresar el cliente.")
	     }		
	   }  
	   	    
	   	function mAbrirVentanas(pstrPagina, pintOrden, pstrAncho, pstrAlto, pstrLeft, pstrTop)
		{
			//09/06/2011 // vVentanas[pintOrden] = window.open (pstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=" + pstrAncho + "px,height=" + pstrAlto + "px,left=" + pstrLeft + "px,top=" + pstrTop + "px");  
			vVentanas[pintOrden] = window.open (pstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=700px,height=400px,left=10px,top=10px");
			for(i=0;i<11;i++)
			{
				if(vVentanas[i]!=null)
				{
					try{vVentanas[i].focus();}
					catch(e){vVentanas[i];}
					finally{;}
				}
			}
			
			vVentanas[pintOrden].focus();
		}
		
		function mCerrarVentanas()
		{
			for(i=0;i<11;i++)
			{
				if(vVentanas[i]!=null)
				{
					vVentanas[i].close();
					vVentanas[i]=null;
				}
			}
		}			
	   	    				
		function expandir()
		{
		if(parent.frames.length!=0)
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function btnDeuda_click()
		{
			document.all("hdnTipoPop").value="D";
			
			
			// Roxi: Modificaci�n 04/12/2009
			// Se agrego el indicador cobra=1 para que el aviso de morosidad se muestre solo si la pagina se invoca desde cobranzas.
            var lstrPagina = "SociDeuda_Pop.aspx?sess=" + document.all("hdnSess").value + "&sel=1&soci_id=" + document.all("hdnSociId").value + "&fecha=" + document.all("hdnFecha").value + "&Origen=Cobranzas&psele="+document.all('hdnCuotSociSele').value+'&cobra=1';
			
			gAbrirVentanas(lstrPagina,8,650,500);

			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}

		function btnFacturas_click(pstrTipo)
		{

			document.all("hdnTipoPop").value = "F";
			if (document.all("hdnTipo").value == "AC" && pstrTipo == "F")
				pstrTipo = "FP";

			if (document.all("hdnTipo").value == "TR" && pstrTipo == "F")
				pstrTipo = "FT";
				
			if (document.all("hdnTipo").value == "AC" && pstrTipo == "D")
				pstrTipo = "DP";
			
			if (document.all("hdnTipo").value == "TR" && pstrTipo == "D")
				pstrTipo = "DT";	
			
			// Roxi: Modificaci�n 04/12/2009
			// Se agrego el indicador cobra=1 para que el aviso de morosidad se muestre solo si la pagina se invoca desde cobranzas.
			var lstrPagina = "CobranzasFact_Pop.aspx?tipo=" + pstrTipo + "&sess=" + document.all("hdnSess").value + "&clie_id=" + document.all("hdnClieId").value + "&fecha=" + document.all("hdnFecha").value + "&venc=0&cobra=1";
			gAbrirVentanas(lstrPagina,9,700,500);
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
				
		function btnVarios_click()
		{
			document.all("hdnTipoPop").value="C";

			var lstrPagina = "CobranzasVarios_Pop.aspx?clie=" + document.all("hdnClieId").value + "&sess=" + document.all("hdnSess").value;
			gAbrirVentanas(lstrPagina,10,650,350);
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
		
		function btnSaldoACta_click()
		{
			document.all("hdnTipoPop").value="S";
            
			var lstrPagina = "CobranzasSaldoACta_Pop.aspx?sess=" + document.all("hdnSess").value  + "&clie_id=" + document.all("hdnClieId").value ;
			gAbrirVentanas(lstrPagina,11,650,350);
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
		
		function btnPagos_click(pPatiId)
		{
			document.all("hdnTipoPop").value="P";

			var lstrNom = 'N';
			if (document.all["hdnNom"].value != '' && document.all["hdnTipo"].value == 'AB')			
				lstrNom = 'S';
			var lstrPagina = "Cobranzaspagos_Pop.aspx?sess=" + document.all("hdnSess").value + "&PatiId=" + pPatiId +"&totalAPagar=" + document.all["lblCabeTotalAplic"].innerText +"&totalPagado=" + document.all["lblCabeTotalPago"].innerText +"&diferencia=" + document.all["lblCabeTotalDifer"].innerText + "&cot=" + document.all["hdnCotDolar"].value + "&nom=" + lstrNom;
			gAbrirVentanas(lstrPagina,7,null,400);
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}

		function btnAplicCreditos_click()
		{
			document.all("hdnTipoPop").value="A";
            
            var lstrPagina = "CobranzasComprobACta_Pop.aspx?sess=" + document.all("hdnSess").value + "&clie_id=" + document.all("hdnClieId").value +"&totalAPagar=" + document.all["lblCabeTotalAplic"].innerText +"&totalPagado=" + document.all["lblCabeTotalPago"].innerText +"&diferencia=" + document.all["lblCabeTotalDifer"].innerText + "&tipo=" + document.all["hdnTipo"].value;
			gAbrirVentanas(lstrPagina,13,650,350);
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');ImprimirTodo();"
		bottommargin="0" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 80px" border="0">
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" height="25">
									<asp:label id="lblTituAbm" runat="server" cssclass="opcion">Cobranzas</asp:label>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center">
									<!--- encabezado de la cobranza---><asp:panel id="panDato" runat="server" Height="5px" Visible="False" BorderWidth="1px" BorderStyle="Solid"
										cssclass="titulo" width="100%">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 5px" cellPadding="0" align="left"
											border="0">
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																			<TD vAlign="top" align="right" width="100%" colSpan="3" height="5">
																				<TABLE id="TableAlertas" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																					<TR>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgTarj" style="DISPLAY: none" runat="server"><A id="imgTarj" href="javascript:mTarjetasAbrir();" runat="server"><IMG alt="Puede operar con tarjetas" src="imagenes/CreditCard.gif" border="0" runat="server" id="imgTarjTele">
																								</A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgAlertaSaldosAzul" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldos" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircAzul.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosAmarillo" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosV" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircAmarillo.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosVerde" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosA" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircVerde.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgAlertaSaldosRojo" style="DISPLAY: none" runat="server"><A id="imgAlertaSaldosR" href="javascript:mEstadosAbrir();" runat="server"><IMG alt="Estado de saldos" src="imagenes/CircRojo.bmp" border="0">
																								</A>
																							</DIV>
																						</TD>
																						<TD vAlign="top" align="right" width="100%" height="14">
																							<DIV id="DivimgEspecAzul" style="DISPLAY: none" runat="server"><A id="imgEspecAzul" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="imagenes/CircTrianAzul.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgEspecVerde" style="DISPLAY: none" runat="server"><A id="imgEspecVerde" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="imagenes/CircTrianVerde.bmp" border="0">
																								</A>
																							</DIV>
																							<DIV id="DivimgEspecRojo" style="DISPLAY: none" runat="server"><A id="imgEspecRojo" href="javascript:mEspecialesAbrir();" runat="server"><IMG alt="Especiales" src="imagenes/CircTrianRojo.bmp" border="0">
																								</A>
																							</DIV>
																						</TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																<TD colSpan="2"></TD>
																<TD vAlign="top" align="right" height="5"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																	<asp:label id="lblFechaIng" runat="server" cssclass="titulo">Fecha Ingreso:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<asp:label id="lblFechaIngreso" runat="server" cssclass="titulo"></asp:label></TD>
																<caption>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                    <tr>
                                                                        <td align="right" background="imagenes/formfdofields.jpg" style="WIDTH: 10%; HEIGHT: 14px" valign="top">
                                                                            <cc1:BotonImagen ID="btnOtrosDatos" runat="server" BackColor="Transparent" BorderStyle="None" BtnImage="edit.gif" CambiaValor="False" ForeColor="Transparent" ImageBoton="btnOtrosDatos.gif" ImageDisable="btnOtrosDatos0.gif" ImageEnable="btnOtrosDatos.gif" ImageOver="btnOtrosDatos2.gif" ImagesUrl="imagenes/" IncludesUrl="includes/" OutImage="del.gif" ToolTip="Otros Datos" />
                                                                        </td>
                                                                    </tr>
                                                                </caption>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																	<asp:label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																	<UC1:CLIE id="usrClie" runat="server" Tabla="Clientes" FilCUIT="True" FilCriaNume="False" FilLegaNume="True" FilDocuNume="true"
																		FilSociNume="False" FilClaveUnica="True" FilFanta="true" Ancho="800"
																		Saltos=""></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD></TD>
																<TD align="left" colSpan="2">
																	<CC1:TEXTBOXTAB id="txtNyap" runat="server" cssclass="cuadrotextodeshab" Width="100%" Enabled="False"
																		Obligatorio="True" onchange="mSetearClie();"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg">
																	<asp:label id="lblSocio" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;</TD>
																<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:numberbox id="txtSocio" runat="server" cssclass="cuadrotexto" Width="80px" onchange="mCargarClienteSocio()"></cc1:numberbox></TD>
																<TD style="HEIGHT: 5px" background="imagenes/formfdofields.jpg"></TD>
															</TR>
															<TR id="divCriador" runat="server">
																<TD style="HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																	<asp:Label id="lblraza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																<TD align="right" colSpan="2">
																	<TABLE class="FdoFld" id="tblCria" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<table border=0 cellpadding=0 cellspacing=0>
																				<tr>
																				<td>
																				<cc1:combobox class="combo" id="cmbRaza" runat="server" cssclass="cuadrotexto" Width="250px" onchange="mCargarClienteCriador('R')"
																					NomOper="razas_cargar" filtra="true" MostrarBotones="False"></cc1:combobox>
																				</td>
																				<td>
																					<IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																								onclick="mBotonBusquedaAvanzada('razas','raza_desc','cmbRaza','Razas','');"
																								alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																				</td>
																				</tr>
																				</table>
																			</TD>
																			<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<asp:label id="lblCriaNume" runat="server" cssclass="titulo"> Nro. Criador:</asp:label>
																				<cc1:numberbox id="txtCriaNume" runat="server" cssclass="cuadrotexto" Visible="true" Width="80px"
																					Obligatorio="True" onchange="mCargarClienteCriador('C')" MaxLength="12" MaxValor="9999999999999"
																					esdecimal="False"></cc1:numberbox></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR>
																<TD align="center" colSpan="3"></TD>
																<TD style="WIDTH: 10%" align="right" background="imagenes/formfdofields.jpg"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
																	height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																<TD style="WIDTH: 5%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 25%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																	<asp:label id="lblTPers" runat="server" cssclass="titulo">Tr�mite personal:</asp:label>&nbsp;</TD>
																<TD style="WIDTH: 70%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" colSpan="2">
																	<asp:CheckBox id="chkTPers" onclick="mSetearCheck();" Checked="True" CssClass="titulo" Runat="server"></asp:CheckBox>&nbsp;
																	<asp:label id="lblFechaValor" runat="server" cssclass="titulo">Fecha Valor:</asp:label>&nbsp;
																	<SPAN>
																		<cc1:DateBox id="txtFechaValor" onchange="mFechaValor();" runat="server" cssclass="cuadrotexto" Width="80px" Obligatorio="true"></cc1:DateBox></SPAN>&nbsp;&nbsp;
																	<asp:label id="lblCotDolar" runat="server" cssclass="titulo">Cot.Dolar:</asp:label>&nbsp;
																	<cc1:numberbox id="txtCotDolar" runat="server" cssclass="cuadrotexto" Width="40px" CambiaValor="True"
																		Enabled="False" CantMax="4" EsDecimal="True"></cc1:numberbox></TD>
																<TD style="WIDTH: 5%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
															</TR>
															<TR>
																<TD vAlign="middle" align="center" background="imagenes/formfdofields.jpg" colSpan="3"
																	height="30"><BUTTON class="boton" id="btnFacturasCab" style="WIDTH: 115px" onclick="btnFacturas_click('F');"
																		type="button" runat="server" value="Detalles">Factura Contado</BUTTON></TD>
															</TR>
															<TR>
																<TD colSpan="3" align="center">
																	<TABLE style="WIDTH: 100%">
																		<TR>
																			<TD style="HEIGHT: 14px">
																				<DIV id="divEstadoSaldos" style="DISPLAY: none" runat=server>
																					<TABLE class="FdoFld" runat=server id="tblEstadoSaldos" style="BORDER-RIGHT: blue thin solid; BORDER-TOP: blue thin solid; BORDER-LEFT: blue thin solid; WIDTH: 300px; BORDER-BOTTOM: blue thin solid; BORDER-COLLAPSE: collapse"
																						cellPadding="0" align="left" border="1">
																						<TR>
																							<TD align="right">
																								<asp:label id="lblSaldoCaCte" runat="server" cssclass="titulo">Saldo de Cta.Cte.:</asp:label>&nbsp;
																							</TD>
																							<TD>
																								<CC1:NUMBERBOX id="txtSaldoCaCteVenc" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblSaldoCtaSoci" runat="server" cssclass="titulo">Saldo de Cta. Social:</asp:label>&nbsp;
																							</TD>
																							<TD>
																								<CC1:NUMBERBOX id="txtSaldoCtaSociVenc" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblImpCuentaCtaCte" runat="server" cssclass="titulo">Importe a cuenta en Cta. Cte.:</asp:label>&nbsp;
																							</TD>
																							<TD>
																								<CC1:NUMBERBOX id="txtImpCuentaCtaCte" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblImpCuentaCtaSocial" runat="server" cssclass="titulo">Importe a cuenta en Cta. Social:</asp:label>&nbsp;
																							</TD>
																							<TD style="WIDTH: 70px">
																								<CC1:NUMBERBOX id="txtImpCuentaCtaSocial" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																						<TR>
																							<TD align="right">
																								<asp:label id="lblImpAcuses" runat="server" cssclass="titulo">Acuses de Recibo Pend.de Proc.:</asp:label>&nbsp;
																							</TD>
																							<TD style="WIDTH: 70px">
																								<CC1:NUMBERBOX id="txtImpAcuses" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																									Enabled="False"></CC1:NUMBERBOX></TD>
																						</TR>
																					</TABLE>
																				</DIV>
																				</td>
																							<TD style="HEIGHT: 14px">
																							<DIV id="divProformas" style="DISPLAY: none" runat="server">
																								<TABLE class="FdoFld" runat="server" id="Table3" style="BORDER-RIGHT: blue thin solid; BORDER-TOP: blue thin solid; BORDER-LEFT: blue thin solid; WIDTH: 290px; BORDER-BOTTOM: blue thin solid; BORDER-COLLAPSE: collapse"
																									cellPadding="0" align="left" border="1">
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfRRGG" runat="server" cssclass="titulo">Total Proformas RRGG:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfRRGG" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfLabo" runat="server" cssclass="titulo">Total Prof.Laboratorio:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfLabo" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfExpo" runat="server" cssclass="titulo">Total Prof.Exposiciones:</asp:label>&nbsp;
																										</TD>
																										<TD>
																											<CC1:NUMBERBOX id="txtTotalProfExpo" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																									<TR>
																										<TD align="right">
																											<asp:label id="lblTotalProfOtras" runat="server" cssclass="titulo">Total Otras Proformas:</asp:label>&nbsp;
																										</TD>
																										<TD style="WIDTH: 70px">
																											<CC1:NUMBERBOX id="txtTotalProfOtras" runat="server" cssclass="cuadrotextodeshab" Width="65px"
																												Enabled="False"></CC1:NUMBERBOX></TD>
																									</TR>
																								</TABLE>
																							</DIV>
																			</TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD align="right" valign=bottom>
																	<asp:ImageButton id="btnSigCabe" runat="server" ImageUrl="imagenes/fle.jpg" ToolTip="Posterior" CausesValidation="False"></asp:ImageButton></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR> <!--- panel compartido con datos importantes del encabezado --->
												<TD style="WIDTH: 100%" colSpan="2">
													<asp:panel id="panEnca" runat="server" cssclass="titulo" BorderWidth="2px" Visible="False"
														Width="100%">
														<TABLE id="tblEnca" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD vAlign="bottom">
																	<asp:label id="lblTitu" runat="server" cssclass="titulo"></asp:label></TD>
															</TR>
															<TR>
																<TD width="100%" colSpan="2">
																	<TABLE style="WIDTH: 100%">
																		<TR>
																			<TD style="WIDTH: 20%" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCabeClien" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;
																			</TD>
																			<TD align="left" background="imagenes/formfdofields.jpg" colSpan="5">
																				<asp:Label id="lblCabeCliente" runat="server" cssclass="titulo" Width="100%"></asp:Label></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCabeTotalApl" runat="server" cssclass="titulo">Total A Pagar:</asp:Label>&nbsp;
																			</TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCabeTotalAplic" runat="server" cssclass="titulo"></asp:Label></TD>
																			<TD align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCabeTotalPag" runat="server" cssclass="titulo">Total Pagado:</asp:Label>&nbsp;
																			</TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCabeTotalPago" runat="server" cssclass="titulo"></asp:Label></TD>
																			<TD align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCabeTotalDif" runat="server" cssclass="titulo">Diferencia:</asp:Label>&nbsp;
																			</TD>
																			<TD background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblCabeTotalDifer" runat="server" cssclass="titulo"></asp:Label></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR> <!--- detalle - Aplic-->
												<TD style="WIDTH: 100%" colSpan="2">
													<asp:panel id="panAplic" runat="server" cssclass="titulo" Visible="False" Width="100%">
														<TABLE class="FdoFld" id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD style="WIDTH: 100%" colSpan="2">
																	<asp:datagrid id="grdAplic" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" Visible="True"
																		AllowPaging="True" HorizontalAlign="Left" CellSpacing="1" GridLines="None" CellPadding="1"
																		OnPageIndexChanged="grdAplic_PageChanged" AutoGenerateColumns="False">
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<Columns>
																			<asp:BoundColumn DataField="temp_id" Visible="False"></asp:BoundColumn>
																			<asp:BoundColumn DataField="temp_tipo" Visible="False"></asp:BoundColumn>
																			<asp:BoundColumn DataField="temp_tipo_desc" HeaderText="Tipo"></asp:BoundColumn>
																			<asp:BoundColumn DataField="temp_desc" HeaderText="Descripci�n"></asp:BoundColumn>
																			<asp:BoundColumn DataField="temp_impo" HeaderText="Importe" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
															<TR>
															    <td background="imagenes/formfdofields.jpg" colspan="2" height="5" valign="bottom"></td>
                                                                <tr>
                                                                    <td align="right" valign="top"></td>
                                                                    <td>
                                                                        <table align="left" border="0" cellpadding="0" style="WIDTH: 100%">
                                                                            <tr id="trImpoApl1" runat="server">
                                                                                <td align="right" valign="top">
                                                                                    <asp:Label ID="lblImpoFactTit" runat="server" cssclass="titulo">Facturas:</asp:Label>
                                                                                    &nbsp;</td>
                                                                                <td>
                                                                                    <asp:Label ID="lblImpoFact" runat="server" cssclass="desc"></asp:Label>
                                                                                    &nbsp;</td>
                                                                                <td align="right" valign="top">
                                                                                    <asp:Label ID="lblImpoCtaCteTit" runat="server" cssclass="titulo">Cta.Cte.:</asp:Label>
                                                                                    &nbsp; </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblImpoCtaCte" runat="server" cssclass="desc"></asp:Label>
                                                                                    &nbsp;</td>
                                                                                <td align="right" valign="top">
                                                                                    <asp:Label ID="lblImpoDeudaTit" runat="server" cssclass="titulo">Deuda Social:</asp:Label>
                                                                                    &nbsp; </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblImpoDeuda" runat="server" cssclass="desc"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr id="trImpoApl2" runat="server">
                                                                                <td align="right" valign="top">
                                                                                    <asp:Label ID="lblImpoVariosTit" runat="server" cssclass="titulo">Varios:</asp:Label>
                                                                                    &nbsp; </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblImpoVarios" runat="server" cssclass="desc"></asp:Label>
                                                                                </td>
                                                                                <td align="right" valign="top">
                                                                                    <asp:Label ID="lblImpoSaldoCtaTit" runat="server" cssclass="titulo">Saldo a Cta.:</asp:Label>
                                                                                    &nbsp; &nbsp;</td>
                                                                                <td>
                                                                                    <asp:Label ID="lblImpoSaldoCta" runat="server" cssclass="desc"></asp:Label>
                                                                                </td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="center" background="imagenes/formfdofields.jpg" colspan="2" height="30" valign="middle">
                                                                        <button id="btnFacturas" runat="server" class="boton" onclick="btnFacturas_click('F');" style="WIDTH: 105px" type="button" value="Detalles">
                                                                            Factura Contado
                                                                        </button>
                                                                        &nbsp;&nbsp;
                                                                        <button id="btnCtaCte" runat="server" class="boton" onclick="btnFacturas_click('D');" style="WIDTH: 105px" type="button" value="Detalles">
                                                                            Deuda Cta.Cte.
                                                                        </button>
                                                                        &nbsp;&nbsp;
                                                                        <button id="btnDeuda" runat="server" class="boton" onclick="btnDeuda_click();" style="WIDTH: 90px" type="button" value="Detalles">
                                                                            Deuda Social
                                                                        </button>
                                                                        &nbsp;&nbsp;
                                                                        <button id="btnVarios" runat="server" class="boton" onclick="btnVarios_click();" style="WIDTH: 60px" type="button" value="Detalles">
                                                                            Varios
                                                                        </button>
                                                                        &nbsp;&nbsp;
                                                                        <button id="btnSaldoACta" runat="server" class="boton" onclick="btnSaldoACta_click();" style="WIDTH: 105px" type="button" value="Detalles">
                                                                            Saldo a Cuenta
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right" background="imagenes/formfdofields.jpg" colspan="2" height="30" valign="middle">
                                                                        <asp:ImageButton ID="btnAnteriorDeta" runat="server" CausesValidation="False" ImageUrl="imagenes/flea.bmp" ToolTip="Anterior" />
                                                                        &nbsp;
                                                                        <asp:ImageButton ID="btnPosteriorDeta" runat="server" CausesValidation="False" ImageUrl="imagenes/fle.jpg" ToolTip="Posterior" />
                                                                    </td>
                                                                </tr>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR> <!--- detalle - Pagos--->
												<TD style="WIDTH: 100%" colSpan="2">
													<asp:panel id="panPagos" runat="server" cssclass="titulo" Visible="False" Width="100%">
														<TABLE class="FdoFld" id="TtblPagos" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD style="WIDTH: 100%" colSpan="2">
																	<asp:datagrid id="grdPagos" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" Visible="True"
																		AllowPaging="True" HorizontalAlign="Left" CellSpacing="1" GridLines="None" CellPadding="1"
																		OnPageIndexChanged="grdPagos_PageChanged" AutoGenerateColumns="False">
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<Columns>
																			<asp:BoundColumn DataField="temp_desc" HeaderText="Descripci�n"></asp:BoundColumn>
																			<asp:BoundColumn DataField="temp_impo" HeaderText="Importe" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																			<asp:BoundColumn DataField="temp_mone_desc" HeaderText="Moneda"></asp:BoundColumn>
																			<asp:BoundColumn DataField="temp_mone_ori" HeaderText="Impo.Ori." DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right" width="120" background="imagenes/formfdofields.jpg">
																	<asp:Label id="Label15" runat="server" cssclass="titulo">Leyenda:</asp:Label>&nbsp;</TD>
																<TD width="100%" background="imagenes/formfdofields.jpg">
																	<CC1:TEXTBOXTAB id="txtObser" runat="server" cssclass="textolibre" Height="54px" Width="100%" TextMode="MultiLine"
																		EnterPorTab="False"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right"></TD>
																<TD>
																	<TABLE style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																		<TR id="trImpo1" runat="server">
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblImpoEfecTit" runat="server" cssclass="titulo">Efectivo:</asp:Label>&nbsp;</TD>
																			<TD>
																				<asp:Label id="lblImpoEfec" runat="server" cssclass="desc"></asp:Label>&nbsp;</TD>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblImpoTarjTit" runat="server" cssclass="titulo">Tarjeta:</asp:Label>&nbsp;
																			</TD>
																			<TD>
																				<asp:Label id="lblImpoTarj" runat="server" cssclass="desc"></asp:Label>&nbsp;</TD>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblImpoCheqTit" runat="server" cssclass="titulo">Cheques:</asp:Label>&nbsp;
																			</TD>
																			<TD>
																				<asp:Label id="lblImpoCheq" runat="server" cssclass="desc"></asp:Label></TD>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblImpoDinElecTit" runat="server" cssclass="titulo">Din.Elect.:</asp:Label>&nbsp;
																			</TD>
																			<TD>
																				<asp:Label id="lblImpoDinElec" runat="server" cssclass="desc"></asp:Label></TD>
																		</TR>
																		<TR>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblImpoAcreBancTit" runat="server" cssclass="titulo">Acred. en Cuenta:</asp:Label>&nbsp;
																			</TD>
																			<TD>
																				<asp:Label id="lblImpoAcreBanc" runat="server" cssclass="desc"></asp:Label></TD>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblImpoReteTit" runat="server" cssclass="titulo">Retenciones:</asp:Label>&nbsp;
																			</TD>
																			<TD>
																				<asp:Label id="lblImpoRete" runat="server" cssclass="desc"></asp:Label></TD>
																			<TD vAlign="top" align="right">
																				<asp:Label id="lblImpoAplicCredTit" runat="server" cssclass="titulo">Aplic. cr�ditos:</asp:Label>&nbsp;
																			</TD>
																			<TD>
																				<asp:Label id="lblImpoAplicCred" runat="server" cssclass="desc"></asp:Label></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
															<TR>
																<TD align="center" colSpan="2">
																	<asp:Label id="lblFactTarj" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
															</TR>
															<TR id="trPago1" runat="server">
																<TD vAlign="middle" align="center" background="imagenes/formfdofields.jpg" colSpan="2"><BUTTON class="boton" id="btnEfectivo" style="WIDTH: 110px" onclick="btnPagos_click(1);"
																		type="button" runat="server" value="Detalles">Efectivo</BUTTON>&nbsp;&nbsp;<BUTTON class="boton" id="btnTarjeta" style="WIDTH: 110px" onclick="btnPagos_click(3);"
																		type="button" runat="server" value="Detalles">Tarjeta</BUTTON>&nbsp;&nbsp;<BUTTON class="boton" id="btnCheques" style="WIDTH: 110px" onclick="btnPagos_click(2);"
																		type="button" runat="server" value="Detalles">Cheques</BUTTON>&nbsp;&nbsp;<BUTTON class="boton" id="btnDinElec" style="WIDTH: 110px" onclick="btnPagos_click(4);"
																		type="button" runat="server" value="Detalles">Din.Elect.</BUTTON>
																</TD>
															</TR>
															<TR id="trPago2" runat="server">
																<TD vAlign="middle" align="center" background="imagenes/formfdofields.jpg" colSpan="2"><BUTTON class="boton" id="btnAcred" style="WIDTH: 110px" onclick="btnPagos_click(5);" type="button"
																		runat="server" value="Detalles">Acred. en Cuenta</BUTTON>&nbsp;&nbsp;<BUTTON class="boton" id="btnReten" style="WIDTH: 110px" onclick="btnPagos_click(7);" type="button"
																		runat="server" value="Detalles">Retenciones</BUTTON>&nbsp;&nbsp;<BUTTON class="boton" id="btnAplic" style="WIDTH: 110px" onclick="btnAplicCreditos_click();"
																		type="button" runat="server" value="Detalles">Aplic. cr�ditos</BUTTON> &nbsp;&nbsp;
																	<asp:Label id="Label1" runat="server" cssclass="titulo" Width="100"></asp:Label></TD>
															</TR>
															<TR id="trPie" runat="server">
																<TD vAlign="middle" align="right" background="imagenes/formfdofields.jpg" colSpan="2"
																	height="30">
																	<asp:ImageButton id="btnAnteriorPie" runat="server" ImageUrl="imagenes/flea.bmp" ToolTip="Anterior"
																		CausesValidation="False"></asp:ImageButton>&nbsp;
																	<asp:ImageButton id="btnEncabezadoPie" runat="server" ImageUrl="imagenes/fleup.jpg" ToolTip="Cabecera"
																		CausesValidation="False"></asp:ImageButton>&nbsp;
																</TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD align="center" onclick="mChequearGeneraFactura();" colSpan="3"><CC1:BOTONIMAGEN id="ImgbtnGenerar" runat="server" BorderStyle="None" CambiaValor="False" ToolTip="Generar Comprobante"
										ImageUrl="imagenes/btngrab2.gif" ImageDisable="btnGrab0.gif" ImageEnable="btnGrab2.gif" BackColor="Transparent" ImageOver="btnGrab.gif" ImageBoton="btnGrab2.gif"
										BtnImage="edit.gif" OutImage="del.gif" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BOTONIMAGEN><CC1:BOTONIMAGEN id="ImgbtnLimpiar" runat="server" BorderStyle="None" CambiaValor="False" ToolTip="Limpiar"
										ImageUrl="imagenes/btnlimp2.gif" BackColor="Transparent" ImageOver="btnlimp.gif" ImageBoton="btnlimp2.gif" BtnImage="edit.gif" OutImage="del.gif" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BOTONIMAGEN></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnAnteFechaValor" runat="server"></asp:textbox>
				<asp:textbox id="hdnCuotSociSele" runat="server"></asp:textbox>
				<asp:textbox id="hdnCotDolar" runat="server"></asp:textbox>
				<asp:textbox id="hdnCotiAnterior" runat="server"></asp:textbox>			
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnAplicId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCria" runat="server"></asp:textbox>
				<asp:textbox id="hdnSociId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSociDatosPop" runat="server"></asp:textbox>
				<asp:textbox id="hdnClieId" runat="server"></asp:textbox>
				<asp:textbox id="hdnAntClieId" runat="server"></asp:textbox>
				<asp:textbox id="hdnFecha" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnTipoPop" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimir" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimirFact" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimioFact" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnImprimio" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnImprimioAcuse" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimirAcuse" runat="server"></asp:textbox>
				<asp:textbox id="hdnReciNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnAcuseNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimirNDs" runat="server"></asp:textbox>
				<asp:textbox id="hdnNDsNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprId" runat="server"></asp:textbox>
				<asp:textbox id="hdnTipoImprime" runat="server"></asp:textbox>
				<asp:textbox id="hdnFactNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimioNDs" runat="server"></asp:textbox>
				<asp:textbox id="hdnGeneraFactura" runat="server"></asp:textbox>
				<asp:textbox id="hdnTipo" runat="server"></asp:textbox>
				<asp:textbox id="hdnSocioId" runat="server"></asp:textbox>				
				<asp:textbox id="hdnMultiClie" runat="server"></asp:textbox>
				<asp:textbox id="hdnUnicaFC" runat="server"></asp:textbox>
				<asp:textbox id="hdnNom" runat="server"></asp:textbox>				
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT></OBJECT>
			</DIV>
		</form>
		<script language="javascript">
		function ImprimirTodo()
		{
			document.all("hdnImprId").value = '';
			
			var sRet;
			sRet = ImprimirRecibo();
			if(sRet!="0")
				sRet = ImprimirFactura();
			if(sRet!="0")
				sRet = ImprimirAcuse();
			if(sRet!="0")
				sRet = ImprimirNDs();

			if (sRet=="0"||document.all("hdnImprimio").value!=""||document.all("hdnImprimioAcuse").value!=""||document.all("hdnImprimioNDs").value!=""||document.all("hdnImprimioFact").value!="")
			{
				if(document.all("hdnImprimio").value=="")
					document.all("hdnImprimio").value="-1";	//por si no hay recibo,solo acuses
				__doPostBack('hdnImprimio','');
			}
		}
		 
		function ImprimirRecibo()
		{
			if(document.all("hdnImprimir").value!="")
			{
				if (window.confirm("�Desea imprimir " + document.all("hdnTipoImprime").value + " Nro " + document.all("hdnReciNume").value + "?"))
				{
					try{
						var sRet = mImprimirCopias(2,'O',document.all("hdnImprimir").value,"Recibo", "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original

						if(sRet=="0") return(sRet);

						if (window.confirm("�Se imprimi� " + document.all("hdnTipoImprime").value + " correctamente?"))
						{
						    var lstrCopias = LeerCamposXML("comprobantes_imprimir", document.all("hdnImprimir").value, "copias");

						    if (lstrCopias != "0" && lstrCopias != "1")
								sRet = mImprimirCopias(lstrCopias,'D',document.all("hdnImprimir").value,"Recibo","<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias

							document.all("hdnImprimio").value=1;							
							ImprimirTalones(document.all("hdnImprimir").value, "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");
						}
						else
						{
							document.all("hdnImprId").value = '';
							ImprimirRecibo();
						}
					}
					catch(e)
					{
						document.all("hdnImprId").value = '';
						alert("Error al intentar efectuar la impresi�n");
					}
				}
				else
				{
					document.all("hdnImprimio").value=0;
				}
			}
		}
		
		function ImprimirFactura()
		{
			if(document.all("hdnImprimirFact").value!="")
			{
				var vFCs = document.all("hdnImprimirFact").value.split(",");
				var vFCNumes = document.all("hdnFactNume").value.split(",");

				for(iFC=0; iFC<vFCs.length; iFC++)
				{
					if (window.confirm("�Desea imprimir la factura Nro " + vFCNumes[iFC] + "?"))
					{
						try
						{
							var lstrOri = 'O';
							var lstrImpre = LeerCamposXML("comprobantesX", vFCs[iFC], "comp_impre");
							if (lstrImpre == '1')
								lstrOri = 'D';
							var sRet = mImprimirCopias(2,lstrOri,vFCs[iFC],"Factura", "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original
														
							if(sRet=="0") return(sRet);
							
							if (window.confirm("�Se imprimi� la factura correctamente?"))
							{
								var lstrCopias = LeerCamposXML("comprobantes_imprimir", vFCs[iFC], "copias");

								if (lstrCopias != "0" && lstrCopias != "1")
									sRet = mImprimirCopias(lstrCopias,'D',vFCs[iFC],"Factura","<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias

								if (document.all("hdnImprimioFact").value!="") document.all("hdnImprimioFact").value += ",";
									document.all("hdnImprimioFact").value += vFCs[iFC];
							}
							else
							{
								document.all("hdnImprId").value = '';
								ImprimirFactura();
							}
						}
						catch(e)
						{
							document.all("hdnImprId").value = '';
							alert("Error al intentar efectuar la impresi�n");
						}
					}
					
					document.all("hdnImprimio").value="-1";
				}
			}
		}
		
		function ImprimirAcuse()
		{
			if(document.all("hdnImprimirAcuse").value!="")
			{
				if (window.confirm("�Desea imprimir el acuse Nro " + document.all("hdnAcuseNume").value + "?"))
				{
					try{
						var sRet = ImprimirReporte("Acuse", "comp_id;random", document.all("hdnImprimirAcuse").value + ";0", "<%=Session("sImpreTipo")%>",document.all("hdnImprimirAcuse").value,"<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");

						if(sRet=="0") return(sRet);
						
						if (window.confirm("�Se imprimi� el acuse correctamente?"))
						{
							document.all("hdnImprimioAcuse").value=1;
						}
						else
						{
							document.all("hdnImprId").value = '';
							ImprimirAcuse();
						}
					}
					catch(e)
					{
						document.all("hdnImprId").value = '';
						alert("Error al intentar efectuar la impresi�n");
					}
				}
				else
				{
					document.all("hdnImprimioAcuse").value=0;
				}
			}
		}
		function ImprimirNDs()
		{
			if(document.all("hdnImprimirNDs").value!="")
			{
				var vNDs = document.all("hdnImprimirNDs").value.split(",");
				var vNDNumes = document.all("hdnNDsNume").value.split(",");

				for(iNd=0; iNd<vNDs.length; iNd++)
				{
					if (window.confirm("�Desea imprimir la ND Nro " + vNDNumes[iNd] + "?"))
					{
						try{
							var sRet = mImprimirCopias(2,'O',vNDs[iNd],"Factura", "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original
													
							if(sRet=="0") return(sRet);
							
							if (window.confirm("�Se imprimi� la ND correctamente?"))
							{
								var lstrCopias = LeerCamposXML("comprobantes_imprimir", vNDs[iNd], "copias");

							    if (lstrCopias != "0" && lstrCopias != "1")
									sRet = mImprimirCopias(lstrCopias,'D',vNDs[iNd],"Factura","<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias

								if (document.all("hdnImprimioNDs").value!="") document.all("hdnImprimioNDs").value += ",";
								document.all("hdnImprimioNDs").value += vNDs[iNd];								
							}
							else
							{
								document.all("hdnImprId").value = '';
								ImprimirNDs();
							}
						}
						catch(e)
						{
							document.all("hdnImprId").value = '';
							alert("Error al intentar efectuar la impresi�n");
						}
					}
					document.all("hdnImprimio").value="-1";
				}
			}
		}
		
		if (document.all["editar"]!= null)
			document.location='#editar';

		if (document.all["usrClie:txtCodi"]!= null)
		{
			if(!document.all["usrClie:txtCodi"].disabled)
				document.all["usrClie:txtCodi"].focus();
		}

        if (document.all("btnOtrosDatos")!=null && document.all("usrClie:txtId")!=null)
            document.all("btnOtrosDatos").disabled = (document.all("usrClie:txtId").value=="");

		function mSetearClie()
		{
			if (document.all("btnOtrosDatos")!=null)
			{			
				document.all("hdnNom").value = document.all("txtNyap").value;
				
				var ClieHabi = true;
				var NyapHabi = true;
		        
				if (document.all("txtNyap").value != "")
					ClieHabi = false
				
				if (document.all("usrClie:txtCodi").value != "")
					NyapHabi = false

				document.all("btnOtrosDatos").disabled = (document.all("usrClie:txtId").value=="");
				
				document.all("hdnClieId").value = document.all("usrClie:txtId").value;

				gHabilitarControl(document.all("txtNyap"), NyapHabi);
				gHabilitarControl(document.all("usrClie:txtCodi"), ClieHabi);
				gHabilitarControl(document.all("usrClie:txtApel"), ClieHabi);

				if(document.all("usrClie:txtId").value!="")
				{
					var vstrRet = LeerCamposXML("clientes_soci", document.all("usrClie:txtId").value, "soci_id,soci_nume").split("|");
					document.all("hdnSociId").value = "";
					document.all("txtSocio").value = "";

					if(vstrRet!="")
					{
						document.all("hdnSociId").value = vstrRet[0];
						document.all("txtSocio").value = vstrRet[1];
					}
					
					if(document.all("tblEstadoSaldos")!=null)
					{
						//vstrRet = LeerCamposXML("clientes_estado_de_saldos", document.all("usrClie:txtId").value, "saldo_cta_cte_venc,saldo_social_venc,imp_a_cta_cta_cte,imp_a_cta_cta_social,TotalProformaRRGG,TotalProformaLabo,TotalProformaExpo,TotalProformaOtras").split("|");
						vstrRet = LeerCamposXML("clientes_estado_de_saldos", document.all("usrClie:txtId").value, "saldo_cta_cte,saldo_social,imp_a_cta_cta_cte,imp_a_cta_cta_social,TotalProformaRRGG,TotalProformaLabo,TotalProformaExpo,TotalProformaOtras").split("|");
						
						document.all("txtSaldoCaCteVenc").value  = vstrRet[0];
						document.all("txtSaldoCtaSociVenc").value  = vstrRet[1];
						document.all("txtImpCuentaCtaCte").value  = vstrRet[2];
						document.all("txtImpCuentaCtaSocial").value  = vstrRet[3];
						document.all("txtTotalProfRRGG").value  = vstrRet[4];
						document.all("txtTotalProfLabo").value  = vstrRet[5];
						document.all("txtTotalProfExpo").value  = vstrRet[6];
						document.all("txtTotalProfOtras").value  = vstrRet[7];				
						
						if(vstrRet[0]!=0||vstrRet[1]!=0||vstrRet[2]!=0||vstrRet[3]!=0)
							document.all("divEstadoSaldos").style.display="inline";
						else
							document.all("divEstadoSaldos").style.display="none";
						if(vstrRet[4]!=0||vstrRet[5]!=0||vstrRet[6]!=0||vstrRet[7]!=0)
							document.all("divProformas").style.display="inline";
						else
							document.all("divProformas").style.display="none";
							
						var vstrRet2 = LeerCamposXML("acuses_total", document.all("usrClie:txtId").value, "total_acuses").split("|");
						document.all("txtImpAcuses").value  = vstrRet2[0];
						
					}                
					mTarjetas();				
					mEstadoSaldos();	            
					mEspeciales();
				}
				else
				{
					document.all("hdnSociId").value = "";
					document.all("txtSocio").value = "";
					document.all("cmbRaza").value= "";
					document.all("txtcmbRaza").value= "";
					document.all("txtCriaNume").value = ""
					document.all("hdnCria").value ="";
					
					if(document.all("tblEstadoSaldos")!=null)
					{
						document.all("divEstadoSaldos").style.display="none";
						document.all("divProformas").style.display="none";
					}
				}
			}
		}		
		function mSetearVentaTele()
		{        
			if (document.all("imgTarjTele")!=null && document.all("usrClie:txtId").value!='')
			{
				// icono venta telefonica	
				var lstrImg = document.all("imgTarjTele").src;
				var strRet = LeerCamposXML("tarjetas_clientes", "@tacl_clie_id="+document.all("usrClie:txtId").value, "tacl_vta_tele");
				if (strRet == "0")
					lstrImg = lstrImg.replace('CreditCardTele.gif','CreditCard.gif');
				else
					lstrImg = lstrImg.replace('CreditCard.gif','CreditCardTele.gif');
				document.all("imgTarjTele").src = lstrImg;
			}
		}
		function mTarjetas()
        {
			var sRet=EjecutarMetodoXML("Utiles.Tarjetas", document.all("usrClie:txtId").value);
			document.all("DivimgTarj").style.display = "none";
			if (sRet == "-1") 
			{
				document.all("DivimgTarj").style.display ="inline";   // mostrar la imagen de la tarjeta  
				mSetearVentaTele();
			}
        } 
        
        function mEspeciales()
        {
			document.all("DivimgEspecVerde").style.display = "none";
			document.all("DivimgEspecRojo").style.display = "none";
			var sRet=EjecutarMetodoXML("Utiles.Especiales", document.all("usrClie:txtId").value);
			if(""!=sRet)
			{      
				if (sRet == "V")
					document.all("DivimgEspecVerde").style.display = "inline";
				else
					document.all("DivimgEspecRojo").style.display = "inline";
			}
         }                 
		
		function mEstadoSaldos()
		{
			document.all("DivimgAlertaSaldosAmarillo").style.display = "none";
			document.all("DivimgAlertaSaldosVerde").style.display = "none";
			document.all("DivimgAlertaSaldosRojo").style.display = "none";
			    
			var sFiltro = document.all("usrClie:txtId").value + ";" + document.all("txtFechaValor").value;
			var sRet=EjecutarMetodoXML("Utiles.EstadoSaldos", sFiltro);
			if(""!=sRet)
		   		{      
					if (sRet == "A")
					document.all("DivimgAlertaSaldosAmarillo").style.display = "inline";
					else
					{
						if (sRet == "V") 
							document.all("DivimgAlertaSaldosVerde").style.display = "inline";
						else
							document.all("DivimgAlertaSaldosRojo").style.display = "inline";
					} 
				}   
		}
		
		function usrClie_onchange()
		{
			mSetearClie();
		}
		
		function mCargarClienteCriador(pstrOri)
		{
			var sFiltro;
			var sFiltroC = document.all("txtCriaNume").value;//nro criador
			var sFiltroR = document.all("cmbRaza").value;//raza

 			if (sFiltroC != '' & sFiltroR != '')
 			{
 				sFiltro = "@cria_nume=" + sFiltroC + ", @raza_id=" + sFiltroR ;
				var vstrRet = LeerCamposXML("criadores_razas", sFiltro, "cria_clie_id,cria_id").split("|");
				
				if(vstrRet!="")
				{
					document.all["usrClie:txtCodi"].value = vstrRet[0]; //selecciona el cliente
					document.all["usrClie:txtCodi"].onchange();
				}
				else
				{
					alert("Raza/Criador inexistente");
					document.all("txtCriaNume").value = "";
				}
			}
		}
		
		function mCargarClienteSocio()
		{
			var sFiltro = document.all("txtSocio").value;

 			if (sFiltro != '')
 			{
 				sFiltro = "@soci_nume=" + sFiltro;
				var vstrRet = LeerCamposXML("socios_datos", sFiltro, "clie_id").split("|");
				
				if(vstrRet[0]=="")
					alert("Socio inexistente");
				else
				{
					document.all["usrClie:txtCodi"].value = vstrRet[0]; //selecciona el cliente
					document.all["usrClie:txtCodi"].onchange();
				}
			}
		} 	
    
		function mSetearCheck()
		{
		    if (document.all["chkTPers"].checked == true)
		    {
			 today = new Date();
	         var fechaActual;
	         fechaActual =today.getDate();
	         if ((fechaActual+"").length==1)
				fechaActual ="0" + fechaActual;

	        if (((today.getMonth()+1)+"").length==1)
				fechaActual = fechaActual+"/0"+(today.getMonth()+1);
			else
				fechaActual = fechaActual+"/"+(today.getMonth()+1);
			
			fechaActual = fechaActual+"/"+today.getYear()
			
             document.all("txtFechaValor").value = fechaActual //fecha de ingreso
			
			}
			
			var activ = document.all["chkTPers"].checked == false

             ActivarFecha("txtFechaValor",activ);
             mFechaValor();
	 	}

	 	function mChequearGeneraFactura()
		{
			if (window.event.srcElement.id=="ImgbtnGenerar")
			{
				//document.all('ImgbtnGenerar').disabled = true;
				
				if(document.all("hdnGeneraFactura").value=="1")
				{   
					if (document.all("hdnMultiClie").value == '')
					{
						// imprime recibo solo si se cancela una unica factura contado
						if (document.all("hdnUnicaFC").value == '1')
						{
							var lvstr = window.showModalDialog("CobranzasCheq_pop.aspx", "", "dialogHeight:160px;dialogWidth:350px;status=no,resizable=no").split(",");
							document.all("hdnGeneraFactura").value=lvstr[0];
							document.all("hdnImprId").value=lvstr[1];
						}
						else
						{
							document.all("hdnGeneraFactura").value=0;
							document.all("hdnImprId").value='';
						}
					}
					else
					{
						// si hay mas de un cliente imprime solo el recibo.
						document.all("hdnGeneraFactura").value=0;
						document.all("hdnImprId").value='';
					}
				}
			}
			return(true)
	 	}
	 	mSetearClie();
		mSetearVentaTele();
	 	if (document.all('chkTPers')!=null)
		{
	 		//document.all('chkTPers').checked = true;
	 		mSetearCheck();
	 	}
      	</script>
	</BODY>
</HTML>
