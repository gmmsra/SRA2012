Namespace SRA

Partial Class Tarjeta_info_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()

      If Page.IsPostBack Then
         For i As Integer = 0 To 2
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            grdConsulta.Columns.Add(dgCol)
         Next
      End If
   End Sub

#End Region


#Region "Definici�n de Variables"
   Public mstrCmd As String
   Public mstrTabla As String
   Public mstrTitulo As String
   Public mstrFiltros As String
   Private mstrConn As String
   Private mbooEsConsul As Boolean
   Private mbooAutopostback As Boolean
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      Try

         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            mConsultar(False)
         End If
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrTabla = Request.QueryString("tabla")
      mstrFiltros = Request.QueryString("filtros")

      lblTituAbm.Text = mstrTitulo

      
      If Not Request.QueryString("Autopostback") Is Nothing Then
         mbooAutopostback = CBool(CInt(Request.QueryString("Autopostback")))
      Else
         mbooAutopostback = True
      End If

      If mbooEsConsul Then
         grdConsulta.Columns(0).Visible = False
      End If
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.EditItemIndex = -1
         If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
            grdConsulta.CurrentPageIndex = 0
         Else
            grdConsulta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      Dim lsbMsg As New StringBuilder

      If mstrTabla <> "establecimientos_expedientes" Then
         lsbMsg.Append("<SCRIPT language='javascript'>")

         lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", E.Item.Cells(1).Text))
         If mbooAutopostback Then
            lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
         End If

         lsbMsg.Append("window.close();")
         lsbMsg.Append("</SCRIPT>")
         Response.Write(lsbMsg.ToString)
      End If

   End Sub

   Public Sub mConsultar(ByVal pbooPage As Boolean)
      Try
         mstrCmd = "exec " + mstrTabla + "_busq @tacl_id="
         mstrCmd += mstrFiltros

         Dim ds As New DataSet
         Dim lintFila As Integer
         ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

         If pbooPage Then
            Dim i As Integer = grdConsulta.Columns.Count - 1
            While i > 0
               grdConsulta.Columns.Remove(grdConsulta.Columns(i))
               i -= 1
            End While
         End If

         For Each dc As DataColumn In ds.Tables(1).Columns
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            dgCol.DataField = dc.ColumnName
            dgCol.HeaderText = dc.ColumnName
            If dc.Ordinal = 0 Then
               dgCol.Visible = False
            End If

            grdConsulta.Columns.Add(dgCol)
         Next
         grdConsulta.DataSource = ds.Tables(1)
         grdConsulta.DataBind()

         txtDire.Text = IIf(ds.Tables(0).Rows(0).Item("tacl_domi") Is DBNull.Value, "", ds.Tables(0).Rows(0).Item("tacl_domi"))
         txtCodSegu.Text = IIf(ds.Tables(0).Rows(0).Item("tacl_code_segu") Is DBNull.Value, "", SRA_Neg.Encript.gDesencriptar(ds.Tables(0).Rows(0).Item("tacl_code_segu").ToString))
         txtTitular.Text = IIf(ds.Tables(0).Rows(0).Item("tacl_titu") Is DBNull.Value, "", ds.Tables(0).Rows(0).Item("tacl_titu"))

         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

End Class

End Namespace
