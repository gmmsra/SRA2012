Namespace SRA

Partial Class Cuotas_Pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeDivponent()

   End Sub


   Protected WithEvents lblTitu As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeDivponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_PagosCuotas
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then

          
            For i As Integer = 0 To Session("mulPaginas")
               Session("mulPag" & i.ToString) = Nothing
            Next
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region


   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", E.Item.Cells(1).Text))
      lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub


   Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
      Dim lstrId As New StringBuilder
      Dim lstrClieAnt, lstrBuqeAnt As String
      Try
         If txtImp.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el importe de la cuota.")
         End If

         If txtFVto.Fecha Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de emisi�n de la cuota.")
         End If

         lstrId.Append(txtImp.Valor)
         lstrId.Append(",")
         lstrId.Append(txtCuotas.Text)
         lstrId.Append(",")
         lstrId.Append(txtFVto.Fecha)
         lstrId.Append(",")


         For i As Integer = 0 To Session("mulPaginas")
            If Not Session("mulPag" & i.ToString) Is Nothing Then
               Dim lstrIdsSess As String = Session("mulPag" & i.ToString)
               If lstrIdsSess <> "" Then
                  If lstrId.Length > 0 Then lstrId.Append(",http://localhost/SRA/PlanDePagos.aspx")
                  lstrId.Append(lstrIdsSess)
               End If
               Session("mulPag" & i.ToString) = Nothing
            End If
         Next


         Dim lsbMsg As New StringBuilder
         lsbMsg.Append("<SCRIPT language='javascript'>")
         lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", lstrId.ToString))
         lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
         lsbMsg.Append("window.close();")
         lsbMsg.Append("</SCRIPT>")
         Response.Write(lsbMsg.ToString)

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub
   



   Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
      Response.Write("<Script>window.close();</script>")
   End Sub

   Private Sub InitializeComponent()

   End Sub
End Class

End Namespace
