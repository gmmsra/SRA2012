Imports System.Data.SqlClient


Namespace SRA


Partial Class Localidades
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblDesc As System.Web.UI.WebControls.Label

   Protected WithEvents lblAnioFil As System.Web.UI.WebControls.Label
   Protected WithEvents lblClasiFil As System.Web.UI.WebControls.Label
   Protected WithEvents lbl As System.Web.UI.WebControls.Label


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "Localidades"
   Private mstrConn As String
   Public mstrTitu As String
   Private mstrPKey As String
   Private mstrOpc As String
   Private mstrDeno As String
   Private mstrCmd As String
   Private mintId As Integer
   Private mintDesc As Integer
   Private mintCP As Integer
   Private mintEstado As Integer
   Private mintGriFechaBaja As Integer
   Private mintFechaBaja As Integer
   Private mbooCodi As Boolean
   Private mbooCodiNum As Boolean
   Private mstrFK As String
   Private mstrFKDeno As String
   Private mstrFKId As String
   Private mstrFKvalor As String

   Private mintPopupPais As Integer
   Private mintPopupPcia As Integer
   Public mstrTipo As String

   Private mstrParaPageSize As Integer
   Private mstrItemCons As String

   Private mstrDescDeno As String

#End Region

#Region "Operaciones sobre la Pagina"

   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         mSetearMaxLength()
         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            'mCargarComboPaises()
            'mCargarProvincias()
            mConsultar()

            If Trim(mstrItemCons) <> "" And mstrTabla = "lineas" Then ' esto nunca se cumple!
               mCargarDatos()
            End If

         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbExclPais, "T")
      cmbExclPais.Valor = IIf(mintPopupPais = 0, clsSQLServer.gCampoValorConsul(mstrConn, "paises_consul @pais_defa=1", "pais_id"), mintPopupPais)
      clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbExclPcia, "T", IIf(cmbExclPais.Valor Is DBNull.Value, "-1", cmbExclPais.Valor))
      cmbExclPcia.Valor = mintPopupPcia

      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPais, "")
      cmbPais.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "paises_consul @pais_defa=1", "pais_id")
      clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbPais.Valor, cmbProv, "id", "descrip", "S", True)
   End Sub
   'Private Sub mCargarComboPaises()
   '   clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbExclPais, "S")
   '   cmbExclPais.Valor = mintPopupPais
   'End Sub
   'Private Sub mCargarProvincias()
   '   clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbExclPcia, "S", IIf(cmbExclPais.Valor Is DBNull.Value, "-1", cmbExclPais.Valor))
   '   cmbExclPcia.Valor = mintPopupPcia
   'End Sub
   'Private Sub cmbExclPais_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbExclPais.SelectedIndexChanged
   '   mCargarProvincias()
   '   mLimpiarGrilla()
   'End Sub
   Private Sub mCargarDatos()

      Dim lstrCmd As String

      mLimpiar()

      lstrCmd = "exec lineas_consul @line_id = " + mstrItemCons

      Dim myConnection As New SqlConnection(Session("sConn").ToString())
      Dim cmdExec = New SqlCommand
      myConnection.Open()
      cmdExec.Connection = myConnection
      cmdExec.CommandType = CommandType.Text
      cmdExec.CommandText = lstrCmd
      Dim dr As SqlDataReader = cmdExec.ExecuteReader()
      While (dr.Read())
         hdnId.Text = dr.GetValue(0).ToString.Trim
         txtDescrip.Text = dr.GetValue(1).ToString.Trim
      End While
      dr.Close()
      myConnection.Close()

      mSetearEditor(False)
      mMostrarPanel(True)

   End Sub 'Nunca se utiliza
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      If (Not clsSQLServer.gMenuPermi(mstrOpc, (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object
      Dim lintCol As Integer

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDescrip.MaxLength = Integer.Parse(lstrLongs(1, 1))
      txtDireCP.MaxLength = Integer.Parse(lstrLongs(2, 1))
   End Sub

   Private Function mEsConsul() As Boolean
      If Not Request.QueryString("EsConsul") Is Nothing Then
         Dim lboolConsul As Boolean = CBool(CInt(Request.QueryString("EsConsul")))
         Return lboolConsul
      Else
         Return False
      End If
   End Function
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mintId = 2
      mintDesc = 3
      mintCP = 4
      mintEstado = 5
      mintGriFechaBaja = 6

      mstrItemCons = Request.QueryString("id")

      hdnPopup.Text = Request.QueryString("t")

      grdDato.Columns(0).Visible = mEsConsul()
      cmbExclPais.Enabled = Not grdDato.Columns(0).Visible
      cmbExclPcia.Enabled = Not grdDato.Columns(0).Visible


      If Request.QueryString("pa") <> "" Then
         mintPopupPais = Request.QueryString("pa")
      End If
      If Request.QueryString("pr") <> "" Then
         mintPopupPcia = Request.QueryString("pr")
      End If

      If hdnPopup.Text <> "" Then
         mstrTipo = Request.QueryString("tipo")
      End If
      'btnAgre.Text = "Nueva Localidad >>"

      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub



#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lDsDatos As DataSet

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(mintId).Text)
      lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With lDsDatos.Tables(0).Rows(0)
         cmbPais.Valor = .Item("_pcia_pais_id")
         cmbProv.Valor = .Item("loca_pcia_id")
         txtDescrip.Valor = .Item("loca_desc")
         txtDireCP.Valor = .Item("loca_cpos")
         txtDireCP.ValidarPaisDefault = IIf(cmbPais.Valor = Session("sPaisDefaId"), "S", "N")
         If Not .IsNull("loca_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("loca_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With

      cmbPais.Enabled = False
      cmbProv.Enabled = False

      mSetearEditor(False)
      mMostrarPanel(True)
   End Sub

   Public Sub mSeleccionarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mRetorno(E.Item.Cells(mintId).Text)
   End Sub
   Private Sub mLimpiarGrilla()
      grdDato.CurrentPageIndex = 0
      mConsultar()

   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul "
         mstrCmd += " @loca_pcia_id = " + clsSQLServer.gFormatArg(cmbExclPcia.SelectedValue, SqlDbType.Int)
         mstrCmd += " ,@pcia_pais_id = " + cmbExclPais.Valor.ToString
         mstrCmd += ",@loca_desc = '" + txtNombreFil.Text + "'"


         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub


   Public Sub mRetorno(ByVal pstrID As String)
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.document.all['hdnLocaPop'].value='{0}';", pstrID))
      lsbMsg.Append("window.opener.__doPostBack('hdnLocaPop','');")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
      Session(mstrTabla) = Nothing

   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      cmbPais.Enabled = True
      cmbProv.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      txtDescrip.Text = ""
      txtDireCP.Text = ""
      lblBaja.Text = ""
      mSetearEditor(True)
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mValidarDatos()

         Dim ldsEstruc As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mValidarDatos()

         Dim ldsEstruc As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidarDatos()
      If txtDescrip.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la descripci�n")
      End If
      If cmbExclPcia.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar una provincia")
      End If

   End Sub
   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
         .Item("loca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("loca_desc") = txtDescrip.Valor
         .Item("loca_cpos") = txtDireCP.Valor
         '.Item("loca_pcia_id") = cmbExclPcia.Valor
         .Item("loca_pcia_id") = cmbProv.Valor
         .Item("loca_baja_fecha") = DBNull.Value 'Chequear si anda bien para reemplazar lo de abajo
         'For i As Integer = 0 To .Table.Columns.Count - 1
         '   If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
         '      .Item(i) = DBNull.Value
         '   End If
         'Next
      End With
      Return ldsEsta
   End Function

#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub cmbExclPcia_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
 Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
  grdDato.CurrentPageIndex = 0
  mConsultar()
 End Sub
 Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
  txtNombreFil.Text = ""
  cmbExclPais.Limpiar()
  cmbExclPcia.Limpiar()
 End Sub
 Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
  Dim intPcia As Integer

  Try
   Dim lstrRptName As String = "Localidades"
   Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
   intPcia = clsSQLServer.gFormatArg(cmbExclPcia.SelectedValue, SqlDbType.Int)

   If cmbExclPcia.SelectedValue <> "" Then
    lstrRpt += "&pcia_id=" + cmbExclPcia.SelectedValue
   End If
   'lstrRpt += "&pcia_id=" + IIf(cmbExclPcia.SelectedValue = "", "null", cmbExclPcia.SelectedValue)
   lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
   Response.Redirect(lstrRpt)

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub
#End Region

End Class
End Namespace
