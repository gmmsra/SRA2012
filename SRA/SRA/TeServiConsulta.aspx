<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.TeServiConsulta" CodeFile="TeServiConsulta.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript"> 
		function mImprimir()
		{
			window.print();
		}		
		function mAlta(pTabla)
		{
			if (pTabla == 'te_denun_deta_naci')
				gAbrirVentanas("DenuTE.aspx", 2, "700","400","100","50");
			else
				gAbrirVentanas("DenunciaServicio.aspx", 2, "700","400","100","50");
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG class="noprint" height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td class="noprint" background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG class="noprint" height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG class="noprint" height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD vAlign="middle" align="right">
									<DIV onclick="window.close();"><IMG class="noprint" style="CURSOR: hand" alt="Cerrar" src="imagenes/close3.bmp" border="0">
									</DIV>
								</TD>
							</TR>
							<TR>
								<TD><asp:label id="lblTituAbm" runat="server" cssclass="titulo" Font-Size="X-Small"></asp:label></TD>
							</TR>
							<TR>
								<TD>
									<TABLE id="Table2" class="noprint" cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD style="WIDTH: 157px" align="right" noWrap>
												<asp:Label id="lblMadreFil" runat="server" cssclass="titulo">Madre:</asp:Label>&nbsp;</TD>
											<TD>
												<cc1:numberbox id="txtMadreNumeFil" runat="server" cssclass="cuadrotexto" esdecimal="False" MaxValor="999999999"
													MaxLength="9" Width="80px"></cc1:numberbox>
												<CC1:TEXTBOXTAB id="txtReceCaraFil" runat="server" cssclass="cuadrotexto" Width="99px" MaxLength="10"></CC1:TEXTBOXTAB>
												<asp:Label id="lblRpFil" runat="server" cssclass="titulo">RP:</asp:Label>
												<CC1:TEXTBOXTAB id="txtMadreRpFil" runat="server" cssclass="cuadrotexto" Width="99px" MaxLength="20"></CC1:TEXTBOXTAB></TD>
											<TD></TD>
										</TR>
										<TR>
											<TD align="right" style="WIDTH: 157px; HEIGHT: 15px">
												<asp:Label id="lblAnioFil" runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
											<TD style="HEIGHT: 15px">
												<cc1:numberbox id="txtAnioFil" runat="server" cssclass="cuadrotexto" Width="80px" MaxLength="4"
													MaxValor="2050" esdecimal="False"></cc1:numberbox></TD>
											<TD rowspan="2">
												<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
													BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
													BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
										</TR>
										<TR>
											<TD align="right" style="WIDTH: 157px">
												<asp:Label id="lblAsocFil" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
											<TD>
												<cc1:combobox class="combo" id="cmbAsocFil" runat="server" Width="203px" AceptaNull="True">
													<asp:ListItem Value="T" Selected="True">(Todos)</asp:ListItem>
													<asp:ListItem Value="S">Asociados a Nacimientos</asp:ListItem>
													<asp:ListItem Value="N">No Asociados a Nacimientos</asp:ListItem>
												</cc1:combobox></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
										ItemStyle-Height="5px" PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="5%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
														Height="5">
														<img src='images/edit.gif' border="0" alt="Seleccionar Registro" style="cursor:hand;"
															vspace="0" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG class="noprint" height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<TR>
					<TD width="9" background="imagenes/reciz.jpg"></TD>
					<TD vAlign="middle" align="right"><div class="noprint">
							<asp:Button id="btnAgre" runat="server" cssclass="boton" Width="145px" Text="Agregar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
							<asp:Button id="btnImpri" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
								Text="Imprimir"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div>
					</TD>
					<TD width="13" background="imagenes/recde.jpg"></TD>
				</TR>
				<tr>
					<td width="9"><IMG class="noprint" height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td class="noprint" background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG class="noprint" height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
