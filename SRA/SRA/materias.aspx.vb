Namespace SRA

Partial Class materias
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents Panedfdf As System.Web.UI.WebControls.Panel
   Protected WithEvents Peri As NixorControls.ComboBox
   Protected WithEvents periodo As NixorControls.TextBoxTab
   Protected WithEvents panDire As System.Web.UI.WebControls.Panel

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "materias"
   Private mstrMateEqui As String = "mate_equiva"
   Private mstrMateCorre As String = "mate_correl"
   Private mstrMateCarre As String = "carreras_materias"
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
   Private mstrItemCons As String
   Private mintinsti As Int32
   Private Enum Columnas As Integer
      MateId = 1
      MateDesc = 2
      MateTeoHs = 3
      MatePracHs = 4
   End Enum
   Private Enum ColumnasDeta As Integer
      Id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar(grdDato, True)
            tabLinks.Visible = False
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaEqui.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaCorre.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaCarre.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Profesores, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Profesores_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrMateriasLong As Object
      lstrMateriasLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrMateriasLong, "mate_desc")
      txtAbre.MaxLength = clsSQLServer.gObtenerLongitud(lstrMateriasLong, "mate_abre")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrItemCons = Request.QueryString("id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintinsti = Convert.ToInt32(Request.QueryString("fkvalor"))
      lblInstituto.Text = Request.QueryString("fkvalor")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(grdDato, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrMateEqui
            btnBajaEqui.Enabled = Not (pbooAlta)
            btnModiEqui.Enabled = Not (pbooAlta)
            btnAltaEqui.Enabled = pbooAlta
         Case mstrMateCorre
            btnBajaCorre.Enabled = Not (pbooAlta)
            btnModiCorre.Enabled = Not (pbooAlta)
            btnAltaCorre.Enabled = pbooAlta
         Case mstrMateCarre
            btnBajaCarre.Enabled = Not (pbooAlta)
            btnModiCarre.Enabled = Not (pbooAlta)
            btnAltaCarre.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      btnList.Visible = False
      lblBaja.Text = ""
      hdnId.Text = ""
      txtDesc.Text = ""
      txtAbre.Text = ""
      txtHsTeo.Text = ""
      txtHsPrac.Text = ""
        Chkintensivo.Checked = True
        ChkPresencia.Checked = False            'AG 24/08/2011
      chkCalifica.Checked = False
      cmbEsta.Limpiar()
      cmbPeri.Limpiar()

      mLimpiarCorre()
      mLimpiarEqui()
      mLimpiarCarre()

      mCrearDataSet("")

      lblTitu.Text = ""

      grdCorre.CurrentPageIndex = 0
      grdCarre.CurrentPageIndex = 0
      grdEqui.CurrentPageIndex = 0
      mShowTabs(1)
      mSetearEditor("", True)
   End Sub
   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub
   Private Sub mLimpiarEqui()
      hdnEquiId.Text = ""
      txtFechaDesdeEqui.Text = ""
      txtFechaHastaEqui.Text = ""
      cmbCarreEqui.Limpiar()
      cmbMateEqui.Limpiar()
      lblBajaEquiva.Text = ""

      mSetearEditor(mstrMateEqui, True)
   End Sub
   Private Sub mLimpiarCorre()
      hdnCorreId.Text = ""

      cmbCarre.Limpiar()
      cmbMate.Limpiar()
      txtFechaHasta.Text = ""
      txtFechaDesde.Text = ""
      lblBajaCorre.Text = ""

      mSetearEditor(mstrMateCorre, True)
   End Sub
   Private Sub mLimpiarCarre()
      hdnCarreId.Text = ""
      chkObligatorio.Checked = True
      txtPerioCarre.Text = ""
      cmbCarreCarre.Limpiar()
      cmbPeriCarre.Limpiar()
      lblBajaCarre.Text = ""

      mSetearEditor(mstrMateCarre, True)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      If (pbooVisi) Then
         hdnPage.Text = " "
      Else
         hdnPage.Text = ""
      End If
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi

      btnAgre.Enabled = Not (panDato.Visible)
      grdDato.Visible = Not panDato.Visible
      btnAgre.Visible = Not (panDato.Visible)

      lnkCabecera.Font.Bold = True
      lnkCorre.Font.Bold = False


      panCabecera.Visible = True
      panCorre.Visible = False

      tabLinks.Visible = pbooVisi

      mCargarCombosDetalles()

      lnkCabecera.Font.Bold = True
      lnkCorre.Font.Bold = False
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "S", "@esti_id = 11")
      clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPeri, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPeriCarre, "S")
      'clsWeb.gCargarRefeCmb(mstrConn, "carreras", cmbCarreCarre, "S")
   End Sub
   Private Sub mCargarCombosDetalles()
      Dim materiaId As String = "@mate_id = "
      Dim filtroMate As String = "@mate_inse_id = " + mintinsti.ToString() + ","
      Dim filtroCarr As String = "@carr_inse_id = " + mintinsti.ToString()
      If (hdnId.Text <> "") Then
         materiaId = materiaId + hdnId.Text
      Else
         materiaId = materiaId + "null"
      End If
      filtroMate = filtroMate + materiaId

      'mCargarComboConDataSet(cmbCarre, mdsDatos.Tables(mstrMateCarre), "cama_carr_id", "_carr_desc")
      clsWeb.gCargarRefeCmb(mstrConn, "carreras", cmbCarre, "S", filtroCarr)
      clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMate, "S", filtroMate)

      'mCargarComboConDataSet(cmbCarreEqui, mdsDatos.Tables(mstrMateCarre), "cama_carr_id", "_carr_desc")
      clsWeb.gCargarRefeCmb(mstrConn, "carreras", cmbCarreEqui, "S", filtroCarr)
      clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMateEqui, "S", filtroMate)

      clsWeb.gCargarRefeCmb(mstrConn, "carreras", cmbCarreCarre, "S", filtroCarr)
   End Sub

   Private Sub mCargarComboConDataSet(ByVal cmb As NixorControls.ComboBox, ByVal dataTable As Data.DataTable, ByVal strId As String, ByVal strDesc As String)
      cmb.DataSource = dataTable
      cmb.DataValueField = strId
      cmb.DataTextField = strDesc
      cmb.DataBind()
      cmb.Items.Insert(0, "(Seleccione)")
      cmb.Items(0).Value = ""
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjMateria As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjMateria.Alta()
         mConsultar(grdDato, False)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         'mChequearEstado()
         mGuardarDatos()
         Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjCliente.Modi()
         mConsultar(grdDato, False)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCliente As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0


         mConsultar(grdDato, False)

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage

         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer

      If txtDesc.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la descripci�n.")
      End If
      If txtAbre.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la abreviatura.")
      End If
      If txtHsTeo.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar las horas de teoria.")
      End If
      If txtHsPrac.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar las horas de pr�ctica.")
      End If
      If cmbPeri.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el periodo.")
      End If
      If ((txtHsPrac.Valor + txtHsTeo.Valor) = 0) Then
         Throw New AccesoBD.clsErrNeg("La materia debe tener carga horaria.")
      End If

      With mdsDatos.Tables(0).Rows(0)
         .Item("mate_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("mate_desc") = txtDesc.Valor
         .Item("mate_abre") = txtAbre.Valor
         .Item("mate_teoria_hs") = txtHsTeo.Valor
         .Item("mate_prac_hs") = txtHsPrac.Valor
         .Item("mate_esta_id") = cmbEsta.Valor
         .Item("mate_inse_id") = mintinsti
            .Item("mate_curs_inte") = Chkintensivo.Checked
            .Item("mate_pres") = ChkPresencia.Checked               'AG 24/08/2011
            .Item("mate_peti_id") = cmbPeri.Valor
         .Item("mate_cali") = chkCalifica.Checked
         .Item("mate_baja_fecha") = DBNull.Value
         'mActualizarPeriodoTipo(cmbPeri.Valor)
         'If Not .IsNull("mate_baja_fecha") Then
         '   lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("mate_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         'Else
         '   lblBaja.Text = ""
         'End If

         'For i As Integer = 0 To .Table.Columns.Count - 1
         '   If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
         '      .Item(i) = DBNull.Value
         '   End If
         'Next
      End With
      Return mdsDatos
   End Function
   Public Sub mCrearDataSet(ByVal pstrId As String)
      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrMateEqui
      mdsDatos.Tables(2).TableName = mstrMateCorre
      mdsDatos.Tables(3).TableName = mstrMateCarre

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      grdEqui.DataSource = mdsDatos.Tables(mstrMateEqui)
      grdEqui.DataBind()

      grdCorre.DataSource = mdsDatos.Tables(mstrMateCorre)
      grdCorre.DataBind()

      grdCarre.DataSource = mdsDatos.Tables(mstrMateCarre)
      grdCarre.DataBind()

      Session(mstrTabla) = mdsDatos
   End Sub
#End Region

#Region "Detalle"
        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.MateId).Text)

            mCrearDataSet(hdnId.Text)

            grdEqui.CurrentPageIndex = 0

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)
                    txtDesc.Valor = .Item("mate_desc")
                    txtAbre.Valor = .Item("mate_abre")
                    Chkintensivo.Checked = .Item("mate_curs_inte")
                    ChkPresencia.Checked = .Item("mate_pres")       'AG 24/08/2011
                    chkCalifica.Checked = .Item("mate_cali")
                    txtHsTeo.Valor = .Item("mate_teoria_hs")
                    txtHsPrac.Valor = .Item("mate_prac_hs")
                    cmbEsta.Valor = IIf(.Item("mate_esta_id").ToString.Length > 0, .Item("mate_esta_id"), String.Empty)
                    cmbPeri.Valor = IIf(.Item("mate_peti_id").ToString.Length > 0, .Item("mate_peti_id"), String.Empty)
                    If Not .IsNull("mate_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("mate_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                        btnList.Visible = False
                    Else
                        lblBaja.Text = ""
                        btnList.Visible = True
                    End If
                End With

                mCargarCombosDetalles()
                mSetearEditor("", False)
                mSetearEditor(mstrMateEqui, True)
                mSetearEditor(mstrMateCorre, True)
                mSetearEditor(mstrMateCarre, True)
                mMostrarPanel(True)
                mShowTabs(1)

            End If
        End Sub
        Public Sub mEditarDatosEqui(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try

                Dim ldrEqui As DataRow

                hdnEquiId.Text = E.Item.Cells(1).Text
                ldrEqui = mdsDatos.Tables(mstrMateEqui).Select("maeq_id=" & hdnEquiId.Text)(0)
                Dim sFiltro As String = "@mate_inse_id = " + mintinsti.ToString() + ",@mate_id = " + hdnEquiId.Text
                With ldrEqui
                    cmbCarreEqui.Valor = .Item("maeq_carr_id")
                    sFiltro = sFiltro + ",@carre_id = " + cmbCarreEqui.Valor
                    clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMateEqui, "S", sFiltro)
                    cmbMateEqui.Valor = .Item("maeq_equi_mate_id")
                    'ver null
                    txtFechaDesdeEqui.Fecha = IIf(.Item("maeq_desde_fecha").ToString.Length > 0, .Item("maeq_desde_fecha"), String.Empty)
                    txtFechaHastaEqui.Fecha = IIf(.Item("maeq_hasta_fecha").ToString.Length > 0, .Item("maeq_hasta_fecha"), String.Empty)
                    'marca el que se va a modificar
                    '.Item("_modif") = "S"
                    '
                    If Not .IsNull("maeq_baja_fecha") Then
                        lblBajaEquiva.Text = "Registro dado de baja en fecha: " & CDate(.Item("maeq_baja_fecha")).ToString("dd/MM/yyyy HH:mm")

                    Else
                        lblBajaEquiva.Text = ""

                    End If
                End With

                mSetearEditor(mstrMateEqui, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
        Public Sub mEditarDatosCorre(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Try

                Dim ldrCorre As DataRow

                hdnCorreId.Text = E.Item.Cells(1).Text
                ldrCorre = mdsDatos.Tables(mstrMateCorre).Select("maco_id=" & hdnCorreId.Text)(0)

                Dim sFiltro As String = "@mate_inse_id = " + mintinsti.ToString() + ",@mate_id = " + hdnEquiId.Text
                With ldrCorre
                    cmbCarre.Valor = .Item("maco_carr_id")
                    sFiltro = sFiltro + ",@carre_id = " + cmbCarre.Valor
                    clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMate, "S", sFiltro)
                    cmbMate.Valor = .Item("maco_corr_mate_id")
                    'ver null
                    txtFechaDesde.Fecha = IIf(.Item("maco_desde_fecha").ToString.Length > 0, .Item("maco_desde_fecha"), String.Empty)
                    txtFechaHasta.Fecha = IIf(.Item("maco_hasta_fecha").ToString.Length > 0, .Item("maco_hasta_fecha"), String.Empty)
                    If Not .IsNull("maco_baja_fecha") Then
                        lblBajaCorre.Text = "Registro dado de baja en fecha: " & CDate(.Item("maco_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBajaCorre.Text = ""

                    End If
                End With

                mSetearEditor(mstrMateCorre, False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
   Public Sub mEditarDatosCarre(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrCarre As DataRow

         hdnCarreId.Text = E.Item.Cells(1).Text
         ldrCarre = mdsDatos.Tables(mstrMateCarre).Select("cama_id=" & hdnCarreId.Text)(0)

         With ldrCarre
            cmbCarreCarre.Valor = .Item("cama_carr_id")
            cmbPeriCarre.Valor = .Item("cama_peti_id")
            txtPerioCarre.Valor = .Item("cama_perio")
            chkObligatorio.Checked = clsWeb.gFormatCheck(.Item("cama_obli"), "True")

            If Not .IsNull("cama_baja_fecha") Then
               lblBajaCarre.Text = "Registro dado de baja en fecha: " & CDate(.Item("cama_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaCarre.Text = ""
            End If
         End With
         mSetearEditor(mstrMateCarre, False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar(ByVal grdDato As DataGrid, ByVal ejecuta As Boolean)
      Try
         mstrCmd = "exec " + mstrTabla + "_consul " + "@mate_inse_id = " + mintinsti.ToString()
         mstrCmd = mstrCmd + ",@formato = 'A'"
         mstrCmd = mstrCmd + ",@descFilt = '" + txtDescFil.Valor.ToString + "'"
         mstrCmd = mstrCmd + ",@abreFilt = '" + txtAbreFil.Valor.ToString + "'"
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarEqui()
      Try
         mGuardarDatosEqui()

         mLimpiarEqui()
         grdEqui.DataSource = mdsDatos.Tables(mstrMateEqui)
         grdEqui.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mActualizarCarre()
      Try
         mGuardarDatosCarre()

         mLimpiarCarre()
         grdCarre.DataSource = mdsDatos.Tables(mstrMateCarre)
         grdCarre.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mActualizarCorre()
      Try
         mGuardarDatosCorre()

         mLimpiarCorre()
         grdCorre.DataSource = mdsDatos.Tables(mstrMateCorre)
         grdCorre.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosEqui()

      Dim ldrDatos As DataRow

      If cmbCarreEqui.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la carrera.")
      End If
      If cmbMateEqui.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia.")
      End If

      If Not (txtFechaDesdeEqui.Fecha Is DBNull.Value) And Not (txtFechaHastaEqui.Fecha Is DBNull.Value) Then
         If txtFechaDesdeEqui.Fecha > txtFechaHastaEqui.Fecha Then
            Throw New AccesoBD.clsErrNeg("La fecha Desde no puede ser mayor a la fecha Hasta.")
         End If
      End If

      If hdnEquiId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrMateEqui).NewRow
         ldrDatos.Item("maeq_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrMateEqui), "maeq_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrMateEqui).Select("maeq_id=" & hdnEquiId.Text)(0)
      End If

      With ldrDatos
         If hdnEquiId.Text <> "" Then
            '.Item("maeq_mate_id") = hdnId.Text
         End If
         .Item("maeq_equi_mate_id") = cmbMateEqui.Valor
         .Item("maeq_carr_id") = cmbCarreEqui.Valor
                .Item("maeq_desde_fecha") = IIf(txtFechaDesdeEqui.Fecha.ToString.Length = 0, DBNull.Value, txtFechaDesdeEqui.Fecha.ToString)

                .Item("maeq_hasta_fecha") = IIf(txtFechaHastaEqui.Fecha.ToString.Length = 0, DBNull.Value, txtFechaHastaEqui.Fecha.ToString)

         .Item("_equi_descComp") = cmbMateEqui.SelectedItem.Text
         .Item("_carr_desc") = cmbCarreEqui.SelectedItem.Text
         .Item("maeq_baja_fecha") = DBNull.Value
         If Not .IsNull("maeq_baja_fecha") Then
            lblBajaEquiva.Text = "Registro dado de baja en fecha: " & CDate(.Item("maeq_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBajaEquiva.Text = ""
         End If

         For i As Integer = 0 To .Table.Columns.Count - 1
            If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
               .Item(i) = DBNull.Value
            End If
         Next
      End With


      Dim lboolValida As Boolean = mEstaEnElDataSet(mdsDatos.Tables(mstrMateEqui), ldrDatos, "maeq_equi_mate_id", "maeq_carr_id", "maeq_id")
      If (lboolValida) Then
         Throw New AccesoBD.clsErrNeg("La materia ya contiene la equivalencia seleccionada.")
      Else
         If (hdnEquiId.Text = "") Then
            mdsDatos.Tables(mstrMateEqui).Rows.Add(ldrDatos)
         End If
      End If
   End Sub
   Private Sub mGuardarDatosCorre()

      Dim ldrCorre As DataRow
      Dim lboolValida As Boolean

      If cmbCarre.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la carrera.")
      End If
      If cmbMate.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la materia.")
      End If
      If Not (txtFechaDesde.Fecha Is DBNull.Value) And Not (txtFechaHasta.Fecha Is DBNull.Value) Then
         If txtFechaDesde.Fecha > txtFechaHasta.Fecha Then
            Throw New AccesoBD.clsErrNeg("La fecha Desde no puede ser mayor a la fecha Hasta.")
         End If
      End If

      If hdnCorreId.Text = "" Then
         ldrCorre = mdsDatos.Tables(mstrMateCorre).NewRow
         ldrCorre.Item("maco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrMateCorre), "maco_id")
      Else
         ldrCorre = mdsDatos.Tables(mstrMateCorre).Select("maco_id=" & hdnCorreId.Text)(0)
      End If

      With ldrCorre
         If hdnCorreId.Text <> "" Then
            '.Item("maeq_mate_id") = hdnId.Text
         End If
         .Item("maco_carr_id") = cmbCarre.Valor
                .Item("maco_corr_mate_id") = cmbMate.Valor
                .Item("maco_desde_fecha") = IIf(txtFechaDesde.Fecha.ToString.Length = 0, DBNull.Value, txtFechaDesde.Fecha.ToString)

                .Item("maco_hasta_fecha") = IIf(txtFechaHasta.Fecha.ToString.Length = 0, DBNull.Value, txtFechaHasta.Fecha.ToString)


         .Item("_corr_descComp") = cmbMate.SelectedItem.Text
         .Item("_carr_desc") = cmbCarre.SelectedItem.Text
         .Item("maco_baja_fecha") = DBNull.Value
         If Not .IsNull("maco_baja_fecha") Then
            lblBajaCorre.Text = "Registro dado de baja en fecha: " & CDate(.Item("maco_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBajaCorre.Text = ""
         End If

         For i As Integer = 0 To .Table.Columns.Count - 1
            If .Table.Columns(i).ColumnName.EndsWith("_baja_fecha") Then
               .Item(i) = DBNull.Value
            End If
         Next
      End With



      lboolValida = mEstaEnElDataSet(mdsDatos.Tables(mstrMateCorre), ldrCorre, "maco_corr_mate_id", "maco_carr_id", "maco_id")


      If (lboolValida) Then
         Throw New AccesoBD.clsErrNeg("La materia ya tiene la correlativa seleccionada.")
      Else
         If (hdnCorreId.Text = "") Then
            mdsDatos.Tables(mstrMateCorre).Rows.Add(ldrCorre)
         End If
      End If
   End Sub
   Private Sub mGuardarDatosCarre()

      Dim ldrCarre As DataRow

      If cmbCarreCarre.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la carrera.")
      End If
      If cmbPeriCarre.SelectedItem.Text = "(Seleccione)" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar el tipo de periodo.")
      End If
      If txtPerioCarre.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el per�odo.")
      End If

      If hdnCarreId.Text = "" Then
         ldrCarre = mdsDatos.Tables(mstrMateCarre).NewRow
         ldrCarre.Item("cama_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrMateCarre), "cama_id")
      Else
         ldrCarre = mdsDatos.Tables(mstrMateCarre).Select("cama_id=" & hdnCarreId.Text)(0)
      End If

      With ldrCarre
         .Item("cama_carr_id") = cmbCarreCarre.Valor
         .Item("cama_peti_id") = cmbPeriCarre.Valor
         .Item("cama_perio") = txtPerioCarre.Valor
         .Item("cama_obli") = chkObligatorio.Checked
         .Item("_carr_desc") = cmbCarreCarre.SelectedItem.Text
         .Item("_peti_desc") = cmbPeriCarre.SelectedItem.Text
         .Item("cama_baja_fecha") = DBNull.Value
      End With
      Dim lboolValida As Boolean = mEstaEnElDataSet(mdsDatos.Tables(mstrMateCarre), ldrCarre, "cama_carr_id", "cama_id")

      If (lboolValida) Then
         Throw New AccesoBD.clsErrNeg("La materia ya pertenece a la carrera seleccionada.")
      Else
         If (hdnCarreId.Text = "") Then
            mdsDatos.Tables(mstrMateCarre).Rows.Add(ldrCarre)
         End If
      End If
   End Sub

   Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCorre As System.Data.DataRow, ByVal campo1 As String, ByVal campo2 As String, ByVal campoId As String) As Boolean
      Dim filtro As String
      filtro = campo1 + " = " + ldrCorre.Item(campo1).ToString() + " AND " + campo2 + " = " + ldrCorre.Item(campo2).ToString() + " AND " + campoId + " <> " + ldrCorre.Item(campoId).ToString()
      Return (table.Select(filtro).GetLength(0) > 0)
   End Function
   Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrCorre As System.Data.DataRow, ByVal campo1 As String, ByVal campoId As String) As Boolean
      Return mEstaEnElDataSet(table, ldrCorre, campo1, campo1, campoId)
   End Function
#End Region

#Region "Eventos de Controles"
   Public Sub grdCorre_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCorre.EditItemIndex = -1
         If (grdCorre.CurrentPageIndex < 0 Or grdCorre.CurrentPageIndex >= grdCorre.PageCount) Then
            grdCorre.CurrentPageIndex = 0
         Else
            grdCorre.CurrentPageIndex = E.NewPageIndex
         End If
         grdCorre.DataSource = mdsDatos.Tables(mstrMateCorre)
         grdCorre.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdEqui_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdEqui.EditItemIndex = -1
         If (grdEqui.CurrentPageIndex < 0 Or grdEqui.CurrentPageIndex >= grdEqui.PageCount) Then
            grdEqui.CurrentPageIndex = 0
         Else
            grdEqui.CurrentPageIndex = E.NewPageIndex
         End If
         grdEqui.DataSource = mdsDatos.Tables(mstrMateEqui)
         grdEqui.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub grdCarre_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCarre.EditItemIndex = -1
         If (grdCarre.CurrentPageIndex < 0 Or grdCarre.CurrentPageIndex >= grdCarre.PageCount) Then
            grdCarre.CurrentPageIndex = 0
         Else
            grdCarre.CurrentPageIndex = E.NewPageIndex
         End If
         grdCarre.DataSource = mdsDatos.Tables(mstrMateCarre)
         grdCarre.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnAltaEqui_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaEqui.Click
      mActualizarEqui()
   End Sub
   Private Sub btnBajaEqui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaEqui.Click
      Try

         'mdsDatos.Tables(mstrMateEqui).Select("maeq_id=" & hdnEquiId.Text)
         'With mdsDatos.Tables(mstrMateEqui).Rows(0)
         '   .Item("maeq_baja_fecha") = System.DateTime.Now.ToString
         '   .Item("_estado") = "Inactivo"
         'End With
         'grdEqui.DataSource = mdsDatos.Tables(mstrMateEqui)
         'grdEqui.DataBind()
         'mLimpiarEqui()

         mdsDatos.Tables(mstrMateEqui).Select("maeq_id=" & hdnEquiId.Text)(0).Delete()
         grdEqui.DataSource = mdsDatos.Tables(mstrMateEqui)
         grdEqui.DataBind()
         mLimpiarEqui()


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnLimpEqui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpEqui.Click
      mLimpiarEqui()

      mCargarComboConDataSet(cmbCarreEqui, mdsDatos.Tables(mstrMateCarre), "cama_carr_id", "_carr_desc")
      clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMateEqui, "S", mintinsti.ToString)

   End Sub
   Private Sub btnModiEqui_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiEqui.Click
      mActualizarEqui()
   End Sub

   Private Sub btnAltaCarre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaCarre.Click
      mActualizarCarre()
   End Sub
   Private Sub btnBajaCarre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaCarre.Click
      Try
         mdsDatos.Tables(mstrMateCarre).Select("cama_id=" & hdnCarreId.Text)(0).Delete()
         grdCarre.DataSource = mdsDatos.Tables(mstrMateCarre)
         grdCarre.DataBind()
         mLimpiarCarre()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnModiCarre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiCarre.Click
      mActualizarCarre()
   End Sub
   Private Sub btnLimpCarre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpCarre.Click
      mLimpiarCarre()
      clsWeb.gCargarRefeCmb(mstrConn, "carreras", cmbCarreCarre, "S", mintinsti.ToString)
      'If (hdnId.Text <> "") Then
      '   clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos_lectu", cmbPeriCarre, "S", hdnId.Text)
      'Else
      '   cmbPeriCarre.Items.Remove(cmbPeri.SelectedItem.Text)
      '   cmbPeriCarre.Items.Add(cmbPeri.SelectedItem.Text)
      'End If
   End Sub

   Private Sub btnAltaCorre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaCorre.Click
      mActualizarCorre()
   End Sub
   Private Sub btnBajaCorre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaCorre.Click
      Try
         mdsDatos.Tables(mstrMateCorre).Select("maco_id=" & hdnCorreId.Text)(0).Delete()
         grdCorre.DataSource = mdsDatos.Tables(mstrMateCorre)
         grdCorre.DataBind()
         mLimpiarCorre()


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnModiCorre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiCorre.Click
      mActualizarCorre()
   End Sub
   Private Sub btnLimpCorre_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpCorre.Click
      mLimpiarCorre()
      mCargarComboConDataSet(cmbCarre, mdsDatos.Tables(mstrMateCarre), "cama_carr_id", "_carr_desc")
      clsWeb.gCargarRefeCmb(mstrConn, "materias", cmbMate, "S", mintinsti.ToString)
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkCorre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCorre.Click
      mShowTabs(2)
   End Sub
   Private Sub lnkEqui_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkEqui.Click
      mShowTabs(3)
   End Sub
   Private Sub lnkCarre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCarre.Click
      mShowTabs(4)
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "materias"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&fkvalor=" + Request.QueryString("fkvalor")
         lstrRpt += "&mate_id=" + hdnId.Text

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      grdDato.CurrentPageIndex = 0
      mConsultar(grdDato, True)
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panEqui.Visible = False
      panCabecera.Visible = False
      panCorre.Visible = False
      panCarre.Visible = False

      lnkCabecera.Font.Bold = False
      lnkCorre.Font.Bold = False
      lnkEqui.Font.Bold = False
      lnkCarre.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Text = "Datos de la Materia "
         Case 2
            panCorre.Visible = True
            lnkCorre.Font.Bold = True
            lblTitu.Text = "Correlativas de la Materia: " & txtDesc.Text
         Case 3
            panEqui.Visible = True
            lnkEqui.Font.Bold = True
            lblTitu.Text = "Equivalentes de la Materia: " & txtDesc.Text
         Case 4
            panCarre.Visible = True
            lnkCarre.Font.Bold = True
            lblTitu.Text = "Carreras con la Materia: " & txtDesc.Text
      End Select
   End Sub
#End Region

End Class
End Namespace
