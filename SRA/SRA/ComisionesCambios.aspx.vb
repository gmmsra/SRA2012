Namespace SRA

Partial Class ComisionesCambios
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   Protected WithEvents lblMate As System.Web.UI.WebControls.Label
   Protected WithEvents lblCiclo As System.Web.UI.WebControls.Label
   Protected WithEvents Label2 As System.Web.UI.WebControls.Label


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ComisionesCambios

   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
   Private mintInse As Int32

   Private Enum Columnas As Integer
      Id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then

            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()

            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarDivisiones()
      clsWeb.gCargarRefeCmb(mstrConn, "divisiones", cmbOrigDivi, "S", "@cicl_carr_id =" & clsSQLServer.gFormatArg(cmbCarr.Valor.ToString, SqlDbType.Int))
      clsWeb.gCargarRefeCmb(mstrConn, "divisiones", cmbDestDivi, "S", "@cicl_carr_id =" & clsSQLServer.gFormatArg(cmbCarr.Valor.ToString, SqlDbType.Int))
      mCargarComisiones()
   End Sub

   Private Sub mCargarComisiones()
      clsWeb.gCargarRefeCmb(mstrConn, "comisionesXalumnos", cmbOrigComi, "T", "@divi_id =" & clsSQLServer.gFormatArg(cmbOrigDivi.Valor.ToString, SqlDbType.Int) & ",@alum_id=" & clsSQLServer.gFormatArg(usrAlum.Valor.ToString, SqlDbType.Int))
      clsWeb.gCargarRefeCmb(mstrConn, "comisiones", cmbDestComi, "T", "@divi_id =" & clsSQLServer.gFormatArg(cmbDestDivi.Valor.ToString, SqlDbType.Int))
   End Sub

   Private Sub mCargarDivisionesFil()
      clsWeb.gCargarRefeCmb(mstrConn, "divisiones", cmbDiviFil, "S", "@cicl_inse_id =" & mintInse)
   End Sub

   Private Sub mCargarCombos()
      mCargarDivisionesFil()
      clsWeb.gCargarRefeCmb(mstrConn, "carreras", cmbCarr, "S", "@carr_inse_id = " + mintInse.ToString())
      mCargarDivisiones()

      clsWeb.gCargarComboBool(cmbApro, "S")
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Incripciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub


#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
      usrAlum.FilInseId = mintInse
      usrAlumFil.FilInseId = mintInse
   End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul" + " @carr_inse_id = " + mintInse.ToString()
         mstrCmd += ", @alum_id  = " + usrAlumFil.Valor.ToString
         mstrCmd += ", @coca_orig_cicl_id = " + cmbDiviFil.Valor.ToString

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"


   Private Sub mLimpiarFiltros()
      hdnId.Text = ""
      cmbDiviFil.Limpiar()
      usrAlumFil.Limpiar()

      mConsultar()
      mMostrarPanel(False)
   End Sub

   Private Sub mSetearEditor(ByVal pbooAlta As Boolean, ByVal pbooApro As Boolean)
      btnBaja.Enabled = Not (pbooAlta) And Not pbooApro
      btnModi.Enabled = Not (pbooAlta) And Not pbooApro
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lbooApro As Boolean

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
         usrAlum.Valor = .Item("alum_id")
         cmbCarr.Valor = .Item("coca_carr_id")
         mCargarDivisiones()
         cmbOrigDivi.Valor = .Item("coca_orig_divi_id")
         cmbDestDivi.Valor = .Item("coca_dest_divi_id")
         mCargarComisiones()
         cmbOrigComi.Valor = .Item("coca_orig_comi_id")
         cmbDestComi.Valor = .Item("coca_dest_comi_id")
         txtFecha.Fecha = .Item("coca_fecha")
         cmbApro.ValorBool = .Item("coca_apro")

                ' lbooApro = Not (cmbApro.ValorBool Is DBNull.Value) AndAlso cmbApro.ValorBool
                lbooApro = Not IsDBNull(cmbApro.ValorBool) AndAlso cmbApro.ValorBool
            End With

      mSetearEditor(False, lbooApro)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      usrAlum.Limpiar()
      cmbCarr.Limpiar()
      mCargarDivisiones()
      cmbApro.Limpiar()

      txtFecha.Text = ""

      lblTitu.Text = ""

      mSetearEditor(True, False)
   End Sub

   Private Sub mCerrar()
      mConsultar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      grdDato.Visible = Not panDato.Visible
      btnAgre.Visible = Not (panDato.Visible)
      panFiltro.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
      End If

      panDato.Visible = pbooVisi

      panCabecera.Visible = True

   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)

         lobjGenerico.Alta()
         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)

         lobjGenerico.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
         .Item("coca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("coca_carr_id") = IIf(cmbCarr.Valor.ToString.Length > 0, cmbCarr.Valor, DBNull.Value)
                .Item("coca_orig_divi_id") = IIf(cmbOrigDivi.Valor.Length > 0, cmbOrigDivi.Valor, DBNull.Value)
                .Item("coca_dest_divi_id") = IIf(cmbDestDivi.Valor.Length > 0, cmbDestDivi.Valor, DBNull.Value)
                .Item("coca_orig_comi_id") = IIf(cmbOrigComi.Valor.Length > 0, cmbOrigComi.Valor, DBNull.Value)
                .Item("coca_dest_comi_id") = IIf(cmbDestComi.Valor.Length > 0, cmbDestComi.Valor, DBNull.Value)
                .Item("coca_fecha") = IIf(txtFecha.Fecha.Length > 0, txtFecha.Fecha, DBNull.Value)
                '.Item("coca_apro") = IIf(cmbApro.ValorBool.ToString.Length > 0, cmbApro.ValorBool, DBNull.Value)
                .Item("coca_apro") = IIf(cmbApro.Valor.ToString.Length > 0, cmbApro.Valor, DBNull.Value)
                .Item("alum_id") = IIf(usrAlum.Valor.ToString.Length > 0, usrAlum.Valor, DBNull.Value)
      End With

      Return ldsEsta
   End Function
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region
End Class
End Namespace
