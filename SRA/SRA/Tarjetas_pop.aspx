<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Tarjetas_pop" CodeFile="Tarjetas_pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title><%=mstrTitulo%></title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
  </HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR height="100%">
					<TD vAlign="top"><asp:datagrid id="grdTarjetas" runat="server" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
							ItemStyle-Height="5px" PageSize="15" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
							CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True" BorderStyle="None" width="100%">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="5%"></HeaderStyle>
									<ItemTemplate>
										<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit" Height="5">
											<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
										</asp:LinkButton>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD vAlign="middle" align="right" height="40"><asp:button id="btnCerrar" runat="server" Text="Cerrar" Width="80px" cssclass="boton"></asp:button>&nbsp;
					</TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</body>
</HTML>
