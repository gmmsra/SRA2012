<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.materias" CodeFile="materias.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Materias </title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO -------------------> &nbsp;
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px" height="33"></TD>
									<TD style="HEIGHT: 33px" vAlign="bottom" colSpan="2" height="33"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Materias</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
											Width="100%" Visible="True">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																		ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																		ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblDescFil" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtDescFil" runat="server" cssclass="cuadrotexto" Width="406px" AceptaNull="False"
																					Obligatorio="False" MaxLength="80"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 10%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblAbreFil" runat="server" cssclass="titulo">Abreviatura:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtAbreFil" runat="server" cssclass="cuadrotexto" Width="144px" AceptaNull="False"
																					Obligatorio="False"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR> <!-- Fin Filtro -->
								<TR>
									<TD style="HEIGHT: 5px" width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
											OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="2%"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" CausesValidation="false" Text="Editar" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="mate_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="_mate_descComp" ReadOnly="True" HeaderText="Materia">
													<HeaderStyle Width="60%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="mate_teoria_hs" ReadOnly="True" HeaderText="Horas Teor&#237;a"></asp:BoundColumn>
												<asp:BoundColumn DataField="mate_prac_hs" ReadOnly="True" HeaderText="Horas Pr&#225;ctica"></asp:BoundColumn>
												<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10"></td>
								</tr>
								<TR>
									<TD></TD>
									<TD vAlign="middle"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
											ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
											ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ToolTip="Agregar una Nueva Materia" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
									<TD align="right">
										<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<tr>
												<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
												<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
												<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Materia</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCarre" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" CausesValidation="False" Height="21px"> Carreras</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkEqui" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="80px" CausesValidation="False" Height="21px"> Equivalentes</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
												<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCorre" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" CausesValidation="False" Height="21px"> Correlativas</asp:linkbutton></td>
												<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
											</tr>
										</table>
									</TD>
								</TR>
								<TR>
									<TD><a name="editar"></a></TD>
									<TD align="center" colSpan="2">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="99%" BorderStyle="Solid" BorderWidth="1px"
												Visible="False" Height="116px">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" align="right" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripcion:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="400px" AceptaNull="False"
																				Obligatorio="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblAbre" runat="server" cssclass="titulo">Abreviatura:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:TEXTBOXTAB id="txtAbre" runat="server" cssclass="cuadrotexto" Width="144px" Obligatorio="True"
																				MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblHsTeo" runat="server" cssclass="titulo">Horas de teoria:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:NumberBox id="txtHsTeo" runat="server" cssclass="cuadrotexto" Width="72px" Obligatorio="True"></CC1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblHsPrac" runat="server" cssclass="titulo">Horas de practica:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<CC1:NumberBox id="txtHsPrac" runat="server" cssclass="cuadrotexto" Width="72px"></CC1:NumberBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 26px" align="right">
																			<asp:Label id="lblperi" runat="server" cssclass="titulo">Tipo de Per�odo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 26px">
																			<cc1:combobox class="combo" id="cmbPeri" runat="server" Width="260px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<cc1:combobox class="combo" id="cmbEsta" runat="server" Width="200px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">&nbsp;
																		</TD>
																		<TD style="HEIGHT: 32px" colSpan="2" height="32">
																			<asp:CheckBox id="Chkintensivo" CssClass="titulo" Runat="server" Checked="True" Text="Admite curso intensivo"></asp:CheckBox>&nbsp;</TD>
																	</TR>
																	<TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">&nbsp;
																		</TD>
																		<TD style="HEIGHT: 32px" colSpan="2" height="32">
																			<asp:CheckBox id="ChkPresencia" CssClass="titulo" Runat="server" Checked="True" Text="Presencial"></asp:CheckBox>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 32px" align="right">&nbsp;</TD>
																		<TD style="HEIGHT: 32px" colSpan="2" height="32">
																			<asp:CheckBox id="chkCalifica" CssClass="titulo" Runat="server" Checked="false" Text="Califica (requiere nota)"></asp:CheckBox>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" colSpan="3">
																			<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label>
																			<asp:imagebutton id="btnList" runat="server" ImageUrl="./imagenes/prints1.GIF" ToolTip="Imprimir"></asp:imagebutton></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCorre" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdCorre" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdCorre_PageChanged"
																				OnEditCommand="mEditarDatosCorre" AutoGenerateColumns="False">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="maco_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_carr_desc" HeaderText="Carrera">
																						<HeaderStyle Width="49%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_corr_descComp" ReadOnly="True" HeaderText="Materia">
																						<HeaderStyle Width="49%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="maco_corr_mate_id" HeaderText="maco_corr_mate_id"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="maco_carr_id" HeaderText="maco_carr_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 24px" align="right">
																			<asp:Label id="lblCarre" runat="server" cssclass="titulo">Carrera:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 24px">
																			<cc1:combobox class="combo" id="cmbCarre" runat="server" Width="500px" Obligatorio="True" onchange="mCargarMateriasCorre(this)"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 20px" align="right">
																			<asp:Label id="lblMate" runat="server" cssclass="titulo">Materia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 20px">
																			<cc1:combobox class="combo" id="cmbMate" runat="server" Width="500px" Obligatorio="True" NomOper="materias_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Fecha de inicio:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Fecha de fin:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<asp:Label id="lblBajaCorre" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaCorre" runat="server" cssclass="boton" Width="120px" Text="Agregar Correlativa"></asp:Button>&nbsp;&nbsp;
																			<asp:Button id="btnBajaCorre" runat="server" cssclass="boton" Width="119px" Text="Eliminar Correlativa"></asp:Button>&nbsp;
																			<asp:Button id="btnModiCorre" runat="server" cssclass="boton" Width="124px" Text="Modificar Correlativa"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpCorre" runat="server" cssclass="boton" Width="116px" Text="Limpiar Correlativa"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panEqui" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="Table4" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="left" colSpan="2">
																			<asp:datagrid id="grdEqui" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdEqui_PageChanged"
																				OnEditCommand="mEditarDatosEqui" AutoGenerateColumns="False">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="maeq_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_carr_desc" HeaderText="Carrera">
																						<HeaderStyle Width="49%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_equi_descComp" HeaderText="Materia">
																						<HeaderStyle Width="49%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="maeq_equi_mate_id" HeaderText="maeq_equi_mate_id"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="maeq_carr_id" HeaderText="maeq_carr_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 24px" align="right">
																			<asp:Label id="lblCarreEqui" runat="server" cssclass="titulo">Carrera:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 24px">
																			<cc1:combobox class="combo" id="cmbCarreEqui" runat="server" Width="500px" Obligatorio="True"
																				onchange="mCargarMateriasEqui(this)"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 20px" align="right">
																			<asp:Label id="lblMateEqui" runat="server" cssclass="titulo">Materia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 20px">
																			<cc1:combobox class="combo" id="cmbMateEqui" runat="server" Width="500px" Obligatorio="True" NomOper="materias_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaDesdeEqui" runat="server" cssclass="titulo">Fecha de inicio:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaDesdeEqui" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Label id="lblFechaHastaEqui" runat="server" cssclass="titulo">Fecha de fin:</asp:Label></TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaHastaEqui" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				Obligatorio="False"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<asp:Label id="lblBajaEquiva" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaEqui" runat="server" cssclass="boton" Width="130px" Text="Agregar Equivalencia"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaEqui" runat="server" cssclass="boton" Width="133px" Text="Eliminar Equivalencia"></asp:Button>&nbsp;
																			<asp:Button id="btnModiEqui" runat="server" cssclass="boton" Width="132px" Text="Modificar Equivalencia"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpEqui" runat="server" cssclass="boton" Width="127px" Text="Limpiar Equivalencia"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCarre" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdCarre" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdCarre_PageChanged"
																				OnEditCommand="mEditarDatosCarre" AutoGenerateColumns="False">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="cama_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="3%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_carr_desc" HeaderText="Carrera">
																						<HeaderStyle Width="72%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_peti_desc" HeaderText="Tipo de Per&#237;odo">
																						<HeaderStyle Width="15%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="cama_perio" HeaderText="Per&#237;odo"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="cama_carr_id" HeaderText="cama_carr_id"></asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="cama_peti_id" HeaderText="cama_peti_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_obli" HeaderText="Obligatoria">
																						<HeaderStyle Width="5%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 24px" align="right">
																			<asp:Label id="lblCarreCarre" runat="server" cssclass="titulo">Carrera:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 24px">
																			<cc1:combobox class="combo" id="cmbCarreCarre" runat="server" Width="500px" Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 26px" align="right">
																			<asp:Label id="lblPeriCarre" runat="server" cssclass="titulo">Tipo de Per�odo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 26px">
																			<cc1:combobox class="combo" id="cmbPeriCarre" runat="server" Width="260px" Obligatorio="True"
																				NomOper="periodo_tipos_lectu_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 16px" align="right">
																			<asp:Label id="lblPerioCarre" runat="server" cssclass="titulo">Per�odo:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 16px" colSpan="2" height="16">
																			<cc1:numberbox id="txtPerioCarre" runat="server" cssclass="cuadrotexto" Width="112px" Obligatorio="True"
																				MaxValor="9999999999999" esdecimal="False" Wrap="False"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 32px" align="right">&nbsp;</TD>
																		<TD style="HEIGHT: 32px" colSpan="2" height="32">
																			<asp:CheckBox id="chkObligatorio" CssClass="titulo" Runat="server" Text="Obligatoria"></asp:CheckBox>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2" height="5">
																			<asp:Label id="lblBajaCarre" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaCarre" runat="server" cssclass="boton" Width="103px" Text="Agregar Carrera"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaCarre" runat="server" cssclass="boton" Width="103px" Text="Eliminar Carrera"></asp:Button>&nbsp;
																			<asp:Button id="btnModiCarre" runat="server" cssclass="boton" Width="108px" Text="Modificar Carrera"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpCarre" runat="server" cssclass="boton" Width="100px" Text="Limpiar Carrera"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Visible="False" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD vAlign="middle" align="center" colSpan="3" height="30">
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnEquiId" runat="server"></asp:textbox><asp:textbox id="hdnCorreId" runat="server"></asp:textbox><asp:textbox id="hdnCarreId" runat="server"></asp:textbox><ASP:TEXTBOX id="lblInstituto" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		
		function mCargarMateriasEqui(pCmb)
		{
		   cargarComboConMaterias("cmbMateEqui",pCmb)
		}

		function mCargarMateriasCorre(pCmb)
		{		   
		   cargarComboConMaterias("cmbMate",pCmb)
		}
		
		function cargarComboConMaterias(pCmbMate,pCmbCarre)
		{		
		    var sFiltro = obtenerFiltro(pCmbCarre)			        
			LoadComboXML("materias_cargar", sFiltro, pCmbMate, "S");		    	
		}

		function obtenerFiltro(pCmb)
		{	
		    var maid = document.all["hdnId"].value;	
		    var carrera = pCmb.value;
		    var sFiltro = "@mate_inse_id = " + document.all["lblInstituto"].value + ",@mate_id = ";
		    if(maid == "")
			  {sFiltro = sFiltro + "null";}
			  else {sFiltro = sFiltro + maid;}	
			if(carrera == "")
			 {carrera = "-1"}  
			sFiltro = sFiltro + ",@carre_id = " + carrera
			return (sFiltro);						
		}
		
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();
		</SCRIPT>
		</TD></TR></TBODY></TABLE>
	</BODY>
</HTML>
