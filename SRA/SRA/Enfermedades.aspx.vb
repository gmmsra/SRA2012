Namespace SRA

Partial Class Enfermedades
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblRazas As System.Web.UI.WebControls.Label
    Protected WithEvents lblEnfermedad As System.Web.UI.WebControls.Label
    Protected WithEvents cmbEnfermedad As NixorControls.ComboBox
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
#Region "Definición de Variables"
    Private mstrTabla As String = "enfermedades"
    Private mstrTablaEnfeRaza As String = "enfermedades_razas"
    Private mstrTablaEnfeResultados As String = "enfermedades_resultados"
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mEstablecerPerfil()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

        txtCodigoFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "enfe_codi")
        txtDenominacionFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "enfe_deno")

        txtCodigo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "enfe_codi")
        txtDenominacion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "enfe_deno")

        txtDiasVigencia.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "enfe_dias_vigencia")


    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspecie, "S")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRaza, "id", "descrip", "S")

        clsWeb.gCargarRefeCmb(mstrConn, "RG_RESULTADOS_SANIDAD", cmbResultados, "S")
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridRaza_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdRaza.EditItemIndex = -1
            If (grdRaza.CurrentPageIndex < 0 Or grdRaza.CurrentPageIndex >= grdRaza.PageCount) Then
                grdRaza.CurrentPageIndex = 0
            Else
                grdRaza.CurrentPageIndex = E.NewPageIndex
            End If
            grdRaza.DataSource = mdsDatos.Tables(mstrTablaEnfeRaza)
            grdRaza.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridResultados_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdResultados.EditItemIndex = -1
            If (grdResultados.CurrentPageIndex < 0 Or grdResultados.CurrentPageIndex >= grdResultados.PageCount) Then
                grdResultados.CurrentPageIndex = 0
            Else
                grdResultados.CurrentPageIndex = E.NewPageIndex
            End If
            grdResultados.DataSource = mdsDatos.Tables(mstrTablaEnfeResultados)
            grdResultados.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul @formato='1'"
            mstrCmd = mstrCmd + ",@enfe_codi='" + txtCodigoFil.Valor.ToString + "'"
            mstrCmd = mstrCmd + ",@enfe_deno='" + txtDenominacionFil.Valor.ToString + "'"
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub

    Private Sub mSetearEditorEnfeRaza(ByVal pbooAlta As Boolean)
        btnBajaRazas.Enabled = Not (pbooAlta)
        btnModiRazas.Enabled = Not (pbooAlta)
        btnAltaRazas.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorEnfeResultados(ByVal pbooAlta As Boolean)
        btnBajaResultados.Enabled = Not (pbooAlta)
        btnModiResultados.Enabled = Not (pbooAlta)
        btnAltaResultados.Enabled = pbooAlta
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                txtCodigo.Valor = .Item("enfe_codi")
                txtCodigo.Enabled = False
                txtDenominacion.Valor = .Item("enfe_deno")
                txtDiasVigencia.Valor = .Item("enfe_dias_vigencia")
                txtEnfermedadResul.Valor = .Item("enfe_codi")

                txtEnfermedadResul.Enabled = False

                If Not .IsNull("enfe_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("enfe_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If
            End With
            mSetearEditor(False)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub
    Public Sub mEditarDatosEnfeRaza(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrEnfeRaza As DataRow

            hdnRazaId.Text = E.Item.Cells(1).Text
            ldrEnfeRaza = mdsDatos.Tables(mstrTablaEnfeRaza).Select("enra_id=" & hdnRazaId.Text)(0)

            With ldrEnfeRaza
                cmbEspecie.Valor = .Item("enra_espe_id")
                cmbRaza.Valor = .Item("enra_raza_id")
                If Not .IsNull("enra_baja_fecha") Then
                    lblBajaRaza.Text = "Dado de baja en fecha: " & CDate(.Item("enra_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaRaza.Text = ""
                End If
            End With
            mSetearEditorEnfeRaza(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mEditarDatosEnfeResultados(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrEnfeResultados As DataRow

            hdnResultadoId.Text = E.Item.Cells(1).Text
            ldrEnfeResultados = mdsDatos.Tables(mstrTablaEnfeResultados).Select("enre_id=" & hdnResultadoId.Text)(0)

            With ldrEnfeResultados

                cmbResultados.Valor = .Item("enre_enfe_id")
                If Not .IsNull("enre_baja_fecha") Then
                    lblBajaResultados.Text = "Dado de baja en fecha: " & CDate(.Item("enre_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaResultados.Text = ""
                End If
            End With
            mSetearEditorEnfeResultados(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaEnfeRaza
        mdsDatos.Tables(2).TableName = mstrTablaEnfeResultados

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        grdRaza.DataSource = mdsDatos.Tables(mstrTablaEnfeRaza)
        grdRaza.DataBind()

        grdResultados.DataSource = mdsDatos.Tables(mstrTablaEnfeResultados)
        grdResultados.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        txtCodigo.Text = ""
        txtCodigo.Enabled = True
        txtDenominacion.Text = ""

        txtDiasVigencia.Text = ""

        lblBaja.Text = ""

        mLimpiarEnfeRaza()

        grdDato.CurrentPageIndex = 0
        grdRaza.CurrentPageIndex = 0


        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarFil()
        txtCodigoFil.Text = ""
        txtDenominacionFil.Text = ""
        mConsultar()
    End Sub
    Private Sub mLimpiarEnfeRaza()
        hdnRazaId.Text = ""
        cmbEspecie.Limpiar()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRaza, "id", "descrip", "S")
        mSetearEditorEnfeRaza(True)
    End Sub

    Private Sub mLimpiarEnfeResultados()
        hdnResultadoId.Text = ""
        cmbResultados.Limpiar()
        'clsWeb.gCargarCombo(mstrConn, "rg_resultados_sanidad_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRaza, "id", "descrip", "S")
        mSetearEditorEnfeResultados(True)
    End Sub



    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        mEstablecerPerfil()
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
            lobjGenerica.Alta()
            'Dim intId As Integer = lobjGenerica.Alta()
            'Dim mdsDs As New DataSet

            'mdsDs = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, intId)

            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
            lobjGenerica.Modi()

           
            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidaDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("enfe_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("enfe_codi") = txtCodigo.Valor
            .Item("enfe_deno") = txtDenominacion.Valor
            .Item("enfe_dias_vigencia") = txtDiasVigencia.Valor
            .Item("enfe_baja_fecha") = DBNull.Value
            .Item("enfe_audi_user") = Session("sUserId").ToString()
        End With
    End Sub


    'Seccion de los Asociacion Resultados
    Private Sub mActualizarEnfeResultados()
        Try
            mGuardarDatosEnfeResultados()

            mLimpiarEnfeResultados()
            grdResultados.DataSource = mdsDatos.Tables(mstrTablaEnfeResultados)
            grdResultados.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaEnfeResultados()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaEnfeResultados).Select("enre_id=" & hdnResultadoId.Text)(0)
            row.Delete()
            grdResultados.DataSource = mdsDatos.Tables(mstrTablaEnfeResultados)
            grdResultados.DataBind()
            mLimpiarEnfeResultados()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    'Seccion de los Asociacion Raza
    Private Sub mActualizarEnfeRaza()
        Try
            mGuardarDatosEnfeRaza()

            mLimpiarEnfeRaza()
            grdRaza.DataSource = mdsDatos.Tables(mstrTablaEnfeRaza)
            grdRaza.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaEnfeRaza()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaEnfeRaza).Select("enra_id=" & hdnRazaId.Text)(0)
            row.Delete()
            grdRaza.DataSource = mdsDatos.Tables(mstrTablaEnfeRaza)
            grdRaza.DataBind()
            mLimpiarEnfeRaza()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosEnfeRaza()
        Dim ldrEnfeRaza As DataRow

        If cmbRaza.Valor.ToString = "" And cmbEspecie.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la especie o raza.")
        End If

        If hdnRazaId.Text = "" Then
            ldrEnfeRaza = mdsDatos.Tables(mstrTablaEnfeRaza).NewRow
            ldrEnfeRaza.Item("enra_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaEnfeRaza), "enra_id")
        Else
            ldrEnfeRaza = mdsDatos.Tables(mstrTablaEnfeRaza).Select("enra_id=" & hdnRazaId.Text)(0)
        End If

        With ldrEnfeRaza
            .Item("enra_raza_id") = cmbRaza.Valor
            .Item("enra_espe_id") = cmbEspecie.Valor
            .Item("_espe_desc") = IIf(cmbEspecie.Valor.ToString = "", "", cmbEspecie.SelectedItem.Text)
            .Item("_razas_desc") = IIf(cmbRaza.Valor.ToString = "", "", cmbRaza.SelectedItem.Text)
            .Item("enra_enfe_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("enra_audi_user") = Session("sUserId").ToString()
            .Item("enra_baja_fecha") = DBNull.Value
        End With
        If (hdnRazaId.Text = "") Then
            mdsDatos.Tables(mstrTablaEnfeRaza).Rows.Add(ldrEnfeRaza)
        End If
    End Sub

    Private Sub mGuardarDatosEnfeResultados()
        Dim ldrEnfeResultados As DataRow

        If cmbResultados.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un resultado.")
        End If


        Dim oEnfermedades As New SRA_Neg.Enfermedades(mstrConn, Session("sUserId").ToString())

        If hdnResultadoId.Text = "" Then
            ldrEnfeResultados = mdsDatos.Tables(mstrTablaEnfeResultados).NewRow
            ldrEnfeResultados.Item("enre_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaEnfeResultados), "enre_id")
        Else
            ldrEnfeResultados = mdsDatos.Tables(mstrTablaEnfeResultados).Select("enre_id=" & hdnResultadoId.Text)(0)
        End If

        With ldrEnfeResultados
            .Item("_resa_deno") = cmbResultados.SelectedItem.Text
            .Item("_enfe_deno") = oEnfermedades.GetEnfermedadDescripcionById(hdnId.Text)
            .Item("enre_resa_id") = cmbResultados.Valor
            .Item("enre_enfe_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("enre_audi_user") = Session("sUserId").ToString()
            .Item("enre_baja_fecha") = DBNull.Value
        End With
        If (hdnResultadoId.Text = "") Then
            mdsDatos.Tables(mstrTablaEnfeResultados).Rows.Add(ldrEnfeResultados)
        End If
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            panRaza.Visible = False
            lnkRazas.Font.Bold = False
            panResultados.Visible = False
            lnkResultados.Font.Bold = False
            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 5
                    panRaza.Visible = True
                    lnkRazas.Font.Bold = True
                Case 6
                    panResultados.Visible = True
                    lnkResultados.Font.Bold = True
            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub
    Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
        Try
            Dim lstrRptName As String

            lstrRptName = "Enfermedades"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            If txtCodigoFil.Text <> "" Then
                lstrRpt += "&enfe_codi=" + txtCodigoFil.Valor.ToString
            End If
            If txtDenominacionFil.Text <> "" Then
                lstrRpt += "&enfe_deno=" + txtDenominacionFil.Valor.ToString
            End If
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    'Botones generales
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    'Botones de asociaciones raza
    Private Sub btnAltaRazas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaRazas.Click
        mActualizarEnfeRaza()
    End Sub
    Private Sub btnBajaRazas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaRazas.Click
        mBajaEnfeRaza()
    End Sub
    Private Sub btnModiRazas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiRazas.Click
        mActualizarEnfeRaza()
    End Sub
    Private Sub btnLimpRazas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpRazas.Click
        mLimpiarEnfeRaza()
    End Sub
    'Solapas
    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkRazas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkRazas.Click
        mShowTabs(5)
    End Sub
    Private Sub lnkResultados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkResultados.Click
        mShowTabs(6)
    End Sub
#End Region

    Private Sub grdResultados_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles grdResultados.SelectedIndexChanged

    End Sub

    Public Sub grdResultados_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdResultados.PageIndexChanged

        Try
            grdResultados.EditItemIndex = -1
            If (grdResultados.CurrentPageIndex < 0 Or grdResultados.CurrentPageIndex >= grdResultados.PageCount) Then
                grdResultados.CurrentPageIndex = 0
            Else
                grdResultados.CurrentPageIndex = e.NewPageIndex
            End If
            grdResultados.DataSource = mdsDatos.Tables(mstrTablaEnfeResultados)
            grdResultados.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Private Sub btnAltaResultados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaResultados.Click
        mActualizarEnfeResultados()
    End Sub

    Private Sub btnBajaResultados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaResultados.Click
        mBajaEnfeResultados()
    End Sub

    Private Sub btnModiResultados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiResultados.Click
        mActualizarEnfeResultados()
    End Sub

    Private Sub btnLimpResultados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpResultados.Click
        mLimpiarEnfeResultados()
    End Sub

   
End Class

End Namespace
