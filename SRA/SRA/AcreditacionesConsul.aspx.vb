Namespace SRA

Partial Class AcreditacionesConsul
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ComprobPagos
   Private mdsDatos As New DataSet
   Private mstrConn As String
   Private mstrParaPageSize As Integer

   Private Enum Columnas As Integer
      Id = 1
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      btnReci.Attributes.Add("onclick", "javascript:ImprimirSeleccion();return(false);")
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub mGuardarChechk()
      Dim dr As DataRow
      'Mantengo los check de las paguinas de la grilla
      For Each oDataItem As DataGridItem In grdDato.Items
            If mdsDatos.Tables(0).Select("comp_id = " & oDataItem.Cells(14).Text).Length <> 0 Then
                dr = mdsDatos.Tables(0).Select("comp_id = " & oDataItem.Cells(14).Text)(0)
                dr.Item("chk") = DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked
                'dr.Item("chk1") = DirectCast(oDataItem.FindControl("chkOrig"), CheckBox).Checked
                dr.Item("original") = DirectCast(oDataItem.FindControl("chkOriginal"), CheckBox).Checked
            End If
        Next
      Session(mstrTabla) = mdsDatos
   End Sub
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            hdnEmctId.Text = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
            mCargarCombos()
            mSetearMaxLength()
            Session(mSess(mstrTabla)) = Nothing

            Dim mobj As SRA_Neg.Facturacion
            mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
            mobj.CentroEmisorNro(mstrConn)

            cmbCentroEmisor.Valor = mobj.pCentroEmisorId
            If Not clsSQLServer.gObtenerValorCampo(mstrConn, "EMISORES_CTROS", cmbCentroEmisor.Valor.ToString, "emct_central") Then
               cmbCentroEmisor.Enabled = False
            End If

                '10/03/2011 mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mSess(mstrTabla))
                If Not mdsDatos Is Nothing Then
                    mGuardarChechk()
                    mObtenerIDs()
                End If

         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function

   Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancFil, "T", "@con_cuentas=1")
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbCentroEmisor, "")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBanc, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancOrig, "S")
        Me.CargarComboEmpresasCobradorasElectronicas()
   End Sub

    ' Dario 2013-06-18 metodo que se utiliza para la carge del combo de empresas cobradoras electronicas    
    Private Sub CargarComboEmpresasCobradorasElectronicas()
        Try
            ' coloco el valor por defecto del combo
            cmbEmpCobElecFil.Items.Add(New System.Web.UI.WebControls.ListItem("(Todos)", "-1"))
            ' instancio el objeto cobranzas de negocio
            Dim _CobranzasBusiness As New Business.Facturacion.CobranzasBusiness
            ' ejecuto el metodo que recupera los datos a mostar en la grilla
            Dim ds As DataSet = _CobranzasBusiness.GetEmpresasCobranzasElectronicas(Me.User.Identity.Name)
            ' controlo que tenga algo la consulta de empresas
            If (Not ds Is Nothing And ds.Tables.Count > 0) Then
                ' si tiene 0 o mas de uno agrega el seleccione sino deja solo el unico que tiene
                If (ds.Tables(0).Rows.Count > 0) Then
                    cmbEmpCobElecFil.Items.Add(New System.Web.UI.WebControls.ListItem("(Solo Acrd. Bacrarias)", "0"))
                End If
                ' recorre el resultado de la consulta y lo carga en el combo
                For Each dr As DataRow In ds.Tables(0).Rows
                    ' agrego el item 
                    cmbEmpCobElecFil.Items.Add( _
                                                New System.Web.UI.WebControls.ListItem(dr("varDesEmpresaCobElect").ToString() _
                                                                                          , dr("intIdEmprezasCobranzasElectronicas").ToString()))
                Next
            End If
            ds.Dispose()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        Dim lintCol As Integer

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtNumeFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "paco_nume")
    End Sub
#End Region

#Region "Seteo de Controles"
   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim lbooRech As Boolean
      Dim lstrCompId As String
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)

      With ldsEsta.Tables(0).Rows(0)
         hdnId.Text = .Item("paco_id").ToString()
         txtImpo.Valor = .Item("paco_impo")
         cmbMone.Valor = .Item("paco_mone_id")
         cmbBanc.Valor = .Item("_cuba_banc_id")
         clsWeb.gCargarRefeCmb(mstrConn, "cuentas_bancos", cmbCuba, "", cmbBanc.Valor)
         cmbCuba.Valor = .Item("paco_cuba_id")
         txtNume.Valor = .Item("paco_nume")
         usrClie.Valor = .Item("_comp_clie_id")
         txtFecha.Fecha = .Item("_comp_fecha")
                txtDepoFecha.Fecha = IIf(.Item("paco_acre_depo_fecha") Is DBNull.Value, "", .Item("paco_acre_depo_fecha"))
                txtIngrFecha.Fecha = .Item("_comp_ingr_fecha")
         cmbBancOrig.Valor = .Item("paco_orig_banc_id")
                txtSucu.Valor = IIf(.Item("paco_orig_banc_suc") Is DBNull.Value, "", .Item("paco_orig_banc_suc"))
                txtMoti.Valor = IIf(.Item("_paco_baja_moti") Is DBNull.Value, "", .Item("_paco_baja_moti"))
                txtMoti.Visible = (Not .IsNull("_paco_baja_fecha"))
         lblMoti.Visible = (Not .IsNull("_paco_baja_fecha"))
         txtClie.Valor = .Item("_code_reci_nyap")

         txtClie.Visible = (txtClie.Text <> "")
         usrClie.Visible = (txtClie.Text = "")

         lstrCompId = .Item("paco_comp_id")

         If Not .IsNull("_paco_baja_fecha") Then
            lblBaja.Text = "Acredicatión bancaria anulada en fecha: " & CDate(.Item("_paco_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            lbooRech = True
         Else
            If Not .IsNull("paco_acre_fecha") Then
                lblBaja.Text = "Acredicatión bancaria confirmada en fecha: " & CDate(.Item("paco_acre_fecha")).ToString("dd/MM/yyyy HH:mm")
                lbooRech = False
            Else
                lblBaja.Text = ""
            End If
         End If
      End With

      mConsultarDeta(lstrCompId)

      mMostrarPanel(True)
   End Sub

   Private Sub mConsultarDeta(ByVal pstrCompId As String)
      Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, "comprobantes_anula", pstrCompId)

      'uno las descripcioneas para el desde/hasta de las cuotas sociales
      For Each lDr As DataRow In ldsDatos.Tables(0).Rows
         With lDr
            If Not .IsNull("descrip2") AndAlso .Item("descrip").ToString.IndexOf(.Item("descrip2").ToString) = -1 Then
               .Item("descrip") = .Item("descrip").ToString + "-" + .Item("descrip2").ToString
            End If
         End With
      Next

      grdAplic.DataSource = ldsDatos.Tables(0)
      grdAplic.DataBind()

      grdPagos.DataSource = ldsDatos.Tables(1)
      grdPagos.DataBind()
   End Sub

   Private Function CrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      ldsEsta.Tables(0).TableName = mstrTabla

      grdAplic.CurrentPageIndex = 0
      grdPagos.CurrentPageIndex = 0

      Return ldsEsta
   End Function

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""

      txtImpo.Text = ""
      cmbBanc.Limpiar()
      cmbBancOrig.Limpiar()
      txtSucu.Text = ""
      txtNume.Text = ""
      txtFecha.Text = ""
      txtIngrFecha.Text = ""
      cmbMone.Limpiar()
      usrClie.Limpiar()
      txtClie.Text = ""

   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible
      btnReci.Visible = Not panDato.Visible
      btnList.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
      End If
   End Sub

   Private Sub mLimpiarFiltros()
      txtFechaDesdeFil.Text = ""
      txtFechaHastaFil.Text = ""
      cmbBancFil.Limpiar()
      cmbCubaFil.Items.Clear()

      Dim mobj As SRA_Neg.Facturacion
      mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
      mobj.CentroEmisorNro(mstrConn)
      cmbCentroEmisor.Valor = mobj.pCentroEmisorId

      txtNumeFil.Text = ""
      cmbEstaFil.Limpiar()
      usrClieFil.Limpiar()
      mConsultar()
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec comprob_pagos_busq")
			lstrCmd.Append(" @paco_acre_depo_fecha_desde=" & clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha))
			lstrCmd.Append(",@paco_acre_depo_fecha_hasta=" & clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha))
            lstrCmd.Append(",@cuba_banc_id =" + cmbBancFil.Valor.ToString)
            lstrCmd.Append(",@paco_cuba_id =" + cmbCubaFil.Valor.ToString)
            lstrCmd.Append(",@paco_nume =" + clsSQLServer.gFormatArg(txtNumeFil.Valor.ToString, SqlDbType.VarChar))
            lstrCmd.Append(",@paco_pati_id =" + CType(SRA_Neg.Constantes.PagosTipos.AcredBancaria, String))
            lstrCmd.Append(",@clie_id =" + usrClieFil.Valor.ToString)
            lstrCmd.Append(",@estado=" + cmbEstaFil.Valor.ToString)
            lstrCmd.Append(",@comp_emct_id =" + cmbCentroEmisor.Valor.ToString)
            ' Dario 2013-06-18 se agragan dos nuevos filtros
            lstrCmd.Append(",@IntIdEmpCobElec =" + cmbEmpCobElecFil.Valor.ToString)
            lstrCmd.Append(",@varNroCompCobElect =" + clsSQLServer.gFormatArg(txtNroCompCobElectFil.Valor.ToString, SqlDbType.VarChar))

            mdsDatos = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)
            Session(mSess(mstrTabla)) = mdsDatos

            If mdsDatos.Tables(0).Rows.Count = 0 Then
                grdDato.CurrentPageIndex = 0
            End If

            grdDato.DataSource = mdsDatos.Tables(0)
            grdDato.DataBind()
            hdnIds.Text = ""
            hdnIdsImpr.Text = ""

            mMostrarPanel(False)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub hdnImprSele_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprSele.TextChanged
       mGuardarChechk()
       mObtenerIDs()
   End Sub
   Private Function mObtenerIDs() As String
      Dim lstrIds As String = ""
      Dim lstrIdsImpr As String = ""
      For Each ldrData As DataRow In mdsDatos.Tables(0).Select("chk=1")
         'comprobantes seleccionados
         If lstrIds <> "" Then
            lstrIds = lstrIds & ";"
         End If
         lstrIds = lstrIds & ldrData.Item("comp_id") & ","
            lstrIds = lstrIds & ldrData.Item("original") & ","
            'lstrIds = lstrIds & ldrData.Item("chk1") & ","
            lstrIds = lstrIds & ldrData.Item("anulado")
         'comprobantes seleccionados no impresos
         'If ldrData.Item("comp_impre") <> 1 Then
            If lstrIdsImpr <> "" Then
                lstrIdsImpr = lstrIdsImpr & ","
            End If
            lstrIdsImpr = lstrIdsImpr & ldrData.Item("comp_id")
         'End If
      Next
      hdnIds.Text = lstrIds
      hdnIdsImpr.Text = lstrIdsImpr
   End Function
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         mGuardarChechk()
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         grdDato.DataSource = mdsDatos
         grdDato.DataBind()
         'mConsultar()
         mObtenerIDs()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(Columnas.Id).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos"
   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub
#End Region

Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnList.Click
      Try
         mListar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
End Sub

   Private Sub mListar()
      Try
         Dim params As String
         Dim lstrRptName As String
         Dim lstrRpt As String

         Dim lintFechaD As Integer, lintFechaH As Integer

         If txtFechaDesdeFil.Text = "" Then
            lintFechaD = 0
         Else
            lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
         End If
         If txtFechaHastaFil.Text = "" Then
            lintFechaH = 0
         Else
            lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
         End If

            params += "&fecha=" & lintFechaD.ToString
            params += "&FechaHasta=" & lintFechaH.ToString
            params += "&paco_nume=" & IIf(txtNumeFil.Valor.ToString = "", "0", txtNumeFil.Valor.ToString)
            params += "&paco_cuba_id=" & cmbCubaFil.Valor.ToString
            params += "&paco_pati_id=" & SRA_Neg.Constantes.PagosTipos.AcredBancaria
            params += "&cuba_banc_id=" & cmbBancFil.Valor.ToString
            params += "&clie_id=" & usrClieFil.Valor.ToString
            params += "&comp_emct_id=" & cmbCentroEmisor.Valor.ToString
            params += "&estado=" & cmbEstaFil.Valor.ToString
            params += "&paco_tarj_id=0"
            params += "&rechazado=False"
            params += "&aprobada=False"
            ' Dario 2013-06-18 se agragan dos nuevos filtros
            params += "&IntIdEmpCobElec=" & cmbEmpCobElecFil.Valor.ToString
            params += "&varNroCompCobElect=" & txtNroCompCobElectFil.Valor.ToString

            lstrRptName = "EstadosAcreditaciones"
            lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            lstrRpt += params
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub

    Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged
        Try
            Dim lstrId As String
            Dim CompIds As String = hdnImprimio.Text
            Dim lobj As New SRA_Neg.Cobranza(mstrConn, Session("sUserId").ToString(), SRA_Neg.Constantes.gTab_Comprobantes)

            While CompIds <> ""
                lstrId = Left(CompIds, IIf(InStr(CompIds, ",") = 0, CompIds.Length, InStr(CompIds, ",") - 1))
                CompIds = Right(CompIds, IIf(InStr(CompIds, ",") = 0, 0, CompIds.Length - InStr(CompIds, ",")))
                lobj.ModiImpreso(lstrId, True)
            End While

            hdnImprimio.Text = ""
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

End Class
End Namespace
