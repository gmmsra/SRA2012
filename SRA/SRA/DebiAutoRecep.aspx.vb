Namespace SRA

Partial Class DebiAutoRecep
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_DebitosCabe
   Private mstrDebitosDeta As String = SRA_Neg.Constantes.gTab_DebitosDeta

   Private mstrConn As String
   Private mstrEtapa As String
   Public mstrTitulo As String
   Private mstrParaPageSize As Integer

   Private mdsDatos As DataSet

   Private Enum ColumnasDeta As Integer
      Id = 0
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      grdDeta.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

                mSetearMaxLength()
            mSetearEventos()

            mCargarCombos()

                    txtGeneFecha.Fecha = Today
                    txtGeneFecha.Enabled = False
            txtEnviFecha.Enabled = False
            txtReceFecha.Enabled = False

            txtAnioFil.Valor = Today.Year

            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Sub mSetearEventos()
        btnModi.Attributes.Add("onclick", "return(mExportar());")
        btnConf.Attributes.Add("onclick", "if(!confirm('Confirma la Recepcion ?')){return false;}else {return(mExportar());}")
        btnApro.Attributes.Add("onclick", "if(!confirm('Confirma la aprobación de todos los socios?')){return false;}else {return(mExportar());}")
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPetiFil, "T", SRA_Neg.Constantes.PeriodoTipos.Bimestre)
        clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPeti, "S", SRA_Neg.Constantes.PeriodoTipos.Bimestre)

        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarjFil, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "S")

        clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", SRA_Neg.Constantes.EstaTipos.EstaTipo_DebitosDeta)

        cmbEsta.Items.Remove(cmbEsta.Items.FindByValue(SRA_Neg.Constantes.Estados.DebitoDeta_Devuelto))
        cmbEsta.Items.Remove(cmbEsta.Items.FindByValue(SRA_Neg.Constantes.Estados.DebitoDeta_Incluido))
    End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, Optional ByVal pbooConfirmado As Boolean = False)
      Select Case pstrTabla
         Case mstrDebitosDeta
            btnAltaDeta.Enabled = Not pbooConfirmado
            usrSoci.Activo = Not pbooConfirmado
            cmbEsta.Enabled = Not pbooConfirmado

         Case Else
            btnModi.Enabled = Not pbooConfirmado
            btnConf.Enabled = Not pbooConfirmado

            cmbPeti.Enabled = pbooAlta
            cmbTarj.Enabled = pbooAlta
            txtAnio.Enabled = pbooAlta
            txtPerio.Enabled = pbooAlta

            grdDeta.Columns(0).Visible = Not pbooConfirmado
      End Select
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim lintComiCant As Integer
      Dim lbooConfirmada As Boolean

      mCrearDataSet(pstrId)

      With mdsDatos.Tables(mstrTabla).Rows(0)
         hdnId.Text = .Item("deca_id").ToString()
                txtGeneFecha.Fecha = IIf(IsDBNull(.Item("deca_gene_fecha")), String.Empty, .Item("deca_gene_fecha"))
                txtEnviFecha.Fecha = IIf(IsDBNull(.Item("deca_envi_fecha")), String.Empty, .Item("deca_envi_fecha"))
                txtReceFecha.Fecha = IIf(IsDBNull(.Item("deca_rece_fecha")), String.Empty, .Item("deca_rece_fecha"))
                cmbTarj.Valor = .Item("deca_tarj_id")
         txtAnio.Valor = .Item("deca_anio")
         cmbPeti.Valor = .Item("deca_peti_id")
         txtPerio.Valor = .Item("deca_perio")

         lbooConfirmada = Not .IsNull("deca_rece_fecha")
      End With

      lblTitu.Text = "Registro Seleccionado: " + CDate(txtGeneFecha.Fecha).ToString("dd/MM/yyyy")

      mSetearEditor("", False, lbooConfirmada)

      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""
            txtGeneFecha.Fecha = Today

            txtEnviFecha.Text = ""
      cmbTarj.Limpiar()
      txtAnio.Text = Today.Year
      cmbPeti.Limpiar()
      txtPerio.Text = ""

      cmbPeti.Enabled = True
      cmbTarj.Enabled = True
      txtAnio.Enabled = True
      txtPerio.Enabled = True

      lblBaja.Text = ""

      grdDeta.CurrentPageIndex = 0

      mCrearDataSet("")

      mLimpiarDeta()

      mShowTabs(1)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      Dim lbooVisiOri As Boolean = panDato.Visible

      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         mShowTabs(1)
         grdDato.DataSource = Nothing
         grdDato.DataBind()

      Else
         Session(mstrTabla) = Nothing
         mdsDatos = Nothing
      End If

      If lbooVisiOri And Not pbooVisi Then
         txtAnioFil.Valor = Today.Year
      End If

      tabLinks.Visible = pbooVisi
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True
      panDeta.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkDeta.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
         Case 2
            panDeta.Visible = True
            lnkDeta.Font.Bold = True
            grdDeta.Visible = True
      End Select
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

   Private Sub lnkDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
      mShowTabs(2)
   End Sub
#End Region

#Region "Opciones de ABM"
        Private Sub mModi(ByVal pbooEsConf As Boolean)
            Try
                If pbooEsConf Then
                    With mdsDatos.Tables(mstrTabla).Rows(0)
                        .Item("deca_rece_fecha") = System.DateTime.Now
                    End With

                    txtReceFecha.Fecha = Today

                    For Each ldrDatos As DataRow In mdsDatos.Tables(mstrDebitosDeta).Select("dede_esta_id=" & SRA_Neg.Constantes.Estados.DebitoDeta_Vigente)
                        ldrDatos.Item("dede_esta_id") = SRA_Neg.Constantes.Estados.DebitoDeta_Aprobado
                    Next
                End If

                ''mGuardarDatos()

                Dim ldsTmp As DataSet = mdsDatos.Copy

                If pbooEsConf Then
                    Me.gAgregarTablaProceso(ldsTmp, mdsDatos, mstrTabla)
                End If

                Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTmp)
                lobjGenericoRel.Modi()

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
                txtReceFecha.Text = ""
            End Try
        End Sub

        Private Sub mGuardarDatos()
            With mdsDatos.Tables(mstrTabla).Rows(0)
                If txtReceFecha.Fecha.Length > 0 Then
                    .Item("deca_rece_fecha") = txtReceFecha.Fecha
                End If
            End With
        End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrDebitosDeta

      With mdsDatos.Tables(mstrTabla).Rows(0)
         If .IsNull("deca_id") Then
            .Item("deca_id") = -1
         End If
      End With

      mConsultarDeta()
      Session(mstrTabla) = mdsDatos
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi(False)
   End Sub

   Private Sub btnConf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConf.Click
      mModi(True)
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub txtAnioFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnioFil.TextChanged
      Try
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @deca_anio=" + txtAnioFil.Valor.ToString)
         lstrCmd.Append(",@deca_peti_id=" + cmbPetiFil.Valor.ToString)
         lstrCmd.Append(",@deca_perio=" + txtPeriFil.Valor.ToString)
         lstrCmd.Append(",@deca_tarj_id=" + cmbTarjFil.Valor.ToString)
         lstrCmd.Append(",@deca_acti_id=" + CType(SRA_Neg.Constantes.Actividades.Socios, String))
         lstrCmd.Append(",@etapa='R'")

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Detalle"
   Public Sub grdDeta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDeta.EditItemIndex = -1
         If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
            grdDeta.CurrentPageIndex = 0
         Else
            grdDeta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarDeta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultarDeta(Optional ByVal pstrSociId As String = "")
      With mdsDatos.Tables(mstrDebitosDeta)
         If pstrSociId = "" Then
            .DefaultView.RowFilter = "dede_total_impo<>0"
            .DefaultView.Sort = "_clie_apel"
         Else
            .DefaultView.RowFilter = "dede_total_impo<>0 AND dede_soci_id = " + pstrSociId
         End If

         grdDeta.DataSource = .DefaultView
         grdDeta.DataBind()
      End With
   End Sub

   Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mCargarDatosDeta(E.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarDatosDeta(ByVal pstrDedeId As String)
      Dim ldrDeta As DataRow

      hdnDedeId.Text = pstrDedeId
      ldrDeta = mdsDatos.Tables(mstrDebitosDeta).Select("dede_id=" & hdnDedeId.Text)(0)

      With ldrDeta
         usrSoci.Valor = .Item("dede_soci_id")
         txtImpo.Valor = .Item("dede_impo")
         cmbEsta.Valor = .Item("dede_esta_id")
         txtTaclNume.Text = .Item("_tacl_nume")

         lblAuto.Text = .Item("_auto").ToString
         lblCotiDesc.Text = .Item("_coti_desc").ToString
      End With
   End Sub

   Private Sub mActualizarDeta(ByVal pbooTodo As Boolean)
      Try
         If Not pbooTodo And usrSoci.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un socio.")
         End If

         mGuardarDatosDeta(pbooTodo)

         mLimpiarDeta()
         mConsultarDeta()

         cmbPeti.Enabled = False
         cmbTarj.Enabled = False
         txtAnio.Enabled = False
         txtPerio.Enabled = False

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosDeta(ByVal pbooTodo As Boolean)
     If Not pbooTodo Then
        For Each ldrDatos As DataRow In mdsDatos.Tables(mstrDebitosDeta).Select("dede_soci_id=" & usrSoci.Valor.ToString)
            With ldrDatos
            .Item("dede_esta_id") = cmbEsta.Valor
            .Item("_esta_desc") = cmbEsta.SelectedItem.Text
            End With
        Next
     Else
        cmbEsta.Valor = SRA_Neg.Constantes.Estados.DebitoDeta_Aprobado
        For Each ldrDatos As DataRow In mdsDatos.Tables(mstrDebitosDeta).Select("")
            With ldrDatos
            .Item("dede_esta_id") = SRA_Neg.Constantes.Estados.DebitoDeta_Aprobado
            .Item("_esta_desc") = cmbEsta.SelectedItem.Text
            End With
        Next
     End If
   End Sub

   Private Sub mLimpiarDeta()
      hdnDedeId.Text = ""
      usrSoci.Limpiar()
      txtImpo.Text = ""
      txtTaclNume.Text = ""
      lblAuto.Text = "NO"
      lblCotiDesc.Text = ""
   End Sub

   Private Sub btnLimpDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
      mLimpiarDeta()
   End Sub

   Private Sub btnAltaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
      mActualizarDeta(False)
   End Sub

   Private Sub btnApro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApro.Click
      mActualizarDeta(True)
   End Sub

#End Region

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub usrSoci_Cambio(ByVal sender As Object) Handles usrSoci.Cambio
      Try
         Dim lstrCmd As New StringBuilder
         Dim lDr As DataRow
         Dim lvDr() As DataRow

         grdDeta.CurrentPageIndex = 0
         mConsultarDeta(usrSoci.Valor.ToString)

         If usrSoci.Valor Is DBNull.Value Then
            Return
         End If

         lvDr = mdsDatos.Tables(mstrDebitosDeta).Select("dede_soci_id=" & usrSoci.Valor)

         If lvDr.GetUpperBound(0) <> -1 Then
            lDr = lvDr(0)
         Else
            Throw New AccesoBD.clsErrNeg("El Socio no está incluido en el débito.")
         End If

         mCargarDatosDeta(lDr.Item("dede_id"))

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         mListar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mListar()
      Dim lstrRptName As String = "Debitos"
      Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

      lstrRpt += "&deca_id=" + hdnId.Text
      lstrRpt += "&esta_id=0"
      lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
      Response.Redirect(lstrRpt)
   End Sub

   Public Shared Sub gAgregarTablaProceso(ByVal ldsEsta As DataSet, ByVal ldsDatos As DataSet, ByVal pstrTabla As String)
      Dim lDr As DataRow

      ldsEsta.Tables.Add(pstrTabla & "_recep")

      With ldsEsta.Tables(ldsEsta.Tables.Count - 1)
         .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_deca_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_cemi_nume", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_emct_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_host", System.Type.GetType("System.String"))
         .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))
         lDr = .NewRow
         .Rows.Add(lDr)
      End With

      Dim oFact As New SRA_Neg.Facturacion("", "", Nothing)
      oFact.CentroEmisorNro()

      With lDr
         .Item("proc_id") = -1
         .Item("proc_deca_id") = ldsDatos.Tables(pstrTabla).Rows(0).Item("deca_id")
         .Item("proc_cemi_nume") = oFact.pCentroEmisorNro
         .Item("proc_emct_id") = oFact.pCentroEmisorId
         .Item("proc_host") = oFact.pHost
      End With
   End Sub

End Class
End Namespace
