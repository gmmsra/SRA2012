Namespace SRA

Partial Class AsientosAjuste
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
   Protected WithEvents dteFechaHastaFil As NixorControls.DateBox
   Protected WithEvents btnImprimir As NixorControls.BotonImagen
   Protected WithEvents btnAltaEstab As System.Web.UI.WebControls.Button
   Protected WithEvents btnBajaEstab As System.Web.UI.WebControls.Button
   Protected WithEvents btnModiEstab As System.Web.UI.WebControls.Button
   Protected WithEvents btnLimpEstab As System.Web.UI.WebControls.Button
   Protected WithEvents panEstab As System.Web.UI.WebControls.Panel


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "asientos"
   Private mstrTablaDetalle As String = "asientos_deta"
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
                mSetearCentroCosto()
				If panDato.Visible Then
					mdsDatos = Session(mstrTabla)
				End If
         End If

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
      '   Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object
      Dim lintCol As Integer

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDetalle)

      txtObservaciones.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asde_obse")
      txtDebe.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asde_impo")
      txtHaber.MaxLength = txtDebe.MaxLength
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuentaContable, "S")
      'clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCentroCosto, "S")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul null,60"
         mstrCmd = mstrCmd + "," + IIf(txtNumeroFil.Valor.ToString = "", "null", txtNumeroFil.Valor)

                If Me.txtFechaDesdeFil.Fecha.ToString.Length = 0 Then
                    mstrCmd = mstrCmd + ",null"
                Else
                    mstrCmd = mstrCmd + ",'" + Format(Me.txtFechaDesdeFil.Fecha, "yyyyMMdd") + "'"
                End If

                If Me.txtFechaHastaFil.Fecha.ToString.Length = 0 Then
                    mstrCmd = mstrCmd + ",null"
                Else
                    mstrCmd = mstrCmd + ",'" + Format(Me.txtFechaHastaFil.Fecha, "yyyyMMdd") + "'"
                End If

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultarDetalle()
      Try
         mLimpiarDeta()
         mstrCmd = "exec " + mstrTablaDetalle + "_consul null"
         mstrCmd = mstrCmd + "," + IIf(hdnId.Text = "", "null", hdnId.Text)
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDetalle)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
      btnBajaDeta.Enabled = Not (pbooAlta)
      btnModiDeta.Enabled = Not (pbooAlta)
      btnAltaDeta.Enabled = pbooAlta
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      mCrearDataSet(hdnId.Text)

      grdDetalle.CurrentPageIndex = 0

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtFecha.Fecha = .Item("asie_fecha")
            txtNumero.Valor = .Item("asie_nume")
            mSetearEditor(False)
            mSetearEditorDeta(True)
            mMostrarPanel(True)
            If .Item("asie_cont_nume").ToString <> "" Then
               mHabilitarControles(False)
            End If
         End With
         mShowTabs(1)
		End If
        'mSetearCentroCosto()
   End Sub

	Function mSetearCentroCosto()
        Dim lstrRet As String = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_ctables", "0" + cmbCuentaContable.Valor.ToString, "cuct_codi").ToString
        Dim lstrCta As String = ""
        If lstrRet <> "" Then lstrCta = lstrRet.Substring(0, 1)
        Dim lbooActi As Boolean = (lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3")
        cmbCentroCosto.Enabled = lbooActi
        If Not cmbCentroCosto.Enabled Or lstrCta = "" Then
            cmbCentroCosto.Limpiar()
        Else
            clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" + lstrCta, cmbCentroCosto, "id", "descrip", "S")
            If Not Request.Form("cmbCentroCosto") Is Nothing Then
                cmbCentroCosto.Valor = Request.Form("cmbCentroCosto")
            Else
                cmbCentroCosto.Valor = hdnCC.Text
            End If
        End If
 End Function


 'Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
 '	Dim lstrCta As String = ""
 '	If Not cmbCuentaContable.Valor Is DBNull.Value Then
 '		lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "cuentas_ctables", "@cuct_id", cmbCuentaContable.Valor)
 '		lstrCta = lstrCta.Substring(0, 1)
 '	End If
 '	If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
 '		If Not Request.Form("cmbCCosConcep") Is Nothing Or pbooEdit Then
 '			clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, cmbCentroCosto, "id", "descrip", "N")
 '			If Not Request.Form("cmbCCosConcep") Is Nothing Then
 '				cmbCentroCosto.Valor = Request.Form("cmbCCosConcep").ToString
 '			End If
 '		End If
 '	Else
 '		cmbCentroCosto.Items.Clear()
 '	End If
 'End Sub





 Public Sub mCrearDataSet(ByVal pstrId As String)

  mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

  mdsDatos.Tables(0).TableName = mstrTabla
  mdsDatos.Tables(1).TableName = mstrTablaDetalle

  If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
   mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
  End If

  grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
  grdDetalle.DataBind()

  Session(mstrTabla) = mdsDatos
 End Sub
 Private Sub mAgregar()
  mLimpiar()
  btnBaja.Enabled = False
  btnModi.Enabled = False
  btnAlta.Enabled = True
  mMostrarPanel(True)
 End Sub
 Private Sub mLimpiar()
  hdnId.Text = ""
  txtFecha.Text = ""
  txtNumero.Text = ""
  cmbCuentaContable.Limpiar()
  cmbCentroCosto.Limpiar()

  mLimpiarDeta()
  mCrearDataSet("")
  grdDetalle.DataSource = Nothing
  grdDetalle.DataBind()

  mShowTabs(1)
  mHabilitarControles(True)
  mSetearEditor(True)
  mSetearEditorDeta(True)
 End Sub
 Private Sub mLimpiarFil()
  txtFechaDesdeFil.Text = ""
  txtFechaHastaFil.Text = ""
  txtNumeroFil.Text = ""
  mConsultar()
 End Sub
 Private Sub mCerrar()
  mLimpiar()
  mMostrarPanel(False)
 End Sub
 Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
  If (pbooVisi) Then
   hdnPage.Text = " "
  Else
   hdnPage.Text = ""
  End If
  panDato.Visible = pbooVisi
  panSolapas.Visible = pbooVisi
  panBotones.Visible = pbooVisi
  btnAgre.Enabled = Not (panDato.Visible)
  mEstablecerPerfil()
 End Sub
 Private Sub mHabilitarControles(ByVal Habilitar As Boolean)
  txtFecha.Enabled = Habilitar
  cmbCuentaContable.Enabled = Habilitar
  cmbCentroCosto.Enabled = Habilitar
  txtDebe.Enabled = Habilitar
  txtHaber.Enabled = Habilitar
  txtObservaciones.Enabled = Habilitar
  btnAlta.Enabled = Habilitar
  btnBaja.Enabled = Habilitar
  btnModi.Enabled = Habilitar
  btnAltaDeta.Enabled = Habilitar
  btnBajaDeta.Enabled = Habilitar
  btnModiDeta.Enabled = Habilitar
 End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
      Dim Debe As Double
      Dim Haber As Double

      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      Dim Fecha As String = clsSQLServer.gExecuteScalar(mstrConn, "asientos_fecha_consul").ToString
      If Fecha <> "" Then
         If txtFecha.Fecha < CDate(Fecha) Then
            Throw New AccesoBD.clsErrNeg("La fecha no puede ser menor a ninguna fecha contabilizada.Fecha m�nima permitida: " & Format(CDate(Fecha), "dd/MM/yyyy").ToString)
         End If
      End If

      If grdDetalle.Items.Count < 2 Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el importe del Debe y del Haber.")
      End If


      For Each oDataItem As DataGridItem In grdDetalle.Items
         Debe = Debe + clsSQLServer.gFormatArg(IIf(oDataItem.Cells(4).Text <> "&nbsp;", oDataItem.Cells(4).Text, "0"), SqlDbType.Decimal)
         Haber = Haber + clsSQLServer.gFormatArg(IIf(oDataItem.Cells(5).Text <> "&nbsp;", oDataItem.Cells(5).Text, "0"), SqlDbType.Decimal)
      Next

      If Debe <> Haber Then
         Throw New AccesoBD.clsErrNeg("La sumatoria del Debe y del Haber deben ser iguales.")
      End If


   End Sub
   Private Sub mGuardarDatos()
      mValidaDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("asie_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("asie_fecha") = txtFecha.Fecha
         .Item("asie_auto") = False
         .Item("asie_tias_id") = 60
         .Item("asie_emct_id") = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
         .Item("asie_audi_user") = Session("sUserId").ToString()
      End With
   End Sub
   'Metodos de los Establecimientos
   Private Sub mActualizarDeta()
      Try
         mGuardarDatosDetalle()

         mLimpiarDeta()
         grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
         grdDetalle.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaDeta()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDetalle).Select("asde_id=" & hdnDetalleId.Text)(0)
         row.Delete()
         grdDetalle.DataSource = mdsDatos.Tables(mstrTablaDetalle)
         grdDetalle.DataBind()
         mLimpiarDeta()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosDetalle()
      Dim ldrCorre As DataRow

      mValidaDatosDetalle()

      If hdnDetalleId.Text = "" Then
         ldrCorre = mdsDatos.Tables(mstrTablaDetalle).NewRow
         ldrCorre.Item("asde_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDetalle), "asde_id")
      Else
         ldrCorre = mdsDatos.Tables(mstrTablaDetalle).Select("asde_id=" & hdnDetalleId.Text)(0)
      End If

      With ldrCorre
         .Item("asde_impo") = IIf(txtDebe.Valor.ToString <> "", txtDebe.Valor, txtHaber.Valor)
                .Item("_debe") = IIf(txtDebe.Valor.Length > 0, txtDebe.Valor, 0)
                .Item("_haber") = IIf(txtHaber.Valor.Length > 0, txtHaber.Valor, 0)
         .Item("asde_dh") = IIf(txtDebe.Valor.ToString <> "", False, True)
         .Item("asde_cuct_id") = cmbCuentaContable.Valor
                .Item("_ctacontable") = cmbCuentaContable.SelectedItem.Text
                If cmbCentroCosto.Valor.Length > 0 Then
                    .Item("asde_ccos_id") = cmbCentroCosto.Valor
                End If
                If cmbCentroCosto.Valor.Length = 0 Then
                    .Item("_ccosto") = ""
                Else
                    .Item("_ccosto") = cmbCentroCosto.SelectedItem.Text
                End If
                .Item("asde_obse") = txtObservaciones.Valor
            End With
      If (hdnDetalleId.Text = "") Then
         mdsDatos.Tables(mstrTablaDetalle).Rows.Add(ldrCorre)
      End If
   End Sub
   Private Sub mValidaDatosDetalle()

      If cmbCuentaContable.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Cuenta Contable.")
      End If

      Dim lstrRet As String = clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_ctables", "0" + cmbCuentaContable.Valor.ToString, "cuct_codi").ToString
      Dim lstrCta As String = ""
      If lstrRet <> "" Then lstrCta = lstrRet.Substring(0, 1)

      mSetearCentroCosto()
      If (lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3") AndAlso cmbCentroCosto.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Centro de Costo.")
      End If

      If txtDebe.Valor.ToString = "" And txtHaber.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el importe del Debe o del Haber.")
      End If

      If txtDebe.Valor.ToString <> "" And txtHaber.Valor.ToString <> "" Then
         Throw New AccesoBD.clsErrNeg("Solo puede ingresar el importe del Debe o del Haber.")
      End If

   End Sub
   Private Sub mLimpiarDeta()
      hdnDetalleId.Text = ""
      cmbCentroCosto.Items.Clear()
      cmbCuentaContable.Limpiar()
      hdnCC.Text = ""
      txtDebe.Text = ""
      txtHaber.Text = ""
      txtObservaciones.Text = ""
      mSetearEditorDeta(True)
   End Sub
   Public Sub mEditarDatosDetalle(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         If txtFecha.Enabled = True Then
            Dim ldrEstab As DataRow

            hdnDetalleId.Text = E.Item.Cells(1).Text
            ldrEstab = mdsDatos.Tables(mstrTablaDetalle).Select("asde_id=" & hdnDetalleId.Text)(0)

            With ldrEstab
               txtDebe.Valor = .Item("_debe")
               txtHaber.Valor = .Item("_haber")
               cmbCuentaContable.Valor = .Item("asde_cuct_id")
               mSetearCentroCosto()
               cmbCentroCosto.Valor = .Item("asde_ccos_id")
               If .IsNull("asde_ccos_id") Then
                  hdnCC.Text = ""
               Else
                  hdnCC.Text = cmbCentroCosto.Valor
               End If
               txtObservaciones.Valor = .Item("asde_obse")
            End With
            mSetearEditorDeta(False)
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panGeneral.Visible = False
         lnkGeneral.Font.Bold = False
         panDetalle.Visible = False
         lnkDetalle.Font.Bold = False
         Select Case Tab
            Case 1
               panGeneral.Visible = True
               lnkGeneral.Font.Bold = True
            Case 2
               panDetalle.Visible = True
               lnkDetalle.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkdetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDetalle.Click
      mShowTabs(2)
   End Sub
   Private Sub btnAltadeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
      mActualizarDeta()
   End Sub
   Private Sub btnBajadeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
      mBajaDeta()
   End Sub
   Private Sub btnModideta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
      mActualizarDeta()
   End Sub
   Private Sub btnLimpdeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
      mLimpiarDeta()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      mConsultar()
   End Sub
#End Region

End Class
End Namespace
