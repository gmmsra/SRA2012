<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Razas" CodeFile="Razas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Razas</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaumltconcntScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/sra.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('')" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<TBODY>
					<tr>
						<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
						<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
						<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
					</tr>
					<tr>
						<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Razas</asp:label></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" Visible="True" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px" align="right">
																		<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																			IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif"
																			ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblEspecieFil" runat="server" cssclass="titulo">Especie:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbEspecieFil" class="combo" runat="server" Width="176px" onchange="mCargarRaza('cmbEspecieFil','cmbRazaFil')"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 15%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblRazasFil" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 85%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox id="cmbRazaFil" class="combo" runat="server" cssclass="cuadrotexto" Width="300px"
																						onchange="setCmbEspecieFil()" MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 20%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3" align="center"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
												OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
												AllowPaging="True" width="100%">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="raza_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="raza_codi" ReadOnly="True" HeaderText="C&#243;digo">
														<HeaderStyle Width="10%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="raza_abre" HeaderText="Abreviatura"></asp:BoundColumn>
													<asp:BoundColumn DataField="raza_desc" ReadOnly="True" HeaderText="Descripci&#243;n"></asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 9px" height="9"></TD>
										<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
												IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
												ImageOver="btnNuev2.gif" ForeColor="Transparent" ImageDisable="btnNuev0.gif" ToolTip="Nuevo Centro de Implantes"></CC1:BOTONIMAGEN></TD>
										<td align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
												BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
												BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent" ImageDisable="btnImpr0.gif"
												ToolTip="Listar"></CC1:BOTONIMAGEN></td>
									</TR>
									<TR>
										<TD height="10" colSpan="3"></TD>
									</TR>
									<TR>
										<TD colSpan="3" align="center"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
												<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
													<TR>
														<TD width="1"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="28"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="28"></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="28"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
																cssclass="solapa" Width="45px" Height="21px" CausesValidation="False"> General</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGenealogia1" runat="server"
																cssclass="solapa" Width="100px" Height="21px" CausesValidation="False">Parametros(1)</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGenealogia2" runat="server"
																cssclass="solapa" Width="100px" Height="21px" CausesValidation="False">Parametros(2)</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkAnalisis" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> An�lisis</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkRazasPadres" runat="server"
																cssclass="solapa" Width="100px" Height="21px" CausesValidation="False">Razas Padres</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDocumentos" runat="server"
																cssclass="solapa" Width="100px" Height="21px" CausesValidation="False">Documentos</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="28"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkNumeradores" runat="server"
																cssclass="solapa" Width="50px" Height="21px" CausesValidation="False">Numeradores</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="28"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD vAlign="top" colSpan="3" align="center"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" width="100%" Height="116px">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TablaGeneral" border="0" cellPadding="0" align="left">
																	<TBODY>
																		<TR>
																			<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																				align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 258px; HEIGHT: 16px" align="left">
																				<asp:Label id="lblTitu" runat="server" cssclass="titulo">Titulo:</asp:Label></TD>
																			<TD style="HEIGHT: 16px"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																				align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 258px; HEIGHT: 16px" align="right">
																				<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 16px">
																				<CC1:TEXTBOXTAB id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																				align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 258px; HEIGHT: 16px" align="right">
																				<asp:Label id="lblDescripcion" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 16px">
																				<CC1:TEXTBOXTAB id="txtDescripcion" runat="server" cssclass="cuadrotexto" Width="184px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
																				align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 258px; HEIGHT: 18px" align="right">
																				<asp:Label id="lblAbreviatura" runat="server" cssclass="titulo">Abreviatura:</asp:Label>&nbsp;</TD>
																			<TD style="HEIGHT: 18px">
																				<CC1:TEXTBOXTAB id="txtAbreviatura" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 260px" align="right">
																				<asp:Label id="lblInscribeSRA" runat="server" cssclass="titulo">Inscribe en la SRA:</asp:Label>&nbsp;
																			</TD>
																			<TD colSpan="4">
																				<cc1:combobox id="cmbraza_InscribeSRA" class="combo" runat="server" Width="50px" obligatorio="true"></cc1:combobox>
																			&nbsp;&nbsp;
																			<asp:Label id="lblFacturaSRA" runat="server" cssclass="titulo">Factura la SRA:</asp:Label>&nbsp;
																			<cc1:combobox id="cmbraza_FacturaSRA" class="combo" runat="server" Width="50px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 260px" align="right">
															<asp:Label id="lblDescorne" runat="server" cssclass="titulo">Permite descorne:</asp:Label>&nbsp;
														</TD>
														<TD colSpan="4">
															<cc1:combobox id="cmbraza_descorne" class="combo" runat="server" Width="50px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 260px" align="right">
															<asp:Label id="lblCondicional" runat="server" cssclass="titulo">Condicional:</asp:Label>&nbsp;
														</TD>
														<TD colSpan="4">
															<cc1:combobox id="cmbraza_Condicional" class="combo" runat="server" Width="50px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 260px" align="right">
															<asp:Label id="lblParteDeEspecie" runat="server" cssclass="titulo">El Tipo de Registro es Parte del </asp:Label>
															<asp:Label id="lblEspeNombNume" runat="server" cssclass="titulo"></asp:Label>&nbsp;
														</TD>
														<TD colSpan="4">
															<cc1:combobox id="cmbraza_maneja_reg_id" class="combo" runat="server" Width="50px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="2">
											<asp:panel id="panGenealogia1" runat="server" cssclass="titulo" Visible="False" Width="100%">
												<TABLE style="WIDTH: 100%" id="TablaEspecies1" border="0" cellPadding="0" align="left">
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="3" align="right">
															<asp:Label id="lblTituGen1" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 10px" align="right">
															<asp:Label id="lblPromVida" runat="server" cssclass="titulo">Prom. vida (a�os)</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 10px">
															<cc1:numberbox style="FONT-SIZE: 8pt" id="txtPromVida" runat="server" cssclass="cuadrotexto" Width="55px"
																EsDecimal="False"></cc1:numberbox>&nbsp;
															<asp:Label id="lblEspPromV" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
														<TD style="HEIGHT: 10px" align="right">
															<asp:Button id="btnListarReglas" runat="server" cssclass="boton" Width="100px" Text="Listar Reglas"></asp:Button></TD>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; heigth: 16px" align="right">
															<asp:Label id="lblDiasGest" runat="server" cssclass="titulo">D�as de Gestaci�n:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 22px" colSpan="2">
															<cc1:numberbox id="txtDiasGest" runat="server" cssclass="cuadrotexto" Width="55px" EsDecimal="False"></cc1:numberbox>&nbsp;
															<asp:Label id="lblEspGestaDias" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" height="2" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="heigth: 16px" align="right">
															<asp:Label id="lblDiasGestaMax" runat="server" cssclass="titulo">D�as Gestaci�n (M�x):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:numberbox style="FONT-SIZE: 8pt" id="txtDiasGestMax" runat="server" cssclass="cuadrotexto"
																Width="55px" EsDecimal="False"></cc1:numberbox>&nbsp;
															<asp:Label id="lblEspGestMax" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblDiasGestMin" runat="server" cssclass="titulo">D�as Gestaci�n (Min):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:numberbox style="FONT-SIZE: 8pt" id="txtDiasGestMin" runat="server" cssclass="cuadrotexto"
																Width="55px" EsDecimal="False"></cc1:numberbox>&nbsp;
															<asp:Label id="lblEspGestMin" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblDiasServCamp" runat="server" cssclass="titulo">D�as entre Servicios a campo (Min):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:numberbox style="FONT-SIZE: 8pt" id="txtDiasServCamp" runat="server" cssclass="cuadrotexto"
																Width="55px" EsDecimal="False"></cc1:numberbox>&nbsp;
															<asp:Label id="lblEspDiasCamp" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblDiasInsem" runat="server" cssclass="titulo">D�as entre Inseminaciones (Min):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:numberbox style="FONT-SIZE: 8pt" id="txtDiasInsem" runat="server" cssclass="cuadrotexto" Width="55px"
																EsDecimal="False"></cc1:numberbox>&nbsp;
															<asp:Label id="lblEspDiasInse" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD align="right">
															<asp:Label id="lblDiasParto" runat="server" cssclass="titulo">D�as entre Partos (Min):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:numberbox style="FONT-SIZE: 8pt" id="txtDiasParto" runat="server" cssclass="cuadrotexto" Width="55px"
																EsDecimal="False"></cc1:numberbox>&nbsp;
															<asp:Label id="lblEspDiasParto" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" align="right">
															<asp:Label id="lblRegPrep" runat="server" cssclass="titulo">Registro Preparatorio:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:combobox id="cmbRegPrep" class="combo" runat="server" Width="70px" onchange="SetCmbCtrlReg()"
																obligatorio="true"></cc1:combobox>&nbsp;
															<asp:Label id="lblEspRegPrep" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" align="right">
															<asp:Label id="lblCtrlReg" runat="server" cssclass="titulo">Controla Registro:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:combobox id="cmbCtrlReg" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
															<asp:Label id="lblEspCtrlReg" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" align="right">
															<asp:Label id="Label4" runat="server" cssclass="titulo">Admite Clones:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:combobox id="cmbAdmiClon" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" align="right">
															<asp:Label id="lblControlStkSemen" runat="server" cssclass="titulo">Control de stock semen:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:combobox id="cmbControlStkSemen" class="combo" runat="server" Width="70px" obligatorio="false">
																<asp:ListItem Value="">Ninguno</asp:ListItem>
																<asp:ListItem Value="1">Dosis</asp:ListItem>
																<asp:ListItem Value="2">Crias</asp:ListItem>
																<asp:ListItem Value="3">Ambos</asp:ListItem>
															</cc1:combobox></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 260px" align="right">
															<asp:Label id="lblNoControlaServicios" runat="server" cssclass="titulo">No Controla Servicios:</asp:Label>&nbsp;
														</TD>
														<TD colSpan="4">
															<cc1:combobox id="cmbNoControlaServicios" class="combo" runat="server" Width="50px" obligatorio="true"></cc1:combobox>&nbsp;
															<asp:Label id="lblServicioFechaDesde" runat="server" cssclass="titulo">desde:</asp:Label>
															<cc1:datebox id="txtFechaNoControlaServicios" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:datebox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 260px" align="right">
															<asp:Label id="lblNoControlaTE" runat="server" cssclass="titulo">No Controla Implantes:</asp:Label>&nbsp;
														</TD>
														<TD colSpan="4">
															<cc1:combobox id="cmbNoControlaTE" class="combo" runat="server" Width="50px" obligatorio="true"></cc1:combobox>&nbsp;
															<asp:Label id="lblTeFechaDesde" runat="server" cssclass="titulo">desde:</asp:Label>
															<cc1:datebox id="txtFechaNoControlaTE" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:datebox></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="2">
											<asp:panel id="panGenealogia2" runat="server" cssclass="titulo" Visible="False" Width="100%">
												<TABLE style="WIDTH: 100%" id="TablaEspecies2" border="0" cellPadding="0" align="left">
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="right">
															<asp:Label id="lblTituGen2" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 16px" vAlign="top" align="right">
															<asp:Label id="lblCaract" runat="server" cssclass="titulo">Caracter�stica a Destacar:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<CC1:TEXTBOXTAB id="txtCaract" runat="server" cssclass="cuadrotexto" Width="100px"></CC1:TEXTBOXTAB>&nbsp;
															<asp:Label id="lblEspCaract" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
															<asp:Label id="lblCaractPrec" runat="server" cssclass="titulo">Caracter�stica Precede al Nro:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbCaractPrec" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
															<asp:Label id="lblEspCaractPrec" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
															<asp:Label id="lblAstado" runat="server" cssclass="titulo">Astado:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbAstado" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
															<asp:Label id="lblEspAstado" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
															<asp:Label id="lblMocho" runat="server" cssclass="titulo">Mocho:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbMocho" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
															<asp:Label id="lblEspMocho" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
															<asp:Label id="lblCtrlColor" runat="server" cssclass="titulo">Controla Color:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbCtrlColor" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
															<asp:Label id="lblEspCtrlColor" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
															<asp:Label id="Label3" runat="server" cssclass="titulo">Controla Pelaje:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbCtrlPela" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox>&nbsp;
															<asp:Label id="lblEspCtrlPela" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 16px" align="right">
															<asp:Label id="lblPerf" runat="server" cssclass="titulo">Performance:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbPerf" class="combo" runat="server" Width="176px">
																<asp:ListItem Selected="True" Value="">Ninguna</asp:ListItem>
																<asp:ListItem Value="0">Lechera</asp:ListItem>
																<asp:ListItem Value="1">Carne</asp:ListItem>
																<asp:ListItem Value="2">Ambas</asp:ListItem>
															</cc1:combobox>&nbsp;
															<asp:Label id="lblEspPerf" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">Label</asp:Label></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 15px" align="right">
															<asp:Label id="lblRequiereInspec" runat="server" cssclass="titulo">Requiere Inspecci�n:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 15px">
															<cc1:combobox id="cmbReqInsp" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 15px" align="right">
															<asp:Label id="lblReqInpecMayor25" runat="server" cssclass="titulo">Requiere Inspecci�n Mayor de 25:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 15px">
															<cc1:combobox id="cmbReqInspMay25" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 21px; heigth: 16px" align="right">
															<asp:Label id="Label2" runat="server" cssclass="titulo">Requiere Diagrama:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 21px">
															<cc1:combobox id="cmbReqDiag" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" height="2" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 21px; heigth: 16px" align="right">
															<asp:Label id="lblCalifica" runat="server" cssclass="titulo">Califica:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 21px">
															<cc1:combobox id="cmbCalifica" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 2px" height="2" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; heigth: 16px" align="right">
															<asp:Label id="lblMuestraCrias" runat="server" cssclass="titulo">Muestra Cr�as:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbMuestraCrias" class="combo" runat="server" Width="70px" onchange="habilitarMuestras()"
																obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px" align="right">
															<asp:Label id="lblMuestraCriasPorc" runat="server" cssclass="titulo">Muestra de Cr�as (%):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:numberbox style="FONT-SIZE: 8pt" id="txtMuestraCriasPorc" runat="server" cssclass="cuadrotexto"
																Width="55px" EsDecimal="False"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px" align="right">
															<asp:Label id="lblMuestraCriasMin" runat="server" cssclass="titulo">Muestra de Cr�as (M�n):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:numberbox style="FONT-SIZE: 8pt" id="txtMuestraCriasMin" runat="server" cssclass="cuadrotexto"
																Width="55px" EsDecimal="False"></cc1:numberbox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD style="WIDTH: 100%" colSpan="3">
											<asp:panel id="PanAnalisis" runat="server" cssclass="titulo" Visible="False" Width="100%">
												<TABLE style="WIDTH: 100%" id="TablaRazas3" border="0" cellPadding="0" align="left">
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="right">
															<asp:Label id="lblAnalisisTipif" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">label</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 258px" align="right">
															<asp:Label id="lblTipifMacho" runat="server" cssclass="titulo">An�lisis Tipificaci�n (M):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbTipifMacho" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 258px" align="right">
															<asp:Label id="lblTipifHembra" runat="server" cssclass="titulo">An�lisis Tipificaci�n (H):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbTipifHembra" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 258px" align="right">
															<asp:Label id="lblTipifCria" runat="server" cssclass="titulo">An�lisis Tipificaci�n (Cr�a):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbTipifCria" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 258px" align="right">
															<asp:Label id="lblTipifCriaTE" runat="server" cssclass="titulo">An�lisis Tipificaci�n (Cr�a TE):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbTipifCriaTE" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 13px" colSpan="2" align="right">
															<asp:Label id="lblAnalisisADN" runat="server" ForeColor="RoyalBlue" Font-Italic="True" Font-Size="XX-Small">label</asp:Label></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 258px" align="right">
															<asp:Label id="lblADNMacho" runat="server" cssclass="titulo">An�lisis ADN(Macho):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbADNMacho" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 258px; HEIGHT: 19px" align="right">
															<asp:Label id="lblADNHembra" runat="server" cssclass="titulo">An�lisis ADN (Hembra):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 19px">
															<cc1:combobox id="cmbADNHembra" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 258px; HEIGHT: 15px" align="right">
															<asp:Label id="lblADNCria" runat="server" cssclass="titulo">An�lisis ADN (Cr�a):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 15px">
															<cc1:combobox id="cmbADNCria" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 258px" align="right">
															<asp:Label id="lblADNCriaTE" runat="server" cssclass="titulo">An�lisis ADN(Cr�a TE):</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbADNCriaTE" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 260px" align="right">
															<asp:Label id="lblInformaLaboratorio" runat="server" cssclass="titulo">Informa Novedades al Laboratorio</asp:Label></TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbInformaLaboratorio" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 260px" align="right">
															<asp:Label id="lblResuADNHabHembDona" runat="server" cssclass="titulo">Resultado ADN habilita como hembra Donante:</asp:Label></TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbResuADNHabHembDona" class="combo" runat="server" Width="250px" obligatorio="False"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 260px" align="right">
															<asp:Label id="lblSNPSsino" runat="server" cssclass="titulo">Controla SNPS:</asp:Label></TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbControlaSNPS" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 260px" align="right">
															<asp:Label id="lblADNFlushing" runat="server" cssclass="titulo">Controla ADN Flushing:</asp:Label></TD>
														<TD style="HEIGHT: 16px">
															<cc1:combobox id="cmbADNFlushing" class="combo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel>
											<asp:panel id="PanRazasPadres" runat="server" cssclass="titulo" Visible="False" Width="100%">
												<TABLE style="WIDTH: 100%" id="TablaAsociacionesRazas" border="0" cellPadding="0" align="left">
													<TR>
														<TD vAlign="top" colSpan="2" align="center">
															<asp:datagrid id="grdRazasPadres" runat="server" BorderStyle="None" BorderWidth="1px" width="100%"
																AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																OnPageIndexChanged="DataGridRazasPadres_Page" OnEditCommand="mEditarDatosRazasPadres" AutoGenerateColumns="False">
																<FooterStyle CssClass="footer"></FooterStyle>
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="false" DataField="rara_id" ReadOnly="True" HeaderText="IDGrilla"></asp:BoundColumn>
																	<asp:BoundColumn Visible="false" DataField="rara_raza_id" ReadOnly="True" HeaderText="IDRaza"></asp:BoundColumn>
																	<asp:BoundColumn Visible="false" DataField="rara_padr_raza_id" ReadOnly="True" HeaderText="IDPadre"></asp:BoundColumn>
																	<asp:BoundColumn Visible="true" DataField="_raza_codi" ReadOnly="True" HeaderText="C�digo"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_raza_desc" ReadOnly="True" HeaderText="Descripci�n">
																		<HeaderStyle Width="40%"></HeaderStyle>
																	</asp:BoundColumn>
																	<asp:BoundColumn DataField="_rara_sexo" ReadOnly="True" HeaderText="Sexo">
																		<HeaderStyle Width="40%"></HeaderStyle>
																	</asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 22px" vAlign="top" align="right">
															<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 22px">
															<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="300px" onchange="setCmbEspecie('cmbEspecie','cmbRaza')"
																MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 255px; HEIGHT: 21px; heigth: 16px" align="right">
															<asp:Label id="lblSexo" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 21px">
															<cc1:combobox id="cmbSexo" class="combo" runat="server" Width="113px" AceptaNull="true">
																<asp:ListItem Selected="True" value="">Ambos</asp:ListItem>
																<asp:ListItem Value="0">Hembra</asp:ListItem>
																<asp:ListItem Value="1">Macho</asp:ListItem>
															</cc1:combobox></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="center">
															<asp:Label id="lblBajaAutor" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD colSpan="2" align="center">
															<asp:Button id="btnAltaRazasPadres" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBajaRazasPadres" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>
															<asp:Button id="btnModiRazasPadres" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>
															<asp:Button id="btnLimpRazasPadres" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel><!-- FM -->
											<asp:panel id="panDocumentos" runat="server" cssclass="titulo" Visible="False" Width="100%">
												<TABLE style="WIDTH: 100%" id="TablaDocumentos" border="0" cellPadding="0" align="left">
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD vAlign="top" colSpan="3" align="center">
															<asp:datagrid id="grdDocum" runat="server" BorderStyle="None" BorderWidth="1px" Visible="True"
																width="100%" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None"
																CellPadding="1" OnPageIndexChanged="DataGridDocum_Page" OnEditCommand="mEditarDatosDocum"
																AutoGenerateColumns="False">
																<FooterStyle CssClass="footer"></FooterStyle>
																<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																<Columns>
																	<asp:TemplateColumn>
																		<HeaderStyle Width="2%"></HeaderStyle>
																		<ItemTemplate>
																			<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																				<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																			</asp:LinkButton>
																		</ItemTemplate>
																	</asp:TemplateColumn>
																	<asp:BoundColumn Visible="False" DataField="razd_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																	<asp:BoundColumn DataField="razd_path" HeaderText="Documento"></asp:BoundColumn>
																	<asp:BoundColumn DataField="razd_refe" HeaderText="Referencia"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_razd_imag" HeaderText="Imagen"></asp:BoundColumn>
																	<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
																</Columns>
																<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
															</asp:datagrid></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 14px" vAlign="top" align="right">
															<asp:Label id="lblFoto" runat="server" cssclass="titulo">Documento:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 14px" height="14" colSpan="2">
															<CC1:TEXTBOXTAB id="txtFoto" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG style="CURSOR: hand" id="imgDelFoto" onclick="javascript:mLimpiar('txtFoto','btnFotoVer');"
																alt="Limpiar Foto" src="imagenes\del.gif" runat="server"><BUTTON style="WIDTH: 90px" id="btnFotoVer" class="boton" runat="server" value="Ver Foto">Ver 
																Foto</BUTTON><BR>
															<INPUT style="WIDTH: 340px; HEIGHT: 22px" id="filDocumDoc" name="File1" size="49" type="file"
																runat="server"></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
															<asp:Label id="lblDocumRefer" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<CC1:TEXTBOXTAB id="txtDocumRefer" runat="server" cssclass="cuadrotexto" Width="376px"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
															<asp:Label id="lblEsImg" runat="server" cssclass="titulo">Es Im�gen:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<asp:checkbox id="chkEsImg" Text="" Runat="server" CssClass="titulo"></asp:checkbox></TD>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="3"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD colSpan="2" align="center">
															<asp:Label id="lblBajaDocum" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD colSpan="3" align="center">
															<asp:Button id="btnAltaDocum" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
															<asp:Button id="btnBajaDocum" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:Button>
															<asp:Button id="btnModiDocum" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:Button>
															<asp:Button id="btnLimpDocum" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:Button></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
												</TABLE>
											</asp:panel>
											<asp:panel id="panNumeradores" runat="server" cssclass="titulo" Visible="False" Width="100%">
												<TABLE style="WIDTH: 100%" id="Tablanumeradores" border="0" cellPadding="0" align="left">
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
															<asp:Label id="lblNumHembras" runat="server" cssclass="titulo">Hembras:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<CC1:TEXTBOXTAB id="txtNumHembras" runat="server" cssclass="cuadrotexto" Width="376px" Enabled="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 23px" align="right">
															<asp:Label id="lblNumMachos" runat="server" cssclass="titulo">Machos:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 23px">
															<CC1:TEXTBOXTAB id="txtNumMachos" runat="server" cssclass="cuadrotexto" Width="376px" Enabled="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
															<asp:Label id="lblNumMachoDona" runat="server" cssclass="titulo">Macho Dador:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<CC1:TEXTBOXTAB id="txtNumMachoDador" runat="server" cssclass="cuadrotexto" Width="376px" Enabled="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2">
														</TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
															<asp:Label id="lblNumHembraDona" runat="server" cssclass="titulo">Hembra Donante:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<CC1:TEXTBOXTAB id="txtNumHembraDona" runat="server" cssclass="cuadrotexto" Width="376px" Enabled="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 1px" height="1" background="imagenes/formdivmed.jpg" colSpan="2"
															align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 27%; HEIGHT: 16px" align="right">
															<asp:Label id="lblNumeTramite" runat="server" cssclass="titulo">Tramite:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px">
															<CC1:TEXTBOXTAB id="txtNumTramite" runat="server" cssclass="cuadrotexto" Width="376px" Enabled="False"></CC1:TEXTBOXTAB></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 16px" align="right">
															<asp:Label id="lblraza_PermiteEditarNumeradorCriador" runat="server" cssclass="titulo">Admite modificar n�mero de criador:</asp:Label>&nbsp;</TD>
														<TD style="HEIGHT: 16px" colSpan="2">
															<cc1:combobox id="cmbraza_PermiteEditarNumeradorCriador" class="combo" runat="server" Width="70px"
																obligatorio="true"></cc1:combobox></TD>
													<TR>
													    <td align="right" background="imagenes/formdivmed.jpg" colspan="2" height="1" style="HEIGHT: 1px">
                                                            <img height="2" src="imagenes/formdivmed.jpg" width="1"> </img> </td>
												</TABLE>
											</asp:panel></TD>
									</TR>
								</TBODY>
							</TABLE>
							</asp:panel><ASP:PANEL id="panBotones" Runat="server">
								<TABLE width="100%">
									<TR>
										<TD align="center">
											<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
									</TR>
									<TR height="30">
										<TD align="center"><A id="editar" name="editar"></A>
											<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
											<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Baja"></asp:Button>&nbsp;
											<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Modificar"></asp:Button>&nbsp;
											<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
												Text="Limpiar"></asp:Button></TD>
									</TR>
								</TABLE>
							</ASP:PANEL>
							<DIV></DIV>
						</td>
					</tr>
				</TBODY>
			</table>
			<!--- FIN CONTENIDO ---> </TD>
			<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			</TR>
			<tr>
				<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE> 
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnAutorId" runat="server"></asp:textbox><asp:textbox id="hdnDocumId" runat="server"></asp:textbox><asp:textbox id="hdnRazasPAdresId" runat="server"></asp:textbox></DIV>
			<div style="DISPLAY: none"><asp:checkbox id="hdnGenealo" runat="server"></asp:checkbox><asp:checkbox id="hdnReqTipi" runat="server"></asp:checkbox><asp:checkbox id="hdnReqADN" runat="server"></asp:checkbox><ASP:TEXTBOX id="hdnRazaID" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnEspecieId" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnDescRaza" runat="server"></ASP:TEXTBOX></div>
		</form>
		<SCRIPT language="javascript">
		function habilitarMuestras()
	   {
			document.all("txtMuestraCriasPorc").disabled = !(document.all("cmbMuestraCrias").value == 1);
			document.all("txtMuestraCriasMin").disabled = !(document.all("cmbMuestraCrias").value == 1);
						
			document.all("txtMuestraCriasPorc").value = "";
			document.all("txtMuestraCriasMin").value = "";
		}	
	   function mLimpiar(pCont,pBot)
		{
			document.all(pCont).value = '';
			document.all(pBot).style.visibility="hidden";;
		}
		
		
	   function SetCmbCtrlReg()
	   {
				if (document.all("cmbRegPrep").value == 0)
					document.all("cmbCtrlReg").value = 0;
		}
		function mCargarRaza(pEspe,pRaza)
		{
		    var sFiltro = "";
		    var sOpcion="S";
		    
		    if (document.all(pEspe).value != '')
				sFiltro = document.all(pEspe).value;
			if (pRaza=="cmbRazaFil")	
				sOpcion="T";
 	       LoadComboXML("razas_cargar", sFiltro, pRaza, sOpcion);
	    }
	    
	    function setCmbEspecie(pEspe,pRaza)
	    {
			if (document.all(pRaza).value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all(pRaza).value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all(pEspe).value = vstrRet[0];
			}		
	    }
	
		function mCargarRaza()
		{
		    var sFiltro = '';
		    if (document.all("cmbEspecieFil").value != '')
				sFiltro = document.all("cmbEspecieFil").value;
				LoadComboXML("razas_cargar", sFiltro, "cmbRazaFil", "S");
	    }
	    function setCmbEspecie()
	    {
			if (document.all("cmbRazaFil").value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all("cmbRazaFil").value, "raza_espe_id").split("|");
				if(vstrRet!="")
					document.all("cmbEspecieFil").value = vstrRet[0];
			}		
	    }
	 
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
