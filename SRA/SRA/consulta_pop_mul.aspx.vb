'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar de la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_consulta_pop_mul_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
'a la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'consulta_pop_mul.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
Namespace SRA

'Partial Class consulta_pop_mul
Partial Class Migrated_consulta_pop_mul

Inherits consulta_pop_mul

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Protected WithEvents lblTitu As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()

      If Page.IsPostBack Then
         For i As Integer = 0 To 2
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            grdConsulta.Columns.Add(dgCol)
         Next
      End If
   End Sub

#End Region


#Region "Definici�n de Variables"
'   Public mstrCmd As String
'   Public mstrTabla As String
'   Public mstrFiltros As String
   Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If Not Page.IsPostBack Then
            mConsultar(False)

            Session("mulPaginas") = grdConsulta.PageCount

            For i As Integer = 0 To Session("mulPaginas")
               Session("mulPag" & i.ToString) = Nothing
            Next
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Overrides Sub mInicializar()
      mstrTabla = Request.QueryString("tabla")
      mstrFiltros = Request.QueryString("filtros")
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Overrides Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         Dim lstrId As New StringBuilder

         For Each oDataItem As DataGridItem In grdConsulta.Items
            If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
               If lstrId.Length > 0 Then lstrId.Append(",")
               lstrId.Append(oDataItem.Cells(1).Text)
            End If
         Next

         Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) = lstrId.ToString

         grdConsulta.CurrentPageIndex = E.NewPageIndex

         mConsultar(True)

         If Not Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) Is Nothing Then
            Dim lstrIdSess As String = Session("mulPag" & grdConsulta.CurrentPageIndex.ToString)
            If lstrIdSess <> "" Then
               lstrIdSess = "," & lstrIdSess & ","

               For Each oDataItem As DataGridItem In grdConsulta.Items
                  If lstrIdSess.IndexOf("," & odataitem.Cells(1).Text & ",") <> -1 Then
                     CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = True
                  End If
               Next
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", E.Item.Cells(1).Text))
      lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub

   Public Overrides Sub mConsultar(ByVal pbooPage As Boolean)
      Try
         mstrCmd = "exec " + mstrTabla + "_busq "
         mstrCmd += mstrFiltros


         Dim ds As New DataSet
         ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

         If pbooPage Then
            Dim i As Integer = grdConsulta.Columns.Count - 1
            While i > 0
               grdConsulta.Columns.Remove(grdConsulta.Columns(i))
               i -= 1
            End While
         End If

         For Each dc As DataColumn In ds.Tables(0).Columns
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            dgCol.DataField = dc.ColumnName
            dgCol.HeaderText = dc.ColumnName
            If dc.Ordinal = 0 Then
               dgCol.Visible = False
            End If

            grdConsulta.Columns.Add(dgCol)
         Next
         grdConsulta.DataSource = ds
         grdConsulta.DataBind()
         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region


   Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
      Dim lstrId As New StringBuilder
      Dim lstrClieAnt, lstrBuqeAnt As String
      Try
         For Each oDataItem As DataGridItem In grdConsulta.Items
            'If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
            If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
               If lstrId.Length > 0 Then lstrId.Append(",")
               lstrId.Append(oDataItem.Cells(1).Text)
            End If
         Next

         For i As Integer = 0 To Session("mulPaginas")
            If Not Session("mulPag" & i.ToString) Is Nothing Then
               Dim lstrIdsSess As String = Session("mulPag" & i.ToString)
               If i <> grdConsulta.CurrentPageIndex And lstrIdsSess <> "" Then
                  If lstrId.Length > 0 Then lstrId.Append(",")
                  lstrId.Append(lstrIdsSess)
               End If
               Session("mulPag" & i.ToString) = Nothing
            End If
         Next

         grdConsulta.DataSource = Nothing
         grdConsulta.DataBind()

         Dim lsbMsg As New StringBuilder
         lsbMsg.Append("<SCRIPT language='javascript'>")
         lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosPop'].value='{0}';", lstrId.ToString))
         lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
         lsbMsg.Append("window.close();")
         lsbMsg.Append("</SCRIPT>")
         Response.Write(lsbMsg.ToString)

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
      Response.Write("<Script>window.close();</script>")
   End Sub
End Class

End Namespace
