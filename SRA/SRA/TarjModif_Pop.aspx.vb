Namespace SRA

Partial Class TarjModif_Pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents grdDato As System.Web.UI.WebControls.DataGrid


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_TarjetasClientes
   Private mstrConn As String

   Private mstrId, mstrClieId, mstrId2 As String


#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      lblTitu.Text = Request.QueryString("Tarjeta")
      mstrId = Request.QueryString("ID")
      mstrClieId = Request.QueryString("ClienID")

      lblTitu2.Text = "Cliente: " & clsSQLServer.gCampoValorConsul(mstrConn, "clientes_consul @clie_id=" + mstrClieId, "clie_apel")

   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
    
         mInicializar()

         If (Not Page.IsPostBack) Then
             mSetearMaxLength()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub mSetearMaxLength()
      Dim lstrLong As Object

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDomi.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tacl_domi")
      txtSegu.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "tacl_code_segu")
   End Sub

   
#End Region

#Region "Seteo de Controles"
   
   Private Sub mLimpiar()
      txtDomi.Valor = ""
      txtSegu.Valor = ""
   End Sub

   Private Sub mCerrar()
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.document.all['hdnOK'].value='{0}';", mstrId2))
      lsbMsg.Append("window.opener.__doPostBack('hdnOK','');")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)

   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mActualizar()
      Try
         mGuardarDatos()
         mstrId2 = mstrId
         mCerrar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   

   Private Sub mGuardarDatos()
      Dim lstrCmd As String
      mValidarDatos()

      'Modifico el registro de 
      lstrCmd = "exec " + mstrTabla + "_Xmodi "
      lstrCmd = lstrCmd + "@tacl_id=" + mstrId
      lstrCmd = lstrCmd + ", @tacl_code_segu=" + txtSegu.Valor
      lstrCmd = lstrCmd + ", @tacl_domi='" + txtDomi.Valor
      lstrCmd = lstrCmd + "', @tacl_audi_user=" + Session("sUserId").ToString()
      clsSQLServer.gExecute(mstrConn, lstrCmd)

     
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If txtDomi.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el domicilio.")

      End If
      If txtSegu.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el código de seguridad.")
      End If
   End Sub
#End Region


   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mActualizar()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnlimpiar.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

End Class
End Namespace
