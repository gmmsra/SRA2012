Imports System.Data.SqlClient


Namespace SRA


Partial Class errores
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub


   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo. 
      InitializeComponent()

      'If Page.IsPostBack Then
      '   For i As Integer = 0 To 2
      '      Dim dgCol As New Web.UI.WebControls.BoundColumn
      '      grdConsulta.Columns.Add(dgCol)
      '   Next
      'End If
   End Sub

#End Region


#Region "Definici�n de Variables"
   Public mstrCmd As String
   Public mstrTabla As String
   Public mstrHdn As String
    Public mstrTitulo As String
    Public mstrIdTabla As String
    Public mstrProce As String
    Public mstrTituProducto As String
    Private mstrConn As String
    Private mbooEsConsul As Boolean
    Private mbooEsExtranjero As Boolean

    Private mbooEsSoloConsulta As Boolean
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      Try

         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            clsWeb.gCargarCombo(mstrConn, "cs_rg_mensajes_cargar", cmbErr, "id", "descrip", "S")
            mConsultar(False)
         End If
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrTabla = Request.QueryString("tabla")
      mstrTitulo = Request.QueryString("titulo")
      mstrIdTabla = Request.QueryString("id")
      mstrProce = Request.QueryString("proce")
        mstrHdn = Request.QueryString("hdn")
        Dim mstropc = Request.QueryString("opc")
		mstrTituProducto = Request.QueryString("Prdct")
        lblTituAbm.Text = mstrTitulo
		lblTituProducto.Text = "Producto:" & " " & mstrTituProducto
        If Not Request.QueryString("EsConsul") Is Nothing Then
            mbooEsConsul = CBool(CInt(Request.QueryString("EsConsul")))
        End If


        If Not Request.QueryString("SC") Is Nothing Then
            mbooEsSoloConsulta = IIf(Request.QueryString("SC").ToLower() = "t", True, False)
            ' Modificado : GSZ 23/04/2015
        End If

        If Not Request.QueryString("EsExtranjero") Is Nothing Then
            mbooEsExtranjero = IIf(Request.QueryString("EsExtranjero").ToLower() = "t", True, False)
            ' Modificado : GSZ 23/04/2015
        End If



        If mbooEsSoloConsulta Or mbooEsExtranjero Then
            btnGuardarCambios.Enabled = False
            btnErr.Enabled = False
            btnSalvar.Enabled = False

        End If

        If mbooEsConsul Then
            grdConsulta.Columns(0).Visible = False
        End If

        grdConsulta.PageSize = 100 'NO PAGINAR PARA PODER IMPRIMIR TODO EL CONTENIDO
        If (mstropc <> 1) Then
            btnImpri.Attributes.Add("onclick", "mImprimir();return(false);")
        End If
        'btnImpri.Attributes.Add("onclick", "mImprimir();return(false);")
        btnGuardarCambios.Attributes.Add("onclick", "mGuardarCambios_Click();")
        btnGuardarCambios.Visible = (mstrProce = 1)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.EditItemIndex = -1
         If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
            grdConsulta.CurrentPageIndex = 0
         Else
            grdConsulta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

      Try
         hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
         txtDesc.Text = clsFormatear.gFormatCadena(E.Item.Cells(3).Text) & " - " & clsFormatear.gFormatCadena(E.Item.Cells(4).Text)
         hdnErr.Text = clsFormatear.gFormatCadena(E.Item.Cells(5).Text)
         hdnUsua.Text = clsFormatear.gFormatCadena(E.Item.Cells(7).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Public Sub mConsultar(ByVal pbooPage As Boolean)
      Try
         mstrCmd = "exec " + mstrTabla + "_busq "
         mstrCmd = mstrCmd & mstrIdTabla & ", " & mstrProce

         Dim ds As New DataSet
         ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

         grdConsulta.DataSource = ds
         grdConsulta.DataBind()
         ds.Dispose()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub

    Public Sub mConsultar(ByVal pbooPage As Boolean, ByVal pTabla As String)
        Try
            'mstrCmd = "exec " + mstrTabla
            'mstrCmd = mstrCmd & mstrIdTabla & ", " & mstrProce

            'Dim ds As New DataSet
            'ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            'grdConsulta.DataSource = ds
            'grdConsulta.DataBind()
            'ds.Dispose()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


#End Region

Private Sub mAgregarError()
    Try
        If cmbErr.SelectedValue = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el error a agregar.")
            Return
        End If

        Dim lstrCmd As String = "exec cs_validaciones_errores_grabar "
            Select Case mstrProce
                Case "1"
                    lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("RG_PRODUCTOS_INSCRIP", SqlDbType.Char)
                    lstrCmd = lstrCmd & ",@id=" & clsSQLServer.gFormatArg(mstrIdTabla, SqlDbType.Int)
                    lstrCmd = lstrCmd & ",@TramiteId=null"
                Case "2"
                    lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("RG_SERVI_DENUNCIAS", SqlDbType.Char)
                    lstrCmd = lstrCmd & ",@id=" & clsSQLServer.gFormatArg(mstrIdTabla, SqlDbType.Int)
                    lstrCmd = lstrCmd & ",@TramiteId=null"
                Case "5", "10"
                    lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("TRAMITES_PRODUCTOS", SqlDbType.Char)
                    lstrCmd = lstrCmd & ",@id=" & clsSQLServer.gFormatArg(mstrIdTabla, SqlDbType.Int)
                    lstrCmd = lstrCmd & ",@TramiteId=null"
                Case "9"
                    lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("IMPORTACION_PRODUCTOS", SqlDbType.Char)
                    lstrCmd = lstrCmd & ",@id=" & clsSQLServer.gFormatArg(mstrIdTabla, SqlDbType.Int)
                    lstrCmd = lstrCmd & ",@TramiteId=null" ' Dario 2014-11-12 se pasa null este y se informa en de arriba
                Case "6"
                    lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("TE_DENUN_DETA", SqlDbType.Char)
                    lstrCmd = lstrCmd & ",@id=" & clsSQLServer.gFormatArg(mstrIdTabla, SqlDbType.Int)
                    lstrCmd = lstrCmd & ",@TramiteId=null"
                Case "12", "13", "14"
                    lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("TRAMITES", SqlDbType.Char)
                    lstrCmd = lstrCmd & ",@id=" & clsSQLServer.gFormatArg(mstrIdTabla, SqlDbType.Int)
                    lstrCmd = lstrCmd & ",@TramiteId=null"
                Case "11", "15", "16"
                    lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("SEMEN_STOCK", SqlDbType.Char)
                    lstrCmd = lstrCmd & ",@id=null"
                    lstrCmd = lstrCmd & ",@TramiteId=" & clsSQLServer.gFormatArg(mstrIdTabla, SqlDbType.Int)
                Case "18"
                    lstrCmd = lstrCmd & " " & clsSQLServer.gFormatArg("PRESTAMOS", SqlDbType.Char)
                    lstrCmd = lstrCmd & ",@id=" & clsSQLServer.gFormatArg(mstrIdTabla, SqlDbType.Int)
                    lstrCmd = lstrCmd & ",@TramiteId=null"
            End Select

            lstrCmd = lstrCmd & ",@rmen_id=" & clsSQLServer.gFormatArg(cmbErr.Valor.ToString(), SqlDbType.Int)
            lstrCmd = lstrCmd & ",@audi_user=" & clsSQLServer.gFormatArg(Session("sUserId").ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@SalvadoActivado=0"
            lstrCmd = lstrCmd & ",@manual=1"
            lstrCmd = lstrCmd & ",@obser=null"
            lstrCmd = lstrCmd & ",@bitErroresSalvar=1"

            clsSQLServer.gExecute(mstrConn, lstrCmd)

            mConsultar(False)

        Catch ex As Exception
        clsError.gManejarError(Me, ex)
    End Try

End Sub

Private Sub mSalvarError()
        Try
            If (hdnErr.Text <> "") Then
                Dim lstrCmd As String = "exec cs_validaciones_errores_salvar "
                Select Case mstrProce
                    Case "1"
                        lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("RG_PRODUCTOS_INSCRIP", SqlDbType.VarChar)
                    Case "2"
                        lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("RG_SERVI_DENUNCIAS", SqlDbType.VarChar)
                    Case "5", "10"
                        lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("TRAMITES_PRODUCTOS", SqlDbType.VarChar)
                    Case "9"
                        lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("IMPORTACION_PRODUCTOS", SqlDbType.VarChar)
                    Case "6"
                        lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("TE_DENUN_DETA", SqlDbType.VarChar)
                    Case "12", "13", "14"
                        lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("TRAMITES", SqlDbType.VarChar)
                    Case "11", "15", "16"
                        lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("SEMEN_STOCK", SqlDbType.Char)
                    Case "18"
                        lstrCmd = lstrCmd & " @tabla=" & clsSQLServer.gFormatArg("PRESTAMOS", SqlDbType.VarChar)
                End Select
                lstrCmd = lstrCmd & ",@id=" & clsSQLServer.gFormatArg(mstrIdTabla, SqlDbType.Int)
                lstrCmd = lstrCmd & ",@err_id=" & clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                lstrCmd = lstrCmd & ",@baja_manual_user=" + clsSQLServer.gFormatArg(Session("sUserId").ToString, SqlDbType.Int)
                If hdnUsua.Text = "" Then
                    lstrCmd = lstrCmd & ",@salvar=1"
                Else
                    lstrCmd = lstrCmd & ",@salvar=0"
                End If
                lstrCmd = lstrCmd & ",@bitErroresSalvar=1"

                clsSQLServer.gExecute(mstrConn, lstrCmd)

                hdnId.Text = ""
                txtDesc.Text = ""
                hdnErr.Text = ""
                hdnUsua.Text = ""

                mConsultar(False)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

End Sub

Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
  mSalvarError()
End Sub

Private Sub btnErr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnErr.Click
  mAgregarError()
End Sub

    Private Sub btnImpri_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImpri.Click
        Try
            Dim params As String
            Dim lstrRptName As String = "NacimientoErrores"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            params = "&id=" + mstrIdTabla
            params += "&proce=" + mstrProce
            params += "&titulo=" + mstrTituProducto.ToString
            lstrRpt += params
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class

End Namespace
