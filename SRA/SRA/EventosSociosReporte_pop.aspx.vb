Namespace SRA

    Partial Class EventosSociosReporte_pop
        Inherits FormGenerico

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Definici�n de Variables"

        Private mstrConn As String
        Private mchrSepa As Char = ";"c

#End Region

#Region "Operaciones sobre la P�gina"

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)

                If Not Page.IsPostBack Then
                    mCargarCombos()
                End If


            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            Try
                Response.Write("<SCRIPT language='javascript'> window.close(); </SCRIPT>")

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mCargarCombos()
            Try
                clsWeb.gCargarRefeCmb(mstrConn, "eventos", cmbEvento, "SS", "@EntreFechas='S'")

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mImprimir()
            Try
                Dim lstrRptName As String
                Dim lstrParametros As String
                Dim lstrRpt As String
                'Dim dteFechaInicio As Date = #1/1/1900#
                'Dim dteFechaDesde As Date = txtFechaDesde.Text
                'Dim dteFechaHasta As Date = txtFechaHasta.Text
                'Dim lngFechaDesde As Long = DateDiff(DateInterval.Day, dteFechaInicio, dteFechaDesde)
                'Dim lngFechaHasta As Long = DateDiff(DateInterval.Day, dteFechaInicio, dteFechaHasta)
                Dim lintFechaDesde As Integer = CType(clsFormatear.gFormatFechaString(txtFechaDesde.Text, "Int32"), Integer)
                Dim lintFechaHasta As Integer = CType(clsFormatear.gFormatFechaString(txtFechaHasta.Text, "Int32"), Integer)

                Select Case hdnReporte.Text
                    Case 0
                        'Resumido.
                        lstrRptName = "SociosEventosResumido"
                        lstrParametros = "&even_id=" + cmbEvento.Valor.ToString
                        lstrParametros += "&fechaDesde=" + lintFechaDesde.ToString
                        lstrParametros += "&fechaHasta=" + lintFechaHasta.ToString

                    Case Else
                        'Detalle.
                        lstrRptName = "SociosEventosDetallado"
                        lstrParametros = "&even_id=" + cmbEvento.Valor.ToString
                        lstrParametros += "&fechaDesde=" + lintFechaDesde.ToString
                        lstrParametros += "&fechaHasta=" + lintFechaHasta.ToString
                        lstrParametros += "&tipo=" + hdnReporte.Text
                        lstrParametros += "&EntradaDesc=" + hdnReporteDesc.Text
                        lstrParametros += "&EventoDesc=" + cmbEvento.SelectedItem.Text
                        lstrParametros += "&OrdenaPor=" + cmbOrdenaPor.Valor.ToString

                End Select

                lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                lstrRpt += lstrParametros

                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                Response.Redirect(lstrRpt)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
            Try
                If mValidaDatos() Then
                    mImprimir()
                End If


            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Function mValidaDatos() As Boolean
            Try
                If txtFechaDesde.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar Fecha Desde.")
                    Return False
                End If

                If txtFechaHasta.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar Fecha Hasta.")
                    Return False
                End If

                If Not (txtFechaDesde.Text = "") And Not (txtFechaHasta.Text = "") Then
                    If CDate(txtFechaDesde.Fecha) > CDate(txtFechaHasta.Fecha) Then
                        Throw New AccesoBD.clsErrNeg("La Fecha Desde no puede ser mayor que la Fecha Hasta.")
                        Return False
                    End If
                End If

                Return True

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
                Return False
            End Try
        End Function

        Private Sub cmbEvento_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbEvento.SelectedIndexChanged
            clsWeb.gCargarRefeCmb(mstrConn, "eventos_entradas_por_evento", cmbReporte, "", "@even_id=" + cmbEvento.Valor.ToString)
        End Sub

#End Region

    End Class

End Namespace
