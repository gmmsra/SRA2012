'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar de la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_FormGenerico_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
' a la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'FormGenerico.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
Imports Generico


Namespace SRA


'Partial Class FormGenerico
Partial Class Migrated_FormGenerico

Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private mstrConn As String

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not Page.IsPostBack Then
            mstrConn = clsWeb.gVerificarConexion(Me)
            AuditarSesionXUsuario()
        End If
    End Sub

    Private Sub AuditarSesionXUsuario()
        Try
            'Obtengo el Nombre de la P�gina.
            Dim strNombrePagina As String = Me.Page.Request.Url.AbsoluteUri.Substring(Me.Page.Request.Url.AbsoluteUri.LastIndexOf("/") + 1, Len(Me.Page.Request.Url.AbsoluteUri) - Me.Page.Request.Url.AbsoluteUri.LastIndexOf("/") - 1)

            'Instancio el objeto Auditoria. 
            Dim objAudi As New Objetos.Auditoria

            'Objeto Auditoria.
            objAudi.UserID = Session("sUserId")
            objAudi.NombreForm = strNombrePagina
            objAudi.Modulo = "Etapa1/Etapa2"
            objAudi.Sistema = "WEB"

            'Audito la sesion por usuario.
            clsAuditoria.AuditoriaSesionesXUsuario(objAudi, mstrConn)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


End Class

End Namespace
