Namespace SRA

Partial Class TarjetasHabilitadas_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()

      If Page.IsPostBack Then
         For i As Integer = 0 To 2
            Dim dgCol As New Web.UI.WebControls.BoundColumn
            grdConsulta.Columns.Add(dgCol)
         Next
      End If
   End Sub

#End Region

   Protected WithEvents lblTitu As System.Web.UI.WebControls.Label

#Region "Definici�n de Variables"
   Public mstrCmd As String
   Public mstrTabla As String
   Public mstrTitulo As String
   Public mstrFiltros As String
   Private mstrConn As String
   Private mbooEsConsul As Boolean


   Private Enum campos
      tacl_id = 0
      tacl_nume = 1
      tarj_desc = 2
      tacl_soci_id = 3
      tacl_alum_id = 4
      tacl_vta_tele = 5
      tacl_code_segu = 6
      tacl_domi = 7
   End Enum

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      Try

         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            mConsultarCabecera()
            mConsultar(False)
         End If
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      mstrTabla = Request.QueryString("tabla")
      mstrTitulo = Request.QueryString("titulo")
      mstrFiltros = Request.QueryString("filtros")
      If Not Request.QueryString("EsConsul") Is Nothing Then
         mbooEsConsul = CBool(CInt(Request.QueryString("EsConsul")))
      End If

      If mbooEsConsul Then
         grdConsulta.Columns(0).Visible = False
      End If
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.EditItemIndex = -1
         If (grdConsulta.CurrentPageIndex < 0 Or grdConsulta.CurrentPageIndex >= grdConsulta.PageCount) Then
            grdConsulta.CurrentPageIndex = 0
         Else
            grdConsulta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar(True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mConsultarCabecera()
      Dim ds As New DataSet
      mstrCmd = "exec ClientesX_busq "
      mstrCmd += mstrFiltros
      ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      'ds = mConsDatos("ClientesX", mstrFiltros)
      With ds.Tables(0).Rows(0)
         txtNomb.Valor = .Item("clie_apel")
         txtDoc.Valor = .Item("docu")
      End With
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      'si es vta telefonica y esta nulo alguno de los campos (segu/domi)
      'abre directamente el ABM de tarjetas

      Dim ds As New DataSet
      Dim booTarjVencida As Boolean
      mstrCmd = "exec " + mstrTabla + "_busq "
      mstrCmd += mstrFiltros + "," + E.Item.Cells(1).Text
      'ds = mConsDatos(mstrTabla, mstrFiltros + "," + E.Item.Cells(1).Text)
      ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      If Not ds.Tables(0).Rows(0).IsNull("tacl_vcto_fecha") Then          'avisa que esta vencida
         If ds.Tables(0).Rows(0).Item("tacl_vcto_fecha") < Now Then
            booTarjVencida = True
         End If
      End If
      If ds.Tables(0).Rows(0).Item("Vta_Tel") = "Si" Then
         If ds.Tables(0).Rows(0).IsNull("tacl_code_segu") Or ds.Tables(0).Rows(0).IsNull("tacl_domi") Then
            'abrir form de edicion de los campos segu/domi)
            mAbrirVentana("Tarjeta: " + E.Item.Cells(3).Text + " Nro.: " + E.Item.Cells(2).Text, E.Item.Cells(1).Text)
         Else
            mSeleccionItem(E.Item.Cells(1).Text, booTarjVencida)
         End If
      Else
         mSeleccionItem(E.Item.Cells(1).Text, booTarjVencida)
      End If

   End Sub
   Private Sub mAbrirVentana(ByVal pstrTarj As String, ByVal pstrId As String)
      Try
         Dim lsbPagina As New System.Text.StringBuilder
         lsbPagina.Append("TarjModif_pop.aspx?Tarjeta=" & pstrTarj & "&ID=" + pstrId & "&ClienID=" & mstrFiltros)
         clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 600, 250, 100, 100)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSeleccionItem(ByVal pstrId As String, Optional ByVal pbooTarjVencida As Boolean = False)
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.document.all['hdnDatosTarjPop'].value='{0}';", pstrId))
      lsbMsg.Append(IIf(pbooTarjVencida, "window.opener.document.all['hdnTarjVenc'].value='1';", "window.opener.document.all['hdnTarjVenc'].value='0';"))
      lsbMsg.Append("window.opener.__doPostBack('hdnDatosTarjPop','');")

      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub
   Private Function mConsDatos(ByVal pstrTabla As String, ByVal pstrFiltro As String) As DataSet
      mstrCmd = "exec " + pstrTabla + "_busq "
      mstrCmd += mstrFiltros

      Dim ds As New DataSet
      ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      Return ds
   End Function
   Public Sub mConsultar(ByVal pbooPage As Boolean)

      Try

         mstrCmd = "exec " + mstrTabla & "_busq "
         mstrCmd += mstrFiltros

         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdConsulta)


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub
#End Region



   Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
      Response.Write("<Script>window.close();</script>")
   End Sub

   Private Sub hdnOK_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
      If hdnOK.Text <> "" Then
         mSeleccionItem(hdnOK.Text)
      End If
   End Sub


   '   Public Sub mItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
   '      Try
   '         If e.Item.ItemIndex <> -1 Then
   '            If Not CType(e.Item.DataItem, DataRowView).Row.IsNull("tacl_code_segu") Then
   '               e.Item.Cells(4).Text = SRA_Neg.Encript.gDesencriptar(CType(e.Item.DataItem, DataRowView).Row.Item("tacl_code_segu").ToString)
   '            End If
   '         End If

   '      Catch ex As Exception
   '         clsError.gManejarError(Me, ex)
   '      End Try
   '   End Sub

End Class

End Namespace

