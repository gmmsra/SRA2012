<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AsientosAjuste" CodeFile="AsientosAjuste.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Asientos de Ajuste</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">		
		function mSetearCC()
		{
			if (document.all('cmbCentroCosto')!=null)
				document.all('hdnCC').value = document.all('cmbCentroCosto').value;
	    }
		function cmbCuentaContable_change(pCmb)
		{
			var strRet = LeerCamposXML("cuentas_ctables", pCmb.value, "cuct_codi");
			document.all('hdnCta').value = '';
			if (strRet!='')
			{
				var lstrCta = strRet.substring(0,1);
				document.all('hdnCta').value = lstrCta;
				if (lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
				{
					document.all('cmbCentroCosto').disabled = true;
					document.all('txtcmbCentroCosto').disabled = true;
					document.all('cmbCentroCosto').innerHTML = '';
					document.all('txtcmbCentroCosto').value = '';
				}
				else
				{
					document.all('cmbCentroCosto').disabled = false;
					document.all('txtcmbCentroCosto').disabled = false;
					LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, "cmbCentroCosto", "N");					
				}
			}			
		}
				
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD></TD>
									<TD width="100%" colSpan="2"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 27px" height="27"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Asientos de Ajuste</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="100%" Visible="True" BorderStyle="Solid"
											BorderWidth="0">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																		ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="10"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 70px">
														<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																	<TABLE height="100%" cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="WIDTH: 2.92%; HEIGHT: 30px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 20.27%; HEIGHT: 30px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblNumeroFil" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 100%; HEIGHT: 30px" background="imagenes/formfdofields.jpg">
																				<CC1:TEXTBOXTAB id="txtNumeroFil" runat="server" cssclass="cuadrotexto" Width="160px"></CC1:TEXTBOXTAB></TD>
																			<TD style="WIDTH: 100%; HEIGHT: 30px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2.92%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 20.27%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 100%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																			<TD style="WIDTH: 100%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 2.92%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"></TD>
																			<TD style="WIDTH: 20.27%; HEIGHT: 17px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 100%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																			<TD style="WIDTH: 100%; HEIGHT: 17px" background="imagenes/formfdofields.jpg"></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD style="HEIGHT: 95px" width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<!---fin filtro --->
								<TR>
									<TD colSpan="3" height="10"></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
											AutoGenerateColumns="False">
											<FooterStyle CssClass="footer"></FooterStyle>
											<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
											<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="2%"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="asie_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
												<asp:BoundColumn DataField="asie_nume" ReadOnly="True" HeaderText="N�mero">
													<HeaderStyle Width="15%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="asie_fecha" ReadOnly="True" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												<asp:BoundColumn DataField="asie_cont_nume" ReadOnly="True" HeaderText="N�mero Definitivo"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<TD colSpan="2"></TD>
								</tr>
								<TR>
									<TD style="HEIGHT: 9px" height="9"></TD>
									<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
											IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
											ImageOver="btnNuev2.gif" ForeColor="Transparent" ToolTip="Agregar un Nuevo Asiento" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<tr>
									<TD colSpan="2"></TD>
								</tr>
								<TR>
									<TD></TD>
									<TD align="center" colSpan="2"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" width="100%" Height="124%">
											<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkGeneral" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
															cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> General</asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
													<TD width="1" background="imagenes/tab_fondo.bmp">
														<asp:linkbutton id="lnkDetalle" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
															cssclass="solapa" Width="70px" Height="21px" CausesValidation="False"> Detalle</asp:linkbutton></TD>
													<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" width="100%" Height="116px">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
												border="0">
												<TR>
													<TD style="WIDTH: 100%" vAlign="top" align="right">
														<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
												</TR>
												<TR>
													<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="TablaGeneral" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" obligatorio="true"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="5"
																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 268px; HEIGHT: 16px" align="right">
																		<asp:Label id="lblNumero" runat="server" cssclass="titulo">N�mero:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 16px" colSpan="4">
																		<CC1:TEXTBOXTAB id="txtNumero" runat="server" cssclass="cuadrotexto" Width="100px" enabled="false"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="5"
																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panDetalle" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE id="TablaDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD vAlign="top" align="center" colSpan="2">
																		<asp:datagrid id="grdDetalle" runat="server" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																			OnEditCommand="mEditarDatosDetalle" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
																			CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%" PageSize="5">
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																			<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="2%"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="asde_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_ctacontable" ReadOnly="True" HeaderText="Cuenta Contable">
																					<HeaderStyle Width="35%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="_ccosto" ReadOnly="True" HeaderText="Centro de Costo">
																					<HeaderStyle Width="35%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="_debe" ReadOnly="True" HeaderText="Debe"></asp:BoundColumn>
																				<asp:BoundColumn DataField="_haber" ReadOnly="True" HeaderText="Haber"></asp:BoundColumn>
																				<asp:BoundColumn DataField="asde_obse" ReadOnly="True" HeaderText="Observaciones">
																					<HeaderStyle Width="15%"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 30%; HEIGHT: 16px" align="right">
																		<asp:Label id="lblCuentaContable" runat="server" cssclass="titulo">Cuenta Contable:</asp:Label>&nbsp;</TD>
																	<TD>
																		<table border=0 cellpadding=0 cellspacing=0>
																		<tr>
																		<td>															
																		<cc1:combobox class="cuadrotexto" id="cmbCuentaContable" runat="server" cssclass="combo" Width="340px"
																		TextMaxLength="7" filtra="true" NomOper="cuentas_ctables_cargar" MostrarBotones="False" onchange="cmbCuentaContable_change(this);"></cc1:combobox>
																		</td>
																		<td>
																			<IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																											onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCuentaContable','Cuentas Contables','');"
																											alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																		</td>
																		</tr>
																		</table>
																	</TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 30%; HEIGHT: 16px" align="right">
																		<asp:Label id="Label3" runat="server" cssclass="titulo">Centro de Costo:</asp:Label>&nbsp;</TD>
																	<TD>
																		<table border=0 cellpadding=0 cellspacing=0>
																		<tr>
																		<td>															
																		<cc1:combobox class="combo" id="cmbCentroCosto" runat="server" Width="340px" TextMaxLength="6"
																		filtra="true" MostrarBotones="False" onchange="javascript:mSetearCC();" nomoper="centrosc_cargar"
																		CssClass="combo"></cc1:combobox>
																		</td>
																		<td>
																			<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																											onclick="var lstrFil='-1'; if (document.all('hdnCta').value != '') lstrFil = document.all('hdnCta').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCentroCosto','Centros de Costos','@cuenta='+lstrFil);"
																											alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																		</td>
																		</tr>
																		</table>
																	</TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 30%; HEIGHT: 22px" align="right">
																		<asp:Label id="lblDebe" runat="server" cssclass="titulo">Debe:</asp:Label>&nbsp;</TD>
																	<TD>
																		<CC1:NUMBERBOX id="txtDebe" runat="server" cssclass="cuadrotexto" Width="100px" esdecimal="true"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 30%; HEIGHT: 22px" align="right">
																		<asp:Label id="lblHaber" runat="server" cssclass="titulo">Haber:</asp:Label>&nbsp;</TD>
																	<TD>
																		<CC1:NUMBERBOX id="txtHaber" runat="server" cssclass="cuadrotexto" Width="100px" esdecimal="true"></CC1:NUMBERBOX></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 30%; HEIGHT: 22px" vAlign="top" align="right">
																		<asp:Label id="lblObservaciones" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																	<TD vAlign="top">
																		<cc1:textboxtab id="txtObservaciones" runat="server" cssclass="textolibre" Width="300px" TextMode="MultiLine"
																			Rows="2" height="46px"></cc1:textboxtab></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR height="30">
																	<TD align="center" colSpan="2">
																		<asp:Button id="btnAltaDeta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																		<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Baja"></asp:Button>
																		<asp:Button id="btnModiDeta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Modificar"></asp:Button>
																		<asp:Button id="btnLimpDeta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																			Text="Limpiar"></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
										<DIV></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnCta" runat="server"></ASP:TEXTBOX>				
				<asp:textbox id="hdnPage" runat="server"></asp:textbox>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetalleId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCC" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all('txtcmbCuentaContable')!=null)
		{
			var lstrCta = '';
			if (document.all('txtcmbCuentaContable').value != '')
				lstrCta = document.all('txtcmbCuentaContable').value.substring(0,1);
			if (lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
			{
				document.all('cmbCentroCosto').disabled = true;
				document.all('txtcmbCentroCosto').disabled = true;
			}
			else
			{
				document.all('cmbCentroCosto').disabled = false;
				document.all('txtcmbCentroCosto').disabled = false;
			}
		}
		mSetearCC();
		if (document.all["editar"]!= null)
			document.location='#editar';
		//if (document.all["txtFecha"]!= null)
		//	document.all["txtFecha"].focus();
								   	
		</SCRIPT>
	</BODY>
</HTML>
