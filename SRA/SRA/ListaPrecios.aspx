<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ListaPrecios" CodeFile="ListaPrecios.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Lista de Precios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
			function expandir()
		{
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		
	    function mRegargos()
		 {
		 	try
				{	
     			sFiltro = document.all("txtImpoSoci").value + ";" 
     			sFiltro = sFiltro + document.all("txtImpoNoSoci").value + ";" 
     			sFiltro = sFiltro + document.all("txtImpoAdhe").value + ";" 
     			sFiltro = sFiltro + document.all("hdnRecaCtaCte").value + ";" 
     			sFiltro = sFiltro + document.all("hdnRecaTarj").value;
				
				vsRet = EjecutarMetodoXML("Utiles.Recargos", sFiltro).split(";");
				
				
				document.all("txtNoSociRecCtaCte").value = vsRet[0];
				document.all("txtNoSociRecTarj").value = vsRet[1];
				document.all("txtsociRecCtaCte").value =vsRet[2];
				document.all("txtsociRecTarj").value = vsRet[3];
				document.all("txtAdheRecCtaCte").value =vsRet[4];
				document.all("txtAdheRecTarj").value = vsRet[5];
		       }    
			
			catch(e)
				{
				alert("Error al intentar calcular los importes con recargo");
				}
	      }
	
		  
	
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Lista de Precios</asp:label></TD>
							</TR>
							<TR>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																	ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 10px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 10px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblActiFil" runat="server" cssclass="titulo">Actividad:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbActiFil" runat="server" Width="250px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="2" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
										HorizontalAlign="Right" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos"
										OnPageIndexChanged="DataGrid_Page">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
														<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="peli_id" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="_acti" HeaderText="Actividad">
												<HeaderStyle Width="60%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="peli_vige_fecha" HeaderText="F. Vigen." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="peli_aprob_fecha" HeaderText="F. Aprob." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="peli_reca_ctac" HeaderText="Rec. Cta. Cte.(%)" DataFormatString="{0:F2}">
												<HeaderStyle HorizontalAlign="Right" Width="14%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="peli_reca_tarj" HeaderText="Rec. Tarj.(%)" DataFormatString="{0:F2}">
												<HeaderStyle HorizontalAlign="Right" Width="11%"></HeaderStyle>
												<ItemStyle HorizontalAlign="Right"></ItemStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="peli_acti_id" HeaderText="peli_acti_id"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD colSpan="2"></TD>
							</TR>
							<TR>
								<TD vAlign="middle"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
										ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ImageDisable="btnNuev0.gif" ToolTip="Agregar una Nueva Lista"></CC1:BOTONIMAGEN></TD>
								<TD align="right">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Lista</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkAran" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" Height="21px" CausesValidation="False"> Aranceles</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Visible="False" BorderWidth="1px"
											BorderStyle="Solid" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD>
															<P></P>
														</TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="40%">
																			<asp:Label id="lblFechaAlta" runat="server" cssclass="titulo">Fecha Alta:</asp:Label>&nbsp;</TD>
																		<TD>
																			<asp:Label id="lblFechaA" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="40%">
																			<asp:Label id="lblActiv" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbActiv" runat="server" Width="180px" obligatorio="true" onchange=" mCargarListaPreciosCopiar()"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="40%">
																			<asp:Label id="lblFechaVige" runat="server" cssclass="titulo">Fecha Vigencia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:DateBox id="txtFechaVige" runat="server" cssclass="cuadrotexto" Width="68px" obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="40%">
																			<asp:Label id="lblFechaAprob" runat="server" cssclass="titulo">Fecha Aprobaci�n:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:DateBox id="txtFechaAprob" runat="server" cssclass="cuadrotexto" Width="68px" obligatorio="false"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="40%">
																			<asp:Label id="lblRecaCtaCte" runat="server" cssclass="titulo">Recargo Cta. Cte.(%):</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtRecaCtaCte" runat="server" cssclass="textomonto" Width="80px" obligatorio="true"
																				EsDecimal="True"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="40%">
																			<asp:Label id="lblRecaTarj" runat="server" cssclass="titulo">Recargo Tarjeta(%):</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtRecaTarj" runat="server" cssclass="textomonto" Width="80px" EsDecimal="True"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD colspan=2 style="HEIGHT: 6px" align="right" width="40%"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 14px" align="right" width="40%">
																			<asp:Label id="Label1" runat="server" cssclass="titulo">Copiar Aranceles de la Lista:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 14px">
																			<cc1:combobox class="combo" id="cmbAranCopi" runat="server" Width="120px" AceptaNull="False" AutoPostBack="True"
																				NomOper="precios_lista_copi_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right" width="40%">
																			<asp:Label id="lblRedo" runat="server" cssclass="titulo">Redondear:</asp:Label>&nbsp;</TD>
																		<TD noWrap>
																			<cc1:combobox class="combo" id="cmbRedo" runat="server" Width="48px" AceptaNull="False" onchange="javascript:mRedondear();"
																				NomOper="precios_lista_copi_cargar">
																				<asp:ListItem Value="N" Selected="True">No</asp:ListItem>
																				<asp:ListItem Value="S">Si</asp:ListItem>
																			</cc1:combobox>
																			<cc1:combobox class="combo" id="cmbRedoDeci" runat="server" Width="170px" AceptaNull="False" NomOper="precios_lista_copi_cargar">
																				<asp:ListItem Value="S" Selected="True">Sin Centavos</asp:ListItem>
																				<asp:ListItem Value="C">Con Centavos m&#250;ltiplos de 10</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="top" align="right" width="40%">
																			<asp:Label id="lblCoefi" runat="server" cssclass="titulo">Coeficientes:</asp:Label></TD>
																		<TD align="left">
																			<asp:datagrid id="grdCoef" runat="server" width="300" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
																				CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="left" AllowPaging="false"
																				ItemStyle-Height="5px" PageSize="100" PagerStyle-CssClass="titulo">
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:BoundColumn Visible="False" DataField="grar_id" HeaderText="grar_id"></asp:BoundColumn>
																					<asp:BoundColumn Visible="true" DataField="grar_desc" HeaderText="Grupo"></asp:BoundColumn>
																					<asp:TemplateColumn Visible="False">
																						<HeaderStyle Width="3%"></HeaderStyle>
																						<ItemTemplate>
																							<DIV runat="server" id="divTotal" style="DISPLAY: none"></DIV>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn HeaderText="Coef. Pesos">
																						<HeaderStyle Width="90px"></HeaderStyle>
																						<ItemTemplate>
																							<CC1:NUMBERBOX id="txtCoefPesos" runat="server" cssclass="cuadrotexto" Width="80px" EsDecimal="True"
																								value="1" onchange=""></CC1:NUMBERBOX>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn HeaderText="Coef. D�lares">
																						<HeaderStyle Width="90px"></HeaderStyle>
																						<ItemTemplate>
																							<CC1:NUMBERBOX id="txtCoefDolar" runat="server" cssclass="cuadrotexto" Width="80px" EsDecimal="True"
																								value="1" onchange=""></CC1:NUMBERBOX>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="40%" height="40"></TD>
																		<TD align="left" height="40">
																			<asp:Button id="btnCopiar" runat="server" cssclass="boton" Width="176px" Text="Copiar Aranceles"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panAran" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="tabAran" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="20%">
																			<asp:Label id="lblArancelFil" runat="server" cssclass="titulo">C�d:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtArancelCodiFil" runat="server" cssclass="cuadrotexto" Width="50px" AceptaNull="False"
																				Obligatorio="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="20%">
																			<asp:Label id="Label2" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtArancelFil" runat="server" cssclass="cuadrotexto" Width="144px" AceptaNull="False"
																				Obligatorio="False"></CC1:TEXTBOXTAB>&nbsp;
																			<asp:Button id="btnBuscFil" runat="server" cssclass="boton" Width="80px" Text="Buscar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdArancel" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																				Visible="False" OnPageIndexChanged="grdAran_PageChanged" AutoGenerateColumns="False" CellPadding="1"
																				GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" OnEditCommand="mEditarDatosArancel">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="prar_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_grar_desc" HeaderText="Grupo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_codi" HeaderText="C&#243;d.">
																						<HeaderStyle Width="5%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_arancel" HeaderText="Arancel">
																						<HeaderStyle Width="40%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_moneda" HeaderText="Moneda">
																						<HeaderStyle Width="5%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="prar_mone_id" HeaderText="prar_mone_id"></asp:BoundColumn>
																					<asp:BoundColumn DataField="prar_impo_soci" HeaderText="Imp.Socio" DataFormatString="{0:F2}">
																						<HeaderStyle HorizontalAlign="Right" Width="10%"></HeaderStyle>
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="prar_impo_noso" HeaderText="Imp.No Socio" DataFormatString="{0:F2}">
																						<HeaderStyle HorizontalAlign="Right" Width="10%"></HeaderStyle>
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="prar_impo_adhe" HeaderText="Imp.Adherente" DataFormatString="{0:F2}">
																						<HeaderStyle HorizontalAlign="Right" Width="10%"></HeaderStyle>
																						<ItemStyle HorizontalAlign="Right"></ItemStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" HeaderText="Estado">
																						<HeaderStyle Width="10%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="prar_aran_id" HeaderText="prar_aran_id"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="20%">
																			<asp:Label id="lblArancel" runat="server" cssclass="titulo">Arancel:</asp:Label>&nbsp;</TD>
																		<TD>
																			<TABLE cellSpacing="0" cellPadding="0" border="0">
																				<TR>
																					<TD>
																						<cc1:combobox id="cmbArancel" runat="server" cssclass="cuadrotexto" Width="250px" AceptaNull="False"
																							NomOper="aranceles_cargar" TextMaxLength="5" filtra="true" MostrarBotones="False"></cc1:combobox></TD>
																					<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																							onclick="mBotonBusquedaAvanzada('aranceles','aran_desc','cmbArancel','Aranceles','');"
																							alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 15px" align="right" width="20%">
																			<asp:Label id="lblMone" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 15px">
																			<cc1:combobox class="combo" id="cmbMone" runat="server" Width="180px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="20%">
																			<asp:Label id="lblImpoSoci" runat="server" cssclass="titulo">Importe socio:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtImpoSoci" runat="server" cssclass="textomonto" Width="80px" onchange="mRegargos();"
																				EsDecimal="True"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblsociRecTarj" runat="server" cssclass="titulo">C/recargo tarj.:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtsociRecTarj" runat="server" cssclass="textomonto" Width="80px" EsDecimal="True"
																				enabled="false"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblsociRecCtaCte" runat="server" cssclass="titulo">C/recargo cta.cte.:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtsociRecCtaCte" runat="server" cssclass="textomonto" Width="80px" EsDecimal="True"
																				enabled="false"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD align="right" width="20%">
																			<asp:Label id="lblImpoNoSoci" runat="server" cssclass="titulo">Importe No Socio:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtImpoNoSoci" runat="server" cssclass="textomonto" Width="80px" onchange="mRegargos();"
																				EsDecimal="True"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblNoSociRecTarj" runat="server" cssclass="titulo">C/recargo tarj.:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtNoSociRecTarj" runat="server" cssclass="textomonto" Width="80px" EsDecimal="True"
																				enabled="false"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblNoSociRecCtaCte" runat="server" cssclass="titulo">C/recargo cta.cte.:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtNoSociRecCtaCte" runat="server" cssclass="textomonto" Width="80px" EsDecimal="True"
																				enabled="false"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR id="divAdherente" runat="server">
																		<TD align="right" width="20%">
																			<asp:Label id="lblImpoAdhe" runat="server" cssclass="titulo">Importe adherente:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:numberbox id="txtImpoAdhe" runat="server" cssclass="textomonto" Width="80px" onchange="mRegargos();"
																				EsDecimal="True"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblAdheRecTarj" runat="server" cssclass="titulo">C/recargo tarj.:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtAdheRecTarj" runat="server" cssclass="textomonto" Width="80px" EsDecimal="True"
																				enabled="false"></cc1:numberbox>&nbsp;
																			<asp:Label id="lblAdheRecCtaCte" runat="server" cssclass="titulo">C/recargo cta.cte.:</asp:Label>&nbsp;
																			<cc1:numberbox id="txtAdheRecCtaCte" runat="server" cssclass="textomonto" Width="80px" EsDecimal="True"
																				enabled="false"></cc1:numberbox></TD>
																	</TR>
																	<TR id="sepAdherente" runat="server">
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="5">
																			<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnAltaAran" runat="server" cssclass="boton" Width="120px" Text="Agregar Arancel"></asp:Button>&nbsp;
																			<asp:Button id="btnBajaAran" runat="server" cssclass="boton" Width="120px" Text="Eliminar Arancel"></asp:Button>&nbsp;
																			<asp:Button id="btnModiAran" runat="server" cssclass="boton" Width="120px" Text="Modificar Arancel"></asp:Button>&nbsp;
																			<asp:Button id="btnLimpAran" runat="server" cssclass="boton" Width="120px" Text="Limpiar Arancel"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" align="center" colSpan="2">
																			<asp:Button id="btnExportar" runat="server" cssclass="boton" Width="100px" Text="Exportar Lista"></asp:Button>&nbsp;
																			<asp:Button id="btnImportar" runat="server" cssclass="boton" Width="100px" Text="Importar Lista"></asp:Button>&nbsp;
																			<INPUT id="txtInputFile" style="WIDTH: 350px; HEIGHT: 22px" type="file" name="File1" runat="server"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center"></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
															Text="Limpiar"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></DIV>
								</TD>
							</TR>
						</TABLE>
					<!--- FIN CONTENIDO --->
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnRecaCtaCte" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnRecaTarj" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnExpo" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnAranId" runat="server"></asp:textbox><asp:textbox id="hdnListaId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		function mRedondear() 
		{
			if (document.all["cmbRedo"]!= null)
			{
				if (document.all["cmbRedo"].value == 'N')
					document.all["cmbRedoDeci"].style.visibility = 'hidden';
				else
					document.all["cmbRedoDeci"].style.visibility = '';
			}
	    }
	    mRedondear();
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["cmbActiv"]!= null)
			document.all["cmbActiv"].focus();
		
		if (document.all("hdnExpo").value!='')
		{
			var winArch = window.open(document.all("hdnExpo").value);
			document.all("hdnExpo").value='';
			__doPostBack('hdnExpo','')
		}
			
		function mCargarListaPreciosCopiar()
		{ 
		    var sFiltro = document.all("cmbActiv").value 		  		   		     
		    if (sFiltro != '')
		    {   		
		        LoadComboXML("precios_lista_copi_cargar", sFiltro, "cmbAranCopi", "S");
		        if (document.all('cmbAranCopi').value == '')
		        	document.frmABM.submit();
		    }
		    else
		    {
				LoadComboXML("precios_lista_copi_cargar", '0', "cmbAranCopi", "S");
				document.frmABM.submit();
		    }
	    }		
		</SCRIPT>
	</BODY>
</HTML>
