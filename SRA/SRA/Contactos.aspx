<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Contactos" CodeFile="Contactos.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Contactos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		
		function mSelecCP()
		  {
		    document.all["txtCpos"].value = '';
			document.all["cmbLocaAux"].value = document.all["cmbExclLoca"].value;
			document.all["txtCpos"].value = document.all["cmbLocaAux"].item(document.all["cmbLocaAux"].selectedIndex).text;
		  }
		
		function mCargarProvincias(pPais,pProvincia,pLocalidad,pLocalidadAux)
		{
		    var sFiltro = document.all(pPais).value;
		    LoadComboXML("provincias_cargar", sFiltro, pProvincia, "S");
		    LoadComboXML("localidades_cargar", "-1", pLocalidad, "N");
		    LoadComboXML("localidades_aux_cargar", "-1", pLocalidadAux, "N");
		}
		
		function mCargarLocalidades(pProvincia,pLocalidad,pLocalidadAux)
		{
		    var sFiltro = document.all(pProvincia).value;
		    if (sFiltro != '' && pProvincia=="cmbExclProv")
		       document.all["txtCpos"].value = "";
		    LoadComboXML("localidades_cargar", sFiltro, pLocalidad, "N");
		    LoadComboXML("localidades_aux_cargar", sFiltro, pLocalidadAux, "N");
		}		
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Contactos</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																	ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif"
																	ImageOver="btnLimp2.gif" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR height="10">
																		<TD background="imagenes/formfdofields.jpg" colSpan="3"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblApelFil" runat="server" cssclass="titulo">Nombre y apellido:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<CC1:TEXTBOXTAB id="txtApelFil" runat="server" cssclass="cuadrotexto" Width="250px"></CC1:TEXTBOXTAB>
																			<asp:checkbox id="chkBusc" Text="Buscar en..." Runat="server" CssClass="titulo"></asp:checkbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblActiFil" runat="server" cssclass="titulo">Actividad:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbActiFil" runat="server" Width="248px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblClasiFil" runat="server" cssclass="titulo">Clasificaci�n:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbClasiFil" runat="server" Width="248px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblNroRefFil" runat="server" cssclass="titulo">Nro. Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<CC1:numberbox id="txtNroRefFil" runat="server" cssclass="cuadrotexto" Width="96px"></CC1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 27.6%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 4.95%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 13.76%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<!---fin filtro --->
							<tr height="10">
								<td colSpan="3"></td>
							</tr>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="DataGrid_Page"
										OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
														<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="cotc_id" HeaderText="id"></asp:BoundColumn>
											<asp:BoundColumn DataField="cotc_nyap" HeaderText="Nomb. y Apel."></asp:BoundColumn>
											<asp:BoundColumn DataField="_acti_desc" HeaderText="Actividad"></asp:BoundColumn>
											<asp:BoundColumn DataField="_organismo" HeaderText="Organismo"></asp:BoundColumn>
											<asp:BoundColumn DataField="_clasificacion" HeaderText="Clasificaci&#243;n"></asp:BoundColumn>
											<asp:BoundColumn DataField="_pcia_desc" HeaderText="Provincia"></asp:BoundColumn>
											<asp:BoundColumn DataField="_loca_desc" HeaderText="Localidad"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle" colSpan="2"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
										IncludesUrl="includes/" BackColor="Transparent" ImageDisable="btnNuev0.gif" ToolTip="Agregar un Nuevo Contacto"></CC1:BOTONIMAGEN></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2"><asp:panel id="panSolapas" runat="server" width="100%" cssclass="titulo" Visible="False" Height="124%">
										<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
											<TR>
												<TD width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
												<TD background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp">
													<asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Contacto</asp:linkbutton></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp">
													<asp:linkbutton id="lnkTele" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" Height="21px" CausesValidation="False"> Telefonos</asp:linkbutton></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp">
													<asp:linkbutton id="lnkMail" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
														Width="70px" Height="21px" CausesValidation="False"> Mails</asp:linkbutton></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp">
													<asp:linkbutton id="lnkEstudios" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Estudios</asp:linkbutton></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp">
													<asp:linkbutton id="lnkExplotaciones" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Explotaciones</asp:linkbutton></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp">
													<asp:linkbutton id="lnkFunciones" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="70px" Height="21px" CausesValidation="False"> Cargos</asp:linkbutton></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
												<TD width="1" background="imagenes/tab_fondo.bmp">
													<asp:linkbutton id="lnkOcupaciones" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
														cssclass="solapa" Width="70px" Height="21px" CausesValidation="False"> Ocupaciones</asp:linkbutton></TD>
												<TD width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Visible="False" BorderWidth="1px"
											BorderStyle="Solid" Height="116px">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
													border="0">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
															height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE id="TablaCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 178px" vAlign="top" align="right">
																			<asp:Label id="lblTrat" runat="server" cssclass="titulo">Tratamiento:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<CC1:TEXTBOXTAB id="txtTrat" runat="server" cssclass="cuadrotexto" Width="192px" Height="22px" MaxLength="20"
																				EnterPorTab="False" Obligatorio="False"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px; HEIGHT: 32px" vAlign="top" align="right">
																			<asp:Label id="lblNyA" runat="server" cssclass="titulo">Nomb. y Apel.:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 32px">
																			<CC1:TEXTBOXTAB id="txtNyA" runat="server" cssclass="cuadrotexto" Width="100%" Height="30px" MaxLength="20"
																				EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" align="right">
																			<asp:Label id="lblActi" runat="server" cssclass="titulo">Actividad:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<cc1:combobox class="combo" id="cmbActi" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" align="right">
																			<asp:Label id="lblOrg" runat="server" cssclass="titulo">Organismo:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<cc1:combobox class="combo" id="cmbOrg" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" align="right">
																			<asp:Label id="lblClasi" runat="server" cssclass="titulo">Clasificaci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<cc1:combobox class="combo" id="cmbClasi" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" vAlign="top" align="right">
																			<asp:Label id="lblDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<CC1:TEXTBOXTAB id="txtDire" runat="server" cssclass="cuadrotexto" Width="100%" Height="50px" MaxLength="20"
																				EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" align="right">
																			<asp:Label id="lblExclPais" runat="server" cssclass="titulo">Pa�s:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<cc1:combobox class="combo" id="cmbExclPais" runat="server" Width="260px" onchange="mCargarProvincias('cmbExclPais','cmbExclPcia','cmbExclLoca','cmbLocaAux')"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" align="right">
																			<asp:Label id="lblExclPcia" runat="server" cssclass="titulo">Provincia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbExclPcia" runat="server" Width="260px" onchange="mCargarLocalidades('cmbExclPcia','cmbExclLoca','cmbLocaAux')"
																				NomOper="provincias_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" align="right">
																			<asp:Label id="lblExcLoca" runat="server" cssclass="titulo">Localidad:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbExclLoca" runat="server" Width="260px" onchange="mSelecCP();"
																				NomOper="localidades_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" align="right">
																			<asp:Label id="lblCpos" runat="server" cssclass="titulo">C�d. Postal:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<CC1:TEXTBOXTAB id="txtCpos" runat="server" cssclass="cuadrotexto" Width="88px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDocu" runat="server" cssclass="titulo">Tipo y Nro.Doc.:</asp:Label>&nbsp;</TD>
																		<TD align="left">
																			<cc1:combobox class="combo" id="cmbDoti" runat="server" Width="84px"></cc1:combobox>
																			<cc1:numberbox id="txtDocuNume" runat="server" cssclass="cuadrotexto" Width="112px" MaxValor="9999999999999"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblNaciFecha" runat="server" cssclass="titulo">Fecha Nac.:</asp:Label></TD>
																		<TD colSpan="2">
																			<TABLE style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
																				<TR>
																					<TD width="105">
																						<cc1:DateBox id="txtNaciFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																					<TD align="right" width="100">
																						<asp:Label id="lblIngrFecha" runat="server" cssclass="titulo">F. Ingreso:</asp:Label></TD>
																					<TD>
																						<cc1:DateBox id="txtIngrFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblDeud" runat="server" cssclass="titulo">Deuda:</asp:Label>&nbsp;</TD>
																		<TD noWrap>
																			<CC1:NUMBERBOX id="txtDeud" runat="server" cssclass="cuadrotexto" Width="75px" EsDecimal="True"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblAnteNume" runat="server" cssclass="titulo">Nro.Referencia:</asp:Label>&nbsp;</TD>
																		<TD noWrap>
																			<CC1:NUMBERBOX id="txtAnteNume" runat="server" cssclass="cuadrotexto" Width="75px"></CC1:NUMBERBOX></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" vAlign="top" align="right">
																			<asp:Label id="lbObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																		<TD>
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" Height="50px" MaxLength="20"
																				EnterPorTab="False" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 178px" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panTelefono" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TablaTelefono" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdTelefono" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				AutoGenerateColumns="False" OnPageIndexChanged="grdTele_PageChanged" OnEditCommand="mEditarDatosTele">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="tect_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_TipoTel" ReadOnly="True" HeaderText="Tipo Tel."></asp:BoundColumn>
																					<asp:BoundColumn DataField="tect_area_code" HeaderText="Cod. Area"></asp:BoundColumn>
																					<asp:BoundColumn DataField="tect_tele" HeaderText="Tel&#233;fono"></asp:BoundColumn>
																					<asp:BoundColumn DataField="tect_refe" HeaderText="Referencia"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="10"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTiTel" runat="server" cssclass="titulo">Tipo de tel.:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbTiTel" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblArea" runat="server" cssclass="titulo">Nro.de �rea:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<cc1:numberbox id="txtArea" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																				EsDecimal="True" height="18px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblTel" runat="server" cssclass="titulo">Tel�fono:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<CC1:TEXTBOXTAB id="txtTel" runat="server" cssclass="cuadrotexto" Width="296px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<CC1:TEXTBOXTAB id="txtRefe" runat="server" cssclass="cuadrotexto" Width="297px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnTelAlta" runat="server" cssclass="boton" Width="100px" Text="Agregar Tel."></asp:Button>&nbsp;
																			<asp:Button id="btnTelBaja" runat="server" cssclass="boton" Width="100px" Text="Eliminar Tel."></asp:Button>&nbsp;
																			<asp:Button id="btnTelModi" runat="server" cssclass="boton" Width="100px" Text="Modificar Tel."></asp:Button>&nbsp;
																			<asp:Button id="btnTelLimp" runat="server" cssclass="boton" Width="100px" Text="Limpiar Tel."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panMail" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TablaMail" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="left" colSpan="2">
																			<asp:datagrid id="grdMail" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False"
																				OnPageIndexChanged="grdMail_PageChanged" OnEditCommand="mEditarDatosMail">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="mact_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="mact_mail" HeaderText="Mail"></asp:BoundColumn>
																					<asp:BoundColumn DataField="mact_refe" HeaderText="Referencia"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="10"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblEmail" runat="server" cssclass="titulo">E-mail:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:textboxtab id="txtEmail" runat="server" cssclass="cuadrotexto" Width="250px" EsMail="True"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblMRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<CC1:TEXTBOXTAB id="txtMRefe" runat="server" cssclass="cuadrotexto" Width="296px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnMailAlta" runat="server" cssclass="boton" Width="100px" Text="Agregar Mail"></asp:Button>&nbsp;
																			<asp:Button id="btnMailBaja" runat="server" cssclass="boton" Width="100px" Text="Eliminar Mail"></asp:Button>&nbsp;
																			<asp:Button id="btnMailModi" runat="server" cssclass="boton" Width="100px" Text="Modificar Mail"></asp:Button>&nbsp;
																			<asp:Button id="btnMailLimp" runat="server" cssclass="boton" Width="100px" Text="Limpiar Mail"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panEstudios" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TablaEstudios" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdEstudios" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				AutoGenerateColumns="False" OnPageIndexChanged="grdEstu_PageChanged" OnEditCommand="mEditarDatosEstu">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="esct_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_EstuDesc" ReadOnly="True" HeaderText="Estudio"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="10"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblEstuEst" runat="server" cssclass="titulo">Estudio:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbEstuEst" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnEstuAlta" runat="server" cssclass="boton" Width="100px" Text="Agregar Est."></asp:Button>&nbsp;
																			<asp:Button id="btnEstuBaja" runat="server" cssclass="boton" Width="100px" Text="Eliminar Est."></asp:Button>&nbsp;
																			<asp:Button id="btnEstuModi" runat="server" cssclass="boton" Width="100px" Text="Modificar Est."></asp:Button>&nbsp;
																			<asp:Button id="btnEstuLimp" runat="server" cssclass="boton" Width="100px" Text="Limpiar Est."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panExplotaciones" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TablaExplotaciones" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdExplotaciones" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				AutoGenerateColumns="False" OnPageIndexChanged="grdExpl_PageChanged" OnEditCommand="mEditarDatosExpl">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton7" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="exct_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_ExplTipoDesc" HeaderText="Tipo"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_PciaDesc" HeaderText="Provincia"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_LocaDesc" HeaderText="Localidad"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="10"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblExplTipo" runat="server" cssclass="titulo">Tipo:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbExplTipo" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblExplPais" runat="server" cssclass="titulo">Pa�s:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 6px">
																			<cc1:combobox class="combo" id="cmbExplPais" runat="server" Width="260px" onchange="mCargarProvincias('cmbExplPais','cmbExplProv','cmbExplLoca','cmbExplLocaAux')"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblExplProv" runat="server" cssclass="titulo">Provincia:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbExplProv" runat="server" Width="260px" onchange="mCargarLocalidades('cmbExplProv','cmbExplLoca','cmbExplLocaAux')"
																				NomOper="provincias_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblExplLoca" runat="server" cssclass="titulo">Localidad:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbExplLoca" runat="server" Width="260px" NomOper="localidades_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblExplRefe" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 32px">
																			<CC1:TEXTBOXTAB id="txtExplRefe" runat="server" cssclass="cuadrotexto" Width="397px" Height="30px"
																				TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnExplAlta" runat="server" cssclass="boton" Width="100px" Text="Agregar Expl."></asp:Button>&nbsp;
																			<asp:Button id="btnExplBaja" runat="server" cssclass="boton" Width="100px" Text="Eliminar Expl."></asp:Button>&nbsp;
																			<asp:Button id="btnExplModi" runat="server" cssclass="boton" Width="100px" Text="Modificar Expl."></asp:Button>&nbsp;
																			<asp:Button id="btnExplLimp" runat="server" cssclass="boton" Width="100px" Text="Limpiar Expl."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panFunciones" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TablaFunciones" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="2">
																			<asp:datagrid id="grdFunciones" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				AutoGenerateColumns="False" OnPageIndexChanged="grdFunc_PageChanged" OnEditCommand="mEditarDatosFunc">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="fuct_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_FuncDesc" HeaderText="Cargo"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="10"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblFuncFunc" runat="server" cssclass="titulo">Cargo:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbFuncFunc" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnFuncAlta" runat="server" cssclass="boton" Width="100px" Text="Agregar Cargo"></asp:Button>&nbsp;
																			<asp:Button id="btnFuncBaja" runat="server" cssclass="boton" Width="100px" Text="Eliminar Cargo"></asp:Button>&nbsp;
																			<asp:Button id="btnFuncModi" runat="server" cssclass="boton" Width="100px" Text="Modificar Cargo"></asp:Button>&nbsp;
																			<asp:Button id="btnFuncLimp" runat="server" cssclass="boton" Width="100px" Text="Limpiar Cargo"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panOcupaciones" runat="server" cssclass="titulo" Width="100%" Visible="False">
																<TABLE id="TablaOcupaciones" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																	<TR>
																		<TD style="WIDTH: 100%" align="left" colSpan="2">
																			<asp:datagrid id="grdOcupaciones" runat="server" width="100%" BorderStyle="None" BorderWidth="1px"
																				AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																				AutoGenerateColumns="False" OnPageIndexChanged="grdOcup_PageChanged" OnEditCommand="mEditarDatosOcup">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="occt_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_OcupDesc" HeaderText="Ocupaci&#243;n"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="10"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD align="right">
																			<asp:Label id="lblOcupOcup" runat="server" cssclass="titulo">Ocupaci�n:</asp:Label>&nbsp;</TD>
																		<TD>
																			<cc1:combobox class="combo" id="cmbOcupOcup" runat="server" Width="260px"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD vAlign="bottom" colSpan="2" height="5"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																			height="2"></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" align="center" colSpan="2" height="30">
																			<asp:Button id="btnOcupAlta" runat="server" cssclass="boton" Width="100px" Text="Agregar Ocup."></asp:Button>&nbsp;
																			<asp:Button id="btnOcupBaja" runat="server" cssclass="boton" Width="100px" Text="Eliminar Ocup."></asp:Button>&nbsp;
																			<asp:Button id="btnOcupModi" runat="server" cssclass="boton" Width="100px" Text="Modificar Ocup."></asp:Button>&nbsp;
																			<asp:Button id="btnOcupLimp" runat="server" cssclass="boton" Width="100px" Text="Limpiar Ocup."></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center">
														<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
														<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnTeleId" runat="server"></asp:textbox><asp:textbox id="hdnMailId" runat="server"></asp:textbox><asp:textbox id="hdnEstuId" runat="server"></asp:textbox><asp:textbox id="hdnExplId" runat="server"></asp:textbox><asp:textbox id="hdnFuncId" runat="server"></asp:textbox><asp:textbox id="hdnOcupId" runat="server"></asp:textbox><cc1:combobox class="combo" id="cmbLocaAux" runat="server" AceptaNull="false"></cc1:combobox><cc1:combobox class="combo" id="cmbExplLocaAux" runat="server" AceptaNull="false"></cc1:combobox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
