Namespace SRA

Partial Class ComprobantesAnul
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents lblFechaIng As System.Web.UI.WebControls.Label
    Protected WithEvents lblFechaIngreso As System.Web.UI.WebControls.Label
    Protected WithEvents btnOtrosDatos As NixorControls.BotonImagen
    Protected WithEvents btnSigCabe As System.Web.UI.WebControls.ImageButton
    Protected WithEvents txtCemi As NixorControls.NumberBox
    Protected WithEvents lblAplic As System.Web.UI.WebControls.Label
    Protected WithEvents lblConcep As System.Web.UI.WebControls.Label
    Protected WithEvents lblVtos As System.Web.UI.WebControls.Label
    Protected WithEvents lblAutor As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipoPago As System.Web.UI.WebControls.Label
    Protected WithEvents txtTipoPago As NixorControls.TextBoxTab


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrCompId As String
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
    Private mstrTablaDetaAran As String = SRA_Neg.Constantes.gTab_ComprobAranc
    Private mstrTablaDetaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep
    Private mstrTablaVtos As String = SRA_Neg.Constantes.gTab_ComprobVtos
    Private mstrTablaSobreTasaVtos As String = SRA_Neg.Constantes.gTab_ComprobSobreTasaVtos ' Dario 2013-07-18
    Private mstrTablaAutor As String = SRA_Neg.Constantes.gTab_ComprobAutor

    Private mstrTipo As String
    Public mstrTitu As String

    Private Enum ColumnasFil As Integer
        lnkEdit = 0
        comp_id = 1
        fecha = 2
        tico_desc = 3
        comp_nro = 4
        cliente = 5
    End Enum
    Private Enum Columnas As Integer
        comp = 3
        activ = 5

    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mstrCompId = Request.QueryString("comp_id")

            mstrTitu = lblTituAbm.Text
            If mstrCompId <> "" Then
                mstrTitu = "Consulta de " + clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantesX", mstrCompId, "coti_desc").ToString
            Else
                If mstrTipo = "AC" Or mstrTipo = "AS" Then
                    mstrTitu = "Anulación de Aplicaciones"
                End If
            End If

            If Not Page.IsPostBack Then
                mEstablecerPerfil()
                mInicializar()
                mSetearEventos()
                mCargarCombos()

                If mstrCompId = "" Then
                    mConsultarBusc()
                Else
                    hdnId.Text = mstrCompId
                    mCargarDatos()
                End If
                clsWeb.gInicializarControles(sender, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mEstablecerPerfil()
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "return(mConfirmarBaja());")
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActiFil, "N", " @usua_id=" + Session("sUserId").ToString() + ",@factura = 1 ")
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "N", " @usua_id=" + Session("sUserId").ToString() + ",@factura = 1 ")
        clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbCoti, "", "@coti_ids='" & SRA_Neg.Constantes.ComprobTipos.Factura & "," & SRA_Neg.Constantes.ComprobTipos.ND & "," & SRA_Neg.Constantes.ComprobTipos.NC & "'")
        cmbCoti.Valor = CInt(SRA_Neg.Constantes.ComprobTipos.Factura)
    End Sub

    Public Sub mInicializar()
        Dim lstrParaPageSize As String
        clsSQLServer.gParaFieldConsul((mstrConn), lstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(lstrParaPageSize)

        mstrTipo = Request("tipo")

        If mstrCompId <> "" Then
            lblTituAbm.Visible = False
            btnBaja.Visible = False
        Else
            btnVista.Visible = False
        End If

        If mstrTipo = "AC" Or mstrTipo = "AS" Then
                txtFechaFil.Fecha = Today
                txtFechaFil.Enabled = False
        End If

        lblTituAbm.Text = mstrTitu
    End Sub
#End Region



#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultarBusc()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub


    Public Sub grdAran_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdAran.CurrentPageIndex = E.NewPageIndex
            mCargarDatos()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub
    Public Sub grdConcep_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdConcep.CurrentPageIndex = E.NewPageIndex
            mCargarDatos()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub
    Public Sub grdVtos_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdVtos.CurrentPageIndex = E.NewPageIndex
            mCargarDatos()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub
    Public Sub grdVtosRetrasados_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdVtosRetrasados.CurrentPageIndex = E.NewPageIndex
            mCargarDatos()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub
    Public Sub grdAutor_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdAutor.CurrentPageIndex = E.NewPageIndex
            mCargarDatos()

        Catch ex As Exception
            clsError.gManejarError(Me.Page, ex)
        End Try
    End Sub

    Public Sub mConsultarBusc()
        Dim lstrFiltros As New System.text.StringBuilder

        With lstrFiltros
            .Append("exec " + mstrTabla + "_busq")
            .Append(" @coti_id=")
            .Append(cmbCoti.Valor)
            .Append(", @clie_id=")
            .Append(usrClieFil.Valor.ToString())
            .Append(", @fecha=")
            .Append(clsFormatear.gFormatFecha2DB(txtFechaFil.Fecha))
            .Append(", @nro=")
            .Append(txtNroCompFil.Valor)
            .Append(", @comp_acti_id=")
            .Append(cmbActiFil.Valor)
            .Append(", @modu_id=")
            .Append(clsSQLServer.gFormatArg(SRA_Neg.Constantes.Modulos.Facturacion & "," & SRA_Neg.Constantes.Modulos.Alumnos_Inscripción & "," & SRA_Neg.Constantes.Modulos.Alumnos_FacturacionCuotas & "," & SRA_Neg.Constantes.Modulos.Alumnos_FacturacionManual & "," & SRA_Neg.Constantes.Modulos.Alumnos_Examen & "," & SRA_Neg.Constantes.Modulos.Facturación_Proformas, SqlDbType.VarChar))
            .Append(", @comp_ingr_fecha=")
            .Append(clsFormatear.gFormatFecha2DB(Date.Today))
            .Append(", @NO_cance=1")
            If Session("sCentroEmisorId") Is Nothing Then
                .Append(", @emct_id=-1")
            Else
                .Append(", @emct_id=")
                .Append(Session("sCentroEmisorId"))
            End If

            clsWeb.gCargarDataGrid(mstrConn, .ToString, grdDato)
        End With
    End Sub

    Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            hdnId.Text = e.Item.Cells(1).Text
            lblTitu.Text = e.Item.Cells(Columnas.comp).Text + " / " + e.Item.Cells(Columnas.activ).Text
            mCargarDatos()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

    Private Sub mCargarDatos()
        Dim ldsDatos As DataSet = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, hdnId.Text)

        With ldsDatos.Tables(mstrTabla).Rows(0)
            txtFechaComp.Fecha = .Item("comp_ingr_fecha")
            txtFechaValor.Fecha = .Item("comp_fecha")
            usrClie.Valor = .Item("comp_clie_id")
                txtNroComp.Valor = .Item("_comp_desc").ToString
                cmbActi.Valor = IIf(.Item("comp_acti_id") Is DBNull.Value, "", .Item("comp_acti_id"))
                txtNeto.Text = CDec(.Item("comp_neto")).ToString("#####0.00")

            mCargarListar(.Item("comp_coti_id"), hdnId.Text)
        End With

        If ldsDatos.Tables(mstrTablaDeta).Rows.Count > 0 Then
            With ldsDatos.Tables(mstrTablaDeta).Rows(0)
                txtCotDolar.Valor = .Item("code_coti")
                    txtObser.Valor = .Item("code_obse").ToString
                    txtNyap.Valor = .Item("code_reci_nyap").ToString

                If Not .IsNull("code_pers") Then
                    chkTPers.Checked = .Item("code_pers")
                End If

                If Not .IsNull("code_impo_ivat_tasa") Then
                    txtIva.Valor = CDec(.Item("code_impo_ivat_tasa") + .Item("code_impo_ivat_tasa_redu")).ToString("#####0.00")
                Else
                    txtIva.Valor = "0.00"
                End If
                If Not .IsNull("code_impo_ivat_tasa_sobre") Then
                    txtST.Valor = CDec(.Item("code_impo_ivat_tasa_sobre")).ToString("#####0.00")
                Else
                    txtST.Valor = "0.00"
                End If

                txtBruto.Valor = CDec(txtNeto.Valor - txtIva.Valor - txtST.Valor).ToString("#####0.00")
            End With
        End If

        grdAran.DataSource = ldsDatos.Tables(mstrTablaDetaAran)
        grdAran.DataBind()

        grdConcep.DataSource = ldsDatos.Tables(mstrTablaDetaConcep)
        grdConcep.DataBind()

        grdAutor.DataSource = ldsDatos.Tables(mstrTablaAutor)
        grdAutor.DataBind()

        grdVtos.DataSource = ldsDatos.Tables(mstrTablaVtos)
        grdVtos.DataBind()
        ' Dario 2013-07-18 grilla de vencimientos de la factura
        grdVtosRetrasados.DataSource = ldsDatos.Tables(mstrTablaSobreTasaVtos)
        grdVtosRetrasados.DataBind()

        mMostrarPanel(True)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        Dim lbooVisiOri As Boolean = panDato.Visible

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        If pbooVisi Then
            grdDato.DataSource = Nothing
        End If
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub mCerrar()
        Try
            If mstrCompId = "" Then
                mMostrarPanel(False)
            Else
                Response.Write("<Script>window.close();</script>")
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub mBaja()
        Try
            Dim lobjNeg As New SRA_Neg.FacturacionABM(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjNeg.Baja(hdnId.Text, hdnPrfrBaja.Text, hdnPrfrNume.Text)
            hdnPrfrBaja.Text = ""
            hdnPrfrNume.Text = ""
            mCerrar()
            mConsultarBusc()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarListar(ByVal pintCotiId As Integer, ByVal pstrId As String)
        Dim lstrRptName As String
        lstrRptName = "Factura"

        Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

        lstrRpt += "&comp_id=" + pstrId

        lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt, False)

        hdnPagina.Text = lstrRpt
    End Sub

    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            grdDato.CurrentPageIndex = 0
            mConsultarBusc()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub

    Private Sub mLimpiarFiltro()
        cmbCoti.Valor = CInt(SRA_Neg.Constantes.ComprobTipos.Factura)
        usrClieFil.Limpiar()
        txtFechaFil.Text = ""
        txtNroCompFil.Text = ""
        cmbActiFil.Limpiar()

        mConsultarBusc()
    End Sub

    Private Sub txtNroCompFil_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNroCompFil.TextChanged

    End Sub
End Class
End Namespace
