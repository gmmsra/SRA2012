<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>

<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Aranceles" CodeFile="Aranceles.aspx.vb" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<html>
<head>
    <title>Aranceles</title>
    <meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
    <meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
    <meta name="vs_defaultpilontScript" content="JavaScript">
    <meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
    <link rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
    <script language="JavaScript" src="includes/utiles.js"></script>
    <script language="JavaScript" src="includes/paneles.js"></script>
    <script language="javascript">
		
		function expandir()
		{
		 if(parent.frames.length>0) try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		
		function mHabilitarCombo(pDesa)
		{
		  document.all("cmbGravaTasa").disabled = pDesa;
          if(document.all("cmbGravaTasa").disabled)
			{
				document.all("cmbGravaTasa").value="";
				document.all("hdnHabComboIVA").value="1";
			}
		   else
			    document.all("hdnHabComboIVA").value="0";
		}
		
		function mHabilitaCuentaOpt()
		{
		if(document.all["cmbActividades"].value=="7")
			{
			document.all["txtcmbCuentaOpt"].disabled=false;
			document.all["cmbCuentaOpt"].disabled=false;
			}
		else
			{
			document.all["cmbCuentaOpt"].value="";
			document.all["txtcmbCuentaOpt"].value="";
			document.all["txtcmbCuentaOpt"].disabled=true;
			document.all["cmbCuentaOpt"].disabled=true;
			}
		
		if (document.all["cmbActividades"].value =="4")
			{
			document.all["cmbTipoServLabo"].disabled = false;
			document.all["cmbTipoServLabo"].value="";
			}
		else
			{
			document.all["cmbTipoServLabo"].disabled = true;
			document.all["cmbTipoServLabo"].value="";
			}			
		}
		
		
		
		
		function mMostrarDescripFormula()
		{
		 document.all["cmbFormAux"].value = document.all["cmbForm"].value;
		 if(document.all["cmbFormAux"].selectedIndex!=-1)
		    {
		     if (document.all["cmbFormAux"].selectedIndex!=0)
		      {
			   document.all["txtFormDesc"].value = document.all["cmbFormAux"].item(document.all["cmbFormAux"].selectedIndex).text;
			  }else
				{
				document.all["txtFormDesc"].value = '';
				}
			} 
		 }	 
    </script>
    <style type="text/css">
        .auto-style1 {
            height: 2px;
        }
    </style>
</head>
<body class="pagina" onload="gSetearTituloFrame('')" leftmargin="5" rightmargin="0" topmargin="5">
    <form id="frmABM" method="post" runat="server">
        <!------------------ RECUADRO ------------------->
        <table border="0" cellspacing="0" cellpadding="0" width="97%" align="center">
            <tr>
                <td width="9">
                    <img border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
                <td background="imagenes/recsup.jpg">
                    <img border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
                <td width="13">
                    <img border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
            </tr>
            <tr>
                <td style="width: 9px" background="imagenes/reciz.jpg" width="9">
                    <img border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
                <td valign="middle" align="center">
                    <!----- CONTENIDO ----->
                    <table style="width: 100%" id="Table1" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td></td>
                                <td width="100%" colspan="2"></td>
                            </tr>
                            <tr>
                                <td style="height: 25px" height="25" valign="bottom" colspan="3">
                                    <asp:Label ID="lblTituAbm" runat="server" CssClass="opcion">Aranceles</asp:Label></td>
                            </tr>
                            <!--- filtro --->
                            <tr>
                                <td valign="top" colspan="3">
                                    <asp:Panel ID="panFiltro" runat="server" CssClass="titulo" BorderWidth="0" BorderStyle="Solid"
                                        Width="100%" Visible="True">
                                        <table style="width: 100%" id="TableFil" border="0" cellspacing="0" cellpadding="0" align="left">
                                            <tr>
                                                <td>
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                                                        <tr>
                                                            <td style="height: 8px" width="24"></td>
                                                            <td style="height: 8px" width="42"></td>
                                                            <td style="height: 8px" width="26"></td>
                                                            <td style="height: 8px"></td>
                                                            <td style="height: 8px" width="26">
                                                                <cc1:BotonImagen ID="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
                                                                    ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                                                    IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></cc1:BotonImagen></td>
                                                            <td style="height: 8px" width="26">
                                                                <cc1:BotonImagen ID="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
                                                                    ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
                                                                    IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></cc1:BotonImagen></td>
                                                            <td style="height: 8px" width="10"></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="height: 8px" width="24">
                                                                <img border="0" src="imagenes/formfle.jpg" width="24" height="25"></td>
                                                            <td style="height: 8px" width="42">
                                                                <img border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></td>
                                                            <td style="height: 8px" width="26">
                                                                <img border="0" src="imagenes/formcap.jpg" width="26" height="25"></td>
                                                            <td style="height: 8px" background="imagenes/formfdocap.jpg" colspan="4">
                                                                <img border="0" src="imagenes/formfdocap.jpg" height="25"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 70px">
                                                    <table border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
                                                        <tr>
                                                            <td background="imagenes/formiz.jpg" width="3">
                                                                <img border="0" src="imagenes/formiz.jpg" width="3" height="30"></td>
                                                            <td style="height: 100%">
                                                                <!-- FOMULARIO -->
                                                                <table class="FdoFld" border="0" cellspacing="0" cellpadding="0" width="100%" height="100%">
                                                                    <tr>
                                                                        <td style="height: 10px" background="imagenes/formfdofields.jpg" colspan="2" align="right"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width: 20%; height: 17px" align="right">
                                                                            <asp:Label ID="lblCodigoDesdeFil" runat="server" CssClass="titulo">C�digo Desde:</asp:Label>&nbsp;</td>
                                                                        <td style="width: 80%">
                                                                            <cc1:TextBoxTab ID="txtCodigoDesdeFil" runat="server" CssClass="cuadrotexto" Width="160px"></cc1:TextBoxTab></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 17px" align="right">
                                                                            <asp:Label ID="Label2" runat="server" CssClass="titulo">C�digo Hasta:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:TextBoxTab ID="txtCodigoHastaFil" runat="server" CssClass="cuadrotexto" Width="160px"></cc1:TextBoxTab></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 17px" align="right">
                                                                            <asp:Label ID="lblDescripcionFil" runat="server" CssClass="titulo">Descripci�n:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:TextBoxTab ID="txtDescripcionFil" runat="server" CssClass="cuadrotexto" Width="350px"></cc1:TextBoxTab></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="2" background="imagenes/formdivmed.jpg" colspan="2" align="right">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 17px" align="right">
                                                                            <asp:Label ID="lblActividadesFil" runat="server" CssClass="titulo">Actividad:</asp:Label>&nbsp;</td>
                                                                        <td>
                                                                            <cc1:ComboBox ID="cmbActividadesFil" class="combo" runat="server" Width="328px" NomOper="actividades_cargar"
                                                                                AceptaNull="False">
                                                                            </cc1:ComboBox></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td background="imagenes/formdivmed.jpg" colspan="2" align="right" class="auto-style1">
                                                                            <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="height: 17px" align="right">
                                                                            <asp:Label ID="lblListaBotFil" runat="server" CssClass="titulo">Lista de precios boti:</asp:Label>
                                                                            &nbsp; </td>
                                                                        <td>

                                                                            <cc1:ComboBox ID="cmbListaBotFil" class="combo" runat="server" Width="100px" AceptaNull="False"></cc1:ComboBox>

                                                                        </td>

                                                                    </tr>

                                                                    <tr>
                                                                        <td style="height: 10px" background="imagenes/formfdofields.jpg" colspan="2" align="right"></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td style="height: 95px" background="imagenes/formde.jpg" width="2">
                                                                <img border="0" src="imagenes/formde.jpg" width="2" height="2"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <!---fin filtro --->
                            <tr>
                                <td height="10" colspan="3"></td>
                            </tr>
                            <tr>
                                <td valign="top" colspan="3" align="center">
                                    <asp:DataGrid ID="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" Height="100%" Width="100%"
                                        AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
                                        AutoGenerateColumns="False" PageSize="15">
                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                        <HeaderStyle Height="10px" CssClass="header"></HeaderStyle>
                                        <FooterStyle CssClass="footer"></FooterStyle>
                                        <Columns>
                                            <asp:TemplateColumn>
                                                <HeaderStyle Width="2%"></HeaderStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:BoundColumn Visible="False" DataField="aran_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="aran_codi" HeaderText="C�digo">
                                                <HeaderStyle Width="5%"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="aran_desc" HeaderText="Descripci�n"></asp:BoundColumn>
                                            <asp:BoundColumn Visible="False" DataField="aran_acti_id" HeaderText="aran_acti_id"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="_acti_desc" HeaderText="Actividad">
                                                <HeaderStyle Width="40%"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="_estado" HeaderText="Estado">
                                                <HeaderStyle Width="5%"></HeaderStyle>
                                            </asp:BoundColumn>
                                        </Columns>
                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                    </asp:DataGrid></td>
                            </tr>
                            <tr>
                                <td height="10" colspan="3"></td>
                            </tr>
                            <tr>
                                <td style="height: 9px" height="9"></td>
                                <td style="height: 9px" height="9" valign="middle">
                                    <cc1:BotonImagen ID="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
                                        ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
                                        ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Nuevo Arancel" ImageDisable="btnNuev0.gif"></cc1:BotonImagen></td>
                                <td align="right">
                                    <cc1:BotonImagen ID="btnImprimir" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
                                        ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
                                        BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Listar" ImageDisable="btnImpr0.gif"></cc1:BotonImagen></td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <asp:Panel ID="panSolapas" runat="server" CssClass="titulo" Visible="False" Height="124%" Width="100%">
                                        <table id="TablaSolapas" border="0" cellspacing="0" cellpadding="0" width="100%" runat="server">
                                            <tr>
                                                <td style="width: 0.24%">
                                                    <img border="0" src="imagenes/tab_a.bmp" width="9" height="27"></td>
                                                <td>
                                                    <img border="0" src="imagenes/tab_b.bmp" width="8" height="27"></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_c.bmp" width="31" height="27"></td>
                                                <td background="imagenes/tab_fondo.bmp" width="1">
                                                    <asp:LinkButton Style="text-align: center; text-decoration: none" ID="lnkGeneral" runat="server"
                                                        CssClass="solapa" Width="80px" Height="21px" CausesValidation="False" Font-Bold="True"> General</asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_f.bmp" width="27" height="27"></td>
                                                <td background="imagenes/tab_fondo.bmp" width="1">
                                                    <asp:LinkButton Style="text-align: center; text-decoration: none" ID="lnkRRGG" runat="server" CssClass="solapa"
                                                        Width="70px" Height="21px" CausesValidation="False"> RRGG</asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_f.bmp" width="27" height="27"></td>
                                                <td background="imagenes/tab_fondo.bmp" width="1">
                                                    <asp:LinkButton Style="text-align: center; text-decoration: none" ID="lnkRangos" runat="server"
                                                        CssClass="solapa" Width="70px" Height="21px" CausesValidation="False"> Rangos</asp:LinkButton></td>
                                                <td width="1">
                                                    <img border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" colspan="3" align="center">
                                    <asp:Panel ID="panDato" runat="server" CssClass="titulo" BorderWidth="1px" BorderStyle="Solid"
                                        Visible="False" Height="116px" Width="100%">
                                        <table style="width: 100%; height: 106px" id="Table2" class="FdoFld" border="0" cellpadding="0"
                                            align="left">
                                            <tr>
                                                <td height="5">
                                                    <asp:Label ID="lblTitu" runat="server" CssClass="titulo" Width="100%"></asp:Label></td>
                                                <td valign="top" align="right">&nbsp;
														<asp:ImageButton ID="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%" colspan="2">
                                                    <asp:Panel ID="panGeneral" runat="server" CssClass="titulo" Width="100%" Height="562px">
                                                        <table style="width: 100%" id="TablaGeneral" border="0" cellpadding="0" align="left">
                                                            <tr>
                                                                <td style="width: 260px; height: 16px" align="right">
                                                                    <asp:Label ID="lblCodigo" runat="server" CssClass="titulo">C�digo:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <cc1:TextBoxTab ID="txtCodigo" runat="server" CssClass="cuadrotexto" Width="160px" Obligatorio="true"></cc1:TextBoxTab></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 16px" align="right">
                                                                    <asp:Label ID="lblDescripcion" runat="server" CssClass="titulo">Descripci�n:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <cc1:TextBoxTab ID="txtDescripcion" runat="server" CssClass="cuadrotexto" Width="350px" Obligatorio="true"></cc1:TextBoxTab></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 12px" align="right">
                                                                    <asp:Label ID="lblActividades" runat="server" CssClass="titulo">Actividad:</asp:Label>&nbsp;</td>
                                                                <td style="height: 12px" colspan="4">
                                                                    <cc1:ComboBox ID="cmbActividades" class="combo" runat="server" Width="328px" NomOper="actividades_cargar"
                                                                        Obligatorio="True" onchange="mHabilitaCuentaOpt()">
                                                                    </cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 1px" height="1" background="imagenes/formdivmed.jpg" colspan="5"
                                                                    align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 12px" align="right">
                                                                    <asp:Label ID="lblGrupoAran" runat="server" CssClass="titulo">Grupo Arancel:</asp:Label>&nbsp;</td>
                                                                <td style="height: 12px" colspan="4">
                                                                    <cc1:ComboBox ID="cmbGrupoAran" class="combo" runat="server" Width="328px" NomOper="grupo_aranceles_cargar"
                                                                        Obligatorio="True">
                                                                    </cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="height: 1px" height="1" background="imagenes/formdivmed.jpg" colspan="5"
                                                                    align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 16px" align="right">
                                                                    <asp:Label ID="lblCuenta" runat="server" CssClass="titulo">Cuenta:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <cc1:ComboBox ID="cmbCuenta" class="combo" runat="server" CssClass="cuadrotexto" Width="280px"
                                                                                    Height="20px" Obligatorio="True" onchange="javascript:;" TextMaxLength="7" MostrarBotones="False"
                                                                                    NomOper="cuentas_ctables_cargar" Filtra="true">
                                                                                </cc1:ComboBox></td>
                                                                            <td>
                                                                                <img style="border-bottom: thin outset; border-left: thin outset; border-top: thin outset; cursor: hand; border-right: thin outset"
                                                                                    id="btnAvanBusq" onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCuenta','Cuentas Contables','[4,5]');"
                                                                                    border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 9px" align="right">
                                                                    <asp:Label ID="lblCuentaOpt" runat="server" CssClass="titulo">Cuenta Optativa:</asp:Label>&nbsp;</td>
                                                                <td style="width: 344px; height: 9px">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <cc1:ComboBox ID="cmbCuentaOpt" class="combo" runat="server" CssClass="cuadrotexto" Width="280px"
                                                                                    Height="20px" onchange="javascript:;" TextMaxLength="7" MostrarBotones="False" NomOper="cuentas_ctables_cargar"
                                                                                    Filtra="true">
                                                                                </cc1:ComboBox></td>
                                                                            <td>
                                                                                <img style="border-bottom: thin outset; border-left: thin outset; border-top: thin outset; cursor: hand; border-right: thin outset"
                                                                                    id="btnAvanBusq" onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCuentaOpt','Cuentas Contables','[2132]');"
                                                                                    border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td style="height: 9px" colspan="3">
                                                                    <asp:Label ID="Label1" runat="server" CssClass="titulo">(Exposiciones y Per�odos)</asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 16px" align="right">
                                                                    <asp:Label ID="lblCentroCosto" runat="server" CssClass="titulo">Centro de Costo (R+):</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <cc1:ComboBox ID="cmbCentroCosto" class="cuadrotexto" runat="server" CssClass="combo" Width="300px"
                                                                                    Obligatorio="True" TextMaxLength="6" MostrarBotones="False" NomOper="centrosc_cargar" Filtra="true">
                                                                                </cc1:ComboBox></td>
                                                                            <td>
                                                                                <img style="border-bottom: thin outset; border-left: thin outset; border-top: thin outset; cursor: hand; border-right: thin outset"
                                                                                    id="btnAvanBusq" onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCentroCosto','Centros de Costo','@cuenta=4');"
                                                                                    border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 17px" align="right">
                                                                    <asp:Label ID="lblCentroCostoNega" runat="server" CssClass="titulo">Centro de Costo (R-):</asp:Label>&nbsp;</td>
                                                                <td style="height: 17px" colspan="4">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <cc1:ComboBox ID="cmbCentroCostoNega" class="cuadrotexto" runat="server" CssClass="combo" Width="300px"
                                                                                    Obligatorio="True" TextMaxLength="6" MostrarBotones="False" NomOper="centrosc_cargar" Filtra="true">
                                                                                </cc1:ComboBox></td>
                                                                            <td>
                                                                                <img style="border-bottom: thin outset; border-left: thin outset; border-top: thin outset; cursor: hand; border-right: thin outset"
                                                                                    id="btnAvanBusq" onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCentroCostoNega','Centros de Costo','@cuenta=5');"
                                                                                    border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 15px" align="right">
                                                                    <asp:Label ID="lblTipoServLabo" runat="server" CssClass="titulo">Tipo de Servicio:</asp:Label>&nbsp;</td>
                                                                <td style="height: 15px" colspan="4">
                                                                    <cc1:ComboBox ID="cmbTipoServLabo" class="combo" runat="server" Width="328px" NomOper="servicios_tipos_labo_cargar"></cc1:ComboBox>
                                                                    <asp:Label ID="Label3" runat="server" CssClass="titulo">(Laboratorio)</asp:Label></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px" align="right">
                                                                    <asp:Label ID="lblClienteGenerico" runat="server" CssClass="titulo" Width="161px">Acepta Cliente Gen�rico:</asp:Label>&nbsp;</td>
                                                                <td colspan="4">
                                                                    <cc1:ComboBox ID="cmbClienteGenerico" class="combo" runat="server" Width="50px" Obligatorio="true"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px" align="right">
                                                                    <asp:Label ID="lblFacturacionExenta" runat="server" CssClass="titulo">Acepta Facturaci�n Exenta:</asp:Label>&nbsp;</td>
                                                                <td colspan="4">
                                                                    <cc1:ComboBox ID="cmbFacturacionExenta" class="combo" runat="server" Width="50px" Obligatorio="true"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px" align="right">
                                                                    <asp:Label ID="lblCantidadAnimales" runat="server" CssClass="titulo">Cantidad Representa Animales:</asp:Label>&nbsp;
                                                                </td>
                                                                <td colspan="4">
                                                                    <cc1:ComboBox ID="cmbCantidadAnimales" class="combo" runat="server" Width="50px" Obligatorio="true"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px" align="right">
                                                                    <asp:Label ID="lblCalcBonifLab" runat="server" CssClass="titulo">Calcula Bonificaci�n Laboratorio:</asp:Label>&nbsp;
                                                                </td>
                                                                <td colspan="4">
                                                                    <cc1:ComboBox ID="cmbCalcBonifLab" class="combo" runat="server" Width="50px" Obligatorio="true"></cc1:ComboBox></td>
                                                                <tr>
                                                                    <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                        <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                                </tr>
                                                            <tr>
                                                                <td style="width: 260px; " align="right">
                                                                    <asp:Label ID="lblListaParaBot" runat="server" CssClass="titulo">Lista de precios para bot:</asp:Label>&nbsp; </td>
                                                                <td colspan="4">
                                                                    <cc1:ComboBox ID="cmbListaBot" runat="server" class="combo" Obligatorio="true" Width="50px">
                                                                    </cc1:ComboBox>
                                                                </td>
                                                            </tr>
                                                                <tr>
                                                                    <td align="right" style="width: 260px; height: 16px">
                                                                        <asp:Label ID="lblIVA" runat="server" CssClass="titulo" Width="43px">I.V.A.:</asp:Label>
                                                                        &nbsp;</td>
                                                                    <td colspan="4" style="height: 16px">
                                                                        <asp:RadioButton ID="rbtGrav" runat="server" Checked="True" CssClass="titulo" GroupName="RadioGroup1" onclick=" mHabilitarCombo(1)" Text="No gravado" />
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="lblIIBBGrava" runat="server" CssClass="titulo">Percepci�n IIBB:</asp:Label>
                                                                        &nbsp;
                                                                        <asp:CheckBox ID="chkIIBBGrava" runat="server" Checked="false" CssClass="titulo" Text="Gravado" />
                                                                    </td>
                                                                </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 16px" align="right"></td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <asp:RadioButton ID="rbtIva" onclick=" mHabilitarCombo(0)" runat="server" CssClass="titulo" GroupName="RadioGroup1"
                                                                        Text="Seleccionar"></asp:RadioButton>&nbsp;
																		<cc1:ComboBox ID="cmbGravaTasa" class="combo" runat="server" Width="160px" CampoVal="Tipo de Tasa"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px" valign="top" align="right">
                                                                    <asp:Label ID="lblValFecha" runat="server" CssClass="titulo">Validar Fecha Acta:</asp:Label></td>
                                                                <td colspan="5">
                                                                    <cc1:TextBoxTab ID="txtValFecha" runat="server" CssClass="cuadrotexto" Width="70px"></cc1:TextBoxTab></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px; height: 19px" valign="top" align="right"></td>
                                                                <td style="height: 19px" colspan="5">
                                                                    <asp:CheckBox ID="chkControl" Checked="false" Text="Control" runat="server" CssClass="titulo"></asp:CheckBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 260px" valign="top" align="right">
                                                                    <asp:Label ID="lblForm" runat="server" CssClass="titulo">F�rmula:</asp:Label>&nbsp;</td>
                                                                <td colspan="5">
                                                                    <table width="100%" height="100%">
                                                                        <tr>
                                                                            <td style="width: 250px" valign="top">
                                                                                <cc1:ComboBox ID="cmbForm" class="combo" runat="server" Width="264px" Obligatorio="True" onchange=" mMostrarDescripFormula();"></cc1:ComboBox></td>
                                                                            <td valign="top">
                                                                                <cc1:TextBoxTab ID="txtFormDesc" runat="server" CssClass="textolibre" Width="240px" Height="46px"
                                                                                    Rows="2" TextMode="MultiLine" ReadOnly="True"></cc1:TextBoxTab></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%" colspan="2">
                                                    <asp:Panel ID="panRRGG" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                                        <table style="width: 100%" id="TablaRRGG" border="0" cellpadding="0" align="left">
                                                            <tr>
                                                                <td style="width: 235px; height: 16px" align="right">
                                                                    <asp:Label ID="lblDescFecha" runat="server" CssClass="titulo">Fecha:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <cc1:TextBoxTab ID="txtDescFecha" runat="server" CssClass="cuadrotexto" Width="350px"></cc1:TextBoxTab></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 235px; height: 16px" align="right">
                                                                    <asp:Label ID="lblTipoServicio" runat="server" CssClass="titulo">Tipo de Servicio:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <cc1:ComboBox ID="cmbTipoServicio" class="combo" runat="server" Width="250px" NomOper="servicios_tipos_rrgg"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 235px; height: 16px" valign="middle" align="right">
                                                                    <asp:Label ID="lblRaza" runat="server" CssClass="titulo">Raza:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" valign="middle" colspan="4">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <cc1:ComboBox ID="cmbRaza" class="combo" runat="server" CssClass="cuadrotexto" Width="280px" Height="20px"
                                                                                    MostrarBotones="False" NomOper="razas_cargar" Filtra="true">
                                                                                </cc1:ComboBox></td>
                                                                            <td>
                                                                                <img style="border-bottom: thin outset; border-left: thin outset; border-top: thin outset; cursor: hand; border-right: thin outset"
                                                                                    id="btnAvanBusq" onclick="mBotonBusquedaAvanzada('razas','raza_desc','cmbRaza','Razas','');"
                                                                                    border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 235px; height: 16px" align="right">
                                                                    <asp:Label ID="lblValoresValidacion" runat="server" CssClass="titulo">Valores para Validaci�n:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <asp:Label ID="lblValMinValidacion" runat="server" CssClass="titulo">M�nimo:</asp:Label>&nbsp;
																		<cc1:NumberBox ID="txtValMinValidacion" runat="server" CssClass="cuadrotexto" Width="56px"></cc1:NumberBox>&nbsp;
																		<asp:Label ID="lblValMaxValidacion" runat="server" CssClass="titulo">M�ximo:</asp:Label>&nbsp;
																		<cc1:NumberBox ID="txtValMaxValidacion" runat="server" CssClass="cuadrotexto" Width="56px"></cc1:NumberBox>&nbsp;
																		<asp:Label ID="lblPeriodoValidacion" runat="server" CssClass="titulo">Per�odo:</asp:Label>&nbsp;
																		<cc1:ComboBox ID="cmbPeriodoValidacion" class="combo" runat="server" Width="85px" CampoVal="Periodo"></cc1:ComboBox>
                                                                    <asp:CheckBox ID="chkError" Checked="false" Text="Error" runat="server" CssClass="titulo"></asp:CheckBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 235px; height: 16px" align="right">
                                                                    <asp:Label ID="lblValoresCalculo" runat="server" CssClass="titulo">Valores para C�lculo:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <asp:Label ID="lblValMinCalculo" runat="server" CssClass="titulo">M�nimo:</asp:Label>&nbsp;
																		<cc1:NumberBox ID="txtValMinCalculo" runat="server" CssClass="cuadrotexto" Width="56px"></cc1:NumberBox>&nbsp;
																		<asp:Label ID="lblValMaxCalculo" runat="server" CssClass="titulo">M�ximo:</asp:Label>&nbsp;
																		<cc1:NumberBox ID="txtValMaxCalculo" runat="server" CssClass="cuadrotexto" Width="56px"></cc1:NumberBox>&nbsp;
																		<asp:Label ID="lblPeriodoCalculo" runat="server" CssClass="titulo">Per�odo:</asp:Label>&nbsp;
																		<cc1:ComboBox ID="cmbPeriodoCalculo" class="combo" runat="server" Width="85px" CampoVal="Periodo"></cc1:ComboBox>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 235px; height: 16px" align="right">
                                                                    <asp:Label ID="lblCalculaMeses" runat="server" CssClass="titulo">Calcula en Meses:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <cc1:ComboBox ID="cmbCalculaMeses" class="combo" runat="server" Width="56px"></cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 235px; height: 16px" align="right">
                                                                    <asp:Label ID="lblSobretasaArancel" runat="server" CssClass="titulo">Sobretasa del Arancel:</asp:Label>&nbsp;</td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <table border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <cc1:ComboBox ID="cmbSobretasaArancel" class="combo" runat="server" Width="300px" NomOper="aranceles"
                                                                                    onchange="javascript:;">
                                                                                </cc1:ComboBox></td>
                                                                            <td>
                                                                                <img style="border-bottom: thin outset; border-left: thin outset; border-top: thin outset; cursor: hand; border-right: thin outset"
                                                                                    id="btnAvanBusq" onclick="mBotonBusquedaAvanzada('aranceles','aran_desc','cmbSobretasaArancel','Aranceles','');"
                                                                                    border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif">
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 235px; height: 16px" align="right"></td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <asp:CheckBox ID="chkTieneRango" Checked="false" Text="Tiene Rango" runat="server" CssClass="titulo"></asp:CheckBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 235px; height: 16px" align="right"></td>
                                                                <td style="height: 16px" colspan="4">
                                                                    <asp:CheckBox ID="chkImprimeRemitos" Checked="false" Text="Imprime en Remitos" runat="server"
                                                                        CssClass="titulo"></asp:CheckBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td height="2" background="imagenes/formdivmed.jpg" colspan="5" align="right">
                                                                    <img src="imagenes/formdivmed.jpg" width="1" height="2"></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 100%" colspan="2">
                                                    <asp:Panel ID="panRangos" runat="server" CssClass="titulo" Width="100%" Visible="False">
                                                        <table style="width: 100%" id="TablaRangos" border="0" cellpadding="0" align="left">
                                                            <tr>
                                                                <td style="height: 12px" width="20%" align="right">
                                                                    <asp:Label ID="lblRango" runat="server" CssClass="titulo">Rango:</asp:Label>&nbsp;</td>
                                                                <td style="height: 12px">
                                                                    <cc1:ComboBox ID="cmbRango" class="combo" runat="server" Width="328px" NomOper="aranceles_rangos_cargar"
                                                                        AutoPostBack="True">
                                                                    </cc1:ComboBox></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td valign="top" colspan="3" align="center">
                                                                    <asp:DataGrid ID="grdRangos" runat="server" BorderWidth="1px" BorderStyle="None" Width="100%"
                                                                        AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
                                                                        OnPageIndexChanged="grdRangos_Page" AutoGenerateColumns="False" PageSize="20">
                                                                        <FooterStyle CssClass="footer"></FooterStyle>
                                                                        <SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
                                                                        <EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
                                                                        <AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
                                                                        <ItemStyle Height="5px" CssClass="item2"></ItemStyle>
                                                                        <HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
                                                                        <Columns>
                                                                            <asp:TemplateColumn>
                                                                                <HeaderStyle Width="2%"></HeaderStyle>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn Visible="False" DataField="raad_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="raad_desde" ReadOnly="True" HeaderText="Desde mes">
                                                                                <HeaderStyle Width="25%"></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="raad_hasta" ReadOnly="True" HeaderText="Hasta mes">
                                                                                <HeaderStyle Width="25%"></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="raad_tope_fecha" ReadOnly="True" HeaderText="Fecha tope" DataFormatString="{0:dd/MM/yyyy}">
                                                                                <HeaderStyle Width="25%"></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="raad_sobre_fecha" ReadOnly="True" HeaderText="Fecha sobretasa" DataFormatString="{0:dd/MM/yyyy}">
                                                                                <HeaderStyle Width="25%"></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                        </Columns>
                                                                        <PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
                                                                    </asp:DataGrid></td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <asp:Panel ID="panBotones" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td align="center">
                                                    <asp:Label ID="lblBaja" runat="server" CssClass="titulo" ForeColor="Red"></asp:Label></td>
                                            </tr>
                                            <tr height="30">
                                                <td align="center"><a id="editar" name="editar"></a>
                                                    <asp:Button ID="btnAlta" runat="server" CssClass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
														<asp:Button ID="btnBaja" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Baja"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnModi" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Modificar"></asp:Button>&nbsp;&nbsp;
														<asp:Button ID="btnLimp" runat="server" CssClass="boton" Width="80px" CausesValidation="False"
                                                            Text="Limpiar"></asp:Button>&nbsp;&nbsp;&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                    <div></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <!--- FIN CONTENIDO --->
                </td>
                <td background="imagenes/recde.jpg" width="13">
                    <img border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
            </tr>
            <tr>
                <td style="width: 9px" width="9">
                    <img border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
                <td background="imagenes/recinf.jpg">
                    <img border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
                <td width="13">
                    <img border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
            </tr>
        </table>
        <!----------------- FIN RECUADRO ----------------->
        <br>
        <div style="display: none">
            <asp:TextBox ID="lblMens" runat="server" Width="140px"></asp:TextBox><asp:TextBox ID="hdnId" runat="server"></asp:TextBox><asp:TextBox ID="hdnRangoId" runat="server"></asp:TextBox><asp:TextBox ID="hdnHabComboIVA" runat="server">1</asp:TextBox><asp:TextBox ID="hdnDatosPop" runat="server"></asp:TextBox><asp:TextBox ID="hdnValoresRangos" runat="server"></asp:TextBox><cc1:ComboBox ID="cmbFormAux" class="combo" runat="server" Width="232px"></cc1:ComboBox>
        </div>
    </form>
    <script language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();
    </script>
    </TR></TBODY></TABLE>
</body>
</html>
