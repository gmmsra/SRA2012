Imports ReglasValida.Validaciones


Namespace SRA

Partial Class ProductosCalif
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub




    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "productos_calif"
    Private mstrCmd As String
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mEstablecerPerfil()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                usrProductoFil.cmbProdRazaExt.Valor = cmbRazaFil.Valor
                usrProductoFil.cmbProdRazaExt.Enabled = (cmbRazaFil.Valor.ToString = "")

                usrProducto.cmbProdRazaExt.Valor = cmbRazaFil.Valor
                usrProducto.cmbProdRazaExt.Enabled = (cmbRazaFil.Valor.ToString = "")
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean

        'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
        '   Response.Redirect("noaccess.aspx")
        'End If

        'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
        'btnAlta.Visible = lbooPermiAlta

        'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

        'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
        'btnModi.Visible = lbooPermiModi

        'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
        'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

        txtCalificacion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "pcal_cali")
        txtObservaciones.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "pcal_refe")
    End Sub
    Public Sub mInicializar()
        usrCriadorFil.Criador = True
        usrCriadorFil.AutoPostback = False
        usrCriadorFil.FilClaveUnica = False
        usrCriadorFil.ColClaveUnica = True
        usrCriadorFil.Ancho = 790
        usrCriadorFil.Alto = 510
        usrCriadorFil.ColDocuNume = False
        usrCriadorFil.ColCUIT = True
        usrCriadorFil.ColCriaNume = True
        usrCriadorFil.FilCUIT = True
        usrCriadorFil.FilDocuNume = True
        usrCriadorFil.FilAgru = False
        usrCriadorFil.FilTarjNume = False

        usrProductoFil.AutoPostback = False
        usrProductoFil.Ancho = 790
        usrProductoFil.Alto = 510

        usrProducto.AutoPostback = False
        usrProducto.Ancho = 790
        usrProducto.Alto = 510

    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_cali=1", cmbRazaFil, "id", "descrip_codi", "T")
        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul "
            mstrCmd = mstrCmd + "@prdt_cria_id=" + IIf(usrCriadorFil.Valor.ToString = "", "0", usrCriadorFil.Valor.ToString)
            mstrCmd = mstrCmd + ",@prdt_raza_id=" + IIf(cmbRazaFil.Valor.ToString = "", "0", cmbRazaFil.Valor.ToString)
            mstrCmd = mstrCmd + ",@pcal_prdt_id=" + IIf(usrProductoFil.Valor.ToString = "", "0", usrProductoFil.Valor.ToString)
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, E.Item.Cells(1).Text)
        ldsEsta.Tables(0).TableName = mstrTabla

        With ldsEsta.Tables(0).Rows(0)
            hdnId.Text = .Item("pcal_id")
            usrProducto.Valor = .Item("pcal_prdt_id")
            txtFecha.Fecha = .Item("pcal_fecha")
            txtCalificacion.Valor = .Item("pcal_cali")
            txtObservaciones.Valor = .Item("pcal_refe")
        End With

        mSetearEditor(False)
        mMostrarPanel(True)
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        usrProducto.Limpiar()
        usrProducto.cmbProdRazaExt.Valor = cmbRazaFil.Valor
        usrProducto.cmbProdRazaExt.Enabled = (cmbRazaFil.Valor.ToString = "")

        txtFecha.Text = ""
        txtCalificacion.Text = ""
        txtObservaciones.Text = ""
        lblBaja.Text = ""

        grdDato.CurrentPageIndex = 0
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarFiltro()
        usrCriadorFil.Limpiar()
        cmbRazaFil.Limpiar()
        usrProductoFil.Limpiar()
        usrProductoFil.cmbProdRazaExt.Valor = cmbRazaFil.Valor
        usrProductoFil.cmbProdRazaExt.Enabled = (cmbRazaFil.Valor.ToString = "")
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        mEstablecerPerfil()
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
            lobjGenerica.Alta()

            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
            lobjGenerica.Modi()
            mProcesar()
            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
    Private Function mGuardarDatos() As DataSet
        Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
        Dim lintCpo As Integer

        mValidarDatos()

        With ldsDatos.Tables(0).Rows(0)
            .Item("pcal_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("pcal_prdt_id") = usrProducto.Valor
            .Item("pcal_fecha") = txtFecha.Fecha
            .Item("pcal_cali") = txtCalificacion.Valor
            .Item("pcal_refe") = txtObservaciones.Valor
        End With
        Return ldsDatos
    End Function
#End Region

#Region "Eventos de Controles"
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
#End Region

    Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
        Try
            Dim lstrRptName As String

            lstrRptName = "ProductosCalif"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&prdt_cria_id=" + IIf(usrCriadorFil.Valor.ToString = "", "0", usrCriadorFil.Valor.ToString)
            lstrRpt += "&prdt_raza_id=" + IIf(cmbRazaFil.Valor.ToString = "", "0", cmbRazaFil.Valor.ToString)
            lstrRpt += "&pcal_prdt_id=" + IIf(usrProductoFil.Valor.ToString = "", "0", usrProductoFil.Valor.ToString)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mProcesar()
        Try
            Dim lstrMsgError As String
            Dim lstrProdId As String
            Dim lstrRazaId As String
            Dim lstrSexo As String
            Dim lstrId As String


            lstrProdId = hdnId.Text
            lstrRazaId = clsSQLServer.gObtenerValorCampo(mstrConn, "productos", "@prdt_id=" & lstrProdId, "prdt_raza_id")
            lstrSexo = usrProducto.Sexo
            'IIf(.IsNull("trad_manu"), True, .Item("trad_manu"))
            Dim pdinid As Object = clsSQLServer.gObtenerValorCampo(mstrConn, "productos", "@prdt_id=" & lstrProdId, "prdt_pdin_id")
            lstrId = IIf(pdinid Is System.DBNull.Value, "-1", pdinid)

            If (lstrId <> "-1" And usrProducto.txtSraNumeExt.Text = "") Then
                'Limpiar errores anteriores.
                ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, lstrId)

                'Aplicar Reglas.
                lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, lstrId, lstrRazaId, lstrSexo, Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), 0, 0)
            End If
            mConsultar()
            mMostrarPanel(False)

            If lstrMsgError <> "" Then
                AccesoBD.clsError.gGenerarMensajes(Me, lstrMsgError)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
