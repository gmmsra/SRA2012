Namespace SRA

Partial Class MesasVota
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents btnBusc As System.Web.UI.WebControls.ImageButton


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_MesasVota
   Private mstrCategorias As String = SRA_Neg.Constantes.gTab_MesasVotaCate

   Private mstrConn As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet


   Private Enum Columnas As Integer
      Id = 1
      SoinId = 2
      SocmId = 3
      chkSel = 8
      cmbResu = 9
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearEventos()

            mCargarCombos()

            clsWeb.gInicializarControles(Me, mstrConn)

            mMostrarPanel(False)
         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnAlta.Attributes.Add("onclick", "if(!confirm('Confirma la generación de Mesas de Votación?')) return false;")
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja de las Mesas de Votación?')) return false;")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsam, "S", "@FechaFin = 'S'")
      mConsultar()
   End Sub
#End Region

#Region "Generacion"
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatosGene()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla + "_proceso", ldsEstruc)
         lobjGenerico.Alta()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         clsSQLServer.gExecute(mstrConn, mstrTabla + "_proceso_baja " + cmbAsam.Valor.ToString)

         grdDato.CurrentPageIndex = 0

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatosGene() As DataSet
      Dim ldsEsta As DataSet = CrearDataSetGene()

      With ldsEsta.Tables(0).Rows(0)
         .Item("mevo_asam_id") = cmbAsam.Valor
      End With

      Return ldsEsta
   End Function

   Private Function CrearDataSetGene() As DataSet
      Dim ldsEsta As New DataSet
      ldsEsta.Tables.Add(mstrTabla & "_proceso")
      With ldsEsta.Tables(0)
         .Columns.Add("mevo_id", System.Type.GetType("System.Int32"))
         .Columns.Add("mevo_asam_id", System.Type.GetType("System.Int32"))
         .Columns.Add("mevo_audi_user", System.Type.GetType("System.Int32"))
         .Rows.Add(.NewRow)
      End With
      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         Dim lbooTieneOrvo As Boolean

         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @mevo_asam_id=")

         If Not cmbAsam.Valor Is DBNull.Value Then
            lstrCmd.Append(cmbAsam.Valor.ToString)
         Else
            lstrCmd.Append("-1")
         End If

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         For Each oDr As DataRow In DirectCast(grdDato.DataSource, DataSet).Tables(0).Rows
            lbooTieneOrvo = lbooTieneOrvo Or oDr.Item("_cant_vota") > 0
         Next

         If grdDato.Items.Count = 0 Then
            btnAlta.Text = "Generar Mesas"
         Else
            btnAlta.Text = "Re-Generar Mesas"
         End If

         If lbooTieneOrvo Then
            lblOrvo.Text = "Las mesas ya tienen asignadas un orden de votación"
            grdCate.Columns(0).Visible = False
            lblOrvo.Visible = True
         Else
            lblOrvo.Text = ""
            grdCate.Columns(0).Visible = True
            lblOrvo.Visible = False
         End If

         btnAlta.Enabled = Not cmbAsam.Valor Is DBNull.Value And Not lbooTieneOrvo
         btnBaja.Enabled = Not cmbAsam.Valor Is DBNull.Value And grdDato.Items.Count > 0 And Not lbooTieneOrvo

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub cmbAsam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAsam.SelectedIndexChanged
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
#End Region

#Region "Modificacion"

#Region "Seteo de Controles"

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla

         Case mstrCategorias
            btnBajaCate.Enabled = Not (pbooAlta) And lblOrvo.Text = ""
            btnModiCate.Enabled = Not (pbooAlta) And lblOrvo.Text = ""
            btnAltaCate.Enabled = pbooAlta And lblOrvo.Text = ""
         Case Else
            btnModi.Enabled = Not (pbooAlta) And lblOrvo.Text = ""
      End Select
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mLimpiar()
         mLimpiarCate()

         hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)

         mCrearDataSet(hdnId.Text)

         If mdsDatos.Tables(mstrTabla).Rows.Count > 0 Then
            With mdsDatos.Tables(mstrTabla).Rows(0)
               hdnId.Text = .Item("mevo_id")
               txtNum.Text = .Item("mevo_nume")
               txtDesde.Text = .Item("mevo_desde")
               txtHasta.Text = .Item("mevo_hasta")
            End With
            mSetearEditor("", False)
            mMostrarPanel(True)
            mShowTabs(1)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtNum.Text = ""
      txtDesde.Text = ""
      txtHasta.Text = ""

      mLimpiarCate()

      lblTitu.Text = ""

      grdCate.CurrentPageIndex = 0

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi

      btnAlta.Visible = Not panDato.Visible
      btnBaja.Visible = Not panDato.Visible
      panFiltro.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      lnkCabecera.Font.Bold = True
      lnkCate.Font.Bold = False

      panCabecera.Visible = True
      panCate.Visible = False

      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True
      lnkCate.Font.Bold = False

      If Not pbooVisi Then
         mdsDatos = Nothing
         Session(mstrTabla) = Nothing
      End If
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True
      panCabecera.Visible = False
      panCate.Visible = False



      lnkCabecera.Font.Bold = False
      lnkCate.Font.Bold = False

      Dim val As String

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Text = "Datos de mesa: " & txtNum.Text
         Case 2
            clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCateC, "S", "@cate_vota=1")
            panCate.Visible = True
            lnkCate.Font.Bold = True
            lblTitu.Text = "Categorias de mesa: " & txtNum.Text
            grdCate.Visible = True
            btnBajaCate.Enabled = False And lblOrvo.Text = ""
            btnModiCate.Enabled = False And lblOrvo.Text = ""
            btnAltaCate.Enabled = True And lblOrvo.Text = ""
      End Select
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         lobjGenericoRel.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      With mdsDatos.Tables(mstrTabla).Rows(0)
         .Item("mevo_nume") = txtNum.Text
         .Item("mevo_desde") = txtDesde.Text
         .Item("mevo_hasta") = txtHasta.Text
      End With
      Return mdsDatos
   End Function

   Public Sub mCrearDataSet(ByVal pstrId As String)
      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrCategorias

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      mConsultarCate()

      Session(mstrTabla) = mdsDatos
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
#End Region

#Region "Detalle"
   Public Sub mEditarDatosCate(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrCate As DataRow
         hdnMecaId.Text = E.Item.Cells(1).Text
         ldrCate = mdsDatos.Tables(mstrCategorias).Select("meca_id=" & hdnMecaId.Text)(0)

         With ldrCate
            cmbCateC.Valor = .Item("meca_cate_id")
         End With

         mSetearEditor(mstrCategorias, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarCategoria()

      Dim ldrCate As DataRow

      If hdnMecaId.Text = "" Then
         If mdsDatos.Tables(mstrCategorias).Select("meca_cate_id=" & cmbCateC.Valor & " AND meca_id <> " & clsSQLServer.gFormatArg(hdnMecaId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
            Throw New AccesoBD.clsErrNeg("La mesa ya tiene la categoria seleccionada.")
         End If

         ldrCate = mdsDatos.Tables(mstrCategorias).NewRow
         ldrCate.Item("meca_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrCategorias), "meca_id")
      Else
         ldrCate = mdsDatos.Tables(mstrCategorias).Select("meca_id=" & hdnMecaId.Text)(0)
      End If

      If (cmbCateC.SelectedItem.Text = "(Seleccione)") Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la categoria.")
      End If

      With ldrCate
         .Item("meca_cate_id") = cmbCateC.Valor
         .Item("_cate_desc") = cmbCateC.SelectedItem.Text
      End With

      If (hdnMecaId.Text = "") Then
         mdsDatos.Tables(mstrCategorias).Rows.Add(ldrCate)
      End If
   End Sub

   Private Sub mLimpiarCate()
      hdnMecaId.Text = ""
      cmbCateC.Limpiar()
      mSetearEditor(mstrCategorias, True)
   End Sub

   Public Sub grdCate_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCate.EditItemIndex = -1
         If (grdCate.CurrentPageIndex < 0 Or grdCate.CurrentPageIndex >= grdCate.PageCount) Then
            grdCate.CurrentPageIndex = 0
         Else
            grdCate.CurrentPageIndex = E.NewPageIndex
         End If

         mConsultarCate()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaCate.Click
      ActualizarCategorias()
   End Sub

   Private Sub btnModiCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCate.Click
      ActualizarCategorias()
   End Sub

   Private Sub btnBajaCate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaCate.Click
      Try
         mdsDatos.Tables(mstrCategorias).Select("meca_id=" & hdnMecaId.Text)(0).Delete()

         mConsultarCate()

         mLimpiarCate()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub ActualizarCategorias()
      Try
         mGuardarCategoria()

         mLimpiarCate()
         mConsultarCate()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConsultarCate()
      grdCate.DataSource = mdsDatos.Tables(mstrCategorias)
      grdCate.DataBind()
   End Sub

   Private Sub lnkCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCate.Click
      mShowTabs(2)
   End Sub

#End Region
#End Region
End Class
End Namespace
