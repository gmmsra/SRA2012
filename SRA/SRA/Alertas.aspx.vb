Namespace SRA

Partial Class Alertas
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Alertas
   Private mstrTablaHorarios As String = SRA_Neg.Constantes.gTab_AlertasHorarios
   Private mstrTablaUsuarios As String = SRA_Neg.Constantes.gTab_AlertasUsuarios
   Private mstrTablaLog As String = SRA_Neg.Constantes.gTab_AlertasLog

   Private mstrConn As String
   Private mstrParaPageSize As Integer

   Private mdsDatos As DataSet
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            mSetearMaxLength()
            mSetearEventos()

            mCargarCombos()

            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)

            mSetearDiaHora()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "alertas_tipos", cmbAltiFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "alertas_tipos", cmbAlti, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "estados", cmbEsta, "", SRA_Neg.Constantes.EstaTipos.EstaTipo_Alertas)
      clsWeb.gCargarRefeCmb(mstrConn, "dias", cmbDia, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "usuarios", cmbUsua, "S")
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLong As Object

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
   End Sub

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      Try
         mAgregar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrTablaHorarios
            btnBajaAlho.Enabled = Not (pbooAlta)
            btnModiAlho.Enabled = Not (pbooAlta)
            btnAltaAlho.Enabled = pbooAlta
         Case mstrTablaUsuarios
            btnBajaAlus.Enabled = Not (pbooAlta)
            btnModiAlus.Enabled = Not (pbooAlta)
            btnAltaAlus.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnProc.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      hdnId.Text = pstrId
      mCrearDataSet(pstrId)

      With mdsDatos.Tables(mstrTabla).Rows(0)
         cmbAlti.Valor = .Item("aler_alti_id")
         txtDesc.Valor = .Item("aler_desc")
         cmbEsta.Valor = .Item("aler_esta_id")

         If Not .IsNull("aler_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("aler_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With

      lblTitu.Text = "Registro Seleccionado: " + txtDesc.Text

      mSetearEditor("", False)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""
      txtDesc.Text = ""
      cmbAlti.Limpiar()
      cmbEsta.Valor = CType(SRA_Neg.Constantes.Estados.Alertas_vigente, String)
      lblBaja.Text = ""

      grdAlho.CurrentPageIndex = 0
      grdAlus.CurrentPageIndex = 0
      grdLog.CurrentPageIndex = 0

      mCrearDataSet("")
      mLimpiarAlho()
      mLimpiarAlus()
      mLimpiarLog()

      mSetearEditor("", True)
      mShowTabs(1)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      Dim lbooVisiOri As Boolean = panDato.Visible

      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible
      tabLinks.Visible = pbooVisi
      btnList.Visible = Not pbooVisi

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()

         mShowTabs(1)
      Else
         Session(mstrTabla) = Nothing
         mdsDatos = Nothing
      End If
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      Try
         panDato.Visible = True
         panBotones.Visible = True
         panAlho.Visible = False
         panAlus.Visible = False
         panLog.Visible = False
         panCabecera.Visible = False

         lnkCabecera.Font.Bold = False
         lnkAlho.Font.Bold = False
         lnkAlus.Font.Bold = False
         lnkLog.Font.Bold = False


         Select Case origen
            Case 1
               panCabecera.Visible = True
               lnkCabecera.Font.Bold = True
               lblTitu.Text = "Datos de la Alerta"
            Case 2
               panAlho.Visible = True
               lnkAlho.Font.Bold = True
               lblTitu.Text = "Horarios de la Alerta: " & txtDesc.Text
            Case 3
               panAlus.Visible = True
               lnkAlus.Font.Bold = True
               lblTitu.Text = "Usuarios de la Alerta: " & txtDesc.Text
            Case 4
               panLog.Visible = True
               lnkLog.Font.Bold = True
               lblTitu.Text = "Historial de la Alerta: " & txtDesc.Text
         End Select


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

   Private Sub lnkAlho_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAlho.Click
      mShowTabs(2)
   End Sub

   Private Sub lnkAlus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAlus.Click
      mShowTabs(3)
   End Sub

   Private Sub lnkLog_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLog.Click
      mShowTabs(4)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         lobjGenericoRel.Alta()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         mGuardarDatos()

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         lobjGenericoRel.Modi()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenericoRel.Baja(hdnId.Text)

         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatos()
      mValidarDatos()

      With mdsDatos.Tables(mstrTabla).Rows(0)
         .Item("aler_id") = clsSQLServer.gFormatArg(Me.hdnId.Text, SqlDbType.Int)
         .Item("aler_alti_id") = cmbAlti.Valor
         .Item("aler_desc") = txtDesc.Text
         .Item("aler_esta_id") = cmbEsta.Valor
         .Item("aler_baja_fecha") = DBNull.Value
      End With
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaHorarios
      mdsDatos.Tables(2).TableName = mstrTablaUsuarios

      mConsultarLog()
      mConsultarAlho()
      mConsultarAlus()

      Session(mstrTabla) = mdsDatos
   End Sub

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub btnProc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProc.Click
      mProcesar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @aler_alti_id=" + cmbAltiFil.Valor.ToString)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "Alertas"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			'lstrRpt += "&fkvalor=" + Request.QueryString("fkvalor")

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mProcesar()
      Try
         SRA_Neg.Alertas.EnviarMailAlertas(mstrConn, Session("sUserId").ToString(), hdnId.Text)
         mConsultarLog()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Horarios"
   Public Sub mEditarDatosAlho(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrAlho As DataRow

         hdnAlhoId.Text = E.Item.Cells(1).Text
         ldrAlho = mdsDatos.Tables(mstrTablaHorarios).Select("alho_id=" & hdnAlhoId.Text)(0)

         With ldrAlho
            chkManual.Checked = .Item("alho_manual")

            mSetearDiaHora()

                    cmbDia.Valor = .Item("alho_dia").ToString
                    txtHora.Valor = .Item("alho_hora").ToString
         End With

         mSetearEventos()
         mSetearEditor(mstrTablaHorarios, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdAlho_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdAlho.EditItemIndex = -1
         If (grdAlho.CurrentPageIndex < 0 Or grdAlho.CurrentPageIndex >= grdAlho.PageCount) Then
            grdAlho.CurrentPageIndex = 0
         Else
            grdAlho.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarAlho()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarAlho()
      Try
         mGuardarDatosAlho()

         mLimpiarAlho()
         mConsultarAlho()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosAlho()
      Dim ldrDatos As DataRow

      If txtHora.Enabled And txtHora.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la hora")
      End If

      If hdnAlhoId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrTablaHorarios).NewRow
         ldrDatos.Item("alho_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaHorarios), "alho_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrTablaHorarios).Select("alho_id=" & hdnAlhoId.Text)(0)
      End If

      With ldrDatos
         .Item("alho_dia") = cmbDia.Valor
         .Item("alho_hora") = txtHora.Valor
         .Item("alho_manual") = chkManual.Checked

         .Item("_dia") = cmbDia.SelectedItem.Text
      End With

      If hdnAlhoId.Text = "" Then
         mdsDatos.Tables(mstrTablaHorarios).Rows.Add(ldrDatos)
      End If
   End Sub

   Private Sub mLimpiarAlho()
      hdnAlhoId.Text = ""
        cmbDia.Limpiar()
        'cmbDia.SelectedIndex = 0
        txtHora.Text = ""
        chkManual.Checked = False

        'mSetearDiaHora()

      mSetearEditor(mstrTablaHorarios, True)
   End Sub

   Private Sub btnLimpAlho_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpAlho.Click
      mLimpiarAlho()
   End Sub

   Private Sub btnBajaAlho_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaAlho.Click
      Try
         mdsDatos.Tables(mstrTablaHorarios).Select("alho_id=" & hdnAlhoId.Text)(0).Delete()
         mConsultarAlho()
         mLimpiarAlho()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaAlho_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaAlho.Click
      mActualizarAlho()
   End Sub

   Private Sub btnModiAlho_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiAlho.Click
      mActualizarAlho()
   End Sub

   Public Sub mConsultarAlho()
      grdAlho.DataSource = mdsDatos.Tables(mstrTablaHorarios)
      grdAlho.DataBind()
   End Sub

   Private Sub mSetearDiaHora()
      cmbDia.Enabled = Not chkManual.Checked
      txtHora.Enabled = Not chkManual.Checked

      If Not cmbDia.Enabled Then
         cmbDia.Limpiar()
         txtHora.Text = ""
      Else
         If Not Request.Form(cmbDia.UniqueID) Is Nothing Then
            cmbDia.Valor = Request.Form(cmbDia.UniqueID)
         End If

         If Not Request.Form(txtHora.UniqueID) Is Nothing Then
            txtHora.Text = Request.Form(txtHora.UniqueID)
         End If
      End If
   End Sub
#End Region

#Region "Usuarios"
   Public Sub mEditarDatosAlus(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrAlus As DataRow

         hdnAlusId.Text = E.Item.Cells(1).Text
         ldrAlus = mdsDatos.Tables(mstrTablaUsuarios).Select("alus_id=" & hdnAlusId.Text)(0)

         With ldrAlus
            cmbUsua.Valor = .Item("alus_usua_id")
         End With

         mSetearEditor(mstrTablaUsuarios, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdAlus_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdAlus.EditItemIndex = -1
         If (grdAlus.CurrentPageIndex < 0 Or grdAlus.CurrentPageIndex >= grdAlus.PageCount) Then
            grdAlus.CurrentPageIndex = 0
         Else
            grdAlus.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarAlus()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarAlus()
      Try
         mGuardarDatosAlus()

         mLimpiarAlus()
         mConsultarAlus()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosAlus()
      Dim ldrDatos As DataRow

      If cmbUsua.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el usuario")
      End If

      If mdsDatos.Tables(mstrTablaUsuarios).Select("alus_usua_id=" & cmbUsua.Valor & " AND alus_id <> " & clsSQLServer.gFormatArg(hdnAlusId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
         Throw New AccesoBD.clsErrNeg("El usuario ya pertenece a la alerta.")
      End If

      If hdnAlusId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrTablaUsuarios).NewRow
         ldrDatos.Item("alus_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaUsuarios), "alus_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrTablaUsuarios).Select("alus_id=" & hdnAlusId.Text)(0)
      End If

      With ldrDatos
         .Item("alus_usua_id") = cmbUsua.Valor
         .Item("_usua_apel") = cmbUsua.SelectedItem.Text
      End With

      If hdnAlusId.Text = "" Then
         mdsDatos.Tables(mstrTablaUsuarios).Rows.Add(ldrDatos)
      End If
   End Sub

   Private Sub mLimpiarAlus()
      hdnAlusId.Text = ""
      cmbUsua.Limpiar()

      mSetearEditor(mstrTablaUsuarios, True)
   End Sub

   Private Sub btnLimpAlus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpAlus.Click
      mLimpiarAlus()
   End Sub

   Private Sub btnBajaAlus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaAlus.Click
      Try
         mdsDatos.Tables(mstrTablaUsuarios).Select("alus_id=" & hdnAlusId.Text)(0).Delete()
         mConsultarAlus()
         mLimpiarAlus()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaAlus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaAlus.Click
      mActualizarAlus()
   End Sub

   Private Sub btnModiAlus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiAlus.Click
      mActualizarAlus()
   End Sub

   Public Sub mConsultarAlus()
      grdAlus.DataSource = mdsDatos.Tables(mstrTablaUsuarios)
      grdAlus.DataBind()
   End Sub
#End Region

#Region "Logs"
   Public Sub mEditarDatosLog(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
                txtError.Valor = clsSQLServer.gObtenerEstruc(mstrConn, mstrTablaLog, E.Item.Cells(1).Text).Tables(0).Rows(0).Item("allo_error").ToString

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub grdLog_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdLog.EditItemIndex = -1
         If (grdLog.CurrentPageIndex < 0 Or grdLog.CurrentPageIndex >= grdLog.PageCount) Then
            grdLog.CurrentPageIndex = 0
         Else
            grdLog.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarLog()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiarLog()
      txtError.Text = ""
   End Sub

   Public Sub mConsultarLog()
      Dim lstrCmd As New StringBuilder
      lstrCmd.Append("exec " + mstrTablaLog + "_consul")
      lstrCmd.Append(" @aler_id=" + clsSQLServer.gFormatArg(Me.hdnId.Text, SqlDbType.Int))

      clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdLog)
   End Sub
#End Region
End Class
End Namespace
