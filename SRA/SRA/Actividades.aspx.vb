Namespace SRA

Partial Class Actividades
    Inherits FormGenerico

    ' Dario 2013-07-02 Cambio proforma factura
    ' Dario 2013-07-08 Cambio vtos a plazos 
    ' Dario 2013-07-18 Cambio pagos parciales @acti_PermitePagoParciales


#Region " Web Form Designer Generated Code "
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblSimulaVtosAPlazos As System.Web.UI.WebControls.Label
    Protected WithEvents cmbSimulaVtosAPlazos As NixorControls.ComboBox
    Protected WithEvents cmblblPermitePagoParciales As NixorControls.ComboBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Actividades

   Private mstrConn As String
   Private mstrParaPageSize As Integer

   Private Enum Columnas As Integer
      Id = 0
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mSetearMaxLength()
            mCargarCombos()

            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", cmbCuct, "S")
        clsWeb.gCargarComboBool(cmbFact, "S")
        clsWeb.gCargarComboBool(cmbAdhe, "S")
        clsWeb.gCargarComboBool(cmbGeneInte, "S")
        clsWeb.gCargarComboBool(cmbDiscriminaPrecioUnitario, "S")
        ' Dario 2013-07-02 Cambio proforma factura y vtos a plazos 
        clsWeb.gCargarComboBool(cmbAplicaSobreTasaPagoRetrasado, "S")
        clsWeb.gCargarComboBool(cmbAdmiteProforma, "S")
        clsWeb.gCargarComboBool(cmbPermitePagoEnCuotas, "S")
        ' fin 2013-07-02
        ' Dario 2013-07-18 Cambio proforma factura y vtos a plazos 
        clsWeb.gCargarComboBool(cmbPermitePagoParciales, "S")
        ' fin 2013-07-18
        clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta='4'", cmbCentroCosto, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta='5'", cmbCentroCostoNega, "id", "descrip", "S")
        clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta='4'", cmbCentroCostoMora, "id", "descrip", "S")
   End Sub

 Private Sub mSetearEventos()
  btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
 End Sub
 Private Sub mSetearMaxLength()
  Dim lstrLong As Object

  lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
  txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "acti_desc")
  txtInteMonto.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "acti_inte_monto")
  txtIntePorc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "acti_inte_porc")
  txtInteMoraPorc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "acti_mora_inte_porc")
  txtVctoCantDias.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "acti_vcto_cant_dias")
  txtVctoFijoDia.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "acti_vcto_fijo_dia")
 End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, ByVal pbooSiste As Boolean)
      btnBaja.Enabled = Not pbooAlta And Not pbooSiste
      btnModi.Enabled = Not pbooAlta
      btnAlta.Enabled = pbooAlta
   End Sub
   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim lbooSiste As Boolean
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)

      With ldsEsta.Tables(0).Rows(0)
            hdnId.Text = .Item("acti_id").ToString()
            txtNroActi.Valor = .Item("acti_id")
            txtDesc.Valor = .Item("acti_desc")
            cmbCuct.Valor = .Item("acti_cuct_id")
			cmbCentroCosto.Valor = .Item("acti_ccos_id")
			cmbCentroCostoNega.Valor = .Item("acti_nega_ccos_id")
			cmbCentroCostoMora.Valor = .Item("acti_mora_ccos_id")
            cmbFact.ValorBool = .Item("acti_fact")
            cmbAdhe.ValorBool = .Item("acti_adhe")
            cmbGeneInte.ValorBool = .Item("acti_gene_inte")
            txtInteMonto.Valor = .Item("acti_inte_monto")
            txtIntePorc.Valor = .Item("acti_inte_porc")
            txtInteMoraPorc.Valor = .Item("acti_mora_inte_porc")
            txtVctoCantDias.Valor = .Item("acti_vcto_cant_dias")
            txtVctoFijoDia.Valor = .Item("acti_vcto_fijo_dia")
            lbooSiste = Not .IsNull("acti_siste") AndAlso .Item("acti_siste")
            cmbDiscriminaPrecioUnitario.ValorBool = .Item("acti_discriminapreciounitario")
            ' Dario cambio del dia 2013-07-02
            cmbAplicaSobreTasaPagoRetrasado.ValorBool = .Item("acti_AplicaSobreTasaPagoRetrasado")
            cmbAdmiteProforma.ValorBool = .Item("acti_AdmiteProforma")
            cmbPermitePagoEnCuotas.ValorBool = .Item("acti_PermitePagoEnCuotas")
            ' fin 2013-07-02
            ' Dario cambio del dia 2013-07-18
            cmbPermitePagoParciales.ValorBool = .Item("acti_PermitePagoParciales")
            ' fin 2013-07-18
            ' Dario cambio del dia 2013-07-08
            txtVcto2CantDias.Valor = .Item("acti_vcto2_cant_dias")
            txtVcto3CantDias.Valor = .Item("acti_vcto3_cant_dias")
            txtVcto4CantDias.Valor = .Item("acti_vcto4_cant_dias")
            txtVcto5CantDias.Valor = .Item("acti_vcto5_cant_dias")
            txtVcto6CantDias.Valor = .Item("acti_vcto6_cant_dias")
            txtVcto7CantDias.Valor = .Item("acti_vcto7_cant_dias")
            txtVcto8CantDias.Valor = .Item("acti_vcto8_cant_dias")
            ' fin 2013-07-08

         If Not .IsNull("acti_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("acti_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If

         If lbooSiste Then
            lblSiste.Text = "ACTIVIDAD DEL SISTEMA"
         Else
            lblSiste.Text = ""
         End If

         trMora.Visible = (CInt(hdnId.Text) = SRA_Neg.Constantes.Actividades.Socios)
      End With

      lblTitu.Text = "Registro Seleccionado: " + txtDesc.Valor.ToString

      mSetearEditor("", False, lbooSiste)
      mMostrarPanel(True)
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnModi.Enabled = False
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
        hdnId.Text = ""
        lblTitu.Text = ""

        txtDesc.Text = ""
        cmbCuct.Limpiar()
        cmbFact.Limpiar()
        cmbAdhe.Limpiar()
		cmbCentroCosto.Limpiar()
		cmbCentroCostoNega.Limpiar()
		cmbCentroCostoMora.Limpiar()
        cmbGeneInte.Limpiar()
        cmbDiscriminaPrecioUnitario.Limpiar()
        ' Dario cambio del dia 2013-07-02
        cmbAplicaSobreTasaPagoRetrasado.Limpiar()
        cmbAdmiteProforma.Limpiar()
        cmbPermitePagoEnCuotas.Limpiar()
        ' fin 2013-07-02
        ' Dario cambio del dia 2013-07-18
        cmbPermitePagoParciales.Limpiar()
        ' fin 2013-07-18
        txtInteMonto.Text = ""
        txtIntePorc.Text = ""
        txtInteMoraPorc.Text = ""
        txtVctoCantDias.Text = "0"
        ' Dario cambio del dia 2013-07-08
        txtVcto2CantDias.Text = "0"
        txtVcto3CantDias.Text = "0"
        txtVcto4CantDias.Text = "0"
        txtVcto5CantDias.Text = "0"
        txtVcto6CantDias.Text = "0"
        txtVcto7CantDias.Text = "0"
        txtVcto8CantDias.Text = "0"
        ' fin 2013-07-02
        txtVctoFijoDia.Text = ""
        lblBaja.Text = ""
        lblSiste.Text = ""

      trMora.Visible = False

      mSetearEditor("", True, False)
   End Sub
   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim lstrId As String
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lstrId = lobjGenerico.Alta()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = CrearDataSet(hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
            .Item("acti_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("acti_desc") = txtDesc.Valor
            .Item("acti_cuct_id") = cmbCuct.Valor
			.Item("acti_ccos_id") = cmbCentroCosto.Valor
			.Item("acti_nega_ccos_id") = cmbCentroCostoNega.Valor
			.Item("acti_mora_ccos_id") = cmbCentroCostoMora.Valor
            .Item("acti_fact") = cmbFact.ValorBool
            .Item("acti_adhe") = cmbAdhe.ValorBool
            .Item("acti_gene_inte") = cmbGeneInte.ValorBool
            .Item("acti_inte_porc") = txtIntePorc.Valor
            .Item("acti_mora_inte_porc") = txtInteMoraPorc.Valor
            .Item("acti_inte_monto") = txtInteMonto.Valor
            .Item("acti_vcto_fijo_dia") = txtVctoFijoDia.Valor
            .Item("acti_vcto_cant_dias") = txtVctoCantDias.Valor
            .Item("acti_discriminapreciounitario") = cmbDiscriminaPrecioUnitario.ValorBool
            ' Dario cambio del dia 2013-07-02
            .Item("acti_AplicaSobreTasaPagoRetrasado") = cmbAplicaSobreTasaPagoRetrasado.ValorBool
            .Item("acti_AdmiteProforma") = cmbAdmiteProforma.ValorBool
            .Item("acti_PermitePagoEnCuotas") = cmbPermitePagoEnCuotas.ValorBool
            ' fin 2013-07-02
            ' Dario cambio del dia 2013-07-18
            .Item("acti_PermitePagoParciales") = cmbPermitePagoParciales.ValorBool
            ' fin 2013-07-18
            ' Dario cambio del dia 2013-07-08
            .Item("acti_vcto2_cant_dias") = txtVcto2CantDias.Valor
            .Item("acti_vcto3_cant_dias") = txtVcto3CantDias.Valor
            .Item("acti_vcto4_cant_dias") = txtVcto4CantDias.Valor
            .Item("acti_vcto5_cant_dias") = txtVcto5CantDias.Valor
            .Item("acti_vcto6_cant_dias") = txtVcto6CantDias.Valor
            .Item("acti_vcto7_cant_dias") = txtVcto7CantDias.Valor
            .Item("acti_vcto8_cant_dias") = txtVcto8CantDias.Valor
            ' fin 2013-07-08
            .Item("acti_baja_fecha") = DBNull.Value
        End With
        Return ldsEsta
   End Function
   Private Function CrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      ldsEsta.Tables(0).TableName = mstrTabla

      Return ldsEsta
   End Function
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
      If cmbCuct.Valor.ToString = "" And hdnId.Text <> CType(SRA_Neg.Constantes.Actividades.Socios, String) Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar una Cuenta")
      End If
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos"
   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      Try
         mAgregar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

    Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
        Try
            Dim lstrRptName As String

            lstrRptName = "Actividades"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
