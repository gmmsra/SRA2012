<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.asistencia" CodeFile="asistencia.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Control de Asistencia</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function mCargarPeriodos(pCiclo)
		{
		    var sFiltro = pCiclo.value;
		    LoadComboXML("comisiones_cargar", sFiltro, "cmbComiFil", "S");
		}			
		function mCargarAlumnos(pComi)
		{
		    var sFiltro = pComi.value;
		    if(sFiltro=="")
				sFiltro="0";

		    LoadComboXML("alumnos_inscriptos_cargar", sFiltro, "cmbAlumFil", "S");
		    LoadComboXML("mesesXComision_cargar", sFiltro, "cmbMesFil", "S");
		}			
		function mGrabar()
		{
			var arrSele = document.getElementsByTagName('input'); 
			var lstrCmd = '';
			for (var i = 0; i < arrSele.length; i++) 
			{
				if (arrSele[i].name.indexOf("chkAsis")!=-1)
				{
					if (lstrCmd != '')
					{
						lstrCmd = lstrCmd + ';';
					}
					if (arrSele[i].checked)
					{					
						lstrCmd = lstrCmd + 'S';
					}
					else
					{
						lstrCmd = lstrCmd + 'N';
					}
					lstrCmd = lstrCmd + arrSele[i].tag.substring(1);
				}
			}
			document.all("hdnExec").value = lstrCmd;
			document.frmABM.submit();
			return(false);
		}
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</script>
</HEAD>
	<BODY onload="gSetearTituloFrame('')" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 27px" height="27"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Control de Asistencia</asp:label></TD>
								</TR>
								<TR>
									<TD></TD>
									<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
											Width="100%" Visible="True">
            <TABLE id=TableFil style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
            align=left border=0>
              <TR>
                <TD style="WIDTH: 100%">
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24></TD>
                      <TD style="HEIGHT: 8px" width=42></TD>
                      <TD style="HEIGHT: 8px" width=26></TD>
                      <TD style="HEIGHT: 8px"></TD>
                      <TD style="HEIGHT: 8px" width=26>
<CC1:BotonImagen id=btnBusc runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
                      <TD style="HEIGHT: 8px" width=26>
<CC1:BotonImagen id=btnLimpFil runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
                      <TD style="HEIGHT: 8px" width=10></TD></TR>
                    <TR>
                      <TD style="HEIGHT: 8px" width=24><IMG height=25 
                        src="imagenes/formfle.jpg" width=24 border=0></TD>
                      <TD style="HEIGHT: 8px" width=42><IMG height=25 
                        src="imagenes/formtxfiltro.jpg" width=113 border=0></TD>
                      <TD style="HEIGHT: 8px" width=26><IMG height=25 
                        src="imagenes/formcap.jpg" width=26 border=0></TD>
                      <TD style="HEIGHT: 8px" background=imagenes/formfdocap.jpg 
                      colSpan=4><IMG height=25 src="imagenes/formfdocap.jpg" 
                        border=0></TD></TR></TABLE></TD></TR>
              <TR>
                <TD>
                  <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
                    <TR>
                      <TD width=3 background=imagenes/formiz.jpg><IMG 
                        height=30 src="imagenes/formiz.jpg" width=3 border=0></TD>
                      <TD><!-- FOMULARIO -->
                        <TABLE cellSpacing=0 cellPadding=0 width="100%" 
border=0>
                          <TR>
                            <TD style="WIDTH: 8%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 10%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblCiclFil runat="server" cssclass="titulo">Ciclo:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 82%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox class=combo id=cmbCiclFil runat="server" Width="100%" EnterPorTab="False" onchange="mCargarPeriodos(this);" AceptaNull="False"></cc1:combobox></TD></TR>
                          <TR>
                            <TD style="WIDTH: 8%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 10%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 82%" 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" 
                          width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 8%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 10%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblComiFil runat="server" cssclass="titulo">Comisi�n:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 82%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox class=combo id=cmbComiFil runat="server" Width="100%" EnterPorTab="False" onchange="mCargarAlumnos(this);" AceptaNull="False" NomOper="comisiones_cargar"></cc1:combobox></TD></TR>
                          <TR>
                            <TD style="WIDTH: 8%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 10%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 82%" 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" 
                          width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 8%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 10%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblAlumFil runat="server" cssclass="titulo">Alumno:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 82%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox class=combo id=cmbAlumFil runat="server" Width="280px" EnterPorTab="False" AceptaNull="False" NomOper="alumnos_inscriptos_cargar"></cc1:combobox></TD></TR>
                          <TR>
                            <TD style="WIDTH: 8%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 10%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 82%" 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" 
                          width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 8%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 10%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblMesFil runat="server" cssclass="titulo">Mes:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 82%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg>
<cc1:combobox class=combo id=cmbMesFil runat="server" Width="160px" EnterPorTab="False" AceptaNull="False" NomOper="mesesXComision_cargar"></cc1:combobox></TD></TR>
                          <TR>
                            <TD style="WIDTH: 8%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 10%" align=right 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" width=1></TD>
                            <TD style="WIDTH: 82%" 
                            background=imagenes/formdivmed.jpg height=2><IMG 
                              height=2 src="imagenes/formdivmed.jpg" 
                          width=1></TD></TR>
                          <TR>
                            <TD style="WIDTH: 8%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg></TD>
                            <TD style="WIDTH: 10%; HEIGHT: 14px" align=right 
                            background=imagenes/formfdofields.jpg>
<asp:label id=lblFechFil runat="server" cssclass="titulo">Fecha:</asp:label>&nbsp;</TD>
                            <TD style="WIDTH: 82%; HEIGHT: 14px" 
                            background=imagenes/formfdofields.jpg>
<cc1:DateBox id=txtFechFil runat="server" cssclass="cuadrotexto" Width="68px" EnterPorTab="False" AceptaNull="False" Obligatorio="False"></cc1:DateBox></TD></TR>
                          <TR>
                            <TD style="WIDTH: 8%" align=right 
                            background=imagenes/formdivfin.jpg height=2><IMG 
                              height=2 src="imagenes/formdivfin.jpg" width=1></TD>
                            <TD style="WIDTH: 10%" align=right 
                            background=imagenes/formdivfin.jpg height=2><IMG 
                              height=2 src="imagenes/formdivfin.jpg" width=1></TD>
                            <TD style="WIDTH: 82%" width=8 
                            background=imagenes/formdivfin.jpg height=2><IMG 
                              height=2 src="imagenes/formdivfin.jpg" 
                          width=1></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" align="center" height="10"></TD>
								</tr>
								<TR>
									<TD colSpan="3" align="center">
										<DIV align="left">
										<asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px" Visible="False" Width="100%">
            <TABLE class=FdoFld id=Table2 style="WIDTH: 100%" cellPadding=0 
            align=left>
              <TR>
                <TD align=right colSpan=3 height=10>
<asp:ImageButton id=imgClose runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton>&nbsp; 
                </TD></TR>
              <TR>
                <TD align=right colSpan=3 height=10></TD></TR>
              <TR>
                <TD></TD>
                <TD vAlign=top align=center colSpan=2 bgcolor="#FFFFFF">
<asp:datagrid id=grdDato runat="server" Visible="False" BorderWidth="1px" BorderStyle="None" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" ShowFooter="True" width="100%">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer" BackColor="#96AEDC"></FooterStyle>
															<Columns>
																<asp:BoundColumn DataField="orden" HeaderText=""></asp:BoundColumn>																
																<asp:BoundColumn Visible="False" DataField="coin_id" HeaderText="coin_id"></asp:BoundColumn>
																<asp:BoundColumn Visible="False" DataField="alum_id" ReadOnly="True" HeaderText="alum_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="lega_nume" ReadOnly="True" HeaderText="Nro.Legajo"></asp:BoundColumn>
																<asp:BoundColumn DataField="nombre" HeaderText="Apellido y Nombre"></asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD></TR>
              <TR>
                <TD height=30></TD>
                <TD vAlign=top align=right colSpan=2 height=30 bgcolor="#FFFFFF">
                  <TABLE id=Table2 cellSpacing=0 cellPadding=0 width="98%"  border=0>
                    <TR>
                      <TD align=left>
<CC1:BotonImagen id=btnModi runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnGrab.gif" ImageOver="btnGrab2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD>
                      <TD align=right>
<CC1:BotonImagen id=btnList runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif" ForeColor="Transparent"></CC1:BotonImagen></TD></TR></TABLE>&nbsp;&nbsp;&nbsp;&nbsp; 
                </TD></TR></TABLE>
										</asp:panel>
										</DIV>
									</TD>
								</TR>
											
							</TBODY>
						</TABLE> <!--- FIN CONTENIDO ---></td>
					<TD width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></TD>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO -----------------><br>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnExec" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtApel"]!= null)
			document.all["txtApel"].focus();		
		</SCRIPT>
	</BODY>
</HTML>
