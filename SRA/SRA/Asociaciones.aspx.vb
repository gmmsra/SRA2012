Namespace SRA

Partial Class Asociaciones
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lblAutorObs As System.Web.UI.WebControls.Label
   Protected WithEvents lblNumera As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Asociaciones
   Private mstrTablaAutoridad As String = SRA_Neg.Constantes.gTab_AutoridAsoc
   Private mstrTablaTele As String = SRA_Neg.Constantes.gTab_AsocTele
   Private mstrTablaMail As String = SRA_Neg.Constantes.gTab_AsocMails
   Private mstrTablaAsocRaza As String = SRA_Neg.Constantes.gTab_AsociacionesRazas
   Private mstrCmd As String
   Private mstrConn As String
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            If panDato.Visible Then
               mdsDatos = Session(mstrTabla)
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLongs As Object

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)

      txtCodigoFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asoc_codi")
      txtDenominacionFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asoc_deno")
      txtSiglaFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asoc_sigla")

      txtCodigo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asoc_codi")
      txtDenominacion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asoc_deno")
      txtSigla.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asoc_sigla")
      txtDireccion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asoc_dire")
      txtObservacion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "asoc_obse")

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaAutoridad)
      txtNyA.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "auas_nyap")
      txtReferencia.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "auas_refe")

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaTele)
      txtTeleAreaCode.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "teas_area_code")
      txtTeleRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "teas_refe")
      txtTeleTele.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "teas_tele")

      lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaMail)
      txtMailRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "maas_refe")
      txtMailMail.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "maas_mail")
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspecieFil, "T")
      clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRazaFil, "id", "descrip", "T")
      clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspecie, "S")
      clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRaza, "id", "descrip", "S")
      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPais, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPaisFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "tele_tipos", cmbTeleTipo, "S")

            SRA_Neg.Utiles.gSetearRaza(cmbRaza)
            SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)

        SRA_Neg.Utiles.gSetearEspecie(cmbEspecie)
        SRA_Neg.Utiles.gSetearEspecie(cmbEspecieFil)
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridAutoridad_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdAutoridad.EditItemIndex = -1
         If (grdAutoridad.CurrentPageIndex < 0 Or grdAutoridad.CurrentPageIndex >= grdAutoridad.PageCount) Then
            grdAutoridad.CurrentPageIndex = 0
         Else
            grdAutoridad.CurrentPageIndex = E.NewPageIndex
         End If
         grdAutoridad.DataSource = mdsDatos.Tables(mstrTablaAutoridad)
         grdAutoridad.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridTele_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdTele.EditItemIndex = -1
         If (grdTele.CurrentPageIndex < 0 Or grdTele.CurrentPageIndex >= grdTele.PageCount) Then
            grdTele.CurrentPageIndex = 0
         Else
            grdTele.CurrentPageIndex = E.NewPageIndex
         End If
         grdTele.DataSource = mdsDatos.Tables(mstrTablaTele)
         grdTele.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridMail_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdMail.CurrentPageIndex < 0 Or grdMail.CurrentPageIndex >= grdMail.PageCount) Then
            grdMail.CurrentPageIndex = 0
         Else
            grdMail.CurrentPageIndex = E.NewPageIndex
         End If
         grdMail.DataSource = mdsDatos.Tables(mstrTablaMail)
         grdMail.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGridAsocRaza_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdAsocRaza.EditItemIndex = -1
         If (grdAsocRaza.CurrentPageIndex < 0 Or grdAsocRaza.CurrentPageIndex >= grdAsocRaza.PageCount) Then
            grdAsocRaza.CurrentPageIndex = 0
         Else
            grdAsocRaza.CurrentPageIndex = E.NewPageIndex
         End If
         grdAsocRaza.DataSource = mdsDatos.Tables(mstrTablaAsocRaza)
         grdAsocRaza.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul @formato='1'"
         mstrCmd = mstrCmd + ",@asoc_codi='" + txtCodigoFil.Valor.ToString + "'"
         mstrCmd = mstrCmd + ",@asoc_deno='" + txtDenominacionFil.Valor.ToString + "'"
         mstrCmd = mstrCmd + ",@asoc_sigla='" + txtSiglaFil.Valor.ToString + "'"
         mstrCmd = mstrCmd + ",@asoc_pais_id=" + IIf(cmbPaisFil.Valor.ToString = "", "0", cmbPaisFil.Valor.ToString)
         mstrCmd = mstrCmd + ",@asra_espe_id=" + IIf(cmbEspecieFil.Valor.ToString = "", "0", cmbEspecieFil.Valor.ToString)
         mstrCmd = mstrCmd + ",@asra_raza_id=" + IIf(cmbRazaFil.Valor.ToString = "", "0", cmbRazaFil.Valor.ToString)
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorAutoridad(ByVal pbooAlta As Boolean)
      btnBajaAutoridad.Enabled = Not (pbooAlta)
      btnModiAutoridad.Enabled = Not (pbooAlta)
      btnAltaAutoridad.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorTele(ByVal pbooAlta As Boolean)
      btnBajaTele.Enabled = Not (pbooAlta)
      btnModiTele.Enabled = Not (pbooAlta)
      btnAltaTele.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorMail(ByVal pbooAlta As Boolean)
      btnBajaMail.Enabled = Not (pbooAlta)
      btnModiMail.Enabled = Not (pbooAlta)
      btnAltaMail.Enabled = pbooAlta
   End Sub
   Private Sub mSetearEditorAsocRaza(ByVal pbooAlta As Boolean)
      btnBajaAsocRaza.Enabled = Not (pbooAlta)
      btnModiAsocRaza.Enabled = Not (pbooAlta)
      btnAltaAsocRaza.Enabled = pbooAlta
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

      mCrearDataSet(hdnId.Text)

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            txtCodigo.Valor = .Item("asoc_codi")
            txtCodigo.Enabled = False
            txtDenominacion.Valor = .Item("asoc_deno")
            txtSigla.Valor = .Item("asoc_sigla")
            txtDireccion.Valor = .Item("asoc_dire")
            cmbPais.Valor = .Item("asoc_pais_id")
            chkNumera.Checked = IIf(.Item("asoc_nume") = "S�", True, False)
            
            txtObservacion.Text = IIf(.Item("asoc_obse").ToString = "", "", .Item("asoc_obse"))
            If Not .IsNull("asoc_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("asoc_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If
         End With
        mSetearEditor(False)
         mMostrarPanel(True)
         mShowTabs(1)
      End If
   End Sub
   Public Sub mEditarDatosAutoridad(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrAutoridad As DataRow

         hdnDireId.Text = E.Item.Cells(1).Text
         ldrAutoridad = mdsDatos.Tables(mstrTablaAutoridad).Select("auas_id=" & hdnDireId.Text)(0)

         With ldrAutoridad
            txtNyA.Valor = .Item("auas_nyap")
            txtReferencia.Valor = .Item("auas_refe")
            If Not .IsNull("auas_baja_fecha") Then
               lblBajaDire.Text = "Direcci�n dada de baja en fecha: " & CDate(.Item("auas_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaDire.Text = ""
            End If
         End With
         mSetearEditorAutoridad(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosTele(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrTele As DataRow

         hdnTeleId.Text = E.Item.Cells(1).Text
         ldrTele = mdsDatos.Tables(mstrTablaTele).Select("teas_id=" & hdnTeleId.Text)(0)

         With ldrTele
            txtTeleRefe.Valor = .Item("teas_refe")
            txtTeleAreaCode.Valor = .Item("teas_area_code")
            txtTeleTele.Valor = .Item("teas_tele")
            cmbTeleTipo.Valor = .Item("teas_teti_id")
            If Not .IsNull("teas_baja_fecha") Then
               lblBajaTele.Text = "Tel�fono dado de baja en fecha: " & CDate(.Item("teas_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaTele.Text = ""
            End If
         End With
         mSetearEditorTele(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosMail(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrMail As DataRow

         hdnMailId.Text = E.Item.Cells(1).Text
         ldrMail = mdsDatos.Tables(mstrTablaMail).Select("maas_id=" & hdnMailId.Text)(0)

         With ldrMail
            txtMailRefe.Valor = .Item("maas_refe")
            txtMailMail.Valor = .Item("maas_mail")
            If Not .IsNull("maas_baja_fecha") Then
               lblBajaMail.Text = "Mail dado de baja en fecha: " & CDate(.Item("maas_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaMail.Text = ""
            End If
         End With
         mSetearEditorMail(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatosAsocRaza(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrAsocRaza As DataRow

         hdnAutorId.Text = E.Item.Cells(1).Text
         ldrAsocRaza = mdsDatos.Tables(mstrTablaAsocRaza).Select("asra_id=" & hdnAutorId.Text)(0)

         With ldrAsocRaza
            cmbEspecie.Valor = .Item("asra_espe_id")
            cmbRaza.Valor = .Item("asra_raza_id")
            If Not .IsNull("asra_baja_fecha") Then
               lblBajaAutor.Text = "Autorizado dado de baja en fecha: " & CDate(.Item("asra_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBajaAutor.Text = ""
            End If
         End With
         mSetearEditorAsocRaza(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaTele
      mdsDatos.Tables(2).TableName = mstrTablaAutoridad
      mdsDatos.Tables(3).TableName = mstrTablaMail
      mdsDatos.Tables(4).TableName = mstrTablaAsocRaza

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If
      grdAutoridad.DataSource = mdsDatos.Tables(mstrTablaAutoridad)
      grdAutoridad.DataBind()

      grdTele.DataSource = mdsDatos.Tables(mstrTablaTele)
      grdTele.DataBind()

      grdMail.DataSource = mdsDatos.Tables(mstrTablaMail)
      grdMail.DataBind()

      grdAsocRaza.DataSource = mdsDatos.Tables(mstrTablaAsocRaza)
      grdAsocRaza.DataBind()

      Session(mstrTabla) = mdsDatos
   End Sub
   Private Sub mAgregar()
        mLimpiar()
        txtCodigo.Text = darProximoCodigo()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
    End Sub
    ''' Informa el proximo numero disponible y lo suguiere al usuario
    ''' MF 22/03/2013
    Private Function darProximoCodigo() As Integer
        darProximoCodigo = clsSQLServer.gExecuteScalar(mstrConn, "Asociaciones_ProxCodigo")

    End Function

    Private Sub mLimpiar()
        hdnId.Text = ""
        txtCodigo.Text = ""
        txtCodigo.Enabled = True
        txtDenominacion.Text = ""
        txtSigla.Text = ""
        txtDireccion.Text = ""
        txtObservacion.Text = ""
        chkNumera.Checked = False
        cmbPais.Limpiar()

        lblBaja.Text = ""

        mLimpiarAutoridad()
        mLimpiarTele()
        mLimpiarMail()
        mLimpiarAsocRaza()

        grdDato.CurrentPageIndex = 0
        grdAutoridad.CurrentPageIndex = 0
        grdTele.CurrentPageIndex = 0
        grdMail.CurrentPageIndex = 0
        grdAsocRaza.CurrentPageIndex = 0

        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarFil()
        txtCodigoFil.Text = ""
        txtDenominacionFil.Text = ""
        txtSiglaFil.Text = ""
        cmbPaisFil.Limpiar()
        cmbEspecieFil.Limpiar()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRazaFil, "id", "descrip", "T")
        cmbRazaFil.Limpiar()
        mConsultar()
    End Sub
    Private Sub mLimpiarAutoridad()
        hdnDireId.Text = ""
        txtNyA.Text = ""
        txtReferencia.Text = ""
        mSetearEditorAutoridad(True)
    End Sub
    Private Sub mLimpiarTele()
        hdnTeleId.Text = ""
        txtTeleRefe.Text = ""
        txtTeleAreaCode.Text = ""
        txtTeleTele.Text = ""
        cmbTeleTipo.Limpiar()
        lblBajaTele.Text = ""
        mSetearEditorTele(True)
    End Sub
    Private Sub mLimpiarMail()
        hdnMailId.Text = ""
        txtMailRefe.Text = ""
        txtMailMail.Text = ""
        lblBajaMail.Text = ""
        mSetearEditorMail(True)
    End Sub
    Private Sub mLimpiarAsocRaza()
        hdnAutorId.Text = ""
        cmbEspecie.Limpiar()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString), cmbRaza, "id", "descrip", "S")
        mSetearEditorAsocRaza(True)
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        mEstablecerPerfil()
    End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         Dim intId As Integer = lobjGenerica.Alta()
         Dim mdsDs As New DataSet

         mdsDs = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, intId)

         clsError.gGenerarMensajes(Me, "Se gener� la Asociaci�n con el c�digo " + mdsDs.Tables(0).Rows(0).Item("asoc_codi").ToString)

         mConsultar()
         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         Dim mdsDs As New DataSet
         mdsDs = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

         clsError.gGenerarMensajes(Me, "Se actualiz� la Asociaci�n con el c�digo " + mdsDs.Tables(0).Rows(0).Item("asoc_codi").ToString)


         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mValidaDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
   Private Sub mGuardarDatos()
      mValidaDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("asoc_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("asoc_codi") = txtCodigo.Valor
         .Item("asoc_dire") = txtDireccion.Valor
         .Item("asoc_deno") = txtDenominacion.Valor
         .Item("asoc_sigla") = txtSigla.Valor
         .Item("asoc_pais_id") = cmbPais.Valor
         .Item("asoc_nume") = IIf(chkNumera.Checked = True, 1, 0)
         .Item("asoc_obse") = txtObservacion.Valor
         .Item("asoc_baja_fecha") = DBNull.Value
         .Item("asoc_audi_user") = Session("sUserId").ToString()
      End With
   End Sub
   'Seccion de los Direcciones
   Private Sub mActualizarAutoridad()
      Try
         mGuardarDatosAutoridad()

         mLimpiarAutoridad()
         grdAutoridad.DataSource = mdsDatos.Tables(mstrTablaAutoridad)
         grdAutoridad.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaAutoridad()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaAutoridad).Select("auas_id=" & hdnDireId.Text)(0)
         row.Delete()
         grdAutoridad.DataSource = mdsDatos.Tables(mstrTablaAutoridad)
         grdAutoridad.DataBind()
         mLimpiarAutoridad()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosAutoridad()
      Dim ldrAutoridad As DataRow

      If txtNyA.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar Nombre y Apellido.")
      End If

      If txtReferencia.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
      End If

      If hdnDireId.Text = "" Then
         ldrAutoridad = mdsDatos.Tables(mstrTablaAutoridad).NewRow
         ldrAutoridad.Item("auas_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaAutoridad), "auas_id")
      Else
         ldrAutoridad = mdsDatos.Tables(mstrTablaAutoridad).Select("auas_id=" & hdnDireId.Text)(0)
      End If

      With ldrAutoridad
         .Item("auas_nyap") = txtNyA.Valor
         .Item("auas_refe") = txtReferencia.Valor
         .Item("auas_asoc_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("auas_audi_user") = Session("sUserId").ToString()
         .Item("auas_baja_fecha") = DBNull.Value
      End With
      If (hdnDireId.Text = "") Then
         mdsDatos.Tables(mstrTablaAutoridad).Rows.Add(ldrAutoridad)
      End If
   End Sub
   'Seccion de los Telefonos
   Private Sub mActualizarTele()
      Try
         mGuardarDatosTele()

         mLimpiarTele()
         grdTele.DataSource = mdsDatos.Tables(mstrTablaTele)
         grdTele.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaTele()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaTele).Select("teas_id=" & hdnTeleId.Text)(0)
         row.Delete()
         grdTele.DataSource = mdsDatos.Tables(mstrTablaTele)
         grdTele.DataBind()
         mLimpiarTele()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosTele()
      Dim ldrTele As DataRow

      If cmbTeleTipo.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Tipo de Tel�fono.")
      End If

      If txtTeleRefe.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
      End If

      If txtTeleAreaCode.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el C�digo de �rea.")
      End If

      If txtTeleTele.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el Tel�fono.")
      End If

      If hdnTeleId.Text = "" Then
         ldrTele = mdsDatos.Tables(mstrTablaTele).NewRow
         ldrTele.Item("teas_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaTele), "teas_id")
      Else
         ldrTele = mdsDatos.Tables(mstrTablaTele).Select("teas_id=" & hdnTeleId.Text)(0)
      End If

      With ldrTele
         .Item("teas_refe") = txtTeleRefe.Valor
         .Item("teas_area_code") = txtTeleAreaCode.Valor
         .Item("teas_tele") = txtTeleTele.Valor
         .Item("teas_teti_id") = cmbTeleTipo.Valor
         .Item("_TipoTele") = cmbTeleTipo.SelectedItem.Text
         .Item("teas_asoc_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("_estado") = "Activo"
         .Item("teas_audi_user") = Session("sUserId").ToString()
         .Item("teas_baja_fecha") = DBNull.Value
      End With
      If (hdnTeleId.Text = "") Then
         mdsDatos.Tables(mstrTablaTele).Rows.Add(ldrTele)
      End If
   End Sub
   'Seccion de los Mails
   Private Sub mActualizarMail()
      Try
         mGuardarDatosMail()

         mLimpiarMail()
         grdMail.DataSource = mdsDatos.Tables(mstrTablaMail)
         grdMail.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaMail()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaMail).Select("maas_id=" & hdnMailId.Text)(0)
         row.Delete()
         grdMail.DataSource = mdsDatos.Tables(mstrTablaMail)
         grdMail.DataBind()
         mLimpiarMail()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosMail()
      Dim ldrMail As DataRow

      If txtMailRefe.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Referencia.")
      End If

      If txtMailMail.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la Direcci�n de Mail.")
      End If

      If hdnMailId.Text = "" Then
         ldrMail = mdsDatos.Tables(mstrTablaMail).NewRow
         ldrMail.Item("maas_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaMail), "maas_id")
      Else
         ldrMail = mdsDatos.Tables(mstrTablaMail).Select("maas_id=" & hdnMailId.Text)(0)
      End If

      With ldrMail
         .Item("maas_refe") = txtMailRefe.Valor
         .Item("maas_mail") = txtMailMail.Valor
         .Item("maas_asoc_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("_estado") = "Activo"
         .Item("maas_audi_user") = Session("sUserId").ToString()
         .Item("maas_baja_fecha") = DBNull.Value
      End With
      If (hdnMailId.Text = "") Then
         mdsDatos.Tables(mstrTablaMail).Rows.Add(ldrMail)
      End If
   End Sub
   'Seccion de los Asociacion Raza
   Private Sub mActualizarAsocRaza()
      Try
         mGuardarDatosAsocRaza()

         mLimpiarAsocRaza()
         grdAsocRaza.DataSource = mdsDatos.Tables(mstrTablaAsocRaza)
         grdAsocRaza.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBajaAsocRaza()
      Try
         Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaAsocRaza).Select("asra_id=" & hdnAutorId.Text)(0)
         row.Delete()
         grdAsocRaza.DataSource = mdsDatos.Tables(mstrTablaAsocRaza)
         grdAsocRaza.DataBind()
         mLimpiarAsocRaza()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosAsocRaza()
      Dim ldrAsocRaza As DataRow

      If cmbRaza.Valor.ToString = "" And cmbEspecie.Valor.ToString = "" Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la especie o raza.")
      End If

      If hdnAutorId.Text = "" Then
         ldrAsocRaza = mdsDatos.Tables(mstrTablaAsocRaza).NewRow
         ldrAsocRaza.Item("asra_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaAsocRaza), "asra_id")
      Else
         ldrAsocRaza = mdsDatos.Tables(mstrTablaAsocRaza).Select("asra_id=" & hdnAutorId.Text)(0)
      End If

      With ldrAsocRaza
         .Item("asra_raza_id") = cmbRaza.Valor
         .Item("asra_espe_id") = cmbEspecie.Valor
         .Item("_espe_desc") = IIf(cmbEspecie.Valor.ToString = "", "", cmbEspecie.SelectedItem.Text)
         .Item("_razas_desc") = IIf(cmbRaza.Valor.ToString = "", "", cmbRaza.SelectedItem.Text)
         .Item("asra_asoc_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("asra_audi_user") = Session("sUserId").ToString()
         .Item("asra_baja_fecha") = DBNull.Value
      End With
      If (hdnAutorId.Text = "") Then
         mdsDatos.Tables(mstrTablaAsocRaza).Rows.Add(ldrAsocRaza)
      End If
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub mShowTabs(ByVal Tab As Byte)
      Try
         panGeneral.Visible = False
         lnkGeneral.Font.Bold = False
         panAutoridad.Visible = False
         lnkDirecciones.Font.Bold = False
         panTelefonos.Visible = False
         lnkTelefonos.Font.Bold = False
         panMails.Visible = False
         lnkMails.Font.Bold = False
         panAsocRazas.Visible = False
         lnkAutorizados.Font.Bold = False
         Select Case Tab
            Case 1
               panGeneral.Visible = True
               lnkGeneral.Font.Bold = True
            Case 2
               panAutoridad.Visible = True
               lnkDirecciones.Font.Bold = True
            Case 3
               panTelefonos.Visible = True
               lnkTelefonos.Font.Bold = True
            Case 4
               panMails.Visible = True
               lnkMails.Font.Bold = True
            Case 5
               panAsocRazas.Visible = True
               lnkAutorizados.Font.Bold = True
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
      mCerrar()
   End Sub
   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub
   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      mConsultar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
      Try
         Dim lstrRptName As String

         lstrRptName = "Asociaciones"

         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&asoc_codi=" + txtCodigoFil.Valor.ToString
         lstrRpt += "&asoc_deno=" + txtDenominacionFil.Valor.ToString
         lstrRpt += "&asoc_sigla=" + txtSiglaFil.Valor.ToString
         lstrRpt += "&asoc_pais_id=" + IIf(cmbPaisFil.Valor.ToString = "", "0", cmbPaisFil.Valor.ToString)
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   'Botones generales
   Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   'Botones de Autoridad
   Private Sub btnAltaAutoridad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaAutoridad.Click
      mActualizarAutoridad()
   End Sub
   Private Sub btnBajaAutoridad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaAutoridad.Click
      mBajaAutoridad()
   End Sub
   Private Sub btnModiAutoridad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiAutoridad.Click
      mActualizarAutoridad()
   End Sub
   Private Sub btnLimpAutoridad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpAutoridad.Click
      mLimpiarAutoridad()
   End Sub
   'Botones de Telefonos
   Private Sub btnAltaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaTele.Click
      mActualizarTele()
   End Sub
   Private Sub btnBajaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaTele.Click
      mBajaTele()
   End Sub
   Private Sub btnModiTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiTele.Click
      mActualizarTele()
   End Sub
   Private Sub btnLimpTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpTele.Click
      mLimpiarTele()
   End Sub
   'Botones de mails
   Private Sub btnAltaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaMail.Click
      mActualizarMail()
   End Sub
   Private Sub btnBajaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaMail.Click
      mBajaMail()
   End Sub
   Private Sub btnModiMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiMail.Click
      mActualizarMail()
   End Sub
   Private Sub btnLimpMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpMail.Click
      mLimpiarMail()
   End Sub
   'Botones de asociaciones raza
   Private Sub btnAltaAsocRaza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaAsocRaza.Click
      mActualizarAsocRaza()
   End Sub
   Private Sub btnBajaAsocRaza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaAsocRaza.Click
      mBajaAsocRaza()
   End Sub
   Private Sub btnModiAsocRaza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiAsocRaza.Click
      mActualizarAsocRaza()
   End Sub
   Private Sub btnLimpAsocRaza_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpAsocRaza.Click
      mLimpiarAsocRaza()
   End Sub
   'Solapas
   Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkDirecciones_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDirecciones.Click
      mShowTabs(2)
   End Sub
   Private Sub lnkTelefonos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTelefonos.Click
      mShowTabs(3)
   End Sub
   Private Sub lnkMails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMails.Click
      mShowTabs(4)
   End Sub
   Private Sub lnkAutorizados_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAutorizados.Click
      mShowTabs(5)
   End Sub
#End Region

End Class
End Namespace
