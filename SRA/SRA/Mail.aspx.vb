Imports System.Data.SqlClient


Namespace SRA


Partial Class Mail
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents lblExenta As System.Web.UI.WebControls.Label
   Protected WithEvents lblPercep As System.Web.UI.WebControls.Label
   Protected WithEvents lblPrecSoci As System.Web.UI.WebControls.Label
   Protected WithEvents lblMiemCD As System.Web.UI.WebControls.Label

    Protected WithEvents btnCtaCte As System.Web.UI.WebControls.Button
    Protected WithEvents btnGenerarCuot As System.Web.UI.WebControls.ImageButton
    Protected WithEvents panDato As System.Web.UI.WebControls.Panel
    Protected WithEvents Label8 As System.Web.UI.WebControls.Label
    Protected WithEvents Label15 As System.Web.UI.WebControls.Label
    Protected WithEvents Label16 As System.Web.UI.WebControls.Label
    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Public mstrTitulo As String
   Private mstrConn As String
   Private mdecTotDebe As Decimal
   Private mdecTotHaber As Decimal
   Private mdecTotAplic As Decimal
   Private mdecTotOrig As Decimal
   Private mdecCtaCteVenc As Decimal = 0
    Private mdecCtaSocVenc As Decimal = 0
    Private mdecCtaCte As Decimal = 0
    Private mdecCtaSoc As Decimal = 0
   Private mdecImpoCtaCte As Decimal = 0
   Private mdecImpoCtaSoc As Decimal = 0
   Private mdecProfExpo As Decimal = 0
   Private mdecProfLabo As Decimal = 0
   Private mdecProfOtra As Decimal = 0
    Private mdecProfRRGG As Decimal = 0
    Private mdecTotaAcus As Decimal = 0
    Private mdecInteCte As Decimal = 0
    Private mdecInteSoc As Decimal = 0


   Private Enum ColumnasFil As Integer
      comp_id = 0
      fecha = 1
      nume = 2
      impo_ori = 3
      debe = 4
      haber = 5
      aplicado = 6
      vto = 7
   End Enum

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then

         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()
      lblTitu.Text = Request.QueryString("titulo")
      hdnClieId.Text = Request.QueryString("clie")
      grdDatoBusq.DataSource = Session("ResumenCuenta")
      grdDatoBusq.DataBind()
      mTotales()
   End Sub
#End Region

   Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCerrar.Click
      mCerrar()
   End Sub

   Private Sub mTotales()
      Try
         Dim dr As SqlClient.SqlDataReader

         If hdnClieId.Text <> "" Then
            Dim lstrCmd As String = "exec clientes_estado_de_saldos_consul @clie_id=" & hdnClieId.Text
            dr = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), lstrCmd)
            While (dr.Read())
                    ' 13/08/2010
                    'mdecCtaCteVenc = mdecCtaCteVenc + dr.GetValue(dr.GetOrdinal("saldo_cta_cte_venc"))
                    'mdecCtaSocVenc = mdecCtaSocVenc + dr.GetValue(dr.GetOrdinal("saldo_social_venc"))
                    mdecCtaCte = mdecCtaCte + dr.GetValue(dr.GetOrdinal("saldo_cta_cte"))
                    mdecCtaSoc = mdecCtaSoc + dr.GetValue(dr.GetOrdinal("saldo_social"))
                    mdecImpoCtaCte = mdecImpoCtaCte + dr.GetValue(dr.GetOrdinal("imp_a_cta_cta_cte"))
                mdecImpoCtaSoc = mdecImpoCtaSoc + dr.GetValue(dr.GetOrdinal("imp_a_cta_cta_social"))
                mdecProfExpo = mdecProfExpo + dr.GetValue(dr.GetOrdinal("TotalProformaExpo"))
                mdecProfLabo = mdecProfLabo + dr.GetValue(dr.GetOrdinal("TotalProformaLabo"))
                mdecProfOtra = mdecProfOtra + dr.GetValue(dr.GetOrdinal("TotalProformaOtras"))
                mdecProfRRGG = mdecProfRRGG + dr.GetValue(dr.GetOrdinal("TotalProformaRRGG"))
                    mdecTotaAcus = mdecTotaAcus + dr.GetValue(dr.GetOrdinal("Total_Acuses"))
                    mdecInteSoc = mdecInteSoc + dr.GetValue(dr.GetOrdinal("Interes_Soc"))
                    mdecInteCte = mdecInteCte + dr.GetValue(dr.GetOrdinal("Interes_Cte"))

                End While
            End If

            'txtCtaCteVenc.Text = mdecCtaCteVenc
            'txtCtaSocVenc.Text = mdecCtaSocVenc

            txtCtaCte.Text = mdecCtaCte
            txtCtaSoc.Text = mdecCtaSoc
         txtImpoCtaCte.Text = mdecImpoCtaCte
         txtImpoCtaSoc.Text = mdecImpoCtaSoc
         txtProfExpo.Text = mdecProfExpo
         txtProfLabo.Text = mdecProfLabo
         txtProfOtra.Text = mdecProfOtra
         txtProfRRGG.Text = mdecProfRRGG
            txtTotalAcuses.Text = mdecTotaAcus
            txtInteresSoc.Text = mdecInteSoc
            txtInteresCte.Text = mdecInteCte
         dr.Close()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCerrar()
      Response.Write("<Script>window.close();</script>")
   End Sub

   Private Sub btnEnvi_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnvi.Click
      Dim lstrMsg As String = ""
      Dim lbooOK As Boolean = False
      Try
         If txtPara.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la direcci�n de mail del destinatario.")
         End If
         Dim lstrTemplate As String = SRA_Neg.clsMail.mLeerTemplate("resumen_cuenta.htm")
         Dim lstrLnk As String = System.Configuration.ConfigurationSettings.AppSettings("conLnkSistema").ToString()
         Dim ldsDatos As DataSet = Session("ResumenCuenta")
         If Not ldsDatos Is Nothing Then
            If ldsDatos.Tables(0).Rows.Count > 0 Then
                mEnviarMensajeDir(mstrConn, "resumen_cuenta", txtPara.Text, ldsDatos, lstrLnk, ldsDatos.Tables(0).Rows(0).Item("clie_apel"))
                lbooOK = True
                Throw New AccesoBD.clsErrNeg("El mail se ha enviado correctamente.")
            End If
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
         If lbooOK Then
            mCerrar()
         End If
      End Try
   End Sub

        Private Sub mEnviarMensajeDir(ByVal pstrConn As String, ByVal pstrTemplate As String, ByVal pstrMails As String, ByVal pdsDatos As DataSet, ByVal pstrLink As String, ByVal pstrClie As String)
            Dim lstrMsg As String = SRA_Neg.clsMail.mLeerTemplate(pstrTemplate + ".htm")
            Dim lstrText As String
            Dim lstrMsgRet As String
            Dim lintPosiIni As Integer
            Dim lintPosiFin As Integer
            Dim lstrTempHead As String
            Dim lstrTempRepe As String
            Dim lstrTempFoot As String
            Dim lstrTemp As String

            If System.Configuration.ConfigurationSettings.AppSettings("conEnviarMails").ToString() = "1" Then

                lintPosiIni = lstrMsg.IndexOf("<!-- inicio_repeticion -->")
                lintPosiFin = lstrMsg.IndexOf("<!-- fin_repeticion -->")

                If (lintPosiIni <> -1) Then
                    If (lintPosiIni <> 0) Then
                        lstrTempHead = lstrMsg.Substring(0, lintPosiIni - 1)
                    End If
                    lstrTempRepe = lstrMsg.Substring(lintPosiIni + 26, (lintPosiFin - lintPosiIni - 26))
                    lstrTempFoot = lstrMsg.Substring(lintPosiFin + 23)
                Else
                    lstrTempRepe = lstrMsg
                End If

                lstrMsgRet = lstrTempHead
                Dim ldrRow As DataRow

                With pdsDatos.Tables(0)
                    For i As Integer = 0 To .DefaultView.Count - 1
                        lstrTemp = lstrTempRepe
                        ldrRow = .DefaultView.Item(i).Row
                        For Each ldcCol As DataColumn In .Columns
                            If Not ldrRow.IsNull(ldcCol.ColumnName.ToLower) Then
                                If ldcCol.DataType.Name = "DateTime" Then
                                    lstrText = CDate(ldrRow.Item(ldcCol.ColumnName.ToLower)).ToString("dd/MM/yyyy")
                                Else
                                    lstrText = ldrRow.Item(ldcCol.ColumnName.ToLower).ToString
                                End If
                            Else
                                lstrText = ""
                            End If
                            lstrText = lstrText.Replace(Chr(10) + Chr(13), "<br>")
                            lstrTemp = lstrTemp.Replace("[" + ldcCol.ColumnName.ToLower + "]", lstrText)
                        Next
                        lstrMsgRet += lstrTemp
                    Next
                End With

                lstrMsgRet += lstrTempFoot

                lstrMsgRet = lstrMsgRet.Replace("[cliente]", pstrClie)
                lstrMsgRet = lstrMsgRet.Replace("lnksist", pstrLink)

                'lstrMsgRet = lstrMsgRet.Replace("[CtaCteVenc]", mdecCtaCteVenc.ToString())
                'lstrMsgRet = lstrMsgRet.Replace("[CtaSocVenc]", mdecCtaSocVenc.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[CtaCte]", mdecCtaCte.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[CtaSoc]", mdecCtaSoc.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[ImpoCtaCte]", mdecImpoCtaCte.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[ImpoCtaSoc]", mdecImpoCtaSoc.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[ProfExpo]", mdecProfExpo.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[ProfLabo]", mdecProfLabo.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[ProfOtra]", mdecProfOtra.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[ProfRRGG]", mdecProfRRGG.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[total_acuses]", mdecTotaAcus.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[Interes_Cte]", mdecInteCte.ToString())
                lstrMsgRet = lstrMsgRet.Replace("[Interes_Soc]", mdecInteSoc.ToString())

                lstrMsgRet = lstrMsgRet.Replace("[TotDebe]", mdecTotDebe.ToString("######0.00"))
                lstrMsgRet = lstrMsgRet.Replace("[TotHaber]", mdecTotHaber.ToString("######0.00"))
                lstrMsgRet = lstrMsgRet.Replace("[TotAplic]", mdecTotAplic.ToString())

                SRA_Neg.clsMail.gEnviarMail(pstrMails, txtAsun.Text, lstrMsgRet, "")

            End If
        End Sub

        Private Sub grdDatoBusq_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdDatoBusq.ItemDataBound
      Try
         Select Case e.Item.ItemType
            Case ListItemType.Header
               mdecTotDebe = 0
               mdecTotHaber = 0
               mdecTotAplic = 0
               mdecTotOrig = 0
            Case ListItemType.Item, ListItemType.AlternatingItem
               With CType(e.Item.DataItem, DataRowView).Row
                  mdecTotDebe += .Item("debe")
                  mdecTotHaber += .Item("haber")
                  mdecTotAplic += .Item("aplicado")
               End With
            Case ListItemType.Footer
               e.Item.Cells(ColumnasFil.debe).Text = mdecTotDebe.ToString("######0.00")
               e.Item.Cells(ColumnasFil.haber).Text = mdecTotHaber.ToString("######0.00")
               e.Item.Cells(ColumnasFil.aplicado).Text = mdecTotAplic.ToString("######0.00")
         End Select
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub

End Class
End Namespace
