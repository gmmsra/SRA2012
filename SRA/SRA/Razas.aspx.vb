' Dario 2013-08-27 se realiza cambio -- agrega nuevo dato en la tabla de raza cmbraza_PermiteEditarNumeradorCriador
' se usa para por parametria permitir la modificacion del nro de criador en expediente
' Dario 2013-09-05 cambio que permite indicar si la raza
' inscribe o no en la SRA para 14 % bonis lab
' Dario 2014-10-16 se agrega combo cmbADNFlushing
' Dario 2015-12-09 se agregan los parametros de no controla servicios ni implantes desde fechas desde
Namespace SRA

Partial Class Razas
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblCtrlPela As System.Web.UI.WebControls.Label
    Protected WithEvents lnkGenealogia As System.Web.UI.WebControls.LinkButton
    Protected WithEvents TEXTBOXTAB1 As NixorControls.TextBoxTab
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Protected WithEvents Numberbox4 As NixorControls.NumberBox
    Protected WithEvents Label5 As System.Web.UI.WebControls.Label
    Protected WithEvents Numberbox5 As NixorControls.NumberBox
    Protected WithEvents Combobox2 As NixorControls.ComboBox
    Protected WithEvents Combobox5 As NixorControls.ComboBox
    Protected WithEvents lblEspecie As System.Web.UI.WebControls.Label
    Protected WithEvents cmbEspecie As NixorControls.ComboBox
    Protected WithEvents txtNumeHembras As NixorControls.TextBoxTab
    Protected WithEvents txtNumeMachos As NixorControls.TextBoxTab
    Protected WithEvents txtNumeMachoDador As NixorControls.TextBoxTab
    Protected WithEvents txtNumeHembraDona As NixorControls.TextBoxTab
    Protected WithEvents txtNume As NixorControls.TextBoxTab
    Protected WithEvents lblNumTramite As System.Web.UI.WebControls.Label




    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Razas
    Private mstrTablaDocum As String = SRA_Neg.Constantes.gTab_RazasDocum
    Private mstrTablaEspecies As String = SRA_Neg.Constantes.gTab_Especies
    Private mstrTablaRazasPadres As String = SRA_Neg.Constantes.gTab_RazasPadres
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)

            If (Not Page.IsPostBack) Then
                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mEstablecerPerfil()
        Dim lbooPermiAlta As Boolean
        Dim lbooPermiModi As Boolean
    End Sub
    Private Sub mCargarCombos()
        'If cmbEspecieFil.Valor.ToString = "" Then
        '    If Session("sEspeId") <> "" Then
        '        clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "T", "@raza_espe_id=" + Session("sEspeId"))
        '    End If

        'Else
        '    clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "T", "@raza_espe_id=" + cmbEspecieFil.Valor.ToString)
        'End If

        clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspecieFil, "T")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecieFil.Valor.ToString = "", "null", cmbEspecieFil.Valor.ToString), cmbRazaFil, "id", "descrip", "T")
        clsWeb.gCargarRefeCmb(mstrConn, "especies", cmbEspecieFil, "S")
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & IIf(cmbEspecieFil.Valor.ToString = "", "null", cmbEspecieFil.Valor.ToString), cmbRaza, "id", "descrip", "S")

        SRA_Neg.Utiles.gSetearRaza(cmbRaza)
        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
        SRA_Neg.Utiles.gSetearEspecie(cmbEspecieFil)
    End Sub

    Private Sub mCargarCombosBis()
        clsWeb.gCargarComboBool(cmbAstado, "N")
        clsWeb.gCargarComboBool(cmbCaractPrec, "N")
        clsWeb.gCargarComboBool(cmbCtrlColor, "N")
        clsWeb.gCargarComboBool(cmbCtrlPela, "N")
        clsWeb.gCargarComboBool(cmbCtrlReg, "N")
        clsWeb.gCargarComboBool(cmbCalifica, "N")
        clsWeb.gCargarComboBool(cmbMocho, "N")
        clsWeb.gCargarComboBool(cmbraza_descorne, "N")

        clsWeb.gCargarComboBool(cmbRegPrep, "N")
        clsWeb.gCargarComboBool(cmbADNCria, "N")
        clsWeb.gCargarComboBool(cmbADNCriaTE, "N")
        clsWeb.gCargarComboBool(cmbADNHembra, "N")
        clsWeb.gCargarComboBool(cmbADNMacho, "N")
        clsWeb.gCargarComboBool(cmbMuestraCrias, "N")
        clsWeb.gCargarComboBool(cmbReqDiag, "N")
        clsWeb.gCargarComboBool(cmbReqInsp, "N")
        clsWeb.gCargarComboBool(cmbReqInspMay25, "N")
        clsWeb.gCargarComboBool(cmbTipifCria, "N")
        clsWeb.gCargarComboBool(cmbTipifCriaTE, "N")
        clsWeb.gCargarComboBool(cmbTipifHembra, "N")
        clsWeb.gCargarComboBool(cmbTipifMacho, "N")
        clsWeb.gCargarComboBool(cmbAdmiClon, "N")
        ' Dario 2013-08-27 cambio que permite en la parte de expedientes 
        ' modificar el nro de criador automatico su este atrubuto esta en true
        clsWeb.gCargarComboBool(Me.cmbraza_PermiteEditarNumeradorCriador, "S")
        ' Dario 2013-09-05 cambio que permite indicar si la raza
        ' inscribe o no en la SRA para 14 % bonis lab
        clsWeb.gCargarComboBool(Me.cmbraza_InscribeSRA, "S")
        ' indica si factura en sra para factura automatica desde rrgg
        clsWeb.gCargarComboBool(Me.cmbraza_FacturaSRA, "S")
        'GSZ admite descorne 09/04/2015
        ' Modificado : GSZ 23/04/2015
        clsWeb.gCargarComboBool(Me.cmbraza_descorne, "S")
        clsWeb.gCargarComboBool(Me.cmbraza_Condicional, "S")
        clsWeb.gCargarComboBool(Me.cmbraza_maneja_reg_id, "S")
        clsWeb.gCargarComboBool(Me.cmbInformaLaboratorio, "S")
        clsWeb.gCargarComboBool(Me.cmbControlaSNPS, "S")
        ' Dario 2014-10-16
        clsWeb.gCargarComboBool(Me.cmbADNFlushing, "S")
        ' Dario 2015-12-09
        clsWeb.gCargarComboBool(Me.cmbNoControlaServicios, "N")
        clsWeb.gCargarComboBool(Me.cmbNoControlaTE, "N")
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object
        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtCodigo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "raza_codi")
        txtDescripcion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "raza_desc")
        txtAbreviatura.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "raza_abre")
        txtCaract.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "raza_cara")
    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub DataGridDocum_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDocum.EditItemIndex = -1
            If (grdDocum.CurrentPageIndex < 0 Or grdDocum.CurrentPageIndex >= grdDocum.PageCount) Then
                grdDocum.CurrentPageIndex = 0
            Else
                grdDocum.CurrentPageIndex = E.NewPageIndex
            End If
            grdDocum.DataSource = mdsDatos.Tables(mstrTablaDocum)
            grdDocum.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub DataGridRazasPadres_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdRazasPadres.EditItemIndex = -1
            If (grdRazasPadres.CurrentPageIndex < 0 Or grdRazasPadres.CurrentPageIndex >= grdRazasPadres.PageCount) Then
                grdRazasPadres.CurrentPageIndex = 0
            Else
                grdRazasPadres.CurrentPageIndex = E.NewPageIndex
            End If
            grdRazasPadres.DataSource = mdsDatos.Tables(mstrTablaRazasPadres)
            grdRazasPadres.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd += " @raza_id = " & IIf(cmbRazaFil.Valor.ToString = "", "null", cmbRazaFil.Valor.ToString)
            mstrCmd += ", @raza_espe_id = " & IIf(cmbEspecieFil.Valor.ToString = "", "null", cmbEspecieFil.Valor.ToString)
            ' Dario 2013-12-13 puesto y comentado por comprotamiento extra�o en el paginado, 
            'If (cmbRazaFil.Valor.ToString <> "") Then
            '    grdDato.CurrentPageIndex = 0
            'End If
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearCmbAnalisis()
        If Not hdnReqADN.Checked Then
            cmbADNCria.Enabled = False
            cmbADNCriaTE.Enabled = False
            cmbADNHembra.Enabled = False
            cmbADNMacho.Enabled = False
            cmbADNCria.ValorBool = False
            cmbADNCriaTE.ValorBool = False
            cmbADNHembra.ValorBool = False
            cmbADNMacho.ValorBool = False
            lblAnalisisADN.Visible = True
        Else
            cmbADNCria.Enabled = True
            cmbADNCriaTE.Enabled = True
            cmbADNHembra.Enabled = True
            cmbADNMacho.Enabled = True
            lblAnalisisADN.Visible = False
        End If
        If Not hdnReqTipi.Checked Then
            cmbTipifCria.Enabled = False
            cmbTipifCriaTE.Enabled = False
            cmbTipifHembra.Enabled = False
            cmbTipifMacho.Enabled = False
            cmbTipifCria.ValorBool = False
            cmbTipifCriaTE.ValorBool = False
            cmbTipifHembra.ValorBool = False
            cmbTipifMacho.ValorBool = False
            lblAnalisisTipif.Visible = True
        Else
            cmbTipifCria.Enabled = True
            cmbTipifCriaTE.Enabled = True
            cmbTipifHembra.Enabled = True
            cmbTipifMacho.Enabled = True
            lblAnalisisTipif.Visible = False
        End If
    End Sub
    Private Sub mSetearEditorDocum(ByVal pbooAlta As Boolean)
        btnBajaDocum.Enabled = Not (pbooAlta)
        btnModiDocum.Enabled = Not (pbooAlta)
        btnAltaDocum.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorRazasPadres(ByVal pbooAlta As Boolean)
        btnBajaRazasPadres.Enabled = Not (pbooAlta)
        btnModiRazasPadres.Enabled = Not (pbooAlta)
        btnAltaRazasPadres.Enabled = pbooAlta
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mLimpiar()
        mCargarCombosBis()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        clsWeb.gCargarCombo(mstrConn, "rg_analisis_resul_cargar @raza_id=" & hdnId.Text, Me.cmbResuADNHabHembDona, "id", "descrip_codi", "S")

        mCrearDataSet(hdnId.Text)
        If mdsDatos.Tables(0).Rows.Count > 0 Then
            lblTitu.Text = "Especie " & cmbEspecieFil.SelectedItem.Text
            With mdsDatos.Tables(0).Rows(0)
                '--------------------------------------------------------------
                'Para el reporte
                hdnRazaID.Text = .Item("raza_id")
                hdnEspecieId.Text = .Item("raza_espe_id")
                hdnDescRaza.Text = .Item("raza_codi") & " - " & .Item("raza_desc")
                'mstrEspeDesc = cmbEspecieFil.SelectedItem.Text
                '--------------------------------------------------------------
                txtCodigo.Valor = .Item("raza_codi")
                txtDescripcion.Valor = .Item("raza_desc")
                txtAbreviatura.Valor = .Item("raza_abre")
                    txtPromVida.Valor = IIf(.Item("raza_prom_vida").ToString.Length > 0, .Item("raza_prom_vida"), String.Empty)
                txtDiasGest.Valor = .Item("raza_gesta_dias")
                txtDiasGestMax.Valor = .Item("raza_gesta_dias_max")
                txtDiasGestMin.Valor = .Item("raza_gesta_dias_min")
                txtDiasServCamp.Valor = .Item("raza_serv_dias")
                txtDiasInsem.Valor = .Item("raza_inse_dias")
                txtDiasParto.Valor = .Item("raza_part_dias")
                cmbRegPrep.ValorBool = .Item("raza_regi_prep")
                cmbCtrlReg.ValorBool = .Item("raza_regi_prep_ctrol")
                cmbAdmiClon.ValorBool = .Item("raza_clon")
                    txtCaract.Valor = .Item("raza_cara").ToString
                    cmbCaractPrec.ValorBool = .Item("raza_cara_prec_nume")
                cmbAstado.ValorBool = .Item("raza_asta")
                cmbMocho.ValorBool = .Item("raza_mocho")
                cmbraza_descorne.ValorBool = .Item("raza_descorne")
                cmbCtrlColor.ValorBool = IIf(.Item("raza_color") = 1, True, False)
                cmbCtrlPela.ValorBool = IIf(.Item("raza_pela") = 1, True, False)
                cmbPerf.Valor = .Item("raza_perf")
                cmbReqInsp.ValorBool = .Item("raza_insp")
                cmbReqInspMay25.ValorBool = .Item("raza_insp_May25")
                cmbReqDiag.ValorBool = IIf(.Item("raza_diag") = 1, True, False)
                cmbCalifica.ValorBool = .Item("raza_cali")
                    cmbMuestraCrias.ValorBool = IIf(IsDBNull(.Item("raza_mues_crias")), String.Empty, .Item("raza_mues_crias"))
                    txtMuestraCriasPorc.Valor = IIf(IsDBNull(.Item("raza_mues_crias_porc")), String.Empty, .Item("raza_mues_crias_porc"))
                    txtMuestraCriasMin.Valor = IIf(IsDBNull(.Item("raza_mues_crias_mini")), String.Empty, .Item("raza_mues_crias_mini"))
                    cmbTipifMacho.ValorBool = .Item("raza_anal_tipi_macho")
                    cmbTipifHembra.ValorBool = .Item("raza_anal_tipi_hembra")
                    cmbTipifCria.ValorBool = .Item("raza_anal_tipi_cria")
                cmbTipifCriaTE.ValorBool = .Item("raza_anal_tipi_cria_te")
                cmbADNMacho.ValorBool = .Item("raza_anal_adn_macho")
                cmbADNHembra.ValorBool = .Item("raza_anal_adn_hembra")
                cmbADNCria.ValorBool = .Item("raza_anal_adn_cria")
                cmbADNCriaTE.ValorBool = .Item("raza_anal_adn_cria_te")
                '---Panel numeradores---
                '---Obtener Numeradores----------
                mCalcularNumeradores(.Item("raza_id"))
                '-------------------------------------
                ' Dario 2013-08-27 nuevo dato solapa numeradodes    
                Me.cmbraza_PermiteEditarNumeradorCriador.ValorBool = .Item("raza_PermiteEditarNumeradorCriador")
                ' Dario 2013-09-05 nuevo dato en solapa general  
                Me.cmbraza_InscribeSRA.ValorBool = .Item("raza_InscribeSRA")
                ' Dario 2016-02-22 nuevo dato en solapa general 
                Me.cmbraza_FacturaSRA.ValorBool = .Item("raza_FacturaSRA")
                ' Dario 2013-12-13 nuevo dato en solapa general  
                Me.cmbraza_Condicional.ValorBool = .Item("raza_Condicional")

                'GSZ 31/03/2014 nuevo dato en solapa general  

                Me.cmbraza_descorne.ValorBool = .Item("raza_descorne")

                Me.cmbraza_maneja_reg_id.ValorBool = .Item("raza_maneja_regt_id")
                If Not hdnEspecieId.Text Is System.DBNull.Value Then
                    Dim oEspecie As New SRA_Neg.Especie(mstrConn, Session("sUserId").ToString())
                    lblEspeNombNume.Text = oEspecie.GetEspecieNomNumeById(hdnEspecieId.Text) + " :"
                End If
                'GSZ 31/03/2014 nuevo dato en SOLAPA ANALISIS
                Me.cmbInformaLaboratorio.ValorBool = .Item("raza_Informa_Laboratorio")
                ' Dario 2014-06-26 nuevo dato en solapa parametros1
                If Not .IsNull("raza_Semen_Control_Crias") Then
                    Me.cmbControlStkSemen.Valor = .Item("raza_Semen_Control_Crias")
                Else
                    Me.cmbControlStkSemen.Valor = ""
                End If
                ' Dario 2014-06-26 nuevo dato en solapa analisis
                If Not .IsNull("raza_Maneja_Snps") Then
                    Me.cmbControlaSNPS.ValorBool = .Item("raza_Maneja_Snps")
                Else
                    Me.cmbControlaSNPS.Valor = ""
                End If
                ' Dario 2014-10-16 nuevo dato en solapa analisis
                If Not .IsNull("raza_adn_flushing") Then
                    Me.cmbADNFlushing.ValorBool = .Item("raza_adn_flushing")
                Else
                    Me.cmbADNFlushing.Valor = ""
                End If

                ' Dario 2015-12-09
                If Not .IsNull("raza_NoControlaServicio") Then
                    Me.cmbNoControlaServicios.ValorBool = .Item("raza_NoControlaServicio")
                Else
                    Me.cmbNoControlaServicios.Valor = ""
                End If
                If Not .IsNull("raza_NoControlaImplante") Then
                    Me.cmbNoControlaTE.ValorBool = .Item("raza_NoControlaImplante")
                Else
                    Me.cmbNoControlaTE.Valor = ""
                End If

                'fecha desde no se controla servicios
                If Not .IsNull("raza_NoControlaServicioDesde") Then txtFechaNoControlaServicios.Text = String.Format("{0:dd/MM/yyyy}", .Item("raza_NoControlaServicioDesde"))
                'fecha desde no se controla implantes
                If Not .IsNull("raza_NoControlaImplanteDesde") Then txtFechaNoControlaTE.Text = String.Format("{0:dd/MM/yyyy}", .Item("raza_NoControlaImplanteDesde"))

                If Not .IsNull("raza_Hembra_Donante_Ares_Id") Then
                    Me.cmbResuADNHabHembDona.Valor = .Item("raza_Hembra_Donante_Ares_Id")
                Else
                    Me.cmbResuADNHabHembDona.Valor = ""
                End If

                If Not .IsNull("raza_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("raza_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If

                mSetearLabels(.Item("raza_espe_id"))
            End With

            mSetearEditor(False)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub

    Public Sub mEditarDatosDocum(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDocum As DataRow
            Dim lstrCarpeta As String

            lstrCarpeta = System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString()
            lstrCarpeta += "/" + clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_raza_path") + "/"

            hdnDocumId.Text = E.Item.Cells(1).Text
            ldrDocum = mdsDatos.Tables(mstrTablaDocum).Select("razd_id=" & hdnDocumId.Text)(0)

            With ldrDocum
                txtDocumRefer.Valor = .Item("razd_refe")
                txtFoto.Valor = .Item("razd_path")
                chkEsImg.Checked = .Item("razd_imag")

                If Not .IsNull("razd_baja_fecha") Then
                    lblBajaDocum.Text = "Documento dado de baja en fecha: " & CDate(.Item("razd_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaDocum.Text = ""
                End If

                If Not .Item("razd_imag") Then
                    txtFoto.Text = ""
                    btnFotoVer.Visible = False
                Else
                    btnFotoVer.Visible = True
                    txtFoto.Text = .Item("razd_path")
                    btnFotoVer.Attributes.Add("onclick", String.Format("gAbrirVentanas('{0}', 14, '200','200','100','100');", lstrCarpeta + hdnDocumId.Text + "_" + txtFoto.Text.Substring(txtFoto.Text.LastIndexOf("\") + 1)))
                End If
                .Item("razd_raza_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("razd_imag") = chkEsImg.Checked
                .Item("razd_audi_user") = Session("sUserId").ToString()
                .Item("razd_baja_fecha") = DBNull.Value
                .Item("_estado") = "Activo"
            End With
            mSetearEditorDocum(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mCrearDataSet(ByVal pstrId As String)
        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaEspecies
        mdsDatos.Tables(2).TableName = mstrTablaDocum
        mdsDatos.Tables(3).TableName = mstrTablaRazasPadres

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        grdRazasPadres.DataSource = mdsDatos.Tables(mstrTablaRazasPadres)
        grdRazasPadres.DataBind()

        grdDocum.DataSource = mdsDatos.Tables(mstrTablaDocum)
        grdDocum.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mSetearLabels(ByVal pstrId As String)
        Dim ldrEspe As DataRow
        ldrEspe = mdsDatos.Tables(mstrTablaEspecies).Select("espe_id=" & pstrId)(0)

        With ldrEspe
            hdnGenealo.Checked = .Item("espe_genealo")
            hdnReqTipi.Checked = .Item("espe_anal_tipi")
            hdnReqADN.Checked = .Item("espe_anal_adn")
            lblTituGen1.Text = "(Datos de Referencia de la Especie Seleccionada)"
            lblTituGen2.Text = "(Datos de Referencia de la Especie Seleccionada)"
            lblAnalisisADN.Text = "(La Especie seleccionada no realiza An�lisis de ADN)"
            lblAnalisisTipif.Text = "(La Especie seleccionada no realiza An�lisis de Tipificacion)"
            lblEspPromV.Text = "(" & .Item("espe_prom_vida").ToString & ")"
            lblEspGestaDias.Text = "(" & IIf(.Item("espe_gesta_dias").ToString = "", "No Especifica", .Item("espe_gesta_dias")) & ")"
            lblEspGestMax.Text = "(" & IIf(.Item("espe_gesta_dias_max").ToString = "", "No Especifica", .Item("espe_gesta_dias_max")) & ")"
            lblEspGestMin.Text = "(" & IIf(.Item("espe_gesta_dias_min").ToString = "", "No Especifica", .Item("espe_gesta_dias_min")) & ")"
            lblEspAstado.Text = "(" & IIf(.Item("espe_asta").ToString = "", "No Especifica", IIf(.Item("espe_asta"), "S�", "No")) & ")"
            lblEspCaract.Text = "(" & IIf(.Item("espe_cara").ToString = "", "No Especifica", .Item("espe_cara")) & ")"
            lblEspCaractPrec.Text = "(" & IIf(.Item("espe_cara_prec_nume").ToString = "", "No Especifica", IIf(.Item("espe_cara_prec_nume"), "S�", "No")) & ")"
            lblEspCtrlColor.Text = "(" & IIf(.Item("espe_color").ToString = "", "No Especifica", IIf(.Item("espe_color"), "S�", "No")) & ")"
            lblEspCtrlPela.Text = "(" & IIf(.Item("espe_pela").ToString = "", "No Especifica", IIf(.Item("espe_pela"), "S�", "No")) & ")"
            lblEspCtrlReg.Text = "(" & IIf(.Item("espe_regi_prep_ctrol").ToString = "", "No Especifica", IIf(.Item("espe_regi_prep_ctrol"), "S�", "No")) & ")"
            lblEspDiasCamp.Text = "(" & IIf(.Item("espe_serv_dias").ToString = "", "No Especifica", .Item("espe_serv_dias")) & ")"
            lblEspDiasInse.Text = "(" & IIf(.Item("espe_inse_dias").ToString = "", "No Especifica", .Item("espe_inse_dias")) & ")"
            lblEspDiasParto.Text = "(" & IIf(.Item("espe_part_dias").ToString = "", "No Especifica", .Item("espe_part_dias")) & ")"
            lblEspMocho.Text = "(" & IIf(.Item("espe_mocho").ToString = "", "No Especifica", IIf(.Item("espe_mocho"), "S�", "No")) & ")"
            lblEspPerf.Text = "(" & IIf(.Item("espe_perf").ToString = "", "Ninguna", IIf(.Item("espe_perf").ToString = "0", "Lechera", IIf(.Item("espe_perf").ToString = "1", "Carne", "Ambas"))) & ")"
            lblEspRegPrep.Text = "(" & IIf(.Item("espe_regi_prep").ToString = "", "No Especifica", IIf(.Item("espe_regi_prep"), "S�", "No")) & ")"
        End With

    End Sub
    Private Sub mAgregar()
        Try
            'GSZ 25/02/2015 Se agrego la validacion d  que especie sea distinto de vacio
            If cmbEspecieFil.Valor Is System.DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar una especie")
            End If

            mLimpiar()
            mLimpiarLblEsp()
            mCargarCombosBis()


            txtCodigo.Text = darProximoCodigo()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnAlta.Enabled = True
            chkEsImg.Checked = False
            lblTitu.Text = "Especie " & cmbEspecieFil.SelectedItem.Text
            mSetearLabels(cmbEspecieFil.Valor.ToString)
            mMostrarPanel(True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try



    End Sub
    ''' Informa el proximo numero disponible y lo suguiere al usuario
    ''' MF 22/03/2013
    Private Function darProximoCodigo() As Integer
        Dim cEspecie As String

        cEspecie = Convert.ToString(cmbEspecieFil.Valor)

        darProximoCodigo = clsSQLServer.gExecuteScalar(mstrConn, "Razas_ProxCodigo " + cEspecie)

    End Function

    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnGenealo.Checked = False
        hdnReqADN.Checked = False
        hdnReqTipi.Checked = False
        chkEsImg.Checked = False
        lblBaja.Text = ""
        grdDato.CurrentPageIndex = 0
        mLimpiarTxt()
        mLimpiarCmb()
        mLimpiarLblEsp()
        mLimpiarDocum()
        mLimpiarRazasPadres()
        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarFil()
        '  cmbEspecieFil.Limpiar()
        If cmbEspecieFil.Valor.ToString = "" Then
            If Session("sEspeId") <> "" Then
                clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "T", "@raza_espe_id=" + Session("sEspeId"))
            End If
        Else
            clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "T", "@raza_espe_id=" + cmbEspecieFil.Valor.ToString)
        End If

        grdDato.CurrentPageIndex = 0
        mConsultar()
        mMostrarPanel(False)
        btnAgre.Visible = True
    End Sub
    Private Sub mLimpiarTxt()
        txtAbreviatura.Text = ""
        txtCaract.Text = ""
        txtCodigo.Text = ""
        txtDescripcion.Text = ""
        txtDiasGest.Text = ""
        txtDiasGestMax.Text = ""
        txtDiasGestMin.Text = ""
        txtDiasInsem.Text = ""
        txtDiasParto.Text = ""
        txtDiasServCamp.Text = ""
        txtDocumRefer.Text = ""
        txtFoto.Text = ""
        txtMuestraCriasMin.Text = ""
        txtMuestraCriasPorc.Text = ""
        txtPromVida.Text = ""
        txtNumTramite.Text = ""
        txtNumMachoDador.Text = ""
        txtNumHembraDona.Text = ""
        txtNumMachos.Text = ""
        txtNumHembras.Text = ""
        Me.txtFechaNoControlaServicios.Text = ""
        Me.txtFechaNoControlaTE.Text = ""
    End Sub
    Private Sub mLimpiarCmb()
        cmbADNCria.Limpiar()
        cmbADNCriaTE.Limpiar()
        cmbADNHembra.Limpiar()
        cmbADNMacho.Limpiar()
        cmbAstado.Limpiar()
        cmbCaractPrec.Limpiar()
        cmbCtrlColor.Limpiar()
        cmbCtrlPela.Limpiar()
        cmbCtrlReg.Limpiar()
        cmbAdmiClon.Limpiar()
        cmbMocho.Limpiar()
        cmbMuestraCrias.Limpiar()
        cmbPerf.Limpiar()
        cmbRegPrep.Limpiar()
        cmbReqDiag.Limpiar()
        cmbReqInsp.Limpiar()
        cmbReqInspMay25.Limpiar()
        cmbTipifCria.Limpiar()
        cmbTipifCriaTE.Limpiar()
        cmbTipifHembra.Limpiar()
        cmbTipifMacho.Limpiar()
        Me.cmbraza_PermiteEditarNumeradorCriador.Limpiar()
        Me.cmbraza_InscribeSRA.Limpiar()
        Me.cmbraza_FacturaSRA.Limpiar()

        Me.cmbraza_descorne.Limpiar()
        Me.cmbraza_Condicional.Limpiar()
        Me.cmbControlaSNPS.Limpiar()
        Me.cmbADNFlushing.Limpiar()
        Me.cmbControlStkSemen.Limpiar()
        Me.cmbNoControlaServicios.Limpiar()
        Me.cmbNoControlaTE.Limpiar()
        cmbraza_maneja_reg_id.Limpiar()
        cmbInformaLaboratorio.Limpiar()
    End Sub
    Private Sub mLimpiarDocum()
        hdnDocumId.Text = ""
        btnFotoVer.Visible = False
        chkEsImg.Checked = False
        txtDocumRefer.Text = ""
        txtFoto.Text = ""
        lblBajaDocum.Text = ""
        mSetearEditorDocum(True)
    End Sub
    Private Sub mLimpiarLblEsp()
        lblEspPromV.Text = ""
        lblEspGestaDias.Text = ""
        lblEspGestMax.Text = ""
        lblEspGestMin.Text = ""
        lblEspAstado.Text = ""
        lblEspCaract.Text = ""
        lblEspCaractPrec.Text = ""
        lblEspCtrlColor.Text = ""
        lblEspCtrlPela.Text = ""
        lblEspCtrlReg.Text = ""
        lblEspDiasCamp.Text = ""
        lblEspDiasInse.Text = ""
        lblEspDiasParto.Text = ""
        lblEspMocho.Text = ""
        lblEspPerf.Text = ""
        lblEspRegPrep.Text = ""
        lblTituGen1.Text = ""
        lblTituGen2.Text = ""
        lblAnalisisADN.Text = ""
        lblAnalisisTipif.Text = ""
        lblEspeNombNume.Text = ""
    End Sub
    Private Sub mImprimir()
        Try
            Dim params As String
            Dim lstrRptName As String = "Razas"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&raza_espe_id=" + IIf(cmbEspecieFil.Valor.ToString = "", "0", cmbEspecieFil.Valor.ToString)
            lstrRpt += "&raza_id=" + IIf(cmbRazaFil.Valor.ToString = "", "0", cmbRazaFil.Valor.ToString)
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
        mEstablecerPerfil()
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            mGuardarDatos()

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
            lobjGenerica.Alta()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
            lobjGenerica.Modi()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
        Private Sub mValidaDatos()

            clsWeb.gInicializarControles(Me, mstrConn)
            If hdnGenealo.Checked Then 'Tengo que cambiar x cada control en funcion de lo que me diga Sergio que es nec validar
                clsWeb.gValidarControles(Me)
            Else
                If txtCodigo.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el campo C�digo")
                End If
                If txtDescripcion.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el campo Descripci�n")
                End If
                If txtAbreviatura.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el campo Abreviatura")
                End If
            End If

            '  If cmbEspecieFil.Valor.Length = 0 Then
            ' Throw New AccesoBD.clsErrNeg("Debe seleccionar una Especie")
            'End If
        End Sub
    Private Sub mGuardarDatos()
        mValidaDatos()
        With mdsDatos.Tables(0).Rows(0)
                .Item("raza_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                If cmbEspecieFil.Valor.Length > 0 Then
                    .Item("raza_espe_id") = cmbEspecieFil.Valor
                End If
                .Item("raza_codi") = UCase(txtCodigo.Valor)
                .Item("raza_desc") = UCase(txtDescripcion.Valor)
                .Item("raza_abre") = UCase(txtAbreviatura.Valor)
                .Item("raza_baja_fecha") = DBNull.Value
                .Item("raza_prom_vida") = IIf(txtPromVida.Valor.Length > 0, txtPromVida.Valor, DBNull.Value)
                .Item("raza_gesta_dias") = txtDiasGest.Valor
                .Item("raza_gesta_dias_max") = txtDiasGestMax.Valor
                .Item("raza_gesta_dias_min") = txtDiasGestMin.Valor
                .Item("raza_serv_dias") = txtDiasServCamp.Valor
                .Item("raza_inse_dias") = txtDiasInsem.Valor
                .Item("raza_part_dias") = txtDiasParto.Valor
                .Item("raza_regi_prep") = cmbRegPrep.ValorBool
                .Item("raza_regi_prep_ctrol") = cmbCtrlReg.ValorBool
                .Item("raza_clon") = cmbAdmiClon.ValorBool
                .Item("raza_cara") = txtCaract.Valor
                .Item("raza_cara_prec_nume") = cmbCaractPrec.ValorBool
                .Item("raza_asta") = cmbAstado.ValorBool
                .Item("raza_mocho") = cmbMocho.ValorBool
                .Item("raza_color") = cmbCtrlColor.ValorBool
                .Item("raza_pela") = cmbCtrlPela.ValorBool
                .Item("raza_perf") = IIf(cmbPerf.Valor.ToString = "", DBNull.Value, cmbPerf.Valor)
                .Item("raza_insp") = cmbReqInsp.ValorBool
                ' GSZ 2014-07-16
                .Item("raza_insp_May25") = cmbReqInspMay25.ValorBool
                .Item("raza_diag") = cmbReqDiag.ValorBool
                .Item("raza_cali") = cmbCalifica.ValorBool
                .Item("raza_mues_crias") = cmbMuestraCrias.ValorBool
                .Item("raza_mues_crias_porc") = txtMuestraCriasPorc.Valor
                .Item("raza_mues_crias_mini") = txtMuestraCriasMin.Valor
                .Item("raza_anal_tipi_macho") = cmbTipifMacho.ValorBool
                .Item("raza_anal_tipi_hembra") = cmbTipifHembra.ValorBool
                .Item("raza_anal_tipi_cria") = cmbTipifCria.ValorBool
                .Item("raza_anal_tipi_cria_te") = cmbTipifCriaTE.ValorBool
                .Item("raza_anal_adn_macho") = cmbADNMacho.ValorBool
                .Item("raza_anal_adn_hembra") = cmbADNHembra.ValorBool
                .Item("raza_anal_adn_cria") = cmbADNCria.ValorBool
                .Item("raza_anal_adn_cria_te") = cmbADNCriaTE.ValorBool
                ' Dario 2013-08-27
                .Item("raza_PermiteEditarNumeradorCriador") = Me.cmbraza_PermiteEditarNumeradorCriador.ValorBool
                ' Dario 2013-09-05
                .Item("raza_InscribeSRA") = Me.cmbraza_InscribeSRA.ValorBool
                ' Dario 2016-02-22
                .Item("raza_FacturaSRA") = Me.cmbraza_FacturaSRA.ValorBool
                ' gsz 09/04/2015
                .Item("raza_descorne") = Me.cmbraza_descorne.ValorBool

                ' Dario 2013-12-13
                .Item("raza_Condicional") = Me.cmbraza_Condicional.ValorBool
                ' Dario 2014-06-26
                .Item("raza_Maneja_Snps") = Me.cmbControlaSNPS.ValorBool
                ' Dario 2014-10-16
                .Item("raza_adn_flushing") = Me.cmbADNFlushing.ValorBool
                ' Dario 2015-12-09
                .Item("raza_NoControlaServicio") = Me.cmbNoControlaServicios.ValorBool
                .Item("raza_NoControlaImplante") = Me.cmbNoControlaTE.ValorBool
                If txtFechaNoControlaServicios.Fecha.Length > 0 Then
                    .Item("raza_NoControlaServicioDesde") = Me.txtFechaNoControlaServicios.Fecha
                End If
                If txtFechaNoControlaTE.Fecha.Length > 0 Then
                    .Item("raza_NoControlaImplanteDesde") = Me.txtFechaNoControlaTE.Fecha
                End If
                ' 
                .Item("raza_Semen_Control_Crias") = IIf(Me.cmbControlStkSemen.Valor.ToString() = "", DBNull.Value, Me.cmbControlStkSemen.Valor)
                ' GSZ 10/04/2015 si es null cancela
                .Item("raza_Hembra_Donante_Ares_Id") = IIf(Me.cmbResuADNHabHembDona.Valor.ToString() = "", DBNull.Value, Me.cmbResuADNHabHembDona.Valor)
                .Item("raza_maneja_regt_id") = Me.cmbraza_maneja_reg_id.ValorBool
                .Item("raza_Informa_Laboratorio") = Me.cmbInformaLaboratorio.ValorBool
            End With
    End Sub

    'Seccion de Razas Padres
    Public Sub mEditarDatosRazasPadres(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrRazasPadres As DataRow
            hdnRazasPAdresId.Text = E.Item.Cells(1).Text

            ldrRazasPadres = mdsDatos.Tables(mstrTablaRazasPadres).Select("rara_id=" & hdnRazasPAdresId.Text)(0)

            With ldrRazasPadres
                If .IsNull("rara_sexo") Then
                    cmbSexo.Valor = ""
                Else
                    cmbSexo.Valor = IIf(.Item("rara_sexo") = True, "1", "0")
                End If
                cmbRaza.Valor = .Item("rara_padr_raza_id")
                If Not .IsNull("rara_baja_fecha") Then
                    lblBajaAutor.Text = "Autorizado dado de baja en fecha: " & CDate(.Item("rara_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaAutor.Text = ""
                End If
            End With
            mSetearEditorRazasPadres(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    Private Sub mActualizarRazasPadres()
        Try
            mGuardarDatosRazasPadres()

            mLimpiarRazasPadres()

            grdRazasPadres.DataSource = mdsDatos.Tables(mstrTablaRazasPadres)
            grdRazasPadres.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBajaRazasPadres()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaRazasPadres).Select("rara_id=" & CInt(hdnRazasPAdresId.Text))(0)
            row.Delete()

            grdRazasPadres.DataSource = mdsDatos.Tables(mstrTablaRazasPadres)
            grdRazasPadres.DataBind()
            mLimpiarRazasPadres()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosRazasPadres()
        Dim ldrRazasPadres As DataRow
        Dim lstrSexoGrilla As String
        Dim lstrErr As String

        If cmbRaza.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la raza.")
        End If
        Dim a As String
        If hdnRazasPAdresId.Text = "" Then
            ldrRazasPadres = mdsDatos.Tables(mstrTablaRazasPadres).NewRow
            ldrRazasPadres.Item("rara_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaRazasPadres), "rara_id")
        Else
            ldrRazasPadres = mdsDatos.Tables(mstrTablaRazasPadres).Select("rara_id=" & hdnRazasPAdresId.Text)(0)
        End If

        With ldrRazasPadres
            .Item("rara_raza_id") = cmbRazaFil.Valor
            .Item("rara_padr_raza_id") = cmbRaza.Valor
            .Item("_raza_codi") = mObtenerCodiXId(cmbRaza.NomOper, cmbRaza.Valor)

            If cmbSexo.Valor.ToString = "" Then
                .Item("rara_sexo") = System.DBNull.Value
            Else
                If cmbSexo.Valor.ToString = "1" Then
                    .Item("rara_sexo") = 1
                Else
                    .Item("rara_sexo") = 0
                End If
            End If
            .Item("_rara_sexo") = IIf(cmbSexo.Valor.ToString = "", "Ambos", cmbSexo.SelectedItem.Text)
            .Item("_raza_desc") = IIf(cmbRaza.Valor.ToString = "", "", cmbRaza.SelectedItem.Text)
            .Item("rara_audi_user") = Session("sUserId").ToString()
            .Item("rara_baja_fecha") = DBNull.Value
        End With
        If (hdnRazasPAdresId.Text = "") Then
            mdsDatos.Tables(mstrTablaRazasPadres).Rows.Add(ldrRazasPadres)
        End If
    End Sub

    Private Function mValidarSexoModi() As String
        'Valida que ingrese correctamente los sexos de la raza
        Dim mdsDatosAux As DataSet
        Dim ldrRazasPadresAux As DataRow
        Dim lstrSexoGrillaAux As String
        Dim lstrErr As String

        Try
            'Maneja un dataset auxiliar 
            mdsDatosAux = mClonarDataSet(mdsDatos, mstrTablaRazasPadres)
            ldrRazasPadresAux = mdsDatosAux.Tables(mstrTablaRazasPadres).Select("rara_id=" & hdnRazasPAdresId.Text)(0)
            ldrRazasPadresAux.Delete()

            'If mdsDatosAux.Tables(mstrTablaRazasPadres).Rows.Count > 0 Then
            If mdsDatosAux.Tables(mstrTablaRazasPadres).Rows.Count > 0 Then
                lstrSexoGrillaAux = mdsDatosAux.Tables(mstrTablaRazasPadres).Rows(0).Item("_rara_sexo")
                If lstrSexoGrillaAux = "Ambos" Then
                    lstrErr = "Ya seleccion� el sexo de los padres."
                Else
                    If cmbSexo.SelectedItem.Text = lstrSexoGrillaAux Or cmbSexo.SelectedItem.Text = "Ambos" Then
                        lstrErr = "Debe seleccionar correctamente el sexo."
                    End If
                End If
            End If
            mValidarSexoModi = lstrErr

        Catch ex As Exception
            mValidarSexoModi = lstrErr
        End Try

    End Function

    Private Function mValidarSexoAlta() As String
        'Valida que ingrese correctamente los sexos de la raza
        Dim lstrErr As String
        Dim lstrSexoGrilla As String

        Try
            If grdRazasPadres.Items.Count > 0 Then
                lstrSexoGrilla = grdRazasPadres.Items(0).Cells(6).Text()     'Campo Sexo
                If grdRazasPadres.Items.Count = 2 Then
                    lstrErr = "Ya seleccion� el sexo de los padres."
                Else
                    If lstrSexoGrilla = "Ambos" Then
                        lstrErr = "Ya seleccion� el sexo de los padres."
                    Else
                        If cmbSexo.SelectedItem.Text = lstrSexoGrilla Or cmbSexo.SelectedItem.Text = "Ambos" Then
                            lstrErr = "Debe seleccionar correctamente el sexo."
                        End If
                    End If
                End If
            End If
            mValidarSexoAlta = lstrErr

        Catch ex As Exception
            mValidarSexoAlta = lstrErr
        End Try

    End Function

    Private Function mClonarDataSet(ByVal ds As DataSet, ByVal lstrTablaRazasPadres As String) As DataSet
        'Crea una copia del dataset sin que afecte al original

        Try
            Dim ldsAux As DataSet = ds.Clone()
            Dim lCopyRows() As DataRow = ds.Tables(lstrTablaRazasPadres).Select()
            Dim ldtAux As DataTable = ldsAux.Tables(lstrTablaRazasPadres)
            Dim lCopyRow As DataRow

            For Each lCopyRow In lCopyRows
                ldtAux.ImportRow(lCopyRow)
            Next
            mClonarDataSet = ldsAux

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Function

    Private Sub mLimpiarRazasPadres()
        hdnRazasPAdresId.Text = ""
        cmbRaza.Limpiar()
        cmbSexo.SelectedIndex = 0
        If cmbEspecieFil.Valor.ToString = "" Then
            If Session("sEspeId") <> "" Then
                clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "T", "@raza_espe_id=" + Session("sEspeId"))
            End If
        Else
            clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "T", "@raza_espe_id=" + cmbEspecieFil.Valor.ToString)
        End If
        '  clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "T", "@raza_espe_id=" + cmbEspecieFil.Valor.ToString)
        mSetearEditorRazasPadres(True)

    End Sub

    Private Function mObtenerCodiXId(ByVal strNomOper As String, ByVal strValor As String) As String
        'Se obtiene el codigo del combo a traves del ID
        Dim ds As New DataSet
        Dim lStrCmd As String
        lStrCmd = "exec " + strNomOper + " @id=" + strValor
        ds = clsSQLServer.gExecuteQuery(mstrConn, lStrCmd)
        If ds.Tables(0).Rows.Count > 0 Then
            Return ds.Tables(0).Rows(0).Item("codi")
        Else
            Return ""
        End If
    End Function

    Private Sub mCalcularNumeradores(ByVal pstrRazaID As String)
        Try
            Dim lds As New DataSet
            Dim lStrCmd As String
            lStrCmd = "exec razas_numera_consul " + pstrRazaID
            lds = clsSQLServer.gExecuteQuery(mstrConn, lStrCmd)
            If lds.Tables(0).Rows.Count > 0 Then
                With lds.Tables(0).Rows(0)
                    txtNumTramite.Text = IIf(.Item("_prdt_tram_nume_max") Is DBNull.Value, " ", .Item("_prdt_tram_nume_max"))
                    lblNumHembras.Text = "Hembras (" & IIf(.Item("_numero") Is DBNull.Value, " ", .Item("_numero")) & "):"
                    lblNumMachos.Text = "Machos (" & IIf(.Item("_numero") Is DBNull.Value, " ", .Item("_numero")) & "):"
                    lblNumMachoDona.Text = "Macho Dador (" & IIf(.Item("_numero") Is DBNull.Value, " ", .Item("_numero")) & "):"
                    lblNumHembraDona.Text = "Hembra Donante (" & IIf(.Item("_numero") Is DBNull.Value, " ", .Item("_numero")) & "):"
                    txtNumMachoDador.Text = IIf(.Item("_prdt_dona_nume_macho") Is DBNull.Value, "", .Item("_prdt_dona_nume_macho"))
                    txtNumHembraDona.Text = IIf(.Item("_prdt_dona_nume_hem") Is DBNull.Value, "", .Item("_prdt_dona_nume_hem"))
                    txtNumMachos.Text = IIf(.Item("_prdt_sra_nume_macho") Is DBNull.Value, "", .Item("_prdt_sra_nume_macho"))
                    txtNumHembras.Text = IIf(.Item("_prdt_sra_nume_hem") Is DBNull.Value, "", .Item("_prdt_sra_nume_hem"))
                End With
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    'Seccion de Fotos
    Private Sub mActualizarDocum()
        Try
            mGuardarDatosDocum()

            mLimpiarDocum()
            grdDocum.DataSource = mdsDatos.Tables(mstrTablaDocum)
            grdDocum.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBajaDocum()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDocum).Select("razd_id=" & hdnDocumId.Text)(0)
            row.Delete()
            grdDocum.DataSource = mdsDatos.Tables(mstrTablaDocum)
            grdDocum.DataBind()
            mLimpiarDocum()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mGuardarDatosDocum()
        Dim ldrDocum As DataRow

        If txtFoto.Valor.ToString = "" And filDocumDoc.Value = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar una Foto.")
        End If
        If txtDocumRefer.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar una Referencia.")
        End If

        If hdnDocumId.Text = "" Then
            ldrDocum = mdsDatos.Tables(mstrTablaDocum).NewRow
            ldrDocum.Item("razd_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocum), "razd_id")
        Else
            ldrDocum = mdsDatos.Tables(mstrTablaDocum).Select("razd_id=" & hdnDocumId.Text)(0)
        End If

        With ldrDocum
            .Item("razd_refe") = txtDocumRefer.Valor

            If filDocumDoc.Value <> "" Then
                .Item("razd_path") = filDocumDoc.Value
            Else
                .Item("razd_path") = txtFoto.Valor
            End If
            .Item("_parampath") = "para_clie_raza_path"
            .Item("razd_raza_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("razd_imag") = chkEsImg.Checked
            .Item("_razd_imag") = IIf(chkEsImg.Checked, "S�", "No")
            .Item("razd_audi_user") = Session("sUserId").ToString()
            .Item("razd_baja_fecha") = DBNull.Value
            .Item("_estado") = "Activo"
        End With
        If (hdnDocumId.Text = "") Then
            mdsDatos.Tables(mstrTablaDocum).Rows.Add(ldrDocum)
        End If
    End Sub

#End Region

#Region "Eventos de Controles"
    Private Sub mShowTabs(ByVal Tab As Byte)
        Try
            panGeneral.Visible = False
            lnkGeneral.Font.Bold = False
            panGenealogia1.Visible = False
            lnkGenealogia1.Font.Bold = False
            panGenealogia2.Visible = False
            lnkGenealogia2.Font.Bold = False
            PanAnalisis.Visible = False
            lnkAnalisis.Font.Bold = False
            PanRazasPadres.Visible = False
            lnkRazasPadres.Font.Bold = False
            panDocumentos.Visible = False
            lnkDocumentos.Font.Bold = False
            panNumeradores.Visible = False
            lnkNumeradores.Font.Bold = False

            Select Case Tab
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                Case 2
                    If Not hdnGenealo.Checked Then
                        mShowTabs(1)
                        Throw New AccesoBD.clsErrNeg("Debe controlar Genealog�a.")
                    End If
                    panGenealogia1.Visible = True
                    lnkGenealogia1.Font.Bold = True
                Case 3
                    If Not hdnGenealo.Checked Then
                        mShowTabs(1)
                        Throw New AccesoBD.clsErrNeg("Debe controlar Genealog�a.")
                    End If
                    panGenealogia2.Visible = True
                    lnkGenealogia2.Font.Bold = True
                Case 4
                    If Not hdnGenealo.Checked Then
                        mShowTabs(1)
                        Throw New AccesoBD.clsErrNeg("Debe controlar Genealog�a.")
                    End If
                    If Not hdnReqADN.Checked And Not hdnReqTipi.Checked Then
                        mShowTabs(1)
                        Throw New AccesoBD.clsErrNeg("La especie correspondiente a esta raza no realiza An�lisis")
                    End If
                    PanAnalisis.Visible = True
                    lnkAnalisis.Font.Bold = True
                    mSetearCmbAnalisis()
                Case 5
                    If Not hdnGenealo.Checked Then
                        mShowTabs(1)
                        Throw New AccesoBD.clsErrNeg("Debe controlar Genealog�a.")
                    End If
                    PanRazasPadres.Visible = True
                    lnkRazasPadres.Font.Bold = True
                Case 6
                    panDocumentos.Visible = True
                    lnkDocumentos.Font.Bold = True
                Case 7
                    panNumeradores.Visible = True
                    lnkNumeradores.Font.Bold = True
            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            mConsultar()
            mMostrarPanel(False)
            btnAgre.Visible = True

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnLimpFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFil()
    End Sub
    'Botones generales
    Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    'Botones de Documentos
    Private Sub btnAltaDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocum.Click
        mActualizarDocum()
    End Sub
    Private Sub btnBajaDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocum.Click
        mBajaDocum()
    End Sub
    Private Sub btnModiDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocum.Click
        mActualizarDocum()
    End Sub
    Private Sub btnLimpDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocum.Click
        mLimpiarDocum()
    End Sub
    'Botones de Razas Padres
    Private Sub btnAltaRazasPadres_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaRazasPadres.Click
        mActualizarRazasPadres()
    End Sub

    Private Sub btnBajaRazasPadres_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaRazasPadres.Click
        mBajaRazasPadres()
    End Sub

    Private Sub btnLimpRazasPadres_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpRazasPadres.Click
        mLimpiarRazasPadres()
    End Sub
    Private Sub btnModiRazasPadres_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiRazasPadres.Click
        mActualizarRazasPadres()
    End Sub
    'Solapas
    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkGenealogia1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGenealogia1.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkGenealogia2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGenealogia2.Click
        mShowTabs(3)
    End Sub
    Private Sub lnkAnalisis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkAnalisis.Click
        mShowTabs(4)
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        mImprimir()
    End Sub
    Private Sub lnkRazasPadres_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRazasPadres.Click
        mShowTabs(5)
    End Sub
    Private Sub lnkDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocumentos.Click
        mShowTabs(6)
    End Sub
    Private Sub lnkNumeradores_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNumeradores.Click
        mShowTabs(7)
    End Sub
    Private Sub cmbEspecieFil_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbEspecieFil.SelectedIndexChanged
        mConsultar()
    End Sub
    Private Sub btnListarReglas_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnListarReglas.Click
        Try
            Dim lstrRptName As String

            lstrRptName = "ReglasValiRaza"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&raza_id=" + hdnRazaID.Text
            lstrRpt += "&especie_id=" + hdnEspecieId.Text
            lstrRpt += "&raza_desc=" + hdnDescRaza.Text
            lstrRpt += "&especie_desc=" + cmbEspecieFil.SelectedItem.Text

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

End Class
End Namespace
