<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.DineroElectronico" CodeFile="DineroElectronico.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Dinero Electrónico</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="javascript:gSetearTituloFrame();"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="TableABM" style="WIDTH: 100%" cellSpacing="1" cellPadding="1" border="0">
							<tr>
								<td width="100%" colSpan="6"></td>
							</tr>
							<tr>
								<td width="100%" colSpan="6"></td>
							</tr>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="5" height="25"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion">Dinero Electrónico</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" AllowSorting="True" AllowPaging="True"
										BorderWidth="1px" BorderColor="WhiteSmoke" CellSpacing="1" GridLines="None" CellPadding="3" OnPageIndexChanged="DataGrid_Page" PageSize="12"
										OnEditCommand="mEditarDatos" AutoGenerateColumns="False" BorderStyle="None">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle cssclass="item"></AlternatingItemStyle>
										<ItemStyle cssclass="item2"></ItemStyle>
										<HeaderStyle cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="3%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="dine_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="dine_desc" ReadOnly="True" HeaderText="Descripci&#243;n" HeaderStyle-Width="50%"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="_cuba_id" ReadOnly="True" HeaderText="CuentaBancoId"></asp:BoundColumn>
											<asp:BoundColumn DataField="_cuba_nume" ReadOnly="True" HeaderText="Cuenta Bancaria" HeaderStyle-Width="40%"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ToolTip="Agregar un Nuevo Registro" ImageUrl="imagenes/btnImpr.jpg"
													ForeColor="Transparent" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif"
													CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD align="center" width="50"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
													ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
													IncludesUrl="includes/" BackColor="Transparent"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD><a name="editar"></a></TD>
								<TD colSpan="5">
									<DIV align="left"><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
											Width="100%" Visible="False">
											<P align="right">
												<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left">
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 10px"></TD>
														<TD style="HEIGHT: 10px" vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 27px" align="right">
															<asp:Label id="lblDesc" runat="server" cssclass="titulo">Descripción:</asp:Label>&nbsp;
														</TD>
														<TD style="HEIGHT: 27px" colSpan="2" height="27">
															<cc1:TextBoxtab id="txtDesc" runat="server" cssclass="cuadrotexto" Width="280px"></cc1:TextBoxtab></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 178px; HEIGHT: 27px" align="right">
															<asp:Label id="lblCuentaBanco" runat="server" cssclass="titulo">Cuenta Bancaria:</asp:Label></TD>
														<TD style="HEIGHT: 17px" vAlign="bottom" colSpan="2" height="17">
															<cc1:combobox class="combo" id="cmbCuentaBanco" onkeydown="mEnterPorTab();" runat="server" Width="300px"
																AutoPostBack="True"></cc1:combobox></TD>
													</TR>
													<TR>
														<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
													</TR>
													<TR height="30">
														<TD align="center" colSpan="3">
															<asp:button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:button>&nbsp;
															<asp:button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Baja"></asp:button>&nbsp;&nbsp;
															<asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Modificar"></asp:button>&nbsp;&nbsp;
															<asp:button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																Text="Limpiar"></asp:button></TD>
													</TR>
												</TABLE>
											</P>
										</asp:panel></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnPage" runat="server"></ASP:TEXTBOX><ASP:TEXTBOX id="hdnId" runat="server"></ASP:TEXTBOX></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.frmABM.hdnPage.value != '') {
		   document.location='#editar';
		   document.frmABM.txtDesc.focus();
		}
		</SCRIPT>
	</BODY>
</HTML>
