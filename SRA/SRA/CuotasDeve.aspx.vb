Namespace SRA

Partial Class CuotasDeve
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub




   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub
#End Region

#Region "Definici�n de Variables"
   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mCargarCombos()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnGene.Attributes.Add("onclick", "return(mDevengar());")
   End Sub


   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "cuotas_estru_perio", cmbDeveFil, "S")
   End Sub

#End Region
   Private Function mFecha() As String
      Dim lstrFecha As String

      lstrFecha = cmbDeveFil.SelectedItem.Text
      lstrFecha = lstrFecha.Substring(0, 10)
      Return (clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(lstrFecha)))
   End Function

   Private Sub mValidarDatos()
      If (cmbDeveFil.Valor Is DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el per�odo a devengar.")
      End If
    

   End Sub

   Public Sub mDevengar()

      Try
         Dim lstrCudcId As String
         Dim lstrFecha As String = ""
         'mValidarDatos()

         If cmbDeveFil.SelectedIndex = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un periodo para devengar.")
         End If

         If usrSociFil.Valor Is DBNull.Value Then
            lstrFecha = clsSQLServer.gCampoValorConsul(mstrConn, "cuotas_estru_verificar " & cmbDeveFil.Valor.ToString(), "fecha_deve")
            If lstrFecha = "N" Then
                Throw New AccesoBD.clsErrNeg("No se permiten devengamientos masivos para la fecha del d�a. Debe cambiar la fecha de devengamiento en la estructura de cuotas del per�odo seleccionado.")
            End If
         End If

         mstrCmd = "exec cuotas_devengamiento_consul"
         mstrCmd += " " & mFecha()
         mstrCmd += ", " & IIf(usrSociFil.Valor Is DBNull.Value, "0", usrSociFil.Valor.ToString)
         mstrCmd += ", " & Session("sUserId").ToString

         lstrCudcId = clsSQLServer.gExecuteScalarTrans(mstrConn, mstrCmd)

         Dim lstrRptName As String = "CuotasDeve"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&id=" + lstrCudcId
            lstrRpt += "&ult_id=0"

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

         mLimpiarFiltros()

         pangraba.Visible = True
         panproce.Visible = False

      Catch ex As Exception
         pangraba.Visible = True
         panproce.Visible = False
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Private Sub mLimpiarFiltros()
      usrSociFil.Valor = 0
      cmbDeveFil.Limpiar()
      pangraba.Visible = False
      panproce.Visible = True


   End Sub

   Private Sub btnGene_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnGene.Click

      mDevengar()
   End Sub

End Class
End Namespace
