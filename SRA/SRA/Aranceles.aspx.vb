' Dario 2013-09-04 se agraga nuevo valor para hacer las promociones por aranceles de laboratorio
Namespace SRA

Partial Class Aranceles
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents Label4 As System.Web.UI.WebControls.Label
    'Protected WithEvents CheckBox2 As System.Web.UI.WebControls.CheckBox


    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Public mstrCmd As String
    Public mstrTabla As String = SRA_Neg.Constantes.gTab_Aranceles
    Public mstrTablaRangos As String = SRA_Neg.Constantes.gTab_ArancelesRangos
    Public mstrRegGen As String = SRA_Neg.Constantes.Actividades.RegistrosGeneal�gicos '"3"
    Public mstrExpPal As String = SRA_Neg.Constantes.Actividades.EXPOSICION_GANADERA_PALERMO '"7"
    Private mstrConn As String
    Private mdsDatos As DataSet
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim mdsTmpDatos As New DataSet

            mstrConn = clsWeb.gVerificarConexion(Me)
            If Not Page.IsPostBack Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                mdsTmpDatos = AccesoBD.clsSQLServer.gExecuteQuery(mstrConn, "grupo_aranceles_consul @SacaDefault=1")
                Session("AranGrupDefa") = mdsTmpDatos.Tables(0).Rows(0).Item(0)

                mCargarCombos()
                mSetearEventos()
                mSetearMaxLength()
                mConsultar()
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaRangos


        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If


        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub
    Private Sub mSetearMaxLength()
        Dim lstrLong As Object
        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtCodigo.MaxLength = 5  'clsSQLServer.gObtenerLongitud(lstrLong, "aran_codi")Pantanettig 24/04/2008
        txtDescripcion.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "aran_desc")
        txtDescFecha.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "aran_rrgg_desc_fech")
        txtCodigoDesdeFil.MaxLength = 5   'clsSQLServer.gObtenerLongitud(lstrLong, "aran_codi")Pantanettig 24/04/2008
        txtCodigoHastaFil.MaxLength = 5   'clsSQLServer.gObtenerLongitud(lstrLong, "aran_codi")Pantanettig 24/04/2008
        txtDescripcionFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "aran_desc")
    End Sub
        Private Sub mCargarCombos()
            'Solapa general
            clsWeb.gCargarRefeCmb(mstrConn, "actividades", Me.cmbActividadesFil, "T")
            clsWeb.gCargarRefeCmb(mstrConn, "actividades", Me.cmbActividades, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", Me.cmbCuenta, "S", "null,null,'4,5'")
            clsWeb.gCargarRefeCmb(mstrConn, "cuentas_ctables", Me.cmbCuentaOpt, "S", "null,null,'2132'")
            clsWeb.gCargarRefeCmb(mstrConn, "grupo_aranceles", cmbGrupoAran, "S")
            cmbGrupoAran.Valor = Session("AranGrupDefa")
            cmbCuentaOpt.Enabled = False
            clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta='4'", cmbCentroCosto, "id", "descrip", "S")
            clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta='5'", cmbCentroCostoNega, "id", "descrip", "S")
            clsWeb.gCargarComboBool(Me.cmbClienteGenerico, "S")
            clsWeb.gCargarComboBool(Me.cmbFacturacionExenta, "S")
            ' Dario 2013-09-04 cambio 14% laboratorio
            clsWeb.gCargarComboBool(Me.cmbCalcBonifLab, "S")
            clsWeb.gCargarComboBool(Me.cmbCantidadAnimales, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "formulas", cmbForm, "S", "@origen=1")
            clsWeb.gCargarRefeCmb(mstrConn, "formulas_desc", cmbFormAux, "S", "@origen=1")
            clsWeb.gCargarRefeCmb(mstrConn, "servicios_tipos_labo", cmbTipoServLabo, "S")
            With cmbGravaTasa
                .Items.Insert(0, "(Seleccione)")
                .Items(0).Value = ""
                .Items.Insert(1, "Tasa Normal")
                .Items(1).Value = "1"
                .Items.Insert(2, "Tasa Reducida")
                .Items(2).Value = "2"
                .Items.Insert(3, "Sobretasa")
                .Items(3).Value = "3"
            End With
            cmbGravaTasa.Enabled = False
            'Solapa RRGG
            clsWeb.gCargarRefeCmb(mstrConn, "aranceles", Me.cmbSobretasaArancel, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "servicios_tipos_rrgg", Me.cmbTipoServicio, "S")
            clsWeb.gCargarComboBool(Me.cmbCalculaMeses, "S")
            clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "N")
            With Me.cmbPeriodoValidacion
                .Items.Insert(0, "(Seleccione)")
                .Items(0).Value = ""
                .Items.Insert(1, "Meses")
                .Items(1).Value = "1"
                .Items.Insert(2, "D�as")
                .Items(2).Value = "2"
            End With
            With Me.cmbPeriodoCalculo
                .Items.Insert(0, "(Seleccione)")
                .Items(0).Value = ""
                .Items.Insert(1, "Meses")
                .Items(1).Value = "1"
                .Items.Insert(2, "D�as")
                .Items(2).Value = "2"
            End With
            'Solapa Rangos
            clsWeb.gCargarRefeCmb(mstrConn, "aranceles_rangos", cmbRango, "N")
            clsWeb.gCargarComboBool(Me.cmbListaBotFil, "T")
            clsWeb.gCargarComboBool(Me.cmbListaBot, "S")
        End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
        Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.EditItemIndex = -1
                If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                    grdDato.CurrentPageIndex = 0
                Else
                    grdDato.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultar()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdRangos_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdRangos.EditItemIndex = -1
                If (grdRangos.CurrentPageIndex < 0 Or grdRangos.CurrentPageIndex >= grdRangos.PageCount) Then
                    grdRangos.CurrentPageIndex = 0
                Else
                    grdRangos.CurrentPageIndex = E.NewPageIndex
                End If
                mConsultarRangos()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mConsultar()
            mstrCmd = "exec " + mstrTabla + "_consul"
            mstrCmd = mstrCmd + " @descripcion = " + clsSQLServer.gFormatArg(txtDescripcionFil.Valor.ToString, SqlDbType.VarChar)
            mstrCmd = mstrCmd + ",@codigo = " + clsSQLServer.gFormatArg(txtCodigoDesdeFil.Valor.ToString, SqlDbType.VarChar)
            mstrCmd = mstrCmd + ",@codigoHasta = " + clsSQLServer.gFormatArg(txtCodigoHastaFil.Valor.ToString, SqlDbType.VarChar)
            mstrCmd = mstrCmd + ",@actividad = " + clsSQLServer.gFormatArg(cmbActividadesFil.Valor.ToString, SqlDbType.Int)
            mstrCmd = mstrCmd + ",@boti = " + IIf(cmbListaBotFil.SelectedValue = String.Empty, "null", clsSQLServer.gFormatArg(cmbListaBotFil.Valor.ToString, SqlDbType.Int))
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        End Sub

        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)

            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

            mCrearDataSet(hdnId.Text)
            If mdsDatos.Tables(0).Rows.Count > 0 Then
                With mdsDatos.Tables(0).Rows(0)

                    'Solapa General
                    hdnId.Text = .Item("aran_id")
                    txtCodigo.Text = .Item("aran_codi")
                    txtDescripcion.Text = .Item("aran_desc")
                    cmbActividades.Valor = .Item("aran_acti_id")
                    cmbGrupoAran.Valor = .Item("aran_grar_id")
                    cmbCuenta.Valor = .Item("aran_cuct_id")
                    cmbCuentaOpt.Valor = .Item("aran_secu_cuct_id").ToString
                    cmbCuentaOpt.Enabled = (cmbActividades.Valor.ToString = mstrExpPal)
                    cmbCentroCosto.Valor = .Item("aran_ccos_id")
                    cmbCentroCostoNega.Valor = .Item("aran_nega_ccos_id")
                    cmbClienteGenerico.Valor = IIf(.Item("aran_clie_gene"), "1", "0")
                    cmbFacturacionExenta.Valor = IIf(.Item("aran_exen"), "1", "0")
                    cmbCantidadAnimales.Valor = IIf(.Item("aran_cant_anim"), "1", "0")
                    If .Item("aran_grav") Then
                        cmbGravaTasa.Valor = .Item("aran_grava_tasa")
                        hdnHabComboIVA.Text = "0"
                        rbtIva.Checked = True
                    End If
                    cmbForm.Valor = .Item("aran_form_id")
                    cmbFormAux.Valor = cmbForm.Valor
                    If cmbFormAux.SelectedIndex <> -1 Then
                        If cmbFormAux.SelectedIndex <> 0 Then
                            txtFormDesc.Text = cmbFormAux.Items(cmbFormAux.SelectedIndex).Text
                        Else
                            txtFormDesc.Text = ""
                        End If
                    End If
                    cmbCantidadAnimales.Valor = IIf(.Item("aran_cant_anim"), "1", "0")
                    If cmbActividades.Valor Is DBNull.Value Then
                        cmbTipoServLabo.Enabled = False
                    ElseIf cmbActividades.Valor = 4 Then
                        cmbTipoServLabo.Enabled = True
                    Else
                        cmbTipoServLabo.Enabled = False
                    End If

                    If .Item("aran_tlab_id").ToString.Length > 0 Then
                        cmbTipoServLabo.Valor = .Item("aran_tlab_id")
                    End If
                    If .Item("aran_val_fecha").ToString.Length > 0 Then
                        txtValFecha.Valor = .Item("aran_val_fecha")
                    End If
                    chkControl.Checked = .Item("aran_control")
                    chkIIBBGrava.Checked = IIf(.Item("aran_iibb_grava") Is DBNull.Value, False, .Item("aran_iibb_grava"))
                    'Solapa RRGG
                    txtDescFecha.Text = .Item("aran_rrgg_desc_fech")
                    cmbTipoServicio.Valor = .Item("aran_rrgg_tser_id").ToString
                    cmbRaza.Valor = .Item("aran_raza_id").ToString
                    txtValMaxValidacion.Valor = .Item("aran_rrgg_vmax").ToString
                    txtValMinValidacion.Valor = .Item("aran_rrgg_vmin").ToString
                    cmbPeriodoValidacion.Valor = .Item("aran_rrgg_vper").ToString
                    txtValMaxCalculo.Valor = .Item("aran_rrgg_cmax").ToString
                    txtValMinCalculo.Valor = .Item("aran_rrgg_cmin").ToString
                    cmbPeriodoCalculo.Valor = .Item("aran_rrgg_cper").ToString
                    cmbCalculaMeses.Valor = IIf(.Item("aran_rrgg_mmyy"), "1", "0")
                    cmbSobretasaArancel.Valor = .Item("aran_rrgg_aran_id").ToString
                    chkTieneRango.Checked = .Item("aran_rrgg_rango").ToString
                    chkError.Checked = IIf(.IsNull("aran_rrgg_verror"), 0, .Item("aran_rrgg_verror"))
                    chkImprimeRemitos.Checked = IIf(.IsNull("aran_remi_rrgg"), 0, .Item("aran_remi_rrgg"))
                    ' Dario 2013-09-04 se agraga nuevo valor para hacer las promociones por aranceles de laboratorio
                    Me.cmbCalcBonifLab.Valor = IIf(.Item("aran_CalculaBonifLab"), "1", "0")

                    'Solapa Rangos
                    If .Item("aran_rrgg_rango") Then
                        cmbRango.Valor = .Item("_raac_id")
                        hdnRangoId.Text = IIf(.IsNull("_arra_id"), "", .Item("_arra_id"))
                        mConsultarRangos()

                    End If

                    If Not .IsNull("aran_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("aran_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If
                    Me.cmbListaBot.Valor = IIf(IsDBNull(.Item("aran_boti")), "0", IIf(.Item("aran_boti") = True, "1", "0"))
                End With


                lblTitu.Text = "Registro Seleccionado: " + txtCodigo.Text + " - " + txtDescripcion.Text


                mSetearEditor(False)
                mMostrarPanel(True)
                mShowTabs(1)
            End If
        End Sub
#End Region

#Region "Seteo de Controles"
        Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
            'cmbCuentaOpt.TextSize("7")
        End Sub
        Private Sub mAgregar()
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnAlta.Enabled = True
            mMostrarPanel(True)
        End Sub

        Private Sub mLimpiar()
            grdDato.CurrentPageIndex = 0
            lblTitu.Text = ""
            'Solapa General
            hdnId.Text = ""
            hdnRangoId.Text = ""
            txtCodigo.Text = ""
            txtDescripcion.Text = ""
            cmbActividades.Limpiar()
            cmbGrupoAran.Valor = Session("AranGrupDefa")
            cmbCuenta.Limpiar()
            cmbCuentaOpt.Limpiar()
            cmbCentroCosto.Limpiar()
            cmbCentroCostoNega.Limpiar()
            cmbClienteGenerico.Limpiar()
            cmbCantidadAnimales.Limpiar()
            cmbFacturacionExenta.Limpiar()
            Me.cmbCalcBonifLab.Limpiar()
            rbtGrav.Checked = True
            rbtIva.Checked = False
            cmbGravaTasa.Limpiar()
            cmbForm.Limpiar()
            cmbFormAux.Limpiar()
            txtFormDesc.Text = ""
            lblBaja.Text = ""
            txtValFecha.Text = ""
            chkControl.Checked = False
            'Solapa RRGG
            txtDescFecha.Text = ""
            cmbTipoServicio.Limpiar()
            cmbRaza.Limpiar()
            txtValMaxValidacion.Text = ""
            txtValMinValidacion.Text = ""
            cmbPeriodoValidacion.Limpiar()
            txtValMaxCalculo.Text = ""
            txtValMinCalculo.Text = ""
            cmbPeriodoCalculo.Limpiar()
            cmbCalculaMeses.Limpiar()
            cmbSobretasaArancel.Limpiar()
            cmbTipoServLabo.Limpiar()
            cmbTipoServLabo.Enabled = False
            chkTieneRango.Checked = False
            chkImprimeRemitos.Checked = False
            chkError.Checked = False

            hdnId.Text = ""
            hdnHabComboIVA.Text = "1"
            hdnValoresRangos.Text = ""
            hdnDatosPop.Text = ""
            cmbRango.Valor = ""

            grdDato.CurrentPageIndex = 0
            grdRangos.CurrentPageIndex = 0

            Me.cmbListaBot.Limpiar()

            mConsultarRangos()

            mCrearDataSet("")
            mSetearEditor(True)
            mShowTabs(1)
        End Sub
        Private Sub mLimpiarFil()
            txtCodigoDesdeFil.Text = ""
            txtCodigoHastaFil.Text = ""
            txtDescripcionFil.Text = ""
            cmbActividadesFil.Limpiar()
            cmbListaBotFil.Limpiar()
            grdDato.CurrentPageIndex = 0
            mConsultar()
        End Sub
        Private Sub mCerrar()
            mMostrarPanel(False)
        End Sub
        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            panDato.Visible = pbooVisi
            panSolapas.Visible = pbooVisi
            panBotones.Visible = pbooVisi
            btnAgre.Enabled = Not (panDato.Visible)
        End Sub
        Private Sub mImprimir()
            Try
                Dim params As String
                Dim lstrRptName As String = "Aranceles"
                Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                lstrRpt += "&codigoDesde=" + txtCodigoDesdeFil.Valor.ToString
                lstrRpt += "&codigoHasta=" + txtCodigoHastaFil.Valor.ToString
                lstrRpt += "&descripcion=" + txtDescripcionFil.Valor.ToString
                lstrRpt += "&actividad=" + cmbActividadesFil.Valor.ToString
                lstrRpt += "&boti=" + cmbListaBotFil.Valor.ToString
                lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                Response.Redirect(lstrRpt)
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
#End Region

#Region "Opciones de ABM"
        Private Sub mValidaDAtos()
            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)

            If rbtIva.Checked And cmbGravaTasa.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Tasa de IVA.")
            End If

            If cmbActividades.Valor.ToString = mstrRegGen Then
                If cmbTipoServicio.Valor.ToString = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Tipo de Servicio (panel RRGG).")
                End If

                If (txtValMaxValidacion.Text <> "" Or txtValMinValidacion.Text <> "") And cmbPeriodoValidacion.Valor.ToString = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Per�odo para Validaci�n (panel RRGG).")
                End If

                If (txtValMaxCalculo.Text <> "" Or txtValMinCalculo.Text <> "") And cmbPeriodoCalculo.Valor.ToString = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Per�odo para C�lculo (panel RRGG).")
                End If

                If cmbCalculaMeses.Valor.ToString = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar si Calcula en Meses (panel RRGG).")
                End If

            End If
            'If cmbActividades.Valor = SRA_Neg.Constantes.Actividades.Laboratorio And cmbTipoServLabo.Valor.ToString = "" Then
            '   Throw New AccesoBD.clsErrNeg("Debe ingresar el Tipo de Servicio (panel General).")
            'End If
        End Sub
        Private Function mGuardarDatos() As DataSet
        cmbCuentaOpt.Enabled = (cmbActividades.Valor.ToString = mstrExpPal)
        cmbGravaTasa.Enabled = (hdnHabComboIVA.Text = "0")

        mValidaDAtos()

        With mdsDatos.Tables(0).Rows(0)
            'Solapa General
            .Item("aran_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("aran_codi") = txtCodigo.Text
            .Item("aran_desc") = txtDescripcion.Text
            .Item("aran_acti_id") = cmbActividades.Valor.ToString
            .Item("aran_grar_id") = cmbGrupoAran.Valor
                .Item("aran_cuct_id") = cmbCuenta.Valor.ToString
                If cmbCuentaOpt.Valor.ToString.Length > 0 Then
                    .Item("aran_secu_cuct_id") = cmbCuentaOpt.Valor.ToString
                End If
                .Item("aran_ccos_id") = cmbCentroCosto.Valor.ToString
                .Item("aran_nega_ccos_id") = cmbCentroCostoNega.Valor.ToString
                .Item("aran_clie_gene") = cmbClienteGenerico.ValorBool
                .Item("aran_exen") = cmbFacturacionExenta.ValorBool
                .Item("aran_audi_user") = Session("sUserId").ToString()
                .Item("aran_grav") = rbtIva.Checked
                If cmbGravaTasa.Valor.Length > 0 Then
                    .Item("aran_grava_tasa") = cmbGravaTasa.Valor
                End If
                .Item("aran_form_id") = cmbForm.Valor.ToString
                .Item("aran_cant_anim") = cmbCantidadAnimales.ValorBool
                .Item("aran_baja_fecha") = DBNull.Value
                If cmbTipoServLabo.Valor.ToString.Length > 0 Then
                    .Item("aran_tlab_id") = cmbTipoServLabo.Valor
                End If
                If txtValFecha.Valor.ToString.Length > 0 Then
                    .Item("aran_val_fecha") = txtValFecha.Valor
                End If
                .Item("aran_control") = chkControl.Checked
                .Item("aran_iibb_grava") = chkIIBBGrava.Checked
                'Solapa RRGG
                .Item("aran_rrgg_desc_fech") = IIf(cmbActividades.Valor.ToString = mstrRegGen, txtDescFecha.Text, "")
                .Item("aran_rrgg_tser_id") = IIf(cmbActividades.Valor.ToString = mstrRegGen, cmbTipoServicio.Valor, System.DBNull.Value)
                .Item("aran_raza_id") = IIf(cmbActividades.Valor.ToString = mstrRegGen, IIf(cmbRaza.Valor.Length > 0, cmbRaza.Valor, System.DBNull.Value), System.DBNull.Value)
                .Item("aran_rrgg_vmax") = IIf(cmbActividades.Valor.ToString = mstrRegGen, IIf(txtValMaxValidacion.Valor.Length > 0, txtValMaxValidacion.Valor, System.DBNull.Value), System.DBNull.Value)
                .Item("aran_rrgg_vmin") = IIf(cmbActividades.Valor.ToString = mstrRegGen, IIf(txtValMinValidacion.Valor.Length > 0, txtValMinValidacion.Valor, System.DBNull.Value), System.DBNull.Value)
                .Item("aran_rrgg_vper") = IIf(cmbActividades.Valor.ToString = mstrRegGen, IIf(txtValMaxValidacion.Valor.ToString <> "" Or txtValMinValidacion.Valor.ToString <> "", cmbPeriodoValidacion.Valor, System.DBNull.Value), System.DBNull.Value)
                .Item("aran_rrgg_cmax") = IIf(cmbActividades.Valor.ToString = mstrRegGen, IIf(txtValMaxCalculo.Valor.Length > 0, txtValMaxCalculo.Valor, System.DBNull.Value), System.DBNull.Value)
                .Item("aran_rrgg_cmin") = IIf(cmbActividades.Valor.ToString = mstrRegGen, IIf(txtValMinCalculo.Valor.Length > 0, txtValMinCalculo.Valor, System.DBNull.Value), System.DBNull.Value)
                .Item("aran_rrgg_cper") = IIf(cmbActividades.Valor.ToString = mstrRegGen, IIf(txtValMaxCalculo.Valor.ToString <> "" Or txtValMinCalculo.Valor.ToString <> "", cmbPeriodoCalculo.Valor, System.DBNull.Value), System.DBNull.Value)
                .Item("aran_rrgg_mmyy") = IIf(cmbActividades.Valor.ToString = mstrRegGen, IIf(cmbCalculaMeses.Valor = "", 0, 1), 0) ' System.DBNull.Value)
                .Item("aran_rrgg_aran_id") = IIf(cmbActividades.Valor.ToString = mstrRegGen, IIf(cmbSobretasaArancel.Valor.Length > 0, cmbSobretasaArancel.Valor, System.DBNull.Value), System.DBNull.Value)
                .Item("aran_rrgg_rango") = IIf(cmbActividades.Valor.ToString = mstrRegGen, chkTieneRango.Checked, 0)
                .Item("aran_rrgg_verror") = chkError.Checked
                ' .Item("rangos") = IIf(cmbActividades.Valor.ToString = mstrRegGen And chkTieneRango.Checked, hdnValoresRangos.Text, "")
                .Item("aran_remi_rrgg") = chkImprimeRemitos.Checked
                ' Dario 2013-09-04 se agraga nuevo valor para hacer las promociones por aranceles de laboratorio
                .Item("aran_CalculaBonifLab") = cmbCalcBonifLab.ValorBool
                .Item("aran_boti") = cmbListaBot.ValorBool
            End With

        mGuardarRangos()
    End Function
    Private Sub mGuardarRangos()
        'si cmbRango.Valor is null

        Dim ldr As DataRow

        If hdnRangoId.Text = "" Then
            ldr = mdsDatos.Tables(mstrTablaRangos).NewRow
            ldr.Item("arra_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaRangos), "arra_id")
        Else
            ldr = mdsDatos.Tables(mstrTablaRangos).Select("arra_id=" & hdnRangoId.Text)(0)
        End If

        With ldr
                .Item("arra_id") = clsSQLServer.gFormatArg(hdnRangoId.Text, SqlDbType.Int)
                If cmbRango.Valor.ToString.Length > 0 Then
                    .Item("arra_raac_id") = cmbRango.Valor
                End If
                .Item("arra_aran_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("arra_audi_user") = Session("sUserId").ToString()

            End With
        If (hdnRangoId.Text = "") Then
            mdsDatos.Tables(mstrTablaRangos).Rows.Add(ldr)
        End If


    End Sub
    Private Sub mAlta()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()
            Dim lobj As New SRA_Neg.Aranceles(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaRangos, mdsDatos, Server)
            ' Dim lobjGenerica As New SRA_Neg.Aranceles(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Server)

            lobj.Alta()

            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobj As New SRA_Neg.Aranceles(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaRangos, mdsDatos, Server)
            '  Dim lobjGenerica As New SRA_Neg.ar.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobj.Modi()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try


    End Sub
    Private Sub mBaja()
        Try
            Dim lobj As New SRA_Neg.Aranceles(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaRangos, mdsDatos, Server)
            '  Dim lobjGenerico As New SRA_Neg.Aranceles(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobj.Baja(hdnId.Text)

            grdDato.CurrentPageIndex = 0
            mMostrarPanel(False)
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mShowTabs(ByVal origen As Byte)
        Try
            Me.cmbGravaTasa.Enabled = (Me.hdnHabComboIVA.Text = "0")
            cmbCuentaOpt.Enabled = (cmbActividades.Valor.ToString = mstrExpPal)
            Select Case origen
                Case 1
                    panGeneral.Visible = True
                    lnkGeneral.Font.Bold = True
                    lnkRRGG.Font.Bold = False
                    panRRGG.Visible = False
                    lnkRangos.Font.Bold = False
                    panRangos.Visible = False
                Case 2
                    If Me.cmbActividades.Valor.ToString <> mstrRegGen Then
                        Throw New AccesoBD.clsErrNeg("La actividad debe ser correspondiente a Registros Geneal�gicos.")
                    End If
                    panGeneral.Visible = False
                    lnkGeneral.Font.Bold = False
                    lnkRRGG.Font.Bold = True
                    panRRGG.Visible = True
                    lnkRangos.Font.Bold = False
                    panRangos.Visible = False
                Case 3
                    If (Me.chkTieneRango.Checked = False And Me.cmbActividades.Valor.ToString = mstrRegGen) Or cmbActividades.Valor.ToString <> mstrRegGen Then
                        Throw New AccesoBD.clsErrNeg("Debe tener seleccionada la opci�n de Rangos en la solapa RRGG.")
                    End If
                    panGeneral.Visible = False
                    lnkGeneral.Font.Bold = False
                    lnkRRGG.Font.Bold = False
                    panRRGG.Visible = False
                    lnkRangos.Font.Bold = True
                    panRangos.Visible = True
            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Opciones de controles"
    Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub
    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFil()
    End Sub
    Private Sub btnImprimir_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
        mImprimir()
    End Sub
    Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkRRGG_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRRGG.Click
        mShowTabs(2)
    End Sub
    Private Sub lnkRangos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRangos.Click
        mShowTabs(3)
    End Sub
#End Region

#Region "rangos"
    Private Sub mConsultarRangos()
        Dim ldsTemp As DataSet

        Dim ds As New DataSet
        mstrCmd = "aranceles_rangos_busq " + clsSQLServer.gFormatArg(cmbRango.Valor.ToString, SqlDbType.Int)
        ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

        grdRangos.DataSource = ds
        grdRangos.DataBind()
    End Sub
#End Region

    Private Sub cmbRango_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbRango.SelectedIndexChanged

        mConsultarRangos()
    End Sub
End Class
End Namespace
