Imports SRA
Public Class CambioTatuaje
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'Protected WithEvents lblTituAbm As System.Web.UI.WebControls.Label
    'Protected WithEvents btnBusc As NixorControls.BotonImagen
    'Protected WithEvents btnLimpFil As NixorControls.BotonImagen
    'Protected WithEvents panFiltro As System.Web.UI.WebControls.Panel
    'Protected WithEvents grdDato As System.Web.UI.WebControls.DataGrid
    'Protected WithEvents lblTitu As System.Web.UI.WebControls.Label
    'Protected WithEvents imgClose As System.Web.UI.WebControls.ImageButton
    'Protected WithEvents panCabecera As System.Web.UI.WebControls.Panel
    'Protected WithEvents btnModi As System.Web.UI.WebControls.Button
    'Protected WithEvents panDato As System.Web.UI.WebControls.Panel
    'Protected WithEvents lblMens As System.Web.UI.WebControls.TextBox
    'Protected WithEvents hdnId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblProductoFil As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label
    'Protected WithEvents usrCriadorFil As usrClieDeriv
    'Protected WithEvents usrProductoFil As usrProdDeriv
    'Protected WithEvents hdnCriaFilId As System.Web.UI.WebControls.TextBox
    'Protected WithEvents lblCriador As System.Web.UI.WebControls.Label
    'Protected WithEvents txtRP As NixorControls.TextBoxTab
    'Protected WithEvents lblRP As System.Web.UI.WebControls.Label
    'Protected WithEvents lblCriadorNombre As System.Web.UI.WebControls.Label
    'Protected WithEvents lblProducto As System.Web.UI.WebControls.Label
    'Protected WithEvents lblProductoNombre As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRPviejo As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRPNumeViejo As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRPNumeViejoValor As System.Web.UI.WebControls.Label
    'Protected WithEvents lblRPNume As System.Web.UI.WebControls.Label
    'Protected WithEvents txtRPNume As NixorControls.TextBoxTab
    'Protected WithEvents lblRPviejoValor As System.Web.UI.WebControls.Label

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()

            If (Not Page.IsPostBack) Then
                mConsultar()
                clsWeb.gInicializarControles(Me, mstrConn)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        usrCriadorFil.FilClaveUnica = False
        usrCriadorFil.ColClaveUnica = False
        usrCriadorFil.ColDocuNume = False
        usrCriadorFil.ColCUIT = False
        usrCriadorFil.ColClaveUnica = False
        usrCriadorFil.FilClaveUnica = True
        usrCriadorFil.FilAgru = False
        usrCriadorFil.FilCriaNume = True
        usrCriadorFil.FilCUIT = False
        usrCriadorFil.FilDocuNume = True
        usrCriadorFil.FilTarjNume = False
        usrCriadorFil.Criador = True
        usrCriadorFil.Inscrip = True
        usrCriadorFil.ColCriaNume = True

        usrProductoFil.CriaId = hdnCriaFilId.Text
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"

    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        btnModi.Enabled = Not (pbooAlta)
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim lDsDatos As DataSet
        mLimpiar()
        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)
        lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        With lDsDatos.Tables(0).Rows(0)
            lblCriadorNombre.Text = .Item("_criador")
            lblProductoNombre.Text = .Item("prdt_nomb")
            lblRPviejoValor.Text = .Item("prdt_rp")
            lblRPNumeViejoValor.Text = .Item("prdt_rp_nume")
        End With
        mSetearEditor("", False)
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiarFiltros()
        usrCriadorFil.Limpiar()
        usrProductoFil.Limpiar()
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnModi.Enabled = False
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnCriaFilId.Text = ""
        lblCriadorNombre.Text = ""
        lblProductoNombre.Text = ""
        lblRPNumeViejoValor.Text = ""
        lblRPviejoValor.Text = ""
        txtRP.Text = ""
        txtRPNume.Text = ""
        mSetearEditor("", True)
    End Sub
    Private Sub mCerrar()
        mConsultarGrilla()
    End Sub
    Private Sub mConsultarGrilla()
        Try
            mConsultar()
            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panCabecera.Visible = True
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()
            Dim lobjinstnEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

            lobjinstnEnti.Alta()
            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()
            Dim lobjinstnEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)

            lobjinstnEnti.Modi()

            mConsultar()

            mMostrarPanel(False)

        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjinstnEnti As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjinstnEnti.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
    Private Function mGuardarDatos() As DataSet
        mValidarDatos()

        Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

        With ldsEsta.Tables(0).Rows(0)
            .Item("prdt_rp_nume") = txtRPNume.Valor
            .Item("prdt_rp") = txtRP.Valor
        End With

        Return ldsEsta
    End Function
#End Region

#Region "Eventos de Controles"
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs)
        mLimpiar()
    End Sub
    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFiltros()
    End Sub
#End Region

#Region "Detalle"
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_busq "

            mstrCmd += "@prdt_cria_id= " + usrCriadorFil.Valor.ToString
            mstrCmd += ",@prdt_id=" + usrProductoFil.Valor.ToString

            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region


End Class
