Namespace SRA

Partial Class PerformCarne
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = "perform_carne"
    Private mstrCmd As String
    Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            If (Not Page.IsPostBack) Then
                mCargarCombos()
                clsWeb.gInicializarControles(Me, mstrConn)
                mMostrarPanel(False)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_perfs='1,2'", cmbRazaFil, "id", "descrip", "T")
        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)

        usrProductoFil.mstrConn = mstrConn
        usrProductoFil.FiltroRazas = "@raza_perfs='1,2'"
        usrProductoFil.mCargarRazas()

        usrCriadorFil.mstrConn = mstrConn
        usrCriadorFil.FiltroRazas = "@raza_perfs='1,2'"
        usrCriadorFil.mCargarRazas()
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            mstrCmd = "exec " + mstrTabla + "_consul "
            mstrCmd = mstrCmd + "@prdt_cria_id=" + usrCriadorFil.Valor.ToString
            mstrCmd = mstrCmd + ",@prdt_raza_id=" + cmbRazaFil.Valor.ToString
            mstrCmd = mstrCmd + ",@pfca_prdt_id=" + usrProductoFil.Valor.ToString

            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim mdsDatos As DataSet
        mLimpiar()

        mdsDatos = clsSQLServer.gExecuteQuery(mstrConn, "exec " + mstrTabla + "_consul @pfca_prdt_id=" & E.Item.Cells(2).Text)
        mdsDatos.Tables(0).TableName = mstrTabla

        With mdsDatos.Tables(mstrTabla).Select()(0)
            hdnId.Text = E.Item.Cells(2).Text '.Item("_pfca_id")
            hdnPfca_prdt_id.Text = .Item("_prdt_id")

            lblTitu.Text = "Producto: " & .Item("_producto").ToString & " <br> Criador: " & .Item("_Criador").ToString

            txtPesoNacer.Text = IIf(.Item("prdt_peso_nacer") Is System.DBNull.Value, 0, .Item("prdt_peso_nacer"))
            txtDepPesoNacer.Text = IIf(.Item("prdt_epd_nacer") Is System.DBNull.Value, 0, .Item("prdt_epd_nacer"))
            txtExaPesoNacer.Text = ""

            txtPesoDeste.Text = IIf(.Item("prdt_peso_deste") Is System.DBNull.Value, 0, .Item("prdt_peso_deste"))
            txtDepPesoDeste.Text = IIf(.Item("prdt_epd_deste") Is System.DBNull.Value, 0, .Item("prdt_epd_deste"))
            txtExaPesoDeste.Text = ""

            txtPesoFinal.Text = IIf(.Item("prdt_peso_final") Is System.DBNull.Value, 0, .Item("prdt_peso_final"))
            txtDepPesoFinal.Text = IIf(.Item("prdt_epd_final") Is System.DBNull.Value, 0, .Item("prdt_epd_final"))
            txtExaPesoFinal.Text = ""

            ' Tabla perform_carne
            txtExaPesoNacer.Text = IIf(.Item("pfca_peso_nacer_exac") Is System.DBNull.Value, 0, .Item("pfca_peso_nacer_exac"))
            txtExaPesoDeste.Text = IIf(.Item("pfca_peso_deste_exac") Is System.DBNull.Value, 0, .Item("pfca_peso_deste_exac"))
            txtExaPesoFinal.Text = IIf(.Item("pfca_peso_final_exac") Is System.DBNull.Value, 0, .Item("pfca_peso_final_exac"))

            txtDepLeche.Text = IIf(.Item("pfca_leche") Is System.DBNull.Value, 0, .Item("pfca_leche"))
            txtExaLeche.Text = IIf(.Item("pfca_leche_exac") Is System.DBNull.Value, 0, .Item("pfca_leche_exac"))
            txtLecheCrece.Text = IIf(.Item("pfca_crec_leche") Is System.DBNull.Value, 0, .Item("pfca_crec_leche"))

            txtDepCircun.Text = IIf(.Item("pfca_circ_escr") Is System.DBNull.Value, 0, .Item("pfca_circ_escr"))
            txtExaCircun.Text = IIf(.Item("pfca_circ_escr_exac") Is System.DBNull.Value, 0, .Item("pfca_circ_escr_exac"))

            txtDepAreaOjo.Text = IIf(.Item("pfca_area_bife") Is System.DBNull.Value, 0, .Item("pfca_area_bife"))
            txtExaAreaOjo.Text = IIf(.Item("pfca_area_bife_exac") Is System.DBNull.Value, 0, .Item("pfca_area_bife_exac"))

            txtDepEngorda.Text = IIf(.Item("pfca_engo_dors") Is System.DBNull.Value, 0, .Item("pfca_engo_dors"))
            txtExaEngorda.Text = IIf(.Item("pfca_engo_dors_exac") Is System.DBNull.Value, 0, .Item("pfca_engo_dors_exac"))
        End With

        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        'Tabla Productos
        txtPesoNacer.Text = "0"
        txtDepPesoNacer.Text = "0"
        txtExaPesoNacer.Text = "0"
        txtPesoDeste.Text = "0"
        txtDepPesoDeste.Text = "0"
        txtExaPesoDeste.Text = "0"
        txtPesoFinal.Text = "0"
        txtDepPesoFinal.Text = "0"
        txtExaPesoFinal.Text = "0"

        'Tabla perform_carne
        txtExaPesoNacer.Text = "0"
        txtExaPesoDeste.Text = "0"
        txtExaPesoFinal.Text = "0"
        txtDepLeche.Text = "0"
        txtExaLeche.Text = "0"
        txtLecheCrece.Text = "0"
        txtDepCircun.Text = "0"
        txtExaCircun.Text = "0"
        txtDepAreaOjo.Text = "0"
        txtExaAreaOjo.Text = "0"
        txtDepEngorda.Text = "0"
        txtExaEngorda.Text = "0"
        grdDato.CurrentPageIndex = 0
    End Sub
    Private Sub mLimpiarFiltro()
        usrCriadorFil.Limpiar()
        cmbRazaFil.Limpiar()
        usrProductoFil.Limpiar()
        mConsultar()
    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltro.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible
        btnListar.Visible = Not panDato.Visible
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mModi()
        Try
            Dim ldsDatos As DataSet = mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos, context)
            lobjGenerica.Modi()

            mConsultar()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub
    Private Function mGuardarDatos() As DataSet
        Dim ldsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)
        mValidarDatos()

        With ldsDatos.Tables(0).Rows(0)
            'Tabla productos 

            .Item("prdt_peso_nacer") = txtPesoNacer.Text
            .Item("prdt_epd_nacer") = txtDepPesoNacer.Text

            .Item("prdt_peso_deste") = txtPesoDeste.Text
            .Item("prdt_epd_deste") = txtDepPesoDeste.Text

            .Item("prdt_peso_final") = txtPesoFinal.Text
            .Item("prdt_epd_final") = txtDepPesoFinal.Text

            'Tabla Perform_Carne
            .Item("_pfca_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("pfca_prdt_id") = hdnPfca_prdt_id.Text

            .Item("pfca_peso_nacer_exac") = txtExaPesoNacer.Text
            .Item("pfca_peso_deste_exac") = txtExaPesoDeste.Text
            .Item("pfca_peso_final_exac") = txtExaPesoFinal.Text

            .Item("pfca_leche") = txtDepLeche.Text
            .Item("pfca_leche_exac") = txtExaLeche.Text
            .Item("pfca_crec_leche") = txtLecheCrece.Text

            .Item("pfca_circ_escr") = txtDepCircun.Text
            .Item("pfca_circ_escr_exac") = txtExaCircun.Text

            .Item("pfca_area_bife") = txtDepAreaOjo.Text
            .Item("pfca_area_bife_exac") = txtExaAreaOjo.Text

            .Item("pfca_engo_dors") = txtDepEngorda.Text
            .Item("pfca_engo_dors_exac") = txtExaEngorda.Text
        End With

        Return ldsDatos
    End Function
#End Region

#Region "Eventos de Controles"
    Private Sub imgClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltro()
    End Sub
    Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub
    Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
        Try
            Dim lstrRptName As String

            lstrRptName = "PerformCarne"

            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&prdt_cria_id=" + usrCriadorFil.Valor.ToString
            lstrRpt += "&prdt_raza_id=" + cmbRazaFil.Valor.ToString
            lstrRpt += "&pfca_prdt_id=" + usrProductoFil.Valor.ToString
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region
End Class
End Namespace
