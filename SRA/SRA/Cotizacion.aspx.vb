Namespace SRA

Partial Class Cotizacion
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents btnList As NixorControls.BotonImagen
   Protected WithEvents Label1 As System.Web.UI.WebControls.Label
   Protected WithEvents Combobox1 As NixorControls.ComboBox

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = "cotiza_monedas"
   Private mdsDatos As DataSet
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mstrConn As String
    Private mintinsti As Int32
    Private mbooPermiAlta As Boolean
   Private mbooPermiBaja As Boolean
   Private mbooPermiModi As Boolean
   
   Private Enum Columnas As Integer
      Id = 1
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearEventos()

            mCargarCombos()
            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
        Dim lstrMoneValor As String
        clsWeb.gCargarRefeCmb(mstrConn, "monedas_sinDefa", cmbMonedaFil, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "monedas_sinDefa", cmbMoneda, "S")
        cmbMonedaFil.SelectedValue = clsSQLServer.gObtenerValorCampo(mstrConn, "parametros", "", "para_dolar_mone_id")
        ' cmbMoneda.SelectedValue = clsSQLServer.gObtenerValorCampo(mstrConn, "parametros", "", "para_dolar_mone_id")
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()

      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintinsti = Convert.ToInt32(Request.QueryString("fkvalor"))
      mEstablecerPerfil()
   End Sub

   Private Sub mEstablecerPerfil()

      If (Not clsSQLServer.gMenuPermi(CType(Opciones.Cotizaciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
         Response.Redirect("noaccess.aspx")
      End If

      mbooPermiAlta = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Cotizaciones_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      mbooPermiBaja = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Cotizaciones_Baja, String), (mstrConn), (Session("sUserId").ToString()))
      mbooPermiModi = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.Cotizaciones_Modificacion, String), (mstrConn), (Session("sUserId").ToString()))

      btnAlta.Visible = mbooPermiAlta
      btnBaja.Visible = mbooPermiBaja
      btnModi.Visible = mbooPermiModi
      btnLimp.Visible = (mbooPermiAlta Or mbooPermiModi)
      btnAgre.Enabled = (mbooPermiAlta Or mbooPermiModi)

   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         mstrCmd = "exec " + mstrTabla + "_consul"
                If txtFechaFil.Fecha.Length = 0 Then
                    mstrCmd += " @fecha_desde=null"
                Else
                    mstrCmd += " @fecha_desde=" + "'" + Format(txtFechaFil.Fecha, "yyyyMMdd") + "'"
                End If
                If txtFechaHastaFil.Fecha.Length = 0 Then
                    mstrCmd += " ,@fecha_hasta=null"
                Else
                    mstrCmd += " ,@fecha_hasta=" + "'" + Format(txtFechaHastaFil.Fecha, "yyyyMMdd") + "'"
                End If
            mstrCmd += ",@como_mone_id = " + IIf(cmbMonedaFil.Valor.ToString = "", "null", cmbMonedaFil.Valor)
            clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Public Sub mCrearDataSet(ByVal pstrId As String)
      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)
      mdsDatos.Tables(0).TableName = mstrTabla
      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If
      Session(mstrTabla) = mdsDatos
   End Sub
   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
      mCrearDataSet(hdnId.Text)
      mstrCmd = "exec " + mstrTabla + "_consul"
      mstrCmd += " " + hdnId.Text

      Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
      If ldsDatos.Tables(0).Rows.Count > 0 Then
         With ldsDatos.Tables(0).Rows(0)
            cmbMoneda.Valor = .Item("como_mone_id")
            txtCotiza.Text = .Item("como_impo_cotiza")
            txtFecha.Fecha = .Item("como_fecha_cotiza")
         End With

         mSetearEditor(False)
         mMostrarPanel(True)
      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
        mMostrarPanel(True)
        cmbMoneda.SelectedValue = clsSQLServer.gObtenerValorCampo(mstrConn, "parametros", "", "para_dolar_mone_id")
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      txtCotiza.Text = ""
      txtFecha.Text = ""
      cmbMoneda.Limpiar()
      mSetearEditor(True)
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
   Private Sub mGuardarDatos()
      mValidarDatos()

      mCrearDataSet(hdnId.Text)

      With mdsDatos.Tables(0).Rows(0)
         .Item("como_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("como_mone_id") = cmbMoneda.Valor
         .Item("como_fecha_cotiza") = txtFecha.Fecha
         .Item("como_impo_cotiza") = txtCotiza.Text
         .Item("_mone_desc") = cmbMoneda.SelectedItem.Text
      End With
   End Sub
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Alta()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos de Controles"
   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
#End Region
   Private Sub cmbFK_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Overloads Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      grdDato.CurrentPageIndex = 0
      mConsultar()
   End Sub
   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFil()
   End Sub
   Private Sub mLimpiarFil()
      txtFechaFil.Text = ""
      cmbMonedaFil.Limpiar()
      mConsultar()
   End Sub
   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
   End Sub
   Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
      Try
         Dim lstrRptName As String = "Cotizacion"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            Dim lintFechaD As Integer
            Dim lintFechaH As Integer
            'If txtFechaFil.Fecha.ToString = "" Then
            '   lintFecha = 0
            'Else
            '   lintFecha = CType(clsFormatear.gFormatFechaString(txtFechaFil.Text, "Int32"), Integer)
            '   End If
            '   If txtFechaHastaFil.Fecha.ToString = "" Then
            '       lintFechaHasta = 0
            '   Else
            '       lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            ''   End If
            'If txtFechaFil.Text <> "" Then
            '    'lstrRpt += CType(clsFormatear.gFormatFechaString(txtFechaFil.Text, "Int32"), Integer)
            'End If
            'If txtFechaHastaFil.Text <> "" Then
            '    'lstrRpt += "&fecha_hasta=" + CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            'End If

            If txtFechaFil.Text = "" Then
                lintFechaD = 0
            Else
                lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaFil.Text, "Int32"), Integer)
            End If

            lstrRpt += "&fecha_desde=" + lintFechaD.ToString

            If txtFechaHastaFil.Text = "" Then
                lintFechaH = 0
            Else
                lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
            End If

            lstrRpt += "&fecha_hasta=" + lintFechaH.ToString
            lstrRpt += "&como_mone_id=" + IIf(cmbMonedaFil.Valor.ToString = "", "null", cmbMonedaFil.Valor.ToString)

            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
