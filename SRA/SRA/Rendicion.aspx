<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Rendicion" CodeFile="Rendicion.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
<HEAD>
<title>Rendiciones</title>
<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
<meta content="JavaScript" name="vs_defaultgrupntScript">
<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
<script language="JavaScript" src="includes/utiles.js"></script>
<SCRIPT language="javascript">
		</SCRIPT>
</HEAD>
<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
<form id="frmABM" method="post" runat="server">
<!------------------ RECUADRO ------------------->
<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
<tr>
<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
</tr>
<tr>
<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
<td vAlign="middle" align="center">
<!----- CONTENIDO ----->
<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
<TR>
<TD width="100%" colSpan="3"></TD>
</TR>
<TR>
<TD width="5"></TD>
<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Rendiciones</asp:label></TD>
</TR>
<TR>
<TD></TD>
<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="97%"
										BorderWidth="0" Visible="True">
<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
<TR>
<TD>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD style="HEIGHT: 8px" width="24"></TD>
<TD style="HEIGHT: 8px" width="42"></TD>
<TD style="HEIGHT: 8px" width="26"></TD>
<TD style="HEIGHT: 8px"></TD>
<TD style="HEIGHT: 8px" width="26">
<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False"
																	OutImage="del.gif" BtnImage="edit.gif"
																	ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"
																	ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
<TD style="HEIGHT: 8px" width="26"></TD>
</TR>
<TR>
<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
<TR>
<TD>
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
<TD><!-- FOMULARIO -->
<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
<TR>
<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
<asp:Label id="lblFechaFil" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
<cc1:DateBox id="txtFechaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
</TR>
<TR>
<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
<TD background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
</TR>
<TR>
<TD background="imagenes/formfdofields.jpg"></TD>
<TD align="right" background="imagenes/formfdofields.jpg">
<asp:Label id="lblEmctFil" runat="server" cssclass="titulo">Centro Emisor Origen:</asp:Label>&nbsp;</TD>
<TD background="imagenes/formfdofields.jpg">
<cc1:combobox class="combo" id="cmbEmctFil" runat="server" Width="200px" AceptaNull="False"></cc1:combobox></TD>
</TR>
<TR>
<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
<TD background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
</TR>
<TR>
<TD background="imagenes/formfdofields.jpg"></TD>
<TD align="right" background="imagenes/formfdofields.jpg"></TD>
<TD background="imagenes/formfdofields.jpg">
<asp:CheckBox id="chkPendiFil" Text="Solo pendientes de recepci�n" Runat="server" CssClass="titulo"
																				Checked="True"></asp:CheckBox></TD>
</TR>
<TR>
<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
</TR>
<TR>
<TD style="WIDTH: 8%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
</TR>
</TABLE>
</TD>
<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
</TR>
</TABLE>
</TD>
</TR>
</TABLE>
</asp:panel></TD>
</TR>
<TR>
<TD colSpan="3" height="10"></TD>
</TR>
<TR>
<TD></TD>
<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1"
										HorizontalAlign="Center" AllowPaging="True">
<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
<FooterStyle cssclass="footer"></FooterStyle>
<Columns>
<asp:TemplateColumn HeaderStyle-Width="9px">
<ItemTemplate>
<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
</asp:LinkButton>
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn Visible="False" DataField="rend_id"></asp:BoundColumn>
<asp:BoundColumn DataField="rend_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
<asp:BoundColumn DataField="rend_nume" HeaderText="Nro"></asp:BoundColumn>
<asp:BoundColumn DataField="_rend_arqu_nume" HeaderText="Nro.Arqueo"></asp:BoundColumn>
<asp:BoundColumn DataField="_orig_emct_desc" HeaderText="C.Emi. Origen"></asp:BoundColumn>
<asp:BoundColumn DataField="_dest_emct_desc" HeaderText="C.Emi. Destino"></asp:BoundColumn>
<asp:BoundColumn DataField="_esta_desc" HeaderText="Estado"></asp:BoundColumn>
</Columns>
<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
</asp:datagrid></TD>
</TR>
<TR height="10">
<TD colSpan="3"></TD>
</TR>
<TR>
<TD width="5"></TD>
<TD vAlign="middle">&nbsp;
<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif"
										OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
										IncludesUrl="includes/" BackColor="Transparent" ToolTip="Generar Nuevo Arqueo"
										ImageDisable="btnNuev0.gif" Visible="False"></CC1:BOTONIMAGEN></TD>
<TD align="right" width="100%">
<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
<tr>
<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Rendici�n</asp:linkbutton></td>
<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkTotales" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="70px" Height="21px" CausesValidation="False"> Totales</asp:linkbutton></td>
<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkRepa" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" Height="21px" CausesValidation="False"> Valores</asp:linkbutton></td>
<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkRedo" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" Height="21px" CausesValidation="False"> Dep�sitos</asp:linkbutton></td>
<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
</tr>
</table>
</TD>
</TR>
<TR>
<TD></TD>
<TD align="center" colSpan="2"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
<TR>
<TD height="5">
<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
<TD vAlign="top" align="right">&nbsp;
<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
</TR>
<TR>
<TD style="WIDTH: 100%" colSpan="3">
<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
<TR>
<TD align="right" width="180">
<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro:</asp:Label>&nbsp;
</TD>
<TD>
<cc1:numberbox id="txtNume" runat="server" cssclass="cuadrotextodeshab" Width="65px" Enabled="false"></cc1:numberbox>&nbsp;</TD>
</TR>
<TR>
<TD align="right">
<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
<TD>
<cc1:DateBox id="txtFecha" disabled runat="server" cssclass="cuadrotextodeshab" Width="70px"
																		Obligatorio="true" MostrarBoton="False"
																		enabled="false"></cc1:DateBox></TD>
</TR>
<TR>
<TD align="right">
<asp:Label id="lblOrigEmct" runat="server" cssclass="titulo">Centro Emisor - Origen:</asp:Label>&nbsp;</TD>
<TD>
<CC1:TEXTBOXTAB id="txtOrigEmct" runat="server" cssclass="cuadrotextodeshab" Width="200px" enabled="false"></CC1:TEXTBOXTAB></TD>
</TR>
<TR>
<TD align="right">
<asp:Label id="lblDestEmct" runat="server" cssclass="titulo">Centro Emisor - Destino:</asp:Label>&nbsp;</TD>
<TD>
<CC1:TEXTBOXTAB id="txtDestEmct" runat="server" cssclass="cuadrotextodeshab" Width="200px" enabled="false"></CC1:TEXTBOXTAB></TD>
</TR>
</TABLE>
</asp:panel></TD>
</TR>
<TR>
<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
</TR>
<TR>
<TD style="WIDTH: 100%" colSpan="3">
<asp:panel id="panTotales" runat="server" cssclass="titulo" Visible="False" Width="100%">
<TABLE id="tabTotales" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
<TR>
<TD style="WIDTH: 100%" colSpan="2">
<asp:datagrid id="grdTotales" runat="server" width="95%" BorderWidth="1px" BorderStyle="None"
																		HorizontalAlign="Center"
																		CellSpacing="1" GridLines="None"
																		CellPadding="1" OnEditCommand="mEditarDatosTotales"
																		AutoGenerateColumns="False">
<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
<FooterStyle CssClass="footer"></FooterStyle>
<Columns>
<asp:TemplateColumn>
<HeaderStyle Width="20px"></HeaderStyle>
<ItemTemplate>
<asp:LinkButton id="lnkTotEdit" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
</asp:LinkButton>
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn Visible="False" DataField="pati_id"></asp:BoundColumn>
<asp:BoundColumn Visible="False" DataField="mone_id"></asp:BoundColumn>
<asp:BoundColumn DataField="mone_desc" HeaderText="Moneda"></asp:BoundColumn>
<asp:BoundColumn DataField="pati_desc" HeaderText="Tipo"></asp:BoundColumn>
<asp:BoundColumn DataField="total" HeaderText="Total" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
<asp:BoundColumn DataField="faltante" HeaderText="Faltante" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
</Columns>
</asp:datagrid></TD>
</TR>
<TR>
<TD>
<asp:panel id="panTotalesDatos" runat="server" cssclass="titulo" Visible="False" Width="100%">
<TABLE id="tabTotalesDatos" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
<TR>
<TD align="right" width="180">
<asp:Label id="lblValor" runat="server" cssclass="titulo">Tipo Valor:</asp:Label>&nbsp;</TD>
<TD>
<CC1:TEXTBOXTAB id="txtValor" runat="server" cssclass="cuadrotextodeshab" Width="200px" enabled="false"></CC1:TEXTBOXTAB></TD>
</TR>
<TR>
<TD align="right">
<asp:Label id="lblFaltante" runat="server" cssclass="titulo">Faltante:</asp:Label>&nbsp;
</TD>
<TD>
<cc1:numberbox id="txtFaltante" runat="server" cssclass="cuadrotexto" Width="65px" AceptaNull="False"></cc1:numberbox>&nbsp;</TD>
</TR>
<TR>
<TD vAlign="bottom" colSpan="2" height="5"></TD>
</TR>
<TR>
<TD vAlign="middle" align="center" colSpan="2" height="30">
<asp:Button id="btnModiTotal" runat="server" cssclass="boton" Width="70px" Text="Aceptar"></asp:Button>&nbsp;
<asp:Button id="btnLimpTotal" runat="server" cssclass="boton" Width="70px" Text="Cancelar"></asp:Button></TD>
</TR>
</TABLE>
</asp:panel></TD>
</TR>
</TABLE>
</asp:panel></TD>
</TR>
<TR>
<TD style="WIDTH: 100%" colSpan="3">
<asp:panel id="panRepa" runat="server" cssclass="titulo" Visible="False" Width="100%">
<TABLE id="Table5" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
<TR>
<TD style="WIDTH: 100%" colSpan="2">
<asp:datagrid id="grdRepa" runat="server" width="95%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																		HorizontalAlign="Center"
																		CellSpacing="1" GridLines="None"
																		CellPadding="1" OnPageIndexChanged="grdRepa_PageChanged"
																		OnEditCommand="mEditarDatosRepa"
																		AutoGenerateColumns="False">
<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
<FooterStyle CssClass="footer"></FooterStyle>
<Columns>
<asp:TemplateColumn>
<HeaderStyle Width="20px"></HeaderStyle>
<ItemTemplate>
<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
</asp:LinkButton>
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn Visible="False" DataField="Repa_id"></asp:BoundColumn>
<asp:BoundColumn DataField="_paco_desc" HeaderText="Pago"></asp:BoundColumn>
<asp:BoundColumn DataField="repa_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
<asp:BoundColumn DataField="_falta" HeaderText="Falta"></asp:BoundColumn>
</Columns>
<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
</asp:datagrid></TD>
</TR>
<TR>
<TD vAlign="bottom" colSpan="2" height="16"></TD>
</TR>
<TR>
<TD align="right" width="90">
<asp:Label id="lblPaco" runat="server" cssclass="titulo">Pago:</asp:Label>&nbsp;</TD>
<TD width="100%">
<cc1:combobox class="combo" id="cmbPaco" runat="server" Width="100%"></cc1:combobox></TD>
</TR>
<TR>
<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
</TR>
<TR id="trRepaFalta" runat="server">
<TD></TD>
<TD>
<asp:CheckBox id="chkRepaFalta" Text="Faltante" Runat="server" CssClass="titulo" Checked="false"></asp:CheckBox></TD>
</TR>
<TR>
<TD vAlign="bottom" colSpan="2" height="5"></TD>
</TR>
<TR>
<TD vAlign="middle" align="center" colSpan="2" height="30">
<asp:Button id="btnAltaRepa" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Agregar Pago"></asp:Button>&nbsp;
<asp:Button id="btnBajaRepa" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Eliminar Pago"></asp:Button>&nbsp;
<asp:Button id="btnModiRepa" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Modificar Pago"></asp:Button>&nbsp;
<asp:Button id="btnLimpRepa" runat="server" cssclass="boton" Visible="False" Width="100px" Text="Limpiar Pago"></asp:Button></TD>
</TR>
</TABLE>
</asp:panel></TD>
</TR>
<TR>
<TD style="WIDTH: 100%" colSpan="3">
<asp:panel id="panRedo" runat="server" cssclass="titulo" Visible="False" Width="100%">
<TABLE id="tabRedo" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
<TR>
<TD style="WIDTH: 100%" colSpan="2">
<asp:datagrid id="grdRedo" runat="server" width="95%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																		HorizontalAlign="Center"
																		CellSpacing="1" GridLines="None"
																		CellPadding="1" OnPageIndexChanged="grdRedo_PageChanged"
																		OnEditCommand="mEditarDatosRedo"
																		AutoGenerateColumns="False">
<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
<FooterStyle CssClass="footer"></FooterStyle>
<Columns>
<asp:TemplateColumn>
<HeaderStyle Width="20px"></HeaderStyle>
<ItemTemplate>
<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
</asp:LinkButton>
</ItemTemplate>
</asp:TemplateColumn>
<asp:BoundColumn Visible="False" DataField="redo_id"></asp:BoundColumn>
<asp:BoundColumn DataField="_depo_desc" HeaderText="Dep�sito"></asp:BoundColumn>
<asp:BoundColumn DataField="redo_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
<asp:BoundColumn DataField="_falta" HeaderText="Falta"></asp:BoundColumn>
</Columns>
<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
</asp:datagrid></TD>
</TR>
<TR>
<TD vAlign="bottom" colSpan="2" height="16"></TD>
</TR>
<TR>
<TD align="right" width="90">
<asp:Label id="lblDepo" runat="server" cssclass="titulo">Dep�sito:</asp:Label>&nbsp;</TD>
<TD width="100%">
<cc1:combobox class="combo" id="cmbDepo" runat="server" Width="100%"></cc1:combobox></TD>
</TR>
<TR id="trRedoFalta" runat="server">
<TD></TD>
<TD>
<asp:CheckBox id="chkRedoFalta" Text="Faltante" Runat="server" CssClass="titulo" Checked="false"></asp:CheckBox></TD>
</TR>
<TR>
<TD vAlign="bottom" colSpan="2" height="5"></TD>
</TR>
<TR>
<TD vAlign="middle" align="center" colSpan="2" height="30">
<asp:Button id="btnAltaRedo" runat="server" cssclass="boton" Width="110px" Text="Agregar Dep."></asp:Button>&nbsp;
<asp:Button id="btnBajaRedo" runat="server" cssclass="boton" Width="110px" Text="Eliminar Dep."></asp:Button>&nbsp;
<asp:Button id="btnModiRedo" runat="server" cssclass="boton" Width="110px" Text="Modificar Dep."></asp:Button>&nbsp;
<asp:Button id="btnLimpRedo" runat="server" cssclass="boton" Width="110px" Text="Limpiar Dep."></asp:Button></TD>
</TR>
</TABLE>
</asp:panel></TD>
</TR>
</TABLE>
</asp:panel><ASP:PANEL id="panBotones" Runat="server">
<TABLE width="100%">
<TR height="30">
<TD width="80">&nbsp;</TD>
<TD align="center"><A id="editar" name="editar"></A>
<asp:Button id="btnAlta" runat="server" cssclass="boton" Visible="False" Width="80px" Text="Alta"></asp:Button>&nbsp;
<asp:Button id="btnModi" runat="server" cssclass="boton" Visible="False" Width="80px" Text="Modificar"
														CausesValidation="False"></asp:Button>&nbsp;&nbsp;
<asp:Button id="btnRecep" runat="server" cssclass="boton" Width="80px" Text="Recibir" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
<asp:Button id="btnBaja" runat="server" cssclass="boton" Visible="False" Width="80px" Text="Baja"
														CausesValidation="False"></asp:Button>&nbsp;&nbsp;
<asp:Button id="btnLimp" runat="server" cssclass="boton" Visible="False" Width="80px" Text="Limpiar"
														CausesValidation="False"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;
<asp:Button id="btnAnul" runat="server" cssclass="boton" Width="80px" Text="Anular" CausesValidation="False"></asp:Button></TD>
<TD width="80">
<CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
														ImagesUrl="imagenes/" CambiaValor="False"
														OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
														ImageOver="btnImpr2.gif" ForeColor="Transparent"
														ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
</TR>
</TABLE>
</ASP:PANEL></TD>
</TR>
</TABLE>
<!--- FIN CONTENIDO ---></td>
<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
</tr>
<tr>
<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
</tr>
</table>
<!----------------- FIN RECUADRO ----------------->
<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnEmctId" runat="server"></asp:textbox><asp:textbox id="hdnDestEmctId" runat="server"></asp:textbox><asp:textbox id="hdnRepaId" runat="server"></asp:textbox><asp:textbox id="hdnRedoId" runat="server"></asp:textbox><asp:textbox id="hdnPatiId" runat="server"></asp:textbox><asp:textbox id="hdnMoneId" runat="server"></asp:textbox><asp:textbox id="hdnImpo" runat="server"></asp:textbox></DIV>
</form>
<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtNume"]!= null&&!document.all["txtNume"].disabled)
			document.all["txtNume"].focus();
		</SCRIPT>
</BODY>
</HTML>
