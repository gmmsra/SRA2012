Namespace SRA

Partial Class Promociones
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Promociones
   Private mstrCategorias As String = SRA_Neg.Constantes.gTab_PromoCategorias
   Private mstrConn As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      Edit = 0
      Id = 1
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mSetearMaxLength()
            mCargarCombo()
            mConsultar()

         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mCargarDatos(ByVal pstrId As String)
      Dim lbooActivo As Boolean
      mCrearDataSet(pstrId)

      With mdsDatos.Tables(0).Rows(0)
         hdnId.Text = .Item("prom_id").ToString()
         txtDesc.Valor = .Item("prom_desc")
         If Not .IsNull("prom_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("prom_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With

      mLimpiarCate()
      mSetearEditor("", False)
      mMostrarPanel(True)
      mShowTabs(1)
   End Sub

   Private Sub mCerrar()
      mConsultar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi

      btnAgre.Visible = Not panDato.Visible
      btnList.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      lnkCabecera.Font.Bold = True
      lnkCate.Font.Bold = False

      panCabecera.Visible = True
      panCate.Visible = False

      tabLinks.Visible = pbooVisi

      If Not pbooVisi Then
         mdsDatos = Nothing
         Session(mstrTabla) = Nothing
      End If
   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True
      panCabecera.Visible = False
      panCate.Visible = False

      lnkCabecera.Font.Bold = False
      lnkCate.Font.Bold = False

      Dim val As String

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Text = "Datos de la promoción: " & txtDesc.Text
         Case 2
            panCate.Visible = True
            lnkCate.Font.Bold = True
            lblTitu.Text = "Categorias de la promoción: " & txtDesc.Text
      End Select
   End Sub

   Private Sub mCargarCombo()
      clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "S", "@cate_paga=1")
   End Sub

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrCategorias
            btnBajaCate.Enabled = Not pbooAlta
            btnModiCate.Enabled = Not pbooAlta
            btnAltaCate.Enabled = pbooAlta
         Case Else
            btnBaja.Enabled = Not pbooAlta
            btnModi.Enabled = Not pbooAlta
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLong As Object

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtDesc.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "prom_desc")
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mCargarDatos(E.Item.Cells(Columnas.Id).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

   Private Sub lnkCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCate.Click
      mShowTabs(2)
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiar()
      mCrearDataSet("")
      lblTitu.Text = ""
      lblBaja.Text = ""
      hdnId.Text = ""
      txtDesc.Text = ""
      mShowTabs(1)

      mSetearEditor("", True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      mMostrarPanel(True)
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()
         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
         lobjGenerico.Alta()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         mGuardarDatos()
         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)
         lobjGenerico.Modi()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatos()
      mValidarDatos()

      With mdsDatos.Tables(0).Rows(0)
         .Item("prom_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("prom_desc") = txtDesc.Valor
         .Item("prom_baja_fecha") = DBNull.Value
      End With
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)
      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrCategorias

      mConsultarCate()
      mLimpiarCate()
      Session(mstrTabla) = mdsDatos
   End Sub
#End Region

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = mstrTabla
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#Region "Detalle"
   Public Sub mEditarDatosCate(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrCate As DataRow
         hdnPrcaId.Text = E.Item.Cells(1).Text
         ldrCate = mdsDatos.Tables(mstrCategorias).Select("prca_id=" & hdnPrcaId.Text)(0)

         With ldrCate
            cmbCate.Valor = .Item("prca_cate_id")
            txtImpo.Valor = .Item("prca_impo")
            txtPorc.Valor = .Item("prca_porc")
         End With

         mSetearEditor(mstrCategorias, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarCategoria()

      Dim ldrCate As DataRow

      If hdnPrcaId.Text = "" Then
         If mdsDatos.Tables(mstrCategorias).Select("prca_cate_id=" & cmbCate.Valor & " AND prca_id <> " & clsSQLServer.gFormatArg(hdnPrcaId.Text, SqlDbType.Int)).GetUpperBound(0) <> -1 Then
            Throw New AccesoBD.clsErrNeg("La mesa ya tiene la categoria seleccionada.")
         End If

         ldrCate = mdsDatos.Tables(mstrCategorias).NewRow
         ldrCate.Item("prca_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrCategorias), "prca_id")
      Else
         ldrCate = mdsDatos.Tables(mstrCategorias).Select("prca_id=" & hdnPrcaId.Text)(0)
      End If

      If cmbCate.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la categoria.")
      End If

      If cmbCate.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar la categoria.")
      End If

      With ldrCate
         .Item("prca_prom_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("prca_cate_id") = cmbCate.Valor
         .Item("prca_impo") = txtImpo.Valor
         .Item("prca_porc") = txtPorc.Valor
         .Item("_cate_desc") = cmbCate.SelectedItem.Text
      End With

      If (hdnPrcaId.Text = "") Then
         mdsDatos.Tables(mstrCategorias).Rows.Add(ldrCate)
      End If
   End Sub

   Private Sub mLimpiarCate()
      hdnPrcaId.Text = ""
      cmbCate.Limpiar()
      txtPorc.Text = ""
      txtImpo.Text = ""

      mSetearEditor(mstrCategorias, True)
   End Sub

   Public Sub grdCate_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCate.EditItemIndex = -1
         If (grdCate.CurrentPageIndex < 0 Or grdCate.CurrentPageIndex >= grdCate.PageCount) Then
            grdCate.CurrentPageIndex = 0
         Else
            grdCate.CurrentPageIndex = E.NewPageIndex
         End If

         mConsultarCate()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaCate.Click
      ActualizarCategorias()
   End Sub

   Private Sub btnModiCate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCate.Click
      ActualizarCategorias()
   End Sub

   Private Sub btnBajaCate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaCate.Click
      Try
         mdsDatos.Tables(mstrCategorias).Select("prca_id=" & hdnPrcaId.Text)(0).Delete()

         mConsultarCate()

         mLimpiarCate()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub ActualizarCategorias()
      Try
         mGuardarCategoria()

         mLimpiarCate()
         mConsultarCate()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mConsultarCate()
      grdCate.DataSource = mdsDatos.Tables(mstrCategorias)
      grdCate.DataBind()
   End Sub
#End Region
End Class
End Namespace
