Namespace SRA

Partial Class ClientesPromociones
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_PromoClientes

   Private mstrClieId As String
   Private mstrParaPageSize As Integer
   Private mstrConn As String

   Private Enum Columnas As Integer
      Id = 1
      Cliente = 2
      Socio = 3
      NomCliente = 4
      Promocion = 5
      Estado = 6
   End Enum

#End Region

#Region "Operaciones sobre la Pagina"

   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()

         If (Not Page.IsPostBack) Then
            mSetearEventos()
            mEstablecerPerfil()
            mCargarCombos()
            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "promociones", cmbPromFil, "T")
      clsWeb.gCargarRefeCmb(mstrConn, "promociones", cmbProm, "S")
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.ClientesEntidades, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.ClientesEntidades_Alta, String), (mstrConn), (Session("sUserId").ToString()))
      'btnAlta.Visible = lbooPermiAlta

      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.ClientesEntidades_Baja, String), (mstrConn), (Session("sUserId").ToString()))

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.ClientesEntidades_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()
      mstrClieId = Request.QueryString("clie_id")
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

      If mstrClieId <> "" Then
         usrClieFil.Valor = mstrClieId
         usrClie.Valor = mstrClieId
         usrClie.Activo = False
         usrClieFil.Activo = False
         btnLimpFil.Visible = False
         btnList.Visible = False
         btnBusc.Visible = False
         btnClosePrinc.Visible = True
      End If
   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Seteo de Controles"

   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

        Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            Dim ldsDatos As DataSet

            mLimpiar()

            hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)

            ldsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

            If ldsDatos.Tables(0).Rows.Count > 0 Then
                With ldsDatos.Tables(0).Rows(0)
                    cmbProm.Valor = .Item("prcl_prom_id")
                    usrClie.Valor = .Item("prcl_clie_id")
                    txtDesdeFecha.Fecha = IIf(.Item("prcl_desde_fecha").ToString.Trim.Length > 0, .Item("prcl_desde_fecha"), String.Empty)
                    txtHastaFecha.Fecha = IIf(.Item("prcl_hasta_fecha").ToString.Trim.Length > 0, .Item("prcl_hasta_fecha"), String.Empty)
                    txtImpo.Valor = IIf(.Item("prcl_impo").ToString.Trim.Length > 0, .Item("prcl_impo"), String.Empty)
                    txtPorc.Valor = IIf(.Item("prcl_porc").ToString.Trim.Length > 0, .Item("prcl_porc"), String.Empty)
                End With

                mSetearEditor("", False)
                mMostrarPanel(True)
            End If
        End Sub

   Private Sub mLimpiarFiltros()
      cmbPromFil.Valor = 0
      usrClieFil.Valor = 0
      grdDato.Visible = False
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      cmbProm.Valor = cmbPromFil.Valor
      usrClie.Valor = usrClieFil.Valor
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblBaja.Text = ""
      cmbProm.Valor = cmbPromFil.Valor
      usrClie.Valor = usrClieFil.Valor
      txtDesdeFecha.Text = ""
      txtHastaFecha.Text = ""
      txtImpo.Text = ""
      txtPorc.Text = ""

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mConsultarGrilla()
   End Sub

   Private Sub mConsultarGrilla()
      Try
         mConsultar()
         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      btnAgre.Enabled = Not (panDato.Visible)
      btnAgre.Visible = Not (panDato.Visible)
      panCabecera.Visible = True
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim lDsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, lDsDatos)

         lobjGenerica.Alta()
         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim lDsDatos As DataSet = mGuardarDatos()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, lDsDatos)

         lobjGenerica.Modi()

         mConsultar()

         mMostrarPanel(False)

      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidaDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

        Private Function mGuardarDatos() As DataSet
            mValidaDatos()

            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnId.Text)

            With ldsEsta.Tables(0).Rows(0)
                .Item("prcl_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("prcl_prom_id") = IIf(cmbProm.Valor.Trim.Length > 0, cmbProm.Valor, DBNull.Value)

                .Item("prcl_clie_id") = IIf(usrClie.Valor.ToString.Trim.Length > 0, usrClie.Valor, DBNull.Value)

                .Item("prcl_desde_fecha") = IIf(txtDesdeFecha.Fecha.Trim.Length > 0, txtDesdeFecha.Fecha, DBNull.Value)
                .Item("prcl_hasta_fecha") = IIf(txtHastaFecha.Fecha.Trim.Length > 0, txtHastaFecha.Fecha, DBNull.Value)
                .Item("prcl_impo") = IIf(txtImpo.Valor.Trim.Length > 0, txtImpo.Valor, DBNull.Value)
                .Item("prcl_porc") = IIf(txtPorc.Valor.Trim.Length > 0, txtPorc.Valor, DBNull.Value)
            End With

            Return ldsEsta
        End Function
#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder

         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @prcl_clie_id = " & usrClieFil.Valor)
         lstrCmd.Append(",@prcl_prom_id = " & cmbPromFil.Valor)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         grdDato.Visible = True

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()
         mMostrarPanel(False)
         btnAgre.Visible = True

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = mstrTabla
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

         lstrRpt += "&clie_id=" + usrClieFil.Valor.ToString
         lstrRpt += "&prom_id=" + cmbPromFil.Valor.ToString

         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnClosePrinc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClosePrinc.Click
      Response.Write("<Script>window.close();</script>")
   End Sub
End Class
End Namespace
