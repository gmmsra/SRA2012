Namespace SRA

Partial Class Cobranzas
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Protected WithEvents imgEspec As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents lblCabeActi As System.Web.UI.WebControls.Label
    Protected WithEvents hdnDetaConcepId As System.Web.UI.WebControls.TextBox
    Protected WithEvents hdnCuotasId As System.Web.UI.WebControls.TextBox
    Protected WithEvents hdnAutorizaId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblCabeTotal As System.Web.UI.WebControls.Label
    Protected WithEvents Label7 As System.Web.UI.WebControls.Label
    Protected WithEvents Label9 As System.Web.UI.WebControls.Label
    Protected WithEvents Label11 As System.Web.UI.WebControls.Label
    Protected WithEvents Label13 As System.Web.UI.WebControls.Label



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
    Private mstrTablaDetaAran As String = SRA_Neg.Constantes.gTab_ComprobAranc
    Private mstrTablaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep
    Private mstrTablaVtos As String = SRA_Neg.Constantes.gTab_ComprobVtos
    Private mstrTablaAutoriza As String = SRA_Neg.Constantes.gTab_ComprobAutor
    Private mstrTablaPagos As String = SRA_Neg.Constantes.gTab_ComprobPagos
    Private mstrTablaCuentasCtes As String = SRA_Neg.Constantes.gTab_CuentasCtes
    Private mstrTablaSaldosACuenta As String = SRA_Neg.Constantes.gTab_SaldosACuenta
    Private mstrTablaComprobACuenta As String = SRA_Neg.Constantes.gTab_ComprobACuenta
    Private mstrTablaBilletes As String = SRA_Neg.Constantes.gTab_Billetes
    Private mstrTablaAcuses As String = SRA_Neg.Constantes.gTab_ComprobAcuses

    Private mstrTablaDeuSoc As String = "cobran_deusoc"
    Private mstrTablaAcred As String = "cobran_acred"
    Private mstrTablaAsiento As String = "cobranzas_asiento"

    Private mstrParaPageSize As Integer
    Private mstrTipo As String
    Private mstrCompId As String
    Private mstrConn As String
    Private mdsDatos As DataSet
    Private mstrFac As String

    Private mchrSepa As Char = ";"c

    Public mstrTitu As String

    Private Enum Columnas As Integer
        Id = 1
    End Enum

    Private Enum Panta As Integer
        Encabezado = 1
        Aplic = 2
        Pagos = 3
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            mSetearClie(False)
            mSetearTramitePersonal()

            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "S")

                mSetearMaxLength()
                Session(mSess(mstrTabla)) = Nothing
                mdsDatos = Nothing

                mAgregar()

                clsWeb.gInicializarControles(Me, mstrConn)

                If mstrCompId <> "" Then
                    mCargarDatos(mstrCompId)
                End If

            Else
                mCotizacionDolar()
                mdsDatos = Session(mSess(mstrTabla))
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDeta)

        txtObser.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "code_obse")
        txtNyap.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "code_reci_nyap")
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        mstrTipo = Request("tipo")
        mstrFac = Request("Fac")
        lblTitu.Text = Request("titu")
        hdnTipo.Text = mstrTipo
        Select Case mstrTipo
            Case "AB"
                mstrTitu = "Acred. Bancarias - Ingreso"
            Case "AC"
                mstrTitu = "Aplicaci�n de Saldos"
            Case "TR"
                mstrTitu = "Transferencias"         '12/2010
            Case "AS"
                mstrTitu = "Aplicaci�n de Cr�ditos"
            Case Else
                mstrTitu = "Cobranzas"
        End Select

        lblTituAbm.Text = mstrTitu

        If mstrTipo = "AC" Or mstrTipo = "AS" Or mstrTipo = "TR" Then    '12/2010
            'si se aplic cred desde fact. se imprime directamente la factura, sin preguntar
            If mstrFac <> "" Then hdnGeneraFactura.Text = "2"
            lblTitu.Text = Request("titu")
            lblImpoAcreBanc.Visible = False
            lblImpoAcreBancTit.Visible = False
            lblImpoCheq.Visible = False
            lblImpoCheqTit.Visible = False
            lblImpoDinElec.Visible = False
            lblImpoDinElecTit.Visible = False
            lblImpoRete.Visible = False
            lblImpoReteTit.Visible = False
            lblImpoEfec.Visible = False
            lblImpoEfecTit.Visible = False
            lblImpoTarj.Visible = False
            lblImpoTarjTit.Visible = False
            lblImpoVarios.Visible = False
            lblImpoVariosTit.Visible = False
            trPago1.Visible = False
            trImpo1.Visible = False
            trImpoApl2.Visible = False

            btnEfectivo.Visible = False
            btnTarjeta.Visible = False
            btnDinElec.Visible = False
            btnAcred.Visible = False
            btnReten.Visible = False

            btnCheques.Visible = False
            btnVarios.Visible = False

            If mstrTipo = "AS" Then
                btnCtaCte.Visible = False
                btnFacturas.Visible = False
                btnFacturasCab.Visible = False
                lblImpoCtaCte.Visible = False
                lblImpoCtaCteTit.Visible = False
                lblImpoFact.Visible = False
                lblImpoFactTit.Visible = False
            End If

            If mstrTipo = "AC" Then
                'btnSaldoACta.InnerText = "Transferencias"
                btnSaldoACta.InnerText = "Saldo A Cuenta"
                btnFacturasCab.Visible = False
                btnDeuda.Visible = False
                btnSaldoACta.Visible = False
            End If

            If mstrTipo = "TR" Then     '12/2010
                'btnSaldoACta.InnerText = "Transferencias"
                btnSaldoACta.InnerText = "Saldo A Cuenta"
                btnFacturasCab.Visible = False
            End If
        End If

        If mstrTipo = "AB" Then
            'si es acreditaci�n bancaria
            lblImpoCheq.Visible = False
            lblImpoCheqTit.Visible = False
            lblImpoDinElec.Visible = False
            lblImpoDinElecTit.Visible = False
            lblImpoEfec.Visible = False
            lblImpoEfecTit.Visible = False
            lblImpoTarj.Visible = False
            lblImpoTarjTit.Visible = False
            lblImpoAplicCred.Visible = False
            lblImpoAplicCredTit.Visible = False
            lblImpoVariosTit.Visible = False
            lblImpoVarios.Visible = False

            trPago1.Visible = False
            trImpo1.Visible = False

            btnEfectivo.Visible = False
            btnTarjeta.Visible = False
            btnDinElec.Visible = False
            btnAplic.Visible = False
            btnVarios.Visible = True

            btnFacturasCab.Visible = False
            btnFacturas.Visible = False
            chkTPers.Checked = False
            chkTPers.Visible = False
            lblTPers.Visible = False
            txtFechaValor.Enabled = True
        Else
            btnAcred.Visible = False
        End If

        If Not Request("comp_id") Is Nothing Then
            mstrCompId = Request("comp_id")
        End If
    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Private Sub mConsultarAplic()
        Dim lintCompClieId As Integer = 0
        Dim lintAnteCompClieId As Integer = 0
        Dim ldsTemp As DataSet = mObtenerDsTempAplic()

        grdAplic.DataSource = ldsTemp
        grdAplic.DataBind()

        hdnMultiClie.Text = ""
        hdnUnicaFC.Text = ""
        For Each lDr As DataRow In ldsTemp.Tables(0).Select("")
            If lDr.IsNull("temp_clie_id") Then
                lintCompClieId = 0
            Else
                lintCompClieId = lDr.Item("temp_clie_id")
            End If
            If lintAnteCompClieId <> 0 And lintCompClieId <> lintAnteCompClieId Then
                hdnMultiClie.Text = "S"
            End If
            lintAnteCompClieId = lintCompClieId
            hdnUnicaFC.Text = ldsTemp.Tables(0).Rows.Count.ToString()
        Next

        mCalcultarTotalesAplic(ldsTemp)
        mChequearGeneraFactura(ldsTemp)
    End Sub

    Private Function mValorMonto(ByVal pstrValor As String) As Decimal
        If pstrValor = "" Then
            Return (0)
        Else
            Return (Convert.ToDecimal(pstrValor))
        End If
    End Function

    Private Sub mRecalcularIntereses()
        Dim lbooModi As Boolean = False
        Dim lstrCmd As String = ""
        Dim ldecInte As Decimal = 0
        Dim ds As DataSet

        'FACTURAS CONTADO Y DEUDA CUENTA CORRIENTE
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("_comp_coti_id in (32,41)")

            lstrCmd = "cobranzas_deuda_interes_consul"
            lstrCmd = lstrCmd & " @clie_id=" & usrClie.Valor
            lstrCmd = lstrCmd & ",@fecha=" & clsSQLServer.gFormatArg(txtFechaValor.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@tipo='D'"
            lstrCmd = lstrCmd & ",@SoloVencidas=0"
            lstrCmd = lstrCmd & ",@comp_id=" & lDr.Item("ctac_comp_id")

            ldecInte = mValorMonto(clsSQLServer.gCampoValorConsul(mstrConn, lstrCmd, "interes"))

            lDr.Item("ctac_impor") = ldecInte  'pagado-neto
            lDr.Item("_comp_impo") = ldecInte  'interes
            lbooModi = True
        Next

        'DEUDA SOCIAL
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDeuSoc).Select("cods_coti_id = 8")

            lstrCmd = "rpt_calculo_deuda_consul"
            lstrCmd = lstrCmd & " @soci_id=" & hdnSociId.Text
            lstrCmd = lstrCmd & ",@fecha=" & clsSQLServer.gFormatArg(txtFechaValor.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@consulta=1"
            lstrCmd = lstrCmd & ",@SoloDevengadas=null"
            lstrCmd = lstrCmd & ",@SoloVencidas=0"
            lstrCmd = lstrCmd & ",@anio=" & lDr.Item("cods_anio")
            lstrCmd = lstrCmd & ",@perio=" & lDr.Item("cods_perio")

            ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)
            If ds.Tables(0).Select("anio=" & lDr.Item("cods_anio") & " and perio=" & lDr.Item("cods_perio")).Length > 0 Then
                ldecInte = ds.Tables(0).Select("anio=" & lDr.Item("cods_anio") & " and perio=" & lDr.Item("cods_perio"))(0).Item("interes")
            Else
                ldecInte = 0
            End If
            lDr.Item("cods_impo") = ldecInte
            lbooModi = True
        Next

        If lbooModi Then
            mConsultarAplic()
        End If

    End Sub

    Private Sub mConsultarPagos()
        Dim ldsTemp As DataSet = mObtenerDsTempPagos()
        With ldsTemp.Tables(0)
            .DefaultView.RowFilter = ""
            .DefaultView.Sort = "temp_pati_id, temp_id desc"
            grdPagos.DataSource = .DefaultView
            grdPagos.DataBind()
        End With

        mCalcultarTotalesPagos(ldsTemp)
    End Sub

    Public Sub grdAplic_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdAplic.EditItemIndex = -1
            If (grdAplic.CurrentPageIndex < 0 Or grdAplic.CurrentPageIndex >= grdAplic.PageCount) Then
                grdAplic.CurrentPageIndex = 0
            Else
                grdAplic.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarAplic()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub grdPagos_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdPagos.EditItemIndex = -1
            If (grdPagos.CurrentPageIndex < 0 Or grdPagos.CurrentPageIndex >= grdPagos.PageCount) Then
                grdPagos.CurrentPageIndex = 0
            Else
                grdPagos.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarPagos()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mCotizacionDolar()
        txtCotDolar.Valor = SRA_Neg.Comprobantes.gCotizaMoneda(mstrConn, txtFechaValor.Fecha)
    End Sub

    Private Sub mSetearEditor()
        ImgbtnGenerar.Enabled = mCompleto()
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnGeneraFactura.Text = ""
        hdnImprId.Text = ""
        hdnCuotSociSele.Text = ""
        hdnUnicaFC.Text = ""
        hdnNom.Text = ""

        tblEstadoSaldos.Visible = clsSQLServer.gObtenerValorCampo(mstrConn, "emisores_ctros", SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request), "emct_datos_fact")

        'If chkTPers.Visible Then
        chkTPers.Checked = True
        'End If
        mSetearTramitePersonal()
        txtObser.Text = ""
        txtNyap.Text = ""
        usrClie.Limpiar()

        txtSocio.Text = ""
        cmbRaza.Limpiar()
        txtCriaNume.Text = ""
        hdnAntClieId.Text = ""

        mSetearClie(True)

            'btnOtrosDatos.Enabled = False
            lblFechaIngreso.Text = Today.ToString("dd/MM/yyyy")
            txtFechaValor.Fecha = Today
            hdnAnteFechaValor.Text = txtFechaValor.Text
            mCotizacionDolar()

        mCrearDataSet("")

        hdnDatosPop.Text = ""
        hdnSociDatosPop.Text = ""
        hdnTipoPop.Text = ""
        hdnImprimio.Text = ""
        hdnImprimir.Text = ""
        hdnImprimioAcuse.Text = ""
        hdnImprimirFact.Text = ""
        hdnImprimioNDs.Text = ""
        hdnImprimioFact.Text = ""

        hdnReciNume.Text = ""
        hdnAcuseNume.Text = ""
        hdnFactNume.Text = ""
        hdnNDsNume.Text = ""

        lblFactTarj.Text = ""

        'mConsultarAplic()
        'mConsultarPagos()

        btnTarjeta.Style.Remove("BACKGROUND-COLOR")

        mShowTabs(Panta.Encabezado)

        btnAnteriorDeta.Visible = True
        btnEncabezadoPie.Visible = True
    End Sub

    Private Sub mVolverFact()
        Dim lsbMsg As New StringBuilder

        mdsDatos = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, "")
        Session(mSess(mstrTabla)) = Nothing
        usrClie.Limpiar()

        lsbMsg.Append("<SCRIPT language='javascript'>")
        lsbMsg.Append("window.close();")
        lsbMsg.Append("</SCRIPT>")
        Response.Write(lsbMsg.ToString)
    End Sub

    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panAplic.Visible = False
        panPagos.Visible = False
        panCabecera.Visible = False
        panEnca.Visible = False

        hdnFecha.Text = txtFechaValor.Text

        mSetearEditor()

        mSetearCliente()

        Select Case origen
            Case 1
                'encabezado
                panCabecera.Visible = True

                If (lblCabeTotalAplic.Text <> "" AndAlso CDec(lblCabeTotalAplic.Text) <> 0) Or (lblCabeTotalPago.Text <> "" AndAlso CDec(lblCabeTotalPago.Text) <> 0) Then
                    usrClie.Activo = False
                Else
                    usrClie.Activo = True
                End If
            Case 2
                'Aplic
                panEnca.Visible = True
                panAplic.Visible = True

                btnDeuda.Disabled = hdnClieId.Text = ""
                btnFacturas.Disabled = hdnClieId.Text = ""
                btnCtaCte.Disabled = hdnClieId.Text = ""

                If hdnClieId.Text = "" Then
                    btnSaldoACta.Disabled = True
                Else
                    Dim lstrPetiId As String
                    Dim lstrGene As String
                    lstrPetiId = clsSQLServer.gCampoValorConsul(mstrConn, "clientes_consul @clie_id=" & hdnClieId.Text, "clie_peti_id")
                    lstrGene = clsSQLServer.gCampoValorConsul(mstrConn, "clientes_consul @clie_id=" & hdnClieId.Text, "clie_gene")
                    btnSaldoACta.Disabled = (lstrPetiId = "3" Or lstrGene = "1" Or UCase(lstrGene) = "TRUE")
                End If
            Case 3
                'Pagos
                panEnca.Visible = True
                panPagos.Visible = True

                btnTarjeta.Disabled = hdnClieId.Text = ""
                btnDinElec.Disabled = hdnClieId.Text = ""
                If mstrTipo = "AB" Then
                    btnAcred.Disabled = False
                Else
                    btnAcred.Disabled = hdnClieId.Text = ""
                End If
                btnReten.Disabled = hdnClieId.Text = ""

                btnAplic.Disabled = hdnClieId.Text = ""
        End Select

        If panEnca.Visible Then
            lblCabeCliente.Text = usrClie.Apel
            If lblCabeCliente.Text = "" Then
                lblCabeCliente.Text = txtNyap.Text
            End If
        End If
    End Sub

    Private Function mSess(ByVal pstrTabla As String) As String
        If hdnSess.Text = "" Then
            hdnSess.Text = pstrTabla & Now.Millisecond.ToString
        End If
        Return (hdnSess.Text)
    End Function

    Private Sub mAgregar()
        mLimpiar()
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mCrearDataSet(ByVal pstrId As String)
        Dim ldsAdic As DataSet
        Dim ldtAdic As DataTable

        mdsDatos = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, pstrId)

        mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)

        mdsDatos.Tables.Add(mstrTablaDeuSoc)
        With mdsDatos.Tables(mstrTablaDeuSoc)
            .Columns.Add("cods_id", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_comp_id", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_clie_id", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_coti_id", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_soci_id", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_apli_comp_id", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_anio", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_perio", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_peti_id", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_inst_id", System.Type.GetType("System.Int32"))
            .Columns.Add("cods_impo", System.Type.GetType("System.Double"))
            .Columns.Add("cods_audi_user", System.Type.GetType("System.Int32"))
            .Columns.Add("_desc", System.Type.GetType("System.String"))
        End With

        mdsDatos.Tables.Add(mstrTablaAcred)
        With mdsDatos.Tables(mstrTablaAcred)
            .Columns.Add("acre_id", System.Type.GetType("System.Int32"))
            .Columns.Add("acre_comp_id", System.Type.GetType("System.Int32"))
            .Columns.Add("acre_sact_id", System.Type.GetType("System.Int32"))
            .Columns.Add("acre_apli_comp_id", System.Type.GetType("System.Int32"))
            .Columns.Add("acre_impo", System.Type.GetType("System.Double"))
            .Columns.Add("acre_desc", System.Type.GetType("System.String"))
            .Columns.Add("acre_auto_auti_id", System.Type.GetType("System.Int32"))
            .Columns.Add("acre_auto_usua_id", System.Type.GetType("System.Int32"))
        End With

        mdsDatos.Tables.Add(mstrTablaAsiento)
        With mdsDatos.Tables(mstrTablaAsiento)
            .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_comp_id", System.Type.GetType("System.Int32"))
            .Columns.Add("proc_ND_comp_id", System.Type.GetType("System.String"))
            .Columns.Add("proc_acuse_comp_id", System.Type.GetType("System.Int32"))

            .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))

            .Rows.Add(.NewRow)
            .Rows(0).Item("proc_id") = -1
        End With

        SRA_Neg.Utiles.gAgregarTabla(mstrConn, mdsDatos, mstrTablaCuentasCtes)
        SRA_Neg.Utiles.gAgregarTabla(mstrConn, mdsDatos, mstrTablaSaldosACuenta)
        SRA_Neg.Utiles.gAgregarTabla(mstrConn, mdsDatos, mstrTablaComprobACuenta)
        SRA_Neg.Utiles.gAgregarTabla(mstrConn, mdsDatos, mstrTablaBilletes)
        SRA_Neg.Utiles.gAgregarTabla(mstrConn, mdsDatos, mstrTablaAcuses)

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        mdsDatos.Tables(mstrTablaDeta).Rows.Add(mdsDatos.Tables(mstrTablaDeta).NewRow)

        grdAplic.CurrentPageIndex = 0
        grdPagos.CurrentPageIndex = 0

        mConsultarAplic()
        mConsultarPagos()

        mdsDatos.Tables(mstrTablaConcep).Columns.Add("_soci_id", System.Type.GetType("System.Int32"))

        Session(mSess(mstrTabla)) = mdsDatos
    End Sub

    Private Sub mAgregarTabla(ByVal pstrTabla As String)
        Dim ldsAdic As DataSet
        Dim ldtAdic As DataTable

        ldsAdic = clsSQLServer.gObtenerEstruc(mstrConn, pstrTabla)
        ldtAdic = ldsAdic.Tables(0)
        If ldtAdic.Rows.Count > 0 Then
            ldtAdic.Rows.Remove(ldtAdic.Rows(0))
        End If
        ldtAdic.TableName = pstrTabla
        ldsAdic.Tables.Remove(ldtAdic)
        mdsDatos.Tables.Add(ldtAdic)
    End Sub

    Private Sub mValidarNavega()
        If usrClie.Valor Is DBNull.Value And txtNyap.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el cliente.")
        End If

        If txtCotDolar.Text = "" OrElse txtCotDolar.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la cotizaci�n.")
        End If
    End Sub

    Private Sub mValidarDatos()
        mValidarNavega()

        If Not mCompleto() Then
            Throw New AccesoBD.clsErrNeg("El importe de los valores y de los items a cobrar no coinciden.")
        End If

        If mstrTipo = "AB" And mdsDatos.Tables(mstrTablaPagos).Select("paco_pati_id=" & SRA_Neg.Constantes.PagosTipos.AcredBancaria).GetUpperBound(0) = -1 Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la acreditaci�n.")
        End If
    End Sub

    Private Function mCompleto() As Boolean
        With mdsDatos.Tables(mstrTabla).Rows(0)
            If .IsNull("comp_neto") OrElse lblCabeTotalAplic.Text = "" OrElse .Item("comp_neto") <> lblCabeTotalAplic.Text OrElse CDbl(lblCabeTotalAplic.Text) = 0 Then
                Return (False)
            End If
        End With

        Return (True)
    End Function

    Private Sub mAlta()
        Try
            Dim lstrId As String
            Dim vstrImpr() As String
            Dim ldrComp As DataRow

            mValidarDatos()
            'si se aplic cred desde fact. se imprime directamente la factura, sin preguntar
            If mstrFac <> "" Then hdnGeneraFactura.Text = "2"
            mGuardarDatos()

            Dim lobj As New SRA_Neg.Cobranza(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
            ' Dario 2013-08-30 Paso al objeto cobranzas el tipo cobranza que estoy haciendo
            lobj.TipoCobranza = hdnTipo.Text
            ' doy de alta la cobranza
            lstrId = lobj.Alta()

            If mstrTipo = "AB" Then
                Dim lDsAler As DataSet

                lDsAler = clsSQLServer.gObtenerEstruc(mstrConn, "alertas", "@aler_alti_id=" & SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_INGRESO)
                If lDsAler.Tables(0).Rows.Count > 0 Then
                    SRA_Neg.Alertas.EnviarMailAlertas(mstrConn, Session("sUserId").ToString(), lDsAler.Tables(0).Rows(0).Item("aler_id"), clsSQLServer.gFormatArg(lobj.AcreId, SqlDbType.VarChar))
                End If
            End If

            'If (mstrTipo <> "AB" And mstrTipo <> "AC" And mstrTipo <> "AS") Or mstrCompId <> "" Then --Requerimiento 382.
            If (mstrTipo <> "AB" And mstrTipo <> "AS") Or mstrCompId <> "" Then
                hdnImprimir.Text = lobj.ReciId
                hdnImprimirFact.Text = lobj.FactId
                hdnImprimirAcuse.Text = lobj.AcuseId
                hdnImprimirNDs.Text = lobj.NDIds
                hdnReciNume.Text = lobj.ReciNume
                hdnFactNume.Text = lobj.FactNume
                hdnAcuseNume.Text = lobj.AcuseNume
                hdnNDsNume.Text = lobj.NDNumes

                'Seteo el valor para mostrar en el mensaje al momento de imprimir.
                Select Case mstrTipo
                    Case "RE"
                        hdnTipoImprime.Text = "el recibo"
                    Case "TR"
                        hdnTipoImprime.Text = "la transferencia"
                    Case "AC"
                        hdnTipoImprime.Text = "la aplicaci�n de saldos"
                End Select


                '            If hdnImprimirFact.Text = "" Then  'si no gener� una cancelaci�n (en cuya caso ya viene el id en hdnImprimirFact)
                For Each ldrCtac As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("_comp_cpti_id in (" & SRA_Neg.Constantes.ComprobPagosTipos.Contado & "," & SRA_Neg.Constantes.ComprobPagosTipos.Tarjeta & ") AND _comp_coti_id<>" & SRA_Neg.Constantes.ComprobTipos.Interes_CtaCte)
                    If ("," + hdnImprimirFact.Text + ",").IndexOf("," + ldrCtac.Item("ctac_comp_id").ToString + ",") = -1 Then
                        If hdnImprimirFact.Text <> "" Then
                            hdnImprimirFact.Text += ","
                            hdnFactNume.Text += ","
                        End If
                        hdnImprimirFact.Text += ldrCtac.Item("ctac_comp_id").ToString
                        ldrComp = clsSQLServer.gObtenerEstruc(mstrConn, "comprobantesX", ldrCtac.Item("ctac_comp_id")).Tables(0).Rows(0)
                        hdnFactNume.Text += ldrComp.Item("numero").ToString
                    End If
                Next
                '        End If

                'si ya eligi� la impresora, imprime desde el servidor
                If hdnImprId.Text <> "" And hdnImprId.Text <> "0" And Session("sImpreTipo").ToString = "S" Then
                    If hdnImprimir.Text <> "" Then   'RECIBO O CANCELACI�N
                        clsWeb.gImprimirReporte(mstrConn, Session("sUserId").ToString(), "Recibo", hdnImprId.Text, "comp_id;random", hdnImprimir.Text + ";0", Request)
                        lobj.ModiImpreso(hdnImprimir.Text, True)
                    End If

                    If hdnImprimirAcuse.Text <> "" Then 'ACUSE
                        clsWeb.gImprimirReporte(mstrConn, Session("sUserId").ToString(), "Acuse", hdnImprId.Text, "comp_id;random", hdnImprimirAcuse.Text + ";0", Request)
                        lobj.ModiImpreso(hdnImprimirAcuse.Text, True)
                    End If

                    If hdnImprimirFact.Text <> "" Then  'FACTURAS CONTADO
                        vstrImpr = hdnImprimirFact.Text.Split(",")

                        For i As Integer = 0 To UBound(vstrImpr)
                            clsWeb.gImprimirReporte(mstrConn, Session("sUserId").ToString(), "Factura", hdnImprId.Text, "comp_id;random", vstrImpr(i) + ";0", Request)
                            lobj.ModiImpreso(vstrImpr(i), True)
                        Next
                    End If

                    If hdnImprimioNDs.Text <> "" Then   'NOTAS DE DEBITO
                        vstrImpr = hdnImprimirNDs.Text.Split(",")

                        For i As Integer = 0 To UBound(vstrImpr)
                            clsWeb.gImprimirReporte(mstrConn, Session("sUserId").ToString(), "Factura", hdnImprId.Text, "comp_id;random", vstrImpr(i) + ";0", Request)
                            lobj.ModiImpreso(vstrImpr(i), True)
                        Next
                    End If

                    mLimpiar()

                Else
                    If hdnImprimir.Text = "" And hdnImprimirFact.Text = "" And hdnImprimirAcuse.Text = "" And hdnImprimirNDs.Text = "" Then
                        mLimpiar()
                    End If
                End If

            Else
                mLimpiar()
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)

            If hdnGeneraFactura.Text = "0" Then
                hdnGeneraFactura.Text = "1"
            End If
        End Try
    End Sub

    Private Sub mBaja()
        Try
            'Dim lintPage As Integer = grdDato.CurrentPageIndex

            'Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
            'lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            'grdDato.CurrentPageIndex = 0

            'mConsultar(grdDato, True)

            'If (lintPage < grdDato.PageCount) Then
            '   grdDato.CurrentPageIndex = lintPage

            'End If

            'mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mGuardarDatos() As DataSet
        Dim ldrCompFact As DataRow
        Dim lintFactCompId As Integer
        Dim ldecConcImpo As Decimal

        With mdsDatos.Tables(mstrTabla).Rows(0)
            .Item("comp_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("comp_fecha") = txtFechaValor.Fecha
            .Item("comp_ingr_fecha") = Today

            .Item("comp_clie_id") = usrClie.Valor
            .Item("comp_dh") = 1    '1 = haber
            .Item("comp_cance") = 1 ' 1 = cancelado
            .Item("comp_mone_id") = SRA_Neg.Comprobantes.gMonedaPesos(mstrConn)

            If mdsDatos.Tables(mstrTablaDeuSoc).Select.GetUpperBound(0) = -1 Then
                .Item("comp_cs") = 1 ' cta cte
            Else
                If mdsDatos.Tables(mstrTablaCuentasCtes).Select.GetUpperBound(0) = -1 And _
                   mdsDatos.Tables(mstrTablaSaldosACuenta).Select.GetUpperBound(0) = -1 Then
                    .Item("comp_cs") = 0 ' cuota social
                Else
                    .Item("comp_cs") = 2 ' mixto
                    .Item("comp_neto_soci") = 0
                    .Item("comp_neto_ctac") = 0
                    For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDeuSoc).Select
                        .Item("comp_neto_soci") += lDr.Item("cods_impo")
                    Next
                    For Each lDr As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("_comp_coti_id=" & SRA_Neg.Constantes.ComprobTipos.Factura)
                        .Item("comp_neto_ctac") += lDr.Item("ctac_impor")
                    Next
                End If
            End If

            If hdnGeneraFactura.Text = "1" Or hdnGeneraFactura.Text = "2" Then '(2-aplica creditos desde fact. emite la factura)
                .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.CancelacionFC
                .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.Cobranza

                lintFactCompId = mdsDatos.Tables(mstrTablaCuentasCtes).Select()(0).Item("ctac_comp_id")
                ldrCompFact = clsSQLServer.gObtenerEstruc(mstrConn, "comprobantesX", lintFactCompId).Tables(0).Rows(0)

                .Item("comp_nume") = ldrCompFact.Item("comp_nume")
                .Item("comp_cemi_nume") = ldrCompFact.Item("comp_cemi_nume")
                .Item("comp_emct_id") = ldrCompFact.Item("comp_emct_id")
                .Item("comp_host") = ldrCompFact.Item("comp_host")
                .Item("comp_letra") = ldrCompFact.Item("comp_letra")
                .Item("comp_acti_id") = ldrCompFact.Item("comp_acti_id")
                .Item("comp_impre") = True
            Else
                .Item("comp_nume") = DBNull.Value
                .Item("comp_letra") = DBNull.Value
                .Item("comp_acti_id") = DBNull.Value
                .Item("comp_impre") = False

                Select Case mstrTipo
                    Case "RE"
                        .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Recibo
                        .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.Cobranza

                    Case "AC"
                        .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.AplicacionCreditos
                        .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.AplicacionCreditos
                    Case "TR"       '12/2010
                        .Item("comp_dh") = 0
                        .Item("comp_coti_id") = 13
                        .Item("comp_cs") = 1
                        .Item("comp_neto_ctac") = .Item("comp_neto")
                        .Item("comp_cance") = 1
                        .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.AplicacionCreditos
                    Case "AS"
                        .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.AplicacionCreditos
                        .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.AplicacionCreditosSocios
                    Case "AB"
                        .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Recibo_Acreditacion
                        .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.Cobranza
                End Select

                Dim oFact As New SRA_Neg.Facturacion("", "", Nothing)
                oFact.CentroEmisorNro()

                .Item("comp_cemi_nume") = oFact.pCentroEmisorNro
                .Item("comp_emct_id") = oFact.pCentroEmisorId
                .Item("comp_host") = oFact.pHost
            End If
            If .IsNull("comp_clie_id") Then
                .Item("comp_clie_id") = clsSQLServer.gParametroValorConsul(mstrConn, "para_reci_clie_id")
            End If

        End With

        With mdsDatos.Tables(mstrTablaDeta).Rows(0)
            .Item("code_id") = -1
            .Item("code_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
            .Item("code_coti") = txtCotDolar.Valor
            .Item("code_obse") = txtObser.Valor
            .Item("code_reci_nyap") = txtNyap.Valor

            .Item("code_pers") = chkTPers.Checked
            .Item("code_impo_ivat_tasa") = 0
            .Item("code_impo_ivat_tasa_redu") = 0
            .Item("code_impo_ivat_tasa_sobre") = 0
            .Item("code_impo_ivat_perc") = 0 ' ver
        End With

        'CONCEPTOS VARIOS
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaConcep).Select
            ldecConcImpo += lDr.Item("coco_impo")
        Next

        mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_neto_conc") = ldecConcImpo

        Return mdsDatos
    End Function

    Private Sub mCargarDatos(ByVal pstrCompId)
        Dim ldsTemp As DataSet
        Dim ldrDatos As DataRow
        Dim ldrCompOri As DataRow

        ldsTemp = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, pstrCompId)

        ldrCompOri = ldsTemp.Tables(mstrTabla).Rows(0)
        With ldrCompOri
            txtFechaValor.Fecha = .Item("comp_fecha")
            usrClie.Valor = .Item("comp_clie_id")
        End With

        mSetearCliente()

        With ldsTemp.Tables(mstrTablaDeta).Rows(0)
            txtCotDolar.Valor = .Item("code_coti")
            txtObser.Valor = .Item("code_obse")
            txtNyap.Valor = .Item("code_reci_nyap")
            chkTPers.Checked = .Item("code_pers")
        End With

        mGuardarDatos()

        ldrDatos = mdsDatos.Tables(mstrTablaCuentasCtes).NewRow
        With ldrDatos
            .Item("ctac_id") = clsSQLServer.gObtenerId(.Table, "ctac_id")
            .Item("ctac_canc_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
            .Item("ctac_comp_id") = ldrCompOri.Item("comp_id")
            .Item("ctac_covt_id") = ldsTemp.Tables(mstrTablaVtos).Rows(0).Item("covt_id")
            .Item("ctac_impor") = ldrCompOri.Item("comp_neto")
            .Item("_comp_cpti_id") = ldrCompOri.Item("comp_cpti_id")

            .Item("_desc") = ldrCompOri.Item("_comp_desc")
            .Item("_comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Factura

            .Table.Rows.Add(ldrDatos)
        End With

        mConsultarAplic()
        mConsultarPagos()

        mShowTabs(Panta.Pagos)

        mCargarDatosFactTarj()

        btnAnteriorDeta.Visible = False
        'btnAnteriorPie.Visible = False
        btnEncabezadoPie.Visible = False
        '      trPie.Visible = False
    End Sub
#End Region

#Region "Eventos de Controles"

#Region "Paneles"
    Private Sub mNaveg(ByVal pstrPos As String)
        Try
            mValidarNavega()

            mGuardarDatos()

            mConsultarPagos()

            If hdnAnteFechaValor.Text <> txtFechaValor.Text Then
                mRecalcularIntereses()
                hdnAnteFechaValor.Text = txtFechaValor.Text
            End If

            Select Case pstrPos
                Case "P"                                'principal
                    mShowTabs(Panta.Aplic)

                Case "D-"
                    mShowTabs(Panta.Encabezado)

                Case "D+"
                    mShowTabs(Panta.Pagos)

                Case "P-"
                    mShowTabs(Panta.Aplic)

                Case "P--"                     'al principal
                    mShowTabs(Panta.Encabezado)
            End Select
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

#Region "Eventos botones Naveg"

    Private Sub btnPosteriorDeta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosteriorDeta.Click
        mNaveg("D+")
    End Sub
    Private Sub btnAnteriorDeta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorDeta.Click
        mNaveg("D-")
    End Sub
    Private Sub btnSigCabe_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSigCabe.Click
        mNaveg("P")
    End Sub
    Private Sub btnAnteriorPie_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorPie.Click
        mNaveg("P-")
    End Sub
    Private Sub btnEncabezadoPie_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEncabezadoPie.Click
        mNaveg("P--")
    End Sub
#End Region

#End Region


#End Region

#Region "Alertas/Ventanas-pop"

    Private Sub mAbrirVentana(ByVal pstrTitulo As String, Optional ByVal pboolTarj As Boolean = False, Optional ByVal pstrOrigen As String = "N", Optional ByVal pintAlto As Integer = 200)
        Try

            Dim lsbPagina As New System.Text.StringBuilder
            If usrClie.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar un cliente.")
            End If

            If pboolTarj Then ' tarjetas
                If pstrOrigen = "L" Then ' leyenda
                    lsbPagina.Append("consulta_pop.aspx?EsConsul=0&titulo=" & pstrTitulo & "&tabla=leyendas_factu")
                Else
                    lsbPagina.Append("consulta_pop.aspx?EsConsul=1&titulo=" & pstrTitulo & "&tabla=tarjetas_clientes")
                    lsbPagina.Append("&filtros=" & usrClie.Valor)
                End If
            Else
                lsbPagina.Append("Alertas_pop.aspx?titulo=" & pstrTitulo & "&clieId=" & usrClie.Valor.ToString & "&origen=" & pstrOrigen & "&FechaValor=" & txtFechaValor.Text & "&sociId=" & usrClie.SociId)
            End If

            clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 500, pintAlto, 150, 150)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    'Private Sub imgTarj_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgTarj.Click
    '   mAbrirVentana("Tarjetas Habilitadas", True)
    'End Sub

    'Private Sub imgEspec_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgEspec.Click
    '   mAbrirVentana("Especificaciones Especiales")
    'End Sub

    'Private Sub imgAlertaSaldos_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgAlertaSaldos.Click
    '   mAbrirVentana("Estado de Saldos", , "S")
    'End Sub

#End Region

    Private Sub mSetearCliente()
        hdnSociId.Text = usrClie.SociId(mstrConn).ToString
        hdnClieId.Text = usrClie.Valor.ToString
        hdnAntClieId.Text = usrClie.Valor.ToString
        hdnCotDolar.Text = txtCotDolar.Valor
        'btnOtrosDatos.Enabled = Not (usrClie.Valor Is DBNull.Value)
    End Sub

    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Try
            hdnDatosPop.Text = hdnDatosPop.Text.Replace("NADA", "")

            Select Case hdnTipoPop.Text
                Case "D"
                    mAgregarDeudaSocial()
                Case "P", "A"
                    mConsultarPagos()
                Case "C", "F", "S"
                    mConsultarAplic()

                    If Not panAplic.Visible And Not (usrClie.Valor Is DBNull.Value) Then
                        mShowTabs(Panta.Aplic)
                    End If

                    If hdnTipoPop.Text = "F" Then
                        mCargarDatosFactTarj()
                    End If
            End Select

        Catch ex As Exception
            clsError.gManejarError(Me, ex)

        Finally
            hdnDatosPop.Text = ""
            hdnSociDatosPop.Text = ""
            hdnTipoPop.Text = ""
        End Try
    End Sub

    Private Sub mCargarDatosFactTarj()
        Dim ldrDatos As DataRow
        Dim lintCompId As Integer

        lblFactTarj.Text = ""

        If mdsDatos.Tables(SRA_Neg.Constantes.gTab_CuentasCtes).Select.GetUpperBound(0) <> -1 Then
            With mdsDatos.Tables(SRA_Neg.Constantes.gTab_CuentasCtes).Select()(0)
                lintCompId = .Item("ctac_comp_id")
            End With
        End If
        btnTarjeta.Style.Remove("BACKGROUND-COLOR")
        If lintCompId <> 0 Then
            If clsSQLServer.gObtenerEstruc(mstrConn, "comprobantesX", lintCompId.ToString).Tables(0).Rows(0).Item("comp_cpti_id").ToString = CType(SRA_Neg.Constantes.ComprobPagosTipos.Tarjeta, String) Then
                lblFactTarj.Text = "El Tipo de Pago de esta Factura es TARJETA"
                btnTarjeta.Style.Add("BACKGROUND-COLOR", "red")
            End If
        End If
    End Sub

    Private Sub mAgregarDeudaSocial()
        Dim ldrDatos As DataRow
        Dim lstrTemp As String = Chr(5) & hdnDatosPop.Text
        Dim lvrDatos, lvrDato As String()
        Dim lstrFiltro As String
        Dim lbooExiste As Boolean
        Dim lstrSociId As String

        If (hdnDatosPop.Text <> "") Then
            lvrDatos = hdnDatosPop.Text.Split(Chr(5))

            Dim ldrDeta As DataRow
            Dim lstrClieId As String
            Dim ldsDeuda As DataSet
            Dim ldecTotDev As Decimal
            Dim lchrDeudSocSepa As Char = Convert.ToChar(";")
            'lstrTemp()

            For i As Integer = 0 To lvrDatos.GetUpperBound(0)
                lvrDato = lvrDatos(i).Split(lchrDeudSocSepa)      'CompId(6)Anio(6)Perio(6)PetiId(6)InstId(6)ImpoCuota(6)ImpoInteres(6)SociId(6)Fecha(5)...
                lstrSociId = lvrDato(7)

                lstrFiltro = "cods_soci_id = " + lstrSociId

                If IsNumeric(lvrDato(0)) Then
                    lstrFiltro += " AND cods_apli_comp_id = " + lvrDato(0)
                Else
                    lstrFiltro += " AND cods_anio = " + lvrDato(1)
                    lstrFiltro += " AND cods_perio = " + lvrDato(2)
                End If

                If mdsDatos.Tables(mstrTablaDeuSoc).Select(lstrFiltro).GetUpperBound(0) = -1 Then
                    'AGREGO LA CUOTA SOCIAL
                    ldrDatos = mdsDatos.Tables(mstrTablaDeuSoc).NewRow

                    With ldrDatos
                        .Item("cods_id") = clsSQLServer.gObtenerId(.Table, "cods_id")
                        .Item("cods_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                        .Item("cods_soci_id") = lvrDato(7)

                        If lstrClieId = "" Then
                            lstrClieId = clsSQLServer.gObtenerValorCampo(mstrConn, "socios", .Item("cods_soci_id"), "soci_clie_id")
                        End If

                        .Item("cods_clie_id") = lstrClieId

                        If IsNumeric(lvrDato(0)) Then .Item("cods_apli_comp_id") = lvrDato(0)

                        .Item("cods_anio") = lvrDato(1)
                        .Item("cods_perio") = lvrDato(2)
                        .Item("cods_peti_id") = lvrDato(3)

                        If IsNumeric(lvrDato(4)) Then .Item("cods_inst_id") = lvrDato(4)

                            .Item("cods_impo") = lvrDato(5) '.ToString().Replace(".", ",")
                            .Item("cods_coti_id") = SRA_Neg.Constantes.ComprobTipos.Cuota_Social

                        .Item("_desc") = "Cuota Social " & .Item("cods_perio") & "/" & .Item("cods_anio")

                        ldsDeuda = SRA_Neg.Comprobantes.gObtenerDatosDeudaSocio(mstrConn, .Item("cods_soci_id"), .Item("cods_anio"), .Item("cods_peti_id"), .Item("cods_perio"), 1)
                    End With

                    ldrDatos.Table.Rows.Add(ldrDatos)

                    If IsNumeric(lvrDato(6)) AndAlso lvrDato(6) <> 0 Then
                        ldecTotDev = 0
                        If ldsDeuda.Tables.Count > 1 Then
                            For Each ldrDeuda As DataRow In ldsDeuda.Tables(1).Rows
                                If Not ldrDeuda.IsNull("comp_neto") Then
                                    'AGREGO EL INTERES DEVENGADO
                                    ldrDatos = mdsDatos.Tables(mstrTablaDeuSoc).NewRow

                                    With ldrDatos
                                        .Item("cods_id") = clsSQLServer.gObtenerId(.Table, "cods_id")
                                        .Item("cods_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                                        .Item("cods_soci_id") = lvrDato(7)
                                        .Item("cods_clie_id") = lstrClieId

                                        .Item("cods_anio") = lvrDato(1)
                                        .Item("cods_perio") = lvrDato(2)
                                        .Item("cods_peti_id") = lvrDato(3)

                                        If IsNumeric(lvrDato(4)) Then .Item("cods_inst_id") = lvrDato(4)

                                        .Item("cods_apli_comp_id") = ldrDeuda.Item("comp_id")
                                        .Item("cods_coti_id") = SRA_Neg.Constantes.ComprobTipos.Interes_CS_Dev
                                        .Item("cods_impo") = ldrDeuda.Item("comp_neto")
                                        ldecTotDev += .Item("cods_impo")
                                        .Item("_desc") = "Interes " & .Item("cods_perio") & "/" & .Item("cods_anio")
                                    End With

                                    ldrDatos.Table.Rows.Add(ldrDatos)
                                End If
                            Next
                        End If

                        If ldecTotDev <> lvrDato(6) Then 'EN REALIDAD EXISTE SIEMPRE
                            'AGREGO EL INTERES FLOTANTE
                            ldrDatos = mdsDatos.Tables(mstrTablaDeuSoc).NewRow

                            With ldrDatos
                                .Item("cods_id") = clsSQLServer.gObtenerId(.Table, "cods_id")
                                .Item("cods_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                                .Item("cods_soci_id") = lvrDato(7)
                                .Item("cods_clie_id") = lstrClieId

                                .Item("cods_anio") = lvrDato(1)
                                .Item("cods_perio") = lvrDato(2)
                                .Item("cods_peti_id") = lvrDato(3)

                                If IsNumeric(lvrDato(4)) Then .Item("cods_inst_id") = lvrDato(4)

                                .Item("cods_coti_id") = SRA_Neg.Constantes.ComprobTipos.Interes_Cuota_Social
                                .Item("cods_impo") = lvrDato(6) - ldecTotDev

                                .Item("_desc") = "Interes " & .Item("cods_perio") & "/" & .Item("cods_anio")
                            End With

                            ldrDatos.Table.Rows.Add(ldrDatos)
                        End If
                    End If
                End If
            Next
        End If

        If hdnSociDatosPop.Text <> "" Then
            SRA_Neg.Comprobantes.gBorrarCuotasSociSobrantes(mdsDatos.Tables(mstrTablaDeuSoc), hdnSociDatosPop.Text, "cods", lvrDatos)
        End If

        If lstrSociId <> "" Then
            mCalcultarDescuentoPagoAdelantado(lstrSociId)
        End If

        mConsultarAplic()

        Dim lstrSele As String = ""

        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDeuSoc).Select("cods_coti_id=1")
            'If lstrSele <> "" Then
            '   lstrSele = lstrSele & mchrSepa
            'End If
            lstrSele = lstrSele & "CS" & lDr.Item("cods_soci_id") & mchrSepa
            lstrSele = lstrSele & lDr.Item("cods_anio") & mchrSepa
            lstrSele = lstrSele & lDr.Item("cods_perio") & mchrSepa
        Next
        hdnCuotSociSele.Text = lstrSele
        'aca va
    End Sub

    Private Sub mCalcultarDescuentoPagoAdelantado(ByVal pstrSociId As String)
        Dim lintAnio, lintPerio, lintPetiId, lintInstId As Integer
        Dim lDs As DataSet
        Dim ldrDesc, ldrCoco As DataRow

        'DEUDA SOCIAL
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDeuSoc).Select("cods_soci_id=" + pstrSociId, "cods_anio DESC, cods_perio DESC")
            With lDr
                lintAnio = .Item("cods_anio")
                lintPerio = .Item("cods_perio")
                lintPetiId = .Item("cods_peti_id")

                If Not .IsNull("cods_inst_id") Then
                    lintInstId = .Item("cods_inst_id")
                End If
            End With
            Exit For
        Next

        If lintAnio <> 0 Then
            lDs = SRA_Neg.Comprobantes.gObtenerDescuentoSocioPagoAdel(mstrConn, pstrSociId, lintAnio, lintPerio, lintPetiId, lintInstId, txtFechaValor.Fecha)

            If lDs.Tables(0).Rows.Count > 0 Then
                ldrDesc = lDs.Tables(0).Rows(0)
                'me fijo si no se agreg� todav�a
                If mdsDatos.Tables(mstrTablaConcep).Select("coco_conc_id=" & ldrDesc.Item("cucd_conc_id") & " AND _soci_id=" & pstrSociId).GetUpperBound(0) = -1 Then

                    ldrCoco = mdsDatos.Tables(mstrTablaConcep).NewRow
                    ldrCoco.Item("coco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaConcep), "coco_id")

                    With ldrCoco
                        .Item("coco_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                        .Item("coco_conc_id") = ldrDesc.Item("cucd_conc_id")
                        .Item("coco_ccos_id") = ldrDesc.Item("ccos_id")
                        .Item("coco_impo") = ldrDesc.Item("cucd_valor")
                        .Item("coco_impo_ivai") = .Item("coco_impo")
                        .Item("_soci_id") = pstrSociId
                        .Item("_desc") = "Socio " + clsSQLServer.gObtenerValorCampo(mstrConn, "socios", .Item("_soci_id"), "soci_nume").ToString() & ": " & ldrDesc.Item("_conc_desc")

                        .Table.Rows.Add(ldrCoco)
                    End With
                End If
            Else
                For Each lDr As DataRow In mdsDatos.Tables(mstrTablaConcep).Select("_soci_id=" & pstrSociId)
                    lDr.Delete()
                Next
            End If
        End If
    End Sub

    Private Sub mCalcultarTotalesAplic(ByVal pdsTemp As DataSet)
        Dim ldecTotal As Double
        Dim ldecConceptos As Double
        Dim ldecDeudaSoci As Double
        Dim ldecDeudaCtaCte As Double
        Dim ldecFactConta As Double
        Dim ldecSaldoCta As Double

        For Each lDr As DataRow In pdsTemp.Tables(0).Select
            Select Case lDr.Item("temp_tipo")
                Case "C"
                    ldecConceptos += lDr.Item("temp_impo")
                Case "S"
                    ldecDeudaSoci += lDr.Item("temp_impo")
                Case "FC"
                    ldecFactConta += lDr.Item("temp_impo")
                Case "FD"
                    ldecDeudaCtaCte += lDr.Item("temp_impo")
                Case "A"
                    ldecSaldoCta += lDr.Item("temp_impo")
            End Select

            ldecTotal += lDr.Item("temp_impo")
        Next

        lblImpoDeuda.Text = ldecDeudaSoci.ToString("#####0.00")
        lblImpoVarios.Text = ldecConceptos.ToString("#####0.00")
        lblImpoCtaCte.Text = ldecDeudaCtaCte.ToString("#####0.00")

        lblImpoFact.Text = ldecFactConta.ToString("#####0.00")
        lblImpoSaldoCta.Text = ldecSaldoCta.ToString("#####0.00")

        lblCabeTotalAplic.Text = ldecTotal.ToString("#####0.00")

        mCalcularDife()
    End Sub

    Private Sub mCalcularDife()
        If lblCabeTotalAplic.Text = "" Then
            lblCabeTotalAplic.Text = "0.00"
        End If

        If lblCabeTotalPago.Text = "" Then
            lblCabeTotalPago.Text = "0.00"
        End If

        lblCabeTotalDifer.Text = (CDbl(lblCabeTotalPago.Text) - CDbl(lblCabeTotalAplic.Text)).ToString("#####0.00")

        mSetearEditor()
    End Sub

    Private Sub mChequearGeneraFactura(ByVal pdsTemp As DataSet)
        Dim lintTotal, lintFactConta As Integer
        Dim lintCompId As Integer
        Dim ldrComp As DataRow


        If hdnGeneraFactura.Text <> "0" Then   'si ya dijo que no, no pisar el hidden
            lintFactConta = pdsTemp.Tables(0).Select("temp_tipo='FC'").GetUpperBound(0) + 1
            lintTotal = pdsTemp.Tables(0).Select().GetUpperBound(0) + 1

            If mstrTipo <> "AB" And lintTotal = lintFactConta And lintFactConta > 0 Then
                lintCompId = mdsDatos.Tables(mstrTablaCuentasCtes).Select("ctac_id=" + pdsTemp.Tables(0).Select()(0).Item("temp_id").ToString)(0).Item("ctac_comp_id")
                ldrComp = clsSQLServer.gObtenerEstruc(mstrConn, "comprobantesX", lintCompId).Tables(0).Rows(0)
                With ldrComp
                    If (Not .IsNull("comp_impre") AndAlso Not .Item("comp_impre")) Then
                        hdnGeneraFactura.Text = "1"
                    Else
                        hdnGeneraFactura.Text = ""
                    End If
                End With
            Else
                hdnGeneraFactura.Text = ""
            End If
        End If
    End Sub

    Private Sub mCalcultarTotalesPagos(ByVal pdsTemp As DataSet)
        Dim ldecTotalEfec As Double
        Dim ldecTotalCheq As Double
        Dim ldecTotalTarj As Double
        Dim ldecTotalDinEle As Double
        Dim ldecTotalRete As Double
        Dim ldecTotalAcreBanc As Double
        Dim ldecTotalAplicCred As Double

        For Each lDr As DataRow In pdsTemp.Tables(0).Select
            Select Case lDr.Item("temp_pati_id")
                Case SRA_Neg.Constantes.PagosTipos.Efectivo
                        ldecTotalEfec += IIf(lDr.Item("temp_impo") Is DBNull.Value, 0, lDr.Item("temp_impo")) 'lDr.Item("temp_impo")
                    Case SRA_Neg.Constantes.PagosTipos.Tarjeta
                        ldecTotalTarj += IIf(lDr.Item("temp_impo") Is DBNull.Value, 0, lDr.Item("temp_impo")) 'lDr.Item("temp_impo")
                    Case SRA_Neg.Constantes.PagosTipos.Cheque
                        ldecTotalCheq += IIf(lDr.Item("temp_impo") Is DBNull.Value, 0, lDr.Item("temp_impo")) 'lDr.Item("temp_impo")
                    Case SRA_Neg.Constantes.PagosTipos.AcredBancaria
                        ldecTotalAcreBanc += IIf(lDr.Item("temp_impo") Is DBNull.Value, 0, lDr.Item("temp_impo")) 'lDr.Item("temp_impo")
                    Case SRA_Neg.Constantes.PagosTipos.AplicCredi
                        ldecTotalAplicCred += IIf(lDr.Item("temp_impo") Is DBNull.Value, 0, lDr.Item("temp_impo")) 'lDr.Item("temp_impo")
                    Case SRA_Neg.Constantes.PagosTipos.DineroElec
                        ldecTotalDinEle += IIf(lDr.Item("temp_impo") Is DBNull.Value, 0, lDr.Item("temp_impo")) 'lDr.Item("temp_impo")
                    Case SRA_Neg.Constantes.PagosTipos.Retenciones
                        ldecTotalRete += IIf(lDr.Item("temp_impo") Is DBNull.Value, 0, lDr.Item("temp_impo")) 'lDr.Item("temp_impo")
                    Case 999
                        ldecTotalAplicCred += IIf(lDr.Item("temp_impo") Is DBNull.Value, 0, lDr.Item("temp_impo"))
                End Select
        Next

        lblImpoEfec.Text = ldecTotalEfec.ToString("#####0.00")
        lblImpoTarj.Text = ldecTotalTarj.ToString("#####0.00")
        lblImpoCheq.Text = ldecTotalCheq.ToString("#####0.00")
        lblImpoDinElec.Text = ldecTotalDinEle.ToString("#####0.00")
        lblImpoRete.Text = ldecTotalRete.ToString("#####0.00")
        lblImpoAcreBanc.Text = ldecTotalAcreBanc.ToString("#####0.00")
        lblImpoAplicCred.Text = ldecTotalAplicCred.ToString("#####0.00")
        lblCabeTotalPago.Text = (ldecTotalEfec + ldecTotalTarj + ldecTotalCheq + ldecTotalDinEle + ldecTotalRete + ldecTotalAcreBanc + ldecTotalAplicCred).ToString("#####0.00")

        mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_neto") = CDbl(lblCabeTotalPago.Text)

        mCalcularDife()
    End Sub

    Private Function mObtenerDsTempAplic() As DataSet
        Dim ldsTemp As New DataSet
        Dim ldtTemp As DataTable
        Dim ldrTemp As DataRow
        Dim lstrDesc As String
        Dim lintSociId As Integer
        Dim lintCovtIdAnt As Integer
        Dim lintCompClieId As Integer

        ldtTemp = ldsTemp.Tables.Add("temp")
        With ldtTemp
            .Columns.Add("temp_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_tipo", System.Type.GetType("System.String"))
            .Columns.Add("temp_tipo_desc", System.Type.GetType("System.String"))
            .Columns.Add("temp_desc", System.Type.GetType("System.String"))
            .Columns.Add("temp_impo", System.Type.GetType("System.Double"))
            .Columns.Add("temp_clie_id", System.Type.GetType("System.Int32"))
        End With

        'DEUDA SOCIAL
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaDeuSoc).Select("", "cods_soci_id, cods_anio, cods_perio")
            If lintSociId <> ldr.Item("cods_soci_id") Then
                If lstrDesc <> "" AndAlso ldrTemp.Item("temp_desc").indexof(lstrDesc) = -1 Then
                    ldrTemp.Item("temp_desc") += " - " + lstrDesc
                    lstrDesc = ""
                End If

                ldrTemp = ldtTemp.NewRow
                With ldrTemp
                    .Item("temp_impo") = 0
                    .Item("temp_tipo") = "S"
                    .Item("temp_tipo_desc") = "Deuda Social"

                    If ldr.Item("cods_coti_id") = SRA_Neg.Constantes.ComprobTipos.Cuota_Social Then
                        .Item("temp_desc") = "Socio " + clsSQLServer.gObtenerValorCampo(mstrConn, "socios", lDr.Item("cods_soci_id"), "soci_nume").toString()
                        .Item("temp_desc") += ": " + lDr.Item("_desc")
                    End If
                End With
                ldtTemp.Rows.Add(ldrTemp)
            End If

                If ldr.Item("cods_coti_id") = SRA_Neg.Constantes.ComprobTipos.Cuota_Social Then
                    lstrDesc = lDr.Item("_desc")
                End If
                ldrTemp.Item("temp_impo") += lDr.Item("cods_impo")
                lintSociId = lDr.Item("cods_soci_id")
            Next

        If lstrDesc <> "" AndAlso ldrTemp.Item("temp_desc").indexof(lstrDesc) = -1 Then
            ldrTemp.Item("temp_desc") += " - " + lstrDesc
        End If

        'CONCEPTOS VARIOS
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaConcep).Select
            ldrTemp = ldtTemp.NewRow

            With ldrTemp
                .Item("temp_id") = lDr.Item("coco_id")
                .Item("temp_tipo") = "C"
                .Item("temp_tipo_desc") = "Varios"
                .Item("temp_desc") = lDr.Item("_desc")
                .Item("temp_impo") = lDr.Item("coco_impo")
            End With

            ldtTemp.Rows.Add(ldrTemp)
        Next

        'FACTURAS CONTADOS Y DEUDA CTA.CTE.
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("_comp_coti_id<>" & SRA_Neg.Constantes.ComprobTipos.Interes_CtaCte & " and ctac_impor<>0", "ctac_comp_id,ctac_covt_id,_comp_coti_id")
            lintCompClieId = clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantesX", lDr.Item("ctac_comp_id"), "comp_clie_id").tostring
            If usrClie.Valor Is DBNull.Value Then
                usrClie.Valor = lintCompClieId
                mSetearCliente()
            End If

            If lintCovtIdAnt <> lDr.Item("ctac_covt_id") Then  'por los intereses (pagados) de cta.cte
                ldrTemp = ldtTemp.NewRow
                ldtTemp.Rows.Add(ldrTemp)
            End If

            With ldrTemp
                .Item("temp_id") = lDr.Item("ctac_id")

                'se modifico el 24/05/2007 para que las pagadas con tarjeta tambien se impriman desde facturacion
                If lDr.Item("_comp_cpti_id") = SRA_Neg.Constantes.ComprobPagosTipos.Contado Or lDr.Item("_comp_cpti_id") = SRA_Neg.Constantes.ComprobPagosTipos.Tarjeta Then
                    .Item("temp_tipo") = "FC"
                    .Item("temp_tipo_desc") = "Factura Contado"
                Else
                    .Item("temp_tipo") = "FD"
                    .Item("temp_tipo_desc") = "Deuda C.C."
                End If


                If lintCovtIdAnt = lDr.Item("ctac_covt_id") Then
                    .Item("temp_impo") += lDr.Item("ctac_impor")
                Else
                    .Item("temp_impo") = lDr.Item("ctac_impor")
                    .Item("temp_desc") = lDr.Item("_desc")

                    If usrClie.Valor Is DBNull.Value OrElse lintCompClieId <> usrClie.Valor Then
                        .Item("temp_desc") += " (" + clsSQLServer.gObtenerValorCampo(mstrConn, "clientesX", lintCompClieId, "clie_apel").ToString + ")"
                    End If
                End If
                .Item("temp_clie_id") = lintCompClieId.ToString
            End With

            lintCovtIdAnt = lDr.Item("ctac_covt_id")
        Next

        'SALDOS A CUENTA
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaSaldosACuenta).Select
            ldrTemp = ldtTemp.NewRow

            With ldrTemp
                .Item("temp_id") = lDr.Item("sact_id")
                .Item("temp_tipo") = "A"
                .Item("temp_tipo_desc") = "Saldos A Cuenta"
                .Item("temp_desc") = lDr.Item("_acti_desc")
                .Item("temp_impo") = lDr.Item("sact_impo")

                If lDr.Item("sact_clie_id") <> usrClie.Valor Then
                    .Item("temp_desc") += " (" + clsSQLServer.gObtenerValorCampo(mstrConn, "clientesX", lDr.Item("sact_clie_id"), "clie_apel").tostring + ")"
                End If
            End With

            ldtTemp.Rows.Add(ldrTemp)
        Next

        Return (ldsTemp)
    End Function

    Private Function mObtenerDsTempPagos() As DataSet
        Dim ldsTemp As New DataSet
        Dim ldtTemp As DataTable
        Dim ldrTemp As DataRow
        Dim lstrMoneDesc As String

        ldtTemp = ldsTemp.Tables.Add("temp")
        With ldtTemp
            .Columns.Add("temp_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_pati_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_desc", System.Type.GetType("System.String"))
            .Columns.Add("temp_mone_desc", System.Type.GetType("System.String"))
            .Columns.Add("temp_impo", System.Type.GetType("System.Double"))
            .Columns.Add("temp_mone_ori", System.Type.GetType("System.Double"))
        End With

        'PAGOS
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaPagos).Select
            ldrTemp = ldtTemp.NewRow

            With ldrTemp
                .Item("temp_id") = ldr.Item("paco_id")
                .Item("temp_pati_id") = ldr.Item("paco_pati_id")
                .Item("temp_desc") = ldr.Item("_desc")
                'Actualizar pagos segun la cotizacion
                If lDr.Item("paco_mone_id") = 2 Then
                    lDr.Item("paco_impo") = lDr.Item("paco_orig_impo") * txtCotDolar.Valor
                End If
                .Item("temp_impo") = lDr.Item("paco_impo")
                .Item("temp_mone_desc") = lDr.Item("_mone_desc")
                .Item("temp_mone_ori") = lDr.Item("paco_orig_impo")
                .Table.Rows.Add(ldrTemp)
            End With
        Next

        'COMPROB_A_CUENTA
        For Each lDr As DataRow In mdsDatos.Tables(mstrTablaAcred).Select
            ldrTemp = ldtTemp.NewRow

            With ldrTemp
                .Item("temp_id") = ldr.Item("acre_id")
                .Item("temp_pati_id") = 999
                .Item("temp_desc") = ldr.Item("acre_desc")
                .Item("temp_impo") = lDr.Item("acre_impo")
                If lstrMoneDesc = "" Then
                    lstrMoneDesc = clsSQLServer.gObtenerValorCampo(mstrConn, "monedas", "1", "mone_desc")
                End If
                .Item("temp_mone_desc") = lstrMoneDesc
                .Table.Rows.Add(ldrTemp)
            End With
        Next

        Return (ldsTemp)
    End Function

    Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged
        Try
            Dim vstrImpr() As String

            If (hdnImprimio.Text <> "" Or hdnImprimioAcuse.Text <> "" Or hdnImprimioNDs.Text <> "") And (hdnImprimir.Text <> "" Or hdnImprimirFact.Text <> "" Or hdnImprimirAcuse.Text <> "" Or hdnImprimirNDs.Text <> "") Then
                Dim lobj As New SRA_Neg.Cobranza(mstrConn, Session("sUserId").ToString(), mstrTabla)

                If hdnImprimir.Text <> "" Then
                    lobj.ModiImpreso(hdnImprimir.Text, CBool(CInt(hdnImprimio.Text)))
                End If
                If hdnImprimirAcuse.Text <> "" Then
                    lobj.ModiImpreso(hdnImprimirAcuse.Text, CBool(CInt(hdnImprimioAcuse.Text)))
                End If
                If hdnImprimioFact.Text <> "" Then
                    vstrImpr = hdnImprimioFact.Text.Split(",")

                    For i As Integer = 0 To UBound(vstrImpr)
                        lobj.ModiImpreso(vstrImpr(i), True)
                    Next
                End If

                If hdnImprimioNDs.Text <> "" Then
                    vstrImpr = hdnImprimioNDs.Text.Split(",")

                    For i As Integer = 0 To UBound(vstrImpr)
                        lobj.ModiImpreso(vstrImpr(i), True)
                    Next
                End If
            End If

            If mstrCompId = "" Then
                mLimpiar()
            Else
                mVolverFact()
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            hdnImprimio.Text = ""
            hdnImprimir.Text = ""
            hdnImprimirFact.Text = ""
            hdnImprimioFact.Text = ""
            hdnImprimioAcuse.Text = ""
            hdnImprimirAcuse.Text = ""
            hdnImprimioNDs.Text = ""
            hdnImprimirNDs.Text = ""
        End Try
    End Sub

    Private Sub mSetearClie(ByVal pbooLimpiar As Boolean)
        If Not pbooLimpiar Then
            If Request.Form(txtNyap.ClientID) <> "" Then
                txtNyap.Text = Request.Form(txtNyap.UniqueID)
                usrClie.Limpiar()
            ElseIf Request.Form(usrClie.txtIdExt.UniqueID) <> "" Then
                usrClie.Valor = Request.Form(usrClie.txtIdExt.UniqueID)
                txtNyap.Text = ""
            End If
        End If

        Dim lbooClieHabi As Boolean = True
        Dim lbooNyapHabi As Boolean = True

        If usrClie.txtCodiExt.Text <> "" Then
            lbooNyapHabi = False
        End If

        If txtNyap.Text <> "" Then
            lbooClieHabi = False
        End If

        clsWeb.gActivarControl(txtNyap, lbooNyapHabi)
        usrClie.Activo = lbooClieHabi

        If Not usrClie.Activo Then
            usrClie.Limpiar()
            hdnClieId.Text = ""
            hdnAntClieId.Text = ""
            hdnSociId.Text = ""
        End If

        If Not txtNyap.Enabled Then
            txtNyap.Text = ""
        End If

        If usrClie.Valor Is DBNull.Value Then
            divEstadoSaldos.Style.Add("display", "none")
        ElseIf hdnAntClieId.Text <> usrClie.Valor.ToString Then
            If clsSQLServer.gObtenerValorCampo(mstrConn, "emisores_ctros", SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request), "emct_datos_fact") Then
                Dim lDs As DataSet = clsSQLServer.gExecuteQuery(mstrConn, "clientes_estado_de_saldos_consul " + usrClie.Valor.ToString)
                With lDs.Tables(0).Rows(0)
                    txtSaldoCaCteVenc.Text = .Item("saldo_cta_cte_venc")
                    txtSaldoCtaSociVenc.Text = .Item("saldo_social_venc")
                    txtImpCuentaCtaCte.Text = .Item("imp_a_cta_cta_cte")
                    txtImpCuentaCtaSocial.Text = .Item("imp_a_cta_cta_social")

                    If .Item("saldo_cta_cte_venc") <> 0 Or .Item("saldo_social_venc") <> 0 Or .Item("imp_a_cta_cta_cte") <> 0 Or .Item("imp_a_cta_cta_social") <> 0 Then
                        divEstadoSaldos.Style.Add("display", "inline")
                    Else
                        divEstadoSaldos.Style.Add("display", "none")
                    End If
                End With
            End If
        End If
        hdnAntClieId.Text = usrClie.Valor.ToString
    End Sub

    Private Sub mSetearTramitePersonal()
        Select Case mstrTipo
            Case "RE"
                txtFechaValor.Enabled = Not chkTPers.Checked
                If Not txtFechaValor.Enabled Then
                        txtFechaValor.Fecha = Today
                    End If
        End Select

    End Sub

    Private Sub ImgbtnGenerar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgbtnGenerar.Click
        ImgbtnGenerar.Enabled = False
        mAlta()
    End Sub

    Private Sub ImgbtnLimpiar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgbtnLimpiar.Click
        If mstrCompId = "" Then
            mLimpiar()
        Else
            mVolverFact()
        End If
    End Sub

    Private Sub btnOtrosDatos_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnOtrosDatos.Click
        mAbrirVentana("Otros Datos", , "O", 500)
    End Sub
End Class
End Namespace
