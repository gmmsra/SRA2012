<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.PlanFacilidades" CodeFile="PlanFacilidades.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Plan de Facilidades de Pago</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/impresion.js"></script>
		<script language="JavaScript">
		function mSetearConcepto(pConc)
		{
			var strRet = LeerCamposXML("conceptos", "@conc_id="+pConc.value, "_cuenta");
			document.all('hdnCta').value = '';
			if (strRet!='')
			{  
				var lstrCta = strRet.substring(0,1);
				document.all('hdnCta').value = lstrCta;
				if (lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
				{
					document.all('cmbCCos').disabled = true;
					document.all('txtcmbCCos').disabled = true;
					document.all('cmbCCos').innerHTML = '';
					document.all('txtcmbCCos').value = '';
				}
				else
				{
					document.all('cmbCCos').disabled = false;
					document.all('txtcmbCCos').disabled = false;
					LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, "cmbCCos", "N");					
				}
			}			
		}
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');Imprimir();" class="pagina" leftMargin="5" topMargin="5"
		rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25" width="100%">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server">Plan de Facilidades de Pago</asp:label>
								</TD>
							</TR>
							<!--- filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2" width="100%">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="97%" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" align="right">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																	BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																	BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																	CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="10"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 20%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 72%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Fecha Hasta:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 20%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 72%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 82%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<UC1:CLIE id="usrClieFil" runat="server" width="100%" FilDocuNume="True" MuestraDesc="False"
																				PermiModi="True" FilSociNume="True" Saltos="1,1,1" Tabla="Clientes" FilCUIT="True" AceptaNull="False"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 20%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD style="WIDTH: 72%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:label id="lblNroSocioFil" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:numberbox id="txtNroSocioFil" runat="server" cssclass="cuadrotexto" Width="80px" onchange="mCargarClienteSocio('F')"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD></TD>
								<TD colSpan="2" vAlign="top" align="center" width="100%">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn DataField="plpa_id" Visible="False"></asp:BoundColumn>
											<asp:BoundColumn DataField="plpa_gene_fecha" HeaderText="Generado" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="plpa_apro_fecha" HeaderText="Aprobado" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_clie_apel" HeaderText="Cliente"></asp:BoundColumn>
											<asp:BoundColumn DataField="_soci_nume" HeaderText="Nro.Socio"></asp:BoundColumn>
											<asp:BoundColumn DataField="_total" HeaderText="Importe" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
												headerstyle-HorizontalAlign="Right"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD width="1"></TD>
								<TD vAlign="middle">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td>
												<CC1:BotonImagen id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnNuev.gif" ForeColor="Transparent"
													ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
													ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ToolTip="Agregar Nuevo D�bito"></CC1:BotonImagen>
											</td>
											<td align="right">
											</td>
										</tr>
									</table>
								</TD>
								<TD align="right">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="45px" CausesValidation="False" Height="21px"> Plan</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDeudaSoc" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Deuda Soc.</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkDeudaCtac" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="100px" CausesValidation="False" Height="21px"> Deuda Cta.Cte.</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkConc" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="80px" CausesValidation="False" Height="21px"> Conceptos</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCuotas" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="50px" CausesValidation="False" Height="21px"> Cuotas</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" Height="116px" BorderStyle="Solid"
										BorderWidth="1px" Visible="False">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
											border="0">
											<TR>
												<TD>
													<P></P>
												</TD>
												<TD height="5">
													<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
												<TD vAlign="top" align="right">&nbsp;
													<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD style="HEIGHT: 24px" vAlign="top" align="right">
																	<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 24px" align="left" width="600">
																	<UC1:CLIE id="usrClie" runat="server" FilDocuNume="True" FilSociNume="True" Saltos="1,2" Tabla="Clientes"
																		Obligatorio="True" CampoVal="Cliente" Ancho="800" Autopostback="true"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 5px" align="right" background="imagenes/formfdofields.jpg">
																	<asp:label id="lblSocio" runat="server" cssclass="titulo">Nro.Socio:</asp:label>&nbsp;</TD>
																<TD style="HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																	<cc1:numberbox id="txtSocio" runat="server" cssclass="cuadrotexto" Width="80px" onchange="mCargarClienteSocio('C')"></cc1:numberbox>&nbsp;
																	<asp:label id="lblActividad" runat="server" cssclass="titulo">Actividad:</asp:label>
																	<cc1:combobox class="combo" id="cmbActividad" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="actividades_cargar"
																		 filtra="False" MostrarBotones="False" TextMaxLength=5></cc1:combobox>
																</TD>
																
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblValorFecha" runat="server" cssclass="titulo">Fecha Valor: </asp:Label></TD>
																<TD>
																	<cc1:DateBox id="txtValorFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblGeneFecha" runat="server" cssclass="titulo">Fecha Generaci�n:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:DateBox id="txtGeneFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblAproSociFecha" runat="server" cssclass="titulo">Fecha Aprob.Socios:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:DateBox id="txtAproSociFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblAproCtacFecha" runat="server" cssclass="titulo">Fecha Aprob.Cta.Cte:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:DateBox id="txtAproCtacFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblAproFecha" runat="server" cssclass="titulo">Fecha Aprob.Final:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:DateBox id="txtAproFecha" runat="server" cssclass="cuadrotextodeshab" Width="70px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblCompGene" runat="server" cssclass="titulo">Comprobante Generado:</asp:Label>&nbsp;</TD>
																<TD>
																	<asp:TextBox id="txtCompGene" runat="server" cssclass="cuadrotextodeshab" Width="200px"></asp:TextBox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 16px" vAlign="top" align="right">
																	<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																<TD style="HEIGHT: 16px" colSpan="2" height="16">
																	<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="100%" Height="50px" EnterPorTab="False"
																		TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="4">
													<asp:panel id="panDeudaSoc" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="Table4" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<tr>
																<TD vAlign="top" align="right" colspan="2">
																	<asp:Label id="lblCtaSocial" runat="server" cssclass="titulo">A favor Cta. Social:</asp:Label>&nbsp;</TD>
																<TD colspan="6">
																	<cc1:numberbox id="txtCtaSocial" style="text-align:right" runat="server" cssclass="cuadrotextodeshab"
																		Width="60px" AceptaNull="False" EsDecimal="True" Enabled="false"></cc1:numberbox></TD>
															</tr>
															<TR>
																<TD style="WIDTH: 100%" align="left" colSpan="8">
																	<asp:datagrid id="grdDeudaSoc" runat="server" BorderWidth="1px" BorderStyle="None" width="100%"
																		AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																		OnPageIndexChanged="grdDeudaSoc_PageChanged" AutoGenerateColumns="False">
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<Columns>
																			<asp:TemplateColumn>
																				<HeaderStyle Width="3%"></HeaderStyle>
																				<ItemTemplate>
																					
																				<asp:CheckBox ID="chkPlan" Runat="server" onclick="chkPlanCheck(this,'Soc');"></asp:CheckBox>
																					<DIV runat="server" id="divTotal" style="DISPLAY: none"><%#(100*DataBinder.Eval(Container, "DataItem.total"))%></DIV>
																					<DIV runat="server" id="divInte" style="DISPLAY: none"><%#(100*("0" + DataBinder.Eval(Container, "DataItem.interes").tostring))%></DIV>
																					<DIV runat="server" id="divDesc" style="DISPLAY: none"><%#(100*DataBinder.Eval(Container, "DataItem.descuento"))%></DIV>
																					<DIV runat="server" id="divPerio" style="DISPLAY: none"><%#(DataBinder.Eval(Container, "DataItem.anio") & "," & DataBinder.Eval(Container, "DataItem.perio"))%></DIV>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn Visible="False" DataField="comp_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="coti_desc" HeaderText="Concepto"></asp:BoundColumn>
																			<asp:BoundColumn DataField="cuota" HeaderText="Cuotas" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
																				headerstyle-HorizontalAlign="Right"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="anio"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="perio"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="peti_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="interes" HeaderText="Intereses" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
																				headerstyle-HorizontalAlign="Right"></asp:BoundColumn>
																			<asp:BoundColumn DataField="descuento" HeaderText="Descuentos Varios" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="conc_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="total" HeaderText="Total" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
																				headerstyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD vAlign="bottom" colSpan="8" height="16"></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblDeudaSocCuot" runat="server" cssclass="titulo">Cuota Pura:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtDeudaSocCuot" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" style="text-align:right" Enabled="false"></cc1:numberbox></TD>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblDeudaSocInte" runat="server" cssclass="titulo">Intereses.:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtDeudaSocInte" runat="server" style="text-align:right" cssclass="cuadrotextodeshab"
																		Width="60px" AceptaNull="False" EsDecimal="True" Enabled="false"></cc1:numberbox></TD>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblDeudaSocDesc" runat="server" cssclass="titulo">Descuentos:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtDeudaSocDesc" runat="server" cssclass="cuadrotextodeshab" Width="50px" AceptaNull="False"
																		EsDecimal="True" Enabled="false" style="text-align:right"></cc1:numberbox></TD>
																<TD vAlign="top" align="right" width="200px">
																	<asp:Label id="lblDeudaSocTotal" runat="server" cssclass="titulo">Total a Pagar Deuda Social:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtDeudaSocTotal" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" Enabled="false" style="text-align:right"></cc1:numberbox></TD>
															</TR>
															<TR style="VISIBILITY: hidden">
																<TD vAlign="top" align="right">
																	<asp:Label id="lblDeudaSocPlan" runat="server" cssclass="titulo">Subtotal (En Plan):</asp:Label>&nbsp;</TD>
																<TD width="80">
																	<cc1:numberbox id="txtDeudaSocPlan" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" Enabled="false"></cc1:numberbox></TD>
																<TD align="right">
																	<asp:Label id="lblDeudaSocPag" runat="server" cssclass="titulo">Subtotal (Pagado):</asp:Label>&nbsp;</TD>
																<TD align="left">
																	<cc1:numberbox id="txtDeudaSocPag" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" Enabled="false"></cc1:numberbox></TD>
															</TR>
															<TR style="VISIBILITY: hidden">
																<TD vAlign="top" align="right">
																	<asp:Label id="lblDeudaSocDife" runat="server" cssclass="titulo">Total a Pagar:</asp:Label>&nbsp;</TD>
																<TD colSpan="3">
																	<cc1:numberbox id="txtDeudaSocDife" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" Enabled="false"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD vAlign="middle" align="center" colSpan="8" height="30">
																	<asp:Button id="btnDeudaSocAprob" runat="server" cssclass="boton" Width="80px" Text="Aprobar"></asp:Button></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="4">
													<asp:panel id="panDeudaCtac" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="TabDeudaCtac" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD align="right"><asp:label id="lblSaldoCtaCC" runat="server" cssclass="titulo">A favor Cta.Cte.:</asp:label>&nbsp;</TD>
																<TD>
																	<CC1:NUMBERBOX id="txtSaldoCtaCC" runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"
																		EsDecimal="True" style="text-align:right"></CC1:NUMBERBOX></TD>
																<TD align="right"><asp:label id="lblTotalCC" runat="server" cssclass="titulo">Deuda Total Cta.Cte.:</asp:label>&nbsp;</TD>
																<TD>
																	<CC1:NUMBERBOX id="txtDeudaCtacTotal" runat="server" cssclass="cuadrotextodeshab" Width="60px"
																		Enabled="False" EsDecimal="True" style="text-align:right"></CC1:NUMBERBOX></TD>
																<td align="right"><asp:label id="lblTotalVencCC" runat="server" cssclass="titulo">Total Vencida:</asp:label>&nbsp;</td>
																<TD>
																	<CC1:NUMBERBOX id="txtTotalVencCC" runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"
																		EsDecimal="True" style="text-align:right"></CC1:NUMBERBOX></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 100%" align="left" colSpan="6">
																	<asp:datagrid id="grdDeudaCtac" runat="server" BorderWidth="1px" BorderStyle="None" width="100%"
																		AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																		OnPageIndexChanged="grdDeudaCtac_PageChanged" AutoGenerateColumns="False">
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<Columns>
																			<asp:TemplateColumn>
																				<HeaderStyle Width="3%"></HeaderStyle>
																				<ItemTemplate>
																					<asp:CheckBox ID="chkPlan" Runat="server" onclick="chkPlanCheck(this,'Ctac');"></asp:CheckBox>
																					<DIV runat="server" id="divTotal" style="DISPLAY: none"><%#(100*(DataBinder.Eval(Container, "DataItem.comp_neto")+DataBinder.Eval(Container, "DataItem.interes")))%></DIV>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn Visible="False" DataField="comp_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="numero" HeaderText="Comprobante"></asp:BoundColumn>
																			<asp:BoundColumn DataField="comp_neto" HeaderText="Importe" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
																				headerstyle-HorizontalAlign="Right"></asp:BoundColumn>
																			<asp:BoundColumn DataField="comp_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																			<asp:BoundColumn DataField="interes" HeaderText="Inter�s" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"
																				headerstyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD vAlign="bottom" colSpan="2" height="16"></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblDeudaCtacPlan" runat="server" cssclass="titulo">Total a Pagar Deuda Cta. Cte:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtDeudaCtacPlan" runat="server" cssclass="cuadrotextodeshab" Width="60px" EsDecimal="True"
																		style="text-align:right" Enabled="false"></cc1:numberbox></TD>
																<TD vAlign="top" align="right" style="VISIBILITY: hidden">
																	<asp:Label id="lblDeudaCtacPag" runat="server" cssclass="titulo">Subtotal (Pagado):</asp:Label>&nbsp;</TD>
																<TD style="VISIBILITY: hidden">
																	<cc1:numberbox id="txtDeudaCtacPag" runat="server" cssclass="cuadrotextodeshab" Width="60px" EsDecimal="True"
																		style="text-align:right" Enabled="false"></cc1:numberbox></TD>
															</TR>
															<TR style="DISPLAY: none">
																<TD vAlign="top" align="right">
																	<asp:Label id="lblDeudaCtacDife" runat="server" cssclass="titulo">Diferencia:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtDeudaCtacDife" runat="server" cssclass="cuadrotextodeshab" Width="60px" EsDecimal="True"
																		Enabled="false"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD vAlign="middle" align="center" colSpan="6" height="30">
																	<asp:Button id="btnDeudaCtacAprob" runat="server" cssclass="boton" Width="80px" Text="Aprobar"></asp:Button></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="4">
													<asp:panel id="panConc" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="TabConc" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblConcDeudaSoc" runat="server" cssclass="titulo">Total a Pagar Deuda Soc.:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtConcDeudaSoc" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" style="text-align:right" Enabled="False"></cc1:numberbox></TD>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblConcDeudaCtac" runat="server" cssclass="titulo">Total a Pagar Deuda Cta.Cte.:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtConcDeudaCtac" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" style="text-align:right" Enabled="false"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 100%" align="left" colSpan="4">
																	<asp:datagrid id="grdConc" runat="server" BorderWidth="1px" BorderStyle="None" width="95%" AllowPaging="True"
																		HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatosConc"
																		OnPageIndexChanged="grdConc_PageChanged" AutoGenerateColumns="False">
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<Columns>
																			<asp:TemplateColumn>
																				<HeaderStyle Width="20px"></HeaderStyle>
																				<ItemTemplate>
																					<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																						<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																					</asp:LinkButton>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn Visible="False" DataField="plco_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_conc_desc" HeaderText="Concepto"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_ccos_desc" HeaderText="C.Costo"></asp:BoundColumn>
																			<asp:BoundColumn DataField="plco_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblConcTotal" runat="server" cssclass="titulo">Total Conceptos:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtConcTotal" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" style="text-align:right" Enabled="false"></cc1:numberbox></TD>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblConcDife" runat="server" cssclass="titulo">Total a Pagar:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtConcDife" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" style="text-align:right" Enabled="false"></cc1:numberbox>
																	</TD>
															</TR>
															<TR>
																<TD vAlign="bottom" colSpan="4" height="16"></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="6" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblConc" runat="server" cssclass="titulo">Concepto:</asp:Label>&nbsp;</TD>
																<TD colSpan="3">
																	<table border=0 cellpadding=0 cellspacing=0>
																	<tr>
																	<td>															
																	<cc1:combobox class="combo" id="cmbConc" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="cuentas_cargar"
																		onchange="javascript:mSetearConcepto(this);" filtra="true" MostrarBotones="False" TextMaxLength=5></cc1:combobox>
																	</td>
																	<td>
																		<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																										onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbConc','Conceptos','@conc_acti_id=2');"
																										alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</td>
																	</tr>
																	</table>	
																</TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblCcos" runat="server" cssclass="titulo">Cto.Costo:</asp:Label>&nbsp;</TD>
																<TD colSpan="3">
																	<table border=0 cellpadding=0 cellspacing=0>
																	<tr>
																	<td>															
																	<cc1:combobox class="combo" id="cmbCcos" runat="server" cssclass="cuadrotexto" Width="300px" NomOper="cuentas_cargar"
																		filtra="true" MostrarBotones="False" TextMaxLength=6></cc1:combobox>
																	</td>
																	<td>
																		<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																										onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCcos','Centros de Costo','@cuenta='+document.all('hdnCta').value);"
																										alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</td>
																	</tr>
																	</table>
																</TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblImpoConc" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																<TD colSpan="3">
																	<cc1:numberbox id="txtImpoConc" runat="server" cssclass="cuadrotexto" Width="100px" EsDecimal="True"
																		style="text-align:right"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD vAlign="middle" align="center" colSpan="4" height="30">
																	<asp:Button id="btnAltaConc" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																	<asp:Button id="btnBajaConc" runat="server" cssclass="boton" Width="85px" Text="Eliminar"></asp:Button>&nbsp;
																	<asp:Button id="btnModiConc" runat="server" cssclass="boton" Width="90px" Text="Modificar"></asp:Button>&nbsp;
																	<asp:Button id="btnLimpConc" runat="server" cssclass="boton" Width="85px" Text="Limpiar"></asp:Button></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="4">
													<asp:panel id="panCuotas" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="TabCuotas" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblCuotDeudaSoc" runat="server" cssclass="titulo">Total Deuda Soc. a Pagar:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtCuotDeudaSoc" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" Enabled="false" style="text-align:right"></cc1:numberbox></TD>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblCuotDeudaCtac" runat="server" cssclass="titulo">Total Deuda Cta.Cte. a Pagar:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtCuotDeudaCtac" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" Enabled="false" style="text-align:right"></cc1:numberbox></TD>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblCuotConc" runat="server" cssclass="titulo">Conceptos:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtCuotConc" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		style="text-align:right" EsDecimal="True" Enabled="false"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 100%" align="left" colSpan="6">
																	<asp:datagrid id="grdCuotas" runat="server" BorderWidth="1px" BorderStyle="None" width="95%" AllowPaging="True"
																		HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnEditCommand="mEditarDatosCuota"
																		OnPageIndexChanged="grdCuotas_PageChanged" AutoGenerateColumns="False">
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<Columns>
																			<asp:TemplateColumn>
																				<HeaderStyle Width="20px"></HeaderStyle>
																				<ItemTemplate>
																					<asp:LinkButton id="Linkbutton5" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																						<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																					</asp:LinkButton>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn Visible="False" DataField="plcu_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																			<asp:BoundColumn DataField="plcu_cuota" HeaderText="Cuota"></asp:BoundColumn>
																			<asp:BoundColumn DataField="plcu_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																			<asp:BoundColumn DataField="plcu_impo" HeaderText="Importe" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblCuotasTotal" runat="server" cssclass="titulo">Total Cuotas:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtCuotasTotal" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" Enabled="false" style="text-align:right"></cc1:numberbox></TD>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblCuotDeudaTotal" runat="server" cssclass="titulo">Total A Pagar:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtCuotDeudaTotal" runat="server" cssclass="cuadrotextodeshab" Width="60px"
																		AceptaNull="False" EsDecimal="True" Enabled="false" style="text-align:right"></cc1:numberbox></TD>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblCuotasDife" runat="server" cssclass="titulo">Diferencia:</asp:Label>&nbsp;</TD>
																<TD>
																	<cc1:numberbox id="txtCuotasDife" runat="server" cssclass="cuadrotextodeshab" Width="60px" AceptaNull="False"
																		EsDecimal="True" Enabled="false" style="text-align:right"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD vAlign="bottom" colSpan="4" height="16"></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="6" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblCuota" runat="server" cssclass="titulo">Cuota:</asp:Label>&nbsp;</TD>
																<TD colSpan="5">
																	<cc1:numberbox id="txtCuota" runat="server" cssclass="cuadrotextodeshab" Width="100px" Enabled="False"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:Label id="lblImpoCuota" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
																<TD colSpan="5">
																	<cc1:numberbox id="txtImpoCuota" runat="server" cssclass="cuadrotexto" Width="100px" EsDecimal="True"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:Label id="lblFechaCuota" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																<TD colSpan="5">
																	<cc1:DateBox id="txtFechaCuota" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
															</TR>
															<TR>
																<TD vAlign="middle" align="center" colSpan="6" height="30">
																	<asp:Button id="btnAltaCuota" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																	<asp:Button id="btnBajaCuota" runat="server" cssclass="boton" Width="85px" Text="Eliminar"></asp:Button>&nbsp;
																	<asp:Button id="btnModiCuota" runat="server" cssclass="boton" Width="90px" Text="Modificar"></asp:Button>&nbsp;
																	<asp:Button id="btnLimpCuota" runat="server" cssclass="boton" Width="85px" Text="Limpiar"></asp:Button></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
										</TABLE>
									</asp:panel>
									<ASP:PANEL Runat="server" ID="panBotones">
										<TABLE width="100%">
											<TR>
												<TD align="center">
													<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
											</TR>
											<TR height="30">
												<TD align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
													<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Baja"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Modificar"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Limpiar"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnConf" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Confirmar"></asp:Button>&nbsp;
													<asp:Button id="btnImpr" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Imprimir"></asp:Button>
												</TD>
											</TR>
										</TABLE>
									</ASP:PANEL>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="hdnCta" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnApro" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnSociId" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnCuotId" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnConcId" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnDeudaSocPlan" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnDeudaSocPag" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnDeudaCtacPag" runat="server"></asp:TextBox>
				<asp:textbox id="hdnImprId" runat="server"></asp:textbox>
				<asp:TextBox id="hdnDeudaCtacPlan" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnDeudaSocDife" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnDeudaCtacDife" runat="server"></asp:TextBox>
				<asp:TextBox id="hdnDeudaSocFecha" runat="server"></asp:TextBox>
				<asp:textbox id="hdnImprimir" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimirInte" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimio" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnFactNume" runat="server"></asp:textbox>
				<asp:textbox id="hdnDeudaSocCuot" runat="server"></asp:textbox>
				<asp:textbox id="hdnDeudaSocInte" runat="server"></asp:textbox>
				<asp:textbox id="hdnDeudaSocDesc" runat="server"></asp:textbox>
				
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT>
				</OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
	function chkPlanCheck(pChk, pTipo)
	{
		var lstrFila;
		var oSubPlan;
		var lintFil= eval(pChk.id.replace("grdDeuda" + pTipo + "__ctl","").replace("_chkPlan",""));
		var lbooChecked = pChk.checked;
		var lbooPlanOri;
		
		lbooPlanOri = !lbooChecked;
		
		dValor = eval(document.all(pChk.id.replace("chkPlan", "divTotal")).innerText.replace(",",""))

		if(pTipo=="Soc")
		{
			mCompletarFilas(lbooChecked, lintFil, pTipo)
			mObtenerTotal(pChk, pTipo);
		}
		else
			mReCalcularTotales(lbooPlanOri, pChk.checked,dValor, pTipo);
	}

	function mObtenerTotal(pChk, pTipo)
	{
		var lstrFiltro, vsRet;
		var lbooChecked = pChk.checked;

		lstrFiltro = document.all("hdnSociId").value + "," + document.all("hdnDeudaSocFecha").value + "," + document.all(pChk.id.replace("chkPlan", "divPerio")).innerText;

		if(lbooChecked)
			lstrFiltro += ",1";
		else
			lstrFiltro += ",0";
			
		vsRet = EjecutarMetodoXML("Utiles.PlanDeudaConsulTotales", lstrFiltro).split(";");
		
		document.all("txtDeuda" + pTipo + "Plan").value = vsRet[0];
		document.all("txtDeuda" + pTipo + "Dife").value = vsRet[1];
		
		document.all("txtDeuda" + pTipo + "Total").value = vsRet[0];
		document.all("txtDeuda" + pTipo + "Cuot").value = vsRet[2];
		document.all("hdnDeuda" + pTipo + "Cuot").value = vsRet[2];
		document.all("txtDeuda" + pTipo + "Inte").value = vsRet[3];
		document.all("hdnDeuda" + pTipo + "Inte").value = vsRet[3];
		document.all("txtDeuda" + pTipo + "Desc").value = vsRet[4];
		document.all("hdnDeuda" + pTipo + "Desc").value = vsRet[4];

		document.all("hdnDeuda" + pTipo + "Plan").value = document.all("txtDeuda" + pTipo + "Plan").value;
		document.all("hdnDeuda" + pTipo + "Dife").value = document.all("txtDeuda" + pTipo + "Dife").value;
	}
	
	function mCompletarFilas(pbooChecked, pintFil, pTipo)
	{
		var lstrFila;
		var oSubPlan;
		var lbooPlanOri;

		for (var fila=3; fila <= document.all("grdDeuda" + pTipo).children(0).children.length; fila++)
		{
			lstrFila = "grdDeuda" + pTipo + "__ctl" + fila.toString()
			oSubPlan = document.all(lstrFila + "_chkPlan");
			dValor = eval(document.all(lstrFila + "_divTotal").innerText.replace(",",""))

			lbooPlanOri = oSubPlan.checked;
	
			if (!pbooChecked&&fila>pintFil)
			{
				oSubPlan.checked = false;
			}
			
			if((pbooChecked)&&(fila<=pintFil))
				oSubPlan.checked = true;
		}
	}
	
	function mReCalcularTotales(PlanCheckOri, PlanCheck, dValor, pTipo)
	{
		var dTotalPlan=0;
		var dDife=0;
		
		dTotalPlan = eval(document.all("txtDeuda" + pTipo + "Plan").value.replace(",","").replace(".",""));
		dDife = eval(document.all("txtDeuda" + pTipo + "Dife").value.replace(",","").replace(".",""));
		
		dDife = dDife + dTotalPlan;

		if(PlanCheckOri&&!PlanCheck)
			dTotalPlan=dTotalPlan-dValor;
		if(!PlanCheckOri&&PlanCheck)
			dTotalPlan=dTotalPlan+dValor;

		dDife = dDife - dTotalPlan;

		dTotalPlan = dTotalPlan.toString();
		dDife = dDife.toString();

		if (dTotalPlan==0)
			document.all("txtDeuda" + pTipo + "Plan").value = "0.00"
		else
			document.all("txtDeuda" + pTipo + "Plan").value = dTotalPlan.substring(0, dTotalPlan.length - 2) + "." + dTotalPlan.substring(dTotalPlan.length - 2 , dTotalPlan.length );

		if (dDife==0)
			document.all("txtDeuda" + pTipo + "Dife").value = "0.00"
		else
			document.all("txtDeuda" + pTipo + "Dife").value = dDife.substring(0, dDife.length - 2) + "." + dDife.substring(dDife.length - 2 , dDife.length );

		document.all("hdnDeuda" + pTipo + "Plan").value = document.all("txtDeuda" + pTipo + "Plan").value;
		document.all("hdnDeuda" + pTipo + "Dife").value = document.all("txtDeuda" + pTipo + "Dife").value;
	}
	
	function Imprimir()
	{
		if(document.all("hdnImprimir").value!="")
		{
			if (window.confirm("�Desea imprimir la ND Nro " + document.all("hdnFactNume").value + "?"))
			{
				try{

					var sRet = mImprimirCopias(2,'O',document.all("hdnImprimir").value,"Factura", "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original

					if(sRet=="0") return(sRet);

					if (window.confirm("�Se imprimio la ND correctamente?"))
					{
						document.all("hdnImprimio").value=1;

						var lstrCopias = LeerCamposXML("comprobantes_imprimir", document.all("hdnImprimir").value, "copias");

						if (lstrCopias != "0" && lstrCopias != "1")
							sRet = mImprimirCopias(lstrCopias,'D',document.all("hdnImprimir").value,"Factura","<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias
							
						document.all("hdnImprId").value = '';

						if (document.all("hdnImprimirInte").value != '')						
						{
							// ND por intereses
							var sRetInte = mImprimirCopias(2,'O',document.all("hdnImprimirInte").value,"Factura", "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original
							var lstrCopiasInte = LeerCamposXML("comprobantes_imprimir", document.all("hdnImprimirInte").value, "copias");

							if (lstrCopiasInte != "0" && lstrCopiasInte != "1")
								sRet = mImprimirCopias(lstrCopiasInte,'D',document.all("hdnImprimirInte").value,"Factura","<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias
						}
					}
					else
					{
						document.all("hdnImprId").value = '';
						Imprimir();
					}
				}
				catch(e)
				{
					alert("Error al intentar efectuar la impresi�n");
				}
			}
			else
			{
				document.all("hdnImprimio").value=0;
			}
			__doPostBack('hdnImprimio','');
		}
	}
	function mCargarClienteSocio(pOrigen)
		{
			var sFiltro = '';
			
			if(pOrigen == 'F')
				sFiltro= document.all("txtNroSocioFil").value;
			else
				sFiltro= document.all("txtSocio").value;

 			if (sFiltro != '')
 			{
 				sFiltro = "@soci_nume=" + sFiltro;
				var vstrRet = LeerCamposXML("socios_datos", sFiltro, "clie_id").split("|");
				
				if(vstrRet[0]=="")
					alert("Socio inexistente");
				else
				{
					if(pOrigen == 'F')
					{	document.all["usrClieFil:txtCodi"].value = vstrRet[0]; //selecciona el cliente
						document.all["usrClieFil:txtCodi"].onchange();
					}
					else
					{	document.all["usrClie:txtCodi"].value = vstrRet[0]; //selecciona el cliente
						document.all["usrClie:txtCodi"].onchange();
					}
				}
			}
		}
		if (document.all["cmbCcos"]!= null)
		{
			document.all('cmbCCos').disabled = (document.all('cmbCCos').options.length <= 1);
			document.all('txtcmbCCos').disabled = (document.all('cmbCCos').options.length <= 1);
		}
		</SCRIPT>
	</BODY>
</HTML>
