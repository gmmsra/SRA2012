'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar de la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_Criaderos_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
' a la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'Criaderos.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
Imports System.Data.SqlClient


Namespace SRA


'Partial Class Criaderos
Partial Class Migrated_Criaderos

Inherits Criaderos


#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Criaderos
   Private mstrCriaRaza As String = SRA_Neg.Constantes.gTab_Criaderos_Razas
   Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
   Private mstrClieDire As String = SRA_Neg.Constantes.gTab_ClientesDire
   Private mstrClieTele As String = SRA_Neg.Constantes.gTab_ClientesTele
   Private mstrClieMail As String = SRA_Neg.Constantes.gTab_ClientesMails
   Private mstrActiTele As String = SRA_Neg.Constantes.gTab_ActiTele
   Private mstrActiDire As String = SRA_Neg.Constantes.gTab_ActiDire
   Private mstrActiMail As String = SRA_Neg.Constantes.gTab_ActiMail
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private mstrConn As String
'   Public mbooActi As Boolean
'   Public mintActiDefa As Integer
'   Public mstrCriaCtrl As String
   Private mstrCriaId As String
   Private mstrValorId As String
   Private mbooSoloBusq As Boolean
   Private mstrFilNomb As String
   Private mstrFilNume As String
   Private mstrFilClie As String
   Private mstrFilRaza As String
   Private mstrFilLoca As String

   Private Enum Columnas As Integer
      CriaSele = 0
      CriaEdit = 1
      CriaId = 2
      CriaNume = 3
      CriaDesc = 4
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Dim lstrFil As String
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializarControl()
         mInicializar()
         If (Not Page.IsPostBack) Then

            If mstrCriaCtrl = "" Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
            End If

            Session(mSess(mstrTabla)) = Nothing

            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mMostrarPanel(False)
            mCargarLocalidades(False)

            If mstrCriaCtrl <> "" Then
                mMostrarPanel(False)
                lstrFil = txtNumeFil.Text & txtNombFil.Text & usrClieFil.Valor.ToString & cmbRazaFil.SelectedValue.ToString & cmbPaisFil.SelectedValue.ToString & cmbProvFil.SelectedValue.ToString & cmbLocaFil.SelectedValue.ToString
                If lstrFil <> "" And lstrFil <> "0" Then
                    mConsultar()
                End If
            Else
                mMostrarPanel(False)
            End If

         Else
            If panDato.Visible Then
               mdsDatos = Session(mSess(mstrTabla))
               Dim x As String = Session.SessionID
            End If
            mCargarLocalidades(True)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarLocalidades(ByVal pbooFiltro As Boolean)

    If Not pbooFiltro Then
      If Not Request.Form(cmbDirePais.UniqueID) Is Nothing Then
         clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
         If Not Request.Form(cmbDirePcia.UniqueID) Is Nothing Then
            cmbDirePcia.Valor = Request.Form(cmbDirePcia.UniqueID)
            If Not cmbDirePcia.Valor Is System.DBNull.Value Then
               clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbDirePcia.Valor, cmbDireLoca, "id", "descrip", "S", True)
               cmbDireLoca.Valor = Request.Form(cmbDireLoca.UniqueID)
            Else
               cmbDireLoca.Limpiar()
            End If
         End If
      End If
    Else
      If Not Request.Form(cmbPaisFil.UniqueID) Is Nothing Then
         clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbPaisFil.Valor, cmbProvFil, "id", "descrip", "S", True)
         If Not Request.Form(cmbProvFil.UniqueID) Is Nothing Then
            cmbProvFil.Valor = Request.Form(cmbProvFil.UniqueID)
            If Not cmbProvFil.Valor Is System.DBNull.Value Then
               clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbProvFil.Valor, cmbLocaFil, "id", "descrip", "S", True)
               cmbLocaFil.Valor = Request.Form(cmbLocaFil.UniqueID)
            Else
               cmbLocaFil.Limpiar()
            End If
         End If
      End If
    End If
   End Sub

   Private Sub mCargarCombos()

      clsWeb.gCargarRefeCmb(mstrConn, "tele_tipos", cmbTeleTipo, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDirePais, "S")
      clsWeb.gCargarCombo(mstrConn, "razas_cargar @espe_genealo=0", cmbRaza, "id", "descrip_codi", "S")
      SRA_Neg.Utiles.gSetearRaza(cmbRaza)

      clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbPaisFil, "S")
      clsWeb.gCargarCombo(mstrConn, "razas_cargar @espe_genealo=0", cmbRazaFil, "id", "descrip_codi", "S")
      SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)

      If Not Request.Form("cmbDirePais") Is Nothing Then
         cmbDirePais.Valor = Request.Form("cmbDirePais")
         clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
      End If

      If Not Request.Form("cmbPaisFil") Is Nothing Then
         cmbPaisFil.Valor = Request.Form("cmbPaisFil")
         clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbPaisFil.Valor, cmbProvFil, "id", "descrip", "S", True)
      End If

      'inicializar valores
      If mValorParametro(Request.QueryString("Raza")) <> "" Then
         cmbRazaFil.Valor = mValorParametro(Request.QueryString("Raza"))
      End If
      If mValorParametro(Request.QueryString("Pais")) <> "" Then
         cmbPaisFil.Valor = mValorParametro(Request.QueryString("Pais"))
      End If
      If mValorParametro(Request.QueryString("Prov")) <> "" Then
         clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbPaisFil.Valor, cmbProvFil, "id", "descrip", "S", True)
         cmbProvFil.Valor = mValorParametro(Request.QueryString("Prov"))
      End If
      If mValorParametro(Request.QueryString("Loca")) <> "" Then
         clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbProvFil.Valor, cmbLocaFil, "id", "descrip", "S", True)
         cmbLocaFil.Valor = mValorParametro(Request.QueryString("Loca"))
      End If

   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaTele.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaDire.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaMail.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnBajaRaza.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub

   Private Sub mSetearMaxLength()

      Dim lstrCriaLong As Object
      Dim lstrClieTeleLong As Object
      Dim lstrClieDireLong As Object
      Dim lstrClieMailLong As Object
      Dim lintCol As Integer

      lstrCriaLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtNume.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
      txtNomb.MaxLength = clsSQLServer.gObtenerLongitud(lstrCriaLong, "crdr_desc")
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrCriaLong, "crdr_obse")

      lstrClieTeleLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClieTele)
      txtTeleNume.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_tele")
      txtTeleArea.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_area_code")
      txtTeleRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieTeleLong, "tecl_refe")

      lstrClieDireLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClieDire)
      txtDire.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_dire")
      txtDireCP.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_cpos")
      txtDireRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieDireLong, "dicl_refe")

      lstrClieMailLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrClieMail)
      txtMail.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieMailLong, "macl_mail")
      txtMailRefe.MaxLength = clsSQLServer.gObtenerLongitud(lstrClieMailLong, "macl_refe")

      txtNumeFil.MaxLength = txtNume.MaxLength
      txtNombFil.MaxLength = txtNomb.MaxLength

   End Sub

#End Region

#Region "Inicializacion de Variables"

   Private Sub mCriadero(ByVal mstrCriaId As String, ByVal mstrCriaNume As String, ByVal mstrCriaNomb As String)
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].value='{1}';", Request.QueryString("ctrlId"), mstrCriaNume))
      lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtId'].value='{1}';", Request.QueryString("ctrlId"), mstrCriaId))
      lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCriaNomb'].value='{1}';", Request.QueryString("ctrlId"), mstrCriaNomb))
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub

   Public Overrides Sub mInicializarControl()

      Try
         mstrCriaCtrl = mValorParametro(Request.QueryString("ctrlId"))
         mbooSoloBusq = mValorParametro(Request.QueryString("SoloBusq")) = "1"
         mstrValorId = mValorParametro(Request.QueryString("ValorId"))
         mstrCriaId = mValorParametro(Request.QueryString("CriaId"))

         If hdnValorId.Text = "" Then
            hdnValorId.Text = mstrValorId 'pone el id seleccionado en el control de b�squeda
         ElseIf hdnValorId.Text = "-1" Then  'si ya se limpiaron los filtros, ignora el id con el que vino
            mstrValorId = ""
         End If

         'campos a filtrar
         If mstrCriaCtrl <> "" Then
            mstrFilNomb = mValorParametro(Request.QueryString("FilNomb"))
            mstrFilNume = mValorParametro(Request.QueryString("FilNume"))
            mstrFilClie = mValorParametro(Request.QueryString("FilClie"))
            mstrFilRaza = mValorParametro(Request.QueryString("FilRaza"))
            mstrFilLoca = mValorParametro(Request.QueryString("FilLoca"))
         Else
            mstrFilNomb = 1
            mstrFilNume = 1
            mstrFilClie = 1
            mstrFilRaza = 1
            mstrFilLoca = 1
         End If

         'valores de los filtros
         If Not Page.IsPostBack Then
            usrClieFil.Valor = mValorParametro(Request.QueryString("ClieNume"))
            txtNombFil.Text = mValorParametro(Request.QueryString("Nomb"))
            txtNumeFil.Text = mValorParametro(Request.QueryString("CriaNume"))
            cmbRazaFil.Valor = mValorParametro(Request.QueryString("Raza"))
            cmbPaisFil.Valor = mValorParametro(Request.QueryString("Pais"))
            cmbProvFil.Valor = mValorParametro(Request.QueryString("Prov"))
            mCargarLocalidades(True)
            cmbLocaFil.Valor = mValorParametro(Request.QueryString("Loca"))
         End If

         'columnas a mostrar
         'mstrColNomb = mValorParametro(Request.QueryString("ColNomb"))

         If mbooSoloBusq Then
            grdDato.Columns(1).Visible = False
            btnAgre.Enabled = False
         End If

         'seteo de controles segun filtros
         grdDato.Columns(0).Visible = (mstrCriaCtrl <> "")

         If mstrCriaCtrl <> "" Then
            'grdDato.Columns(mintGrdEsta).Visible = (mstrCriaCtrl = "")
         Else
            'grdDato.Columns(mintGrdNomb).Visible = False
         End If

         If mstrCriaCtrl <> "" Then
            lblNombFil.Visible = True '(mstrFilNomb = "1")
            txtNombFil.Visible = True '(mstrFilNomb = "1")
            lblClieFil.Visible = (mstrFilClie = "1")
            usrClieFil.Visible = (mstrFilClie = "1")
            lblRazaFil.Visible = (mstrFilRaza = "1")
            cmbRazaFil.Visible = (mstrFilRaza = "1")
            lblLocaFil.Visible = (mstrFilLoca = "1")
            cmbLocaFil.Visible = (mstrFilLoca = "1")
            lblPaisFil.Visible = (mstrFilLoca = "1")
            cmbPaisFil.Visible = (mstrFilLoca = "1")
            lblProvFil.Visible = (mstrFilLoca = "1")
            cmbProvFil.Visible = (mstrFilLoca = "1")
         Else
            'cmbRazaFil.Visible = True
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub mInicializar()
      clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintActiDefa = clsSQLServer.gConsultarValor("exec usuarios_actividades_default " & Session("sUserId").ToString, mstrConn, 0)

      usrClieFil.Tabla = mstrClientes
      usrClieFil.AutoPostback = False
      usrClieFil.FilClaveUnica = True
      usrClieFil.ColClaveUnica = True
      usrClieFil.FilTipo = "T"
      usrClieFil.Ancho = 790
      usrClieFil.Alto = 510

      usrClie.Tabla = mstrClientes
      usrClie.AutoPostback = False
      usrClie.FilClaveUnica = True
      usrClie.ColClaveUnica = True
      usrClie.FilTipo = "T"
      usrClie.Ancho = 790
      usrClie.Alto = 510

   End Sub

   Private Function mValorParametro(ByVal pstrPara As String) As String
      Dim lstrPara As String
      If pstrPara Is Nothing Then
         lstrPara = ""
      Else
         If pstrPara Is System.DBNull.Value Then
            lstrPara = ""
         Else
            lstrPara = pstrPara
         End If
      End If
      Return (lstrPara)
   End Function

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Overrides Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.EditItemIndex = -1
         If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
            grdDato.CurrentPageIndex = 0
         Else
            grdDato.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub mValidarConsulta()

      Try

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub mSeleccionarCriadero(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lstrCriaId As String, lstrCriaNume As String, lstrCriaNomb As String
      lstrCriaId = E.Item.Cells(Columnas.CriaId).Text
      lstrCriaNume = Replace(E.Item.Cells(Columnas.CriaNume).Text, "&nbsp;", "")
      lstrCriaNomb = Replace(E.Item.Cells(Columnas.CriaDesc).Text, "&nbsp;", "")
      mCriadero(lstrCriaId, lstrCriaNume, lstrCriaNomb)
   End Sub

   Public Overrides Sub mConsultar()
      Try
         Dim lstrCmd As String

         lstrCmd = ObtenerSql(Session("sUserId").ToString(), IIf(chkBusc.Checked, 1, 0), IIf(chkBaja.Checked, 1, 0), usrClieFil.Valor, txtNumeFil.Text, _
                              cmbPaisFil.SelectedValue, cmbProvFil.SelectedValue, cmbLocaFil.SelectedValue, txtNombFil.Text, cmbRazaFil.SelectedValue)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

         If Not Page.IsPostBack And grdDato.Items.Count = 1 And mstrCriaCtrl <> "" Then
            If mstrValorId = "" Then
               mCriadero(grdDato.Items(0).Cells(Columnas.CriaId).Text, grdDato.Items(0).Cells(Columnas.CriaNume).Text, grdDato.Items(0).Cells(Columnas.CriaDesc).Text)
            Else
               mCargarDatos(mstrValorId)
               Return
            End If
         End If

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

'   Public Shared Function ObtenerSql(ByVal pstrUserId As String, ByVal pintBusc As Integer, ByVal pintBaja As Integer, ByVal pstrClieId As String, ByVal pstrNume As String, _
'                                     ByVal pstrPais As String, ByVal pstrProv As String, ByVal pstrLoca As String, ByVal pstrDesc As String, ByVal pstrRaza As String) As String
'      Dim lstrCmd As New StringBuilder
'
'      'esta funci�n se llama tambi�n desde clsXMLHTTP
'      lstrCmd.Append("exec criaderos_busq")
'      lstrCmd.Append(" @audi_user=" + clsSQLServer.gFormatArg(pstrUserId, SqlDbType.Int))
'      lstrCmd.Append(" , @crdr_clie_id=" + clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int))
'      lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)
'      lstrCmd.Append(" , @buscar_en=" + pintBusc.ToString)
'      lstrCmd.Append(" , @crdr_nume=" + clsSQLServer.gFormatArg(pstrNume, SqlDbType.Int))
'      lstrCmd.Append(" , @crdr_desc=" + clsSQLServer.gFormatArg(pstrDesc, SqlDbType.VarChar))
'      lstrCmd.Append(" , @pais_id=" + clsSQLServer.gFormatArg(pstrPais, SqlDbType.Int))
'      lstrCmd.Append(" , @prov_id=" + clsSQLServer.gFormatArg(pstrProv, SqlDbType.Int))
'      lstrCmd.Append(" , @loca_id=" + clsSQLServer.gFormatArg(pstrLoca, SqlDbType.Int))
'      lstrCmd.Append(" , @raza_id=" + clsSQLServer.gFormatArg(pstrRaza, SqlDbType.Int))
'
'      Return (lstrCmd.ToString)
'   End Function
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      Select Case pstrTabla
         Case mstrClieTele
            btnBajaTele.Enabled = Not (pbooAlta)
            btnModiTele.Enabled = Not (pbooAlta)
            btnAltaTele.Enabled = pbooAlta
         Case mstrClieDire
            btnBajaDire.Enabled = Not (pbooAlta)
            btnModiDire.Enabled = Not (pbooAlta)
            btnAltaDire.Enabled = pbooAlta
         Case mstrClieMail
            btnBajaMail.Enabled = Not (pbooAlta)
            btnModiMail.Enabled = Not (pbooAlta)
            btnAltaMail.Enabled = pbooAlta
         Case mstrCriaRaza
            btnBajaRaza.Enabled = True 'Not (pbooAlta)
            btnModiRaza.Enabled = True 'Not (pbooAlta)
            btnAltaRaza.Enabled = True 'pbooAlta
         Case Else
            btnBaja.Enabled = Not (pbooAlta)
            btnModi.Enabled = Not (pbooAlta)
            btnAlta.Enabled = pbooAlta
      End Select
   End Sub

   Public Overrides Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         mCargarDatos(E.Item.Cells(Columnas.CriaId).Text)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub mCargarDatos(ByVal pstrId As String)

      mLimpiar()

      hdnId.Text = clsFormatear.gFormatCadena(pstrId)

      mCrearDataSet(hdnId.Text)

      grdTele.CurrentPageIndex = 0
      grdDire.CurrentPageIndex = 0
      grdMail.CurrentPageIndex = 0
      grdRaza.CurrentPageIndex = 0

      If mdsDatos.Tables(0).Rows.Count > 0 Then
         With mdsDatos.Tables(0).Rows(0)
            hdnId.Text = .Item("crdr_id")
            usrClie.Valor = .Item("crdr_clie_id")
            txtObse.Valor = .Item("crdr_obse")
            txtNume.Valor = .Item("crdr_nume")
            txtNomb.Valor = .Item("crdr_desc")

            If Not .IsNull("crdr_baja_fecha") Then
               lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("crdr_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
            Else
               lblBaja.Text = ""
            End If

            lblTitu.Text = "Datos del Criadero: " & .Item("crdr_desc")
            lblTitu.Visible = False
         End With

         mSetearEditor("", False)
         mSetearEditor(mstrClieTele, True)
         mSetearEditor(mstrClieDire, True)
         mSetearEditor(mstrClieMail, True)
         mSetearEditor(mstrCriaRaza, True)
         mMostrarPanel(True)
         mShowTabs(1)

      End If
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnAlta.Enabled = True
      mMostrarPanel(True)
   End Sub

   Private Sub mLocalidades()
      Try
         Dim lstrPagina As String = "localidades.aspx?t=popup&pa=" & cmbDirePais.Valor & "&pr=" & cmbDirePcia.Valor
         clsWeb.gGenerarPopUp(Me, lstrPagina, 650, 400, 10, 75, True)
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mLimpiarFiltros()

      hdnValorId.Text = "-1"  'para que ignore el id que vino del control
      txtNumeFil.Text = ""
      txtNombFil.Text = ""
      chkBaja.Checked = False
      chkBusc.Checked = False
      usrClieFil.Limpiar()
      cmbRazaFil.Limpiar()
      cmbPaisFil.Limpiar()
      cmbProvFil.Limpiar()
      cmbLocaFil.Limpiar()

   End Sub

   Private Sub mLimpiar()

      hdnId.Text = ""
      lblBaja.Text = ""
      txtObse.Text = ""
      txtNume.Text = ""
      txtNomb.Text = ""
      usrClie.Limpiar()

      mLimpiarTelefono()
      mLimpiarDireccion()
      mLimpiarMail()

      mCrearDataSet("")

      lblTitu.Text = ""

      grdTele.CurrentPageIndex = 0
      grdDire.CurrentPageIndex = 0
      grdMail.CurrentPageIndex = 0
      grdRaza.CurrentPageIndex = 0

      mSetearEditor("", True)

   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      btnAgre.Visible = (Not panDato.Visible And Not mbooActi)

      lnkCabecera.Font.Bold = True
      lnkTele.Font.Bold = False

      panDato.Visible = pbooVisi
      panFiltros.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      panCabecera.Visible = True
      panTele.Visible = False

      tabLinks.Visible = pbooVisi

      lnkCabecera.Font.Bold = True
      lnkTele.Font.Bold = False

   End Sub

   Private Sub mShowTabs(ByVal origen As Byte)
      Dim lstrTitu As String
      If lblTitu.Text <> "" Then
         lstrTitu = lblTitu.Text.Substring(lblTitu.Text.IndexOf(":") + 2)
      End If

      panDato.Visible = True
      panBotones.Visible = True
      btnAgre.Visible = False
      panTele.Visible = False
      panDire.Visible = False
      panMail.Visible = False
      panRaza.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkTele.Font.Bold = False
      lnkDire.Font.Bold = False
      lnkMail.Font.Bold = False
      lnkRaza.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Visible = False
         Case 2
            panTele.Visible = True
            lnkTele.Font.Bold = True
            lblTitu.Text = "Tel�fonos del Criadero: " & lstrTitu
            lblTitu.Visible = True
            grdTele.Visible = True
         Case 3
            panDire.Visible = True
            lnkDire.Font.Bold = True
            lblTitu.Text = "Direcciones del Criadero: " & lstrTitu
            lblTitu.Visible = True
            grdDire.Visible = True
         Case 4
            panMail.Visible = True
            lnkMail.Font.Bold = True
            lblTitu.Text = "Mails del Criadero: " & lstrTitu
            lblTitu.Visible = True
            grdMail.Visible = True
         Case 5
            panRaza.Visible = True
            lnkRaza.Font.Bold = True
            lblTitu.Text = "Razas del Criadero: " & lstrTitu
            lblTitu.Visible = True
            grdRaza.Visible = True
      End Select

   End Sub

   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub

#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim lstrClieId As String

         mGuardarDatos()
         Dim lobjCriadero As New SRA_Neg.Criaderos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrClieTele, mstrClieDire, mstrClieMail, mstrCriaRaza, mdsDatos, Server)

         lstrClieId = lobjCriadero.Alta()

         If mstrCriaCtrl <> "" Then
            mCriadero(lstrClieId, txtNume.Text, "")
            Return
         End If

         mConsultar()

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try

         mGuardarDatos()
         Dim lobjCriadero As New SRA_Neg.Criaderos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrClieTele, mstrClieDire, mstrClieMail, mstrCriaRaza, mdsDatos, Server)

         lobjCriadero.Modi()

         If mstrValorId <> "" Then
            mCriadero(hdnId.Text, txtNume.Text, "")
            Return
         End If

         mConsultar()

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lintPage As Integer = grdDato.CurrentPageIndex

         Dim lobjCriadero As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjCriadero.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         grdDato.CurrentPageIndex = 0

         mConsultar()

         If (lintPage < grdDato.PageCount) Then
            grdDato.CurrentPageIndex = lintPage
            mConsultar()
         End If

         mMostrarPanel(False)

         hdnModi.Text = "S"

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim lintCpo As Integer

      If txtNume.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el nro. del criadero.")
      End If

      If txtNomb.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el nombre del criadero.")
      End If

      If usrClie.Valor Is DBNull.Value Or usrClie.Valor = 0 Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el cliente.")
      End If

      If mdsDatos.Tables(mstrClieDire).Select().GetUpperBound(0) <> -1 Then
         If mdsDatos.Tables(mstrClieDire).Select("dicl_defa=True").GetUpperBound(0) <> 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar una, y solo una, direcci�n default.")
         End If
      End If

      With mdsDatos.Tables(0).Rows(0)
         .Item("crdr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("crdr_nume") = txtNume.Valor
         .Item("crdr_desc") = txtNomb.Valor
         .Item("crdr_obse") = txtObse.Valor
         .Item("crdr_clie_id") = usrClie.Valor
      End With

      Return mdsDatos
   End Function

   Public Overrides Sub mCrearDataSet(ByVal pstrId As String)

      Dim lstrOpci As String

      lstrOpci = "@acti=3"
      lstrOpci = lstrOpci & ",@audi_user=" & Session("sUserId").ToString
      mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, lstrOpci)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrClieTele
      mdsDatos.Tables(2).TableName = mstrClieDire
      mdsDatos.Tables(3).TableName = mstrClieMail
      mdsDatos.Tables(4).TableName = mstrCriaRaza

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
      grdTele.DataBind()

      grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
      grdDire.DataBind()

      grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
      grdMail.DataBind()

      grdRaza.DataSource = mdsDatos.Tables(mstrCriaRaza)
      grdRaza.DataBind()

      Session(mSess(mstrTabla)) = mdsDatos

   End Sub

#End Region

#Region "Eventos de Controles"

   Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

#End Region

#Region "Detalle"
   Public Overrides Sub mEditarDatosTele(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrTele As DataRow
         Dim lstrActiProp As String
         Dim lstrActiNoProp As String
         Dim lbooGene As Boolean

         hdnTeleId.Text = E.Item.Cells(1).Text
         ldrTele = mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & hdnTeleId.Text)(0)

         With ldrTele
            txtTeleArea.Valor = .Item("tecl_area_code")
            txtTeleNume.Valor = .Item("tecl_tele")
            txtTeleRefe.Valor = .Item("tecl_refe")
            cmbTeleTipo.Valor = .Item("tecl_teti_id")
            lstrActiProp = .Item("acti_prop")
            lstrActiNoProp = .Item("acti_noprop")
            lbooGene = .Item("tecl_gene")
         End With

         If mbooActi Then
            mHabilitarTele(mintActiDefa.ToString = lstrActiProp And Not lbooGene)
         End If

         grdActiTele.Visible = True

         mSetearEditor(mstrClieTele, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub mEditarDatosDire(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrDire As DataRow
         Dim lstrActiProp As String
         Dim lstrActiNoProp As String
         Dim lbooGene As Boolean

         hdnDireId.Text = E.Item.Cells(1).Text
         ldrDire = mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & hdnDireId.Text)(0)

         With ldrDire
            cmbDirePais.Valor = .Item("_dicl_pais_id")
            txtDire.Valor = .Item("dicl_dire")
            txtDireCP.Valor = .Item("dicl_cpos")
            txtDireCP.ValidarPaisDefault = IIf(cmbDirePais.Valor = Session("sPaisDefaId"), "S", "N")
            txtDireRefe.Valor = .Item("dicl_refe")
            clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
            cmbDirePcia.Valor = .Item("_dicl_pcia_id")
            clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbDirePcia.Valor, cmbDireLoca, "id", "descrip", "S", True)
            clsWeb.gCargarCombo(mstrConn, "localidades_aux_cargar " & cmbDirePcia.Valor, cmbLocaAux, "id", "descrip", "S", True)
            cmbDireLoca.Valor = .Item("dicl_loca_id")
            chkDireDefa.Checked = .Item("dicl_defa")
            lstrActiProp = .Item("acti_prop")
            lstrActiNoProp = .Item("acti_noprop")
            lbooGene = .Item("dicl_gene")
         End With

         If mbooActi Then
            mHabilitarDire(mintActiDefa.ToString = lstrActiProp And Not lbooGene)
         End If

         grdActiDire.Visible = True

         mSetearEditor(mstrClieDire, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub mEditarDatosRaza(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrRaza As DataRow

         hdnRazaId.Text = E.Item.Cells(1).Text
         ldrRaza = mdsDatos.Tables(mstrCriaRaza).Select("crrz_id=" & hdnRazaId.Text)(0)

         With ldrRaza
            cmbRaza.Valor = .Item("crrz_raza_id")
         End With

         mSetearEditor(mstrCriaRaza, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub mEditarDatosMail(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim ldrMail As DataRow
         Dim lstrActiProp As String
         Dim lstrActiNoProp As String
         Dim lbooGene As Boolean

         hdnMailId.Text = E.Item.Cells(1).Text
         ldrMail = mdsDatos.Tables(mstrClieMail).Select("macl_id=" & hdnMailId.Text)(0)

         With ldrMail
            txtMail.Valor = .Item("macl_mail")
            txtMailRefe.Valor = .Item("macl_refe")
            lstrActiProp = .Item("acti_prop")
            lstrActiNoProp = .Item("acti_noprop")
            lbooGene = .Item("macl_gene")
         End With

         If mbooActi Then
            mHabilitarMail(mintActiDefa.ToString = lstrActiProp And Not lbooGene)
         End If

         grdActiMail.Visible = True

         mSetearEditor(mstrClieMail, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub grdTele_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdTele.EditItemIndex = -1
         If (grdTele.CurrentPageIndex < 0 Or grdTele.CurrentPageIndex >= grdTele.PageCount) Then
            grdTele.CurrentPageIndex = 0
         Else
            grdTele.CurrentPageIndex = E.NewPageIndex
         End If
         grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
         grdTele.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub grdDire_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDire.EditItemIndex = -1
         If (grdDire.CurrentPageIndex < 0 Or grdDire.CurrentPageIndex >= grdDire.PageCount) Then
            grdDire.CurrentPageIndex = 0
         Else
            grdDire.CurrentPageIndex = E.NewPageIndex
         End If
         grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
         grdDire.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub grdRaza_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdRaza.EditItemIndex = -1
         If (grdRaza.CurrentPageIndex < 0 Or grdRaza.CurrentPageIndex >= grdRaza.PageCount) Then
            grdRaza.CurrentPageIndex = 0
         Else
            grdRaza.CurrentPageIndex = E.NewPageIndex
         End If
         grdRaza.DataSource = mdsDatos.Tables(mstrCriaRaza)
         grdRaza.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Overrides Sub grdMail_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdMail.EditItemIndex = -1
         If (grdMail.CurrentPageIndex < 0 Or grdMail.CurrentPageIndex >= grdMail.PageCount) Then
            grdMail.CurrentPageIndex = 0
         Else
            grdMail.CurrentPageIndex = E.NewPageIndex
         End If
         grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
         grdMail.DataBind()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarTelefono(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosTele(pbooAlta)

         mLimpiarTelefono()
         grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
         grdTele.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarMail(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosMail(pbooAlta)

         mLimpiarMail()
         grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
         grdMail.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarRaza(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosRaza(pbooAlta)

         mLimpiarRaza()
         grdRaza.DataSource = mdsDatos.Tables(mstrCriaRaza)
         grdRaza.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarDireccion(ByVal pbooAlta As Boolean)
      Try
         mGuardarDatosDire(pbooAlta)

         mLimpiarDireccion()
         grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
         grdDire.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatosTele(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow
      Dim lstrCheck As System.Web.UI.WebControls.CheckBox
      Dim lstrProp As NixorControls.ComboBox
      Dim lstrActiProp As String
      Dim lstrActiNoProp As String
      Dim lstrActiPropDesc As String

      If txtTeleArea.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el c�digo de area.")
      End If

      If txtTeleNume.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el n�mero de tel�fono.")
      End If

      'If txtTeleRefe.Text = "" Then
         'Throw New AccesoBD.clsErrNeg("Debe indicar una referencia.")
      'End If

      If cmbTeleTipo.SelectedValue = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el tipo de tel�fono.")
      End If

      If hdnTeleId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrClieTele).NewRow
         ldrDatos.Item("tecl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieTele), "tecl_id")
         If mbooActi Then
            ldrDatos.Item("acti_prop") = mintActiDefa
         End If
      Else
         ldrDatos = mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & hdnTeleId.Text)(0)
      End If

      With ldrDatos
         .Item("tecl_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
         .Item("tecl_tele") = txtTeleNume.Valor
         .Item("tecl_area_code") = txtTeleArea.Valor
         .Item("_tele_desc") = txtTeleArea.Valor & " " & txtTeleNume.Valor
         .Item("tecl_refe") = txtTeleRefe.Valor
         .Item("tecl_teti_id") = cmbTeleTipo.Valor
         .Item("tecl_gene") = 0
         .Item("acti_prop") = "3"
         .Item("acti_noprop") = ""
      End With

      If hdnTeleId.Text = "" Then
         mdsDatos.Tables(mstrClieTele).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosMail(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow
      Dim lstrCheck As System.Web.UI.WebControls.CheckBox
      Dim lstrProp As NixorControls.ComboBox
      Dim lstrActiProp As String
      Dim lstrActiNoProp As String
      Dim lstrActiPropDesc As String

      If txtMail.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la direcci�n de mail.")
      End If

      'If txtMailRefe.Text = "" Then
         'Throw New AccesoBD.clsErrNeg("Debe indicar una referencia.")
      'End If

      If hdnMailId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrClieMail).NewRow
         ldrDatos.Item("macl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieMail), "macl_id")
         If mbooActi Then
            ldrDatos.Item("acti_prop") = mintActiDefa
         End If
      Else
         ldrDatos = mdsDatos.Tables(mstrClieMail).Select("macl_id=" & hdnMailId.Text)(0)
      End If

      With ldrDatos
         .Item("macl_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
         .Item("macl_mail") = txtMail.Valor
         .Item("macl_refe") = txtMailRefe.Valor
         .Item("macl_gene") = 0
         .Item("acti_prop") = "3"
      End With

      If hdnMailId.Text = "" Then
         mdsDatos.Tables(mstrClieMail).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosDire(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow
      Dim lstrCheck As System.Web.UI.WebControls.CheckBox
      Dim lstrProp As NixorControls.ComboBox
      Dim lstrActiProp As String
      Dim lstrActiNoProp As String
      Dim lstrActiPropDesc As String
      Dim vbooDefault As Boolean

      mCargarLocalidades(False)

      If txtDire.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar una direcci�n.")
      End If

      If txtDireCP.Text = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el c�digo postal.")
      End If

      'If txtDireRefe.Text = "" Then
         'Throw New AccesoBD.clsErrNeg("Debe indicar una referencia.")
      'End If

      If cmbDireLoca.SelectedValue = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la localidad.")
      End If

      If chkDireDefa.Checked Then
         For Each dr As DataRow In mdsDatos.Tables(mstrClieDire).Select("dicl_defa=1")
            With dr
               .Item("dicl_defa") = False
               .Item("_defa") = ""
            End With
         Next
      End If

      vbooDefault = (mdsDatos.Tables(mstrClieDire).Select("dicl_defa=1").GetLength(0) = 0)

      If hdnDireId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrClieDire).NewRow
         ldrDatos.Item("dicl_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrClieDire), "dicl_id")
         If mbooActi Then
            ldrDatos.Item("acti_prop") = mintActiDefa
         End If
      Else
         ldrDatos = mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & hdnDireId.Text)(0)
      End If

      With ldrDatos
         .Item("dicl_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
         .Item("dicl_dire") = txtDire.Valor
         .Item("dicl_refe") = txtDireRefe.Valor
         .Item("dicl_cpos") = txtDireCP.Valor
         .Item("_dire_desc") = txtDire.Valor
         .Item("dicl_loca_id") = cmbDireLoca.Valor
         .Item("_dicl_pcia_id") = cmbDirePcia.Valor
         .Item("_dicl_pais_id") = cmbDirePais.Valor
         .Item("dicl_defa") = vbooDefault
         .Item("_defa") = IIf(.Item("dicl_defa"), "X", "")
         .Item("dicl_gene") = 0
         .Item("acti_prop") = "3"
      End With

      If hdnDireId.Text = "" Then
         mdsDatos.Tables(mstrClieDire).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mGuardarDatosRaza(ByVal pbooAlta As Boolean)

      Dim ldrDatos As DataRow

      If cmbRaza.SelectedValue = "" Then
         Throw New AccesoBD.clsErrNeg("Debe indicar la raza.")
      End If

      If (mdsDatos.Tables(mstrCriaRaza).Select("crrz_raza_id=" & cmbRaza.Valor)).GetUpperBound(0) <> -1 Then
         Throw New AccesoBD.clsErrNeg("La raza indicada ya se encuentra asociada al criadero.")
      End If

      If hdnRazaId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrCriaRaza).NewRow
         ldrDatos.Item("crrz_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrCriaRaza), "crrz_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrCriaRaza).Select("crrz_id=" & hdnRazaId.Text)(0)
      End If

      With ldrDatos
         .Item("crrz_raza_id") = cmbRaza.Valor
         .Item("_raza_desc") = cmbRaza.SelectedItem.Text
         .Item("crrz_baja_fecha") = System.DBNull.Value
         .Item("_estado") = "Activo"
      End With

      If hdnRazaId.Text = "" Then
         mdsDatos.Tables(mstrCriaRaza).Rows.Add(ldrDatos)
      End If

   End Sub

   Private Sub mLimpiarTelefono()
      hdnTeleId.Text = ""
      txtTeleNume.Text = ""
      txtTeleArea.Text = ""
      txtTeleRefe.Text = ""
      grdActiTele.Visible = True
      cmbTeleTipo.Limpiar()
      mSetearEditor(mstrClieTele, True)
      mHabilitarTele(True)
   End Sub

   Private Sub mHabilitarTele(ByVal pbooHabi As Boolean)
      txtTeleNume.Enabled = pbooHabi
      txtTeleArea.Enabled = pbooHabi
      txtTeleRefe.Enabled = pbooHabi
      cmbTeleTipo.Enabled = pbooHabi
   End Sub

   Private Sub mHabilitarDire(ByVal pbooHabi As Boolean)
      txtDire.Enabled = pbooHabi
      txtDireCP.Enabled = pbooHabi
      txtDireRefe.Enabled = pbooHabi
      cmbDirePais.Enabled = pbooHabi
      cmbDirePcia.Enabled = pbooHabi
      cmbDireLoca.Enabled = pbooHabi
      btnLoca.Enabled = pbooHabi
      chkDireDefa.Enabled = pbooHabi
   End Sub

   Private Sub mLimpiarDireccion()
      hdnDireId.Text = ""
      txtDire.Text = ""
      txtDireCP.Text = ""
      txtDireRefe.Text = ""
      chkDireDefa.Checked = False
      grdActiDire.Visible = True
      cmbDirePais.Limpiar()
      cmbDirePcia.Items.Clear()
      cmbDireLoca.Items.Clear()
      cmbLocaAux.Items.Clear()
      mSetearEditor(mstrClieDire, True)
      mHabilitarDire(True)
   End Sub

   Private Sub mLimpiarRaza()
      hdnRazaId.Text = ""
      cmbRaza.Limpiar()
      mSetearEditor(mstrCriaRaza, True)
   End Sub

   Private Sub mLimpiarMail()
      hdnMailId.Text = ""
      txtMail.Text = ""
      txtMailRefe.Text = ""
      grdActiMail.Visible = True
      mSetearEditor(mstrClieMail, True)
      mHabilitarMail(True)
   End Sub

   Private Sub mHabilitarMail(ByVal pbooHabi As Boolean)
      txtMail.Enabled = pbooHabi
      txtMailRefe.Enabled = pbooHabi
   End Sub

#End Region

   Private Sub lnkRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkRaza.Click
      mShowTabs(5)
   End Sub

   Private Sub lnkTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkTele.Click
      mShowTabs(2)
   End Sub

   Private Sub lnkDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkDire.Click
      mShowTabs(3)
   End Sub

   Private Sub lnkMail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkMail.Click
      mShowTabs(4)
   End Sub

   Private Sub btnLimpDire_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDire.Click
      mLimpiarDireccion()
   End Sub

   Private Sub btnLimpMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpMail.Click
      mLimpiarMail()
   End Sub

   Private Sub btnLimpTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpTele.Click
      mLimpiarTelefono()
   End Sub

   Private Sub btnBajaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaTele.Click
      Try
         With mdsDatos.Tables(mstrClieTele).Select("tecl_id=" & hdnTeleId.Text)(0)
            If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("tecl_gene")) Then
               Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
            End If
            .Delete()
         End With
         grdTele.DataSource = mdsDatos.Tables(mstrClieTele)
         grdTele.DataBind()
         mLimpiarTelefono()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaRaza.Click
      Try
         With mdsDatos.Tables(mstrCriaRaza).Select("crrz_id=" & hdnRazaId.Text)(0)
            .Item("crrz_baja_fecha") = System.DateTime.Now.ToString
            .Item("_estado") = "Inactivo"
         End With
         grdRaza.DataSource = mdsDatos.Tables(mstrCriaRaza)
         grdRaza.DataBind()
         mLimpiarRaza()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDire.Click
      Try
         With mdsDatos.Tables(mstrClieDire).Select("dicl_id=" & hdnDireId.Text)(0)
            If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("dicl_gene")) Then
               Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
            End If
            .Item("dicl_baja_fecha") = System.DateTime.Now.ToString
            .Item("_estado") = "Inactivo"
         End With
         grdDire.DataSource = mdsDatos.Tables(mstrClieDire)
         grdDire.DataBind()
         mLimpiarDireccion()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaMail.Click
      Try
         With mdsDatos.Tables(mstrClieMail).Select("macl_id=" & hdnMailId.Text)(0)
            If mbooActi And (mintActiDefa.ToString <> .Item("acti_prop") Or .Item("macl_gene")) Then
               Throw New AccesoBD.clsErrNeg("Solo puede eliminar datos de los cuales es propietario.")
            End If
            .Delete()
         End With
         grdMail.DataSource = mdsDatos.Tables(mstrClieMail)
         grdMail.DataBind()
         mLimpiarMail()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaTele.Click
      mActualizarTelefono(True)
   End Sub

   Private Sub btnAltaRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaRaza.Click
      mActualizarRaza(True)
   End Sub

   Private Sub btnAltaMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaMail.Click
      mActualizarMail(True)
   End Sub

   Private Sub btnAltaDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDire.Click
      mActualizarDireccion(True)
   End Sub

   Private Sub btnModiTele_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiTele.Click
      mActualizarTelefono(False)
   End Sub

   Private Sub btnModiDire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDire.Click
      mActualizarDireccion(False)
   End Sub

   Private Sub btnModiMail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiMail.Click
      mActualizarMail(False)
   End Sub

   Private Sub btnModiRaza_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiRaza.Click
      mActualizarRaza(False)
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      grdDato.CurrentPageIndex = 0
      mValidarConsulta()
      mConsultar()
   End Sub

   Private Sub btnLoca_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoca.Click
      mLocalidades()
   End Sub

   Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      mAgregar()
   End Sub

   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function

   Protected Overrides Sub Finalize()
      MyBase.Finalize()
   End Sub

   Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
      Try
         If (hdnDatosPop.Text <> "") Then

            mstrCmd = "exec dbo.dire_clientes_consul "
            mstrCmd += hdnDatosPop.Text


            Dim ds As New DataSet
            ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

            If ds.Tables(0).Rows.Count > 0 Then
               With ds.Tables(0).Rows(0)
                  txtDire.Valor = .Item("dicl_dire")
                  txtDireCP.Valor = .Item("dicl_cpos")
                  txtDireRefe.Valor = .Item("dicl_refe")
                  cmbDirePais.Valor = .Item("_dicl_pais_id")
                  clsWeb.gCargarCombo(mstrConn, "provincias_cargar " & cmbDirePais.Valor, cmbDirePcia, "id", "descrip", "S", True)
                  cmbDirePcia.Valor = .Item("_dicl_pcia_id")
                  clsWeb.gCargarCombo(mstrConn, "localidades_cargar " & cmbDirePcia.Valor, cmbDireLoca, "id", "descrip", "S", True)
                  clsWeb.gCargarCombo(mstrConn, "localidades_aux_cargar " & cmbDirePcia.Valor, cmbLocaAux, "id", "descrip", "S", True)
                  cmbDireLoca.Valor = .Item("dicl_loca_id")
               End With
            End If
            hdnDatosPop.Text = ""

         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

   End Sub

End Class
End Namespace
