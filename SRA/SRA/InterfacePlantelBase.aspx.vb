Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.OleDb
Imports System.IO


Namespace SRA


Partial Class InterfacePlantelBase
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

	'El Dise�ador de Web Forms requiere esta llamada.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents lblProv As System.Web.UI.WebControls.Label
	Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
	Protected WithEvents cmbActiProp As NixorControls.ComboBox
	Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
	Protected WithEvents grdConsulta As System.Web.UI.WebControls.DataGrid
	
	'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
	'No se debe eliminar o mover.

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
		'No la modifique con el editor de c�digo.
		InitializeComponent()
	End Sub

#End Region

#Region "Definici�n de Variables"

	Private mstrTabla As String = SRA_Neg.Constantes.gTab_Plantel_Base
	Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_Plantel_Base_Detalle
	Private mstrTablaPRDT As String = SRA_Neg.Constantes.gTab_Productos
	Private mstrCmd As String
	Private mstrConn As String
	Private mstrArchivoOri As String
	Private lstrServDetaId As String
	Private lstrServId As String

#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Dim lstrFil As String
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			If (Not Page.IsPostBack) Then
				btnImpo.Attributes.Add("onclick", "if(confirm('Desea iniciar el proceso de importaci�n de datos?')){}else {return false} ")
				mInicializar()
				mCargarCombos()
				mSetearEventos()
			End If
			
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

#End Region

	Protected Overrides Sub Finalize()
		MyBase.Finalize()
	End Sub

	Private Sub mSetearEventos()
		btnImpo.Attributes.Add("onclick", "return(mImportar());")
	End Sub
	Private Sub mCargarCombos()
		clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
		clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar", cmbAsoc, "id", "descrip", "S")

	End Sub

	Public Sub mInicializar()
		txtFecha.Fecha = Date.Now
	End Sub
	Private Sub mLimpiar()
		cmbAsoc.Limpiar()
		cmbRaza.Limpiar()
		txtFechaPresen.Text = ""
		txtFecha.Fecha = Date.Now
	End Sub

	Private Sub mImportar()

		If (filArch.Value = "") Then
			lblMens.Text = "Error: " & "Debe indicar el archivo a importar."
			Return
		End If
		If (txtFechaPresen.Text = "") Then
			lblMens.Text = "Error: " & "Debe indicar la Fecha de Presentaci�n."
			Return
		End If
		If (cmbRaza.SelectedValue.ToString = "") Then
			lblMens.Text = "Error: " & "Debe indicar la Raza."
			Return
		End If

		Try

			lisResu.Items.Clear()

			Dim lstrFilename As String = filArch.Value
			Dim lstrTempPath As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_temp_path")
			Dim lstrArchNomb As String = lstrFilename.Substring(lstrFilename.LastIndexOf("\") + 1)
			mstrArchivoOri = lstrArchNomb.ToString()

			Dim lstrExte As String = IIf(cmbTipo.Valor.ToString = "DBF", "dbf", "xls")
			lstrArchNomb = lstrArchNomb.Substring(0, lstrArchNomb.LastIndexOf(".") + 1) & lstrExte
			Dim lstrArchTemp As String = Server.MapPath("") & "\" & lstrTempPath & "\" & lstrArchNomb

			If Not (filArch.PostedFile Is Nothing) Then
				If Not System.IO.Directory.Exists(Server.MapPath("") & "\" & lstrTempPath) Then
					System.IO.Directory.CreateDirectory(Server.MapPath("") & "\" & lstrTempPath)
				End If
				filArch.PostedFile.SaveAs(lstrArchTemp)
			End If

			mProcesarArchivo(lstrArchTemp)

			File.Delete(lstrArchTemp)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try

	End Sub

	Protected Sub mMostrarMensaje(ByVal pstrError As String, ByVal pintRegi As Integer)
		Dim lstrMsg As String = ""
		If (pintRegi <> -1) Then
			lstrMsg = lstrMsg & "Registro #" & Convert.ToString(pintRegi + 1) & ": "
		End If
		lstrMsg = lstrMsg & pstrError
		lisResu.Items.Insert(0, lstrMsg)
	End Sub

	Private Function mProcesarArchivoCabe(ByVal pTrans As SqlTransaction, ByVal pmyConn As SqlConnection, ByVal pstrArchi As String, ByVal pstrFechaRecep As String, ByVal pstrTipo As String) As String

		Dim lstrCmd As String = ""
		Dim lstrId As String = ""
		'esto es una prueba para ver si trago bien la version del SourSafe
		lstrCmd = "exec rg_importaciones_alta"
		lstrCmd = lstrCmd + " '" + pstrArchi + "'"
		lstrCmd = lstrCmd + "," + clsSQLServer.gFormatArg(pstrFechaRecep, SqlDbType.SmallDateTime)
		lstrCmd = lstrCmd + "," + clsSQLServer.gFormatArg(Session("sUserId").ToString(), SqlDbType.Int)
		lstrCmd = lstrCmd + ",'" + pstrTipo + "'"
		lstrId = clsSQLServer.gExecuteScalarTransParam(pTrans, pmyConn, lstrCmd)
		Return (lstrId)

	End Function


	Protected Sub mProcesarArchivo(ByVal pstrFile As String)
		Dim lTransac As SqlClient.SqlTransaction
		Dim myConnection As New SqlConnection(mstrConn)
		Try
			Dim lstrId As String
			Dim lstrIdArch As String
			Dim lstrIdDeta As String
			Dim lstrCriaID As String
			Dim lstrPRDTId As String
			Dim lstrRegtID As String
			Dim lstrFecha As String
			Dim lstrMsgError As String
			Dim lstrRaza As String
			Dim lstrProp As String
			Dim lstrNoveTipo As String
			Dim lintCantRegi As Integer = 0
			Dim lintCantIgno As Integer = 0
			Dim lintCantError As Integer = 0
			Dim ldsDatos As New DataSet
			Dim ldsTemp As New DataSet
			Dim lstrCriaNume As String = ""
			Dim i As Integer

			Dim lstrPath As String = pstrFile.Substring(0, pstrFile.LastIndexOf("\"))
			Dim lstrArch As String = pstrFile.Substring(pstrFile.LastIndexOf("\") + 1)

			'Abro la conexion y Transacion
			myConnection.Open()
			lTransac = myConnection.BeginTransaction()


			ldsTemp = clsImporta.clsAbrirArchivo.mImportarExcelAsoc(pstrFile, "*", "", "HEMBRAS")
			ldsDatos.Tables.Add(ldsTemp.Tables(0).Copy())
			ldsTemp.Clear()
			ldsTemp = clsImporta.clsAbrirArchivo.mImportarExcelAsoc(pstrFile, "*", "", "MACHOS")
			ldsDatos.Tables.Add(ldsTemp.Tables(0).Copy())
			ldsTemp.Clear()

			lisResu.Items.Clear()

			'Doy de alta en la tabla rg_importaciones con tipo de Archivo "P" = Plantel Base
			lstrIdArch = mProcesarArchivoCabe(lTransac, myConnection, mstrArchivoOri, txtFechaPresen.Fecha, "P")

			'mMostrarMensaje("Iniciando proceso de importaci�n...", -1)

			For i = 0 To ldsDatos.Tables.Count - 1
				ldsDatos.Tables(i).Columns(1).ColumnName = "criador"
				'ldt.Select("", "criador asc")
				For Each odrDato As DataRow In ldsDatos.Tables(i).Select("", "criador asc")

					lintCantRegi = lintCantRegi + 1					'Cuento los registros

					If Not odrDato.Item(1).ToString = "" Then

						lstrRegtID = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_id", "@regt_raza_id=" & cmbRaza.SelectedValue.ToString & ",@regt_desc='" & Trim(odrDato.Item(5).ToString) & "'")

						If Trim(odrDato.Item(1).ToString) <> lstrCriaNume Then
							lstrCriaNume = Trim(odrDato.Item(1).ToString)

							lstrCriaID = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "criadores", "cria_id", "@cria_raza_id=" & cmbRaza.SelectedValue.ToString & ",@cria_nume=" & lstrCriaNume)
							'doy de alta la cabecera
							mstrCmd = mObtenerParamABMCabe(mstrTabla & "_alta", lstrIdArch, lstrCriaID)
							lstrId = clsSQLServer.gExecuteScalarTransParam(lTransac, myConnection, mstrCmd)

						End If
						mstrCmd = ""
						mstrCmd = mObtenerParamABMDeta(mstrTablaDeta & "_alta", odrDato, lstrId, lstrRegtID)
						lstrIdDeta = clsSQLServer.gExecuteScalarTransParam(lTransac, myConnection, mstrCmd)

						If lstrIdDeta <> "0" Then

							mstrCmd = ""
							mstrCmd = mbtenerParamABM_PRDT(mstrTablaDeta & "_" & mstrTablaPRDT & "_alta", odrDato, lstrIdDeta, lstrCriaID, lstrRegtID)
							lstrPRDTId = clsSQLServer.gExecuteScalarTransParam(lTransac, myConnection, mstrCmd)
							lstrIdDeta = ""

						Else
							lstrMsgError = ""
							lstrMsgError = odrDato.Item(0).ToString & " - "
							lstrMsgError = odrDato.Item(1).ToString & " - "
							lstrMsgError = odrDato.Item(2).ToString & " - "
							lstrMsgError = odrDato.Item(3).ToString & " - "
							lstrMsgError = odrDato.Item(4).ToString & " - "
							lstrMsgError = odrDato.Item(5).ToString & " -- Resultado: No se Grab�"
							mMostrarMensaje("Registro Duplicado: " & lstrMsgError, -1)
							lintCantIgno = lintCantIgno + 1
						End If
					Else
						lstrMsgError = ""
						lstrMsgError = IIf(odrDato.Item(0).ToString = "", "error", odrDato.Item(0).ToString) & " - "
						lstrMsgError = lstrMsgError & IIf(odrDato.Item(1).ToString = "", "error", odrDato.Item(1).ToString) & " - "
						lstrMsgError = lstrMsgError & IIf(odrDato.Item(2).ToString = "", "error", odrDato.Item(2).ToString) & " - "
						lstrMsgError = lstrMsgError & IIf(odrDato.Item(3).ToString = "", "error", odrDato.Item(3).ToString) & " - "
						lstrMsgError = lstrMsgError & IIf(odrDato.Item(4).ToString = "", "error", odrDato.Item(4).ToString) & " - "
						lstrMsgError = lstrMsgError & IIf(odrDato.Item(5).ToString = "", "error", odrDato.Item(5).ToString) & " -- Resultado: No se Grab�"
						mMostrarMensaje("Registro Erroneo: " & lstrMsgError, -1)
						lintCantError = lintCantError + 1						 'cuento los registros Ignorados
					End If

				Next
			Next

			lTransac.Commit()
			myConnection.Close()

			mMostrarMensaje("Registros Erroneos: " + lintCantError.ToString(), -1)
			mMostrarMensaje("Registros Duplicados: " + lintCantIgno.ToString(), -1)
			mMostrarMensaje("Registros procesados: " + lintCantRegi.ToString(), -1)
			mMostrarMensaje("Se complet� el proceso de importaci�n del archivo: " + mstrArchivoOri, -1)

		Catch ex As Exception
			lTransac.Rollback()
			myConnection.Close()
			clsError.gManejarError(Me, ex)
		End Try

	End Sub

	Private Function mObtenerParamABMCabe(ByVal pstrProcedure As String, ByVal pstrIDarchivo As String, ByVal pstrCriaID As String) As String
		Dim lstrCmd As String

		lstrCmd = "exec " + pstrProcedure

		lstrCmd += " @plba_alta_fecha=" & clsSQLServer.gFormatArg(txtFecha.Text, SqlDbType.SmallDateTime)
		lstrCmd += ",@plba_fecha_presen=" & clsSQLServer.gFormatArg(txtFechaPresen.Text, SqlDbType.SmallDateTime)
		lstrCmd += ",@plba_raza_id=" & clsSQLServer.gFormatArg(cmbRaza.SelectedValue.ToString, SqlDbType.Int)
		lstrCmd += ",@plba_cria_id=" & clsSQLServer.gFormatArg(pstrCriaID, SqlDbType.Int)
		lstrCmd += ",@plba_asoc_id=" & clsSQLServer.gFormatArg(cmbAsoc.SelectedValue.ToString, SqlDbType.Int)
		lstrCmd += ",@plba_audi_user=" & Session("sUserId").ToString()
		lstrCmd += ",@plba_tipo='I'"
		lstrCmd += ",@plba_impo_id=" & pstrIDarchivo

		Return (lstrCmd)
	End Function

	Function mbtenerParamABM_PRDT(ByVal pstrProcedure As String, ByVal plDr As DataRow, ByVal pDetaID As String, ByVal pCriaID As String, ByVal pRegtID As String) As String
		Dim lstrCmd As String

		lstrCmd = "exec " + pstrProcedure
		lstrCmd += " @pbde_id=" & pDetaID
		lstrCmd += ",@pbde_presen_fecha=" & clsSQLServer.gFormatArg(txtFechaPresen.Text, SqlDbType.SmallDateTime)
		lstrCmd += ",@pbde_raza_id=" & clsSQLServer.gFormatArg(cmbRaza.SelectedValue.ToString, SqlDbType.Int)
		lstrCmd += ",@pbde_cria_id=" & clsSQLServer.gFormatArg(pCriaID, SqlDbType.Int)
		If cmbAsoc.SelectedValue.ToString = "" Then
			lstrCmd += ",@pbde_asoc_id=null"
		Else
			lstrCmd += ",@pbde_asoc_id=" & clsSQLServer.gFormatArg(cmbAsoc.SelectedValue.ToString, SqlDbType.Int)
		End If
		With plDr
			If Trim(.Item(0).ToString) = "H" Then
				lstrCmd += ",@pbde_sexo=0"
			ElseIf Trim(.Item(0).ToString) = "M" Then
				lstrCmd += ",@pbde_sexo=1"
			End If
			'lstrRegtID = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_id", "@regt_raza_id=" & cmbRaza.SelectedValue.ToString & ",@regt_desc='" & Trim(.Item(5).ToString) & "'")
			If pRegtID = "" Then
				lstrCmd += ",@pbde_regt_id=null"
			Else
				lstrCmd += ",@pbde_regt_id=" & pRegtID
			End If
			lstrCmd += ",@pbde_px=null"
			lstrCmd += ",@pbde_rp='" & Trim(.Item(2).ToString) & "'"
			lstrCmd += ",@pbde_rp_nume=" & mDeterminarNumero(Trim(.Item(2).ToString))
			lstrCmd += ",@pbde_pref_id=null"
			lstrCmd += ",@pbde_nomb=null"
			lstrCmd += ",@pbde_naci_fecha=null"
            lstrCmd += ",@pbde_asoc_nume_lab=" & mDeterminarNumero(Trim(.Item(4).ToString))
            'todo: corregir pbde_asoc_nume
            'lstrCmd += ",@pbde_asoc_nume=" & mDeterminarNumero(Trim(.Item(4).ToString))
			lstrCmd += ",@pbde_pela_id=null"
			lstrCmd += ",@pbde_vari_id=null"
			lstrCmd += ",@pbde_padre_id=null"
			lstrCmd += ",@pbde_madre_id=null"
			lstrCmd += ",@pbde_audi_user=" & Session("sUserId").ToString()
		End With

		Return (lstrCmd)
	End Function

	Private Function mDeterminarNumero(ByVal pstrRP As String) As Integer
		Dim lstrRp As String
		Dim lstrChar As String

		If pstrRP <> "" Then
			Dim i As Integer
			For i = 0 To pstrRP.Length - 1
				lstrChar = pstrRP.Substring(i, 1)
				If IsNumeric(lstrChar) Then lstrRp = lstrRp + lstrChar
			Next
		End If

		Return (lstrRp)
	End Function

    Function mObtenerParamBusqDeta(ByVal plDr As DataRow, ByVal pRegtID As String) As String
        Dim lstrCmd As String
        Dim lstrNumero As String

        lstrCmd = ""

        With plDr
            If Trim(.Item(0).ToString) = "H" Then
                lstrCmd += "@pbde_sexo=0"
            ElseIf Trim(.Item(0).ToString) = "M" Then
                lstrCmd += "@pbde_sexo=1"
            Else
                lstrCmd += "@pbde_sexo=null"
            End If
            'lstrRegtID = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_id", "@regt_raza_id=" & cmbRaza.SelectedValue.ToString & ",@regt_desc='" & Trim(.Item(5).ToString) & "'")
            If pRegtID = "" Then
                lstrCmd += ",@pbde_regt_id=null"
            Else
                lstrCmd += ",@pbde_regt_id=" & pRegtID
            End If
            lstrCmd += ",@pbde_rp='" & Trim(.Item(2).ToString) & "'"
            lstrNumero = mDeterminarNumero(Trim(.Item(2).ToString))
            lstrCmd += ",@pbde_rp_nume=" & IIf(lstrNumero = "", "null", lstrNumero)
            lstrNumero = mDeterminarNumero(Trim(.Item(4).ToString))
            lstrCmd += ",@pbde_asoc_nume_lab=" & IIf(lstrNumero = "", "null", lstrNumero)
            '  pbde_asoc_nume
            'TODO: GSZ - agregar pbde_asoc_nume
        End With
        Return (lstrCmd)
    End Function


    Function mObtenerParamABMDeta(ByVal pstrProcedure As String, ByVal plDr As DataRow, ByVal pstrCabeID As String, ByVal pRegtID As String) As String
        Dim lstrCmd As String
        Dim lstrNumero As String

        lstrCmd = "exec " + pstrProcedure

        With plDr
            lstrCmd += " @pbde_plba_id=" & clsSQLServer.gFormatArg(pstrCabeID, SqlDbType.Int)
            If Trim(.Item(0).ToString) = "H" Then
                lstrCmd += ",@pbde_sexo=0"
            ElseIf Trim(.Item(0).ToString) = "M" Then
                lstrCmd += ",@pbde_sexo=1"
            Else
                lstrCmd += "@pbde_sexo=null"
            End If

            'lstrRegtID = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "rg_registros_tipos", "regt_id", "@regt_raza_id=" & cmbRaza.SelectedValue.ToString & ",@regt_desc='" & Trim(.Item(5).ToString) & "'")
            If pRegtID = "" Then
                lstrCmd += ",@pbde_regt_id=null"
            Else
                lstrCmd += ",@pbde_regt_id=" & pRegtID
            End If
            lstrCmd += ",@pbde_px_id=null"
            lstrCmd += ",@pbde_rp='" & Trim(.Item(2).ToString) & "'"
            lstrNumero = mDeterminarNumero(Trim(.Item(2).ToString))
            lstrCmd += ",@pbde_rp_nume=" & IIf(lstrNumero = "", "null", lstrNumero)
            lstrCmd += ",@pbde_pref_id=null"
            lstrCmd += ",@pbde_nomb=null"
            lstrCmd += ",@pbde_naci_fecha=null"
            lstrNumero = mDeterminarNumero(Trim(.Item(4).ToString))
            lstrCmd += ",@pbde_asoc_nume=" & IIf(lstrNumero = "", "null", lstrNumero)
            lstrCmd += ",@pbde_pela_id=null"
            lstrCmd += ",@pbde_vari_id=null"
            lstrCmd += ",@pbde_prdt_id=null"
            lstrCmd += ",@pbde_padre_id=null"
            lstrCmd += ",@pbde_madre_id=null"
            lstrCmd += ",@pbde_audi_user=" & Session("sUserId").ToString()

        End With
        Return (lstrCmd)
    End Function

    Private Sub btnImpo_Click1(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImpo.Click
        mImportar()
        mLimpiar()
    End Sub

End Class

End Namespace
