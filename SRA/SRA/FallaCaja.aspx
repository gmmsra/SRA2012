<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.FallaCaja" CodeFile="FallaCaja.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Falla de Caja</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<SCRIPT language="javascript">
		function mSetearConcepto(pConc)
		{
			var strRet = LeerCamposXML("conceptos", "@conc_id="+pConc.value, "_cuenta");
			if (strRet!='')
			{ 
				var lstrCta = strRet.substring(0,1);
				document.all('hdnCta').value = lstrCta;
				if (lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
				{
					document.all('cmbCcos').disabled = true;
					document.all('txtcmbCcos').disabled = true;
					document.all('cmbCcos').innerHTML = '';
					document.all('txtcmbCcos').value = '';
				}
				else
				{
					document.all('cmbCcos').disabled = false;
					document.all('txtcmbCcos').disabled = false;
					LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, "cmbCcos", "N");					
				}
			}			
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Fallas de Caja</asp:label></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" Width="97%"
										BorderWidth="0" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																	ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																	ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif"
																	ImageOver="btnLimp2.gif" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																			<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;
																			<asp:Label id="lblFHastaHastaFil" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																			<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg"></TD>
																		<TD align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblEmctFil" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbEmctFil" runat="server" Width="200px" AceptaNull="False"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																		<TD background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																	</TR>
																	<TR>
																		<TD background="imagenes/formfdofields.jpg"></TD>
																		<TD align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblMonedaFil" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
																		<TD background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbMonedaFil" runat="server" cssclass="cuadrotexto" Width="90px"
																				Obligatorio="True"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="fall_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="fall_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_emct_desc" HeaderText="C.Emi."></asp:BoundColumn>
											<asp:BoundColumn DataField="_mone_desc" HeaderText="Moneda"></asp:BoundColumn>
											<asp:BoundColumn DataField="fall_impo" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
												HeaderText="Importe" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle" width="100%">&nbsp;
									<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
										ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ToolTip="Generar Nueva Falla"
										ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
								<TD align="right"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
										ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
										ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BOTONIMAGEN></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD height="5">
													<asp:Label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
												<TD vAlign="top" align="right">&nbsp;
													<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="2">
													<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
														<TR>
															<TD align="right" width="100">
																<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblEmct" runat="server" cssclass="titulo">Centro Emisor:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:combobox class="combo" id="cmbEmct" runat="server" Width="200px"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblMone" runat="server" cssclass="titulo">Moneda:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:combobox class="combo" id="cmbMone" runat="server" cssclass="cuadrotexto" Width="90px" Obligatorio="True"></cc1:combobox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblImpo" runat="server" cssclass="titulo">Importe:</asp:Label>&nbsp;</TD>
															<TD>
																<cc1:numberbox id="txtImpo" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="True"
																	EsDecimal="true"></cc1:numberbox></TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblConc" runat="server" cssclass="titulo">Concepto:</asp:Label>&nbsp;</TD>
															<TD colSpan="3">
																<table border=0 cellpadding=0 cellspacing=0>
																	<tr>
																	<td>															
																	<cc1:combobox class="combo" id="cmbConc" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="True"
																	TextMaxLength="5" MostrarBotones="False" filtra="true" NomOper="cuentas_cargar" onchange="javascript:mSetearConcepto(this);"></cc1:combobox>
																	</td>
																	<td>
																		<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																										onclick="mBotonBusquedaAvanzada('conceptos','conc_desc','cmbConc','Conceptos','@conc_acti_id=1');"
																										alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</td>
																	</tr>
																	</table>
														</TD>
														</TR>
														<TR>
															<TD align="right">
																<asp:Label id="lblCcos" runat="server" cssclass="titulo">Ctro. Costo:</asp:Label>&nbsp;</TD>
															<TD colSpan="3">
																<table border=0 cellpadding=0 cellspacing=0>
																	<tr>
																	<td>															
																	<cc1:combobox class="combo" id="cmbCcos" runat="server" cssclass="cuadrotexto" Width="300px" Obligatorio="false"
																	TextMaxLength="6" MostrarBotones="False" filtra="true" NomOper="centrosc_cuenta_cargar"></cc1:combobox>
																	</td>
																	<td>
																		<IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																										onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCcos','Centros de Costo','@cuenta='+document.all('hdnCta').value);"
																										alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																	</td>
																	</tr>
																	</table>
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel><ASP:PANEL id="panBotones" Runat="server">
										<TABLE width="100%">
											<TR>
												<TD align="center">
													<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
											</TR>
											<TR height="30">
												<TD align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
													<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Baja"></asp:Button>&nbsp;
													<asp:Button id="btnModi" runat="server" cssclass="boton" Visible="False" Width="80px" CausesValidation="False"
														Text="Modificar"></asp:Button>
													<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Limpiar"></asp:Button></TD>
											</TR>
										</TABLE>
									</ASP:PANEL></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
			<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			<ASP:TEXTBOX id="hdnCta" runat="server"></ASP:TEXTBOX>
			<asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnEmctId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtImpo"]!= null&&!document.all["txtImpo"].disabled)
			document.all["txtImpo"].focus();
		if (document.all["cmbCcos"]!= null)
		{
			document.all('cmbCcos').disabled = (document.all('cmbCcos').options.length <= 1);
			document.all('txtcmbCcos').disabled = (document.all('cmbCcos').options.length <= 1);
		}
		</SCRIPT>
	</BODY>
</HTML>
