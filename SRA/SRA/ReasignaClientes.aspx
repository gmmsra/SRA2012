<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ReasignaClientes" CodeFile="ReasignaClientes.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Reasignación de Clientes</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px" vAlign="bottom" colSpan="3" height="33"><asp:label id="lblTitulo" runat="server" width="391px" cssclass="opcion">Reasignación de Clientes</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD vAlign="top" colSpan="3"><asp:panel id="Panel1" runat="server" cssclass="titulo" visible="True" BorderWidth="0" BorderStyle="Solid"
											Width="100%">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																		ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																		ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																		ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif"
																		ImageOver="btnLimp2.gif" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 161px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblFechaDesde" runat="server" cssclass="titulo">Período Desde:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 85%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																				<cc1:DateBox id="txtFechaDesde" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;
																				<asp:Label id="lblFechaHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;
																				<cc1:DateBox id="txtFechaHasta" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 161px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="lblClieAnteFil" runat="server" cssclass="titulo">Cliente Anterior:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 85%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																				<cc1:numberbox id="txtClieAnteFil" runat="server" cssclass="cuadrotexto" Width="128px" esdecimal="False"
																					MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD noWrap align="right" background="imagenes/formfdofields.jpg">&nbsp;
																				<asp:Label id="lblClieNuevoFil" runat="server" cssclass="titulo">Cliente Reemplazante:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 85%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrClieNuevoFil" runat="server" Tabla="Clientes" Saltos="1,1" FilSociNume="True"
																					MuestraDesc="true" FilDocuNume="True" Ancho="800" AceptaNull="false"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR> <!-- Fin Filtro -->
								<TR>
									<TD align="center" colSpan="7">
										<DIV><asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" Height="116px">
												<TABLE id="Table2" style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
													<TR>
														<TD vAlign="top" width="100%" colSpan="3"></TD>
													</TR> <!-- Fin Filtro -->
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panApod" runat="server" cssclass="titulo" Width="100%" Visible="True">
																<TABLE id="TableDetalle" cellPadding="0" width="100%" align="left" border="0">
																	<TR>
																		<TD align="left" width="100%" colSpan="3">
																			<asp:datagrid id="grdReas" runat="server" width="100%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdReas_PageChanged"
																				OnEditCommand="mEditarDatos" AutoGenerateColumns="False">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="clre_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="clre_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
																					<asp:BoundColumn DataField="clre_ante_clie_id" HeaderText="Cliente Anterior"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_cliente" HeaderText="Cliente Reemplazante"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_usuario" HeaderText="Usuario"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="4">&nbsp;&nbsp;
																			<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																				<TR>
																					<TD><A id="editar" name="editar"></A>
																						<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																							ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
																							ImageOver="btnNuev2.gif" ForeColor="Transparent" ToolTip="Nuevo Registro" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
																					<TD align="right">
																						<CC1:BOTONIMAGEN id="btnListar" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																							ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif"
																							ImageOver="btnImpr2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Listar"></CC1:BOTONIMAGEN></TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="4">
																			<asp:panel id="PanDetalle" runat="server" cssclass="titulo" width="100%" BorderStyle="Solid"
																				BorderWidth="1px">
																				<TABLE class="FdoFld" id="TableaPnDetalle" style="WIDTH: 100%" cellPadding="0" align="left"
																					border="0">
																					<TR>
																						<TD style="WIDTH: 132px" align="left"></TD>
																						<TD align="right" height="5">
																							<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 132px" vAlign="top" align="right">
																							<asp:Label id="lblClieAnte" runat="server" cssclass="titulo" Visible="True">Cliente a Eliminar:</asp:Label>&nbsp;
																						</TD>
																						<TD vAlign="bottom" height="5">
																							<asp:Label id="txtClieAnte" runat="server" cssclass="titulo" Visible="True"></asp:Label>
																							<UC1:CLIE id="usrClieAnte" runat="server" Tabla="Clientes" Saltos="1,1" FilSociNume="True"
																								MuestraDesc="true" FilDocuNume="True" Ancho="800" AceptaNull="false"></UC1:CLIE></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 132px" noWrap align="right">
																							<asp:Label id="lblClieNuevo" runat="server" cssclass="titulo" Visible="True">Reemplazar por: </asp:Label>&nbsp;</TD>
																						<TD height="5">
																							<UC1:CLIE id="usrClieNuevo" runat="server" Tabla="Clientes" Saltos="1,1" FilSociNume="True"
																								MuestraDesc="true" FilDocuNume="True" Ancho="800" AceptaNull="false"></UC1:CLIE></TD>
																					</TR>
																					<TR>
																						<TD style="WIDTH: 132px" noWrap align="right">
																							<asp:Label id="lblUsua" runat="server" cssclass="titulo" Visible="True">Usuario:</asp:Label>&nbsp;</TD>
																						<TD height="5">
																							<asp:Label id="txtUsua" runat="server" cssclass="titulo" Visible="True"></asp:Label>&nbsp;</TD>
																					</TR>
																					<TR>
																						<TD vAlign="bottom" colSpan="2" height="5"></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="center" colSpan="2" height="30">
																							<asp:Button id="btnGrab" runat="server" cssclass="boton" Width="90px" Text="Reasignar"></asp:Button>&nbsp; 
																							&nbsp;<BUTTON class="boton" id="btnAutoUsua" style="WIDTH: 90px" onclick="btnAutoUsua_click();"
																								type="button" runat="server" value="Detalles">Autorizante</BUTTON>
																						</TD>
																					</TR>
																				</TABLE>
																			</asp:panel></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server"></ASP:PANEL></DIV>
									</TD>
								</TR>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Visible="True"></ASP:TEXTBOX><asp:textbox id="hdnReasId" runat="server" Visible="True"></asp:textbox>
				<asp:textbox id="hdnLoginPop" runat="server" onchange="hdnLoginPop_change();"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">	
		function hdnLoginPop_change()
		{
			document.all("txtUsua").innerText = LeerCamposXML("usuarios", "0" + document.all("hdnLoginPop").value, "_nombre");
		}		
		function btnAutoUsua_click()
		{
			gAbrirVentanas("Login_pop.aspx", 3, "200","110");
		}
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
		<DIV></DIV>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
