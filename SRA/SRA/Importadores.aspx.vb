Imports System.Data.SqlClient


Namespace SRA


Partial Class Importadores
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
    Protected WithEvents cmbActiProp As NixorControls.ComboBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label
    Protected WithEvents txtCodiDesdeFil As NixorControls.DateBox
    Protected WithEvents txtCodiHastaFil As NixorControls.DateBox

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Importadores
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mdsDatos As DataSet
    Private mstrConn As String

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lstrFil As String
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            If (Not Page.IsPostBack) Then

                Session(mSess(mstrTabla)) = Nothing

                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()
                mMostrarPanel(False)

                mConsultar()
            Else
                If panDato.Visible Then
                    mdsDatos = Session(mSess(mstrTabla))
                    Dim x As String = Session.SessionID
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "paises", cmbDefaDirePais, "")
        clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDefaDirePcia, "S", "0" + cmbDefaDirePais.Valor.ToString)

    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mSetearMaxLength()

        Dim lstrLong As Object

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtCodi.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtDeno.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "impr_deno")
        txtDire.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "impr_dire")
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "impr_obse")

    End Sub

#End Region

#Region "Inicializacion de Variables"

    Public Sub mInicializar()
        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As String

            lstrCmd = "exec " & mstrTabla & "_consul"

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)

            grdDato.Visible = True

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mCargarDatos(E.Item.Cells(1).Text)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mCargarDatos(ByVal pstrId As String)

        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(pstrId)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                hdnId.Text = .Item("impr_id")
                txtCodi.Valor = .Item("impr_codi")
                txtObse.Valor = .Item("impr_obse")
                txtDire.Valor = .Item("impr_dire")
                txtDeno.Valor = .Item("impr_deno")
                txtDefaDireCP.Valor = .Item("impr_cpos")
                txtDefaDireCP.ValidarPaisDefault = IIf(cmbDefaDirePais.Valor = Session("sPaisDefaId"), "S", "N")
                cmbDefaDirePais.Valor = .Item("impr_pais_id")
                clsWeb.gCargarRefeCmb(mstrConn, "provincias", cmbDefaDirePcia, "S", cmbDefaDirePais.Valor.ToString)
                cmbDefaDirePcia.Valor = .Item("impr_pcia_id")
                clsWeb.gCargarRefeCmb(mstrConn, "localidades", cmbDefaDireLoca, "S", "0" + cmbDefaDireLoca.Valor.ToString)
                cmbDefaDireLoca.Valor = .Item("impr_loca_id")

                If Not .IsNull("impr_baja_fecha") Then
                    lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("impr_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBaja.Text = ""
                End If

            End With

            mSetearEditor("", False)
            mMostrarPanel(True)

        End If
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiar()

        hdnId.Text = ""
        lblBaja.Text = ""
        cmbDefaDirePais.Limpiar()
        txtCodi.Text = ""
        txtDeno.Text = ""
        txtObse.Text = ""
        txtDire.Text = ""
        hdnLocaPop.Text = ""
        cmbDefaDirePcia.Limpiar()
        cmbDefaDireLoca.Limpiar()
        txtDefaDireCP.Valor = ""

        mCrearDataSet("")

        lblTitu.Text = ""

        mSetearEditor("", True)

    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible
    End Sub

#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim lstrImpoId As String

            mGuardarDatos()
            Dim lobjCtrl As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lstrImpoId = lobjCtrl.Alta()

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try

            mGuardarDatos()
            Dim lobjCtrl As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjCtrl.Modi()

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjCtrl As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjCtrl.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mGuardarDatos() As DataSet

        Dim lintServCanti As Integer

        mValidarDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("impr_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("impr_codi") = txtCodi.Valor.ToString
                .Item("impr_dire") = txtDire.Valor.ToString
                .Item("impr_deno") = txtDeno.Valor.ToString
                .Item("impr_obse") = txtObse.Valor.ToString
                .Item("impr_pais_id") = cmbDefaDirePais.Valor
                If cmbDefaDirePcia.Valor.Length > 0 Then
                    .Item("impr_pcia_id") = cmbDefaDirePcia.Valor
                End If
                If cmbDefaDireLoca.Valor.Length > 0 Then
                    .Item("impr_loca_id") = cmbDefaDireLoca.Valor
                End If
                If cmbDefaDireLoca.Valor.Length > 0 Then
                    .Item("impr_cpos") = txtDefaDireCP.Valor
                End If
            End With

        Return mdsDatos

    End Function

    Public Sub mCrearDataSet(ByVal pstrId As String)

        Dim lstrOpci As String

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        Session(mSess(mstrTabla)) = mdsDatos

    End Sub

    Private Function mSess(ByVal pstrTabla As String) As String
        If hdnSess.Text = "" Then
            hdnSess.Text = pstrTabla & Now.Millisecond.ToString
        End If
        Return (hdnSess.Text)
    End Function

#End Region

#Region "Eventos de Controles"

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

#End Region

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub hdnLocaPop_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hdnLocaPop.TextChanged
        Try
            If (hdnLocaPop.Text <> "") Then
                cmbDefaDireLoca.Valor = hdnLocaPop.Text
                txtDefaDireCP.Valor = clsSQLServer.gObtenerValorCampo(mstrConn, "localidades", cmbDefaDireLoca.Valor.ToString, "loca_cpos").ToString
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnDefaLoca_ServerClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDefaLoca.ServerClick

    End Sub
End Class
End Namespace
