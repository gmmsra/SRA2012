<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Migrated_Criadores" CodeFile="Criadores.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Expedientes</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultpilontScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		var gstrCria = '';
		var vVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null,null);
		
		function btnEstab_click() 
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=ESTABLECIMIENTOS&tabla=establecimientos_expedientes&filtros=" + document.all("hdnId").value, 1, "600","300");
		}
		function btnOtros_click()
		{
			gAbrirVentanas("Alertas_pop.aspx?titulo=Otros Datos&amp;origen=O&amp;clieId=" + document.all("hdnClieId").value+"&amp;", 2, "600", "300"); 
		}
		function btnPrefijos_click()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=PREFIJOS&tabla=prefijos&filtros=" + document.all("hdnId").value, 3, "600", "300");
		}
		function btnComisiones_click()
		{
			gAbrirVentanas("comisiones_pop.aspx??EsConsul=1&titulo=COMISIONES&cria_id=" + document.all("hdnId").value, 4, "600", "300");  	
		}
		function btnTramites_click()
		{
			gAbrirVentanas("consulta_Tramites_pop.aspx?pTipo=0&prdt_cria_id=" + document.all("hdnId").value, 4, "600", "300");  	
		}
		
		function mVerStockSemen()
		{
			var lstrClieId = document.all("hdnClieId").value;
			var lstrClieNomb = document.all('usrClie_txtApel').value;
			var lstrRazaId = document.all('cmbRaza').value;
		
			gAbrirVentanas("Reportes/SaldoSemenStock.aspx?criadorID=" + lstrClieId + "&criadorNomb=" + lstrClieNomb +"&RazaId=" + lstrRazaId, 1, "1050","650","20","40");
		}
		
		function mVerStockEmbriones()
		{
			var lstrClieId = document.all("hdnClieId").value;
			var lstrClieNomb = document.all('usrClie_txtApel').value;
			gAbrirVentanas("Reportes/SaldoEmbrionesStock.aspx?criadorID=" + lstrClieId + "&criadorNomb=" + lstrClieNomb, 1, "1050","650","20","40");
		}
		
		function btnServi_click()
		{
			gAbrirVentanas("ConsultaFiltros.aspx?EsConsul=1&titulo=SERVICIOS&tabla=productos_servicios&filtros=null," + document.all("hdnId").value, 2, "600","400","100","50");
		}
		function btnNaci_click()
		{
			gAbrirVentanas("consulta_pop.aspx?EsConsul=1&titulo=NACIMIENTOS&tabla=productos_inscrip&filtros=" + document.all("hdnId").value, 3, "600", "300");
		}
		
		function usrClie_onchange()
		{
			var lstrClie = document.all("usrClie:txtCodi").value;
			var lstrRet = LeerCamposXML("clientes", lstrClie, "_clie_respa");
			document.all('txtRespa').value = lstrRet;				
		}	
		
		//function mSelecCP()
		//{
		//    document.all["txtDireCP"].value = '';
		//	document.all["cmbLocaAux"].value = document.all["cmbDireLoca"].value;
		//	document.all["txtDireCP"].value = document.all["cmbLocaAux"].item(document.all["cmbLocaAux"].selectedIndex).text;
		//}
		function btnSelecDire_click()
	   	{
		 gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Direciones de los integrantes&tabla=dire_clientesX&filtros='"+ document.all("hdnClieIds").value +"'", 7, "700","300");
		}
	  //  function expandir()
		//{
		//	try{ parent.frames("menu").CambiarExp();}catch(e){;}
		//}
		
		
		function mCargarProvincias(pPais,pcmbPcia)
		{
		    var sFiltro = pPais.value;
   		    if (sFiltro != '')
			   {
			    LoadComboXML("provincias_cargar", sFiltro, pcmbPcia, "S");
			   }
			else
			{
				document.all(pcmbPcia).innerText='';
			}
		}	
		
		/*
			Dario 2015-04-15 (funcion que habilita el los botones y el txt del nro de criador
							  de acuerdo al raza_PermiteEditarNumeradorCriador de razas)	
		*/
		function HabilitarTXTNume()
		{
			document.all("txtNume").value = ""
			document.all("btnNroCriador").disabled = true;
			document.all("btnNroNoCriador").disabled = true;
			
			if (document.all("cmbRaza").value != "")
			{
				var vstrRet = LeerCamposXML("razas","@raza_id = " + document.all("cmbRaza").value, "raza_PermiteEditarNumeradorCriador");
				
				if(vstrRet == '1')
				{
					document.all("txtNume").disabled = false;
					document.all("btnNroCriador").disabled = true;
					document.all("btnNroNoCriador").disabled = false;
				}
				else
				{
					document.all("txtNume").disabled = true;
					document.all("btnNroCriador").disabled = false;
					document.all("btnNroNoCriador").disabled = false;
				}  
			}
		}
		
		function btnNroCriador_click()
		{
			if (document.all("cmbRaza").value != "")
				{
					var vstrRet = LeerCamposXML("criador_max_numero","@raza_id = " + document.all("cmbRaza").value + ",@bitNoCriador = 0", "numero");
					/* 
					  Dario 2013-08-27
					  se pone validacion de que raza permite modificar el nro de criador 
					  traido automaticamente
					  Dario 2015-04-15 se pasa a la funcion	 HabilitarTXTNume solo cuando cambia la raza
					var vstrRet2 = LeerCamposXML("razas","@raza_id = " + document.all("cmbRaza").value, "raza_PermiteEditarNumeradorCriador");
				
					if(vstrRet2 == '1')
					{
						document.all("txtNume").disabled = false;
					}
					else
					{
						document.all("txtNume").disabled = true;
					}  
					*/
					document.all("txtNume").value = vstrRet;
					document.all("hdnNumeCria").value = vstrRet;
				}
			else
				alert("Debe ingresar la Raza");
		}
		
		function btnNroNoCriador_click()
		{
			if (document.all("cmbRaza").value != "")
				{
					if(document.all("usrClie:txtCodi").value != "")
					{
						var idCliente = document.all("usrClie:txtCodi").value;
						
						var vstrRet = LeerCamposXML("criador_max_numero"
												   ,"@raza_id = " + document.all("cmbRaza").value  + ",@bitNoCriador = 1" + ",@clie_id=" + idCliente
												   ,"numero");
					
						document.all("txtNume").value = vstrRet;
						document.all("hdnNumeCria").value = vstrRet;
					}
					else
					   alert("Debe Indicar el Cliente");
				}
			else
				alert("Debe ingresar la Raza");
		}
			
		function mHabilitarMotivoBaja()
		{
			if (document.all("cmbEsta").value != "")
			{
				var vstrRet = LeerCamposXML("estados", document.all("cmbEsta").value, "esta_baja").split("|");
				if(vstrRet[0]==1)
				{
					//if (document.all("txtBajaMoti")!=null)
					//	document.all("txtBajaMoti").disabled = false ;
					ActivarFecha("txtFechaBaja",true);
					document.all("hdnBaja").value = "1";							
				}
				else
				{
					//if (document.all("txtBajaMoti")!=null)
					//{
					//	document.all("txtBajaMoti").value = "";
					//	document.all("txtBajaMoti").disabled = true;
					//}
					document.all("txtFechaBaja").value = "";
					document.all("hdnBaja").value = "";
					ActivarFecha("txtFechaBaja",false);
				}
			}						
		}
		</script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('')" class="pagina" onunload="gCerrarVentanas();" leftMargin="5"
		rightMargin="0" topMargin="5">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Expedientes</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" colSpan="3" noWrap><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderStyle="none" BorderWidth="1px"
											width="100%">
											<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<TR>
													<TD colSpan="4">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																		ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE class="FdoFld" border="0" cellSpacing="0" cellPadding="0" width="100%">
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 20%; HEIGHT: 36px" vAlign="top" align="right">
																				<asp:label id="lblNumeFil" runat="server" cssclass="titulo">Nro. Raza:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 80%; HEIGHT: 36px">
																				<TABLE style="WIDTH: 568px; HEIGHT: 16px">
																					<TR>
																						<TD style="WIDTH: 291px; HEIGHT: 17px" vAlign="bottom">
																							<cc1:combobox id="cmbRazaFil" class="combo" runat="server" cssclass="cuadrotexto" Width="200px"
																								AceptaNull="False" Height="20px" MostrarBotones="False" NomOper="razas_cargar" filtra="true"></cc1:combobox>&nbsp;</TD>
																						<TD vAlign="top">
																							<asp:label id="Label1" runat="server" cssclass="titulo">Nro. Criador:</asp:label>
																							<cc1:numberbox id="txtCriaNumeFil" runat="server" cssclass="cuadrotexto" Width="112px" Visible="true"
																								esdecimal="False" MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																					</TR>
																				</TABLE>
																			</TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 2px" height="2" background="imagenes/formdivmed.jpg" colSpan="2"
																				align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblApelFil" runat="server" cssclass="titulo">Apellido/Raz�n Social:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtApelFil" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblNombFil" runat="server" cssclass="titulo">Nombre:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtNombFil" runat="server" cssclass="cuadrotexto" Width="80%"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																			<TD>
																				<asp:checkbox id="chkBusc" Text="Buscar en..." CssClass="titulo" Runat="server"></asp:checkbox>&nbsp;&nbsp;&nbsp;
																				<asp:checkbox id="chkBaja" Text="Incluir Dados de Baja" CssClass="titulo" Runat="server"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblPaisFil" runat="server" cssclass="titulo">Pa�s:</asp:label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox id="cmbPaisFil" class="combo" runat="server" Width="260px" onchange="mCargarProvincias(this,'cmbProvFil')"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblProvFil" runat="server" cssclass="titulo">Provincia:</asp:label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox id="cmbProvFil" class="combo" runat="server" Width="260px"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblDescFil" runat="server" cssclass="titulo">Establecimiento:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtDescFil" runat="server" cssclass="cuadrotexto" Width="300px"></CC1:TEXTBOXTAB>
																				<asp:checkbox id="chkBuscFil" Text="Buscar en..." CssClass="titulo" Runat="server"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:Label id="lblClieFil" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																			<TD>
																				<UC1:CLIE id="usrClieFil" runat="server" AceptaNull="false" FilDocuNume="True" MuestraDesc="False"
																					FilTipo="S" FilSociNume="True" AutoPostBack="False" Saltos="1,2" Tabla="Clientes"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblRespaFil" runat="server" cssclass="titulo">Nro. RENSPA:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtRespaFil" runat="server" cssclass="cuadrotexto" Width="200px"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblSociNumeFil" runat="server" cssclass="titulo">Nro. Socio:</asp:label>&nbsp;</TD>
																			<TD>
																				<cc1:numberbox id="txtSociNumeFil" runat="server" cssclass="cuadrotexto" Width="200px" esdecimal="False"
																					MaxValor="9999999999999" MaxLength="12"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblCuitFil" runat="server" cssclass="titulo">CUIT:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:CUITBOX id="txtCuitFil" runat="server" cssclass="cuadrotexto" Width="200px"></CC1:CUITBOX></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblDocuFil" runat="server" cssclass="titulo">Nro. Documento:</asp:label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox id="cmbDocuTipoFil" class="combo" runat="server" Width="100px" AceptaNull="False"></cc1:combobox>
																				<cc1:numberbox id="txtDocuNumeFil" runat="server" cssclass="cuadrotexto" Width="143px" esdecimal="False"
																					MaxValor="9999999999999"></cc1:numberbox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblCunicaFil" runat="server" cssclass="titulo">Clave �nica:</asp:label>&nbsp;</TD>
																			<TD>
																				<CC1:TEXTBOXTAB id="txtCunicaFil" onkeypress="gMascaraCunica(this)" runat="server" cssclass="cuadrotexto"
																					Width="161px" maxlength="13"></CC1:TEXTBOXTAB></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 17px" align="right">
																				<asp:label id="lblProdFil" runat="server" cssclass="titulo" Visible="False">Producto:</asp:label>&nbsp;</TD>
																			<TD></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD height="10" colSpan="3"></TD>
								</TR>
								<TR>
									<TD vAlign="top" colSpan="3" align="right"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos"
											OnPageIndexChanged="DataGrid_Page" OnEditCommand="mSeleccionarCriador">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton3" runat="server" Text="Seleccionar" CausesValidation="false" CommandName="Edit">
															<img src='images/sele.gif' border="0" alt="Seleccionar Criador" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="false" DataField="cria_id" ReadOnly="True" HeaderText="Id Criador"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="_raza_desc" ReadOnly="True" HeaderText="Raza"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="cria_nume" ReadOnly="True" HeaderText="Nro.Criador"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="clie_id" ReadOnly="True" HeaderText="Nro.Cliente"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="clie_apel" ReadOnly="True" HeaderText="Apellido/Raz&#243;n Social"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="clie_nomb" HeaderText="Nombre"></asp:BoundColumn>
												<asp:BoundColumn Visible="True" DataField="_clie_socio_nume" HeaderText="Nro.Socio"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="_cuit" ReadOnly="True" HeaderText="CUIT"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="_clie_docu" HeaderText="Documento"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="_clie_cunica" HeaderText="Clave Unica"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="_cria_raza_id" ReadOnly="True" HeaderText="Raza"></asp:BoundColumn>
												<asp:BoundColumn Visible="false" DataField="_raza_codi" ReadOnly="True" HeaderText="Raza"></asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="_Prefijo" ReadOnly="True" HeaderText="Prefijo"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<TR>
									<TD height="10" colSpan="3"></TD>
								</TR>
								<TR>
									<TD vAlign="middle" width="100" colSpan="2">&nbsp;<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
											IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif"
											ForeColor="Transparent" ImageDisable="btnNuev0.gif" ToolTip="Agregar un Nuevo Expediente"></CC1:BOTONIMAGEN></TD>
									<TD align="right"></TD>
								</TR>
								<TR>
									<TD colSpan="3" align="center">
										<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
												width="100%" Height="116px" Visible="False">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD height="5" colSpan="2">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3" align="center">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD vAlign="top" width="100" align="right">
																			<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label></TD>
																		<TD width="100%" colSpan="2">
																			<UC1:CLIE id="usrClie" runat="server" FilDocuNume="True" Saltos="1,1,1" Tabla="Clientes" Obligatorio="true"
																				Ancho="780" FilCUIT="True" FilClie="True" Alto="560"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="3" align="center">
																			<TABLE style="MARGIN-TOP: 10px" id="Table9" class="marco" cellSpacing="0" cellPadding="0"
																				width="100%">
																				<TR>
																					<TD style="WIDTH: 15%" align="right">
																						<asp:Label id="lblDPDireccion" runat="server" cssclass="titulo">Direcci�n Postal:</asp:Label>&nbsp;</TD>
																					<TD style="WIDTH: 85%" align="left">
																						<CC1:TEXTBOXTAB id="txtDPDireccion" runat="server" cssclass="cuadrotextodeshab" Width="85%" Enabled="False"></CC1:TEXTBOXTAB>
																						<asp:Label id="lblDPCP" runat="server" cssclass="titulo">CP:</asp:Label>&nbsp;
																						<CC1:TEXTBOXTAB id="txtDPCP" runat="server" cssclass="cuadrotextodeshab" Width="10%" Enabled="False"></CC1:TEXTBOXTAB></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 15%" align="right">&nbsp;</TD>
																					<TD noWrap>
																						<asp:checkbox id="chkRetDocuPersonal" Text="Retira Documentacion en forma personal" CssClass="titulo"
																							Runat="server"></asp:checkbox></TD>
																				</TR>
																				<TR>
																					<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																				</TR>
																				<TR>
																					<TD colSpan="2">
																						<TABLE style="WIDTH: 100%" border="0" cellSpacing="0" cellPadding="0">
																							<TR>
																								<TD style="WIDTH: 15%" align="right">
																									<asp:Label id="lblTParea" runat="server" cssclass="titulo">�rea:</asp:Label>&nbsp;</TD>
																								<TD style="WIDTH: 41px">
																									<CC1:TEXTBOXTAB id="txtTParea" runat="server" cssclass="cuadrotextodeshab" Width="40px" Enabled="False"></CC1:TEXTBOXTAB></TD>
																								<TD style="WIDTH: 8%" align="right">
																									<asp:Label id="lblTPTelefono" runat="server" cssclass="titulo">Tel�fono:</asp:Label>&nbsp;</TD>
																								<TD>
																									<CC1:TEXTBOXTAB id="txtTPTelefono" runat="server" cssclass="cuadrotextodeshab" Width="100%" Enabled="False"></CC1:TEXTBOXTAB></TD>
																							</TR>
																							<TR>
																								<TD align="right">
																									<asp:Label id="lblMPMail" runat="server" cssclass="titulo">Mail:</asp:Label>&nbsp;</TD>
																								<TD colSpan="3">
																									<CC1:TEXTBOXTAB id="txtMPMail" runat="server" cssclass="cuadrotextodeshab" Width="100%" Enabled="False"
																										Panel="Mail" EsMail="True"></CC1:TEXTBOXTAB></TD>
																							</TR>
																						</TABLE>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD colSpan="3" align="center">
																			<TABLE style="MARGIN-TOP: 10px" id="Table8" class="marco" cellSpacing="0" cellPadding="0"
																				width="100%">
																				<TR>
																					<TD style="WIDTH: 15%" align="right">
																						<asp:Label id="lblEstablecimiento" runat="server" cssclass="titulo">Establecimiento:</asp:Label>&nbsp;</TD>
																					<TD>
																						<CC1:TEXTBOXTAB id="txtEstablecimiento" runat="server" cssclass="cuadrotextodeshab" Width="100%"
																							Obligatorio="True" Enabled="False"></CC1:TEXTBOXTAB></TD>
																				</TR>
																				<TR>
																					<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																				</TR>
																				<TR>
																					<TD align="right">
																						<asp:Label id="lblDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label>&nbsp;</TD>
																					<TD>
																						<CC1:TEXTBOXTAB id="txtDire" runat="server" cssclass="cuadrotextodeshab" Width="85%" Enabled="False"></CC1:TEXTBOXTAB>
																						<asp:Label id="lblCP" runat="server" cssclass="titulo">CP:</asp:Label>&nbsp;
																						<CC1:TEXTBOXTAB id="txtCP" runat="server" cssclass="cuadrotextodeshab" Width="10%" Enabled="False"></CC1:TEXTBOXTAB></TD>
																				</TR>
																				<TR>
																					<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																				</TR>
																				<TR>
																					<TD colSpan="2">
																						<TABLE style="WIDTH: 100%" border="0" cellSpacing="0" cellPadding="0">
																							<TR>
																								<TD style="WIDTH: 15%" align="right">
																									<asp:Label id="lblTeleArea" runat="server" cssclass="titulo">�rea:</asp:Label>&nbsp;</TD>
																								<TD style="WIDTH: 41px">
																									<CC1:TEXTBOXTAB id="txtTeleArea" runat="server" cssclass="cuadrotextodeshab" Width="40px" Enabled="False"></CC1:TEXTBOXTAB></TD>
																								<TD style="WIDTH: 8%" align="right">
																									<asp:Label id="lblTeleNume" runat="server" cssclass="titulo">Tel�fono:</asp:Label>&nbsp;</TD>
																								<TD>
																									<CC1:TEXTBOXTAB id="txtTeleNume" runat="server" cssclass="cuadrotextodeshab" Width="100%" Enabled="False"></CC1:TEXTBOXTAB></TD>
																							</TR>
																							<TR>
																								<TD align="right">
																									<asp:Label id="lblMail" runat="server" cssclass="titulo">Mail:</asp:Label>&nbsp;</TD>
																								<TD colSpan="3">
																									<CC1:TEXTBOXTAB id="txtMail" runat="server" cssclass="cuadrotextodeshab" Width="100%" Enabled="False"
																										Panel="Mail" EsMail="True"></CC1:TEXTBOXTAB></TD>
																							</TR>
																						</TABLE>
																					</TD>
																				</TR>
																			</TABLE>
																		</TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD colSpan="3">
															<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table4" class="FdoFld" border="0" cellPadding="0"
																align="left">
																<TR>
																	<TD style="WIDTH: 20%; HEIGHT: 17px" align="right">
																		<asp:Label id="lblRaza" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 80%" colSpan="2">
																		<cc1:combobox id="cmbRaza" class="combo" runat="server" cssclass="cuadrotexto" Width="300px" AceptaNull="False"
																			Height="20px" MostrarBotones="False" NomOper="razas_cargar" filtra="true" onchange="HabilitarTXTNume();"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" align="right">
																		<asp:Label id="lblNume" runat="server" cssclass="titulo">Nro. Criador:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:numberbox id="txtNume" runat="server" cssclass="cuadrotexto" Width="170px" esdecimal="False"></cc1:numberbox>&nbsp;<BUTTON style="WIDTH: 125px; HEIGHT: 20px" id="btnNroCriador" class="boton" disabled onclick="btnNroCriador_click();"
																			runat="server" value="Detalles">Traer N� Criador</BUTTON>&nbsp;<BUTTON style="WIDTH: 125px; HEIGHT: 20px" id="btnNroNoCriador" class="boton" disabled onclick="btnNroNoCriador_click();"
																			runat="server" value="Detalles">Traer N� No Criador</BUTTON>&nbsp;
																	</TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" align="right">
																		<asp:Label id="Label2" runat="server" cssclass="titulo">Convenio RRGG:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:combobox id="cmbConvenioRRGG" class="combo" runat="server" Width="120px" AceptaNull="true"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" vAlign="middle" align="right">
																		<asp:Label id="lblRespa" runat="server" cssclass="titulo">Nro. RENSPA:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<CC1:TEXTBOXTAB id="txtRespa" runat="server" cssclass="cuadrotexto" Width="170px"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" align="right">
																		<asp:Label id="lblAltaFecha" runat="server" cssclass="titulo">Fecha Alta:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:DateBox id="txtAltaFecha" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;&nbsp;&nbsp;
																	</TD>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" align="right"></TD>
																	<TD colSpan="2">
																		<asp:CheckBox id="chkConfAviso" Text="Mail de Confirmaci�n" CssClass="titulo" Runat="server" ToolTip="Enviar aviso de tr�mites aprobados"></asp:CheckBox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" align="right"></TD>
																	<TD colSpan="2">
																		<asp:CheckBox id="chkIntegraComi" Text="Integra Comisi�n de Razas" CssClass="titulo" Runat="server"
																			Enabled="False"></asp:CheckBox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" align="right">
																		<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:combobox id="cmbEsta" class="combo" runat="server" Width="120px" AceptaNull="False" onchange="mHabilitarMotivoBaja();"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" align="right">
																		<asp:Label id="lblFechaBaja" runat="server" cssclass="titulo">Fecha Baja:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<cc1:DateBox id="txtFechaBaja" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" vAlign="top" align="right">
																		<asp:Label id="lblBajaMoti" runat="server" cssclass="titulo">Motivo de la Baja:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<CC1:TEXTBOXTAB id="txtBajaMoti" runat="server" cssclass="cuadrotexto" Width="80%" Height="41px"
																			TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 197px" vAlign="top" align="right">
																		<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:</asp:Label>&nbsp;</TD>
																	<TD colSpan="2">
																		<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Width="80%" Height="41px" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD height="34" vAlign="bottom" colSpan="3" align="center"><BUTTON style="WIDTH: 109px; HEIGHT: 20px" id="btnEstab" class="boton" onclick="btnEstab_click();"
																value="Detalles">Establecimientos</BUTTON>&nbsp;<BUTTON style="WIDTH: 100px" id="btnOtros" class="boton" onclick="btnOtros_click();" runat="server"
																value="Detalles">Otros Datos</BUTTON>&nbsp;<BUTTON style="WIDTH: 100px" id="btnPrefijos" class="boton" onclick="btnPrefijos_click();"
																runat="server" value="Deuda">Prefijos</BUTTON>&nbsp;<BUTTON style="WIDTH: 100px" id="btnComisiones" class="boton" onclick="btnComisiones_click();"
																runat="server" value="Deuda">Comisiones</BUTTON>&nbsp;&nbsp;</TD>
													</TR>
													<TR>
														<TD height="34" vAlign="bottom" colSpan="3" align="center"><BUTTON style="WIDTH: 100px" id="btnTramites" class="boton" runat="server" value="Tr�mites">Tr�mites</BUTTON>&nbsp;<BUTTON style="WIDTH: 100px" id="btnSemen" class="boton" runat="server" value="Semen">Semen 
																Stock</BUTTON>&nbsp;<BUTTON style="WIDTH: 110px" id="btnEmbriones" class="boton" runat="server" value="Embriones">Embriones 
																Stock</BUTTON>&nbsp;<BUTTON style="WIDTH: 100px" id="btnServi" class="boton" runat="server" value="Servicios">Servicios</BUTTON>&nbsp;<BUTTON style="WIDTH: 100px" id="btnNaci" class="boton" runat="server" value="Nacimientos">Nacimientos</BUTTON>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<DIV style="DISPLAY: inline" id="divgraba" runat="server">
							<ASP:PANEL id="panBotones" Runat="server">
								<TABLE width="100%">
									<TR>
										<TD align="center">
											<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
									</TR>
									<TR height="30">
										<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
											<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
									</TR>
								</TABLE>
							</ASP:PANEL>
						</DIV>
						<DIV style="DISPLAY: none" id="divproce" runat="server">
							<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							</asp:panel>
						</DIV>
					</td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<!--- FIN CONTENIDO --->
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnClieId" runat="server"></asp:textbox><asp:textbox id="hdnBaja" runat="server"></asp:textbox><asp:textbox id="hdnValorId" runat="server"></asp:textbox><asp:textbox id="hdnModi" runat="server"></asp:textbox><asp:textbox id="hdnSess" runat="server"></asp:textbox><asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox><asp:textbox id="hdnNumeCria" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		function mPorcPeti()
		{
			document.all("divgraba").style.display ="none";
			document.all("divproce").style.display ="inline";
		}
		
		if (document.all["usrClieFil_txtCodi"]!= null && !document.all["usrClieFil_txtCodi"].disabled)
			document.all["usrClieFil_txtCodi"].focus();
					
		if (document.all["btnEstab"]!=null)
		{
			document.all["btnEstab"].disabled = (document.all["hdnId"].value=='');
			document.all["btnComisiones"].disabled = (document.all["hdnId"].value=='');
			document.all["btnOtros"].disabled = (document.all["hdnId"].value=='');
			document.all["btnPrefijos"].disabled = (document.all["hdnId"].value=='');
			document.all["btnTramites"].disabled = (document.all["hdnId"].value=='');
			document.all["btnServi"].disabled = (document.all["hdnId"].value=='');
			document.all["btnNaci"].disabled = (document.all["hdnId"].value=='');
		}
		
		/*
		if (document.all["cmbRaza"]!=null)
			document.all["btnNroCriador"].disabled = !(document.all("txtNume").value < 10000);
		*/
		if (document.all["cmbEsta"]!=null)
			mHabilitarMotivoBaja();
		
		
		//gSetearTituloFrame('Expedientes');
	
		
		</SCRIPT>
	</BODY>
</HTML>
