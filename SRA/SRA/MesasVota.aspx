<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.MesasVota" CodeFile="MesasVota.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Mesas de Votaci�n</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('')" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server" width="391px">Mesas de Votaci�n</asp:label>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="0"
										Width="97%" Visible="True">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD vAlign="top"><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblAsam" runat="server" cssclass="titulo">Asamblea:</asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<cc1:combobox class="combo" id="cmbAsam" runat="server" Width="350px" AutoPostback="true"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 82%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD vAlign="middle" width="100%">
									<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
										<TR>
											<TD>
												<asp:button cssclass="boton" id="btnAlta" runat="server" Width="170" CausesValidation="False"
													Text="Generar Mesas"></asp:button>
											</TD>
											<TD align="right">
												<asp:button cssclass="boton" id="btnBaja" runat="server" Width="170" CausesValidation="False"
													Text="Borrar Mesas"></asp:button>
											</TD>
										</TR>
										<TR>
											<TD colspan="2">
												<asp:Label id="lblOrvo" runat="server" cssclass="titulo" ForeColor="Red" Visible="False"></asp:Label>
											</TD>
										</TR>
									</TABLE>
								</TD>
								<TD width="50" align="right"></TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR>
								<TD colSpan="3" vAlign="top" align="center">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None"
										CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="2%">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="mevo_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="mevo_nume" HeaderText="Mesa"></asp:BoundColumn>
											<asp:BoundColumn DataField="mevo_desde" HeaderText="Desde"></asp:BoundColumn>
											<asp:BoundColumn DataField="mevo_hasta" HeaderText="Hasta"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="right" colspan="3">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Mesa</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCate" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="80px" CausesValidation="False" Height="21px">Categorias</asp:linkbutton></td>
											<td width="1"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"></td>
											<td width="1"></td>
											<td width="1"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"></td>
											<td width="1"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"></td>
											<td width="1"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV><asp:panel id="panDato" runat="server" width="99%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											Visible="False">
											<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD></TD>
													<TD height="5">
														<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
													<TD vAlign="top" align="right">&nbsp;
														<asp:ImageButton id="imgClose" runat="server" CausesValidation="False" ImageUrl="images\Close.bmp"
															ToolTip="Cerrar"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" align="right" colSpan="3">
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 16px" align="right" width="100">
																		<asp:Label id="lblNum" runat="server" cssclass="titulo">Numero:</asp:Label>&nbsp;
																	</TD>
																	<TD style="HEIGHT: 16px" colSpan="2" height="16">
																		<CC1:NumberBox id="txtNum" runat="server" cssclass="cuadrotexto" Width="64px" Obligatorio="True"></CC1:NumberBox></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 16px" align="right">
																		<asp:Label id="lblDesde" runat="server" cssclass="titulo">Desde:</asp:Label>&nbsp;
																	</TD>
																	<TD style="HEIGHT: 16px" colSpan="2" height="16">
																		<CC1:TEXTBOXTAB id="txtDesde" runat="server" cssclass="cuadrotexto" Width="75px" Obligatorio="True"
																			apeDesc MaxLength="8"></CC1:TEXTBOXTAB></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 21px" align="right">
																		<asp:Label id="lblHasta" runat="server" cssclass="titulo">Hasta:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 21px" colSpan="2" height="21">
																		<CC1:TEXTBOXTAB id="txtHasta" runat="server" cssclass="cuadrotexto" Width="75px" Obligatorio="True"
																			apeDesc MaxLength="8"></CC1:TEXTBOXTAB></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="3">
														<asp:panel id="panCate" runat="server" cssclass="titulo" Visible="False" Width="100%">
															<TABLE id="TableDetalle" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																<TR>
																	<TD style="WIDTH: 100%" colSpan="2">
																		<asp:datagrid id="grdCate" runat="server" width="95%" Visible="False" BorderWidth="1px" BorderStyle="None"
																			AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1"
																			OnPageIndexChanged="grdCate_PageChanged" OnEditCommand="mEditarDatosCate" AutoGenerateColumns="False">
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<Columns>
																				<asp:TemplateColumn>
																					<HeaderStyle Width="20px"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																				<asp:BoundColumn Visible="False" DataField="meca_id" ReadOnly="True" HeaderText="ID">
																					<HeaderStyle Width="2%"></HeaderStyle>
																				</asp:BoundColumn>
																				<asp:BoundColumn DataField="_cate_desc" HeaderText="Categor&#237;a">
																					<HeaderStyle Width="98%"></HeaderStyle>
																				</asp:BoundColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 17px" align="right">
																		<asp:Label id="lblCateC" runat="server" cssclass="titulo">Categor�a:</asp:Label>&nbsp;</TD>
																	<TD style="HEIGHT: 17px">
																		<cc1:combobox class="combo" id="cmbCateC" runat="server" Width="400px"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD vAlign="bottom" colSpan="2" height="5"></TD>
																</TR>
																<TR>
																	<TD vAlign="middle" align="center" colSpan="2" height="30">
																		<asp:Button id="btnAltaCate" runat="server" cssclass="boton" Width="123px" Text="Agregar Categor�a"></asp:Button>&nbsp;&nbsp;
																		<asp:Button id="btnBajaCate" runat="server" cssclass="boton" Width="123px" Text="Eliminar Categor�a"></asp:Button>&nbsp;
																		<asp:Button id="btnModiCate" runat="server" cssclass="boton" Width="129px" Text="Modificar Categor�a"></asp:Button>&nbsp;
																		<asp:Button id="btnLimpCate" runat="server" cssclass="boton" Width="119px" Text="Limpiar Categor�a"></asp:Button></TD>
																</TR>
															</TABLE>
														</asp:panel></TD>
												</TR>
											</TABLE>
										</asp:panel><ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR>
													<TD align="center"></TD>
												</TR>
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button></TD>
												</TR>
											</TABLE>
										</ASP:PANEL></DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnMecaId" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>
