Namespace SRA

Partial Class FacExterna_detalle_pop
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblExenta As System.Web.UI.WebControls.Label
   Protected WithEvents lblPercep As System.Web.UI.WebControls.Label
   Protected WithEvents lblPrecSoci As System.Web.UI.WebControls.Label
   Protected WithEvents lblMiemCD As System.Web.UI.WebControls.Label
   Protected WithEvents lblCuitT As System.Web.UI.WebControls.Label
   Protected WithEvents lblNroSocio As System.Web.UI.WebControls.Label
   Protected WithEvents lblCondIVAT As System.Web.UI.WebControls.Label
   Protected WithEvents lblestado As System.Web.UI.WebControls.Label
   Protected WithEvents lblCatego As System.Web.UI.WebControls.Label
   Protected WithEvents lblISEAT As System.Web.UI.WebControls.Label
   Protected WithEvents lblCEIDAT As System.Web.UI.WebControls.Label
   Protected WithEvents btnGenerarCuot As System.Web.UI.WebControls.ImageButton
   Protected WithEvents panDato As System.Web.UI.WebControls.Panel
   Protected WithEvents txtNroSocio As NixorControls.NumberBox
   Protected WithEvents txtestado As NixorControls.TextBoxTab
   Protected WithEvents txtCatego As NixorControls.TextBoxTab
   Protected WithEvents txtcuit As NixorControls.TextBoxTab
   Protected WithEvents txtISEA As NixorControls.NumberBox
   Protected WithEvents txtCEIDA As NixorControls.NumberBox
   Protected WithEvents txtCondIVA As NixorControls.TextBoxTab

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrId01 As String
   Private mstrId02 As String
   Private mstrConn As String
   Dim mdsDatos As DataSet
   Dim mstrCmd As String

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If Not Page.IsPostBack Then
            mConsultar()
         End If
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try

   End Sub
#End Region

#Region "Inicializacion de Variables"
   Public Sub mInicializar()

      mstrId01 = Request.QueryString("id01")
      mstrId02 = Request.QueryString("id02")
      lblActividad.Text = "Actividad: " + Request.QueryString("actividad")
      lblCliente.Text = "Cliente: " + Request.QueryString("cliente")
      lblFechaImporte.Text = "Fecha: " + Request.QueryString("fecha") + "  Importe($): " + Request.QueryString("importe")
   End Sub
#End Region

   Public Sub mConsultar()
      Try
         Dim ds As DataSet
         'conceptos
         ds = clsSQLServer.gExecuteQuery(mstrConn, "exec dbo.FACA802_consul " + clsSQLServer.gFormatArg(mstrId01, SqlDbType.Int) + "," + clsSQLServer.gFormatArg(mstrId02, SqlDbType.Int))
         grdConceptos.DataSource = ds
         grdConceptos.DataBind()

         'vtos 
         ds = clsSQLServer.gExecuteQuery(mstrConn, "exec dbo.FACA803_consul " + clsSQLServer.gFormatArg(mstrId01, SqlDbType.Int) + "," + clsSQLServer.gFormatArg(mstrId02, SqlDbType.Int))
         grdVtos.DataSource = ds
         grdVtos.DataBind()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConceptos.EditItemIndex = -1
         If (grdConceptos.CurrentPageIndex < 0 Or grdConceptos.CurrentPageIndex >= grdConceptos.PageCount) Then
            grdConceptos.CurrentPageIndex = 0
         Else
            grdConceptos.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnCerrar.Click
      Response.Write("<Script>window.close();</script>")
   End Sub
End Class

End Namespace
