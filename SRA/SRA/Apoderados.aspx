<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Apoderados" CodeFile="Apoderados.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Poderes</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px" vAlign="bottom" colSpan="3" height="33"><asp:label id="Label4" runat="server" cssclass="opcion" width="391px">Poderes</asp:label></TD>
								</TR>
								<!--- filtro --->
								<TR>
									<TD vAlign="top" colSpan="3"><asp:panel id="Panel1" runat="server" cssclass="titulo" Width="100%" BorderStyle="Solid" BorderWidth="0"
											visible="True">
											<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																		ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnCons.gif"
																		ImageOver="btnCons2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
																		ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif"
																		ImageOver="btnLimp2.gif" ForeColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD>
														<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
															<TR>
																<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																<TD><!-- FOMULARIO -->
																	<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="Label5" runat="server" cssclass="titulo">Asamblea:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 85%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																				<cc1:combobox class="combo" id="cmbAsamFil" runat="server" Width="272px" IncludesUrl="../Includes/"
																					ImagesUrl="../Images/" AceptaNull="false">
																					<asp:ListItem Value="0">General</asp:ListItem>
																				</cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="Label1" runat="server" cssclass="titulo">Poderdante:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 85%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrSocioFil" runat="server" width="100%" autopostback="True" FilCUIT="True"
																					Tabla="Socios" Saltos="1,1,1" FilSociNume="True" PermiModi="True" MuestraDesc="False"
																					CatePode="1" FilDocuNume="True"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="../imagenes/formdivmed.jpg" width="1"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%; HEIGHT: 24px" align="right" background="imagenes/formfdofields.jpg">
																				<asp:Label id="Label2" runat="server" cssclass="titulo">Apoderado:</asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 85%; HEIGHT: 24px" background="imagenes/formfdofields.jpg">
																				<UC1:CLIE id="usrApoderadoFil" runat="server" width="100%" autopostback="True" FilCUIT="True"
																					Tabla="Socios" Saltos="1,1,1" FilSociNume="True" PermiModi="True" MuestraDesc="False"
																					CatePode="1" FilDocuNume="True"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg" colSpan="2"></TD>
																		</TR>
																	</TABLE>
																</TD>
																<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR> <!-- Fin Filtro -->
								<TR>
									<TD align="center" colSpan="7">
										<DIV><asp:panel id="panDato" runat="server" Height="116px" cssclass="titulo" width="100%">
												<TABLE id="Table2" style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
													<TR>
														<TD vAlign="top" width="100%" colSpan="3"></TD>
													</TR> <!-- Fin Filtro -->
													<TR>
														<TD style="HEIGHT: 15px" align="left" colSpan="3" height="15">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo">Apoderados</asp:Label></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panApod" runat="server" cssclass="titulo" Width="100%" Visible="True">
																<TABLE id="TableDetalle" cellPadding="0" width="100%" align="left" border="0">
																	<TR>
																		<TD align="left" width="100%" colSpan="3">
																			<asp:datagrid id="grdApod" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AllowPaging="True"
																				HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="grdApod_PageChanged"
																				OnEditCommand="mEditarDatos" AutoGenerateColumns="False">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="20px"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="apod_id" ReadOnly="True" HeaderText="ID">
																						<HeaderStyle Width="2%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_soci_nume" HeaderText="Nro"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_socio" HeaderText="Poderdante">
																						<HeaderStyle Width="46%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_apod_soci_nume" HeaderText="Nro"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_apoderado" HeaderText="Apoderado">
																						<HeaderStyle Width="46%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn DataField="_general" HeaderText="General">
																						<HeaderStyle Width="6%"></HeaderStyle>
																					</asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="4">
																			<asp:Button id="btnNuevoApoderado" runat="server" cssclass="boton" Width="130px" Text="Nuevo poder >>"></asp:Button>&nbsp;&nbsp;
																		</TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 100%" colSpan="4">
																			<asp:panel id="PanDetalle" runat="server" cssclass="titulo" Width="100%" Visible="false">
																				<TABLE class="FdoFld" id="TableaPnDetalle" style="WIDTH: 100%" cellPadding="0" align="left"
																					border="0">
																					<TR>
																						<TD align="left"></TD>
																						<TD align="right" height="5">
																							<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="Label3" runat="server" cssclass="titulo" Visible="True">Poderdante:</asp:Label></TD>
																						<TD height="5">
																							<UC1:CLIE id="usrSocio" runat="server" width="100%" FilCUIT="True" Tabla="Socios" Saltos="1,1,1"
																								FilSociNume="True" PermiModi="True" MuestraDesc="False" CatePode="1" FilDocuNume="True"
																								Visible="True"></UC1:CLIE></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="lblApod" runat="server" cssclass="titulo" Visible="True">Socio apoderado:</asp:Label></TD>
																						<TD height="5">
																							<UC1:CLIE id="usrApoderado" runat="server" width="100%" FilCUIT="True" Tabla="Socios" Saltos="1,1,1"
																								FilSociNume="True" PermiModi="True" MuestraDesc="False" CatePode="1" FilDocuNume="True"
																								Visible="True"></UC1:CLIE></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD style="HEIGHT: 15px" align="right">
																							<asp:Label id="lblAsam" runat="server" cssclass="titulo">Asamblea: &nbsp; </asp:Label></TD>
																						<TD style="HEIGHT: 15px">
																							<asp:RadioButton id="chkTodo" runat="server" cssclass="titulo" Text="General" GroupName="asam" Checked="True"></asp:RadioButton></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD style="HEIGHT: 19px" align="right"></TD>
																						<TD style="HEIGHT: 19px">
																							<asp:RadioButton id="chkSele" runat="server" cssclass="titulo" Text="Seleccionar" GroupName="asam"></asp:RadioButton>&nbsp;
																							<cc1:combobox class="combo" id="cmbAsam" runat="server" Width="444px" Enabled="False" Obligatorio="False"></cc1:combobox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD align="right">
																							<asp:Label id="lblVenc" runat="server" cssclass="titulo">Vencimiento:</asp:Label></TD>
																						<TD height="5">
																							<cc1:DateBox id="txtVenc" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																								Obligatorio="False"></cc1:DateBox></TD>
																					</TR>
																					<TR>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																						<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																					</TR>
																					<TR>
																						<TD vAlign="bottom" colSpan="2" height="5"></TD>
																					</TR>
																					<TR>
																						<TD vAlign="middle" align="center" colSpan="2" height="30"><A id="editar" name="editar"></A>
																							<asp:Button id="BtnAltaApod" runat="server" cssclass="boton" Width="123px" Text="Agregar apoderado"></asp:Button>&nbsp;&nbsp;
																							<asp:Button id="btnBajaApod" runat="server" cssclass="boton" Width="123px" Text="Eliminar apoderado"></asp:Button>&nbsp;
																							<asp:Button id="btnModiApod" runat="server" cssclass="boton" Width="129px" Text="Modificar apoderado"></asp:Button>&nbsp;
																							<asp:Button id="btnLimpApod" runat="server" cssclass="boton" Width="119px" Text="Limpiar apoderado"></asp:Button></TD>
																					</TR>
																				</TABLE>
																			</asp:panel></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server"></ASP:PANEL></DIV>
									</TD>
								</TR>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Visible="True"></ASP:TEXTBOX><asp:textbox id="hdnApodId" runat="server" Visible="True"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">	
		function mCargarCombo()
		{
		  LoadComboXML("asambleas_poderes_cargar","","cmbAsam", "S");
		  document.all("cmbAsam").disabled = false;		 				  
		}
		
		function mDescargarCombo()
		{
		  document.all("cmbAsam").innerText = ""	
		  document.all("cmbAsam").disabled = true;		 				  
		}
		
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE>
		<DIV></DIV>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
