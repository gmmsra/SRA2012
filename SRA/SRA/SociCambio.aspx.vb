Namespace SRA

Partial Class SoliCambioCate
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub
#End Region

#Region "Definición de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_SoliCambios

    Private mstrConn As String
    Private mstrCmd As String
    Private mstrParaPageSize As Integer

    Private mdsDatos As DataSet

    Private Enum Columnas As Integer
        Edit = 0
        Id = 1
        Nume = 2
    End Enum
#End Region

#Region "Inicialización de Variables"
    Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        If Not usrSoci.Valor Is DBNull.Value Then
            mCargarDatosSocio(usrSoci.Valor)
        End If
    End Sub

#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()

            If (Not Page.IsPostBack) Then

                mSetearEventos()
                mSetearMaxLength()
                Session(mstrTabla) = Nothing
                mdsDatos = Nothing

                mCargarCombos()
                mConsultar()

            Else
                mdsDatos = Session(mstrTabla)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEventos()
        btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "categorias", cmbCate, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "distritos", cmbDist, "S")
        clsWeb.gCargarComboBool(cmbAuto, "N")
    End Sub

        Private Sub mSetearCate()
            Dim lbooDist As Boolean
            If Not cmbCate.Valor Is DBNull.Value Then
                With clsSQLServer.gObtenerEstruc(mstrConn, "categorias", cmbCate.Valor.ToString).Tables(0).Rows(0)
                    If IsDBNull(.Item("cate_dist")) Then
                        lbooDist = False
                    Else
                        lbooDist = True
                    End If
                    'lbooDist = .Item("cate_dist")
                End With
            End If
            cmbDist.Enabled = lbooDist
            If Not cmbDist.Enabled Then
                cmbDist.Limpiar()
            Else
                If Not Request.Form(cmbDist.UniqueID) Is Nothing Then
                    cmbDist.Valor = Request.Form(cmbDist.UniqueID)
                End If
            End If
        End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLong As Object

        lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtMoti.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "socm_obse")
    End Sub
#End Region

#Region "Seteo de Controles"

    Public Sub mCargarDatos(ByVal pstrId As String)

        Dim lbooAuto As Boolean

        mCrearDataSet(pstrId)
        With mdsDatos.Tables(mstrTabla).Rows(0)
            hdnId.Text = .Item("socm_id").ToString()
            usrSoci.Valor = .Item("socm_soci_id")
                txtMoti.Valor = .Item("socm_obse").ToString
                lblFechaAnterior.Text = .Item("_ante_fecha").ToString
            lblCategoriaAnterior.Text = .Item("_cate_esta_actual")
                txtFecha.Fecha = .Item("socm_fecha").ToString
                cmbCate.Valor = .Item("socm_cate_id").ToString
                'cmbAuto.ValorBool = "1"
                'cmbAuto.ValorBool = IIf(.Item("socm_auto").ToString().Trim().Length > 0, True, False)
                cmbAuto.Valor = .Item("socm_auto").ToString

                mSetearCate()

                cmbDist.Valor = .Item("socm_dist_id").ToString

                txtEsta.Text = .Item("_solicitud").ToString

                ' lbooAuto = IIf(.Item("_automatico") = 1, True, False)
                Dim auto = .Item("_automatico").ToString()
                lbooAuto = IIf(.Item("_automatico").ToString().Trim().Length > 0, True, False)



            End With

        mSetearEditor(False, lbooAuto)
        mMostrarPanel(True)

    End Sub

    Private Sub mCerrar()
        mConsultar()
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
    End Sub

#End Region

#Region "Opciones de ABM"

    Private Sub mAlta()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Alta()

            mMostrarPanel(False)
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mModi()
        Try
            Dim ldsEstruc As DataSet = mGuardarDatos()

            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
            lobjGenerico.Modi()

            mMostrarPanel(False)
            mConsultar()

        Catch ex As Exception

            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBaja()
        Try
            Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerico.Baja(hdnId.Text)

            mMostrarPanel(False)
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

        Private Function mGuardarDatos() As DataSet

            Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, hdnId.Text, "@tipo='C'")

            If cmbCate.Valor.ToString = CType(SRA_Neg.Constantes.Categorias.Activo, String) Or _
                    cmbCate.Valor.ToString = CType(SRA_Neg.Constantes.Categorias.Honorario, String) Or _
                    cmbCate.Valor.ToString = CType(SRA_Neg.Constantes.Categorias.Vitalicio, String) Then
                cmbDist.Enabled = True
            Else
                cmbDist.Enabled = False
            End If

            If (usrSoci.Valor Is System.DBNull.Value) Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el socio.")
            End If

            If (txtFecha.Text = "") Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de la solicitud.")
            End If

            If (cmbCate.Valor Is System.DBNull.Value) Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la nueva categoría.")
            End If

            If (cmbCate.SelectedItem.Text = lblCategoriaAnterior.Text) Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar una categoria diferente a la actual.")
            End If

            If cmbDist.Enabled And cmbDist.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar un Distrito.")
            End If

            With ldsEsta.Tables(0).Rows(0)
                .Item("socm_cate_id") = IIf(cmbCate.Valor.ToString.Trim.Length > 0, cmbCate.Valor, DBNull.Value)
                .Item("socm_soci_id") = IIf(usrSoci.Valor.ToString.Trim.Length > 0, usrSoci.Valor, DBNull.Value)
                .Item("socm_fecha") = IIf(txtFecha.Fecha.Trim.Length > 0, txtFecha.Fecha, DBNull.Value)
                .Item("socm_obse") = txtMoti.Text
                .Item("socm_dist_id") = IIf(cmbDist.Valor.Trim.Length > 0, cmbDist.Valor, DBNull.Value)
                .Item("socm_auto") = IIf(cmbAuto.Valor.ToString.Trim.Length > 0, cmbAuto.Valor, DBNull.Value)
                '.Item("socm_auto") = IIf(cmbAuto.ValorBool.ToString.Trim.Length > 0, cmbAuto.ValorBool, DBNull.Value)
            End With

            Return ldsEsta
        End Function

    Public Sub mCrearDataSet(ByVal pstrId As String)
        mdsDatos = clsSQLServer.gObtenerEstrucParam(mstrConn, mstrTabla, pstrId, "@tipo='C'")
        mdsDatos.Tables(0).TableName = mstrTabla
        Session(mstrTabla) = mdsDatos
    End Sub

#End Region

#Region "Operacion Sobre la Grilla"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.CurrentPageIndex = E.NewPageIndex
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mCargarDatos(E.Item.Cells(Columnas.Id).Text)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

    Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    Private Sub mCargarDatosSocio(ByVal pintSoci As Integer)
        Dim ldsDatos As DataSet
        Try
            mstrCmd = "exec socio_cate_esta_consul " & pintSoci & ",'C'"
            ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
            If ldsDatos.Tables(0).Rows.Count <> 0 Then
                With ldsDatos.Tables(0).Rows(0)
                    lblCategoriaAnterior.Text = .Item("categoria")
                    lblFechaAnterior.Text = .Item("cate_fecha")
                End With
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mConsultar()
        Try
            Dim lstrCmd As New StringBuilder
            lstrCmd.Append("exec " + mstrTabla + "_consul ")
            lstrCmd.Append(" @esta = " + cmbEsta.Valor)
            lstrCmd.Append(",@tipo = 'C'")
            If Not usrSociFil.Valor Is System.DBNull.Value Then
                lstrCmd.Append(", @socm_soci_id=" + clsSQLServer.gFormatArg(usrSociFil.Valor, SqlDbType.Int))
            End If

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiar()
        hdnId.Text = ""
        txtEsta.Text = ""
        txtFecha.Fecha = Now
        txtMoti.Text = ""
        lblFechaAnterior.Text = ""
        lblCategoriaAnterior.Text = ""
        cmbCate.Limpiar()

        cmbAuto.Limpiar()
        mSetearCate()
        usrSoci.Valor = 0
        mSetearEditor(True, False)
    End Sub

    Private Sub mSetearEditor(ByVal pbooAlta As Boolean, ByVal pbooAuto As Boolean)
        btnBaja.Enabled = Not (pbooAlta) And Not (pbooAuto)
        btnModi.Enabled = Not (pbooAlta) And Not (pbooAuto)
        btnAlta.Enabled = pbooAlta
    End Sub

    Private Sub mAgregar()
        mLimpiar()
        usrSoci.Valor = usrSociFil.Valor
        If Not usrSoci.Valor Is DBNull.Value Then
            mCargarDatosSocio(usrSoci.Valor)
        End If
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiarFiltros()
        usrSociFil.Valor = 0
        cmbEsta.SelectedIndex = 0
    End Sub

    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        Try
            grdDato.CurrentPageIndex = 0
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub btnAlta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnLimp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub

End Class
End Namespace
