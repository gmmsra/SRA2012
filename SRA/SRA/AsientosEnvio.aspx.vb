Imports System.Diagnostics

Namespace SRA

    Partial Class AsientosEnvio
        Inherits FormGenerico

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        '  Protected WithEvents lblDesdeFecha As System.Web.UI.WebControls.Label
        'Protected WithEvents txtDesdeFecha As NixorControls.DateBox


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Definición de Variables"
        Private mstrTabla As String = SRA_Neg.Constantes.gTab_AsientosEnvios
        Private mstrTablaCabe As String = SRA_Neg.Constantes.gTab_AsientosEnviosCabe
        Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_AsientosEnviosDeta

        Private mstrConn As String
        Private mstrParaPageSize As Integer

        Private mdsDatos As DataSet

        Private Enum ColumnasDeta As Integer
            Id = 0
        End Enum
#End Region

#Region "Inicialización de Variables"
        Private Sub mInicializar()
            Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
            grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
            grdAsec.PageSize = Convert.ToInt32(mstrParaPageSize)
            grdAsed.PageSize = Convert.ToInt32(mstrParaPageSize)
        End Sub
#End Region

#Region "Operaciones sobre la Pagina"
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                mstrConn = clsWeb.gVerificarConexion(Me)
                mInicializar()
                If (Not Page.IsPostBack) Then
                    Session(mstrTabla) = Nothing
                    mdsDatos = Nothing

                    mSetearEventos()
                    txtAnioFil.Valor = Today.Year

                    mConsultar()
                    clsWeb.gInicializarControles(Me, mstrConn)
                Else
                    mdsDatos = Session(mstrTabla)
                End If

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mSetearEventos()
            btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
            btnEnviar.Attributes.Add("onclick", "if(!confirm('Confirma el envío del registro?')) return false;")
        End Sub
#End Region

#Region "Seteo de Controles"
        Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, Optional ByVal pbooCerrado As Boolean = False)
            Select Case pstrTabla
                Case Else
                    btnBaja.Enabled = Not (pbooAlta) And Not pbooCerrado
                    btnModi.Enabled = Not (pbooAlta) And Not pbooCerrado
                    btnEnviar.Enabled = Not (pbooAlta) And Not pbooCerrado
                    btnAlta.Enabled = pbooAlta And Not pbooCerrado
                    btnList.Visible = Not pbooAlta

                    btnAltaAsecTodos.Enabled = Not pbooCerrado

                    btnAltaAsecTodos.Text = IIf(pbooAlta, "Generar", "Regenerar")
            End Select
        End Sub

        Public Sub mCargarDatos(ByVal pstrId As String)

            Dim lbooEnviado As Boolean

            mCrearDataSet(pstrId)

            With mdsDatos.Tables(mstrTabla).Rows(0)
                hdnId.Text = .Item("asen_id").ToString()

                'txtDesdeFecha.Fecha = .Item("asen_desde_fecha")
                txtHastaFecha.Fecha = .Item("asen_hasta_fecha")
                If .Item("asen_envi_fecha").ToString.Length > 0 Then
                    txtEnviFecha.Fecha = .Item("asen_envi_fecha")
                End If

                lbooEnviado = Not .IsNull("asen_envi_fecha")
            End With

            lblTitu.Text = "Registro Seleccionado: AL " + CDate(txtHastaFecha.Fecha).ToString("dd/MM/yyyy")

            mSetearEditor("", False, lbooEnviado)
            mMostrarPanel(True)
        End Sub

        Private Sub mAgregar()
            mLimpiar()
            btnBaja.Enabled = False
            btnModi.Enabled = False
            btnEnviar.Enabled = False
            mMostrarPanel(True)
        End Sub

        Private Sub mLimpiar()
            hdnId.Text = ""
            hdnAsecId.Text = ""

            lblTitu.Text = ""
            'txtDesdeFecha.Text = ""
            txtHastaFecha.Text = ""

            txtEnviFecha.Text = ""

            lblBaja.Text = ""

            btnAltaAsecTodos.Text = "Generar"

            grdAsec.CurrentPageIndex = 0
            grdAsed.CurrentPageIndex = 0

            grdAsed.Visible = False

            mCrearDataSet("")

            mSetearEditor("", True, False)
        End Sub

        Private Sub mCerrar()
            mLimpiar()
            mConsultar()
        End Sub

        Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
            Dim lbooVisiOri As Boolean = panDato.Visible

            panDato.Visible = pbooVisi
            panBotones.Visible = pbooVisi
            panFiltro.Visible = Not panDato.Visible
            btnAgre.Visible = Not panDato.Visible
            grdDato.Visible = Not panDato.Visible

            If pbooVisi Then
                grdDato.DataSource = Nothing
                grdDato.DataBind()

            Else
                Session(mstrTabla) = Nothing
                mdsDatos = Nothing
            End If

            If lbooVisiOri And Not pbooVisi Then
                txtAnioFil.Valor = Today.Year
            End If
        End Sub
#End Region

#Region "Opciones de ABM"
        Private Sub mAlta()
            Try
                mGuardarDatos(False)

                Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
                lobjGenerico.Alta()

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mModi()
            Try
                mGuardarDatos(False)

                Dim ldsTmp As DataSet = mdsDatos.Copy

                Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTmp)
                lobjGenerico.Modi()

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mEnviar()
            Try
                txtEnviFecha.Fecha = Today
                mGuardarDatos(False)

                Dim ldsTmp As DataSet = mdsDatos.Copy

                Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTmp)
                lobjGenerico.Modi()

                mListar()



            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub mBaja()
            Try
                Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
                lobjGenerico.Baja(hdnId.Text)

                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mGuardarDatos(ByVal pbooProcesar As Boolean)
            mValidarDatos()

            With mdsDatos.Tables(mstrTabla).Rows(0)
                .Item("asen_desde_fecha") = CDate("01/01/1900")
                .Item("asen_hasta_fecha") = txtHastaFecha.Fecha
                If txtEnviFecha.Fecha.Length > 0 Then
                    .Item("asen_envi_fecha") = txtEnviFecha.Fecha
                End If
                .Item("procesar") = pbooProcesar
            End With
        End Sub

        Private Sub mValidarDatos()
            clsWeb.gInicializarControles(Me, mstrConn)
            clsWeb.gValidarControles(Me)
        End Sub

        Public Sub mCrearDataSet(ByVal pstrId As String)
            mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

            mdsDatos.Tables(0).TableName = mstrTabla
            mdsDatos.Tables(1).TableName = mstrTablaCabe

            With mdsDatos.Tables(mstrTabla).Rows(0)
                If .IsNull("asen_id") Then
                    .Item("asen_id") = -1
                End If
            End With

            mConsultarCabe(pstrId)
            Session(mstrTabla) = mdsDatos
        End Sub

        Public Sub mConsultarCabe(ByVal pstrId As String)
            With mdsDatos.Tables(mstrTablaCabe)
                .DefaultView.RowFilter = ""
                '.DefaultView.Sort = "asec_asiento"
                grdAsec.DataSource = .DefaultView
                grdAsec.DataBind()
            End With

            grdAsec.Visible = pstrId <> ""
            grdAsed.Visible = False
        End Sub

        Public Sub mConsultarDeta()
            Dim lstrCmd As New StringBuilder

            lstrCmd.Append("exec " + mstrTablaDeta + "_consul")
            lstrCmd.Append(" @ased_asec_id=" + clsSQLServer.gFormatArg(hdnAsecId.Text, SqlDbType.Int))

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdAsed)

            grdAsed.Visible = True
        End Sub

        Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
            mAlta()
        End Sub

        Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
            mBaja()
        End Sub

        Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
            mModi()
        End Sub

        Private Sub btnEnviar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnviar.Click
            Try
                mEnviar()
            Catch ex As Exception

            Finally
                If (Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings("IntgrAsicentosFinneg"))) Then
                    Dim programa As String = System.Configuration.ConfigurationSettings.AppSettings("IntegracionAsicentosFinneg")
                    Dim proseso As New Process()
                    proseso.StartInfo.FileName = programa
                    proseso.Start()
                End If
            End Try
        End Sub

        Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
            mLimpiar()
        End Sub

        Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
            mCerrar()
        End Sub

        Private Sub txtAnioFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnioFil.TextChanged
            Try
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mConsultar()
            Try
                Dim lstrCmd As New StringBuilder
                lstrCmd.Append("exec " + mstrTabla + "_consul")
                lstrCmd.Append(" @anio=" + clsSQLServer.gFormatArg(txtAnioFil.Text, SqlDbType.Int))

                clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

                mMostrarPanel(False)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
#End Region

#Region "Operacion Sobre la Grilla"
        Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdDato.CurrentPageIndex = E.NewPageIndex
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            Try
                mCargarDatos(e.Item.Cells(1).Text)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdAsec_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdAsec.CurrentPageIndex = E.NewPageIndex
                mConsultarCabe(hdnId.Text)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub grdAsed_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
            Try
                grdAsed.CurrentPageIndex = E.NewPageIndex
                mConsultarDeta()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub
#End Region

        Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
            Try
                grdDato.CurrentPageIndex = 0
                mConsultar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
            Try
                mAgregar()
            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub btnAltaAsecTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaAsecTodos.Click
            Try
                mGuardarDatos(True)
                Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)

                If hdnId.Text = "" Then
                    hdnId.Text = lobjGenerico.Alta
                Else
                    lobjGenerico.Modi()
                End If

                mCargarDatos(hdnId.Text)

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosAsec(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
            Try
                hdnAsecId.Text = e.Item.Cells(1).Text
                mConsultarDeta()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub


        Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
            Try
                mListar()

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Private Sub mListar()
            Dim lstrRptName As String = "AsientosEnvios"
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&asen_id=" + hdnId.Text
            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)
        End Sub
    End Class
End Namespace
