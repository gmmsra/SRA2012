<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CobranzasFact_Pop" CodeFile="CobranzasFact_Pop.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<META http-equiv="Content-Type" content="text/html; charset=windows-1252">
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<!-- Roxi: Modificaci�n 04/12/2009
			 Se agrego el include valDecimal.js porque la funcion gRedondear daba error por no estar definida.
		 -->
		<script language="JavaScript" src="includes/valDecimal.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" onunload="gCerrarVentanas();"
		onload="ChequearConf();">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!-----------------CONTENIDO---------------------->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
							<TR>
								<TD vAlign="top" width="100%" align="right" colspan="6">
								<asp:ImageButton id="imgClose" runat="server" ImageUrl="imagenes\Close3.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
							</TR>
							<TR>
								<TD align="right" width="10%">
									<asp:Label id="lblClie" runat="server" cssclass="titulo">Cliente:</asp:Label></TD>
								<TD colspan="5">
									<UC1:CLIE id="usrClie" runat="server" width="100%" FilCUIT="True" Tabla="Clientes" Saltos="1,1,1"
										autopostback="true" AceptaNull="False" FilClieNume="True" FilSociNume="True" MuestraDesc="False"
										FilDocuNume="True" FilClaveUnica="True"></UC1:CLIE></TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top" colspan="6"><asp:datagrid id="grdConsulta" runat="server" AutoGenerateColumns="False" ItemStyle-Height="5px"
										PageSize="100" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
										BorderWidth="1px" AllowPaging="True" PagerStyle-CssClass="titulo" BorderStyle="None" width="100%">
										<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
										<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle CssClass="item2"></ItemStyle>
										<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
										<FooterStyle CssClass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="3%"></HeaderStyle>
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" Runat="server" onclick="chkSelCheck(this);"></asp:CheckBox>
													<DIV runat="server" id="divTotal" style="DISPLAY: none"><%#(100 *(DataBinder.Eval(Container, "DataItem.comp_neto")+ DataBinder.Eval(Container, "DataItem.interes") + DataBinder.Eval(Container, "DataItem.SobreTasasAlVto")))%></DIV>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="comp_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="covt_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="clie_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="acti_ccos_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="numero" HeaderText="Comprobante"></asp:BoundColumn>
											<asp:BoundColumn DataField="cliente" HeaderText="Cliente"></asp:BoundColumn>
											<asp:BoundColumn DataField="acti_desc" HeaderText="Actividad"></asp:BoundColumn>
											<asp:BoundColumn DataField="covt_fecha" HeaderText="Vto." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="comp_neto" HeaderText="Importe" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="SobreTasasAlVto" HeaderText="Adic Sobre Tasa Vto" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:BoundColumn DataField="interes" HeaderText="Inter�s" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
											<asp:TemplateColumn HeaderText="Pagado">
												<HeaderStyle Width="50px"></HeaderStyle>
												<ItemTemplate>
													<CC1:NUMBERBOX id="txtPagado" runat="server" cssclass="cuadrotexto" Width="50px" EsDecimal="True"
														onchange="txtPagado_change(this);"></CC1:NUMBERBOX>
													<DIV runat="server" id="divActiPermitePagoParciales" style="DISPLAY: none"><%# DataBinder.Eval(Container, "DataItem.acti_PermitePagoParciales")%></DIV>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="comp_acti_id"></asp:BoundColumn>
											<asp:BoundColumn Visible="False" DataField="acti_PermitePagoParciales"></asp:BoundColumn>
											<asp:TemplateColumn>
												<HeaderStyle Width="1%"></HeaderStyle>
												<ItemTemplate>
													<A href="javascript:mAbrirComp(<%#DataBinder.Eval(Container, "DataItem.comp_coti_id")%>,<%#DataBinder.Eval(Container, "DataItem.comp_id")%>);">
													<img src='imagenes/edit.gif' border="0" alt="Ver Comprobante" style="cursor:hand;" />
													</A>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD align="right">
									<asp:Label id="lblTotal" runat="server" cssclass="titulo">Total Facturas:</asp:Label>&nbsp;</TD>
								<TD>
									<CC1:NUMBERBOX id="txtTotal" runat="server" cssclass="cuadrotextodeshab" Enabled="False" Width="65px"></CC1:NUMBERBOX></TD>
								<TD align="right">
									<asp:Label id="lblTotalSel" runat="server" cssclass="titulo">Total Seleccionado:</asp:Label>&nbsp;</TD>
								<TD>
									<CC1:NUMBERBOX id="txtTotalSel" runat="server" cssclass="cuadrotextodeshab" Enabled="False" Width="65px"></CC1:NUMBERBOX></TD>
							</TR>
							<TR>
								<TD vAlign="middle" align="right" height="40" colspan="4">
									<asp:button id="btnAceptar" Width="80px" cssclass="boton" runat="server" Text="Aceptar"></asp:button></TD>
							</TR>
						</TABLE>
					<!----------------CONTENIDO-------------->
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnConf" runat="server" AutoPostBack="True"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnTipo" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
	function chkSelCheck(pChk)
	{
		var dTotal=0;
		var lintFil= eval(pChk.id.replace("grdConsulta__ctl","").replace("_chkSel",""));
		var lbooChecked = pChk.checked;
		var oText = document.all(pChk.id.replace("chkSel","txtPagado"));
		var bitActiPermitePagoParciales = document.all(pChk.id.replace("chkSel","divActiPermitePagoParciales"));
		var sImporte;
		
		/* Dario 2013-07-24 se agrega control por fila que determina si la actividad permite
		   el pago parcial si es asi si se checkea se habilita el control txtPagado para poder 
		   ingresar	la cantidad manuelamente, si es false desabilita el control y deja la suma de los valores
		   a pagar en la factura+sobretasas+intereses */
		if(document.all("hdnTipo").value!="F" && bitActiPermitePagoParciales.innerText == 'True')
			ActivarControl(oText, lbooChecked);
		else
			ActivarControl(oText, false);							
		
		if(!lbooChecked)
			oText.value = "0";
		else
		{
			dTotal = eval(document.all("grdConsulta__ctl" + lintFil.toString() + "_divTotal").innerText);
			dTotal = dTotal.toString();
			oText.value = dTotal.substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2, dTotal.length);
            //document.all(pChk.id.replace("chkSel", "txtPagado")).innerText = oText.value;
		}
		
		mCalcularTotales();
	}
	
	function txtPagado_change(pTxt)
	{	
		var sImporte = pTxt.value;
		var iComa;
		
		if(sImporte=="")
			sImporte = "0.00";
			
		sImporte = sImporte.replace(",",".");
		iComa = sImporte.indexOf(".");
		if (iComa==-1)
			sImporte += ".00";
		
		if (iComa==sImporte.length-1)
			sImporte += "00";
			
		if (iComa==sImporte.length-2)
			sImporte += "0";
		pTxt.value = sImporte;

		mCalcularTotales();
	}
	
	function mCalcularTotales()
	{
		var dTotal=0;
		if (document.all("grdConsulta").children(0).children.length > 1)
		{
			for (var fila=3; fila <= document.all("grdConsulta").children(0).children.length; fila++)
			{
				// Dario 2013-11-21 se agrega el catch para que no se rompa cuando viene el check oculto
				// esto se da cuando viene como pop up de info no de operaciones
				try
				{
					if ((document.all("grdConsulta__ctl" + fila.toString() + "_chkSel").checked))
					if (document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value.indexOf('.') == -1)
						document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value = document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value + '.00';
					if (document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value.substring(0,1) == '.')
						document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value = '0' + document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value;
					//dTotal += eval(document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value.replace(".",""));
					dTotal += eval(document.all("grdConsulta__ctl" + fila.toString() + "_txtPagado").value);
				}
				catch(err){}
			}
		}
		dTotal = gRedondear(dTotal,2);
		dTotal = dTotal.toString();
		
		if (dTotal==0)
			document.all("txtTotalSel").value = "0.00"
		else
			document.all("txtTotalSel").value = dTotal;
			//document.all("txtTotalSel").value = dTotal.substring(0, dTotal.length - 2) + "." + dTotal.substring(dTotal.length - 2 , dTotal.length );
		if (document.all("txtTotalSel").value.indexOf('.') == -1)
			document.all("txtTotalSel").value = document.all("txtTotalSel").value + '.00';
	}
	
	function mAbrirComp(pCotiId,pCompId){
		var lstrPagina;
		if(pCotiId==30||pCotiId==100)
			lstrPagina = "CobranzasAnul.aspx?comp_id=";
		else
			lstrPagina = "ComprobantesAnul.aspx?comp_id=";
		
			lstrPagina += pCompId;
			gAbrirVentanas(lstrPagina, 15, 700, 500, 50, 50)
	}
	
	function ChequearConf(){
		if(document.all("hdnConf").value=="0")
		{
			var bRet;
			bRet = confirm('El cliente de la cobranza y el de las facturas no coincide, confirma la selecci�n?');

			if(bRet)
				document.all("hdnConf").value ="-1";
			else
				document.all("hdnConf").value ="00";
			
			__doPostBack('hdnConf','');
		}
	}
	mCalcularTotales();
        </SCRIPT>
	</BODY>
</HTML>
