Namespace SRA

Partial Class rangos_Pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblCentId As System.Web.UI.WebControls.TextBox



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = "cheques"
   Private mstrConn As String
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mConsultar()
         End If
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub mConsultar()
      Dim lstrCmd As New StringBuilder
      Dim ds As DataSet
      Dim ldsDatos As DataSet

      lstrCmd.Append("exec aranceles_rangos_consul")

      ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)
      ldsDatos = Session(mstrTabla)

      grdConsulta.DataSource = ds
      grdConsulta.DataBind()
      ds.Dispose()

      Session("mulPaginas") = grdConsulta.PageCount
   End Sub
#End Region

   Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
      Dim lstrId As String = mGuardarDatos()
      Try

         Dim lsbMsg As New StringBuilder
         lsbMsg.Append("<SCRIPT language='javascript'>")
         lsbMsg.Append("window.opener.document.all['hdnDatosPop'].value='")
         lsbMsg.Append(lstrId & "';")
         lsbMsg.Append("window.opener.__doPostBack('hdnDatosPop','');")
         lsbMsg.Append("window.close();")
         lsbMsg.Append("</SCRIPT>")
         Response.Write(lsbMsg.ToString)

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub
   Private Function mGuardarDatos() As String
      Dim lstrId As New StringBuilder

      For Each oDataItem As DataGridItem In grdConsulta.Items
         If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Then
            If lstrId.Length > 0 Then lstrId.Append(",")
            lstrId.Append(oDataItem.Cells(1).Text)
         End If
      Next

      Return (lstrId.ToString)
   End Function

End Class

End Namespace
