Imports System.Data.SqlClient


Namespace SRA


Partial Class consulta_arbol
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

   'El Dise�ador de Web Forms requiere esta llamada.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
Protected WithEvents btnExpa As System.Web.UI.HtmlControls.HtmlButton
Protected WithEvents btnCola As System.Web.UI.HtmlControls.HtmlButton

   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region


#Region "Definici�n de Variables"
   Public mstrCmd As String
   Public mstrTabla As String
   Public mstrTitulo As String
   Public mstrFiltros As String
   Private mstrConn As String
   Private mbooEsConsul As Boolean

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
      Catch ex As Exception

         clsError.gManejarError(Me, ex)
      End Try
   End Sub

#End Region

#Region "Inicializacion de Variables"

   Public Sub mInicializar()
      mstrTabla = Request.QueryString("tabla")
      mstrTitulo = Request.QueryString("titulo")
      mstrFiltros = Request.QueryString("filtros")
      lblTituAbm.Text = mstrTitulo
      btnAbrir.Attributes.Add("onclick", "javacript:gAbrirArbol();return(false);")
      btnCerrar.Attributes.Add("onclick", "javacript:gCerrarArbol();return(false);")
   End Sub

#End Region

   Private Sub Imagebutton1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Imagebutton1.Click
      Response.Write("<Script>window.close();</script>")
   End Sub

Public Sub mArmarArbolPedigree()

      Try
         Dim mdsDatos As DataSet
         Dim lstrId As String
         Dim lstrOrigen As String
         Dim lstrProd As String
         Dim lstrEspe As String
         Dim lstrSexo As String
         Dim lstrIcono As String
         Dim lstrPadre As String
         Dim lintNivel As Integer = 1
         Dim lintRegi As Integer
         Dim lstrCmd As String = "exec productos_pedigree_busq " & mstrFiltros

         Dim myConnection As New SqlConnection(Session("sConn").ToString())
         Dim cmdExec = New SqlCommand
         myConnection.Open()
         cmdExec.Connection = myConnection
         cmdExec.CommandType = CommandType.Text
         cmdExec.CommandText = lstrCmd
         Dim dr As SqlDataReader = cmdExec.ExecuteReader()
         While (dr.Read())
            lstrId = dr.GetValue(0).ToString().Trim()
            lstrSexo = dr.GetValue(3).ToString().Trim()
            lstrEspe = dr.GetValue(4).ToString().Trim()
            Select Case lstrEspe
                Case SRA_Neg.Constantes.Especies.Bovinos
                    If lstrSexo = "0" Then
                        lstrIcono = "<img border=0 src=images/especies/vaca.gif>"
                    Else
                        lstrIcono = "<img border=0 src=images/especies/toro.gif>"
                    End If
                Case SRA_Neg.Constantes.Especies.Camelidos
                    lstrIcono = "<img border=0 src=images/especies/camello" & lstrSexo & ".gif>"
                Case SRA_Neg.Constantes.Especies.Caprinos
                    lstrIcono = "<img border=0 src=images/especies/cabra" & lstrSexo & ".gif>"
                Case SRA_Neg.Constantes.Especies.Equinos
                    lstrIcono = "<img border=0 src=images/especies/caballo" & lstrSexo & ".gif>"
                Case SRA_Neg.Constantes.Especies.Ovinos
                    lstrIcono = "<img border=0 src=images/especies/oveja" & lstrSexo & ".gif>"
                Case Else
                    lstrIcono = ""
            End Select
            lstrProd = lstrIcono & "&nbsp;&nbsp;<span class=desc>" & dr.GetValue(1).ToString().Trim() & "</span>"
            lstrPadre = dr.GetValue(2).ToString().Trim()
            If lstrPadre = -1 Then
                lstrOrigen = lstrId
                Response.Write(" foldersTree = gFld('" & lstrProd & "', '');")
                Response.Write(" foldersTree.treeID = 'Frameset';")
            Else
                If lstrPadre = lstrOrigen Then
                    Response.Write(" aux" & lstrId & " = insFld(foldersTree, gFld('" & lstrProd & "', '')); ")
                Else
                   Response.Write(" aux" & lstrId & " = insFld(aux" & lstrPadre & ", gFld('" & lstrProd & "', '')); ")
                End If
            End If
            lintNivel = lintNivel + 1
         End While
         dr.Close()
         myConnection.Close()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try

End Sub

End Class

End Namespace
