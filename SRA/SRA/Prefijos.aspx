<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Prefijos" CodeFile="Prefijos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Prefijos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<SCRIPT language="javascript">
		function expandir()
		{
		 try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</SCRIPT>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Prefijos</asp:label></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" BorderWidth="0" BorderStyle="Solid"
												Width="100%" Visible="True">
												<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
													<TR>
														<TD>
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																			ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																			ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																			IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="10"></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="4"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD style="HEIGHT: 50px">
															<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																<TR>
																	<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																	<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																		<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 10%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblCriadorFil" runat="server" cssclass="titulo">Raza/Criador:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrCriaFil" runat="server" Tabla="Criadores" Saltos="1,1,1" FilDocuNume="True"
																						Ancho="780" FilCUIT="True" MuestraDesc="False" FilClie="True" Alto="560" AceptaNull="false"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 10%; HEIGHT: 10px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblClienteFil" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrClieFil" runat="server" Saltos="1,1,1,1" Ancho="800" MuestraDesc="False"
																						AceptaNull="false" FilFanta="true" tabla="clientes"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 10%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right">
																					<asp:Label id="lblPrefijoFil" runat="server" cssclass="titulo">Prefijo:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<cc1:TextBoxtab id="txtPrefijoFil" runat="server" cssclass="cuadrotexto" Width="100px"></cc1:TextBoxtab>
																					<asp:checkbox id="chkBuscFil" CssClass="titulo" Runat="server" Text="Buscar en..."></asp:checkbox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 10%; HEIGHT: 17px" background="imagenes/formfdofields.jpg" align="right"></TD>
																				<TD style="WIDTH: 90%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																					<asp:CheckBox id="chkTodos" runat="server" cssclass="titulo" Text="Todos" Font-Size="XX-Small"
																						Height="8px"></asp:CheckBox></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD style="HEIGHT: 17px" background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="100%" AllowPaging="True"
												HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
												AutoGenerateColumns="False">
												<FooterStyle CssClass="footer"></FooterStyle>
												<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
												<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="prca_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="_clienume" ReadOnly="True" HeaderText="N�" HeaderStyle-HorizontalAlign="Center"></asp:BoundColumn>
													<asp:BoundColumn DataField="_cliente" ReadOnly="True" HeaderText="Cliente">
														<HeaderStyle Width="45%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_raza" ReadOnly="True" HeaderText="Raza">
														<HeaderStyle Width="20%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_criador" ReadOnly="True" HeaderText="N�Criador" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
													<asp:BoundColumn DataField="_prefijo" ReadOnly="True" HeaderText="Prefijo">
														<HeaderStyle Width="20%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="prca_acep_fecha" ReadOnly="True" HeaderText="Aprobaci�n" ItemStyle-HorizontalAlign="Center"
														DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
													<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado">
														<HeaderStyle Width="15%"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="prca_baja_fecha" ReadOnly="True" HeaderText="Fec.Baja" ItemStyle-HorizontalAlign="Center"
														DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD colspan="2" align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
												ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
												BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Argerar un Nuevo Registro" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
										<TD align="right"><CC1:BOTONIMAGEN id="btnListar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnImpr2.gif"
												ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
												BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Listar"></CC1:BOTONIMAGEN></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<TR>
										<TD align="center" colSpan="3"><asp:panel id="panSolapas" runat="server" cssclass="titulo" Visible="False" Height="124%" width="100%">
												<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
													<TR>
														<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
														<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkGeneral" runat="server"
																cssclass="solapa" Width="80px" Height="21px" Font-Bold="True" CausesValidation="False"> General</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
														<TD background="imagenes/tab_fondo.bmp" width="1">
															<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkPrefijos" runat="server"
																cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Prefijos</asp:linkbutton></TD>
														<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD vAlign="top" align="center" colSpan="3"><asp:panel id="panDato" runat="server" cssclass="titulo" Visible="False" Height="116px" width="100%">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD style="WIDTH: 100%" vAlign="top" align="right">
															<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
													</TR>
													<TR>
														<TD height="2" background="imagenes/formdivmed.jpg" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TablaGeneralPrefijos" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 15%; HEIGHT: 21px" vAlign="middle" align="right">
																			<asp:Label id="lblFechaPrefijos" runat="server" cssclass="titulo">Fecha Alta:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"
																				obligatorio="true"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 21px" vAlign="middle" align="right">
																			<asp:Label id="lblCria" runat="server" cssclass="titulo">Criador:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<UC1:CLIE id="usrCria" runat="server" Tabla="Criadores" Saltos="1,1,1" FilDocuNume="True"
																				Ancho="780" FilCUIT="True" MuestraDesc="False" FilClie="True" Alto="560"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 21px" vAlign="middle" align="right">
																			<asp:Label id="lblCliente" runat="server" cssclass="titulo">Cliente:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<UC1:CLIE id="usrClie" runat="server" Saltos="1,1,1,1" Ancho="800" MuestraDesc="False" AceptaNull="false"
																				FilFanta="true" tabla="clientes"></UC1:CLIE></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 21px" vAlign="middle" align="right">
																			<asp:Label id="lblFechaAprob" runat="server" cssclass="titulo">Fecha Aprobaci�n:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:DateBox id="txtFechaAprob" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 21px" vAlign="middle" align="right">
																			<asp:CheckBox id="chkVigTresA" runat="server" cssclass="titulo" Text="Vigencia tres A�os:" onclick="VigTresA()"></asp:CheckBox>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:DateBox id="txtFechaVigenc" runat="server" cssclass="cuadrotexto" Width="70px" AceptaNull="True"></cc1:DateBox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="HEIGHT: 21px" vAlign="middle" align="right">
																			<asp:Label id="lblActa" runat="server" cssclass="titulo">Acta:</asp:Label>&nbsp;</TD>
																		<TD vAlign="middle">
																			<cc1:TEXTBOXTAB id="txtActa" runat="server" cssclass="cuadrotexto" Width="200px"></cc1:TEXTBOXTAB></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%">
															<asp:panel id="panPrefijos" runat="server" cssclass="titulo" Visible="False" Width="100%">
																<TABLE style="WIDTH: 100%" id="TablaPrefijos" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD vAlign="top" colSpan="2" align="center">
																			<asp:datagrid id="grdPrefijos" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
																				OnEditCommand="mEditarDatosPref" OnPageIndexChanged="DataGridPref_Page" CellPadding="1" GridLines="None"
																				CellSpacing="1" HorizontalAlign="Center" AllowPaging="True" width="100%" PageSize="10">
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																				<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																				<Columns>
																					<asp:TemplateColumn>
																						<HeaderStyle Width="2%"></HeaderStyle>
																						<ItemTemplate>
																							<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																								<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																							</asp:LinkButton>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:BoundColumn Visible="False" DataField="pref_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="pref_orden" ReadOnly="True" HeaderText="Orden"></asp:BoundColumn>
																					<asp:BoundColumn DataField="pref_desc" ReadOnly="True" HeaderText="Prefijo">
																						<HeaderStyle Width="35%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="pref_sexo" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_sexo" ReadOnly="True" HeaderText="Sexo">
																						<HeaderStyle Width="20%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="pref_baja_fecha" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="pref_baja_fecha" ReadOnly="True" HeaderText="Fec.Baja" DataFormatString="{0:dd/MM/yyyy}">
																						<HeaderStyle Width="20%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="pref_esta_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_estado" ReadOnly="True" HeaderText="Estado">
																						<HeaderStyle Width="20%"></HeaderStyle>
																					</asp:BoundColumn>
																					<asp:BoundColumn Visible="False" DataField="_OrdenPorSistema" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblOrdenPref" runat="server" cssclass="titulo">Orden:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:numberbox id="txtOrdenPref" runat="server" cssclass="cuadrotexto" Width="50px"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblSexoPref" runat="server" cssclass="titulo">Sexo:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:combobox id="cmbSexoPref" class="combo" runat="server" Width="100px">
																				<asp:ListItem Value="" Selected="True">Ambos</asp:ListItem>
																				<asp:ListItem Value="1">Masculino</asp:ListItem>
																				<asp:ListItem Value="0">Femenino</asp:ListItem>
																			</cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblDescPref" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:textboxtab id="txtDescPref" runat="server" cssclass="cuadrotexto" Width="50%"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblDescAmplPref" runat="server" cssclass="titulo">Descripci�n Ampliada:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:textboxtab id="txtDescAmplPref" runat="server" cssclass="textolibre" Width="90%" TextMode="MultiLine"
																				Rows="5" height="50px"></cc1:textboxtab></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%" vAlign="top" align="right">
																			<asp:Label id="lblRazaPref" runat="server" cssclass="titulo">Raza:</asp:Label>&nbsp;</TD>
																		<TD vAlign="top">
																			<cc1:combobox id="cmbRazaPref" class="combo" runat="server" cssclass="cuadrotexto" Width="320px"
																				Height="20px" MostrarBotones="False" filtra="true" NomOper="razas_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 20%; HEIGHT: 109px" vAlign="top" align="right">
																			<asp:Label id="lblEstadoPref" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																		<TD style="HEIGHT: 109px" vAlign="top">
																			<cc1:combobox id="cmbEstadoPref" class="combo" runat="server" Width="208px" AceptaNull="False"
																				NomOper="estados_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD colSpan="2" align="center">
																			<asp:Label id="lblBajaPref" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																	</TR>
																	<TR height="30">
																		<TD colSpan="2" align="center">
																			<asp:Button id="btnAltaPref" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>
																			<asp:Button id="btnBajaPref" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>
																			<asp:Button id="btnModiPref" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>
																			<asp:Button id="btnLimpPref" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL>
											<DIV></DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnPrefId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
			
			function usrClie_onchange()
		{
			var lstrClie = document.all('usrClie:txtId').value;
			document.all('usrCria:txtId').value = '';
			document.all('usrCria:txtCodi').value = '';
			document.all('usrCria:txtApel').value = '';
	
		}	
		function usrCria_onchange()
		{
			var lstrCria = document.all('usrCria:txtId').value;
			document.all('usrClie:txtId').value = '';
			document.all('usrClie:txtCodi').value = '';
			document.all('usrClie:txtApel').value = '';
			document.all('usrClie:txtDesc').value = '';
		}		
		
		function VigTresA()
		{
			var chk = document.all('chkVigTresA').checked;
			
			if(chk)
			{
				//document.all('txtFechaVigenc').disabled = false;
				if(document.getElementById("txtFechaAprob").value != '')
				{
					var inicio= document.getElementById("txtFechaAprob").value;
					var vec = inicio.split('/');
					var yar = parseInt(vec[2]) + 3;
					
					var fechaFin = vec[0] +'/'+ vec[1] +'/'+ yar;
				
					document.getElementById('txtFechaVigenc').value = fechaFin;
				}
			}
			else
			{
				document.all('txtFechaVigenc').value = '';
			}
			return false;
		}
		
		</SCRIPT>
		</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
