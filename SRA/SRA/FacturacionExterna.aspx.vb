Namespace SRA

Partial Class FacturacionExterna
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents btnAgre As NixorControls.BotonImagen

    Protected WithEvents panCriador As System.Web.UI.WebControls.Panel



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        If Page.IsPostBack Then
            For i As Integer = 0 To 1
                Dim dgCol As New Web.UI.WebControls.BoundColumn
                'grdFac.Columns.Add(dgCol)
            Next
        End If
    End Sub

#End Region

#Region "Definición de Variables"

    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
    Private mstrTablaDetaAran As String = SRA_Neg.Constantes.gTab_ComprobAranc
    Private mstrTablaDetaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep
    Private mstrTablaVtos As String = SRA_Neg.Constantes.gTab_ComprobVtos
    Private mstrTablaAutor As String = SRA_Neg.Constantes.gTab_ComprobAutor

    Private mstrConn As String
    Private mdsDatos As DataSet
    Private mstrParaPageSize As Integer
    Private mobj As SRA_Neg.Facturacion

    Private Const constFactura As Integer = 29
    Private Const constNotaCredito As Integer = 31
    Private Const constNotaDebito As Integer = 32

    Private Const mintCtaCte As Integer = 2
    Private Const mintExpoGana As Integer = 7

    Public Enum TiposIVA As Integer
        tasa = 1
        reduc = 2
        sobre = 3

    End Enum

    Private Enum Columnas As Integer
        Incluir = 0
        Id01 = 1
        Id02 = 2
        resul = 3
        tipo = 4
        acti_id = 5
        vto = 10
        BotConceptosVtos = 11
        BotVerObser = 12
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())

            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                mSetearEventos()
                mCargarCombos()
                mobj.CentroEmisorNro(mstrConn)
                clsWeb.gInicializarControles(Me, mstrConn)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Function mCotizacionDolar() As Double
        mobj.CotizaDolar(mstrConn, txtFechaValor.Fecha)
        Return mobj.pCotizaDolar
    End Function
    Private Sub mSetearEventos()
        btngene.Attributes.Add("onclick", "if(!confirm('Confirma la generación de facturas?')) return false; else return(ObtenerImpresora('Factura'))")
    End Sub
#End Region

#Region "Opciones de ABM"

    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDeta
        mdsDatos.Tables(2).TableName = mstrTablaDetaAran
        mdsDatos.Tables(3).TableName = mstrTablaDetaConcep
        mdsDatos.Tables(4).TableName = mstrTablaVtos
        mdsDatos.Tables(5).TableName = mstrTablaAutor

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        Session(mstrTabla) = mdsDatos

    End Sub

    Private Function mGuardarDatos(ByVal pintClave1 As Integer, ByVal pintClave2 As Integer) As DataSet
        Dim ldrDatos As DataRow
        Dim ldsDatos As DataSet
        Dim ldatFechaValor As DateTime
        Dim pboolDiscIva As Boolean
        Dim ldTotalIVA, ldTotalIVAR, ldTotalIVAS As Decimal
        Dim lintTipoTasa As Integer
        Dim ldouImporte As Double
        Dim lstrTipo As String

        mCrearDataSet("")

        'el ds con los datos del comprobante
        'trae solo el registro que se va a grabar. 
        'No guarda el DS que llena la grilla en memoria

        Dim dsCab As DataSet = mobj.CabeFactExterna(mstrConn, pintClave1, pintClave2)
        mdsDatos.Tables(mstrTabla).Select()
        ldrDatos = mdsDatos.Tables(mstrTabla).Rows(0)
        dsCab.Tables(0).Select()
        Dim ldrCabe As DataRow = dsCab.Tables(0).Rows(0)
        With ldrCabe
            ldatFechaValor = IIf(txtFechaValor.Fecha Is DBNull.Value, .Item("f801_fecha"), txtFechaValor.Fecha)
            pboolDiscIva = mobj.ClienteDiscrimIVA(mstrConn, .Item("clie_id"))
            ldrDatos.Item("comp_id") = -1
            ldrDatos.Item("comp_fecha") = ldatFechaValor
            ldrDatos.Item("comp_clie_id") = .Item("clie_id")
            lstrTipo = .Item("f801_tipocomp")
            Select Case lstrTipo
                Case "1"    'FC 
                    ldrDatos.Item("comp_dh") = 0 ' debe 
                    ldrDatos.Item("comp_coti_id") = constFactura
                Case "2"    'ND
                    ldrDatos.Item("comp_dh") = 0 ' debe 
                    ldrDatos.Item("comp_coti_id") = constNotaDebito
                Case "3"    'NC
                    ldrDatos.Item("comp_dh") = 1 ' haber
                    ldrDatos.Item("comp_coti_id") = constNotaCredito
            End Select
            ldrDatos.Item("comp_nume") = .Item("f801_nrocomp") ' Dario 2015-07-23
            ldrDatos.Item("comp_neto") = .Item("importe")
            ldouImporte = .Item("importe")
            ldrDatos.Item("comp_cance") = IIf(.Item("importe") = 0, 1, 0)  ' cancelado o no
            ldrDatos.Item("comp_cs") = 1
            ldrDatos.Item("comp_mone_id") = 1 ' tipo de moneda $
            ldrDatos.Item("comp_cemi_nume") = mobj.pCentroEmisorNro
            ldrDatos.Item("comp_emct_id") = mobj.pCentroEmisorId
            mobj.LetraComprob(mstrConn, mobj.ClienteTipoIva(mstrConn, .Item("clie_id")))
            ldrDatos.Item("comp_letra") = mobj.pLetraComprob
            ldrDatos.Item("comp_cpti_id") = mintCtaCte
            ldrDatos.Item("comp_ivar_fecha") = IIf(txtFechaValor.Fecha Is DBNull.Value, .Item("f801_fecha"), txtFechaValor.Fecha)
            ldrDatos.Item("comp_acti_id") = .Item("acti_id")
            ldrDatos.Item("comp_impre") = 0 ' impresa o no
            ldrDatos.Item("comp_host") = mobj.pHost  ' nombre del host
            ldrDatos.Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.FacturacionExterna
        End With

        ' conceptos
        Dim dsC As DataSet = mobj.ConceptosFactExterna(mstrConn, pintClave1, pintClave2)
        For Each dr As DataRow In dsC.Tables(0).Select
            With dr
                ldrDatos = mdsDatos.Tables(mstrTablaDetaConcep).NewRow()
                ldrDatos.Item("coco_id") = -1
                ldrDatos.Item("coco_conc_id") = .Item("conc_id")
                ldrDatos.Item("coco_ccos_id") = .Item("ccos_id")
                ldrDatos.Item("coco_tasa_iva") = mobj.ObtenerValorCampo(mstrConn, "conceptosX", "valor_tasa", .Item("conc_id").ToString + "," + IIf(txtFechaValor.Fecha Is DBNull.Value, clsSQLServer.gFormatArg(ldatFechaValor, SqlDbType.SmallDateTime), clsSQLServer.gFormatArg(txtFechaValor.Text, SqlDbType.SmallDateTime)))
                lintTipoTasa = mobj.ObtenerValorCampo(mstrConn, "conceptosX", "tipo_tasa", .Item("conc_id").ToString + "," + IIf(txtFechaValor.Fecha Is DBNull.Value, clsSQLServer.gFormatArg(ldatFechaValor, SqlDbType.SmallDateTime), clsSQLServer.gFormatArg(txtFechaValor.Text, SqlDbType.SmallDateTime)))
                If pboolDiscIva Then
                    Dim ldouImporteiva As Double
                    ldouImporteiva = CType(.Item("f802_impitem"), Double) - (CType(.Item("f802_impitem"), Double) / (1 + (ldrDatos.Item("coco_tasa_iva") / 100)))
                    ldrDatos.Item("coco_impo") = CType(.Item("f802_impitem"), Double) - ldouImporteiva
                    Select Case lintTipoTasa
                        Case TiposIVA.tasa
                            ldTotalIVA = ldTotalIVA + ldouImporteiva
                        Case TiposIVA.reduc
                            ldTotalIVAR = ldTotalIVAR + ldouImporteiva
                        Case TiposIVA.sobre
                            ldTotalIVAS = ldTotalIVAS + ldouImporteiva
                    End Select
                Else
                    ldrDatos.Item("coco_impo") = .Item("f802_impitem")
                End If

                ldrDatos.Item("coco_impo_ivai") = .Item("f802_impitem")
                ldrDatos.Item("coco_desc_ampl") = .Item("f802_detalle")
                ldrDatos.Item("coco_auto") = 0
                ldrDatos.Table.Rows.Add(ldrDatos)
            End With
        Next

        mdsDatos.Tables(mstrTablaDeta).Select()
        ldrDatos = mdsDatos.Tables(mstrTablaDeta).NewRow()
        ' guarda los datos de la tabla comprob_deta
        With ldrDatos
            .Item("code_id") = -1
            .Item("code_conv_id") = DBNull.Value
            .Item("code_tarj_id") = DBNull.Value
            .Item("code_tarj_cuot") = DBNull.Value
            .Item("code_tarj_vtat") = DBNull.Value
            .Item("code_coti") = mCotizacionDolar()
            .Item("code_pers") = DBNull.Value
            .Item("code_raza_id") = DBNull.Value
            .Item("code_impo_ivat_tasa") = ldTotalIVA
            .Item("code_impo_ivat_tasa_redu") = ldTotalIVAR
            .Item("code_impo_ivat_tasa_sobre") = ldTotalIVAS
            .Item("code_obse") = DBNull.Value
            .Item("code_reci_nyap") = DBNull.Value
            .Item("code_sistgen") = pintClave1
            .Item("code_opergen") = pintClave2
            .Table.Rows.Add(ldrDatos)
        End With

        ' Vtos
        Dim dsV As DataSet = mobj.VtosFactExterna(mstrConn, pintClave1, pintClave2)
        'no tiene cargado vtos
        If dsV.Tables(0).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTablaVtos).Select()
            ldrDatos = mdsDatos.Tables(mstrTablaVtos).NewRow()
            ldrDatos.Item("covt_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaVtos), "covt_id")

            Dim ldatFecha As Date
            ldatFecha = CDate(txtFechaValor.Fecha).AddMonths(1)
            ldatFecha = New Date(ldatFecha.Year, ldatFecha.Month, mobj.DiasVtoFactura(mstrConn))
            With ldrDatos
                .Item("covt_fecha") = ldatFecha
                .Item("covt_porc") = 0
                .Item("covt_impo") = ldouImporte
                .Item("covt_cance") = IIf(ldouImporte = 0, 1, 0)
            End With
        Else
            For Each dr As DataRow In dsV.Tables(0).Select
                With dr
                    ldrDatos = mdsDatos.Tables(mstrTablaVtos).NewRow()
                    ldrDatos.Item("covt_id") = -1
                    ldrDatos.Item("covt_fecha") = .Item("f803_fecvto")
                    ldrDatos.Item("covt_porc") = 0
                    ldrDatos.Item("covt_impo") = .Item("f803_impvto")
                    ldrDatos.Item("covt_cance") = IIf(.Item("f803_impvto") = 0, 1, 0)
                    ldrDatos.Table.Rows.Add(ldrDatos)
                End With
            Next
        End If
        Return mdsDatos

    End Function

    Private Sub mValidarDatos()
        Dim lbooSelec As Boolean
        If Not txtFechaValor.Fecha Is DBNull.Value Then
            'valida que la fecha valor ingresada no sea mayor a la de vto
            For Each oItem As DataGridItem In grdFac.Items
                lbooSelec = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked
                If lbooSelec Then
                    ' If txtFechaValor.Fecha > clsSQLServer.gFormatArg(oItem.Cells(Columnas.vto).Text(), SqlDbType.SmallDateTime) Then
                    If txtFechaValor.Fecha > clsFormatear.gFormatFechaDateTime(oItem.Cells(Columnas.vto).Text()) Then
                        Throw New AccesoBD.clsErrNeg("La fecha Valor no puede ser mayor a la fecha del primer vencimiento.")
                    End If
                End If
            Next
        End If
        'solo es facturacion de exposiciones palermo
        'For Each oItem As DataGridItem In grdFac.Items
        '    lbooSelec = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked
        '    If lbooSelec Then
        '        If oItem.Cells(Columnas.acti_id).Text() = mintExpoGana And cmbExpo.Valor Is DBNull.Value Then
        '            Throw New AccesoBD.clsErrNeg("Debe seleccionar la exposición.")
        '        End If

        '    End If
        'Next
        ' esta validacion es por ahora, porque solo es facturacion de exposiciones 
        'If cmbExpo.Valor Is DBNull.Value Then
        'Throw New AccesoBD.clsErrNeg("Debe seleccionar la exposición.")
        'End If
    End Sub

    Private Sub mAlta()
        Dim lDsDatos As DataSet
        Dim lstrId As String
        Dim lbooSelec As Boolean
        Dim lstrReactivIDS, lstrClave As String
        Dim lbooComp As Boolean

        mValidarDatos()

        For Each oItem As DataGridItem In grdFac.Items
            lbooSelec = DirectCast(oItem.FindControl("chkSel"), CheckBox).Checked
            If lbooSelec Then
                lDsDatos = mGuardarDatos(oItem.Cells(Columnas.Id01).Text(), oItem.Cells(Columnas.Id02).Text())

                Dim lobj As New SRA_Neg.FacturacionABM(mstrConn, Session("sUserId").ToString(), mstrTabla, lDsDatos)

                lstrId = lobj.AltaFacExt()

                If clsWeb.gImprimirReporte(mstrConn, Session("sUserId").ToString(), "Factura", hdnImprId.Text, "comp_id", lstrId, Request) <> 0 Then
                    lbooComp = True
                    mActualizarCampoImpresion(lstrId)
                End If
            End If
        Next
        If lbooComp Then
            hdnCompGenera.Text = "Se imprimieron el/los comprobantes"
        End If
    End Sub

    Private Sub mActualizarCampoImpresion(ByVal pintID)
        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pintID)

        With mdsDatos.Tables(0)
            .TableName = mstrTabla
            .Rows(0).Item("comp_impre") = True
        End With

        While mdsDatos.Tables.Count > 1
            mdsDatos.Tables.Remove(mdsDatos.Tables(1))
        End While

        Dim lobj As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

        lobj.Modi()
    End Sub
#End Region

#Region "Operacion Sobre la Grilla"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdFac.EditItemIndex = -1
            If (grdFac.CurrentPageIndex < 0 Or grdFac.CurrentPageIndex >= grdFac.PageCount) Then
                grdFac.CurrentPageIndex = 0
            Else
                grdFac.CurrentPageIndex = E.NewPageIndex
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mMostrarBotonObser()
        For Each oItem As DataGridItem In grdFac.Items
            If oItem.Cells(Columnas.resul).Text() = 0 Then
                oItem.Cells(Columnas.BotVerObser).Visible = False
            Else
                oItem.Cells(Columnas.Incluir).Enabled = False
            End If
        Next
    End Sub
    Private Sub grdFac_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Try
            If e.Item.ItemIndex <> -1 Then
                Dim ochkSel As CheckBox = e.Item.FindControl("chkSel")

                With CType(e.Item.DataItem, DataRowView).Row
                    ochkSel.Checked = .Item("sacd_id") <> 0
                End With
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mChequearGrilla(ByVal pboolTodo As Boolean)
        For Each oDataItem As DataGridItem In grdFac.Items
            If oDataItem.Cells(Columnas.Incluir).Enabled = True Then

                CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = pboolTodo
            End If
        Next
    End Sub
    Private Sub btnNinguno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNinguno.Click
        mChequearGrilla(False)
    End Sub
    Private Sub btnTodos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTodos.Click
        mChequearTodos()
    End Sub
    Private Sub mChequearTodos()
        mChequearGrilla(True)
    End Sub
    Public Sub mConsultar()
        Try
            mLimpiar()
            Dim ldt As DataTable

            Dim res As Integer = mobj.FacExposicionesDTS(mstrConn)
            If res <> 1 Then
                Throw New AccesoBD.clsErrNeg("Se produjo un error al intentar obtener los datos de facturación de exposiciones.")
            End If

            ldt = mobj.FacExposicionesPendientes(mstrConn, usrClieFil.Valor, cmbActi.Valor)

            grdFac.DataSource = ldt
            grdFac.DataBind()

            mMostrarBotonObser()
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "otros"
    Private Sub mLimpiar()
        hdnCompGenera.Text = ""
        txtFechaValor.Text = ""
    End Sub

    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S", " @usua_id=" + Session("sUserId").ToString() + ",@factura = 1 ")
        clsWeb.gCargarRefeCmb(mstrConn, "exposiciones", cmbExpo, "S")
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panFiltro.Visible = pbooVisi
        panDato.Visible = Not panDato.Visible
    End Sub
    Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
        mConsultar()
    End Sub
    Private Sub imgClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mMostrarPanel(True)
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        Try
            cmbActi.Limpiar()
            usrClieFil.Valor = ""
            txtFechaValor.Text = ""
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btngene_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btngene.Click
        Try
            mAlta()
            mMostrarPanel(True)
            mCrearDataSet("")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region


End Class
End Namespace
