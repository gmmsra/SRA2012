<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ComisionesCambios" CodeFile="ComisionesCambios.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Cambio de Comisi�n</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function cmbCarr_change()
		{
			mCargarDivisiones();
		}
	
		function mCargarDivisiones()
		{
			var sFiltro = "@cicl_carr_id=";
			var sValor = document.all["cmbCarr"].value;

			if(sValor == "") 
				sValor = "0";
				
			sFiltro += sValor;
			LoadComboXML("divisiones_cargar", sFiltro, "cmbDestDivi", "S");
						
			sValor = document.all("usrAlum:txtId").value;
			if(sValor == "") 
				sValor = "0";
				
			sFiltro += ",@insc_alum_id=" + sValor;
			LoadComboXML("divisiones_cargar", sFiltro, "cmbOrigDivi", "S");			
		}
		
		function cmbDivi_Change(pstrTipo)
		{
		  mCargarComisiones(pstrTipo);
		}
		
		function mCargarComisiones(pstrTipo)
		{
			var sFiltro = "@divi_id=";
			var sValor = "";
			var sOper="comisiones_cargar";
			
			sValor = document.all["cmb" + pstrTipo + "Divi"].value;
			
			if(sValor == "") 
				sValor = "0";
			
			sFiltro += sValor;
			
			if(pstrTipo == "Orig")
			{
				sValor=document.all["usrAlum:txtId"].value;
			
				if(sValor == "") 
					sValor = "0";
				
				sFiltro += ",@alum_id =" + sValor;
				sOper="comisionesXalumnos_cargar";
			}

			LoadComboXML(sOper, sFiltro, "cmb" + pstrTipo + "Comi", "T");
		}
		
		function usrAlum_onchange()
		{
			mCargarDivisiones();
		}
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" height="25"></TD>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion" width="391px">Cambios de Comisi�n</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD></TD>
										<TD vAlign="top" colSpan="2"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="97%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnBusc" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																			BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																			BtnImage="edit.gif" ImageBoton="btnCons.gif" ImageOver="btnCons2.gif"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			ForeColor="Transparent" BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/"
																			CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnLimp.gif" ImageOver="btnLimp2.gif"></CC1:BotonImagen></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblFechaHastaFil" runat="server" cssclass="titulo">Divisi�n:</asp:Label>&nbsp;</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																					<cc1:combobox class="combo" id="cmbDiviFil" runat="server" Width="500px" NomOper="divisiones_cargar"
																						AceptaNull="false" Obligatorio="True"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 10%" align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																				<TD style="WIDTH: 88%" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 10%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																					<asp:Label id="lblLegajoFil" runat="server" cssclass="titulo">Legajo:</asp:Label>&nbsp;<BR>
																					<asp:Label id="lboAlumFil" runat="server" cssclass="titulo">Alumno:</asp:Label>&nbsp;
																				</TD>
																				<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																					<UC1:CLIE id="usrAlumFil" runat="server" AceptaNull="false" FilCUIT="True"
																						FilDocuNume="True" FilLegaNume="True" Saltos="1,1,1" Tabla="Alumnos"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 10%; HEIGHT: 14px" align="right" background="imagenes/formfdofields.jpg"></TD>
																				<TD style="WIDTH: 88%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 2%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 10%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																				<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<TR>
										<TD colSpan="3" height="10"></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD></TD>
										<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
												OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
												AllowPaging="True">
												<FooterStyle CssClass="footer"></FooterStyle>
												<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
												<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
												<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
												<Columns>
													<asp:TemplateColumn>
														<HeaderStyle Width="2%"></HeaderStyle>
														<ItemTemplate>
															<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
															</asp:LinkButton>
														</ItemTemplate>
													</asp:TemplateColumn>
													<asp:BoundColumn Visible="False" DataField="coca_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
													<asp:BoundColumn DataField="coca_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}">
														<HeaderStyle Width="70px"></HeaderStyle>
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_alum_desc" HeaderText="Alumno">
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_orig_divi_desc" HeaderText="Divisi�n Orig.">
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_dest_divi_desc" HeaderText="Divisi�n Dest.">
													</asp:BoundColumn>
													<asp:BoundColumn DataField="_apro" HeaderText="Aceptado">
													</asp:BoundColumn>
												</Columns>
												<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
											</asp:datagrid></TD>
									</TR>
									<TR height="10">
										<TD></TD>
									</TR>
									<TR>
										<TD></TD>
										<TD vAlign="middle">
											<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
												<TR>
													<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
															BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
															ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Cambio de Comisi�n"></CC1:BOTONIMAGEN></TD>
													<TD></TD>
													<TD align="center" width="50"></TD>
												</TR>
											</TABLE>
										</TD>
									</TR>
									<TR>
										<TD></TD>
										<TD align="center" colSpan="2">
											<DIV><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Visible="False" BorderWidth="1px"
													BorderStyle="Solid" Height="116px">
													<P align="right">
														<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
															border="0">
															<TR>
																<TD>
																	<P></P>
																</TD>
																<TD height="5">
																	<asp:label id="lblTitu" runat="server" width="100%" cssclass="titulo"></asp:label></TD>
																<TD vAlign="top" align="right">&nbsp;
																	<asp:imagebutton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 100%; HEIGHT: 225px" align="center" colSpan="3">
																	<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																		<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																			<TR>
																				<TD style="WIDTH: 220px; HEIGHT: 4px" vAlign="top" align="right">
																					<asp:Label id="lblAlum" runat="server" cssclass="titulo">Legajo:</asp:Label>&nbsp;<BR>
																					<asp:Label id="Label1" runat="server" cssclass="titulo">Alumno:</asp:Label>&nbsp;
																				</TD>
																				<TD style="HEIGHT: 4px">
																					<UC1:CLIE id="usrAlum" runat="server" Obligatorio="true" FilCUIT="True" FilDocuNume="True" CampoVal="Alumno"
																						FilLegaNume="True" Saltos="1,1,1" Tabla="Alumnos"></UC1:CLIE></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right">
																					<asp:Label id="lblCarr" runat="server" cssclass="titulo">Carrera:</asp:Label>&nbsp;</TD>
																				<TD>
																					<cc1:combobox class="combo" id="cmbCarr" runat="server" Width="100%" Obligatorio="True" onchange="cmbCarr_change();"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right">
																					<asp:Label id="lblOrigDivi" runat="server" cssclass="titulo">Divisi�n Origen:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 20px" align="left">
																					<cc1:combobox class="combo" id="cmbOrigDivi" runat="server" Width="424px" NomOper="divisiones_cargar"
																						Obligatorio="True" onchange="cmbDivi_Change('Orig')"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right">
																					<asp:Label id="lblDestDivi" runat="server" cssclass="titulo">Divisi�n Destino:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 20px" align="left">
																					<cc1:combobox class="combo" id="cmbDestDivi" runat="server" Width="424px" NomOper="divisiones_cargar"
																						Obligatorio="True" onchange="cmbDivi_Change('Dest')"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right">
																					<asp:Label id="lblOrigComi" runat="server" cssclass="titulo">Comisi�n Origen:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 20px" align="left">
																					<cc1:combobox class="combo" id="cmbOrigComi" runat="server" Width="424px" NomOper="comisiones_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right">
																					<asp:Label id="lblDestComi" runat="server" cssclass="titulo">Comisi�n Destino:</asp:Label>&nbsp;</TD>
																				<TD style="HEIGHT: 20px" align="left">
																					<cc1:combobox class="combo" id="cmbDestComi" runat="server" Width="424px" NomOper="comisiones_cargar"></cc1:combobox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right" width="20%">
																					<asp:Label id="lblFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
																				<TD>
																					<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD align="right">
																					<asp:Label id="lblApro" runat="server" cssclass="titulo">Aceptado:</asp:Label>&nbsp;
																				</TD>
																				<TD>
																					<cc1:combobox class="combo" id="cmbApro" runat="server" Obligatorio="True" Width="60px"></cc1:combobox></TD>
																			</TR>
																		</TABLE>
																	</asp:panel>
																	<asp:label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:label></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="3"
																	height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR height="30">
																<TD align="center" colSpan="3"><A id="editar" name="editar"></A>
																	<asp:button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:button>&nbsp;
																	<asp:button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Baja"></asp:button>&nbsp;&nbsp;
																	<asp:button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Modificar"></asp:button>&nbsp;&nbsp;
																	<asp:button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Limpiar"></asp:button></TD>
															</TR>
														</TABLE>
													</P>
												</asp:panel>
												<DIV></DIV>
											</DIV>
										</TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></FORM>
	</BODY>
</HTML>
