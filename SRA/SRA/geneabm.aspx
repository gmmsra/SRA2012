<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.geneabm" CodeFile="geneabm.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitu%>
		</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY class="pagina" onload="gSetearTituloFrame('');" leftMargin="5" rightMargin="0" topMargin="5">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><IMG border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><IMG border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%; HEIGHT: 130px" id="Table1" border="0">
							<TR>
								<TD width="100%" colSpan="2"></TD>
							</TR>
							<TR>
								<TD style="HEIGHT: 25px" height="25"></TD>
								<TD style="HEIGHT: 25px" height="25" vAlign="bottom"><asp:label id="lblTituAbm" runat="server" width="391px" cssclass="opcion"></asp:label></TD>
							</TR>
							<tr>
								<td></td>
								<td><asp:panel id="panFiltro" runat="server" cssclass="titulo" Width="97%" BorderStyle="Solid"
										BorderWidth="0" Visible="False">
										<TABLE style="WIDTH: 100%" id="TableFil" border="0" cellSpacing="0" cellPadding="0" align="left">
											<TR>
												<TD>
													<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnConsultar" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg"
																	ForeColor="Transparent" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif"
																	OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
															<TD style="HEIGHT: 8px" width="26">
																<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																	ForeColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																	OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE id="Table4" border="0" cellSpacing="0" cellPadding="0" width="100%">
														<TR>
															<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="30"></TD>
															<TD vAlign="top"><!-- FOMULARIO -->
																<TABLE id="Table5" border="0" cellSpacing="0" cellPadding="0" width="100%">
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
																			<asp:Label id="lblFil" runat="server" cssclass="titulo"></asp:Label>&nbsp;</TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
																			<CC1:TEXTBOXTAB id="txtFil" runat="server" cssclass="cuadrotexto" Width="150px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																		<TD style="WIDTH: 20%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
																		<TD style="WIDTH: 72%; HEIGHT: 14px" background="imagenes/formfdofields.jpg"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 8%" height="2" background="imagenes/formdivfin.jpg" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 10%" height="2" background="imagenes/formdivfin.jpg" align="right"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																		<TD style="WIDTH: 82%" height="2" background="imagenes/formdivfin.jpg" width="8"><IMG src="imagenes/formdivfin.jpg" width="1" height="2"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD background="imagenes/formde.jpg" width="2"><IMG border="0" src="imagenes/formde.jpg" width="2" height="2"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE>
									</asp:panel></td>
							</tr>
							<TR>
								<TD></TD>
								<TD><asp:label id="lblFK" runat="server" cssclass="titulo"></asp:label><cc1:combobox id="cmbFK" class="combo" runat="server" Width="200px" autopostback="true"></cc1:combobox></TD>
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center"><asp:datagrid id="grdDato" runat="server" width="95%" BorderStyle="None" BorderWidth="1px" AllowPaging="True"
										HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" OnPageIndexChanged="DataGrid_Page" OnEditCommand="mEditarDatos"
										AutoGenerateColumns="False">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn>
												<HeaderStyle Width="2%"></HeaderStyle>
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" CausesValidation="false" Text="Editar" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="Id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
											<asp:BoundColumn DataField="codigo" ReadOnly="True" HeaderText="C&#243;digo">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn DataField="descrip" ReadOnly="True" HeaderText="Descripci&#243;n"></asp:BoundColumn>
											<asp:BoundColumn DataField="adic" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn DataField="adic2" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn DataField="adic3" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn DataField="adic4" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn DataField="adic5" ReadOnly="True"></asp:BoundColumn>
											<asp:BoundColumn DataField="defa" HeaderText="Final"></asp:BoundColumn>
											<asp:BoundColumn HeaderText="Estado">
												<HeaderStyle Width="10%"></HeaderStyle>
											</asp:BoundColumn>
											<asp:BoundColumn Visible="False" HeaderText="fecha"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<TABLE style="WIDTH: 100%; HEIGHT: 100%" border="0" cellPadding="0" align="left">
										<TR>
											<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
													ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnNuev.gif"
													ImageOver="btnNuev2.gif" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar un Nuevo Registro"
													ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
											<TD></TD>
											<TD width="50" align="center"><CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" BackColor="Transparent" IncludesUrl="includes/"
													ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif" BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"
													ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
										</TR>
									</TABLE>
								</TD>
							</TR>
							<TR>
								<TD><a name="editar"></a></TD>
								<TD align="center"><asp:panel id="panDato" runat="server" width="95%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
										Visible="False">
										<TABLE style="WIDTH: 100%; HEIGHT: 100%" id="Table2" class="FdoFld" border="0" cellPadding="0"
											align="left">
											<TR>
												<TD style="WIDTH: 261px">
													<P></P>
												</TD>
												<TD height="5">
													<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="219px"></asp:Label></TD>
												<TD vAlign="top" rowSpan="2" align="right">&nbsp;
													<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblCodi" runat="server" cssclass="titulo" Width="100%">Código:</asp:Label></TD>
												<TD>
													<CC1:NUMBERBOX id="txtCodiNum" runat="server" cssclass="cuadrotexto" Width="199px"></CC1:NUMBERBOX>
													<CC1:TEXTBOXTAB id="txtCodi" runat="server" cssclass="cuadrotexto" Width="199px"></CC1:TEXTBOXTAB></TD>
											</TR>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblDesc" runat="server" cssclass="titulo" Width="100%">Descripción:</asp:Label></TD>
												<TD style="HEIGHT: 16px" colSpan="2">
													<CC1:TEXTBOXTAB id="txtDesc" runat="server" cssclass="cuadrotexto" Width="250px"></CC1:TEXTBOXTAB></TD>
											</TR>
											<% If mstrAdic <> "" Then %>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<% End If %>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblAdic" runat="server" cssclass="titulo"></asp:Label></TD>
												<TD colSpan="2">
													<CC1:NUMBERBOX id="txtAdicNum" runat="server" cssclass="cuadrotexto" Width="199px" visible="false"></CC1:NUMBERBOX>
													<CC1:TEXTBOXTAB id="txtAdic" runat="server" cssclass="cuadrotexto" Width="250px" visible="false"></CC1:TEXTBOXTAB>
													<cc1:combobox id="cmbAdic" class="combo" runat="server" Width="200px" visible="false"></cc1:combobox></TD>
											</TR>
											<% If mstrGpAran <> "" Then %>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 261px" background="imagenes/formfdofields.jpg" align="right">
													<asp:Label id="lblCCosConcepBonifLab" runat="server" cssclass="titulo">Cto.Costo Bonif. Lab:</asp:Label>&nbsp;
												</TD>
												<TD style="WIDTH: 70%" background="imagenes/formfdofields.jpg">
													<TABLE border="0" cellSpacing="0" cellPadding="1">
														<TR>
															<TD>
																<cc1:combobox id="cmbCCosConcepBonifLab" class="combo" runat="server" cssclass="cuadrotexto" Width="300px"
																	Obligatorio="True" TextMaxLength="6" MostrarBotones="False" filtra="true" NomOper="centrosc_cuenta_cargar"></cc1:combobox></TD>
															<TD><IMG style="BORDER-BOTTOM: thin outset; BORDER-LEFT: thin outset; BORDER-TOP: thin outset; CURSOR: hand; BORDER-RIGHT: thin outset"
																	id="btnAvanBusq2" onclick="mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCCosConcep','Centros de Costo','@cuenta='+document.all('hdnCta').value);"
																	border="0" alt="Busqueda avanzada" src="imagenes/Buscar16.gif" runat="server">
															</TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblProcBonifPorCantLab1" runat="server" cssclass="titulo" Width="100%">1ª Procentaje Bonificación por Cantidad:</asp:Label></TD>
												<TD>
													<CC1:NUMBERBOX id="txtProcBonifPorCantLab1" runat="server" cssclass="cuadrotexto" Width="50px"
														MaxLength="5" esdecimal="true"></CC1:NUMBERBOX></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblCantidadABonificar1" runat="server" cssclass="titulo" Width="100%">1ª Cantidad a Bonificar:</asp:Label></TD>
												<TD>
													<CC1:NUMBERBOX id="txtCantidadABonificar1" runat="server" cssclass="cuadrotexto" Width="50px" MaxLength="3"
														esdecimal="False"></CC1:NUMBERBOX></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblProcBonifPorCantLab2" runat="server" cssclass="titulo" Width="100%">2ª Procentaje Bonificación por Cantidad:</asp:Label></TD>
												<TD>
													<CC1:NUMBERBOX id="txtProcBonifPorCantLab2" runat="server" cssclass="cuadrotexto" Width="50px"
														MaxLength="5" esdecimal="true"></CC1:NUMBERBOX></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblCantidadABonificar2" runat="server" cssclass="titulo" Width="100%">2ª Cantidad a Bonificar:</asp:Label></TD>
												<TD>
													<CC1:NUMBERBOX id="txtCantidadABonificar2" runat="server" cssclass="cuadrotexto" Width="50px" MaxLength="3"
														esdecimal="False"></CC1:NUMBERBOX></TD>
											</TR>
											<% End If %>
											<% If mstrAdic2 <> "" Then %>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<% End If %>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblAdic2" runat="server" cssclass="titulo"></asp:Label></TD>
												<TD colSpan="2">
													<CC1:NUMBERBOX id="txtAdicNum2" runat="server" cssclass="cuadrotexto" Width="199px" visible="false"></CC1:NUMBERBOX>
													<CC1:TEXTBOXTAB id="txtAdic2" runat="server" cssclass="cuadrotexto" Width="250px" visible="false"></CC1:TEXTBOXTAB>
													<cc1:combobox id="cmbAdic2" class="combo" runat="server" Width="200px" visible="false"></cc1:combobox></TD>
											</TR>
											<% If mstrAdic3 <> "" Then %>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<% End If %>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblAdic3" runat="server" cssclass="titulo"></asp:Label></TD>
												<TD colSpan="2">
													<CC1:NUMBERBOX id="txtAdic3Num" runat="server" cssclass="cuadrotexto" Width="199px" visible="false"></CC1:NUMBERBOX>
													<CC1:TEXTBOXTAB id="txtAdic3" runat="server" cssclass="cuadrotexto" Width="250px" visible="false"></CC1:TEXTBOXTAB>
													<cc1:combobox id="cmbAdic3" class="combo" runat="server" Width="200px" visible="false"></cc1:combobox></TD>
											</TR>
											<% If mstrAdic4 <> "" Then %>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<% End If %>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblAdic4" runat="server" cssclass="titulo"></asp:Label></TD>
												<TD colSpan="2">
													<CC1:NUMBERBOX id="txtAdic4Num" runat="server" cssclass="cuadrotexto" Width="199px" visible="false"></CC1:NUMBERBOX>
													<CC1:TEXTBOXTAB id="txtAdic4" runat="server" cssclass="cuadrotexto" Width="250px" visible="false"></CC1:TEXTBOXTAB>
													<cc1:combobox id="cmbAdic4" class="combo" runat="server" Width="200px" visible="false"></cc1:combobox></TD>
											</TR>
											<% If mstrAdic5 <> "" Then %>
											<TR>
												<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
											</TR>
											<% End If %>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblAdic5" runat="server" cssclass="titulo"></asp:Label></TD>
												<TD colSpan="2">
													<CC1:NUMBERBOX id="txtAdic5Num" runat="server" cssclass="cuadrotexto" Width="199px" visible="false"></CC1:NUMBERBOX>
													<CC1:TEXTBOXTAB id="txtAdic5" runat="server" cssclass="cuadrotexto" Width="250px" visible="false"></CC1:TEXTBOXTAB>
													<cc1:combobox id="cmbAdic5" class="combo" runat="server" Width="200px" visible="false"></cc1:combobox></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 261px" align="right">
													<asp:Label id="lblDefa" runat="server" cssclass="titulo">Default:</asp:Label></TD>
												<TD colSpan="2">
													<asp:CheckBox id="chkDefa" Runat="server" CssClass="Titulo"></asp:CheckBox></TD>
											</TR>
											<TR>
												<TD colSpan="3" align="center">
													<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
											</TR>
											<TR> <!--TD style="WIDTH: 142px"></TD-->
												<TD colSpan="3" align="center"><A id="editar" name="editar"></A>
													<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
													<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Baja"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Modificar"></asp:Button>&nbsp;&nbsp;
													<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
														Text="Limpiar"></asp:Button></TD>
											</TR>
										</TABLE>
									</asp:panel></TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
					<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
					<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnId" runat="server"></asp:textbox></DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		if (document.all["txtCodi"]!= null)
			document.all["txtCodi"].focus();
		if (document.all["txtCodiNum"]!= null)
			document.all["txtCodiNum"].focus();
		</SCRIPT>
	</BODY>
</HTML>
