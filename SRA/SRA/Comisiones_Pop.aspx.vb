Namespace SRA

Partial Class Comisiones_Pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblTotal As System.Web.UI.WebControls.Label
   Protected WithEvents txtTotal As NixorControls.NumberBox
   Protected WithEvents lblTotalSel As System.Web.UI.WebControls.Label
   Protected WithEvents txtTotalSel As NixorControls.NumberBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrTitulo As String
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
   Private mstrTablaComprobACuenta As String = SRA_Neg.Constantes.gTab_ComprobACuenta

   Private mstrParaPageSize As Integer

   Private Enum Columnas As Integer
      Chk = 0
      SactId = 1
      Numero = 2
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            mConsultar()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.CurrentPageIndex = E.NewPageIndex

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mConsultar()
      Dim lstrCmd As New StringBuilder
      Dim ds As DataSet
      Dim ldsDatos As DataSet

      lstrCmd.Append("exec comisiones_criad_actas")
      lstrCmd.Append(" @cria_id=")
      lstrCmd.Append(Request("cria_id"))

      ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

      grdConsulta.DataSource = ds
      grdConsulta.DataBind()
      ds.Dispose()
   End Sub
#End Region

End Class
End Namespace
