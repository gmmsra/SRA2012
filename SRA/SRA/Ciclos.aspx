<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Ciclos" CodeFile="Ciclos.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
  <HEAD>
		<title>Ciclos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellpadding="0" cellspacing="0" align="center" width="97%">
				<tr>
					<td width="9"><img src="imagenes/recsupiz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td background="imagenes/recsup.jpg"><img src="imagenes/recsup.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td width="13"><img src="imagenes/recsupde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><img src="imagenes/reciz.jpg" border="0" WIDTH="9" HEIGHT="10"></td>
					<td valign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server" width="391px">Ciclos</asp:label>
								</TD>
							</TR>
							<TR>
								<TD height="8"></TD>
								<TD vAlign="bottom" colSpan="2" height="8"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" vAlign="top"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="1px"
										Width="94%" BorderStyle="Solid" Height="44">
            <TABLE id=TableFil style="WIDTH: 103.5%; HEIGHT: 42px" cellSpacing=1 
            cellPadding=1 align=left border=0>
              <TR>
                <TD style="WIDTH: 107px; HEIGHT: 16px" align=right width=107>
<asp:Label id=lblAnioFil runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp; 
                </TD>
                <TD style="WIDTH: 144px; HEIGHT: 16px" width=144>
<cc1:numberbox id=txtAnioFil runat="server" cssclass="cuadrotexto" Width="80px" AceptaNull="False" MaxValor="2099"></cc1:numberbox></TD>
                <TD style="HEIGHT: 27px" align=right>
<CC1:BotonImagen id=btnBuscar runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent" ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"></CC1:BotonImagen></TD></TR></TABLE>
									</asp:panel></TD>
							</TR>
							<TR>
								<TD height="8"></TD>
								<TD vAlign="top" colSpan="2" height="8"></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" vAlign="top" align="center">
									<asp:datagrid id="grdDato" runat="server" width="100%" BorderStyle="None" AutoGenerateColumns="False"
										CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" BorderWidth="1px"
										OnEditCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" AllowPaging="True">
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" cssclass="item2"></ItemStyle>
										<HeaderStyle Height="20px" cssclass="header"></HeaderStyle>
										<FooterStyle cssclass="footer"></FooterStyle>
										<Columns>
											<asp:TemplateColumn HeaderStyle-Width="9px">
												<ItemTemplate>
													<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
														<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
													</asp:LinkButton>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="cicl_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="cicl_anio" HeaderText="A�o"></asp:BoundColumn>
											<asp:BoundColumn DataField="cicl_ciclo" HeaderText="Ciclo"></asp:BoundColumn>
											<asp:BoundColumn DataField="_carr_desc" HeaderText="Descripci�n"></asp:BoundColumn>
											<asp:BoundColumn DataField="cicl_curs_inic_fecha" HeaderText="Inic.Curs." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="cicl_curs_fina_fecha" HeaderText="Fina.Curs." DataFormatString="{0:dd/MM/yyyy}"></asp:BoundColumn>
											<asp:BoundColumn DataField="_tucu_desc" HeaderText="Turno"></asp:BoundColumn>
											<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" cssclass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR HEIGHT="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="middle">
									<CC1:BotonImagen id="btnAgre" runat="server" BorderStyle="None" ToolTip="Agregar un Nuevo Ciclo"
										ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
										BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
										BackColor="Transparent" ImageDisable="btnNuev0.gif"></CC1:BotonImagen>
								</TD>
								<TD align="right">
									<table id="tabLinks" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
										<tr>
											<td width="1"><IMG height="28" src="imagenes/tab_a.bmp" width="9" border="0"></td>
											<td background="imagenes/tab_b.bmp"><IMG height="28" src="imagenes/tab_b.bmp" width="8" border="0"></td>
											<td width="1"><IMG height="28" src="imagenes/tab_c.bmp" width="31" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkCabecera" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
													cssclass="solapa" Width="80px" CausesValidation="False" Height="21px"> Ciclo</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_f.bmp" width="27" border="0"></td>
											<td width="1" background="imagenes/tab_fondo.bmp"><asp:linkbutton id="lnkMate" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server" cssclass="solapa"
													Width="70px" CausesValidation="False" Height="21px"> Materias</asp:linkbutton></td>
											<td width="1"><IMG height="28" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></td>
										</tr>
									</table>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD colSpan="2" align="center">
									<DIV>
										<asp:panel cssclass="titulo" id="panDato" runat="server" width="100%" Height="116px" BorderStyle="Solid"
											BorderWidth="1px" Visible="False">
            <P align=right>
            <TABLE class=FdoFld id=Table2 style="WIDTH: 100%; HEIGHT: 106px" 
            cellPadding=0 align=left border=0>
              <TR>
                <TD>
                  <P></P></TD>
                <TD height=5>
<asp:Label id=lblTitu runat="server" width="100%" cssclass="titulo"></asp:Label></TD>
                <TD vAlign=top align=right>&nbsp; 
<asp:ImageButton id=imgClose runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD></TR>
              <TR>
                <TD style="WIDTH: 100%" colSpan=3>
<asp:panel id=panCabecera runat="server" cssclass="titulo" Width="100%">
                  <TABLE id=TableCabecera style="WIDTH: 100%" cellPadding=0 
                  align=left border=0>
                    <TR>
                      <TD align=right width=90>
<asp:Label id=lblAnio runat="server" cssclass="titulo">A�o:</asp:Label>&nbsp;</TD>
                      <TD>
                        <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
                        border=0>
                          <TR>
                            <TD align=left width="15%">
<cc1:numberbox id=txtAnio runat="server" cssclass="cuadrotexto" Width="60px" MaxValor="2099" Obligatorio="true"></cc1:numberbox></TD>
                            <TD align=right width="15%">
<asp:Label id=lblCiclo runat="server" cssclass="titulo">Ciclo:</asp:Label>&nbsp; 
                            </TD>
                            <TD>
<cc1:numberbox id=txtCiclo runat="server" cssclass="cuadrotextodeshab" Width="60px" Enabled="False"></cc1:numberbox></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblCarr runat="server" cssclass="titulo">Carrera:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbCarr runat="server" Width="100%" Obligatorio="True"></cc1:combobox></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=2 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right width=90>
<asp:Label id=lblCursInicFecha runat="server" cssclass="titulo">Inicio de Cursada:</asp:Label>&nbsp;</TD>
                      <TD>
                        <TABLE style="WIDTH: 100%" cellSpacing=0 cellPadding=0 
                        border=0>
                          <TR>
                            <TD align=left width="30%">
<cc1:DateBox id=txtCursInicFecha runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD>
                            <TD align=right width="20%">
<asp:Label id=lblCursFinaFecha runat="server" cssclass="titulo">Finaliza:</asp:Label></TD>
                            <TD>
<cc1:DateBox id=txtCursFinaFecha runat="server" cssclass="cuadrotexto" Width="70px" Obligatorio="true"></cc1:DateBox></TD></TR></TABLE></TD></TR>
                    <TR>
                      <TD align=right background=imagenes/formdivmed.jpg 
                      colSpan=3 height=2><IMG height=2 
                        src="imagenes/formdivmed.jpg" width=1></TD></TR>
                    <TR>
                      <TD align=right>
<asp:Label id=lblTucu runat="server" cssclass="titulo">Turno:</asp:Label>&nbsp;</TD>
                      <TD>
<cc1:combobox class=combo id=cmbTucu runat="server" Width="160px" Obligatorio="true"></cc1:combobox></TD></TR></TABLE></asp:panel></TD></TR>
              <TR>
                <TD style="WIDTH: 100%" colSpan=3>
<asp:panel id=panMate runat="server" cssclass="titulo" Width="100%" Visible="False">
                  <TABLE id=TableDetalle style="WIDTH: 100%" cellPadding=0 
                  align=left border=0>
                    <TR>
                      <TD style="WIDTH: 100%" colSpan=2>
<asp:datagrid id=grdMate runat="server" width="95%" BorderStyle="None" BorderWidth="1px" AllowPaging="True" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" PageSize="100">
																				<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																				<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																				<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																				<FooterStyle CssClass="footer"></FooterStyle>
																				<Columns>
																					<asp:BoundColumn Visible="False" DataField="cima_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																					<asp:BoundColumn DataField="_mate_desc" ReadOnly="True" HeaderText="Materia"></asp:BoundColumn>
																					<asp:TemplateColumn HeaderStyle-Width="7%" HeaderText="Inicio">
																						<ItemTemplate>
																							<cc1:datebox id="txtInicFecha" runat="server" cssclass="cuadrotexto" Width="70px" height="18px" 
 style="font-size: 8pt"></cc1:datebox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																					<asp:TemplateColumn HeaderStyle-Width="7%" HeaderText="Fin">
																						<ItemTemplate>
																							<cc1:datebox id="txtFinaFecha" runat="server" cssclass="cuadrotexto" Width="70px" height="18px" 
 style="font-size: 8pt"></cc1:datebox>
																						</ItemTemplate>
																					</asp:TemplateColumn>
																				</Columns>
																				<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																			</asp:datagrid></TD></TR></TABLE></asp:panel></TD></TR></TABLE></P>
										</asp:panel>
										<ASP:PANEL Runat="server" ID="panBotones">
            <TABLE width="100%">
              <TR>
                <TD align=center>
<asp:Label id=lblBaja runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD></TR>
              <TR height=30>
                <TD align=center><A id=editar name=editar></A>
<asp:Button id=btnAlta runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp; 
<asp:Button id=btnBaja runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Baja"></asp:Button>&nbsp;&nbsp; 
<asp:Button id=btnModi runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Modificar"></asp:Button>&nbsp;&nbsp; 
<asp:Button id=btnLimp runat="server" cssclass="boton" Width="80px" CausesValidation="False" Text="Limpiar"></asp:Button></TD></TR></TABLE>
										</ASP:PANEL>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><img src="imagenes/recinfiz.jpg" border="0" WIDTH="9" HEIGHT="15"></td>
					<td background="imagenes/recinf.jpg"><img src="imagenes/recinf.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
					<td width="13"><img src="imagenes/recinfde.jpg" border="0" WIDTH="13" HEIGHT="15"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:TextBox id="hdnId" runat="server"></asp:TextBox>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		if (document.all["txtDesc"]!= null)
			document.all["txtDesc"].focus();
		</SCRIPT>
	</BODY>
</HTML>
