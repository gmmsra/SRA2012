Namespace SRA

Partial Class RevaluosConfirma
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub



    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mstrCmd As String
   Private mstrParaPageSize As Integer
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If (Not Page.IsPostBack) Then
            mInicializar()
            mEstablecerPerfil()
            mConsultar()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mInicializar()
        Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdCons.PageSize = Convert.ToInt32(mstrParaPageSize)
      btnConf.Attributes.Add("onclick", "if(!confirm('Confirma los revaluos de cuotas?')) return false;")
      btnBorr.Attributes.Add("onclick", "if(!confirm('Elimina los revaluos de cuotas?')) return false;")
   End Sub

   Private Sub mEstablecerPerfil()
      Dim lbooPermiAlta As Boolean
      Dim lbooPermiModi As Boolean

      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones, String), (mstrConn), (Session("sUserId").ToString()))) Then
      'Response.Redirect("noaccess.aspx")
      'End If

      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Inscripciones_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
      'btnModi.Visible = lbooPermiModi

    End Sub

#End Region

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdCons.EditItemIndex = -1
         If (grdCons.CurrentPageIndex < 0 Or grdCons.CurrentPageIndex >= grdCons.PageCount) Then
            grdCons.CurrentPageIndex = 0
         Else
            grdCons.CurrentPageIndex = E.NewPageIndex
         End If
            mConsultar()
            btnConf.Enabled = False

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

    Private Sub mConsultar()
      Try
         mstrCmd = "exec revaluos_consul "
         
         clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdCons)

         btnConf.Enabled = (grdCons.Items.Count <> 0)
         btnBorr.Enabled = btnConf.Enabled

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub

    Private Sub btnCons_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        grdCons.CurrentPageIndex = 0
        mConsultar()
    End Sub

   

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs)
      grdCons.CurrentPageIndex = 0
      mConsultar()
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         Dim lstrRptName As String = "Revaluos"
         Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
         lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
         Response.Redirect(lstrRpt)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnConf_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnConf.Click
      Try

         mstrCmd = "exec revaluos_confirmar "
         mstrCmd = mstrCmd & Session("sUserId").ToString

         clsSQLServer.gExecute(mstrConn, mstrCmd)

         mConsultar()
         btnConf.Enabled = False
         btnBorr.Enabled = False

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBorr.Click
      Try

         mstrCmd = "exec revaluos_borrar"


         clsSQLServer.gExecute(mstrConn, mstrCmd)

         mConsultar()
         btnConf.Enabled = False
         btnBorr.Enabled = False

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
