<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.CategoCuentas" CodeFile="CategoCuentas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Cuentas por Categor�a</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">
		function mSetearCuenta()
		{
			var lstrCta = '';
			if (document.all('txtcmbCta07').value!='')
				lstrCta = document.all('txtcmbCta07').value.substring(0,1);
			document.all('hdnCta').value = lstrCta;
			if (lstrCta=='' || lstrCta=='1' || lstrCta=='2' || lstrCta=='3')
			{
				document.all('cmbCcos07').disabled = true;
				document.all('txtcmbCcos07').disabled = true;
				document.all('cmbCcos07').innerHTML = '';
				document.all('txtcmbCcos07').value = '';
			}
			else
			{
				document.all('cmbCcos07').disabled = false;
				document.all('txtcmbCcos07').disabled = false;
				LoadComboXML("centrosc_cuenta_cargar", "@cuenta="+lstrCta, "cmbCcos07", "N");
			}			
		}
		function mSelecCate()
		  {
			document.all["cmbCateAux"].value = document.all["cmbCate"].value;
						
			if (document.all["cmbCateAux"].item(document.all["cmbCateAux"].selectedIndex).text == "S")
			  {	
			  	document.all["cmbInst"].disabled = false;
				
               }   
                else
               {
               
                document.all["cmbInst"].disabled = true;
                document.all["cmbInst"].selectedIndex=0;
                
			
               }   
		   }
		   
		function cmbCta07_change(pCmb)
		{
			var pHabi=false;
			var pstrRet;
			if(pCmb.value!="")
			{
				pstrRet = LeerCamposXML("ctas_ctables", pCmb.value, "cuct_codi");
				pHabi = pstrRet.substring(0,1)=="4"||pstrRet.substring(0,1)=="5";
			}
			document.all["cmbCCos07"].disabled = !pHabi;
			if(document.all["cmbCCos07"].disabled)
				document.all["cmbCCos07"].selectedIndex=0;
		}
		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="HEIGHT: 33px" vAlign="bottom" colSpan="3" height="33"><asp:label id="Label4" runat="server" width="391px" cssclass="opcion">Cuentas por Categor�a</asp:label></TD>
								</TR>
								<TR>
									<TD align="center" colSpan="3">
										<TABLE id="Table2" style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
											<TR>
												<TD vAlign="top" width="100%" colSpan="3"></TD>
											</TR> <!-- Fin Filtro -->
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panCabecera" runat="server" cssclass="titulo" Visible="True" Width="100%">
														<TABLE id="TableDetalle" cellPadding="0" width="100%" align="left" border="0">
															<TR>
																<TD align="left" width="100%">
																	<asp:datagrid id="grdDato" runat="server" width="100%" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
																		OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
																		AllowPaging="True" BorderWidth="1px" BorderStyle="None">
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<Columns>
																			<asp:TemplateColumn>
																				<HeaderStyle Width="20px"></HeaderStyle>
																				<ItemTemplate>
																					<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																						<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																					</asp:LinkButton>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn Visible="False" DataField="cacu_id" ReadOnly="True" HeaderText="ID">
																				<HeaderStyle Width="2%"></HeaderStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="cacu_cate_id" HeaderText="cacu_cate_id">
																				<HeaderStyle Width="25%"></HeaderStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="cacu_inst_id" HeaderText="cacu_inst_id">
																				<HeaderStyle Width="25%"></HeaderStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="cacu_bime_cuct_id" HeaderText="cacu_bime_cuct_id">
																				<HeaderStyle Width="15%"></HeaderStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="cacu_venc_cuct_id" HeaderText="cacu_venc_cuct_id">
																				<HeaderStyle Width="15%"></HeaderStyle>
																			</asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="cacu_adel_cuct_id" HeaderText="cacu_adel_cuct_id"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="cacu_cuot_cuct_id" HeaderText="cacu_cuot_cuct_id"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="cacu_acta_cuct_id" HeaderText="cacu_acta_cuct_id"></asp:BoundColumn>
																			<asp:BoundColumn Visible="False" DataField="cacu_soli_cuct_id" HeaderText="cacu_soli_cuct_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_categoria" HeaderText="Categor&#237;a"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_institucion" HeaderText="Instituci&#243;n"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_soli" HeaderText="Sol. ingreso"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_soci" HeaderText="Ctas. sociales"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_bime" HeaderText="Bim. en curso"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_venc" HeaderText="Ctas. vencidas"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_adel" HeaderText="Ctas. adelantadas"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_pago" HeaderText="Pago a cta."></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
															<TR>
																<TD height="10"></TD>
															</TR>
															<TR>
																<TD vAlign="middle">
																	<TABLE style="WIDTH: 100%; HEIGHT: 100%" cellPadding="0" align="left" border="0">
																		<TR>
																			<TD align="left" width="99%">
																				<CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																					BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																					BtnImage="edit.gif" ImageBoton="btnNuev.gif" ImageOver="btnNuev2.gif" ImageDisable="btnNuev0.gif"
																					ToolTip="Agregar una Nueva Relaci�n"></CC1:BOTONIMAGEN></TD>
																			<TD></TD>
																			<TD align="center" width="100">
																				<CC1:BOTONIMAGEN id="btnList" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" ForeColor="Transparent"
																					BackColor="Transparent" IncludesUrl="includes/" ImagesUrl="imagenes/" CambiaValor="False" OutImage="del.gif"
																					BtnImage="edit.gif" ImageBoton="btnImpr.gif" ImageOver="btnImpr2.gif"></CC1:BOTONIMAGEN></TD>
																		</TR>
																	</TABLE>
																</TD>
															<TR>
																<TD height="10"></TD>
															</TR>
															<TR>
																<TD style="WIDTH: 100%">
																	<asp:panel id="PanDato" runat="server" cssclass="titulo" Width="100%" Visible="false">
																		<TABLE class="FdoFld" id="TableaPnDetalle" style="WIDTH: 100%" cellPadding="0" align="left"
																			border="0">
																			<TR>
																				<TD align="left" colSpan="3"></TD>
																				<TD align="right" height="5">
																					<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px" align="right">
																					<asp:Label id="lblCate" runat="server" cssclass="titulo">Categor�a:</asp:Label></TD>
																				<TD colSpan="3" height="5">
																					<CC1:COMBOBOX class="combo" id="cmbCate" runat="server" Width="321px" onchange=" mSelecCate();"
																						AceptaNull="false" onselectedItem=" mSelecCate();"></CC1:COMBOBOX></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px" align="right">
																					<asp:Label id="lblInst" runat="server" cssclass="titulo">Instituci�n:</asp:Label></TD>
																				<TD colSpan="3" height="5">
																					<CC1:COMBOBOX class="combo" id="cmbInst" runat="server" Width="320px" Obligatorio="False"></CC1:COMBOBOX></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px" align="right">
																					<asp:Label id="lblSoli" runat="server" cssclass="titulo">Solicitud de ingreso:</asp:Label></TD>
																				<TD colSpan="3" height="5">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbCta01" runat="server" cssclass="cuadrotexto" Width="280px"
																									Obligatorio="True" Height="20px" MostrarBotones="False" NomOper="cuentas_ctables_cargar" filtra="true"
																									TextMaxLength="7"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCta01','Cuentas Contables','[1,2,4,5]');"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px" align="right">
																					<asp:Label id="lblCtaSoci" runat="server" cssclass="titulo">Pagos a cuenta:</asp:Label></TD>
																				<TD colSpan="3" height="5">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbcta02" runat="server" cssclass="cuadrotexto" Width="280px"
																									Obligatorio="True" Height="20px" MostrarBotones="False" NomOper="cuentas_ctables_cargar" filtra="true"
																									TextMaxLength="7"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbcta02','Cuentas Contables','[1,2,4,5]');"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px" align="right">
																					<asp:Label id="lblRCta" runat="server" cssclass="titulo">Revaluo Cuotas:</asp:Label></TD>
																				<TD colSpan="3" height="5">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbRCta" runat="server" cssclass="cuadrotexto" Width="280px" Obligatorio="True"
																									Height="20px" MostrarBotones="False" NomOper="cuentas_ctables_cargar" filtra="true" TextMaxLength="7"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbRCta','Cuentas Contables','[1,2,4,5]');"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px" align="right">
																					<asp:Label id="lblCategoria" runat="server" cssclass="titulo">Cambio Categoria:</asp:Label></TD>
																				<TD colSpan="3" height="5">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbCategoria" runat="server" cssclass="cuadrotexto" Width="280px"
																									Obligatorio="True" Height="20px" MostrarBotones="False" NomOper="cuentas_ctables_cargar" filtra="true"
																									TextMaxLength="7"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCategoria','Cuentas Contables','[1,2,4,5]');"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px; HEIGHT: 17px" align="right">
																					<asp:Label id="lblBiCur" runat="server" cssclass="titulo">Bimestre en curso:</asp:Label></TD>
																				<TD style="HEIGHT: 17px" colSpan="3" height="17">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbCta03" runat="server" cssclass="cuadrotexto" Width="280px"
																									Obligatorio="True" Height="20px" MostrarBotones="False" NomOper="cuentas_ctables_cargar" filtra="true"
																									TextMaxLength="7"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCta03','Cuentas Contables','[1,2,4,5]');"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px; HEIGHT: 13px" align="right">
																					<asp:Label id="lblCtaVda" runat="server" cssclass="titulo">Cuotas vencidas:</asp:Label></TD>
																				<TD style="HEIGHT: 13px" colSpan="3" height="13">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbCta04" runat="server" cssclass="cuadrotexto" Width="280px"
																									Obligatorio="True" Height="20px" MostrarBotones="False" NomOper="cuentas_ctables_cargar" filtra="true"
																									TextMaxLength="7"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCta04','Cuentas Contables','[1,2,4,5]');"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="4"
																					height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px" align="right">
																					<asp:Label id="lblCtaAde" runat="server" cssclass="titulo">Cuotas adelantadas:</asp:Label></TD>
																				<TD colSpan="3" height="5">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbCta05" runat="server" cssclass="cuadrotexto" Width="280px"
																									Obligatorio="True" Height="20px" MostrarBotones="False" NomOper="cuentas_ctables_cargar" filtra="true"
																									TextMaxLength="7"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCta05','Cuentas Contables','[1,2,4,5]');"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="4"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px; HEIGHT: 19px" align="right">
																					<asp:Label id="lblPagCta" runat="server" cssclass="titulo">Cuotas sociales:</asp:Label></TD>
																				<TD style="HEIGHT: 19px" colSpan="3" height="19">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbCta07" runat="server" cssclass="cuadrotexto" Width="280px"
																									onchange="javascript:mSetearCuenta();" Obligatorio="True" Height="20px" MostrarBotones="False"
																									NomOper="cuentas_ctables_cargar" filtra="true" TextMaxLength="7"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mBotonBusquedaAvanzada('cuentas_ctables','cuct_desc','cmbCta07','Cuentas Contables','[1,2,4,5]');"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 341px" align="right">
																					<asp:Label id="lblCCosSoci" runat="server" cssclass="titulo">C.Costo:</asp:Label></TD>
																				<TD width="76%" colSpan="3" height="5">
																					<TABLE cellSpacing="0" cellPadding="0" border="0">
																						<TR>
																							<TD>
																								<cc1:combobox class="combo" id="cmbCcos07" runat="server" cssclass="cuadrotexto" Width="280px"
																									Obligatorio="True" Height="20px" MostrarBotones="False" NomOper="centrosc_cargar" filtra="true"
																									TextMaxLength="6"></cc1:combobox></TD>
																							<TD><IMG id="btnAvanBusq2" style="BORDER-RIGHT: thin outset; BORDER-TOP: thin outset; BORDER-LEFT: thin outset; CURSOR: hand; BORDER-BOTTOM: thin outset"
																									onclick="mSetearCuenta(); var lstrFil='-1'; if (document.all('hdnCta').value != '') lstrFil = document.all('hdnCta').value; mBotonBusquedaAvanzada('centrosc','ccos_desc','cmbCcos07','Centros de Costo','@cuenta='+lstrFil);"
																									alt="Busqueda avanzada" src="imagenes/Buscar16.gif" border="0">
																							</TD>
																						</TR>
																					</TABLE>
																				</TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="../imagenes/formfdofields.jpg" colSpan="4"></TD>
																			</TR>
																			<TR>
																				<TD align="right" background="imagenes/formdivmed.jpg" colSpan="4" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																			</TR>
																			<TR>
																				<TD vAlign="middle" align="center" colSpan="4" height="30">
																					<ASP:PANEL id="panBotones" Runat="server"><A id="editar" name="editar"></A>
<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="90px" Text="Alta"></asp:Button>&nbsp;&nbsp; 
<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="90px" Text="Eliminar"></asp:Button>&nbsp; 
<asp:Button id="btnModi" runat="server" cssclass="boton" Width="90px" Text="Modificar"></asp:Button>&nbsp; 
<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="90px" Text="Limpiar"></asp:Button></ASP:PANEL></TD>
																			</TR>
																		</TABLE>
																	</asp:panel></TD>
															</TR>
														</TABLE>
													</asp:panel>
												</TD>
											</TR>
										</TABLE>
									</TD>
								</TR>
							</TABLE>
							<!--- FIN CONTENIDO ---></td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<asp:textbox id="hdnCta" runat="server"></asp:textbox>
				<ASP:TEXTBOX id="lblMens" runat="server" Visible="True"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server" Visible="True"></asp:textbox>
				<cc1:combobox class="combo" id="cmbCateAux" runat="server" Width="100%" AceptaNull="false"></cc1:combobox>
			</DIV>
		</form>
	</BODY>
</HTML>
