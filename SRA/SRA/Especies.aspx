<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Especies" CodeFile="Especies.aspx.vb" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
						<HEAD>
												<title>Especies</title>
												<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
												<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
												<meta content="JavaScript" name="vs_defaumltconcntScript">
												<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
												<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
												<script language="JavaScript" src="includes/utiles.js"></script>
												<script language="JavaScript" src="includes/paneles.js"></script>
						</HEAD>
						<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('')" rightMargin="0">
												<form id="frmABM" method="post" runat="server">
																		<!------------------ RECUADRO ------------------->
																		<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
																								<TBODY>
																														<tr>
																																				<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
																																				<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
																																				<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
																														</tr>
																														<tr>
																																				<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
																																				<td vAlign="middle" align="center">
																																										<!----- CONTENIDO ----->
																																										<TABLE id="Table1" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" border="0">
																																																<TBODY>
																																																						<TR>
																																																												<TD></TD>
																																																												<TD width="100%" colSpan="2"></TD>
																																																						</TR>
																																																						<TR>
																																																												<TD style="HEIGHT: 27px" height="27"></TD>
																																																												<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Especies</asp:label></TD>
																																																						</TR>
																																																						<TR>
																																																												<TD colSpan="3" height="10"></TD>
																																																						</TR>
																																																						<TR>
																																																												<TD></TD>
																																																												<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" AutoGenerateColumns="False" OnEditCommand="mEditarDatos"
																																																																								OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True"
																																																																								BorderWidth="1px" BorderStyle="None" width="100%">
																																																																								<FooterStyle CssClass="footer"></FooterStyle>
																																																																								<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																																																																								<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																																																																								<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																																																																								<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																																																																								<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																																																																								<Columns>
																																																																														<asp:TemplateColumn>
																																																																																				<HeaderStyle Width="2%"></HeaderStyle>
																																																																																				<ItemTemplate>
																																																																																										<asp:LinkButton id="Linkbutton6" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																																																																																																<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																																																																																										</asp:LinkButton>
																																																																																				</ItemTemplate>
																																																																														</asp:TemplateColumn>
																																																																														<asp:BoundColumn Visible="False" DataField="espe_id" ReadOnly="True" HeaderText="ID"></asp:BoundColumn>
																																																																														<asp:BoundColumn DataField="espe_codi" ReadOnly="True" HeaderText="C&#243;digo">
																																																																																				<HeaderStyle Width="10%"></HeaderStyle>
																																																																														</asp:BoundColumn>
																																																																														<asp:BoundColumn DataField="espe_desc" ReadOnly="True" HeaderText="Descripci&#243;n"></asp:BoundColumn>
																																																																														<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
																																																																								</Columns>
																																																																								<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																																																																		</asp:datagrid></TD>
																																																						</TR>
																																																						<tr>
																																																												<TD align="right" colSpan="2"></TD>
																																																						</tr>
																																																						<TR>
																																																												<TD style="HEIGHT: 9px" height="9"></TD>
																																																												<TD align="left"><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageDisable="btnNuev0.gif" ToolTip="Nuevo Centro de Implantes"
																																																																								ForeColor="Transparent" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif"
																																																																								CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></TD>
																																																												<td align="right"><CC1:BOTONIMAGEN id="btnImprimir" runat="server" BorderStyle="None" ImageDisable="btnImpr0.gif" ToolTip="Listar"
																																																																								ForeColor="Transparent" ImageOver="btnImpr2.gif" ImageBoton="btnImpr.gif" BtnImage="edit.gif" OutImage="del.gif"
																																																																								CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BOTONIMAGEN></td>
																																																						</TR>
																																																						<tr>
																																																												<TD colSpan="2"></TD>
																																																						</tr>
																																																						<TR>
																																																												<TD></TD>
																																																												<TD align="center" colSpan="2"><asp:panel id="panSolapas" runat="server" cssclass="titulo" width="100%" Height="124%" Visible="False">
																																																																								<TABLE id="TablaSolapas" cellSpacing="0" cellPadding="0" width="100%" border="0" runat="server">
																																																																														<TR>
																																																																																				<TD style="WIDTH: 0.24%"><IMG height="27" src="imagenes/tab_a.bmp" width="9" border="0"></TD>
																																																																																				<TD background="imagenes/tab_b.bmp"><IMG height="27" src="imagenes/tab_b.bmp" width="8" border="0"></TD>
																																																																																				<TD width="1"><IMG height="27" src="imagenes/tab_c.bmp" width="31" border="0"></TD>
																																																																																				<TD width="1" background="imagenes/tab_fondo.bmp">
																																																																																										<asp:linkbutton id="lnkGeneral" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																																																																																																cssclass="solapa" Height="21px" Width="80px" CausesValidation="False" Font-Bold="True"> General</asp:linkbutton></TD>
																																																																																				<TD style="WIDTH: 23px" width="23"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
																																																																																				<TD width="1" background="imagenes/tab_fondo.bmp">
																																																																																										<asp:linkbutton id="lnkGenealogia1" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																																																																																																cssclass="solapa" Height="21px" Width="100px" CausesValidation="False">Genealog�a(1)</asp:linkbutton></TD>
																																																																																				<TD width="1"><IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"></TD>
																																																																																				<TD width="1" background="imagenes/tab_fondo.bmp">
																																																																																										<asp:linkbutton id="lnkGenealogia2" style="TEXT-ALIGN: center; TEXT-DECORATION: none" runat="server"
																																																																																																cssclass="solapa" Height="21px" Width="100px" CausesValidation="False">Genealog�a(2)</asp:linkbutton></TD>
																																																																																				<TD width="1"><IMG height="27" src="imagenes/tab_fondo_fin.bmp" width="31" border="0"></TD>
																																																																														</TR>
																																																																								</TABLE>
																																																																		</asp:panel></TD>
																																																						</TR>
																																																						<TR>
																																																												<TD></TD>
																																																												<TD vAlign="top" align="center" colSpan="2"><asp:panel id="panDato" runat="server" cssclass="titulo" width="100%" Height="116px" Visible="False">
																																																																								<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
																																																																														border="0">
																																																																														<TR>
																																																																																				<TD style="WIDTH: 100%" vAlign="top" align="right">
																																																																																										<asp:imagebutton id="btnClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:imagebutton></TD>
																																																																														</TR>
																																																																														<TR>
																																																																																				<TD align="right" background="imagenes/formdivmed.jpg" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																														</TR>
																																																																														<TR>
																																																																																				<TD style="WIDTH: 100%">
																																																																																										<asp:panel id="panGeneral" runat="server" cssclass="titulo" Width="100%">
																																																																																																<TABLE id="TablaGeneral" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																																																																																																						<TR>
																																																																																																												<TD style="WIDTH: 240px; HEIGHT: 16px" align="right">
																																																																																																																		<asp:Label id="lblCodigo" runat="server" cssclass="titulo">C�digo:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="HEIGHT: 16px">
																																																																																																																		<CC1:TEXTBOXTAB id="txtCodigo" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																																																																																																																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="WIDTH: 240px; HEIGHT: 16px" align="right">
																																																																																																																		<asp:Label id="lblDescripcion" runat="server" cssclass="titulo">Descripci�n:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="HEIGHT: 16px">
																																																																																																																		<CC1:TEXTBOXTAB id="txtDescripcion" runat="server" cssclass="cuadrotexto" Width="50%" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																																																																																																																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="WIDTH: 240px; HEIGHT: 16px" align="right">
																																																																																																																		<asp:Label id="lblTipoEspecie" runat="server" cssclass="titulo">Tipo de Especie:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="HEIGHT: 16px">
																																																																																																																		<cc1:combobox class="combo" id="cmbTipoEspecie" runat="server" Width="136px" Obligatorio="True"></cc1:combobox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																																																																																																																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="WIDTH: 240px; HEIGHT: 16px" align="right">
																																																																																																																		<asp:Label id="lblNomNum" runat="server" cssclass="titulo">Denominaci�n N�mero:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="HEIGHT: 16px">
																																																																																																																		<CC1:TEXTBOXTAB id="txtNomNum" runat="server" cssclass="cuadrotexto" Width="100px" Obligatorio="True"></CC1:TEXTBOXTAB></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																																																																																																																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="WIDTH: 240px; HEIGHT: 10px" align="right">
																																																																																																																		<asp:Label id="lblPromVida" runat="server" cssclass="titulo">Prom. vida (a�os)</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="HEIGHT: 10px">
																																																																																																																		<cc1:numberbox id="txtPromVida" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																																																																																																																								maxvalor="200" EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																																																																																																																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="WIDTH: 240px; HEIGHT: 17px" align="right">
																																																																																																																		<asp:Label id="lblGenealo" runat="server" cssclass="titulo">Controla Genealog�a:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="HEIGHT: 17px">
																																																																																																																		<cc1:combobox class="combo" id="cmbGenealo" runat="server" Width="70px" obligatorio="true"></cc1:combobox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="WIDTH: 240px; HEIGHT: 16px" align="right">
																																																																																																																		<asp:Label id="lblAnillo" runat="server" cssclass="titulo">LLeva Anillo:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="HEIGHT: 16px">
																																																																																																																		<cc1:combobox class="combo" id="cmbAnillo" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="WIDTH: 240px; HEIGHT: 16px" align="right">
																																																																																																																		<asp:Label id="lblVentaSRA" runat="server" cssclass="titulo">Venta SRA:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="HEIGHT: 16px">
																																																																																																																		<cc1:combobox class="combo" id="cmbVentaSRA" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																																																																																																																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																</TABLE>
																																																																																										</asp:panel></TD>
																																																																														</TR>
																																																																														<TR>
																																																																																				<TD style="WIDTH: 100%" colSpan="3">
																																																																																										<asp:panel id="panGenealogia1" runat="server" cssclass="titulo" Visible="False" Width="100%">
																																																																																																<TABLE id="TablaEspecies1" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblDiasGest" runat="server" cssclass="titulo">D�as de Gestaci�n:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%; HEIGHT: 22px">
																																																																																																																		<cc1:numberbox id="txtDiasGest" runat="server" cssclass="cuadrotexto" Width="55px" EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblDiasGestaMax" runat="server" cssclass="titulo">D�as Gestaci�n (M�x):</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:numberbox id="txtDiasGestMax" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																																																																																																																								Width="55px" EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblDiasGestMin" runat="server" cssclass="titulo">D�as Gestaci�n (Min):</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:numberbox id="txtDiasGestMin" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																																																																																																																								Width="55px" EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblDiasServCamp" runat="server" cssclass="titulo">D�as entre Servicios a campo (Min):</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:numberbox id="txtDiasServCamp" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto"
																																																																																																																								Width="55px" EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblDiasInsem" runat="server" cssclass="titulo">D�as entre Inseminaciones (Min):</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:numberbox id="txtDiasInsem" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																																																																																																																								EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblDiasParto" runat="server" cssclass="titulo">D�as entre Partos (Min):</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:numberbox id="txtDiasParto" style="FONT-SIZE: 8pt" runat="server" cssclass="cuadrotexto" Width="55px"
																																																																																																																								EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblMelli" runat="server" cssclass="titulo">Cantidad de Mellizos:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:numberbox id="txtMelli" runat="server" cssclass="cuadrotexto" Width="55px" maxvalor="20" EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblReqAnalisis" runat="server" cssclass="titulo">Requiere An�lisis Tipificaci�n:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbReqAnalisis" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblReqADN" runat="server" cssclass="titulo">Requiere An�lisis ADN:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbReqADN" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblMuesCrias" runat="server" cssclass="titulo">Muestreo Crias:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbMuesCrias" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblMuesCriasPorc" runat="server" cssclass="titulo">Muestreo de Crias (%):</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:numberbox id="txtMuesCriasPorc" runat="server" cssclass="cuadrotexto" Width="55px" maxvalor="20"
																																																																																																																								EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblMuesCriasMini" runat="server" cssclass="titulo">Muestreo de Crias (Min):</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:numberbox id="txtMuesCriasMini" runat="server" cssclass="cuadrotexto" Width="55px" maxvalor="20"
																																																																																																																								EsDecimal="False"></cc1:numberbox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																</TABLE>
																																																																																										</asp:panel></TD>
																																																																														</TR>
																																																																														<TR>
																																																																																				<TD style="WIDTH: 100%" colSpan="3">
																																																																																										<asp:panel id="panGenealogia2" runat="server" cssclass="titulo" Visible="False" Width="100%">
																																																																																																<TABLE id="TablaEspecies2" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblRegPrep" runat="server" cssclass="titulo">Registro Preparatorio:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbRegPrep" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblCtrlReg" runat="server" cssclass="titulo">Controla Registro:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbCtrlReg" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD vAlign="top" align="right">
																																																																																																																		<asp:Label id="lblCaract" runat="server" cssclass="titulo">Caracter�stica a Destacar:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<CC1:TEXTBOXTAB id="txtCaract" runat="server" cssclass="cuadrotexto" Width="150px"></CC1:TEXTBOXTAB></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblCaractPrec" runat="server" cssclass="titulo">Caracter�stica Precede al Nro:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbCaractPrec" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="HEIGHT: 21px" align="right">
																																																																																																																		<asp:Label id="lblAstado" runat="server" cssclass="titulo">Astado:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbAstado" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblMocho" runat="server" cssclass="titulo">Mocho:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbMocho" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblCtrlColor" runat="server" cssclass="titulo">Controla Color:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbCtrlColor" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblCtrlPelaje" runat="server" cssclass="titulo">Controla Pelaje:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbCtrlPelaje" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblPerf" runat="server" cssclass="titulo">Performance:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbPerf" runat="server" Width="75px">
																																																																																																																								<asp:ListItem Selected="True" Value="">(Ninguna)</asp:ListItem>
																																																																																																																								<asp:ListItem Value="0">Lechera</asp:ListItem>
																																																																																																																								<asp:ListItem Value="1">Carne</asp:ListItem>
																																																																																																																								<asp:ListItem Value="2">Ambas</asp:ListItem>
																																																																																																																		</cc1:combobox></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblSiemMult" runat="server" cssclass="titulo">Siembra Multiple:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbSiemMult" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblFlus" runat="server" cssclass="titulo">Flushing:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbFlus" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD style="HEIGHT: 1px" align="right" background="imagenes/formdivmed.jpg" colSpan="2"
																																																																																																																		height="1"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																						<TR>
																																																																																																												<TD align="right">
																																																																																																																		<asp:Label id="lblSani" runat="server" cssclass="titulo">Sanidad:</asp:Label>&nbsp;</TD>
																																																																																																												<TD style="WIDTH: 60%">
																																																																																																																		<cc1:combobox class="combo" id="cmbSani" runat="server" Width="70px"></cc1:combobox></TD>
																																																																																																						<TR>
																																																																																																												<TD align="right" background="imagenes/formdivmed.jpg" colSpan="2" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
																																																																																																						</TR>
																																																																																																</TABLE>
																																																																																										</asp:panel></TD>
																																																																														</TR>
																																																																								</TABLE>
																																																																		</asp:panel><ASP:PANEL id="panBotones" Runat="server">
																																																																								<TABLE width="100%">
																																																																														<TR>
																																																																																				<TD align="center">
																																																																																										<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
																																																																														</TR>
																																																																														<TR height="30">
																																																																																				<TD align="center"><A id="editar" name="editar"></A>
																																																																																										<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
																																																																																										<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																																																																																																Text="Baja"></asp:Button>&nbsp;
																																																																																										<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																																																																																																Text="Modificar"></asp:Button>&nbsp;
																																																																																										<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																																																																																																Text="Limpiar"></asp:Button></TD>
																																																																														</TR>
																																																																								</TABLE>
																																																																		</ASP:PANEL>
																																																																		<DIV></DIV>
																																																												</TD>
																																																						</TR>
																																																</TBODY>
																																										</TABLE>
																																										<!--- FIN CONTENIDO ---></td>
																																				<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
																														</tr>
																														<tr>
																																				<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
																																				<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
																																				<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
																														</tr>
																								</TBODY>
																		</table>
																		<!----------------- FIN RECUADRO ----------------->
																		<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX><asp:textbox id="hdnPage" runat="server"></asp:textbox><asp:textbox id="hdnId" runat="server"></asp:textbox><asp:textbox id="hdnAutorId" runat="server"></asp:textbox><asp:textbox id="hdnDireId" runat="server"></asp:textbox><asp:textbox id="hdnTeleId" runat="server"></asp:textbox><asp:textbox id="hdnMailId" runat="server"></asp:textbox></DIV>
												</form>
												<SCRIPT language="javascript">
		function mCargarRaza()
		{
		    var sFiltro = '';
		    if (document.all("cmbEspecie").value != '')
				sFiltro = document.all("cmbEspecie").value;
				//if (sFiltro != '')
		  	    		   	    
		       LoadComboXML("razas_cargar", sFiltro, "cmbRaza", "S");
	    }
	    function setCmbEspecie()
	    {
			if (document.all("cmbRaza").value != "")
			{
				var vstrRet = LeerCamposXML("razas", document.all("cmbRaza").value, "raza_espe_id").split("|");
				if(vstrRet!="")
				{
					document.all("cmbEspecie").value = vstrRet[0];
				}
			}		
	    }
		if (document.all["editar"]!= null)
			document.location='#editar';
												</SCRIPT>
												</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></FORM>
						</BODY>
</HTML>
