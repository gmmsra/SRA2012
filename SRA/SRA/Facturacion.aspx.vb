' Dario 2013-10-03 se coloca filtro en la carga del combo "tarjetas", cmbTarj
' Dario 2013-10-09 nuevo concepto automatico bonif laboratorio
Imports Business.Facturacion.ActividadesBusiness ' Dario 2013-07-02
Imports Entities.Facturacion.ComprobanteSobreTasaVtosEntity ' Dario 2013-09-13
Imports Entities.Facturacion.ParametosBonifLabEntity ' Dario 2013-10-15


Namespace SRA


Partial Class Facturacion
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub




    Protected WithEvents panFiltro As System.Web.UI.WebControls.Panel
    Protected WithEvents btnBusc As NixorControls.BotonImagen
    Protected WithEvents btnList As NixorControls.BotonImagen
    Protected WithEvents lblActiFil As System.Web.UI.WebControls.Label
    Protected WithEvents cmbActiFil As NixorControls.ComboBox
    Protected WithEvents lblFecha As System.Web.UI.WebControls.Label
    Protected WithEvents txtFecha As NixorControls.DateBox
    Protected WithEvents lblClieFil As System.Web.UI.WebControls.Label
    Protected WithEvents lblTipoComp As System.Web.UI.WebControls.Label
    Protected WithEvents cmbTipoComp As NixorControls.ComboBox
    Protected WithEvents lblNro As System.Web.UI.WebControls.Label
    Protected WithEvents txtCEmi As NixorControls.NumberBox
    Protected WithEvents lblSepa As System.Web.UI.WebControls.Label
    Protected WithEvents txtNro As NixorControls.NumberBox


    Protected WithEvents imgClose As System.Web.UI.WebControls.ImageButton



    Protected WithEvents lblMail As System.Web.UI.WebControls.Label
    Protected WithEvents txtMail As NixorControls.TextBoxTab
    Protected WithEvents txtMailRefe As NixorControls.TextBoxTab





    Protected WithEvents imgCerrarEnca As System.Web.UI.WebControls.ImageButton
    Protected WithEvents hdnTarjId As System.Web.UI.WebControls.TextBox
    Protected WithEvents hdnEncaId As System.Web.UI.WebControls.TextBox
    Protected WithEvents hdnDescBusqPop As System.Web.UI.WebControls.TextBox

    Protected WithEvents lblTotIVA As System.Web.UI.WebControls.Label
    Protected WithEvents lblTotIVARedu As System.Web.UI.WebControls.Label
    Protected WithEvents lblTotIVASTasa As System.Web.UI.WebControls.Label
    Protected WithEvents lblExentoConcep As System.Web.UI.WebControls.Label
    Protected WithEvents chkExentoConcep As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblSCargoConcep As System.Web.UI.WebControls.Label
    Protected WithEvents chkSCargoConcep As System.Web.UI.WebControls.CheckBox
    Protected WithEvents lblCtoCostoAranRRGG As System.Web.UI.WebControls.Label
    Protected WithEvents lblTramAranRRGG As System.Web.UI.WebControls.Label

    Protected WithEvents lblCCos As System.Web.UI.WebControls.Label
    Protected WithEvents cmbConcepAux As NixorControls.ComboBox

    Protected WithEvents cmbCcosAran As NixorControls.ComboBox
    Protected WithEvents cmbConcepAux2 As NixorControls.ComboBox



    Protected WithEvents btAltaCuot As System.Web.UI.WebControls.Button

    Protected WithEvents txtTotalSobretasa1 As NixorControls.NumberBox
    Protected WithEvents lblCriador As System.Web.UI.WebControls.Label


    Protected WithEvents panClieGene As System.Web.UI.WebControls.Panel
    Protected WithEvents pantblClieGene As System.Web.UI.WebControls.Panel

    Protected WithEvents divSinCargo As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents imgEspec As System.Web.UI.HtmlControls.HtmlAnchor
    Protected WithEvents Button1 As System.Web.UI.WebControls.Button




    Protected WithEvents txtSaldoCaCteVenc As NixorControls.NumberBox
    Protected WithEvents txtSaldoCtaSociVenc As NixorControls.NumberBox



    ' Dario 2013-09-12 para panel de lab bonif
    ' Dario 2013-10-23 cambio por bonif laboratorio

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"

    Private mstrTablaProfo As String = SRA_Neg.Constantes.gTab_Proformas

    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
    Private mstrTablaDetaAran As String = SRA_Neg.Constantes.gTab_ComprobAranc
    Private mstrTablaDetaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep
    Private mstrTablaVtos As String = SRA_Neg.Constantes.gTab_ComprobVtos
    Private mstrTablaAutor As String = SRA_Neg.Constantes.gTab_ComprobAutor
    ' Dario 2013-07-19 nueva tabla en el consult
    Private mstrTablaSobreTasaVtos As String = SRA_Neg.Constantes.gTab_ComprobSobreTasaVtos
    ' Dario 2013-10-25 nueva tabla 
    Private mstrTablaSaldosACta As String = SRA_Neg.Constantes.gTab_SaldosACuenta


    Private mintDiasAnio As Integer = 365

    Private mstrTablaTempDeta As String = "comprob_aranc_concep"

    Private mstrParaPageSize As Integer
    Private mintRecalcuProforma As Integer
    Private mstrCmd As String
    Private mstrConn As String
    Private mdsDatos As DataSet
    Private mobj As SRA_Neg.Facturacion

    'forma de pago
    Private Const mintTarj As Integer = 3
    Private Const mintCtaCte As Integer = 2
    Private Const mintContado As Integer = 1
    Private Const mintDif As Integer = 4

    Private mstrCobraTipo As String
    Private mstrCobraId As Integer
    Private mstrCobraOtro As String


    Public Enum Formulas
        AjustePorConvenio = 1
        DescFactSinCargo = 3
        DescuentoFacturacionExenta = 14
        AdicionalSuspensionServicios = 26
    End Enum

    Public Enum CamposEspeciales As Integer
        exento = 0
        iva = 1
        soci = 2
        precio_socio = 3
        miem = 4
        color = 5
        desc = 6
        socio_falle_precio_socio = 7
        soli_tram_precio_socio = 8
        obser = 9
        precio_no_socio = 10 ' fallec, renun en tram, baja, susp

    End Enum
    Private Enum CamposTurnos
        ID = 0
        TURN_OTO = 1
        TURN_REC = 2
        FECHA_REC = 3
        FECHA_COSE = 4
        FECHA_FISE = 5
        SOB_PFF = 6
        REC_EFF = 7
        DTO_OPP = 8
        DTO_OPC = 9
        REC_PTU = 10
        REGICONT = 11
        clcu_id = 12
        cant = 13
    End Enum
    Public Enum TipoPeriodo As Integer
        mes = 1
        dia = 2
    End Enum
    Private Enum TablasProformas
        Proformas = 0
        Proforma_deta = 1
        Proforma_aranc = 2
        Proforma_concep = 3
        Proforma_autor = 4
    End Enum
    Private Enum TablasTurnos
        inturfac_bov = 0
        inturfac_equ = 1
    End Enum
    Private Enum TipoComprobGene As Integer
        Factura = 0
        Proforma = 1
        ProformaAuto = 2
        ProformaExcede = 3
    End Enum
    Public Enum TiposComprobantes As Integer
        Factura = 29
        Proforma = 28
    End Enum

    Private Enum OrdenItems As Integer
        arancel = 1
        sobretasas = 2
        conceptos = 3
        conceptosAuto = 4
    End Enum
    Private Enum Columnas As Integer
        id = 1
        tipo = 2
        codi_aran_concep = 3
        desc_aran_concep = 4
        cant = 5
        cant_sin_cargo = 6
        anim_no_fact = 7
        impo = 8
        fecha = 9
        inic_rp = 10
        fina_rp = 11
        tram = 12
        acta = 13
        ccos_codi = 14
        exen = 15
        sin_carg = 16
        aran_concep_id = 17
        ccos_id = 18
        impo_bruto = 19
        tipo_IVA = 20
        tasa_iva = 21
        impo_ivai = 22
        desc_ampl = 23
        autom = 24
        desc_fecha = 25
        rela_id = 26
        borra = 27
        orden = 28
        turnos = 28
        unit_s_iva = 30
        unit_c_iva = 31
    End Enum
    Public Enum Actividades As Integer
        Varios = 1 ' administracion por ejemplo, pero en relidad son todos los demas
        RRGG = 3
        Laboratorio = 4
        AlumnosISEA = 6
        Expo = 7
        EstPreLimAntecGen = 8
        AlumnosCEIDA = 10
    End Enum
    Private Enum Panta As Integer
        Encabezado = 1
        Pie = 2
        Cuotas = 3
        Autorizacion = 4
        Arancel = 5
        Concepto = 6
        Tarjeta = 7
    End Enum

    Private Enum CamposEstado As Integer
        saldo_cta_cte = 0
        saldo_social = 1
        saldo_total = 2
        imp_a_cta_cta_cte = 3
        imp_a_cta_cta_social = 4
        lim_saldo = 5 ' si es nulo, el cliente no opera con Cta. Cte.
        color = 6
        Saldo_excedido = 7
    End Enum
#End Region

#Region "Operaciones sobre la Pagina"
    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            mstrConn = clsWeb.gVerificarConexion(Me)
            mInicializar()
            mInicializarFacturacion()
            mSetearClie()

            If (Not Page.IsPostBack) Then
                Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                mobj.CentroEmisorNro(mstrConn)

                Me.hdnIdEspecieLaboBonif.Text = ""

                ImgbtnGenerar.Attributes.Add("onclick", "return(mExportar());")

                mSetearMaxLength()
                Session(mSess(mstrTabla)) = Nothing
                mdsDatos = Nothing
                mAgregar()

                ' Dario 2013-01-23 para que cargue el combo aryividad del usuario se paso aca esta funcion 
                mCargarCombos()
                ' fin cambio dario
                ' Dario 2013-10-08 cambio para tomar la actividad desde el hiden y no del combo en el js mSetearTextoGrabar
                hdnActiId.Text = cmbActi.Valor
                ' fin cambio 

                ' Dario 2013-04-29 Seteo para por defecto poner contado en cbo formas de pago
                cmbFormaPago.Valor = mintContado
                ' fin cambio dario

                lblFechaIngreso.Text = Today.ToString("dd/MM/yyyy")

                mobj.IVA(mstrConn, txtFechaIva.Text)

                clsWeb.gInicializarControles(Me, mstrConn)
                mSetearControles()

                If (cmbActi.Valor = Actividades.Expo) Then
                    clsWeb.gCargarRefeCmb(mstrConn, "exposiciones", cmbExpo, "S")
                End If

                ' Dario 2013-01-23 para que cargue el combo actividad del usuario
                ' se genera la carga de estos elemento que normalmente se cargan por 
                ' javascript con el onchange del combo actividad
                If (cmbActi.Valor = Actividades.Laboratorio) Then
                    Me.DivimgCtrlTurn.Style.Add("display", "inline")
                    Me.DivimgCtrlAnalisis.Style.Add("display", "inline")
                Else
                    Me.DivimgCtrlTurn.Style.Add("display", "none")
                    Me.DivimgCtrlAnalisis.Style.Add("display", "none")
                End If
                ' Dario 2013-10-23 oculto el control que muestra si corresponde 
                ' la simulacion de la NC por descuento del laboratorio
                Me.trSimulacionNCAuto.Style.Add("display", "none")
                Me.hdnGeneraNCAuto.Text = "No"
                Me.hdnImporteNCAuto.Text = "0"
                Me.hdnImporteDescuentoAplicadoNCAuto.Text = "0"

                If (cmbActi.SelectedIndex > 0 And Me.txtFechaValor.Text <> "") Then
                    Dim sFiltro As String
                        sFiltro = " @acti_id=" & cmbActi.SelectedValue.ToString() _
                            & ",@peli_vige_fecha='" _
                            & Convert.ToDateTime(Me.txtFechaValor.Fecha).ToString("yyyy/MM/dd") + "'"
                        clsWeb.gCargarRefeCmb(mstrConn, "precios_lista_fact", cmbListaPrecios, "", sFiltro, False)
                End If
                ' fin cambio Dario

            Else
                mCargarCentroCostos(False)
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                    mSetearControles()
                    mCotizacionDolar()
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


    '   Private Sub mEstablecerPerfil()
    '      Dim lbooPermiAlta As Boolean
    '      Dim lbooPermiModi As Boolean

    '      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
    '      'Response.Redirect("noaccess.aspx")
    '      'End If

    '      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
    '      'btnAlta.Visible = lbooPermiAlta

    '      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

    '      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificaci�n, String), (mstrConn), (Session("sUserId").ToString()))
    '      'btnModi.Visible = lbooPermiModi

    '      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
    '      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
    '   End Sub

    Private Sub mSetearMaxLength()
        Dim lstrLongs As Object
        Dim lintCol As Integer

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDeta)
        txtObser.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "code_obse")
        txtTarjNro.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "code_tarj_nume")
        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDetaConcep)
        txtDescAmpliConcep.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "coco_desc_ampl")

        lstrLongs = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDetaAran)
        txtRPAranDdeRRGG.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "coan_inic_rp")
        txtRPAranHtaRRGG.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "coan_fina_rp")
        txtActaAranRRGG.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "coan_acta")
        txtDescAmpliAran.MaxLength = clsSQLServer.gObtenerLongitud(lstrLongs, "coan_desc_ampl")
    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Sub mInicializar()


        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDetalle.PageSize = Convert.ToInt32(mstrParaPageSize)

        mintRecalcuProforma = clsSQLServer.gCampoValorConsul(mstrConn, "parametros_consul", "para_recalcu_prof_dias")

        'Clave unica
        usrClie.ColClaveUnica = True
        usrClie.FilClaveUnica = True
        'Numero de cliente
        usrClie.ColClieNume = True
        'muestra desc
        usrClie.MuestraDesc = False

        'Fantasia
        '26/08/2010
        usrClie.ColFanta = False
        usrClie.FilFanta = False
        'Numero de CUIT
        usrClie.ColCUIT = True
        usrClie.FilCUIT = True
        'Numero de documento
        'usrClie.ColDocuNume = True
        ' usrClie.FilDocuNume = True
        'Numero de socio
        ' usrClie.ColSociNume = True
        ' usrClie.FilSociNume = True

    End Sub
#End Region

#Region "Seteo de Controles"

    Private Sub mSelecProformaoFactura()
        ' debe haber seleccionado del combo, sino por default se toma factura
        If cmbComp.Valor = 0 Or cmbComp.Valor = TiposComprobantes.Factura Then
            mobj.pTipoComprobGenera = TipoComprobGene.Factura
            'hdnComprobante.Text = "Factura"
            'ImgbtnGenerar.Attributes.Add("onclick", "if(!confirm('Confirma la generaci�n de la factura?. '))return false;")
        Else
            'hdnComprobante.Text = "Proforma"
            mobj.pTipoComprobGenera = TipoComprobGene.Proforma
            'ImgbtnGenerar.Attributes.Add("onclick", "if(!confirm('Confirma la generaci�n de la proforma?. '))return false;")
        End If
    End Sub
    Private Sub mMostrarSolapaArancel(ByVal pboolArancel As Boolean)
        panAran.Visible = pboolArancel
        If pboolArancel Then
            btnAran.ImageUrl = "imagenes/tabArancel1.gif"
            btnConcep.ImageUrl = "imagenes/tabConcep0.gif"
        Else
            btnAran.ImageUrl = "imagenes/tabArancel0.gif"
            btnConcep.ImageUrl = "imagenes/tabConcep1.gif"
        End If
        panConcep.Visible = Not pboolArancel
    End Sub
    Private Sub mRRGGoLabComprobVisible(ByVal pboolVisible As Boolean)
        panRRGGoLab.Visible = pboolVisible
    End Sub
    Private Sub mShowTabs(ByVal origen As Byte)
        panDato.Visible = True
        panDeta.Visible = False
        panAran.Visible = False
        panConcep.Visible = False
        panPie.Visible = False
        panCuotas.Visible = False
        panAutoriza.Visible = False
        panCabecera.Visible = False
        panEnca.Visible = False
        panPagoTarjeta.Visible = False
        PanBotonesNavegDeta.Visible = False
        PanBotonesNavegCuot.Visible = False
        panBotonesNavegAutiza.Visible = False
        panBotonesPie.Visible = False
        PanBotonesPagoTarjeta.Visible = False
        btnAran.ImageUrl = "imagenes/tabArancel1.gif"
        btnConcep.ImageUrl = "imagenes/tabConcep0.gif"

        Select Case origen
            Case 1
                'encabezado
                panCabecera.Visible = True
                ' Dario 2013-12-10 se bloquea el contro raza cuando es laboratorio y tenia un valor anterior
                ' no se deja cambiar sebun lo solicitado.
                If (Not Me.cmbRaza.Valor Is System.DBNull.Value) Then
                    If (Me.cmbRaza.SelectedValue.Trim().Length > 0 And cmbActi.Valor = Actividades.Laboratorio) Then
                        Me.cmbRaza.Enabled = False
                    Else
                        Me.cmbRaza.Enabled = True
                    End If
                Else
                    Me.cmbRaza.Enabled = True
                End If

            Case 5
                    'Detalle / arancel
                    panDeta.Visible = True
                    panAran.Visible = True
                    mControlesIVA()
                    panEnca.Visible = True
                    PanBotonesNavegDeta.Visible = True
                    hdnFechaValor.Text = txtFechaValor.Text
                    'Dario 2013-09-12 para ocultar o mostrar el div laboratorio bonif
                    If (Me.hdnMuestraContadorLabiratorio.Text = "Si") Then
                        Me.divTotalizador.Style.Add("display", "inline")
                        Me.mCompletarPanelLabBonif()
                        'Me.divFacturado.Style.Add("display", "inline")
                        'Me.divTotalAcum.Style.Add("display", "inline")
                    Else
                        Me.divTotalizador.Style.Add("display", "none")
                        'Me.divFacturado.Style.Add("display", "none")
                        'Me.divTotalAcum.Style.Add("display", "none")
                    End If
            Case 6
                    'Detalle / conceptos
                    btnConcep.ImageUrl = "imagenes/tabConcep1.gif"
                    btnAran.ImageUrl = "imagenes/tabArancel0.gif"
                    panDeta.Visible = True
                    panConcep.Visible = True
                    mControlesIVA()
                    panEnca.Visible = True
                    PanBotonesNavegDeta.Visible = True

            Case 3
                    'cuotas
                    panEnca.Visible = True
                    panCuotas.Visible = True
                    PanBotonesNavegCuot.Visible = True
            Case 4
                    'autorizaciones
                    panEnca.Visible = True
                    panAutoriza.Visible = True
                    panBotonesNavegAutiza.Visible = True
            Case 2
                    'pie
                    panEnca.Visible = True
                    panPie.Visible = True
                    panBotonesPie.Visible = True
            Case 7
                    'pago tarjeta
                    panEnca.Visible = True
                    panPagoTarjeta.Visible = True
                    PanBotonesPagoTarjeta.Visible = True
                    chkVtaTele.Checked = Not chkTPers.Checked = True
                    txtTarjCuo.Valor = 1
                    If hdnDatosTarjPop.Text <> "" Then
                        txtTarjNro.Valor = mobj.ObtenerValorCampo(mstrConn, "tarjetas_clientes", "tacl_nume", hdnDatosTarjPop.Text)
                        cmbTarj.Valor = mobj.ObtenerValorCampo(mstrConn, "tarjetas_clientes", "tacl_tarj_id", hdnDatosTarjPop.Text)
                        hdnTarjClieId.Text = hdnDatosTarjPop.Text
                        hdnDatosTarjPop.Text = ""
                    End If
                    If hdnTarjClieId.Text <> "" Then
                        txtTarjNro.Enabled = False
                    Else
                        txtTarjNro.Enabled = True
                    End If

        End Select
    End Sub
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, Optional ByVal pboolConcepAuto As Boolean = False)
        Select Case pstrTabla

            Case mstrTablaDetaConcep
                btnBajaConcep.Enabled = Not (pbooAlta)
                If pboolConcepAuto Then
                    ' no pueden modificarse conceptos autom., solo eliminarse
                    btnModiConcep.Enabled = False
                Else
                    btnModiConcep.Enabled = Not (pbooAlta)
                End If
                btnAltaConcep.Enabled = pbooAlta


            Case mstrTablaDetaAran
                btnBajaAran.Enabled = Not (pbooAlta)
                If mobj.pAranRRGGSobretasa <> 0 Then

                    ' no pueden modificarse sobretasas, solo eliminarse
                    btnModiAran.Enabled = False
                Else
                    btnModiAran.Enabled = Not (pbooAlta)
                End If
                btnAltaAran.Enabled = pbooAlta

            Case mstrTablaVtos
                btnBajaCuot.Enabled = Not (pbooAlta)
                btnModiCuot.Enabled = Not (pbooAlta)
                btnAltaCuot.Enabled = pbooAlta


            Case Else
                ImgbtnGenerar.Enabled = pbooAlta
                mHabilitarCobranza(pbooAlta)


        End Select
    End Sub
    Private Function mSess(ByVal pstrTabla As String) As String
        If hdnSess.Text = "" Then
            hdnSess.Text = pstrTabla & Now.Millisecond.ToString
        End If
        Return (hdnSess.Text)
    End Function
    Private Sub mAgregar()
        mLimpiar()
        mInicializaFactura()
        mLlenaComboListaPrecios(txtFechaValor.Fecha, cmbActi.Valor)
        mInicializarVariablesCalculo()
    End Sub
    Private Sub mAgregarP()
        mLimpiar(1)
        mInicializaFactura(False)
        mInicializarVariablesCalculo()
    End Sub
    Private Sub mInicializaFactura(Optional ByVal pboolTodo As Boolean = True)
        mobj.Limpiar(pboolTodo)
        mInicializarFacturacion()
        mShowTabs(Panta.Encabezado)
    End Sub
    Private Sub mInicializarFacturacion()
        mobj = New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
    End Sub
    Private Sub mCerrar()
        mAgregar()
    End Sub

#End Region

#Region "Operaciones sobre el DataGrid"

    Public Sub grdDetalle_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDetalle.EditItemIndex = -1
            If (grdDetalle.CurrentPageIndex < 0 Or grdDetalle.CurrentPageIndex >= grdDetalle.PageCount) Then
                grdDetalle.CurrentPageIndex = 0
            Else
                grdDetalle.CurrentPageIndex = E.NewPageIndex
            End If

            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta)
            grdDetalle.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub grdAutoriza_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdAutoriza.ItemDataBound
        Try
            If e.Item.ItemIndex <> -1 Then
                Dim oComen As NixorControls.TextBoxTab = e.Item.FindControl("txtComen")
                ' Dim ochkApro As NixorControls.CheckBoxSimple = e.Item.FindControl("chkApro")
                With CType(e.Item.DataItem, DataRowView).Row
                    DirectCast(e.Item.FindControl("chkApro"), CheckBox).Checked = .Item("coau_auto")
                        oComen.Valor = .Item("coau_obse").ToString

                    If .IsNull("coau_id") Then
                        .Item("coau_id") = -1 * e.Item.ItemIndex
                        e.Item.Cells(0).Text = .Item("coau_id")
                    End If


                End With
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub grdCuotas_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdCuotas.EditItemIndex = -1
            If (grdCuotas.CurrentPageIndex < 0 Or grdCuotas.CurrentPageIndex >= grdCuotas.PageCount) Then
                grdCuotas.CurrentPageIndex = 0
            Else
                grdCuotas.CurrentPageIndex = E.NewPageIndex
            End If
            grdCuotas.DataSource = mdsDatos.Tables(mstrTablaVtos)
            grdCuotas.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mCompletarPanelLabBonif()
        Dim _FacturacionBusiness As New Business.Facturacion.FacturacionBusiness



        Dim sRetResultPanel() As String = _FacturacionBusiness.GetDatosPanelLaboratorioAcumEspecie(Convert.ToInt32(cmbActi.Valor) _
                                                                                                 , Convert.ToInt32(Me.hdnClieId.Text) _
                                                                                                 , Convert.ToInt32(Me.cmbAran.Valor) _
                                                                                                 , Convert.ToDateTime(Me.txtFechaValor.Fecha) _
                                                                                                 , mdsDatos).Split(";")

        ' Dario 2013-12-09 se comenta y se cambia por otra cosa
        'Me.lblTotalizadorTit.Text = sRetResultPanel(0)
        '"Cantidad de an�lisis Acumulados"
        Me.lblTotalizadorTit.Text = "Cantidad de an�lisis Acumulados " & sRetResultPanel(8) & "(" _
                                  & Convert.ToDateTime(sRetResultPanel(5)).ToString("dd/MM/yyyy") & " - " _
                                  & Convert.ToDateTime(sRetResultPanel(7)).ToString("dd/MM/yyyy") & ")"
        Me.lblTotalizadorCantLab.Text = sRetResultPanel(1)
        Me.lblFacturadoDato.Text = sRetResultPanel(2)
        Me.lblTotalAcumDato.Text = sRetResultPanel(3)
        Me.hdnIdEspecieLaboBonif.Text = sRetResultPanel(4)

    End Sub

    ' Dario 2014-02-07 
    ' Funcion que retorna el id de especie si el arancel aplica descuento o bonificacion de laboratorio
    Public Function mGetEspecieArancelConBonif(ByVal idArancel As Int32) As String
        Dim _FacturacionBusiness As New Business.Facturacion.FacturacionBusiness

        Dim sRetResultPanel() As String = _FacturacionBusiness.GetDatosPanelLaboratorioAcumEspecie(Convert.ToInt32(cmbActi.Valor) _
                                                                                                 , Convert.ToInt32(Me.hdnClieId.Text) _
                                                                                                 , Convert.ToInt32(idArancel) _
                                                                                                 , Convert.ToDateTime(Me.txtFechaValor.Fecha) _
                                                                                                 , mdsDatos).Split(";")
        Return sRetResultPanel(4)

    End Function

        Public Sub mEditarDatosDetalle(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
            ' si es un arancel de turnos, llenar los hdl correspondientes (equi / bov)
            '.Item("temp_rela_id")

            Try
                grdDetalle.CurrentPageIndex = 0

                Dim ldrTemp As DataRow
                hdnDetaTempId.Text = E.Item.Cells(1).Text
                ldrTemp = mdsDatos.Tables(mstrTablaTempDeta).Select("temp_id=" & hdnDetaTempId.Text)(0)

                With ldrTemp
                    If .Item("temp_tipo") = "A" Or .Item("temp_tipo") = "S" Then  ' aranceles 
                        hdnDetaTempTipo.Text = .Item("temp_tipo")
                        mobj.AranRRGG(mstrConn, .Item("temp_aran_concep_id"), IIf(.IsNull("temp_sobre_auto"), 0, 1))
                        If mobj.pAranRRGGSobretasa <> 0 Then
                            mCargarAranceles(1)
                        Else
                            mCargarArancelesyConceptos()
                        End If
                        cmbAran.Valor = .Item("temp_aran_concep_id")
                        mSetearDatosArancel(IIf(.IsNull("temp_sobre_auto"), 0, 1))
                        ' Dario 2013-09-12 cambio para que aparezca y recalcule los totales del
                        ' panel de bonificacion del laboratorio
                        Me.divTotalizador.Style.Add("display", "none")
                        Me.hdnMuestraContadorLabiratorio.Text = "No"
                        Me.hdnIdEspecieLaboBonif.Text = ""
                        If (Not .Item("temp_AcancelABonif") Is DBNull.Value) Then
                            If (.Item("temp_AcancelABonif") = "Si") Then
                                Me.divTotalizador.Style.Add("display", "inline")
                                Me.hdnMuestraContadorLabiratorio.Text = "Si"
                                Me.hdnIdEspecieLaboBonif.Text = .Item("temp_IdEspecie")
                                Me.mCompletarPanelLabBonif()
                            End If
                        End If

                        If mobj.pRRGG Then
                            txtAranSTCodi.Valor = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_codi", .Item("temp_aran_concep_id").ToString)
                            txtAranSTDesc.Valor = .Item("temp_desc_aran_concep")
                            txtCantNoFacAranRRGG.Valor = .Item("temp_anim_no_fact")
                            txtFechaAranRRGG.Fecha = IIf(.Item("temp_fecha") Is DBNull.Value, "", .Item("temp_fecha"))
                            txtRPAranDdeRRGG.Valor = .Item("temp_inic_rp")
                            txtRPAranHtaRRGG.Valor = .Item("temp_fina_rp")
                            txtActaAranRRGG.Valor = .Item("temp_acta")
                            lblFechaAranRRGG.Text = .Item("temp_desc_fecha")
                        End If
                        If Not .IsNull("temp_sobre_impo") Then
                            txtTotalSobreTasa.Text = Format(.Item("temp_sobre_impo"), "###0.00")
                            txtTasaSobreTasa.Text = Format(.Item("temp_sobre_porc"), "###0.00")
                        Else
                            txtTotalSobreTasa.Text = "0.00"
                            txtTasaSobreTasa.Text = "0.00"
                        End If
                        hdnTotalSobreTasa.Text = txtTotalSobreTasa.Text
                        hdnTasaSobreTasa.Text = txtTasaSobreTasa.Text
                        txtCantAran.Valor = .Item("temp_cant")
                        txtCantSCAran.Valor = .Item("temp_cant_sin_cargo")
                        ' txtPUnitAran.Valor = .Item("temp_unit_impo")
                        ' txtImpoAran.Valor = .Item("temp_impo")
                        chkExentoAran.Checked = IIf(.Item("temp_exen") = "Si", 1, 0)
                        chkSCargoAran.Checked = IIf(.Item("temp_sin_carg") = "Si", 1, 0)
                        txtDescAmpliAran.Valor = .Item("temp_desc_ampl")
                        txtTramNume.Valor = .Item("temp_tram").ToString
                        ' laboratorios - turnos
                        If mobj.pLabora Then
                            Select Case .Item("temp_turnos").ToString
                                Case "B"
                                    hdnDatosPopTurnoBov.Text = .Item("temp_rela_turno_id")
                                Case "E"
                                    hdnDatosPopTurnoEqu.Text = .Item("temp_rela_turno_id")
                            End Select
                            txtTurnosSP.Valor = .Item("temp_turno_SP")
                            txtTurnosRT.Valor = .Item("temp_turno_RT")
                            txtTurnosRTU.Valor = .Item("temp_turno_RTU")
                            txtTurnosDP.Valor = .Item("temp_turno_DP")
                            txtTurnosDC.Valor = .Item("temp_turno_DC")
                        End If

                        mLimpiarDetaConcep(False)
                        mMostrarSolapaArancel(True)
                        'precio unitario sin iva
                        txtPUnitAranSIVA.Valor = Format(.Item("temp_unit_impo"), "###0.00")
                        'muestra el precio unitario con o sin IVA segun la cond. del cliente 
                        If usrClie.DiscrimIVA Then
                            txtPUnitAran.Valor = Format(.Item("temp_unit_impo"), "###0.00")
                        Else
                            txtPUnitAran.Valor = Format(.Item("temp_unit_c_iva"), "###0.00")
                        End If
                        'tasa iva
                        hdnTasaIVAAran.Text = .Item("temp_tasa_iva")
                        'importe total sin iva
                        hdlImpoIvaAran.Text = CType(.Item("temp_impo_ivai"), Double) - CType(.Item("temp_impo"), Double)
                        If usrClie.DiscrimIVA Then
                            'tasa iva
                            txtTasaIVAAran.Valor = Format(CType(.Item("temp_tasa_iva"), Double), "###0.00")
                            'importe iva
                            txtImpoIvaAran.Valor = Format(CType(hdlImpoIvaAran.Text, Double), "###0.00")
                            'importe total 
                            txtImpoAran.Valor = Format(CType(.Item("temp_impo"), Double), "###0.00")
                        Else
                            'importe total neto
                            txtImpoAran.Valor = Format(CType(.Item("temp_impo_ivai"), Double), "###0.00")

                        End If
                        mSetearEditor(mstrTablaDetaAran, False)
                    Else ' concepto
                        hdnDetaTempTipo.Text = .Item("temp_tipo")
                        cmbConcep.Valor = .Item("temp_aran_concep_id")
                        mCargarCentroCostos(True)
                        cmbCCosConcep.Valor = IIf(.Item("temp_ccos_id") Is DBNull.Value, "", .Item("temp_ccos_id"))
                        txtImporteConcep.Valor = .Item("temp_impo")
                        txtDescAmpliConcep.Valor = .Item("temp_desc_ampl")
                        If Not .IsNull("temp_sobre_impo") Then
                            txtTotalSobreTasaC.Text = Format(.Item("temp_sobre_impo"), "###0.00")
                            txtTasaSobreTasaC.Text = Format(.Item("temp_sobre_porc"), "###0.00")
                        Else
                            txtTotalSobreTasaC.Text = "0.00"
                            txtTasaSobreTasaC.Text = "0.00"
                        End If
                        ' Dario 2013-09-12 cambio para que aparezca y recalcule los totales del
                        ' panel de bonificacion del laboratorio
                        Me.divTotalizador.Style.Add("display", "none")
                        Me.hdnMuestraContadorLabiratorio.Text = "No"
                        Me.hdnIdEspecieLaboBonif.Text = ""
                        If (Not .Item("temp_AcancelABonif") Is DBNull.Value) Then
                            If (.Item("temp_AcancelABonif") = "Si") Then
                                Me.divTotalizador.Style.Add("display", "inline")
                                Me.hdnMuestraContadorLabiratorio.Text = "Si"
                                Me.hdnIdEspecieLaboBonif.Text = .Item("temp_IdEspecie")
                                Me.mCompletarPanelLabBonif()
                            End If
                        End If
                        hdnTotalSobreTasaC.Text = txtTotalSobreTasaC.Text
                        hdnTasaSobreTasaC.Text = txtTasaSobreTasaC.Text
                        'tasa iva
                        hdnTasaIVAConcep.Text = .Item("temp_tasa_iva")
                        'importe total sin iva
                        hdnImpoIvaConcep.Text = CType(.Item("temp_impo_ivai"), Double) - CType(.Item("temp_impo"), Double)
                        If usrClie.DiscrimIVA Then
                            'tasa iva
                            txtTasaIVAConcep.Valor = Format(CType(.Item("temp_tasa_iva"), Double), "###0.00")
                            'importe iva
                            txtImpoIvaConcep.Valor = Format(CType(hdnImpoIvaConcep.Text, Double), "###0.00")
                        End If
                        mCalcularImporteConceptos("P")
                        mLimpiarDetaAran(False)
                        mMostrarSolapaArancel(False)
                        Dim lboolConcepAuto As Boolean
                        lboolConcepAuto = mobj.EsConceptoAutom(ldrTemp)
                        mSetearEditor(mstrTablaDetaConcep, False, lboolConcepAuto)
                    End If

                End With


            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

        Public Sub mEditarDatosCuotas(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            Dim ldrCuotas As DataRow
            hdnCuotasId.Text = E.Item.Cells(1).Text
            ldrCuotas = mdsDatos.Tables(mstrTablaVtos).Select("covt_id=" & hdnCuotasId.Text)(0)

            With ldrCuotas
                txtCuotFecha.Fecha = .Item("covt_fecha")
                txtCuotPorc.Valor = .Item("covt_porc")
                txtCuotImporte.Valor = .Item("covt_impo")

            End With

            mSetearEditor(mstrTablaVtos, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


#End Region

#Region "Opciones de ABM"

    Public Sub mCrearDataSet(ByVal pstrId As String)

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDeta
        mdsDatos.Tables(2).TableName = mstrTablaDetaAran
        mdsDatos.Tables(3).TableName = mstrTablaDetaConcep
        mdsDatos.Tables(4).TableName = mstrTablaVtos
        mdsDatos.Tables(5).TableName = mstrTablaAutor
        mdsDatos.Tables(7).TableName = mstrTablaSobreTasaVtos ' Dario 2013-07-19

        mdsDatos.Tables.Add(mstrTablaTempDeta)
        With mdsDatos.Tables(mstrTablaTempDeta)

            Dim lintCol As DataColumn = .Columns.Add("temp_id", Type.GetType("System.Int32"))
            lintCol.AllowDBNull = False
            lintCol.Unique = True
            lintCol.AutoIncrement = True
            lintCol.AutoIncrementSeed = 1
            lintCol.AutoIncrementStep = 1


            .Columns.Add("temp_tipo", System.Type.GetType("System.String"))
            .Columns.Add("temp_codi_aran_concep", System.Type.GetType("System.String"))
            .Columns.Add("temp_desc_aran_concep", System.Type.GetType("System.String"))
            .Columns.Add("temp_desc_aran_concep_cortada", System.Type.GetType("System.String"))
            '23/08/2010 Agregado para tratar el pedido de autorizacion
            .Columns.Add("temp_aran_rrgg_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_cant", System.Type.GetType("System.Double"))
            .Columns.Add("temp_sobre_impo", System.Type.GetType("System.Double"))
            .Columns.Add("temp_sobre_porc", System.Type.GetType("System.Double"))
            .Columns.Add("temp_cant_sin_cargo", System.Type.GetType("System.Double"))
            .Columns.Add("temp_anim_no_fact", System.Type.GetType("System.Double"))
            .Columns.Add("temp_unit_impo", System.Type.GetType("System.Double"))
            .Columns.Add("temp_impo", System.Type.GetType("System.Double"))
            .Columns.Add("temp_fecha", System.Type.GetType("System.DateTime"))
            .Columns.Add("temp_inic_rp", System.Type.GetType("System.String"))
            .Columns.Add("temp_fina_rp", System.Type.GetType("System.String"))
            .Columns.Add("temp_tram", System.Type.GetType("System.String"))
            .Columns.Add("temp_acta", System.Type.GetType("System.String"))
            .Columns.Add("temp_ccos_codi", System.Type.GetType("System.String"))
            .Columns.Add("temp_exen", System.Type.GetType("System.String"))
            .Columns.Add("temp_sin_carg", System.Type.GetType("System.String"))
            .Columns.Add("temp_aran_concep_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_ccos_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_impo_bruto", System.Type.GetType("System.Double"))
            .Columns.Add("temp_tipo_IVA", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_tasa_iva", System.Type.GetType("System.Double"))
            .Columns.Add("temp_impo_ivai", System.Type.GetType("System.Double"))
            .Columns.Add("temp_desc_ampl", System.Type.GetType("System.String"))
            .Columns.Add("temp_auto", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_desc_fecha", System.Type.GetType("System.String"))
            .Columns.Add("temp_rela_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_borra", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_orden", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_turnos", System.Type.GetType("System.String"))
            .Columns.Add("temp_unit_s_iva", System.Type.GetType("System.Double"))
            .Columns.Add("temp_unit_c_iva", System.Type.GetType("System.Double"))
            .Columns.Add("temp_impo_Nosocio_aran", System.Type.GetType("System.Double"))
            .Columns.Add("temp_sobre_auto", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_rela_turno_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_rela_id_aux", System.Type.GetType("System.Int32"))
            'turnos
            .Columns.Add("temp_turno_SP", System.Type.GetType("System.Double"))
            .Columns.Add("temp_turno_RT", System.Type.GetType("System.Double"))
            .Columns.Add("temp_turno_RTU", System.Type.GetType("System.Double"))
            .Columns.Add("temp_turno_DP", System.Type.GetType("System.Double"))
            .Columns.Add("temp_turno_DC", System.Type.GetType("System.Double"))
            .Columns.Add("temp_deta_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_deta_prfr_id", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_turno_tipo", System.Type.GetType("System.String"))
            ' Dario 2013-04-26 agregado precio adherente
            .Columns.Add("temp_impo_Adhe_aran", System.Type.GetType("System.Double"))
            ' Dario 2013-09-12 se agrega especie y bool por si se tiene que contar para 
            ' descuento laboratorio
            .Columns.Add("temp_IdEspecie", System.Type.GetType("System.Int32"))
            .Columns.Add("temp_AcancelABonif", System.Type.GetType("System.String"))
        End With
        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If
        grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta)
        grdDetalle.DataBind()

        grdCuotas.DataSource = mdsDatos.Tables(mstrTablaVtos)
        grdCuotas.DataBind()

        Session(mstrTabla) = mdsDatos


    End Sub
    Private Sub mBajaItemTempDeta(ByVal pintId As Integer, Optional ByVal pboolRemove As Boolean = False)
        With mdsDatos.Tables(mstrTablaTempDeta)
            If pboolRemove Then
                ' elimina definitivamente la fila (es para sobretasas o conceptos eliminados por el programa, no por el usuario)
                .Rows.Remove(.Select("temp_id=" & pintId)(0))
            Else
                .Select("temp_id=" & pintId)(0).Item("temp_borra") = 1
            End If
        End With

        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta).DefaultView
        grdDetalle.DataBind()
        mCalcularTotal()
        mCalcularCantTurnos()
        mCalcularSaldoCuotas()

    End Sub
    Private Function mAltaReal() As String
        'Parte del alta que comparten tanto la generacion sin cobranza como la que tiene cobranza
        Dim lDsDatos, lDsDatosNC, lDsDatosTemp As DataSet
        Dim lstrId, lstrIdNC As String
        Dim bitOperOk As Boolean = False
        Dim bitPrimerRegistro As Boolean = True
        Dim lstrtabla As String = "comprob_aranceles_deta"
        Dim lTransac As SqlClient.SqlTransaction
        Dim listComprobanteSobreTasaVtos As New ArrayList

        ' declaro la variabe en donde guardo la fecha de valor original
        Dim fechaValorOrig As DateTime = Me.txtFechaValor.Fecha

        Try
            If mobj.pTipoComprobGenera <> TipoComprobGene.Factura Then
                mstrTabla = mstrTablaProfo
                lstrtabla = "proforma_aranceles_deta"
            Else ' Dario 2013-10-08 por error en los mensajes de generacion de factura / proforma
                hdnComprobante.Text = "Factura"
            End If

            '20/09/06
            'si el total da cero (0), no necesita confirmaci�n de autorizaciones y se emite una factura (no proforma)  
            If txtTotNeto.Valor > 0 Then
                mCargarAutorizaciones()
            Else
                mobj.pTipoComprobGenera = TipoComprobGene.Factura
            End If

            ' guardo datos en el ds ppal
            lDsDatos = mGuardarDatos()
            ' Dario 2013-09-13 obtengo otro tmp de trabajo
            lDsDatosTemp = mGuardarDatos()

            ' crea Objeto facturacion para poder acceder a la nueva funcion de vtos futuros
            Dim _FacturacionBusiness As New Business.Facturacion.FacturacionBusiness

            ' Dario 2013-07-10 Cambio por calculo de sobretasas con vto a futuro
            ' crea Objeto para obtener el dato de actividad
            Dim _ActividadesBusiness As New Business.Facturacion.ActividadesBusiness
            Dim bitAplicaSobreTasaPagoRetrasado As Boolean = _ActividadesBusiness.GetAplicaSobreTasaPagoRetrasado(cmbActi.Valor)
            ' Dario comentado para pasaje
            ' valido que la actividad permita el pago con recargo a diferentes vtos y la forma
            ' de pago seta cta corrientes, y si el comprobante que se genera es factura, si el total de la factura es > 0 y
            ' si el hdproforma es nada(ya que no se tomo de una proforma), sino no aplica la nueva funcion
            If (bitAplicaSobreTasaPagoRetrasado = True And Me.cmbFormaPago.Valor = mintCtaCte _
                And mobj.pTipoComprobGenera = TipoComprobGene.Factura And txtTotNeto.Valor > 0) Then
                ' obtengo la fecha desde la cual no se aplica el recargo por pago en plazos
                Dim fhFacProfSinRecargo As DateTime = clsSQLServer.gParametroValorConsul(mstrConn, "para_FacProformaSinRecargoDesdeFecha")
                ' valido que se pueda generar los recargos si viene desde la proforma segun
                '  fecha de parametros
                Dim bitEjecutoCarculo As Boolean = True
                ' verifico que tenga proforma
                If (hdnProformaId.Text.Trim().Length > 0) Then
                    ' verifico si es la fecha de la ingreso de la proforma es posterior a la fecha del parametro
                    ' en ese caso no tiene que generar el calculo para pago a futuro
                    If (Convert.ToDateTime(hdnDatosPopProformaFechaIngre.Text) >= fhFacProfSinRecargo) Then
                        bitEjecutoCarculo = False
                    End If
                End If

                ' solo pone false si esta factura viene de una proforma y la fecha de ingreso de la misma
                ' es menor a la de parametro
                If (bitEjecutoCarculo = True) Then
                    ' creo el vector para colocar el resultado de vtos dias
                    Dim intCantDiasVtos() As Integer
                    ' cargo el resultado de los dias a futuro para calcular
                    intCantDiasVtos = _FacturacionBusiness.GetDiasSobreTasaVtosPorIdActividad(cmbActi.Valor)
                    'ojo sacar solo para test   
                    'Dim fhCantDiasVtos(intCantDiasVtos.Length - 1) As DateTime
                    'Dim index As Integer = 0
                    ' recorro el vector para calcular la nueva fecha de valor y simular 
                    ' el vto a futuro de las sobretasas
                    For Each diasASumarAFechValor As Integer In intCantDiasVtos
                        ' controlo que el dato del vertor sea mayor a cero sino salgo del bucle
                        If (diasASumarAFechValor > 0) Then
                            ' calculo la proxima fecha valor
                            Dim fechaCalculada As DateTime = Me.txtFechaValor.Fecha
                            If (bitPrimerRegistro) Then
                                ' ejecuto la funcion que registra en la coleccion temporal comprobantes aranceles en este caso
                                ' ya que es la primera son las calculadas ya no hay que calcular nada, 
                                ' ejecuto la funcion que registra en la coleccion temporallas nuevas sobre tasas calculadas
                                _FacturacionBusiness.CargarColeccionComprobanteSobreTasaVtos(lDsDatos _
                                                                                            , fechaCalculada.AddDays(diasASumarAFechValor) _
                                                                                            , bitPrimerRegistro _
                                                                                            , Me.Session("sUserId") _
                                                                                            , -1 _
                                                                                            , listComprobanteSobreTasaVtos)
                                ' seteo el false el primer registro
                                bitPrimerRegistro = False

                            Else
                                ' seteo la nueva fecha de valor antes del recalculo
                                Me.txtFechaValor.Fecha = fechaCalculada.AddDays(diasASumarAFechValor)
                                ' cargo la lista de precios que corresponde con la nueva fecha
                                Me.mLlenaComboListaPrecios(Me.txtFechaValor.Fecha, Me.cmbActi.Valor)
                                ' llamo a la funcion de recalculo de sobretasas para fechas de pago futuras
                                Me.mModifDatosDetaSoloSobretasaPorVto()
                                lDsDatos = mGuardarDatosRecalculo()
                                ' ejecuto la funcion que registra en la coleccion temporal comprobantes aranceles en este caso
                                _FacturacionBusiness.CargarColeccionComprobanteSobreTasaVtos(lDsDatos _
                                                                                            , Me.txtFechaValor.Fecha _
                                                                                            , bitPrimerRegistro _
                                                                                            , Me.Session("sUserId") _
                                                                                            , -1 _
                                                                                            , listComprobanteSobreTasaVtos)


                            End If
                            ' seteo nuevamente la fecha original para la proximo recalculo
                            Me.txtFechaValor.Fecha = fechaValorOrig
                            ' ojo comentar lo de abajo es para test
                            'fhCantDiasVtos(index) = fechaCalculada.AddDays(diasASumarAFechValor)
                            'index = index + 1
                        Else
                            Exit For
                        End If
                    Next
                End If
            End If
            ' seteo nuevamente la fecha original para la proximo recalculo
            Me.txtFechaValor.Fecha = fechaValorOrig
            ' Paso los datos del ds originaes al de la pagina 
            lDsDatos = lDsDatosTemp
            ' Dario 2013-10-25 Si corresponde NC auto cargo el ds correspondiente
            'If (Me.hdnGeneraNCAuto.Text = "Si" And mobj.pTipoComprobGenera = TipoComprobGene.Factura) Then
            '    lDsDatosNC = Me.mGuardarDatosNC()
            'End If

            ' Dario 2013-07-10 cambio para colocar aca el metodo que recalcula las sobre tasa segun fecha de pago a futuro
            ' creo la transaccion para pasar al metodo de alta
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            ' Dario 2013-10-24  OJO DESCOMENTAR
            Dim lobj As New SRA_Neg.FacturacionABM(lTransac, Session("sUserId").ToString(), mstrTabla, lDsDatos)
            Dim ldsturnos As DataSet = mobj.TurnosFacturadosDT(lTransac, mdsDatos, lstrtabla)

            lstrId = lobj.Alta(lTransac, hdnProformaId.Text, ldsturnos, lstrtabla)
            ' Registro en la base de datos los vtos a futuro
            bitOperOk = _FacturacionBusiness.CrearComprobanteSobreTasaVtos(lTransac, listComprobanteSobreTasaVtos, Me.Session("sUserId"), lstrId)
            If (bitOperOk = False) Then
                Throw New Exception("bitOperOk: false CrearComprobanteSobreTasaVtos")
            End If
            ' Dario 2013-10-24  OJO DESCOMENTAR
            ' fin comentado para pasaje
            ' Dario 2013-10-24 - Genera NC si corresponde
            If (Me.hdnGeneraNCAuto.Text = "Si" And mobj.pTipoComprobGenera = TipoComprobGene.Factura) Then
                lDsDatosNC = Me.mGuardarDatosNC()
                Dim objNC As New SRA_Neg.FacturacionABM(lTransac, Session("sUserId").ToString(), mstrTabla, lDsDatosNC)
                lstrIdNC = objNC.Alta(lTransac, True)
            End If

            ' hacemos commit de la operacion ya que termino exitosamente
            clsSQLServer.gCommitTransac(lTransac)

            hdnCompGenera.Text = mobj.NroComprobanteGenerado(mstrConn, lstrId)
            hdnCompGeneraNC.Text = mobj.NroComprobanteGenerado(mstrConn, lstrIdNC)

            clsSQLServer.gCloseTransac(lTransac)

            hdnIdGenerado.Text = lstrId
            hdnIdGeneradoNC.Text = lstrId
            hdnImprimirNC.Text = lstrIdNC

        Catch ex As Exception
            ' Rollback
            clsSQLServer.gRollbackTransac(lTransac)
            ' seteo nuevamente la fecha original para la proximo recalculo
            Me.txtFechaValor.Fecha = fechaValorOrig
            ' recalculo la factura
            mRecalculaAranceles()
            clsError.gManejarError(Me, ex)
            Throw (ex)
        End Try

        Return lstrId

    End Function

    Private Sub mAlta()

        Dim lstrId As String

        ' Dario 2013-08-30 si viene "" coloco Factura como tipo doc a generar
        If (hdnComprobante.Text = "") Then
            hdnComprobante.Text = "Factura"
        End If

        lstrId = mAltaReal()

        '07/11/06
        'se pueden generar proformas al contado, si el usuario lo indica, estas si se imprimen.
        '01/07 las facturas con valor 0 pueden imprimirse 
        If (cmbFormaPago.Valor = mintContado Or cmbFormaPago.Valor = mintTarj Or cmbFormaPago.Valor = mintDif) And hdnComprobante.Text <> "Proforma" And txtTotNeto.Valor <> 0 Then
            'no imprime

            Dim lstrclieId As Integer = usrClie.Valor
            Dim lstrcomp As String = hdnComprobante.Text
            Dim lstrCompGenera As String = hdnCompGenera.Text
            mobj.LetraComprob(mstrConn, usrClie.TipoIVAId)
            Dim lstrletra As String = mobj.pLetraComprob
            'mAgregar()
            hdnNoImprimir.Text = "Se gener� la " + lstrcomp + " " + lstrletra + " " + lstrCompGenera
            hdnProforma.Text = mCerrarProforma(lstrId)
            'mAbrirVentana(lstrId, lstrcomp, lstrCompGenera, lstrletra, "")
        Else
            hdnImprimir.Text = lstrId
            If hdnComprobante.Text <> "Proforma" Then
                hdnProforma.Text = mCerrarProforma(lstrId)
            End If
        End If

    End Sub
    Private Sub mAltaConCobranzas(ByVal pstrTipo As String, Optional ByVal pstrFac As String = "")
        Dim idComp As Integer = mAltaReal()

        'Dim lstrclieId As Integer = usrClie.Valor
        'Dim lstrcomp As String = hdnComprobante.Text
        'Dim lstrCompGenera As String = hdnCompGenera.Text
        'mobj.LetraComprob(mstrConn, usrClie.TipoIVAId)
        'Dim lstrletra As String = mobj.pLetraComprob

        hdnCobranzaId.Text = idComp
        hdnCobranzaTipo.Text = pstrTipo
        hdnCobranzaFac.Text = pstrFac

        If hdnComprobante.Text <> "Proforma" Then
            hdnProfCobrar.Text = mCerrarProforma(idComp)
            If hdnProfCobrar.Text = "" And hdnCobranzaId.Text <> "" Then
                mAltaConCobranzas2(idComp, pstrTipo, pstrFac)
            End If
        End If

        'mAgregar()
        'Dim lstrComprobante As String = lstrcomp + " " + lstrletra + " " + lstrCompGenera

        'lsbPagina.Append("Cobranzas.aspx?comp_id=" & idComp & "&tipo=" & pstrTipo & "&fac=" & pstrFac & "&titu=" & lstrComprobante)
        'clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 700, 600, 100, 100)
        'mAltaConCobranzas2(idComp, pstrTipo, pstrFac)

    End Sub
    Private Sub mAltaConCobranzas2(ByVal pidComp As Integer, ByVal pstrTipo As String, ByVal pstrFac As String)

        Dim lsbPagina As New System.Text.StringBuilder

        Dim lstrclieId As Integer = usrClie.Valor
        Dim lstrcomp As String = hdnComprobante.Text
        Dim lstrCompGenera As String = hdnCompGenera.Text
        mobj.LetraComprob(mstrConn, usrClie.TipoIVAId)
        Dim lstrletra As String = mobj.pLetraComprob

        mAgregar()
        Dim lstrComprobante As String = lstrcomp + " " + lstrletra + " " + lstrCompGenera

        '02/08/2010 - Muestra el pop-up de Numero de Tramite    *******************
        Dim cadena As String
        cadena = "'NroTramite_pop.aspx?titulo=N�meros de tr�mite" & "&compId=" + Str(pidComp) & "&titu=" + lstrComprobante & "&prof='S''"
        cadena = "NroTramite_pop.aspx?compId=" & Str(pidComp) & "&titu=" + lstrComprobante

        Page.RegisterStartupScript("PopupScript", "<script language='JavaScript'>" + "window.open('" + cadena + "', 'CustomPopUp', " + "'width=600, height=250, menubar=no, fullscreen = no, resizable = yes, scrollbars =  yes')" + "</script>")
        '**************************************************************************
        lsbPagina.Append("Cobranzas.aspx?comp_id=" & pidComp & "&tipo=" & pstrTipo & "&fac=" & pstrFac & "&titu=" & lstrComprobante)
        clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 700, 600, 100, 100)


    End Sub
    Private Function mGuardarDatos() As DataSet
        Dim ldrDatos As DataRow
        Dim ldsDatos As DataSet
        Dim lstrPrefCab As String = "comp"
        Dim lstrPrefDeta As String = "code"
        Dim lstrPrefDetaAran As String = "coan"
        Dim lstrPrefDetaConcep As String = "coco"

        Dim lstrTabla As String = mstrTabla
        Dim lstrTablaDeta As String = mstrTablaDeta
        Dim lstrTablaDetaAran As String = mstrTablaDetaAran
        Dim lstrTablaDetaConcep As String = mstrTablaDetaConcep
        Dim lstrTablaAutor As String = mstrTablaAutor
        Dim lintIDAux As Integer

        Dim lintComprob As Integer

        If mobj.pAutorizaciones Then
            mobj.GuardarAutorizaciones(mdsDatos, grdAutoriza)
        End If
        mValidarDatos()
        mCargarCuotaUnica()

        ' si es proforma
        ''If mobj.pTipoComprobGenera <> TipoComprobGene.Factura Then        16/05/2011
        If hdnComprobante.Text = "Proforma" Then
            'proformas

            lstrTabla = SRA_Neg.Constantes.gTab_Proformas
            lstrTablaDeta = SRA_Neg.Constantes.gTab_ProformaDeta
            lstrTablaDetaAran = SRA_Neg.Constantes.gTab_ProformaAranc
            lstrTablaDetaConcep = SRA_Neg.Constantes.gTab_ProformaConcep
            lstrTablaAutor = SRA_Neg.Constantes.gTab_ProformaAutor

            Dim ldsDatosProfo As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, lstrTabla, "")

            ldsDatosProfo.Tables(0).TableName = lstrTabla
            ldsDatosProfo.Tables(1).TableName = lstrTablaDeta
            ldsDatosProfo.Tables(2).TableName = lstrTablaDetaAran
            ldsDatosProfo.Tables(3).TableName = lstrTablaDetaConcep
            ldsDatosProfo.Tables(4).TableName = lstrTablaAutor

            ldsDatosProfo.Tables(lstrTabla).Rows.Add(ldsDatosProfo.Tables(lstrTabla).NewRow)

            lstrPrefCab = "prfr"
            lstrPrefDeta = "prde"
            lstrPrefDetaAran = "pran"
            lstrPrefDetaConcep = "prco"


            ldsDatos = ldsDatosProfo.Copy

            For Each dr As DataRow In mdsDatos.Tables(mstrTablaAutor).Select
                With dr
                    ldrDatos = ldsDatos.Tables(lstrTablaAutor).NewRow()
                    ldrDatos.Item("prau_id") = -1
                    ldrDatos.Item("prau_auti_id") = .Item("coau_auti_id")
                    ldrDatos.Item("prau_auto") = .Item("coau_auto")
                    ldrDatos.Item("prau_obse") = .Item("coau_obse")
                    ldrDatos.Table.Rows.Add(ldrDatos)
                End With
            Next

        Else
            ldsDatos = mdsDatos.Copy
            ldsDatos.Tables.Remove(mstrTablaTempDeta)
        End If

        'If mobj.pTipoComprobGenera = TipoComprobGene.Factura Then  16/05/2011
        If hdnComprobante.Text = "Proforma" Then
            lintComprob = TiposComprobantes.Proforma
        Else
            lintComprob = TiposComprobantes.Factura
        End If

        With ldsDatos.Tables(lstrTabla).Rows(0)
            .Item(lstrPrefCab + "_id") = -1
            .Item(lstrPrefCab + "_fecha") = txtFechaValor.Fecha
            .Item(lstrPrefCab + "_clie_id") = usrClie.Valor
            .Item(lstrPrefCab + "_dh") = 0 ' debe 
            .Item(lstrPrefCab + "_neto") = txtTotNeto.Valor
            .Item(lstrPrefCab + "_cance") = IIf(txtTotNeto.Valor = 0, 1, 0)  ' cancelado o no
            .Item(lstrPrefCab + "_cs") = 1
            .Item(lstrPrefCab + "_coti_id") = lintComprob
            .Item(lstrPrefCab + "_mone_id") = 1 ' tipo de moneda $
            .Item(lstrPrefCab + "_cemi_nume") = mobj.pCentroEmisorNro
            .Item(lstrPrefCab + "_emct_id") = mobj.pCentroEmisorId
            mobj.LetraComprob(mstrConn, usrClie.TipoIVAId)
            .Item(lstrPrefCab + "_letra") = mobj.pLetraComprob
            .Item(lstrPrefCab + "_cpti_id") = IIf(cmbFormaPago.Valor = mintDif, mintContado, cmbFormaPago.Valor) 'pago diferido se graba como pago al al contado
            .Item(lstrPrefCab + "_ivar_fecha") = txtFechaIva.Fecha
            .Item(lstrPrefCab + "_acti_id") = cmbActi.Valor
            .Item(lstrPrefCab + "_impre") = 0 ' impresa o no
            .Item(lstrPrefCab + "_host") = mobj.pHost  ' nombre del host
            .Item(lstrPrefCab + "_modu_id") = SRA_Neg.Constantes.Modulos.Facturacion
        End With

        ldsDatos.Tables(lstrTablaDeta).Select()
        ldrDatos = ldsDatos.Tables(lstrTablaDeta).NewRow()
        ' guarda los datos de la tabla comprob_deta
        With ldrDatos
            .Item(lstrPrefDeta + "_id") = -1
            .Item(lstrPrefDeta + "_conv_id") = cmbConv.Valor
            .Item(lstrPrefDeta + "_coti") = txtCotDolar.Valor
                .Item(lstrPrefDeta + "_pers") = chkTPers.Checked
                If cmbRaza.Valor.Length > 0 Then
                    .Item(lstrPrefDeta + "_raza_id") = cmbRaza.Valor 'raza
                End If
                .Item(lstrPrefDeta + "_cria_id") = IIf(hdnCria.Text = "", DBNull.Value, hdnCria.Text)   'criador
                .Item(lstrPrefDeta + "_impo_ivat_tasa") = txtTotIVA.Valor
                .Item(lstrPrefDeta + "_impo_ivat_tasa_redu") = txtTotIVARedu.Valor
                .Item(lstrPrefDeta + "_impo_ivat_tasa_sobre") = txtTotIVASTasa.Valor
                .Item(lstrPrefDeta + "_obse") = txtObser.Valor
                .Item(lstrPrefDeta + "_reci_nyap") = txtNombApel.Valor
                If cmbExpo.Valor.Length > 0 Then
                    .Item(lstrPrefDeta + "_expo_id") = cmbExpo.Valor
                End If
                If hdnTarjClieId.Text.Length > 0 Then
                    .Item(lstrPrefDeta + "_tacl_id") = IIf(hdnTarjClieId.Text = "", DBNull.Value, hdnTarjClieId.Text)
                    .Item(lstrPrefDeta + "_tarj_nume") = txtTarjNro.Valor
                    .Item(lstrPrefDeta + "_tarj_id") = cmbTarj.Valor
                    .Item(lstrPrefDeta + "_tarj_cuot") = txtTarjCuo.Valor
                    .Item(lstrPrefDeta + "_tarj_vtat") = chkVtaTele.Checked
                End If

                .Table.Rows.Add(ldrDatos)
            End With

        'copia los datos de la tabla temporal en la tabla de comprob_aran y comprob_conceptos segun corresponda
        'no pasa los eliminados 
        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In mdsDatos.Tables(mstrTablaTempDeta).DefaultView

            With ldr

                If .Item("temp_tipo") = "A" Or .Item("temp_tipo") = "S" Then
                    ldrDatos = ldsDatos.Tables(lstrTablaDetaAran).NewRow()
                    'pasar al aux
                    If Not .Item("temp_rela_id_aux") Is DBNull.Value Then .Item("temp_rela_id") = .Item("temp_rela_id_aux")
                    ldrDatos.Item(lstrPrefDetaAran + "_id") = -1 * IIf(.Item("temp_rela_id") Is DBNull.Value, 1, .Item("temp_rela_id"))
                    ldrDatos.Item(lstrPrefDetaAran + "_aran_id") = .Item("temp_aran_concep_id")
                    ldrDatos.Item(lstrPrefDetaAran + "_ccos_id") = .Item("temp_ccos_id")
                    ldrDatos.Item(lstrPrefDetaAran + "_cant") = .Item("temp_cant")
                    If usrClie.DiscrimIVA Then
                        ldrDatos.Item(lstrPrefDetaAran + "_unit_impo") = .Item("temp_unit_impo")
                    Else
                        ldrDatos.Item(lstrPrefDetaAran + "_unit_impo") = .Item("temp_unit_c_iva")
                    End If

                    ldrDatos.Item(lstrPrefDetaAran + "_impo") = .Item("temp_impo")
                    ldrDatos.Item(lstrPrefDetaAran + "_exen") = IIf(.Item("temp_exen") = "Si", 1, 0)
                    ldrDatos.Item(lstrPrefDetaAran + "_sin_carg") = IIf(.Item("temp_sin_carg") = "Si", 1, 0)
                    ldrDatos.Item(lstrPrefDetaAran + "_impo_ivai") = .Item("temp_impo_ivai")
                    ldrDatos.Item(lstrPrefDetaAran + "_tasa_iva") = .Item("temp_tasa_iva")
                    If mobj.pRRGG Then
                        ldrDatos.Item(lstrPrefDetaAran + "_fecha") = IIf(.Item("temp_fecha").ToString = "", DBNull.Value, .Item("temp_fecha"))
                        ldrDatos.Item(lstrPrefDetaAran + "_inic_rp") = IIf(.Item("temp_inic_rp").ToString = "", DBNull.Value, .Item("temp_inic_rp"))
                        ldrDatos.Item(lstrPrefDetaAran + "_fina_rp") = IIf(.Item("temp_fina_rp").ToString = "", DBNull.Value, .Item("temp_fina_rp"))
                        ldrDatos.Item(lstrPrefDetaAran + "_tram") = IIf(.Item("temp_tram").ToString = "", DBNull.Value, .Item("temp_tram"))
                        ldrDatos.Item(lstrPrefDetaAran + "_acta") = IIf(.Item("temp_acta").ToString = "", DBNull.Value, .Item("temp_acta"))
                        ldrDatos.Item(lstrPrefDetaAran + "_anim_no_fact") = IIf(.Item("temp_anim_no_fact").ToString = "", DBNull.Value, .Item("temp_anim_no_fact"))
                        'si tiene un id que relaciona el arancel con otro arancel, lo graba para conservar la relacion arancel/sobretasa               
                        If Not .Item("temp_rela_id") Is DBNull.Value Then
                            Dim ldrSobre As DataRow = mdsDatos.Tables(mstrTablaTempDeta).Select("temp_id=" & .Item("temp_rela_id").ToString)(0)
                            ldrDatos.Item(lstrPrefDetaAran + "_rrgg_aran_id") = ldrSobre.Item("temp_aran_concep_id")
                        End If
                    End If
                    ldrDatos.Item(lstrPrefDetaAran + "_auto") = .Item("temp_sobre_auto")
                    ldrDatos.Item(lstrPrefDetaAran + "_desc_ampl") = .Item("temp_desc_ampl")

                    If lintComprob = TiposComprobantes.Factura Then
                        ldrDatos.Item(lstrPrefDetaAran + "_pran_id") = IIf(.Item("temp_deta_prfr_id").ToString = "", DBNull.Value, .Item("temp_deta_prfr_id"))
                    End If

                    ldrDatos.Table.Rows.Add(ldrDatos)
                Else
                    ldrDatos = ldsDatos.Tables(lstrTablaDetaConcep).NewRow()
                    ldrDatos.Item(lstrPrefDetaConcep + "_id") = -1
                    ldrDatos.Item(lstrPrefDetaConcep + "_conc_id") = .Item("temp_aran_concep_id")
                    ldrDatos.Item(lstrPrefDetaConcep + "_ccos_id") = .Item("temp_ccos_id")
                    ldrDatos.Item(lstrPrefDetaConcep + "_impo") = .Item("temp_impo")
                    ldrDatos.Item(lstrPrefDetaConcep + "_impo_ivai") = .Item("temp_impo_ivai")
                    ldrDatos.Item(lstrPrefDetaConcep + "_tasa_iva") = .Item("temp_tasa_iva")
                    ldrDatos.Item(lstrPrefDetaConcep + "_desc_ampl") = .Item("temp_desc_ampl")
                    ldrDatos.Item(lstrPrefDetaConcep + "_auto") = .Item("temp_auto")
                    If lintComprob = TiposComprobantes.Factura Then
                        ldrDatos.Item(lstrPrefDetaConcep + "_prco_id") = IIf(.Item("temp_deta_prfr_id").ToString = "", DBNull.Value, .Item("temp_deta_prfr_id"))
                    End If
                    ldrDatos.Table.Rows.Add(ldrDatos)
                End If
            End With
        Next

        Return ldsDatos


    End Function
    ' Dario 2013-10-24 Metodo que se usa para generar el dataset de la nota de credito
        Private Function mGuardarDatosNC() As DataSet
            Dim ldrDatos As DataRow
            Dim ldsDatos As DataSet

            Dim lstrTabla As String = mstrTabla
            Dim lstrTablaDeta As String = mstrTablaDeta
            Dim lstrTablaDetaConcep As String = mstrTablaDetaConcep

            ldsDatos = mdsDatos.Copy
            ldsDatos.Tables.Remove(mstrTablaTempDeta)
            ldsDatos.Tables.Remove(mstrTablaDetaAran)
            ldsDatos.Tables(mstrTablaVtos).Clear()

            Dim decNeto As Decimal = Convert.ToDecimal(Me.hdnImporteNCAuto.Text) - (Convert.ToDecimal(Me.hdnImporteDescuentoAplicadoNCAuto.Text) * -1)

            With ldsDatos.Tables(mstrTabla).Rows(0)
                .Item("comp_id") = -1
                .Item("comp_fecha") = txtFechaValor.Fecha
                .Item("comp_clie_id") = usrClie.Valor
                .Item("comp_dh") = 1 ' paga 
                .Item("comp_neto") = decNeto
                .Item("comp_cance") = 0 ' cancelado
                .Item("comp_cs") = 1
                .Item("comp_coti_id") = Common.ComprobantesTipos.NC
                .Item("comp_mone_id") = 1 ' tipo de moneda $
                .Item("comp_cemi_nume") = mobj.pCentroEmisorNro
                .Item("comp_emct_id") = mobj.pCentroEmisorId
                mobj.LetraComprob(mstrConn, usrClie.TipoIVAId)
                .Item("comp_letra") = mobj.pLetraComprob
                .Item("comp_cpti_id") = mintCtaCte
                .Item("comp_ivar_fecha") = txtFechaIva.Fecha
                .Item("comp_acti_id") = cmbActi.Valor
                .Item("comp_impre") = 0 ' impresa o no
                .Item("comp_host") = mobj.pHost  ' nombre del host
                .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.Facturacion
            End With

            ldsDatos.Tables(lstrTablaDeta).Select()
            ldrDatos = ldsDatos.Tables(lstrTablaDeta).NewRow()
            ' guarda los datos de la tabla comprob_deta
            With ldrDatos
                .Item("code_id") = -1
                .Item("code_comp_id") = -1
                .Item("code_conv_id") = cmbConv.Valor
                .Item("code_coti") = txtCotDolar.Valor
                .Item("code_pers") = DBNull.Value
                .Item("code_raza_id") = cmbRaza.Valor 'raza
                .Item("code_cria_id") = IIf(hdnCria.Text = "", DBNull.Value, hdnCria.Text)   'criador
                .Item("code_impo_ivat_tasa") = txtTotIVA.Valor
                .Item("code_impo_ivat_tasa_redu") = txtTotIVARedu.Valor
                .Item("code_impo_ivat_tasa_sobre") = txtTotIVASTasa.Valor
                .Item("code_obse") = txtObser.Valor
                .Item("code_reci_nyap") = "" ' se usa solo para recibos sin clientes
                .Item("code_expo_id") = DBNull.Value
                .Item("code_tacl_id") = IIf(hdnTarjClieId.Text = "", DBNull.Value, hdnTarjClieId.Text)
                .Item("code_tarj_nume") = DBNull.Value
                .Item("code_tarj_id") = DBNull.Value
                .Item("code_tarj_cuot") = DBNull.Value
                .Item("code_tarj_vtat") = DBNull.Value
                .Item("code_asoc_comp_id") = DBNull.Value
                .Table.Rows.Add(ldrDatos)
            End With

            ldrDatos = ldsDatos.Tables(mstrTablaVtos).NewRow

            With ldrDatos
                .Item("covt_id") = -1
                .Item("covt_fecha") = txtFechaValor.Fecha
                .Item("covt_porc") = 0
                .Item("covt_impo") = decNeto
                .Item("covt_cance") = 0
                .Table.Rows.Add(ldrDatos)
            End With

            ldsDatos.Tables.Add(mstrTablaSaldosACta)

            With ldsDatos.Tables(mstrTablaSaldosACta)
                .Columns.Add("sact_id", System.Type.GetType("System.Int32"))
                .Columns.Add("sact_comp_id", System.Type.GetType("System.Int32"))
                .Columns.Add("sact_clie_id", System.Type.GetType("System.Int32"))
                .Columns.Add("sact_acti_id", System.Type.GetType("System.Int32"))
                .Columns.Add("sact_impo", System.Type.GetType("System.Decimal"))
                .Columns.Add("sact_cance", System.Type.GetType("System.Boolean"))
                .Columns.Add("sact_audi_user", System.Type.GetType("System.Int32"))
                ldrDatos = .NewRow
                .Rows.Add(ldrDatos)
            End With

            ldrDatos.Item("sact_id") = -1
            ldrDatos.Item("sact_comp_id") = -1
            ldrDatos.Item("sact_clie_id") = usrClie.Valor
            ldrDatos.Item("sact_acti_id") = cmbActi.Valor
            ldrDatos.Item("sact_impo") = decNeto
            ldrDatos.Item("sact_cance") = 0
            ldrDatos.Item("sact_audi_user") = Session("sUserId").ToString()

            ' buscar la cantidad para aplicar el descuento y el % del mismo por la raza
            ' crea el objeto para contener el resultado de la busqueda de parametros
            Dim objParametosBonifLab As New Entities.Facturacion.ParametosBonifLabEntity
            ' crea Objeto para obtener el dato de facturacion
            Dim _FacturacionBusiness As New Business.Facturacion.FacturacionBusiness
            objParametosBonifLab = _FacturacionBusiness.GetParametosBonifLaboratorio(Me.cmbRaza.Valor)
            Dim vartemp_desc_ampl As String = ""

            ' crea el objeto para obtener el acumulado para esa especie para el cliente
            ' para ver si corresponde generar descuento automatico
            Dim objResultPanelLabBonif As New Entities.Facturacion.ResultPanelLabBonif
            objResultPanelLabBonif = _FacturacionBusiness.GetDatosLaboratorioAcumEspecie(Convert.ToInt32(cmbActi.Valor) _
                                                                                       , Convert.ToInt32(Me.hdnClieId.Text) _
                                                                                       , objParametosBonifLab.intIdEspecie _
                                                                                       , Convert.ToDateTime(Me.txtFechaValor.Fecha) _
                                                                                       , mdsDatos)

            ' Dario 2013-12-10 cambio por lab
            'vartemp_desc_ampl = "Descuentos por " + objResultPanelLabBonif.TituAcum + " Aplicado sobre " + objParametosBonifLab.intCantABonificar2.ToString + " an�lisis"
            'vartemp_desc_ampl = "Descuentos por Cantidad de an�lisis Acumulados " + objResultPanelLabBonif.varEspecie + " (" _
            '                  + objResultPanelLabBonif.fechaDesde.ToString("dd/MM/yyyy") + " - " + objResultPanelLabBonif.hoy.ToString("dd/MM/yyyy HH:mm") _
            '                  + ") Aplicado sobre " + objParametosBonifLab.intCantABonificar2.ToString + " an�lisis"
            ' Dario 2013-12-11 nuevo cambio de leyenda
            'DESCUENTO POR CANTIDAD (100 an�lisis acumulados OVINOS entre el 01/07/13 y 11/12/13)
            vartemp_desc_ampl = "(" + objParametosBonifLab.intCantABonificar2.ToString + " an�lisis acumulados " + objResultPanelLabBonif.varEspecie + " entre el " _
                              + objResultPanelLabBonif.fechaDesde.ToString("dd/MM/yyyy") + " y " + objResultPanelLabBonif.hoy.ToString("dd/MM/yyyy") _
                              + ")"


            ' generar un concepto por el total de la bonificacion calculada
            ldrDatos = ldsDatos.Tables(lstrTablaDetaConcep).NewRow()
            ldrDatos.Item("coco_id") = -1
            ldrDatos.Item("coco_conc_id") = objParametosBonifLab.intIdConceptoBonifLab
            ldrDatos.Item("coco_ccos_id") = objParametosBonifLab.intCCosConcepBonifLab
            ldrDatos.Item("coco_impo") = Convert.ToDecimal(Me.hdnImporteNCAuto.Text)
            ldrDatos.Item("coco_impo_ivai") = Convert.ToDecimal(Me.hdnImporteNCAuto.Text)
            ldrDatos.Item("coco_tasa_iva") = 0
            ldrDatos.Item("coco_desc_ampl") = vartemp_desc_ampl
            ldrDatos.Item("coco_auto") = 1
            ldrDatos.Table.Rows.Add(ldrDatos)

            ' Dario 2013-12-11 nuevo cambio de leyenda
            ' FACTURAS RELACIONADAS: B-0003-00052842, B 0003-00052845
            'vartemp_desc_ampl = "Descuentos ya aplicados con anterioridad (" + objResultPanelLabBonif.varFacturasBonifLab + ")"
            vartemp_desc_ampl = "FACTURAS RELACIONADAS: " + objResultPanelLabBonif.varFacturasBonifLab

            ' generar un concepto por el total de descuento ya aplicado negativo
            ldrDatos = ldsDatos.Tables(lstrTablaDetaConcep).NewRow()
            ldrDatos.Item("coco_id") = -1
            ldrDatos.Item("coco_conc_id") = objParametosBonifLab.intIdConceptoBonifLab
            ldrDatos.Item("coco_ccos_id") = objParametosBonifLab.intCCosConcepBonifLab
            ldrDatos.Item("coco_impo") = Convert.ToDecimal(Me.hdnImporteDescuentoAplicadoNCAuto.Text)
            ldrDatos.Item("coco_impo_ivai") = Convert.ToDecimal(Me.hdnImporteDescuentoAplicadoNCAuto.Text)
            ldrDatos.Item("coco_tasa_iva") = 0
            ldrDatos.Item("coco_desc_ampl") = vartemp_desc_ampl
            ldrDatos.Item("coco_auto") = 1
            ldrDatos.Table.Rows.Add(ldrDatos)

            Return ldsDatos
        End Function

    ' Dario 2013-07-12
    ' metodo que completa el ds de datos luego del recalculo de spobre tasas
    Private Function mGuardarDatosRecalculo() As DataSet
        Dim ldrDatos As DataRow
        Dim ldsDatos As DataSet
        Dim lstrPrefCab As String = "comp"
        Dim lstrPrefDeta As String = "code"
        Dim lstrPrefDetaAran As String = "coan"
        Dim lstrPrefDetaConcep As String = "coco"

        Dim lstrTabla As String = mstrTabla
        Dim lstrTablaDeta As String = mstrTablaDeta
        Dim lstrTablaDetaAran As String = mstrTablaDetaAran
        Dim lstrTablaDetaConcep As String = mstrTablaDetaConcep
        Dim lstrTablaAutor As String = mstrTablaAutor
        Dim lintIDAux As Integer

        Dim lintComprob As Integer

        ldsDatos = mdsDatos.Copy
        ldsDatos.Tables.Remove(mstrTablaTempDeta)

        lintComprob = TiposComprobantes.Factura

        'copia los datos de la tabla temporal en la tabla de comprob_aran y comprob_conceptos segun corresponda
        'no pasa los eliminados 
        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In mdsDatos.Tables(mstrTablaTempDeta).DefaultView

            With ldr

                If .Item("temp_tipo") = "A" Or .Item("temp_tipo") = "S" Then
                    ldrDatos = ldsDatos.Tables(lstrTablaDetaAran).NewRow()
                    'pasar al aux
                    If Not .Item("temp_rela_id_aux") Is DBNull.Value Then .Item("temp_rela_id") = .Item("temp_rela_id_aux")
                    ldrDatos.Item(lstrPrefDetaAran + "_id") = -1 * IIf(.Item("temp_rela_id") Is DBNull.Value, 1, .Item("temp_rela_id"))
                    ldrDatos.Item(lstrPrefDetaAran + "_aran_id") = .Item("temp_aran_concep_id")
                    ldrDatos.Item(lstrPrefDetaAran + "_ccos_id") = .Item("temp_ccos_id")
                    ldrDatos.Item(lstrPrefDetaAran + "_cant") = .Item("temp_cant")
                    If usrClie.DiscrimIVA Then
                        ldrDatos.Item(lstrPrefDetaAran + "_unit_impo") = .Item("temp_unit_impo")
                    Else
                        ldrDatos.Item(lstrPrefDetaAran + "_unit_impo") = .Item("temp_unit_c_iva")
                    End If

                    ldrDatos.Item(lstrPrefDetaAran + "_impo") = .Item("temp_impo")
                    ldrDatos.Item(lstrPrefDetaAran + "_exen") = IIf(.Item("temp_exen") = "Si", 1, 0)
                    ldrDatos.Item(lstrPrefDetaAran + "_sin_carg") = IIf(.Item("temp_sin_carg") = "Si", 1, 0)
                    ldrDatos.Item(lstrPrefDetaAran + "_impo_ivai") = .Item("temp_impo_ivai")
                    ldrDatos.Item(lstrPrefDetaAran + "_tasa_iva") = .Item("temp_tasa_iva")
                    If mobj.pRRGG Then
                        ldrDatos.Item(lstrPrefDetaAran + "_fecha") = IIf(.Item("temp_fecha").ToString = "", DBNull.Value, .Item("temp_fecha"))
                        ldrDatos.Item(lstrPrefDetaAran + "_inic_rp") = IIf(.Item("temp_inic_rp").ToString = "", DBNull.Value, .Item("temp_inic_rp"))
                        ldrDatos.Item(lstrPrefDetaAran + "_fina_rp") = IIf(.Item("temp_fina_rp").ToString = "", DBNull.Value, .Item("temp_fina_rp"))
                        ldrDatos.Item(lstrPrefDetaAran + "_tram") = IIf(.Item("temp_tram").ToString = "", DBNull.Value, .Item("temp_tram"))
                        ldrDatos.Item(lstrPrefDetaAran + "_acta") = IIf(.Item("temp_acta").ToString = "", DBNull.Value, .Item("temp_acta"))
                        ldrDatos.Item(lstrPrefDetaAran + "_anim_no_fact") = IIf(.Item("temp_anim_no_fact").ToString = "", DBNull.Value, .Item("temp_anim_no_fact"))
                        'si tiene un id que relaciona el arancel con otro arancel, lo graba para conservar la relacion arancel/sobretasa               
                        If Not .Item("temp_rela_id") Is DBNull.Value Then
                            Dim ldrSobre As DataRow = mdsDatos.Tables(mstrTablaTempDeta).Select("temp_id=" & .Item("temp_rela_id").ToString)(0)
                            ldrDatos.Item(lstrPrefDetaAran + "_rrgg_aran_id") = ldrSobre.Item("temp_aran_concep_id")
                        End If
                    End If
                    ldrDatos.Item(lstrPrefDetaAran + "_auto") = .Item("temp_sobre_auto")
                    ldrDatos.Item(lstrPrefDetaAran + "_desc_ampl") = .Item("temp_desc_ampl")

                    If lintComprob = TiposComprobantes.Factura Then
                        ldrDatos.Item(lstrPrefDetaAran + "_pran_id") = IIf(.Item("temp_deta_prfr_id").ToString = "", DBNull.Value, .Item("temp_deta_prfr_id"))
                    End If

                    ldrDatos.Table.Rows.Add(ldrDatos)
                Else
                    ldrDatos = ldsDatos.Tables(lstrTablaDetaConcep).NewRow()
                    ldrDatos.Item(lstrPrefDetaConcep + "_id") = -1
                    ldrDatos.Item(lstrPrefDetaConcep + "_conc_id") = .Item("temp_aran_concep_id")
                    ldrDatos.Item(lstrPrefDetaConcep + "_ccos_id") = .Item("temp_ccos_id")
                    ldrDatos.Item(lstrPrefDetaConcep + "_impo") = .Item("temp_impo")
                    ldrDatos.Item(lstrPrefDetaConcep + "_impo_ivai") = .Item("temp_impo_ivai")
                    ldrDatos.Item(lstrPrefDetaConcep + "_tasa_iva") = .Item("temp_tasa_iva")
                    ldrDatos.Item(lstrPrefDetaConcep + "_desc_ampl") = .Item("temp_desc_ampl")
                    ldrDatos.Item(lstrPrefDetaConcep + "_auto") = .Item("temp_auto")
                    If lintComprob = TiposComprobantes.Factura Then
                        ldrDatos.Item(lstrPrefDetaConcep + "_prco_id") = IIf(.Item("temp_deta_prfr_id").ToString = "", DBNull.Value, .Item("temp_deta_prfr_id"))
                    End If
                    ldrDatos.Table.Rows.Add(ldrDatos)
                End If
            End With
        Next

        Return ldsDatos
    End Function
    Private Sub mAvisoCCostoDesc()
        'si es un descuento
        If txtImporteConcep.Valor < 0 Then
            'si es distinto el cod de c. costo a 06  
            If mobj.CentroCostoDistDesc(cmbCCosConcep.Valor) Then
                AccesoBD.clsError.gGenerarMensajes(Me, "El Centro de Costo seleccionado no comienza con 06")
            End If
        End If
    End Sub
    Private Sub mGuardarDatosDeta_concep()
        mValidarConceptos("T")
        'mAvisoCCostoDesc()       20/05/2011 No verificar mas.
        Dim lstrConcepto As String = mobj.ObtenerValorCampo(mstrConn, "conceptos", "conc_desc", cmbConcep.Valor.ToString)
        Dim ldrDatos As DataRow
        Dim lstrCod As String

        If hdnDetaTempId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).Select("temp_id = " & hdnDetaTempId.Text)(0)
            If ldrDatos.IsNull("temp_auto") Then ldrDatos.Item("temp_auto") = 0
            mValidarConceptosAutomaticos(CType(ldrDatos.Item("temp_auto"), Integer))
        End If
        With ldrDatos
            .Item("temp_tipo") = "C"
            .Item("temp_aran_concep_id") = cmbConcep.Valor
            .Item("temp_codi_aran_concep") = mobj.ObtenerValorCampo(mstrConn, "conceptos", "conc_codi", cmbConcep.Valor.ToString)
            .Item("temp_desc_aran_concep") = cmbConcep.SelectedItem.ToString.Replace(.Item("temp_codi_aran_concep"), "").Trim()
            lstrConcepto = mobj.CortarCadena(lstrConcepto, 30)
            If mobj.pRRGG Then
                lstrConcepto = "C" + "-" + Trim(.Item("temp_codi_aran_concep")) + "-" + lstrConcepto
                'lstrConcepto = "C" + "-" + lstrConcepto
            End If
            .Item("temp_desc_aran_concep_cortada") = lstrConcepto
            .Item("temp_tipo_IVA") = mobj.ObtenerValorCampo(mstrConn, "conceptosX", "tipo_tasa", cmbConcep.Valor.ToString)
            ' importe sin iva
            .Item("temp_impo") = txtImporteConcep.Valor
            'importe con iva
            If usrClie.DiscrimIVA Then
                    .Item("temp_impo_ivai") = (txtImporteConcep.Valor + CType("0" + hdnImpoIvaConcep.Text, Double))
                Else
                .Item("temp_impo_ivai") = txtImporteConcep.Valor
            End If
            'tasa iva
            .Item("temp_tasa_iva") = CType(hdnTasaIVAConcep.Text, Double)
            If hdnTotalSobreTasaC.Text = "" Then
                .Item("temp_sobre_impo") = DBNull.Value
            Else
                .Item("temp_sobre_impo") = hdnTotalSobreTasaC.Text
            End If
            If hdnTasaSobreTasaC.Text = "" Then
                .Item("temp_sobre_porc") = DBNull.Value
            Else
                .Item("temp_sobre_porc") = hdnTasaSobreTasaC.Text
            End If
            If Not cmbCCosConcep.Valor Is DBNull.Value Then
                .Item("temp_ccos_codi") = mobj.ObtenerValorCampo(mstrConn, "centrosc", "ccos_codi", cmbCCosConcep.Valor.ToString)
                    .Item("temp_ccos_id") = IIf(cmbCCosConcep.Valor.Trim().Length > 0, cmbCCosConcep.Valor, DBNull.Value)

                End If
            .Item("temp_desc_ampl") = txtDescAmpliConcep.Valor
            .Item("temp_auto") = 0 ' si el desc, es automatico o no (lo trae la formula)
            .Item("temp_orden") = OrdenItems.conceptos
        End With

        If hdnDetaTempId.Text = "" Then
            mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
        End If


    End Sub

    Private Sub mBorrarSobretasas_Conceptos(Optional ByVal pstrIdDeta As String = "")
        'elimina concep automaticos y sobretasas
        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.Sort = "temp_id"
        For Each ldr As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select("temp_rela_id is not null" & IIf(pstrIdDeta = "", "", " and temp_rela_id = " & pstrIdDeta), "temp_id desc")
            mBajaItemTempDeta(ldr.Item("temp_id"), True)
        Next
        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.Sort = ""
    End Sub

    Private Sub mBorrar_Conceptos()
        'conceptos para varios aranceles (ej adic por precio No socio)
        For Each ldr As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select
            If ldr.Item("temp_rela_id") Is DBNull.Value And Not (ldr.Item("temp_auto") Is DBNull.Value) Then
                If ldr.Item("temp_auto") = 1 Then mBajaItemTempDeta(ldr.Item("temp_id"), True)
            End If
        Next

    End Sub
    Private Function mClienteArancelExento() As String
        Dim lstrAranExen As String = clsSQLServer.gCampoValorConsul(mstrConn, "aranceles_consul " & cmbAran.Valor.ToString, "aran_exen")
        Dim lstrClieExen As String = clsSQLServer.gCampoValorConsul(mstrConn, "clientes_consul " & usrClie.Valor.ToString, "clie_fact_exen")
        If UCase(lstrAranExen) = "TRUE" And UCase(lstrClieExen) = "TRUE" Then
            Return ("Si")
        Else
            Return ("No")
        End If
    End Function
    Private Sub mGuardarDatosDeta_aran()
        mValidarAranceles()
        mArancel()
        Dim lstrVacio As String = ""
        Dim lstrArancel As String = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_desc", cmbAran.Valor.ToString)
        Dim ldrDatos As DataRow
        Dim lintIdDeta As Integer
        If hdnDetaTempId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).Select("temp_id = " & hdnDetaTempId.Text)(0)
            mobj.SobretasasDelArancelNormal(mstrConn, cmbAran.Valor, cmbActi.Valor, txtFechaIva.Fecha)
        End If
        With ldrDatos
            .Item("temp_tipo") = "A"
            .Item("temp_aran_concep_id") = cmbAran.Valor
            .Item("temp_codi_aran_concep") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_codi", cmbAran.Valor.ToString)
            .Item("temp_desc_aran_concep") = cmbAran.SelectedItem.ToString.Replace(.Item("temp_codi_aran_concep"), "").Trim()
            lstrArancel = mobj.CortarCadena(lstrArancel, 30)
            If mobj.pRRGG Then
                Dim lstrsobre As Integer = IIf(mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_rrgg_aran_id", cmbAran.Valor.ToString()) = "", 0, 1)
                Dim lstrEnc As String = "A"
                If lstrsobre = 1 Then lstrEnc = "S"
                lstrArancel = lstrEnc + "-" + Trim(.Item("temp_codi_aran_concep")) + "-" + lstrArancel
                'lstrArancel = "A" + "-" + lstrArancel
                .Item("temp_desc_fecha") = IIf(mobj.pAranRRGGDescFecha = "", "Fecha:", mobj.pAranRRGGDescFecha)
                    .Item("temp_anim_no_fact") = txtCantNoFacAranRRGG.Valor
                    If (txtFechaAranRRGG.Text.Trim().Length > 0) Then
                        .Item("temp_fecha") = txtFechaAranRRGG.Fecha '.ToString("dd/MM/yyyy")
                    Else
                        .Item("temp_fecha") = DBNull.Value
                    End If

                    .Item("temp_inic_rp") = txtRPAranDdeRRGG.Valor
                        .Item("temp_fina_rp") = txtRPAranHtaRRGG.Valor
                        'If Not .IsNull("temp_tram") and Then
                        'If mobj.ObtenerValorCampo(mstrConn, "RRGG_representa_anim", "repre_anim", cmbAran.Valor) = "N" Then
                        'si representa animales, y tenia nro de tramite, lo conserva
                        '.Item("temp_tram") = 0
                        'End If
                        'Else
                        If txtTramNume.Text <> "" Then
                            .Item("temp_tram") = txtTramNume.Text   ' toma el de pantalla
                        Else
                            .Item("temp_tram") = 0 ' lo genera automaticamente el SP de alta
                        End If
                        'End If

                        .Item("temp_acta") = txtActaAranRRGG.Valor
                    Else

                        .Item("temp_desc_fecha") = "Fecha"
            End If
            .Item("temp_desc_aran_concep_cortada") = lstrArancel
            .Item("temp_cant") = txtCantAran.Valor
            If hdnTotalSobreTasa.Text = "" Then
                .Item("temp_sobre_impo") = DBNull.Value
            Else
                .Item("temp_sobre_impo") = hdnTotalSobreTasa.Text
            End If
            If hdnTasaSobreTasa.Text = "" Then
                .Item("temp_sobre_porc") = DBNull.Value
            Else
                .Item("temp_sobre_porc") = hdnTasaSobreTasa.Text
            End If
            .Item("temp_cant_sin_cargo") = txtCantSCAran.Valor
            .Item("temp_tipo_IVA") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "tipo_tasa", cmbAran.Valor.ToString)
            'tasa iva
            .Item("temp_tasa_iva") = CType(hdnTasaIVAAran.Text, Double)
            'precio unitario sin iva
            .Item("temp_unit_impo") = txtPUnitAranSIVA.Valor
            'precio unitario con iva
            .Item("temp_unit_c_iva") = txtPUnitAranSIVA.Valor * (1 + (CType(hdnTasaIVAAran.Text, Double) / 100))
            .Item("temp_desc_ampl") = txtDescAmpliAran.Valor
            If usrClie.DiscrimIVA Then
                'importe sin iva
                .Item("temp_impo") = txtImpoAran.Valor
                'importe con iva
                .Item("temp_impo_ivai") = txtImpoAran.Valor + CType(hdlImpoIvaAran.Text, Double)
            Else
                'importe sin iva
                '.Item("temp_impo") = txtImpoAran.Valor - CType(hdlImpoIvaAran.Text, Double)
                'importe con iva
                .Item("temp_impo_ivai") = txtImpoAran.Valor
                .Item("temp_impo") = txtImpoAran.Valor
            End If

            'precio No Socio para luego generar el concepto adicional
            mobj.pPrecioNosocio = True
            Dim lstrFiltro As String
            ' If hdnProformaId.Text = "" Then ' no es proforma
            ' lstrFiltro = cmbAran.Valor.ToString() + ";" + txtCantAran.Valor.ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + txtFechaValor.Fecha.ToString() + ";" + lstrVacio
            ' Else
            lstrFiltro = cmbAran.Valor.ToString() + ";" + txtCantAran.Valor.ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + cmbListaPrecios.Valor.ToString() + ";" + lstrVacio
            'End If
            Dim vsRet As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
            mobj.pPrecioNosocio = False
            If usrClie.DiscrimIVA Then
                .Item("temp_impo_Nosocio_aran") = vsRet(1)
            Else
                .Item("temp_impo_Nosocio_aran") = vsRet(4)
            End If
            ' Dario 2013-04-26 incluyo adherente
            .Item("temp_impo_Adhe_aran") = vsRet(8)
            .Item("temp_ccos_codi") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "ccos_codi", cmbAran.Valor.ToString)
            .Item("temp_ccos_id") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "ccos_id", cmbAran.Valor.ToString)
            '.Item("temp_exen") = IIf(chkExentoAran.Checked(), "Si", "No")
            .Item("temp_exen") = mClienteArancelExento()
            .Item("temp_sin_carg") = IIf(chkSCargoAran.Checked(), "Si", "No")
            ' Dario 2013-09-12 Cambio por laboratotio bonificado
            If (Me.hdnIdEspecieLaboBonif.Text <> "") Then
                .Item("temp_IdEspecie") = CType(Me.hdnIdEspecieLaboBonif.Text, Int32)
                .Item("temp_AcancelABonif") = IIf(Me.hdnIdEspecieLaboBonif.Text.Trim.Length > 0, "Si", "No")
            Else
                .Item("temp_IdEspecie") = CType(0, Int32)
                .Item("temp_AcancelABonif") = "No"
            End If

            ' laboratorios - turnos
            If mobj.pLabora Then
                Dim lintId As Integer = mTurnoRela()
                'If lintId <> 0 Then ' para cualquier arancel que sea de laboratorio 
                .Item("temp_rela_turno_id") = lintId
                .Item("temp_rela_id_aux") = lintId
                    .Item("temp_turno_SP") = txtTurnosSP.Valor '.Replace(".", ",")
                    .Item("temp_turno_RT") = txtTurnosRT.Valor '.Replace(".", ",")
                    .Item("temp_turno_RTU") = txtTurnosRTU.Valor '.Replace(".", ",")
                    .Item("temp_turno_DP") = txtTurnosDP.Valor '.Replace(".", ",")
                    .Item("temp_turno_DC") = txtTurnosDC.Valor '.Replace(".", ",")
                    'End If

                End If
            .Item("temp_orden") = OrdenItems.arancel
            .Item("temp_turnos") = mTurno()
            If hdnDetaTempId.Text = "" Then
                mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
                lintIdDeta = ldrDatos.Item("temp_id")
            Else
                '***************************************************
                ' si el arancel que modifico tiene sobretasas o
                ' conceptos automaticos, los elimino 
                '***************************************************
                lintIdDeta = hdnDetaTempId.Text
                mBorrarSobretasas_Conceptos(lintIdDeta)
            End If
            'bloquea raza y criador si el arancel tiene raza
            If cmbActi.Valor = Actividades.Laboratorio Or cmbActi.Valor = Actividades.RRGG Then
                Dim lstrRazaid As String = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_raza_id", cmbAran.Valor.ToString)
                If lstrRazaid <> "" Then mBloqueacontrolesRaza()
            End If
            ' sobretasas - solo para RRGG 
            mSobretasas(lintIdDeta, cmbAran.Valor, txtCantAran.Valor, IIf(txtFechaAranRRGG.Text = "", Today.Date, txtFechaAranRRGG.Fecha), IIf(hdnFechaVigenciaProf.Text = "", txtFechaValor.Fecha, clsFormatear.gFormatFechaDateTime(hdnFechaVigenciaProf.Text)))
            ' conceptos autom�ticos
            mConceptoAutoSinCargo(lintIdDeta, IIf(usrClie.DiscrimIVA, .Item("temp_impo"), .Item("temp_impo_ivai")), cmbAran.Valor)
            mConceptoAutoLabTurnos(ldrDatos, lintIdDeta)
            'elimino los conceptos automaticos que representan varios aranceles
            mBorrar_Conceptos()
            mConceptoAutoAdicionalNoSocio()
            mConceptoAutoAdicionalConvenio()
            ' Dario 2013-10-09 nuevo concepto automatico bonif laboratorio
            Me.mConceptoAutoBonifLab()
            'Roxi: 11/10/2007 agregar la diferencia (precios nosocio-socio) para las sobretasas
            mAdicionalesPreciosSobreTasas()
        End With
    End Sub

    ' Dario 2013-10-09 metodo que genera si corresponde el concepto de descuento
    ' por bonificacion de laboratorio
    Private Sub mConceptoAutoBonifLab()
            ' controlo que la raza no este nula sino no hago nada
            ' Dario 2013-10-15 IMPORTANTE 
            ' se toma la raza para obtener los valores de descuento del grupo arancel que corresponde
            ' ojo con esto que si dividen las especies en diferentes grupos aranceles hay que modificarlo.
            If (Not Me.cmbRaza.Valor.ToString.Length = 0 And Convert.ToInt16(cmbActi.Valor) = Actividades.Laboratorio And Me.chkSCargoAran.Checked = False) Then
                ' buscar la cantidad para aplicar el descuento y el % del mismo por la raza
                ' crea el objeto para contener el resultado de la busqueda de parametros
                Dim objParametosBonifLab As New Entities.Facturacion.ParametosBonifLabEntity
                ' crea Objeto para obtener el dato de facturacion
                Dim _FacturacionBusiness As New Business.Facturacion.FacturacionBusiness
                objParametosBonifLab = _FacturacionBusiness.GetParametosBonifLaboratorio(Me.cmbRaza.Valor)

                If (Me.hdnClieId.Text() Is Nothing Or Me.hdnClieId.Text() = "") Then
                    Me.hdnClieId.Text = Me.usrClie.Valor
                End If

                ' crea el objeto para obtener el acumulado para esa especie para el cliente
                ' para ver si corresponde generar descuento automatico
                Dim objResultPanelLabBonif As New Entities.Facturacion.ResultPanelLabBonif
                objResultPanelLabBonif = _FacturacionBusiness.GetDatosLaboratorioAcumEspecie(Convert.ToInt32(cmbActi.Valor) _
                                                                                           , Convert.ToInt32(Me.hdnClieId.Text) _
                                                                                           , objParametosBonifLab.intIdEspecie _
                                                                                           , Convert.ToDateTime(Me.txtFechaValor.Fecha) _
                                                                                           , mdsDatos)
                ' si resultado de la consulta de acumulado por cliente  tiene resultado para ese arencel actividad
                ' y la cantidad desde la cual se bonivica es > 0 ahi calculo 
                If (objResultPanelLabBonif.TituAcum.Length > 0 And objParametosBonifLab.intCantABonificar2 > 0) Then
                    Dim importe As Decimal = 0
                    Dim importeIVAi As Decimal = 0
                    Dim boolGeneraConcepto As Boolean = False
                    Dim vartemp_desc_ampl As String = ""
                    ' Si la cantidad acumulada es mayor o igual al la cantidad minima a tomar para la bonificacion
                    ' genera el concepto automatico los analisis ingresados en la factura
                    If (objResultPanelLabBonif.Cantidad >= objParametosBonifLab.intCantABonificar2) Then
                        ' controlo que la cantidad de elemneto de este tipo de la factura sea mayor que 0
                        If (objResultPanelLabBonif.CantidadFactura > 0) Then
                            ' calculo los importes y pongo el flag en true para que genere el concepto
                            importe = (objResultPanelLabBonif.Importe * objParametosBonifLab.decPorcBonifPorCantidad2) * -1
                            importeIVAi = (objResultPanelLabBonif.ImporteIVAi * objParametosBonifLab.decPorcBonifPorCantidad2) * -1
                            ' Dario 2013-12-09 cambio por lab
                            'vartemp_desc_ampl = "Descuentos por " + objResultPanelLabBonif.TituAcum + " Aplicado sobre " + objResultPanelLabBonif.CantidadFactura.ToString + " an�lisis"
                            ' Dario 2013-12-11 nuevo cambio de leyenda
                            'vartemp_desc_ampl = "Descuentos por Cantidad de an�lisis Acumulados " + objResultPanelLabBonif.varEspecie + " (" _
                            '                  + objResultPanelLabBonif.fechaDesde.ToString("dd/MM/yyyy") + " - " + objResultPanelLabBonif.hoy.ToString("dd/MM/yyyy HH:mm") _
                            '                  + ") Aplicado sobre " + objResultPanelLabBonif.CantidadFactura.ToString + " an�lisis"
                            'DESCUENTO POR CANTIDAD (100 an�lisis acumulados OVINOS entre el 01/07/13 y 11/12/13)
                            vartemp_desc_ampl = "(" + objResultPanelLabBonif.CantidadFactura.ToString + " an�lisis acumulados " + objResultPanelLabBonif.varEspecie + " entre el " _
                                              + objResultPanelLabBonif.fechaDesde.ToString("dd/MM/yyyy") + " y " + objResultPanelLabBonif.hoy.ToString("dd/MM/yyyy") _
                                              + ")"


                            boolGeneraConcepto = True
                        End If
                    Else
                        ' controlo si la cantidad acumulada mas la de las factura es mayor que la cantidad desde la cual se bonifica
                        ' genera NC y concepto de descuento sobre los que sobran para llegar al tope 
                        If ((objResultPanelLabBonif.Cantidad + objResultPanelLabBonif.CantidadFactura) > objParametosBonifLab.intCantABonificar2) Then
                            Dim cantidadaBonificar As Integer = (objResultPanelLabBonif.Cantidad + objResultPanelLabBonif.CantidadFactura) - objParametosBonifLab.intCantABonificar2
                            importe = (((objResultPanelLabBonif.Importe / objResultPanelLabBonif.CantidadFactura) _
                                       * cantidadaBonificar) * objParametosBonifLab.decPorcBonifPorCantidad2) * -1
                            importeIVAi = (((objResultPanelLabBonif.ImporteIVAi / objResultPanelLabBonif.CantidadFactura) _
                                       * cantidadaBonificar) * objParametosBonifLab.decPorcBonifPorCantidad2) * -1
                            ' Dario 2013-12-09 cambio por lab
                            'vartemp_desc_ampl = "Descuentos por " + objResultPanelLabBonif.TituAcum + " Aplicado sobre " + cantidadaBonificar.ToString + " an�lisis"
                            ' Dario 2013-12-11 nuevo cambio de leyenda
                            'vartemp_desc_ampl = "Descuentos por Cantidad de an�lisis Acumulados " + objResultPanelLabBonif.varEspecie + " (" _
                            '                  + objResultPanelLabBonif.fechaDesde.ToString("dd/MM/yyyy") + " - " + objResultPanelLabBonif.hoy.ToString("dd/MM/yyyy HH:mm") _
                            '                  + ") Aplicado sobre " + cantidadaBonificar.ToString + " an�lisis"

                            'DESCUENTO POR CANTIDAD (100 an�lisis acumulados OVINOS entre el 01/07/13 y 11/12/13)
                            vartemp_desc_ampl = "(" + cantidadaBonificar.ToString + " an�lisis acumulados " + objResultPanelLabBonif.varEspecie + " entre el " _
                                              + objResultPanelLabBonif.fechaDesde.ToString("dd/MM/yyyy") + " y " + objResultPanelLabBonif.hoy.ToString("dd/MM/yyyy") _
                                              + ")"

                            boolGeneraConcepto = True
                            '  si es igual a la cantidad de analisis miminos para bonicar no genero conecpto
                            ' y se genera NC automatica sobre los analisis acumulados
                        ElseIf ((objResultPanelLabBonif.Cantidad + objResultPanelLabBonif.CantidadFactura) = objParametosBonifLab.intCantABonificar2) Then
                            boolGeneraConcepto = False
                            ' si es menor que la cantidad para aplicar el segundo descuento
                            ' entonces controlo si ingreso en esta factura la candida de analisis para el 
                            ' primer descuento
                        ElseIf ((objResultPanelLabBonif.Cantidad + objResultPanelLabBonif.CantidadFactura) < objParametosBonifLab.intCantABonificar2) Then
                            If (objResultPanelLabBonif.CantidadFactura >= objParametosBonifLab.intCantABonificar) Then
                                ' calculo los importes y pongo el flag en true para que genere el concepto
                                importe = (objResultPanelLabBonif.Importe * objParametosBonifLab.decPorcBonifPorCantidad) * -1
                                importeIVAi = (objResultPanelLabBonif.ImporteIVAi * objParametosBonifLab.decPorcBonifPorCantidad) * -1

                                ' Dario 2013-12-09 cambio por lab
                                'vartemp_desc_ampl = "Descuentos por " + objResultPanelLabBonif.TituAcum + " Aplicado sobre " + objResultPanelLabBonif.CantidadFactura.ToString + " an�lisis"
                                ' Dario 2013-12-11 nuevo cambio de leyenda
                                'vartemp_desc_ampl = "Descuentos por Cantidad de an�lisis Acumulados " + objResultPanelLabBonif.varEspecie + " (" _
                                '                  + objResultPanelLabBonif.fechaDesde.ToString("dd/MM/yyyy") + " - " + objResultPanelLabBonif.hoy.ToString("dd/MM/yyyy HH:mm") _
                                '                  + ") Aplicado sobre " + objResultPanelLabBonif.CantidadFactura.ToString + " an�lisis"

                                'DESCUENTO POR CANTIDAD (50 an�lisis acumulados OVINOS entre el 01/07/13 y 11/12/13)
                                ' Dario 2013-12-26 se comenta a p�dido Jorge Chanseaud
                                'vartemp_desc_ampl = "(" + objResultPanelLabBonif.CantidadFactura.ToString + " an�lisis acumulados " + objResultPanelLabBonif.varEspecie + " entre el " _
                                '                  + objResultPanelLabBonif.fechaDesde.ToString("dd/MM/yyyy") + " y " + objResultPanelLabBonif.hoy.ToString("dd/MM/yyyy") _
                                '                  + ")"
                                vartemp_desc_ampl = ""

                                boolGeneraConcepto = True
                            Else
                                boolGeneraConcepto = False
                            End If
                        Else
                            boolGeneraConcepto = False
                        End If
                    End If

                    ' genera si corresponde el concepto automatico
                    If (boolGeneraConcepto) Then
                        ' generar un concepto con el calculo 
                        Dim ldrDatos As DataRow = mdsDatos.Tables(mstrTablaTempDeta).NewRow
                        With ldrDatos
                            .Item("temp_tipo") = "C"
                            .Item("temp_aran_concep_id") = objParametosBonifLab.intIdConceptoBonifLab
                            .Item("temp_codi_aran_concep") = objParametosBonifLab.varCodigoConcepto
                            .Item("temp_desc_aran_concep") = objParametosBonifLab.varConceptoBonifLab.Trim() + " (Bonif. Laboratorio Auto)"
                            .Item("temp_desc_aran_concep_cortada") = "C" + "-" + Trim(objParametosBonifLab.varCodigoConcepto) + "-" + mobj.CortarCadena(objParametosBonifLab.varConceptoBonifLab, 30)
                            .Item("temp_tipo_IVA") = 0
                            .Item("temp_impo") = importe
                            .Item("temp_impo_ivai") = importeIVAi
                            .Item("temp_tasa_iva") = 0
                            .Item("temp_ccos_codi") = objParametosBonifLab.varCCosConcepBonifLab
                            .Item("temp_ccos_id") = objParametosBonifLab.intCCosConcepBonifLab
                            .Item("temp_desc_ampl") = vartemp_desc_ampl
                            .Item("temp_auto") = 1
                            .Item("temp_rela_id") = DBNull.Value
                            .Item("temp_orden") = OrdenItems.conceptosAuto
                            mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
                        End With
                        ' Dario 2013-11-19 se genera mensaje si se genera concepto auto y hay otro manual sobre la mismo arencel
                        If (Me.txtTurnosDC.Text.Trim().Length > 0) Then
                            Try
                                Dim decTurnosDC As Decimal = Convert.ToDecimal(Me.txtTurnosDC.Text.Trim())
                                If (decTurnosDC > 0) Then
                                    AccesoBD.clsError.gGenerarMensajes(Me, "se genero descuento autom�tico conjuntamente con uno manual, por favor de verificar")
                                End If
                            Catch ex As Exception
                                clsError.gManejarError(Me, New Exception("Facturacion.aspx.vb.mConceptoAutoBonifLab", ex.InnerException))
                            End Try
                        End If
                    End If
                End If
            End If
        End Sub

    Private Sub mAdicionalesPreciosSobreTasas()
        Dim ldecPrecDife As Decimal = 0
        Dim ldecPrecSobre As Decimal = 0
        'Dim lstrForm As String = ""
        Dim ldecPrecio As Decimal = 0
        'para cada sobretasa tomar la diferencia de precios
        For Each ldr As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select("temp_sobre_auto=1")
            ldecPrecDife = ldecPrecDife + (clsSQLServer.gObtenerValorCampo(mstrConn, "precios_aran", "@prar_aran_id=" & ldr.Item("temp_aran_concep_id") & ",@prar_peli_id=" & cmbListaPrecios.Valor.ToString, "_dife_soci_impo") * ldr.Item("temp_cant"))
        Next
        'solo laboratorio
        If mobj.pLabora Then
            'sumar recargos y restar descuentos al adicional por suspension de servicios
            For Each ldr As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select("temp_tipo='A' and isnull(temp_borra,0) <> 1")
                ldecPrecio = ldr.Item("temp_impo_nosocio_aran") - ldr.Item("temp_unit_c_iva")
                If ldr.Item("temp_turno_sp") <> 0 Then
                    ldecPrecSobre = ldecPrecSobre + ldecPrecio * ldr.Item("temp_turno_sp") - ldecPrecio
                End If
                If ldr.Item("temp_turno_rt") <> 0 Then
                    ldecPrecSobre = ldecPrecSobre + ldecPrecio * ldr.Item("temp_turno_rt") - ldecPrecio
                End If
                If ldr.Item("temp_turno_rtu") <> 0 Then
                    ldecPrecSobre = ldecPrecSobre + ldecPrecio * ldr.Item("temp_turno_rtu") - ldecPrecio
                End If
                If ldr.Item("temp_turno_dp") <> 0 Then
                    ldecPrecSobre = ldecPrecSobre - ldecPrecio * ldr.Item("temp_turno_dp")
                End If
                If ldr.Item("temp_turno_dc") <> 0 Then
                    ldecPrecSobre = ldecPrecSobre - ldecPrecio * ldr.Item("temp_turno_dc")
                End If
                'lstrForm = clsSQLServer.gObtenerValorCampo(mstrConn, "conceptos", ldr.Item("temp_aran_concep_id"), "conc_form_id").ToString()
                'Select Case lstrForm
                '    Case SRA_Neg.Constantes.Formulas.Descuento_por_Cantidad
                '       ldecPrecSobre = ldecPrecSobre + ldr.Item("temp_impo")
                '    Case SRA_Neg.Constantes.Formulas.Dto_por_Promocion_en_Laboratorio
                '       ldecPrecSobre = ldecPrecSobre + ldr.Item("temp_impo")
                '    Case SRA_Neg.Constantes.Formulas.Recargo_fuera_de_Fecha
                '       ldecPrecSobre = ldecPrecSobre + ldr.Item("temp_impo")
                '    Case SRA_Neg.Constantes.Formulas.Sobretasa_Palermo_tipificacion_bovinos
                '       ldecPrecSobre = ldecPrecSobre + ldr.Item("temp_impo")
                '    Case SRA_Neg.Constantes.Formulas.Recargo_por_Tramite_Urgente
                '       ldecPrecSobre = ldecPrecSobre + ldr.Item("temp_impo")
                'End Select
            Next
        End If
        'agregar el importe al concepto adicional por suspension de servicios
        For Each ldr As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select("temp_tipo='C'")
            If clsSQLServer.gObtenerValorCampo(mstrConn, "conceptos", ldr.Item("temp_aran_concep_id"), "conc_form_id") = SRA_Neg.Constantes.Formulas.Adicional_por_suspension_de_servicios Then
                ldr.Item("temp_impo") = ldr.Item("temp_impo") + ldecPrecDife + ldecPrecSobre
                If ldr.Item("temp_impo") < 0 Then
                    ldr.Item("temp_impo") = 0
                End If
            End If
        Next
    End Sub
    Private Sub mConceptoAutoLabTurnos(ByVal pdrDatos As DataRow, ByVal pintIdDeta As Integer, ByVal pstrImpoAran As String, ByVal pstrAranId As String)
        '**********************************************
        ' conceptos autom�ticos de laboratorio (turnos)
        '**********************************************
        If mobj.pActiv <> Actividades.Laboratorio Then Exit Sub
        mConcepAutomTurnos(pdrDatos, pintIdDeta, pstrImpoAran, pstrAranId)
    End Sub
    Private Sub mConceptoAutoLabTurnos(ByVal pdrDatos As DataRow, ByVal pintIdDeta As Integer)
        '**********************************************
        ' conceptos autom�ticos de laboratorio (turnos)
        '**********************************************
        If mobj.pActiv <> Actividades.Laboratorio Then Exit Sub
        If (hdnDatosPopTurnoBov.Text <> "" Or hdnDatosPopTurnoEqu.Text <> "") Or mRecaDescTurnosCargada() Then
            mConcepAutomTurnos(pdrDatos, pintIdDeta, txtImpoAran.Valor, cmbAran.Valor)
        End If


    End Sub
    Private Function mRecaDescTurnosCargada() As Boolean
        If txtTurnosSP.Valor <> 0 Or txtTurnosRT.Valor <> 0 Or _
           txtTurnosRTU.Valor <> 0 Or txtTurnosDP.Valor <> 0 Or txtTurnosDC.Valor <> 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub mConcepAutomTurnos(ByVal pdrDatos As DataRow, ByVal pintIdDeta As Integer, ByVal pdouImpoAran As Double, ByVal pintAranId As Integer)
        Dim dt As DataTable = mobj.ConceptosAutoma(mdsDatos, mstrConn, pintIdDeta, pdouImpoAran, (mobj.ObtenerValorCampo(mstrConn, "arancelesX", "ccos_id", clsSQLServer.gFormatArg(pintAranId, SqlDbType.Int))), usrClie.Valor)
        For Each ldr As DataRow In dt.Select
            pdrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
            With pdrDatos
                .Item("temp_tipo") = "C"
                .Item("temp_aran_concep_id") = ldr.Item("temp_aran_concep_id")
                .Item("temp_codi_aran_concep") = ldr.Item("temp_codi_aran_concep")
                .Item("temp_desc_aran_concep") = ldr.Item("temp_desc_aran_concep")
                .Item("temp_desc_aran_concep_cortada") = ldr.Item("temp_desc_aran_concep_cortada")
                .Item("temp_tipo_IVA") = ldr.Item("temp_tipo_IVA")
                .Item("temp_impo") = ldr.Item("temp_impo")
                .Item("temp_impo_ivai") = ldr.Item("temp_impo_ivai")
                .Item("temp_tasa_iva") = ldr.Item("temp_tasa_iva")
                .Item("temp_ccos_codi") = ldr.Item("temp_ccos_codi")
                .Item("temp_ccos_id") = ldr.Item("temp_ccos_id")
                .Item("temp_desc_ampl") = ldr.Item("temp_desc_ampl")
                .Item("temp_auto") = ldr.Item("temp_auto")
                .Item("temp_rela_id") = ldr.Item("temp_rela_id")
                .Item("temp_orden") = OrdenItems.conceptosAuto
                .Item("temp_turnos") = mTurno()
                mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(pdrDatos)
            End With
        Next

    End Sub
    Private Sub mConceptoAutoSinCargo(ByVal pintIdDeta As Integer, ByVal pdouImpoAran As Double, ByVal pintAranId As Integer)
        Dim ldrDatos As DataRow
        Dim lintForm As Integer
        If chkSCargoAran.Checked Or mExento(pintAranId) Then
            If chkSCargoAran.Checked Then
                lintForm = Formulas.DescFactSinCargo
            Else
                lintForm = Formulas.DescuentoFacturacionExenta
            End If
            Dim dt As DataTable = mobj.ConceptosAutoma(mstrConn, mdsDatos, pintIdDeta, pdouImpoAran, CType((mobj.ObtenerValorCampo(mstrConn, "arancelesX_nega", "aran_nega_ccos_id", clsSQLServer.gFormatArg(pintAranId, SqlDbType.Int))), Integer), lintForm, usrClie.Valor, txtFechaIva.Fecha, cmbActi.Valor)
            For Each ldr As DataRow In dt.Select
                ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
                With ldrDatos
                    .Item("temp_tipo") = "C"
                    .Item("temp_aran_concep_id") = ldr.Item("temp_aran_concep_id")
                    .Item("temp_codi_aran_concep") = ldr.Item("temp_codi_aran_concep")
                    .Item("temp_desc_aran_concep") = ldr.Item("temp_desc_aran_concep")
                    .Item("temp_desc_aran_concep_cortada") = ldr.Item("temp_desc_aran_concep_cortada")
                    .Item("temp_tipo_IVA") = ldr.Item("temp_tipo_IVA")
                    .Item("temp_impo") = ldr.Item("temp_impo")
                    .Item("temp_impo_ivai") = ldr.Item("temp_impo_ivai")
                    .Item("temp_tasa_iva") = ldr.Item("temp_tasa_iva")
                    .Item("temp_ccos_codi") = ldr.Item("temp_ccos_codi")
                    .Item("temp_ccos_id") = ldr.Item("temp_ccos_id")
                    .Item("temp_desc_ampl") = ldr.Item("temp_desc_ampl")
                    .Item("temp_auto") = ldr.Item("temp_auto")
                    .Item("temp_rela_id") = ldr.Item("temp_rela_id")
                    .Item("temp_orden") = OrdenItems.conceptosAuto
                    mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
                End With
            Next
        End If
    End Sub
    Private Sub mConceptoAutoAdicionalNoSocio()
        Dim ldrDatos As DataRow
        Dim lbooAdicional, lboolPrecioSocio As Boolean
        lbooAdicional = (clsSQLServer.gCampoValorConsul(mstrConn, "clientes_alertas_especiales_consul " & usrClie.Valor.ToString(), "adicional") = "S")
        ' Dario 2013-04-26 para pasar precio adherente en el caso del mix de clientes socios morosoa
        'lboolPrecioSocio = (clsSQLServer.gCampoValorConsul(mstrConn, "clientes_alertas_especiales_consul " & usrClie.Valor.ToString(), "precio_no_socio") = "No")
        'If lbooAdicional And mobj.pEspecialesTodo(CamposEspeciales.precio_no_socio) = "Si" Then    29/12/2010
        If lbooAdicional Then
                Dim dt As DataTable = mobj.ConceptosAutoma(mdsDatos, mstrConn, usrClie.DiscrimIVA, Convert.ToInt32(cmbActi.Valor), False)
            If dt Is Nothing Then Exit Sub
            mCargarConcepAuto(dt)
        End If
    End Sub
    Private Sub mConceptoAutoAdicionalConvenio()

        If cmbConv.Valor <> 0 Then
            ' Dario 2013-09-12 se corrige el tipo de dato para pasar al metodo sin va sin tipo no encuentra
            ' la sobrecarga correcta
                Dim dt As DataTable = mobj.ConceptosAutoma(mdsDatos, mstrConn, usrClie.DiscrimIVA, Convert.ToInt32(cmbActi.Valor), Convert.ToInt32(cmbConv.Valor))
            If dt Is Nothing Then Exit Sub
            mCargarConcepAuto(dt)
        End If

    End Sub
    Private Sub mCargarConcepAuto(ByVal pdt As DataTable)
        'carga el concepto automatico en el ds detalle
        Dim ldrDatos As DataRow
        For Each ldr As DataRow In pdt.Select
            ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
            With ldrDatos
                .Item("temp_tipo") = "C"
                .Item("temp_aran_concep_id") = ldr.Item("temp_aran_concep_id")
                .Item("temp_codi_aran_concep") = ldr.Item("temp_codi_aran_concep")
                .Item("temp_desc_aran_concep") = ldr.Item("temp_desc_aran_concep")
                .Item("temp_desc_aran_concep_cortada") = ldr.Item("temp_desc_aran_concep_cortada")
                'Roxi: 02/10/2007: los conceptos con formula "Adicional por suspension de servicios" llevan IVA.
                If clsSQLServer.gObtenerValorCampo(mstrConn, "conceptos", ldr.Item("temp_aran_concep_id"), "conc_form_id") = SRA_Neg.Constantes.Formulas.Adicional_por_suspension_de_servicios Then
                    .Item("temp_tipo_IVA") = 1
                    .Item("temp_impo") = ldr.Item("temp_impo")
                    If hdnTasaIVAAran.Text <> "" Then
                        .Item("temp_impo_ivai") = ldr.Item("temp_impo") * (1 + CDbl(hdnTasaIVAAran.Text) / 100)
                        .Item("temp_tasa_iva") = CDbl(hdnTasaIVAAran.Text)
                    Else
                        .Item("temp_impo_ivai") = 0
                        .Item("temp_tasa_iva") = 0
                    End If
                Else
                    .Item("temp_tipo_IVA") = ldr.Item("temp_tipo_IVA")
                    .Item("temp_impo") = ldr.Item("temp_impo")
                    .Item("temp_impo_ivai") = ldr.Item("temp_impo_ivai")
                    .Item("temp_tasa_iva") = ldr.Item("temp_tasa_iva")
                End If
                .Item("temp_ccos_codi") = ldr.Item("temp_ccos_codi")
                .Item("temp_ccos_id") = ldr.Item("temp_ccos_id")
                .Item("temp_desc_ampl") = ldr.Item("temp_desc_ampl")
                .Item("temp_auto") = ldr.Item("temp_auto")
                ' .Item("temp_rela_id") =  'ldr.Item("temp_rela_id")
                .Item("temp_orden") = OrdenItems.conceptosAuto
                mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
            End With
        Next
    End Sub
    Private Sub mSobretasas(ByVal pintIdDeta As Integer, ByVal pintAranId As Integer, ByVal pintCant As Integer, ByVal pdateFechaAranRRGG As Date, ByVal pdateFechaVigencia As Date)
        '*****************************
        ' sobretasas - solo para RRGG 
        '*****************************
        Dim ldrDatos As DataRow
        If mobj.pRRGG And mobj.pAranRRGGDescFecha <> "" Then ' solo se calculan sobretasas cuando la fecha de refe esta activada
            Dim dt As DataTable = mobj.Sobretasas(mdsDatos, mstrConn, pintIdDeta, pintAranId, pintCant, cmbActi.Valor, cmbFormaPago.Valor, usrClie.DiscrimIVA, pdateFechaAranRRGG, txtFechaValor.Fecha, txtFechaIva.Fecha, cmbListaPrecios.Valor.ToString(), txtFechaAranRRGG.Text)
            For Each ldr As DataRow In dt.Select
                ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
                With ldrDatos
                    .Item("temp_tipo") = "S"
                    .Item("temp_aran_concep_id") = ldr.Item("temp_aran_concep_id")
                    .Item("temp_codi_aran_concep") = ldr.Item("temp_codi_aran_concep")
                    .Item("temp_desc_aran_concep") = ldr.Item("temp_desc_aran_concep")
                    .Item("temp_desc_aran_concep_cortada") = ldr.Item("temp_desc_aran_concep_cortada")
                    .Item("temp_cant") = ldr.Item("temp_cant")
                    .Item("temp_cant_sin_cargo") = ldr.Item("temp_cant_sin_cargo")
                    .Item("temp_unit_impo") = ldr.Item("temp_unit_impo")
                    .Item("temp_impo") = ldr.Item("temp_impo")
                    .Item("temp_anim_no_fact") = ldr.Item("temp_anim_no_fact")
                    .Item("temp_fecha") = ldr.Item("temp_fecha")
                    .Item("temp_inic_rp") = txtRPAranDdeRRGG.Valor ' ldr.Item("temp_inic_rp")
                    .Item("temp_fina_rp") = txtRPAranHtaRRGG.Valor 'ldr.Item("temp_fina_rp")
                    .Item("temp_tram") = ldr.Item("temp_tram")
                    .Item("temp_acta") = ldr.Item("temp_acta")
                    .Item("temp_ccos_codi") = ldr.Item("temp_ccos_codi")
                    .Item("temp_ccos_id") = ldr.Item("temp_ccos_id")
                    .Item("temp_exen") = IIf(ldr.Item("temp_exen"), "Si", "No")
                    .Item("temp_sin_carg") = IIf(ldr.Item("temp_sin_carg"), "Si", "No")
                    .Item("temp_tipo_IVA") = ldr.Item("temp_tipo_IVA")
                    .Item("temp_impo_ivai") = ldr.Item("temp_impo_ivai")
                    'precio unitario con iva
                    .Item("temp_unit_c_iva") = SRA_Neg.Utiles.gRound2(CType(ldr.Item("temp_unit_impo"), Double) * (1 + (CType(ldr.Item("temp_tasa_iva"), Double) / 100)))
                    .Item("temp_tasa_iva") = ldr.Item("temp_tasa_iva")
                    .Item("temp_desc_fecha") = ldr.Item("temp_desc_fecha")
                    .Item("temp_rela_id") = ldr.Item("temp_rela_id")
                    .Item("temp_orden") = OrdenItems.sobretasas
                    .Item("temp_sobre_auto") = 1
                    mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
                End With
            Next
        End If
    End Sub

    ' Dario 2013-07-19 calcula sobretasas para recalculo por diferente fecha de vtos para retrasado
    Private Sub mSobretasasVtosRetrasados(ByVal pintIdDeta As Integer, ByVal pintAranId As Integer, ByVal pintCant As Integer _
                                        , ByVal pdateFechaAranRRGG As Date, ByVal pdateFechaVigencia As Date _
                                        , ByVal pinicRP As String, ByVal pfinaRP As String)
        '*****************************
        ' sobretasas - solo para RRGG 
        '*****************************
        Dim ldrDatos As DataRow
        If mobj.pRRGG And mobj.pAranRRGGDescFecha <> "" Then ' solo se calculan sobretasas cuando la fecha de refe esta activada
            Dim dt As DataTable = mobj.Sobretasas(mdsDatos, mstrConn, pintIdDeta, pintAranId, pintCant, cmbActi.Valor, cmbFormaPago.Valor, usrClie.DiscrimIVA, pdateFechaAranRRGG, txtFechaValor.Fecha, txtFechaIva.Fecha, cmbListaPrecios.Valor.ToString(), txtFechaAranRRGG.Text)
            For Each ldr As DataRow In dt.Select
                ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
                With ldrDatos
                    .Item("temp_tipo") = "S"
                    .Item("temp_aran_concep_id") = ldr.Item("temp_aran_concep_id")
                    .Item("temp_codi_aran_concep") = ldr.Item("temp_codi_aran_concep")
                    .Item("temp_desc_aran_concep") = ldr.Item("temp_desc_aran_concep")
                    .Item("temp_desc_aran_concep_cortada") = ldr.Item("temp_desc_aran_concep_cortada")
                    .Item("temp_cant") = ldr.Item("temp_cant")
                    .Item("temp_cant_sin_cargo") = ldr.Item("temp_cant_sin_cargo")
                    .Item("temp_unit_impo") = ldr.Item("temp_unit_impo")
                    .Item("temp_impo") = ldr.Item("temp_impo")
                    .Item("temp_anim_no_fact") = ldr.Item("temp_anim_no_fact")
                    .Item("temp_fecha") = ldr.Item("temp_fecha")
                    .Item("temp_inic_rp") = pinicRP
                    .Item("temp_fina_rp") = pfinaRP
                    .Item("temp_tram") = ldr.Item("temp_tram")
                    .Item("temp_acta") = ldr.Item("temp_acta")
                    .Item("temp_ccos_codi") = ldr.Item("temp_ccos_codi")
                    .Item("temp_ccos_id") = ldr.Item("temp_ccos_id")
                    .Item("temp_exen") = IIf(ldr.Item("temp_exen"), "Si", "No")
                    .Item("temp_sin_carg") = IIf(ldr.Item("temp_sin_carg"), "Si", "No")
                    .Item("temp_tipo_IVA") = ldr.Item("temp_tipo_IVA")
                    .Item("temp_impo_ivai") = ldr.Item("temp_impo_ivai")
                    'precio unitario con iva
                    .Item("temp_unit_c_iva") = SRA_Neg.Utiles.gRound2(CType(ldr.Item("temp_unit_impo"), Double) * (1 + (CType(ldr.Item("temp_tasa_iva"), Double) / 100)))
                    .Item("temp_tasa_iva") = ldr.Item("temp_tasa_iva")
                    .Item("temp_desc_fecha") = ldr.Item("temp_desc_fecha")
                    .Item("temp_rela_id") = ldr.Item("temp_rela_id")
                    .Item("temp_orden") = OrdenItems.sobretasas
                    .Item("temp_sobre_auto") = 1
                    mdsDatos.Tables(mstrTablaTempDeta).Rows.Add(ldrDatos)
                End With
            Next
        End If
    End Sub


    Private Function mExento(ByVal pintIdAran As Integer) As Boolean
        If mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_exen", pintIdAran.ToString) And usrClie.Exento Then
            Return True
        End If
    End Function
    Private Function mTurnoRela() As Integer
        Dim lintRes As Integer
        If hdnDatosPopTurnoBov.Text <> "" Then
            lintRes = hdnDatosPopTurnoBov.Text
        Else
            If hdnDatosPopTurnoEqu.Text <> "" Then lintRes = hdnDatosPopTurnoEqu.Text
        End If
        Return lintRes
    End Function
    Private Function mTurno() As String
        If hdnDatosPopTurnoBov.Text <> "" Then Return "B"
        If hdnDatosPopTurnoEqu.Text <> "" Then Return "E"
    End Function
    Private Function mValidarNulos(ByVal pstrCampo As String, ByVal pboolnull As Boolean) As String
        Dim lstrRt As String = pstrCampo
        If pstrCampo Is DBNull.Value Then If pboolnull Then lstrRt = "" Else lstrRt = "0"
        Return lstrRt
    End Function

    Private Sub mLlenaComboListaPrecios(ByVal pstrFecha As Date, ByVal pstrActiv As String)
        If pstrFecha.ToString <> "" Then
            Dim lstrFiltro As String
            Dim lstrValor As String = ""
            If Not cmbListaPrecios.Valor Is DBNull.Value Then
                lstrValor = cmbListaPrecios.Valor
            End If
            'Dim ldateFecha As Date = clsSQLServer.gFormatArg(pstrFecha, SqlDbType.SmallDateTime)
            lstrFiltro = clsFormatear.gFormatFecha2DB(pstrFecha) + ", " + pstrActiv
            clsWeb.gCargarRefeCmb(mstrConn, "precios_lista_fact", cmbListaPrecios, "", lstrFiltro)
            If lstrValor <> "" Then
                cmbListaPrecios.Valor = lstrValor
            End If

                '10/03/2011 (33) Toma la lista de precios que corresponde a esa fecha
                Dim diferencia As Int32
                diferencia = DateDiff(DateInterval.Day, CDate(txtFechaValor.Fecha), Today.Date)
                'If DateDiff(DateInterval.Day, CDate(txtFechaValor.Fecha), Today.Date) > mintRecalcuProforma And (hdnConf.Text = "0" Or hdnConf.Text = "") And hdnProformaId.Text <> "" Then
                If diferencia > mintRecalcuProforma And (hdnConf.Text = "0" Or hdnConf.Text = "") And hdnProformaId.Text <> "" Then
                    cmbListaPrecios.Limpiar()
                    clsWeb.gCargarRefeCmb(mstrConn, "precios_lista_fact_consul_fecha", cmbListaPrecios, "", lstrFiltro)
                    cmbListaPrecios.Valor = lstrValor
                End If

                If cmbListaPrecios.Items.Count > 0 And hdnProformaId.Text <> "" And hdnConf.Text = "1" Then
                'si viene de proforma y se recalculan los valores entonces tomar el ultimo precio
                cmbListaPrecios.SelectedIndex = cmbListaPrecios.Items.Count - 1
            End If
            'cmbListaPrecios.Valor = mobj.ObtenerValorCampo(mstrConn, "precios_lista_fact", "lista_id", lstrFiltro)
            hdnListaPrecios.Text = cmbListaPrecios.Valor
        End If
    End Sub
    Private Sub mGuardarDatos(ByVal pintProformaId As Integer)

        'mValidarAranceles()
        Dim ldrDatos As DataRow
        Dim lintIdDeta As Integer
        Dim lstrDesc, lstrFiltro As String
        Dim lstrVacio As String = ""
        Dim ldouImporte, ldouTasaIVA As Double

        ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
        'Dim ds As DataSet = mobj.ArancelesDeProforma(mdsDatos, mstrConn, pintProformaId)
        mAgregarP()
        hdnProformaId.Text = pintProformaId
        Dim ldsProformas As DataSet = mobj.ConsultarProforma(mdsDatos, mstrConn, pintProformaId)
        hdnFechaIva.Text = txtFechaIva.Text
            ' Dario 2013-10-29 para que no pinche con lo nuevo del % 14 bonif lab
            Me.hdnClieId.Text = IIf(Me.usrClie.Valor Is DBNull.Value, "", Me.usrClie.Valor)
            Me.hdnCriaNume.Text = Me.txtCriaNume.Text
        ' fin 2013-10-29
        ' carga la cabecera de la proforma
        With ldsProformas.Tables(TablasProformas.Proformas).Rows(0)
            txtFechaValor.Fecha = .Item("prfr_fecha")
            usrClie.Valor = .Item("prfr_clie_id")
            txtTotNeto.Valor = .Item("prfr_neto")
            ''If cmbFormaPago.Valor Is DBNull.Value Then cmbFormaPago.Valor = .Item("prfr_cpti_id")
            cmbFormaPago.Valor = .Item("prfr_cpti_id")
                If txtFechaIva.Fecha Is DBNull.Value Then txtFechaIva.Fecha = Today ' .Item("prfr_ivar_fecha")
                If cmbActi.Valor = 0 Then cmbActi.Valor = .Item("prfr_acti_id")

            'calcula si pasaron 30 dias desde el fecha Valor de la Proforma y la actual
            'If DateDiff(DateInterval.Day, .Item("prfr_fecha"), txtFechaValor.Fecha) > 30 Then
            ' 07/11/06
            'calcula si pasaron 30 dias desde el fecha Valor de la Proforma y la del dia
            If DateDiff(DateInterval.Day, .Item("prfr_ingr_fecha"), Today.Date) > mintRecalcuProforma Then
                mobj.pProformaRecalcularaValores = True
            Else
                mobj.pProformaRecalcularaValores = False
            End If
            ' por default toma para calcular los aranceles, la fecha originaria de la proforma
            hdnFechaVigenciaProf.Text = .Item("prfr_fecha")

        End With
        mSetearControles()

        mCliente()
        mobj.CambioActividad(cmbActi.Valor)
        mobj.Especiales(mstrConn, usrClie.Valor)
        mEstadoDeSaldos()
        mobj.ConvSaldo(mstrConn, cmbConv.Valor)
        With ldsProformas.Tables(TablasProformas.Proforma_deta).Rows(0)
            If cmbConv.Valor = 0 Then cmbConv.Valor = .Item("prde_conv_id")
            txtCotDolar.Valor = Format(.Item("prde_coti"), "###0.00")
            hdnCotDolar.Text = txtCotDolar.Valor
            'chkTPers.Checked = .Item("prde_pers")
            chkTPers.Checked = True
            If hdnProformaId.Text = "" Then
                chkTPers.Enabled = False
            Else
                'si viene de proforma habilitar siempre para venta telefonica.
                chkTPers.Enabled = True
            End If
            txtTotIVA.Valor = .Item("prde_impo_ivat_tasa")
            txtTotIVARedu.Valor = .Item("prde_impo_ivat_tasa_redu")
            txtTotIVASTasa.Valor = .Item("prde_impo_ivat_tasa_sobre")
            txtObser.Valor = .Item("prde_obse")

            'Si se recalcula el arancel
            'If mobj.pRecalculaValoresProforma Then hdnFechaVigenciaProf.Text = txtFechaValor.Fecha
            '7/11/06 
            'Si se recalcula el arancel se toma la fecha del d�a, que es con la que se calcula 
            ' si parason o no 30 dias


            'Comentado Roxi: 28/10/2008 toma la fecha de la proforma tambien al recalcular.
            '''If mobj.pRecalculaValoresProforma Then hdnFechaVigenciaProf.Text = Today.Date


            'lista de precios
            If hdnFechaVigenciaProf.Text <> "" Then
                'mLlenaComboListaPrecios("", cmbActi.Valor.ToString)
                'Else
                mLlenaComboListaPrecios(CDate(hdnFechaVigenciaProf.Text), cmbActi.Valor.ToString)
            End If

            If hdnProformaId.Text = "" Then  'si viene de proforma tomar el ultimo precio
                cmbListaPrecios.Valor = .Item("prde_peli_id")
                hdnListaPrecios.Text = cmbListaPrecios.Valor
            End If

            'La fecha y lista de precios se habilitan solo si pasaron mas de 30 dias.
            If mobj.pRecalculaValoresProforma Then
                'Roxi: 28/10/2008 toma la fecha de la proforma al recalcular pero la cotizacion actual.
                Dim lstrFecha As String = txtFechaValor.Text
                txtFechaValor.Fecha = Today.Date
                mCotizacionDolar()
                hdnCotDolar.Text = txtCotDolar.Valor
                txtFechaValor.Text = lstrFecha
                hdnCotiProf.Text = hdnCotDolar.Text
                    mLlenaComboListaPrecios(CDate(txtFechaValor.Fecha), cmbActi.Valor.ToString)
                End If
                '03/01/2011
                If DateDiff(DateInterval.Day, CDate(txtFechaValor.Fecha), Today.Date) < mintRecalcuProforma Then
                    cmbListaPrecios.SelectedIndex = 0
                    hdnListaPrecios.Text = cmbListaPrecios.Valor
                End If

                'Roxi: 28/10/2008 deja la fecha disabled
                'txtFechaValor.Enabled = (hdnProf30Dias.Text = "1")
                txtFechaValor.Enabled = False

            cmbListaPrecios.Enabled = (hdnProf30Dias.Text = "1")

            If txtCotDolar.Valor = "0.00" Then
                mCotizacionDolar()
                hdnCotDolar.Text = txtCotDolar.Valor
            End If

            If cmbActi.Valor = Actividades.RRGG Or cmbActi.Valor = Actividades.Laboratorio Or cmbActi.Valor = Actividades.Expo Or cmbActi.Valor = 8 Then     '30/05/2011
                clsWeb.gCargarRefeCmb(mstrConn, "razas", cmbRaza, "N")
                    If .IsNull("prde_cria_id") Then
                        cmbRaza.Valor = IIf(.Item("prde_raza_id") Is DBNull.Value, "", .Item("prde_raza_id"))
                        ' Dario 2013-10-29 para que no pinche con lo nuevo del % 14 bonif lab
                        Me.hdnRazaId.Text = IIf(Me.cmbRaza.Valor Is System.DBNull.Value, "", Me.cmbRaza.Valor)
                        ' fin 2013-10-29
                    Else
                        hdnCria.Text = .Item("prde_cria_id")
                    cmbRaza.Valor = mobj.ObtenerValorCampo(mstrConn, "criadores_razas", "raza_id", .Item("prde_cria_id"))
                    txtCriaNume.Valor = mobj.ObtenerValorCampo(mstrConn, "criadores_razas", "cria_nume", .Item("prde_cria_id"))
                    ' Dario 2013-10-29 para que no pinche con lo nuevo del % 14 bonif lab
                    Me.hdnRazaId.Text = IIf(Me.cmbRaza.Valor Is System.DBNull.Value, "", Me.cmbRaza.Valor)
                    ' fin 2013-10-29
                End If

                mBloqueacontrolesRaza()
            End If

        End With

        mCargarArancelesyConceptos()

        ' carga los aranceles
        For Each ldr As DataRow In ldsProformas.Tables(TablasProformas.Proforma_aranc).Select
            ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
            With ldrDatos
                mobj.SobretasasDelArancelNormal(mstrConn, ldr.Item("pran_aran_id"), cmbActi.Valor, txtFechaIva.Fecha)
                mobj.AranRRGG(mstrConn, ldr.Item("pran_aran_id"), IIf(ldr.IsNull("pran_auto"), 0, 1))
                ' solo los aranceles que no son sobretasa automaticas
                .Item("temp_deta_id") = ldr.Item("pran_id")
                .Item("temp_deta_prfr_id") = ldr.Item("pran_id")
                If mobj.pAranRRGGSobretasa = 0 Then
                    lstrDesc = ldr.Item("_desc_aran")
                    .Item("temp_tipo") = "A"
                    .Item("temp_aran_concep_id") = mValidarNulos(ldr.Item("pran_aran_id"), False)
                    .Item("temp_codi_aran_concep") = ldr.Item("_codi_aran")
                    .Item("temp_desc_ampl") = ldr.Item("pran_desc_ampl")
                    .Item("temp_desc_aran_concep") = ldr.Item("_desc_aran")
                    lstrDesc = mobj.CortarCadena(lstrDesc, 30)
                    .Item("temp_desc_fecha") = "Fecha"
                    If mobj.pRRGG Then
                        Dim lstrsobre As Integer = IIf(mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_rrgg_aran_id", ldr.Item("pran_aran_id")) = "", 0, 1)
                        Dim lstrEnc As String = "A"
                        If lstrsobre = 1 Then lstrEnc = "S"
                        lstrDesc = lstrEnc + "-" + Trim(ldr.Item("_codi_aran")) + "-" + lstrDesc
                        .Item("temp_anim_no_fact") = mValidarNulos(ldr.Item("pran_anim_no_fact"), False)
                        .Item("temp_fecha") = ldr.Item("pran_fecha")
                        .Item("temp_inic_rp") = ldr.Item("pran_inic_rp")
                        .Item("temp_fina_rp") = ldr.Item("pran_fina_rp")
                        .Item("temp_tram") = ldr.Item("pran_tram") '''ver si la proforma crea nro de tramite
                        .Item("temp_acta") = ldr.Item("pran_acta")
                        .Item("temp_desc_fecha") = IIf(mobj.pAranRRGGDescFecha = "", "Fecha:", mobj.pAranRRGGDescFecha)
                        .Item("temp_sobre_auto") = ldr.Item("pran_auto") ' sobretasa automatica
                    End If
                    .Item("temp_cant") = mValidarNulos(ldr.Item("pran_cant"), False)
                    .Item("temp_cant_sin_cargo") = IIf(ldr.IsNull("pran_cant_sin_carg"), 0, ldr.Item("pran_cant_sin_carg"))
                    .Item("temp_desc_aran_concep_cortada") = lstrDesc
                    .Item("temp_tipo_IVA") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "tipo_tasa", ldr.Item("pran_aran_id").tostring)
                    '***************** calculos sobre el arancel ***************************
                    Dim lstrFechaVig As String = ""
                    'lstrfiltro = ldr.Item("pran_aran_id").ToString() + ";" + ldr.Item("pran_cant").ToString() + ";" + cmbActi.Valor.ToString() + ";" + txtFechaIva.Text + ";" + txtCotDolar.Text + ";" + cmbFormaPago.Valor.ToString + ";" + clsFormatear.gFormatFechaDateTime(hdnFechaVigenciaProf.Text) + ";" + lstrVacio
                    lstrFiltro = ldr.Item("pran_aran_id").ToString() + ";" + ldr.Item("pran_cant").ToString() + ";" + cmbActi.Valor.ToString() + ";" + txtFechaIva.Text + ";" + txtCotDolar.Text + ";" + cmbFormaPago.Valor.ToString + ";" + cmbListaPrecios.Valor.ToString + ";" + lstrVacio
                    Dim vsRet As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
                    'precio unitario del arancel
                    .Item("temp_unit_impo") = vsRet(0)
                    'precio unitario con iva
                    .Item("temp_unit_c_iva") = vsRet(5)
                    'tasa iva
                    .Item("temp_tasa_iva") = vsRet(2)
                    'importe total s/iva
                    .Item("temp_impo") = vsRet(1)
                    'importe total c/iva
                    .Item("temp_impo_ivai") = vsRet(4)

                    'precio No Socio para luego generar el concepto adicional
                    mobj.pPrecioNosocio = True

                    lstrFiltro = ldr.Item("pran_aran_id").tostring + ";" + ldr.Item("pran_cant").ToString() + ";" + cmbActi.Valor.ToString() + ";" + txtFechaIva.Text + ";" + txtCotDolar.Valor.ToString + ";" + cmbFormaPago.Valor.ToString + ";" + cmbListaPrecios.Valor.ToString + ";" + lstrVacio
                    Dim vsRets As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
                    mobj.pPrecioNosocio = False
                    If usrClie.DiscrimIVA Then
                        .Item("temp_impo_Nosocio_aran") = vsRets(1)
                    Else
                        .Item("temp_impo_Nosocio_aran") = vsRets(4)
                    End If
                    ' Dario 2013-04-26 incluyo adherente
                    .Item("temp_impo_Adhe_aran") = vsRet(8)

                    .Item("temp_ccos_codi") = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "ccos_codi", ldr.Item("pran_aran_id").tostring)
                    .Item("temp_ccos_id") = ldr.Item("pran_ccos_id")
                    .Item("temp_exen") = IIf(ldr.Item("pran_exen") = 1, "Si", "No")
                    .Item("temp_sin_carg") = IIf(ldr.Item("pran_sin_carg") = 1, "Si", "No")
                    .Item("temp_orden") = OrdenItems.arancel

                    ' Dario 2014-02-07 ejecuto funcion para tenerminar si aplica o no 
                    ' esta fila a la bonificacion del laboratorio
                    Dim IdEspecieLaboBonif As String = Me.mGetEspecieArancelConBonif(mValidarNulos(ldr.Item("pran_aran_id"), False))

                    ' Dario 2014-02-06 Cambio por laboratotio bonificado
                    ' Dario 2014-05-16 se controla que sea distinto de 0 que seria arancel que no calcula bonif lab
                    If (IdEspecieLaboBonif <> "0") Then
                        .Item("temp_IdEspecie") = CType(IdEspecieLaboBonif, Int32)
                        .Item("temp_AcancelABonif") = IIf(IdEspecieLaboBonif.Trim.Length > 0, "Si", "No")
                    Else
                        .Item("temp_IdEspecie") = CType(0, Int32)
                        .Item("temp_AcancelABonif") = "No"
                    End If

                    chkSCargoAran.Checked = IIf(.Item("temp_sin_carg") = "Si", 1, 0)
                    ldrDatos.Table.Rows.Add(ldrDatos)
                    lintIdDeta = ldrDatos.Item("temp_id")

                    ' sobretasas - solo para RRGG 
                    If mobj.pRRGG Then
                        mSobretasas(lintIdDeta, ldr.Item("pran_aran_id"), ldr.Item("pran_cant"), IIf(ldr.IsNull("pran_fecha"), Today.Date, ldr.Item("pran_fecha")), IIf(hdnFechaVigenciaProf.Text = "", Today.Date, clsFormatear.gFormatFechaDateTime(hdnFechaVigenciaProf.Text)))
                    End If

                    'laboratorio: agrega recargo/descuentos de turnos
                    If mobj.pLabora Then
                        .Item("temp_turno_SP") = ldr.Item("_prad_usua_sobr_paff")
                        .Item("temp_turno_RT") = ldr.Item("_prad_usua_reca_turn")
                        .Item("temp_turno_DP") = ldr.Item("_prad_usua_dcto_prom")
                        .Item("temp_turno_DC") = ldr.Item("_prad_usua_dcto_cant")
                        .Item("temp_turno_RTU") = ldr.Item("_prad_usua_tram_urge")
                        .Item("temp_turno_tipo") = ldr.Item("_prad_tipo")
                    End If


                    ' conceptos autom�ticos
                    mConceptoAutoSinCargo(lintIdDeta, IIf(usrClie.DiscrimIVA, .Item("temp_impo"), .Item("temp_impo_ivai")), ldr.Item("pran_aran_id"))
                    mConcepAutomTurnos(ldrDatos, lintIdDeta, vsRet(1), ldr.Item("pran_aran_id"))
                    'mConceptoAutoLabTurnos(ldrDatos, lintIdDeta, .Item("temp_impo").ToString(), .Item("temp_aran_concep_id").ToString())

                    'elimino los conceptos automaticos que representan varios aranceles
                    mBorrar_Conceptos()
                    mConceptoAutoAdicionalNoSocio()
                    mConceptoAutoAdicionalConvenio()

                    ' Dario 2014-02-07 nuevo concepto automatico bonif laboratorio
                    Me.mConceptoAutoBonifLab()
                    'Roxi: 11/10/2007 agregar la diferencia (precios nosocio-socio) para las sobretasas
                    mAdicionalesPreciosSobreTasas()

                End If 'aranceles que no son sobretasa
            End With

        Next
        'carga conceptos
        For Each ldr As DataRow In ldsProformas.Tables(TablasProformas.Proforma_concep).Select

            ldrDatos = mdsDatos.Tables(mstrTablaTempDeta).NewRow
            With ldrDatos
                'conceptos que no sean autom�ticos
                If ldr.IsNull("prco_auto") Or ldr.item("prco_auto") = 0 Then
                    lstrDesc = ldr.Item("_conc_desc")
                    .Item("temp_tipo") = "C"
                    .Item("temp_aran_concep_id") = ldr.item("prco_conc_id")
                    .Item("temp_codi_aran_concep") = mobj.ObtenerValorCampo(mstrConn, "conceptos", "conc_codi", .Item("temp_aran_concep_id"))
                    lstrDesc = mobj.CortarCadena(lstrDesc, 30)
                    If mobj.pRRGG Then
                        lstrDesc = "C" + "-" + Trim(.Item("temp_codi_aran_concep")) + "-" + lstrDesc
                        ' lstrDesc = "C" + "-" + lstrDesc
                    End If
                    .Item("temp_desc_aran_concep_cortada") = lstrDesc
                    If cmbConcep.SelectedItem.Value <> "" Then
                        .Item("temp_desc_aran_concep") = cmbConcep.SelectedItem
                    Else
                        .Item("temp_desc_aran_concep") = ldr.item("_conc_desc").ToString
                    End If
                    .Item("temp_ccos_id") = ldr.Item("prco_ccos_id")
                    .Item("temp_deta_prfr_id") = ldr.Item("prco_id")
                    '********************* calculos sobre el concepto ***************
                    '****************************************************************
                    .Item("temp_impo") = ldr.Item("prco_impo")
                    ldouImporte = ldr.Item("prco_impo")
                    If usrClie.DiscrimIVA Then
                        ldouTasaIVA = mValorTasaIva(ldr.item("prco_conc_id").ToString, txtFechaIva.Fecha)
                        If ldouTasaIVA > 0 Then
                            ldouImporte = CType(ldr.Item("prco_impo"), Double) * (1 + (ldouTasaIVA / 100))
                        End If
                    End If
                    .Item("temp_impo_ivai") = ldouImporte

                    '*****************************************************************
                    .Item("temp_desc_ampl") = ldr.Item("prco_desc_ampl")
                    .Item("temp_tasa_iva") = ldr.Item("prco_tasa_iva")
                    .Item("temp_auto") = ldr.Item("prco_auto")
                    .Item("temp_tipo_IVA") = mobj.ObtenerValorCampo(mstrConn, "conceptosX", "tipo_tasa", ldr.item("prco_conc_id").ToString)

                    ldrDatos.Table.Rows.Add(ldrDatos)
                End If 'concep No automa
            End With
        Next
        If mobj.pLabora Then
            mTurnosPendientes("B")
            mTurnosPendientes("E")
        End If
        chkSCargoAran.Checked = False
        mCalcularTotal()
        mCargarAutorizaciones()


    End Sub
    Private Sub mTurnosPendientes(ByVal pstrOrigen As String)
        'busca el turno que fue ingresado en la proforma

        If mdsDatos.Tables(mstrTablaTempDeta).Select("temp_tipo='A' and temp_turno_tipo='" & pstrOrigen & "'").GetUpperBound(0) > -1 Then

            Dim ldt As DataTable = mobj.TurnosGrillaDS(mstrConn, usrClie.Valor.ToString, hdnProformaId.Text, pstrOrigen)
            Dim lstrFiltro As String
            Dim ldr, ldrTurnos, ldrDetaAran As DataRow
            Dim ldsDetaAran As DataSet
            If ldt.Select().GetUpperBound(0) > -1 Then
                'recorre los aranceles (TablaTempDeta)
                For Each ldrDeta As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select("temp_tipo='A'")
                    'busca el detalle del turno, si lo tuviese, del arancel  
                    lstrFiltro = "@prad_pran_id=" + clsSQLServer.gFormatArg(ldrDeta.Item("temp_deta_id"), SqlDbType.Int) + ", @prad_tipo=" + pstrOrigen
                    ldsDetaAran = clsSQLServer.gExecuteQuery(mstrConn, "exec dbo.proforma_aranceles_deta_consul " & lstrFiltro)
                    'crea el filtro para seleccionar el turno que le corresponde al arancel
                    If ldsDetaAran.Tables(0).Select().GetUpperBound(0) > -1 Then
                        ldrDetaAran = ldsDetaAran.Tables(0).Select()(0)
                        'ldr = ldt.Select()(0)
                        lstrFiltro = "TURN_REC >=" + ldrDetaAran.Item("prad_turn_reci").ToString
                        lstrFiltro = lstrFiltro + " and SOB_PFF =" + ldrDetaAran.Item("prad_sobr_paff").ToString
                        lstrFiltro = lstrFiltro + " and REC_EFF =" + ldrDetaAran.Item("prad_reca_turn").ToString
                        lstrFiltro = lstrFiltro + " and DTO_OPP =" + ldrDetaAran.Item("prad_dcto_prom").ToString
                        lstrFiltro = lstrFiltro + " and DTO_OPC =" + ldrDetaAran.Item("prad_dcto_cant").ToString
                        lstrFiltro = lstrFiltro + " and REC_PTU =" + ldrDetaAran.Item("prad_tram_urge").ToString
                        lstrFiltro = lstrFiltro + " and REGICONT =" + ldrDetaAran.Item("prad_codi").ToString
                        lstrFiltro = lstrFiltro + " and FECHA_REC =" + clsFormatear.gFormatFechaDS(ldrDetaAran.Item("prad_rece_fecha"))

                        'ingresa el id del dt de turnos (es el mismo que muestra la grilla de turnos)
                        ldrTurnos = ldt.Select(lstrFiltro)(0)
                        ldrDeta.Item("temp_rela_turno_id") = ldrTurnos.Item("ID")
                        ldrDeta.Item("temp_rela_id_aux") = ldrTurnos.Item("ID")
                        ldrDeta.Item("temp_turnos") = pstrOrigen
                        mobj.CantTotalTurnosFact(mdsDatos, ldrTurnos.Item("ID"), pstrOrigen)
                    End If
                Next
            End If
        End If
    End Sub
        Private Sub mGuardarDatos_cuotas(Optional ByVal lboolAuto As Boolean = False)

            Dim ldrDatos As DataRow
            If Not lboolAuto Then
                mValidarCuotas()
            End If
            If hdnCuotasId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrTablaVtos).NewRow
                ldrDatos.Item("covt_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaVtos), "covt_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrTablaVtos).Select("covt_id=" & hdnCuotasId.Text)(0)
            End If
            Dim lDsDatos As DataSet

            With ldrDatos
                If lboolAuto Then
                    If cmbFormaPago.Valor <> mintCtaCte Then
                        .Item("covt_fecha") = Today ' fecha del dia
                    Else
                        .Item("covt_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, cmbActi.Valor, Today)
                    End If
                    .Item("covt_porc") = 0
                    If (txtTotNeto.Text.Trim().Length > 0) Then
                        .Item("covt_impo") = txtTotNeto.Valor
                    Else
                        .Item("covt_impo") = 0
                    End If
                    .Item("covt_cance") = IIf(txtTotNeto.Valor = "0", 1, 0)
                Else
                    mobj.CalcularValoresCuota(lblImpTotal.Text, IIf(txtCuotPorc.Text.Trim().Length = 0, 0, txtCuotPorc.Valor), IIf(txtCuotImporte.Text.Trim().Length = 0, 0, txtCuotImporte.Valor))
                    'si es cta. cte no puede tener una cuota en cero
                    If mobj.pPagoCtaCte And mobj.pCuotaImporte = 0 Then Exit Sub
                    .Item("covt_fecha") = txtCuotFecha.Fecha
                    .Item("covt_porc") = mobj.pCuotaPorcentaje
                    .Item("covt_impo") = mobj.pCuotaImporte
                    .Item("covt_cance") = 0
                End If

            End With
            If (mEstaEnElDataSetFecha(mdsDatos.Tables(mstrTablaVtos), ldrDatos, "covt_fecha", "covt_id")) Then
                If Not lboolAuto Then Throw New AccesoBD.clsErrNeg("Ya existe una cuota con esa fecha.")
            Else
                If hdnCuotasId.Text = "" Then
                    mdsDatos.Tables(mstrTablaVtos).Rows.Add(ldrDatos)
                End If
            End If
        End Sub

        Private Sub mCargarAutorizaciones()


        mobj.CargarAutorizaciones(mdsDatos, mstrConn, usrClie.Valor, txtFechaValor.Text, cmbConv.Valor, cmbListaPrecios.Valor.ToString(), cmbActi.Valor.ToString())
        mdsDatos.Tables(mstrTablaAutor).DefaultView.Sort = "_auti_desc"
        grdAutoriza.DataSource = mdsDatos.Tables(mstrTablaAutor).DefaultView
        grdAutoriza.DataBind()

    End Sub
    Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrDatos As System.Data.DataRow, ByVal campo1 As String, ByVal campo2 As String, ByVal campoId As String) As Boolean
        Dim filtro As String
        filtro = campo1 + " = " + ldrDatos.Item(campo1).ToString() + " AND " + campo2 + " = " + ldrDatos.Item(campo2).ToString() + " AND " + campoId + " <> " + ldrDatos.Item(campoId).ToString()
        Return (table.Select(filtro).GetLength(0) > 0)
    End Function
    Private Function mEstaEnElDataSet(ByVal table As System.Data.DataTable, ByVal ldrDatos As System.Data.DataRow, ByVal campo1 As String, ByVal campoId As String) As Boolean
        Return mEstaEnElDataSet(table, ldrDatos, campo1, campo1, campoId)
    End Function
    Private Function mEstaEnElDataSetFecha(ByVal table As System.Data.DataTable, ByVal ldrDatos As System.Data.DataRow, ByVal campofecha As String, ByVal campoId As String) As Boolean
        Dim filtro As String
        'filtro = campofecha + " = '" + CDate(ldrDatos.Item(campofecha)).ToString("MM/dd/yyyy") + "' AND " + campoId + " <> " + ldrDatos.Item(campoId).ToString()
        filtro = campofecha + " = '" + ldrDatos.Item(campofecha) + "' AND " + campoId + " <> " + ldrDatos.Item(campoId).ToString()

        Return (table.Select(filtro).GetLength(0) > 0)
    End Function
    Public Sub mConsultar(ByVal pOk As Boolean)
        Try
            Dim strFin As String = ""
            If Not pOk Then
                strFin = ",@ejecuta = N "
            End If
            mstrCmd = "exec " + mstrTabla + "_consul " + strFin
            'clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBorrarDatosCuotas()
        For Each ldr As DataRow In mdsDatos.Tables(mstrTablaVtos).Select
            With ldr
                ldr.Delete()
            End With
        Next

    End Sub
    Private Sub mActualizarCuotas(Optional ByVal lboolAuto As Boolean = False)

        mGuardarDatos_cuotas(lboolAuto)
        mCalcularSaldoCuotas()

        mLimpiarCuotas()
        With mdsDatos.Tables(mstrTablaVtos)
            .DefaultView.Sort = "covt_fecha"
            grdCuotas.DataSource = .DefaultView
        End With
        grdCuotas.DataBind()

    End Sub
    Private Sub mCargarGrillaArancelesConceptos()
        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.Sort = "temp_orden"
        grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta).DefaultView
        grdDetalle.DataBind()
        mCalcularTotal()
    End Sub
    Private Sub mActualizarAranceles()
        Dim lintId As Integer
        mValidaDatosRRGG_Avisos() ' son solo avisos, el programa de debe seguir
        mGuardarDatosDeta_aran()
        ' turnos de laboratorio
        mCalcularCantTurnos()
        mLimpiarDetaAran()

        mCargarGrillaArancelesConceptos()

    End Sub
    Private Sub mBloqueacontrolesRaza()
        If cmbActi.Valor = Actividades.RRGG Or cmbActi.Valor = Actividades.Laboratorio Then
            If Not cmbRaza.Valor Is DBNull.Value Then
                cmbRaza.Enabled = False
                txtCriaNume.Enabled = False
            End If
        End If
    End Sub
    Private Function mCalcularCantTurnos()
        If mobj.pLabora Then
            Dim lintId As Integer
            Dim lstrTipo As String

            If hdnDatosPopTurnoBov.Text <> "" Or hdnDatosPopTurnoEqu.Text <> "" Then
                If hdnDatosPopTurnoBov.Text <> "" Then
                    lintId = hdnDatosPopTurnoBov.Text
                    lstrTipo = "B"
                End If
                If hdnDatosPopTurnoEqu.Text <> "" Then
                    lintId = hdnDatosPopTurnoEqu.Text
                    lstrTipo = "E"
                End If
                mobj.CantTotalTurnosFact(mdsDatos, lintId, lstrTipo)
            End If
        End If
    End Function
    Private Sub mActualizarConceptos()
        mGuardarDatosDeta_concep()
        mLimpiarDetaConcep()
        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.Sort = "temp_orden"
        grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta).DefaultView
        grdDetalle.DataBind()
        mCalcularTotal()
    End Sub
#End Region

#Region "Eventos de Controles"


    Private Sub txtFechaValor_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFechaValor.TextChanged
        Try
            mLlenaComboListaPrecios(txtFechaValor.Fecha, cmbActi.Valor.ToString())
            mRecalculaAranceles()
            hdnFocoSig.Text = "S"
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub cmbFormaPago_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFormaPago.SelectedIndexChanged
        mRecalculaAranceles()
    End Sub
    Private Sub cmbListaPrecios_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbListaPrecios.SelectedIndexChanged
        mRecalculaAranceles()
    End Sub

    Private Sub hdnDatosPopTurnoEqu_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPopTurnoEqu.TextChanged
        Try
            Dim larrDato As System.Collections.ArrayList
            Dim lstrIDTurno As String
            If (hdnDatosPopTurnoEqu.Text <> "") Then
                Dim lvrDatos As String = hdnDatosPopTurnoEqu.Text
                'carga la cantidad 
                larrDato = mobj.FilaDSTurnos(TablasTurnos.inturfac_equ, CType(hdnDatosPopTurnoEqu.Text, Integer))
                'si la cant es cero no la carga
                If larrDato.Item(CamposTurnos.cant) > 0 Then
                    lstrIDTurno = hdnDatosPopTurnoEqu.Text
                    mLimpiarDetaAran()
                    hdnDatosPopTurnoEqu.Text = lstrIDTurno
                    txtCantAran.Valor = larrDato.Item(CamposTurnos.cant)
                    txtTurnosSP.Valor = larrDato.Item(CamposTurnos.SOB_PFF)
                    txtTurnosRT.Valor = larrDato.Item(CamposTurnos.REC_EFF)
                    txtTurnosRTU.Valor = larrDato.Item(CamposTurnos.REC_PTU)
                    txtTurnosDP.Valor = larrDato.Item(CamposTurnos.DTO_OPP)
                    txtTurnosDC.Valor = larrDato.Item(CamposTurnos.DTO_OPC)
                End If
                mSetearEditor("", True)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub hdnDatosPopTurnoBov_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPopTurnoBov.TextChanged
        Try
            Dim larrDato As System.Collections.ArrayList
            Dim lstrIDTurno As String
            If (hdnDatosPopTurnoBov.Text <> "") Then
                Dim lvrDatos As String = hdnDatosPopTurnoBov.Text
                'carga la cantidad 
                larrDato = mobj.FilaDSTurnos(TablasTurnos.inturfac_bov, CType(hdnDatosPopTurnoBov.Text, Integer))
                'si la cant es cero no la carga
                If larrDato.Item(CamposTurnos.cant) > 0 Then
                    lstrIDTurno = hdnDatosPopTurnoBov.Text
                    mLimpiarDetaAran()
                    hdnDatosPopTurnoBov.Text = lstrIDTurno
                    txtCantAran.Valor = larrDato.Item(CamposTurnos.cant)
                    txtTurnosSP.Valor = larrDato.Item(CamposTurnos.SOB_PFF)
                    txtTurnosRT.Valor = larrDato.Item(CamposTurnos.REC_EFF)
                    txtTurnosRTU.Valor = larrDato.Item(CamposTurnos.REC_PTU)
                    txtTurnosDP.Valor = larrDato.Item(CamposTurnos.DTO_OPP)
                    txtTurnosDC.Valor = larrDato.Item(CamposTurnos.DTO_OPC)
                End If
                mSetearEditor("", True)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub hdnDatosTarjPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosTarjPop.TextChanged
        Try
            If (hdnDatosTarjPop.Text <> "") Then
                cmbFormaPago.Valor = mintTarj
                'copiar los datos de la tarjeta 
            End If

            If hdnTarjVenc.Text = "1" Then
                clsError.gGenerarMensajes(Me, "La tarjeta esta vencida")
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub hdnAutoriza_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnAutoriza.TextChanged
        'If hdnAutoriza.Text <> "" Then
        'si autoriza que se emita una fact. en cta. cte. sin tenerla
        Try
            mobj.GuardarAutorizaciones(mdsDatos, grdAutoriza)

            '   End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged

        Try
            ' si no cancela la impresion
            ' y no es proforma (no es necesario controlar la impresi�n)
            If CBool(CInt(hdnImprimio.Text)) And mobj.pTipoComprobGenera = TipoComprobGene.Factura Then
                mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnImprimir.Text)

                With mdsDatos.Tables(0)
                    .TableName = mstrTabla
                    .Rows(0).Item("comp_impre") = CBool(CInt(hdnImprimio.Text))
                End With

                While mdsDatos.Tables.Count > 1
                    mdsDatos.Tables.Remove(mdsDatos.Tables(1))
                End While

                Dim lobj As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

                lobj.Modi()
            End If
            'ventana con tramites generados (tanto facturas como proformas)
            mobj.LetraComprob(mstrConn, usrClie.TipoIVAId)
            Dim lstrletra As String = mobj.pLetraComprob
            Dim lstrProf As String = ""
            If mobj.pTipoComprobGenera <> TipoComprobGene.Factura Then lstrProf = "S"
            mAbrirVentana(hdnIdGenerado.Text, hdnComprobante.Text, hdnCompGenera.Text, lstrletra, lstrProf)
            mAgregar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub cmbComp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbComp.SelectedIndexChanged
        Try
            mSelecProformaoFactura()
            mHabilitarCobranza(True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAran_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAran.Click
        Try
            mShowTabs(Panta.Arancel)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnConcep_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnConcep.Click
        Try
            mShowTabs(Panta.Concepto)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub txtCantAran_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCantAran.TextChanged
        Try
            ' mCalcularImporteAranceles()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub txtImporteConcep_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtImporteConcep.TextChanged
        Try
            mCalcularImporteConceptos("P")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub ImgbtnLimpiar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgbtnLimpiar.Click
        Try
            mAgregar()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAltaCuot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaCuot.Click
        Try
            mActualizarCuotas()
            mCalcularSaldoCuotas()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiCuot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCuot.Click
        Try
            mActualizarCuotas()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnLimpiarCuot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpiarCuot.Click
        Try
            mLimpiarCuotas()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnBajaCuot_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaCuot.Click
        Try
            mdsDatos.Tables(mstrTablaVtos).Select("covt_id=" & hdnCuotasId.Text)(0).Delete()
            grdCuotas.DataSource = mdsDatos.Tables(mstrTablaVtos)
            grdCuotas.DataBind()
            mLimpiarCuotas()
            mCalcularSaldoCuotas()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mCantTotalTurnosFact()
        'cuando se da de baja un arancel
        Dim ldrDatos As DataRow = mdsDatos.Tables(mstrTablaTempDeta).Select("temp_id = " & hdnDetaTempId.Text)(0)
        Dim lintIdTurnos As Integer
        With ldrDatos
            lintIdTurnos = .Item("temp_rela_turno_id")

        End With
    End Sub
    Private Sub btnBajaConcep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaConcep.Click
        Try
            mBajaItemTempDeta(hdnDetaTempId.Text)
            mLimpiarDetaConcep()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnModiConcep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiConcep.Click
        Try
            mActualizarConceptos()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnBajaAran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaAran.Click
        Try

            mBorrarSobretasas_Conceptos(hdnDetaTempId.Text)
            mBajaItemTempDeta(hdnDetaTempId.Text)
            mLimpiarDetaAran()
            mBorrar_Conceptos()
            mConceptoAutoAdicionalNoSocio()
            mConceptoAutoAdicionalConvenio()
            mAdicionalesPreciosSobreTasas()
            mCargarGrillaArancelesConceptos()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub ImgbtnGenerar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImgbtnGenerar.Click
        Try
            mRecalculaAranceles()
            mAlta()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnLimpAran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpAran.Click
        mLimpiarDetaAran()
    End Sub
    Private Sub btnModiAran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiAran.Click
        Try
            mActualizarAranceles()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAltaAran_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaAran.Click
        Try
            mActualizarAranceles()

            mSetearEditor("", True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAplicCred_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAplicCred.Click
        Try
            mAltaConCobranzas("AC", "2")
            'mAltaConCobranzas2(mstrCobraId, "AC", "2")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnCobra_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCobra.Click
        Try
            mAltaConCobranzas("RE")
            'mAltaConCobranzas2(mstrCobraId, "RE", "")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAltaConcep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaConcep.Click
        Try
            mActualizarConceptos()
            mSetearEditor("", True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnLimpiarConcep_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpiarConcep.Click

        mLimpiarDetaConcep()
    End Sub

#End Region

#Region "Paneles"
    Private Sub mNaveg(ByVal pstrPos As String)

        mValidarNavega()
        mPasarSalopaCab(pstrPos)

        Select Case pstrPos
            Case "P"                                'principal
                hdnActiId.Text = cmbActi.Valor
                hdnFechaIva.Text = txtFechaIva.Text
                Me.hdnClieId.Text = usrClie.Valor
                mCargarAutorizaciones()
                If mobj.CambioActividad(cmbActi.Valor) Then
                    If hdnProformaId.Text = "" Then mLimpiar(True)
                    mCargarArancelesyConceptos()
                Else
                    mCargarAranceles()
                End If
                mCargarArancelesyConceptos()
                'mobj.Actividad(cmbActi.Valor)
                'mobj.Autorizaciones()
                mTituloCabecera()
                mControlesIVA()
                'se agrego aca esta validacion para que le avise al usuario antess de cambiar de solapa que 
                'debe seleccionar otra forma de pago para poder seguir adelante
                mValidaSinCtaCte()
                mControlesMostrarOcultar()
                mShowTabs(Panta.Arancel)

                mobj.PrecioSocio_Falle(mstrConn, usrClie.Valor, txtFechaValor.Fecha)

                ' Dario 2013-08-28 crea Objeto para obtener el dato de actividad permite proforma
                Dim _ActividadesBusiness As New Business.Facturacion.ActividadesBusiness
                Dim bitAdmiteProforma As Boolean = _ActividadesBusiness.GetAdmiteProforma(cmbActi.Valor)
                ' 
                If mobj.CajaCerrada(mstrConn, cmbFormaPago.Valor, mobj.pCentroEmisorId) Then
                    ' Dario 2013-08-08 se controla si admite proforma para mortrar el error que corresponda
                    If (bitAdmiteProforma = True) Then
                        AccesoBD.clsError.gGenerarMensajes(Me, "La caja fue cerrada. Solamente podra generar FC. en cta.cte. o Proformas. ")
                    Else
                        AccesoBD.clsError.gGenerarMensajes(Me, "La caja fue cerrada. Solamente podra generar FC. en cta.cte. ")
                    End If
                End If

                If hdnProformaId.Text = "" Then
                    mobj.Proforma(cmbActi.Valor)
                    If mobj.pProforma Then '(cta-cte y RRGG o Lab)
                        Dim lstrMensaje As String
                        mProformaMensaje(lstrMensaje)
                        If lstrMensaje <> "" Then
                            ' Dario 2013-08-08  se controla si admite proforma para mortrar el error que corresponda
                            If (bitAdmiteProforma = True) Then
                                hdnComprobante.Text = "Proforma"
                                If mobj.SeleccionaComprobante(hdnComprobante.Text, cmbComp.Valor, mdsDatos) = TipoComprobGene.Factura Then hdnComprobante.Text = "Factura"

                                AccesoBD.clsError.gGenerarMensajes(Me, "Si no es autorizada la generaci�n de la factura, se emitir� una Proforma. " & lstrMensaje)
                            Else
                                AccesoBD.clsError.gGenerarMensajes(Me, "Si no es autorizada la generaci�n de la factura, solamente podra generar FC. en cta.cte. ")
                            End If
                        End If
                    Else
                        mobj.pProformaAutoExcSaldo = False
                        'mRRGGoLabComprobVisible(True)
                    End If
                End If
                mRecalculaAranceles()
                mBloquearControlesCabe(True)

            Case "D-"
                mShowTabs(Panta.Encabezado)
                mDiv()
            Case "D+"                            'deta posterior
                mCalcularTotal()

                ' Dario 2013-07-08 Objeto para obtener el dato de actividad
                Dim _ActividadesBusiness As New Business.Facturacion.ActividadesBusiness
                Dim bitPermitePagoEnCuotas As Boolean = _ActividadesBusiness.GetPermitePagoEnCuotas(cmbActi.Valor)
                ' si la forma de pago es cuenta corriente y la actividad permite el pago en cuotas
                ' muestro panel que corresonda
                If mobj.pPagoCtaCte And bitPermitePagoEnCuotas = True Then  'And Not mobj.pProforma
                    'cuotas
                    mShowTabs(Panta.Cuotas)
                Else
                    mCargarAutorizaciones()
                    If mobj.pAutorizaciones() Then
                        'tiene autorizaciones
                        mShowTabs(Panta.Autorizacion)
                    Else
                        If cmbFormaPago.Valor = mintTarj Then ' pago tarjeta
                            'pago con tarjeta
                            mShowTabs(Panta.Tarjeta)
                        Else
                            'va al pie (leyenda)  
                            mShowTabs(Panta.Pie)
                            mCargarComboComprob()
                        End If
                    End If
                End If

            Case "C-"
                If mobj.pRRGG Then                'RRGG
                    'mostrar los controles necesarios
                Else
                    'ocultar los controles necesarios
                End If
                mShowTabs(Panta.Arancel)

            Case "C+"
                mCargarAutorizaciones()
                If mobj.pAutorizaciones() Then
                    'tiene autorizaciones
                    mShowTabs(Panta.Autorizacion)
                Else
                    If cmbFormaPago.Valor = mintTarj Then ' pago tarjeta
                        'pago con tarjeta
                        mShowTabs(Panta.Tarjeta)
                    Else
                        'va al pie (leyenda)  
                        mShowTabs(Panta.Pie)
                        mCargarComboComprob()
                    End If
                End If
            Case "A+"
                If cmbFormaPago.Valor = mintTarj Then ' pago tarjeta
                    'pago con tarjeta
                    mShowTabs(Panta.Tarjeta)
                Else
                    'va al pie (leyenda)  
                    mShowTabs(Panta.Pie)
                    mCargarComboComprob()
                End If
            Case "A-"
                ' Dario 2013-07-08 Objeto para obtener el dato de actividad
                Dim _ActividadesBusiness As New Business.Facturacion.ActividadesBusiness
                Dim bitPermitePagoEnCuotas As Boolean = _ActividadesBusiness.GetPermitePagoEnCuotas(cmbActi.Valor)
                ' si la forma de pago es cuenta corriente y la actividad permite el pago en cuotas
                ' muestro panel que corresonda
                If mobj.pPagoCtaCte And bitPermitePagoEnCuotas = True Then  'And Not mobj.pProforma
                    'cuotas
                    mShowTabs(Panta.Cuotas)
                Else
                    'detalle 
                    If mobj.pRRGG Then                'RRGG
                        'mostrar los controles necesarios
                    Else
                        'ocultar los controles necesarios
                    End If
                    mShowTabs(Panta.Arancel)
                End If
            Case "T+"
                'va al pie (leyenda)  
                mShowTabs(Panta.Pie)
                mCargarComboComprob()
            Case "T-"
                If mobj.pAutorizaciones() Then
                    'tiene autorizaciones
                    mShowTabs(Panta.Autorizacion)
                Else
                    ' Dario 2013-07-08 Objeto para obtener el dato de actividad
                    Dim _ActividadesBusiness As New Business.Facturacion.ActividadesBusiness
                    Dim bitPermitePagoEnCuotas As Boolean = _ActividadesBusiness.GetPermitePagoEnCuotas(cmbActi.Valor)
                    ' si la forma de pago es cuenta corriente y la actividad permite el pago en cuotas
                    ' muestro panel que corresonda
                    If mobj.pPagoCtaCte And bitPermitePagoEnCuotas = True Then  'And Not mobj.pProforma
                        'cuotas
                        mShowTabs(Panta.Cuotas)
                    Else
                        'detalle
                        If mobj.pRRGG Then             'RRGG
                            'mostrar los controles necesarios
                        Else
                            'ocultar los controles necesarios
                        End If
                        mShowTabs(Panta.Arancel)
                    End If
                End If
            Case "P-"                          ' pie
                If cmbFormaPago.Valor = mintTarj Then ' pago tarjeta
                    'pago con tarjeta
                    mShowTabs(Panta.Tarjeta)
                Else
                    If mobj.pAutorizaciones() Then
                        'tiene autorizaciones
                        mShowTabs(Panta.Autorizacion)
                    Else
                        ' Dario 2013-07-08 Objeto para obtener el dato de actividad
                        Dim _ActividadesBusiness As New Business.Facturacion.ActividadesBusiness
                        Dim bitPermitePagoEnCuotas As Boolean = _ActividadesBusiness.GetPermitePagoEnCuotas(cmbActi.Valor)
                        ' si la forma de pago es cuenta corriente y la actividad permite el pago en cuotas
                        ' muestro panel que corresonda
                        If mobj.pPagoCtaCte And bitPermitePagoEnCuotas = True Then  'And Not mobj.pProforma
                            'cuotas
                            mShowTabs(Panta.Cuotas)
                        Else
                            'detalle
                            If mobj.pRRGG Then             'RRGG
                                'mostrar los controles necesarios
                            Else
                                'ocultar los controles necesarios
                            End If
                            mShowTabs(Panta.Arancel)
                        End If
                    End If
                End If
            Case "P--"                     'al principal
                mShowTabs(Panta.Encabezado)
                mDiv()
        End Select

    End Sub

#Region "Eventos botones Naveg"

    Private Sub btnPosteriorDeta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosteriorDeta.Click
        Try
            mNaveg("D+")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAnteriorDeta_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorDeta.Click
        mNaveg("D-")
    End Sub
    Private Sub btnSigCabe_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnSigCabe.Click
        Try
            mNaveg("P")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnPosteriorTarj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosteriorTarj.Click
        Try
            mNaveg("T+")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAnteriorTarj_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorTarj.Click
        Try
            mNaveg("T-")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAnteriorPie_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorPie.Click
        Try
            mNaveg("P-")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAnteriorCuot_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorCuot.Click
        Try
            mNaveg("C-")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnPosteriorCuot_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosteriorCuot.Click
        Try
            mNaveg("C+")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnPosteriorAutoriza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPosteriorAutoriza.Click
        Try
            mobj.GuardarAutorizaciones(mdsDatos, grdAutoriza)
            mNaveg("A+")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAnteriorAutiza_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAnteriorAutiza.Click
        Try
            mobj.GuardarAutorizaciones(mdsDatos, grdAutoriza)
            mNaveg("A-")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnEncabezadoPie_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEncabezadoPie.Click
        Try
            mNaveg("P--")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#End Region

#Region "Alertas/Ventanas-pop"



    Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
        Try
            If (hdnDatosPop.Text <> "") Then
                Dim lvrDatos As String = hdnDatosPop.Text
                txtObser.Valor = mobj.ObtenerValorCampo(mstrConn, "leyendas_factu", "lefa_desc", lvrDatos)
                hdnDatosPop.Text = ""
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub hdnDatosPopProforma_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPopProforma.TextChanged
        Try
            If (hdnDatosPopProforma.Text <> "") Then
                mobj.RecalculaValoresProforma(IIf(hdnConf.Text = "", 0, hdnConf.Text))
                Dim lvrDatos As String = hdnDatosPopProforma.Text
                'cargar la proforma y hacer todos los calculos y validaciones
                mGuardarDatos(hdnDatosPopProforma.Text)
                mCargarGrillaArancelesConceptos()
                mobj.pProformaPend = True
                hdnDatosPopProforma.Text = ""
                hdnProf30Dias.Text = ""
                'hdnConf.Text = ""
                hdnFPagoAnterior.Text = ""
                mBloquearControlesCabe(True)
                mSetearEditor("", True)
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub hdnDatosCPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosCPop.TextChanged
        Try
            If hdnDatosCPop.Text = 1 Then
                ' selecciona factura o proforma
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            hdnDatosCPop.Text = ""
        End Try
    End Sub


    Private Sub mValidarSeleccionCliente()
        If usrClie.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar un cliente.")
        End If
    End Sub
    Private Sub mSetearControles()
        'para que no grabe mas de una vez
        btnAltaAran.Attributes.Add("onclick", "return GrabarUnaVez(this);")
        btnAltaConcep.Attributes.Add("onclick", "return GrabarUnaVez(this);")
        ' txtFechaValor.Attributes.Add("onchange", "return mPasarFoco();")

        If mobj.pTarjetas Then
            DivimgTarj.Style.Add("display", "inline")
            Dim lstrImg As String = imgTarjTele.Src
            ' Dario 2013-08-29 por error cuando no trae datos de venta telefonica
            Dim taclvtatele As Boolean = False
            If (clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_clientes_consul @tacl_clie_id=" + usrClie.Valor, "tacl_vta_tele") <> "") Then
                taclvtatele = Convert.ToBoolean(clsSQLServer.gCampoValorConsul(mstrConn, "tarjetas_clientes_consul @tacl_clie_id=" + usrClie.Valor, "tacl_vta_tele"))
            End If
            ' fin Dario 2013-08-29
            If taclvtatele Then
                lstrImg = lstrImg.Replace("CreditCardTele.gif", "CreditCard.gif")
            Else
                lstrImg = lstrImg.Replace("CreditCard.gif", "CreditCardTele.gif")
            End If
            imgTarjTele.Src = lstrImg
        Else
            DivimgTarj.Style.Add("display", "none")
        End If
        divClieGene.Style.Add("display", IIf(usrClie.ClienteGenerico(mstrConn), "inline", "none"))
        divExpo.Style.Add("display", IIf(cmbActi.Valor = Actividades.Expo, "inline", "none"))

        DivTurnos.Style.Add("display", IIf(cmbActi.Valor = Actividades.Laboratorio, "inline", "none"))

        ' Dario 2013-01-23 para que cargue el combo actividad del usuario
        ' se genera la carga de estos elemento que normalmente se cargan por 
        ' javascript con el onchange del combo actividad
        If (cmbActi.Valor = Actividades.Laboratorio) Then
            Me.DivimgCtrlAnalisis.Style.Add("display", "inline")
        Else
            Me.DivimgCtrlAnalisis.Style.Add("display", "none")
        End If

        'sin cargo (RRGG / LAB no lo muestran)
        'divSinCargo.Style.Add("display", IIf(cmbActi.Valor = Actividades.RRGG Or cmbActi.Valor = Actividades.Laboratorio, "none", "inline"))

        If hdnProformaId.Text = "" Then
            'solo si no viene de proforma
            txtFechaValor.Enabled = Not chkTPers.Checked
        End If

        mEstadoSaldos()
        mEspeciales()
        'mSetearTramitePersonal()
        'RRGG
        If mobj.pRRGG Then
            DivAranSobretasa.Style.Add("display", IIf(mobj.pAranRRGGSobretasa <> 0, "inline", "none"))
            DivAranNormal.Style.Add("display", IIf(mobj.pAranRRGGSobretasa <> 0, "none", "inline"))
            mSetearRRGG("0")
        End If
        '  If hdnUbic.Text = "P" Then
        mArancel()
        'End If

        If hdnLoginPop.Text <> "" Then
            lblAutoUsuaTit.Text = clsSQLServer.gObtenerValorCampo(mstrConn, "usuarios", hdnLoginPop.Text, "usua_user")
        Else
            lblAutoUsuaTit.Text = ""
        End If
    End Sub

    Private Sub mSetearClie()
        If usrClie.Valor Is DBNull.Value Then
            divEstadoSaldos.Style.Add("display", "none")
            divProformas.Style.Add("display", "none")
        ElseIf hdnAntClieId.Text <> usrClie.Valor.ToString Then
            If clsSQLServer.gObtenerValorCampo(mstrConn, "emisores_ctros", SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request), "emct_datos_fact") Then
                Dim lDs As DataSet = clsSQLServer.gExecuteQuery(mstrConn, "clientes_estado_de_saldos_consul " + usrClie.Valor.ToString)
                With lDs.Tables(0).Rows(0)
                    'txtSaldoCaCteVenc.Text = .Item("saldo_cta_cte_venc")
                    '     txtSaldoCtaSociVenc.Text = .Item("saldo_social_venc")
                    txtSaldoCaCte.Text = .Item("saldo_cta_cte")
                    txtSaldoCtaSoci.Text = .Item("saldo_social")
                    txtImpCuentaCtaCte.Text = .Item("imp_a_cta_cta_cte")
                    txtImpCuentaCtaSocial.Text = .Item("imp_a_cta_cta_social")
                    txtTotalProfRRGG.Text = .Item("TotalProformaRRGG")
                    txtTotalProfLabo.Text = .Item("TotalProformaLabo")
                    txtTotalProfExpo.Text = .Item("TotalProformaExpo")
                    txtTotalProfOtras.Text = .Item("TotalProformaOtras")

                    If .Item("saldo_cta_cte_venc") <> 0 Or .Item("saldo_social_venc") <> 0 Or .Item("imp_a_cta_cta_cte") <> 0 Or .Item("imp_a_cta_cta_social") <> 0 Then
                        divEstadoSaldos.Style.Add("display", "inline")
                    Else
                        divEstadoSaldos.Style.Add("display", "none")
                    End If

                    If .Item("saldo_cta_cte_venc") <> 0 Or .Item("saldo_social_venc") <> 0 Or .Item("imp_a_cta_cta_cte") <> 0 Or .Item("imp_a_cta_cta_social") <> 0 Then
                        divProformas.Style.Add("display", "inline")
                    Else
                        divProformas.Style.Add("display", "none")
                    End If
                End With
            End If
        End If
        hdnAntClieId.Text = usrClie.Valor.ToString
    End Sub

    Private Sub mEstadoDeSaldos()
        mobj.EstadoSaldos(mstrConn, usrClie.Valor, txtFechaValor.Fecha)
    End Sub
    Private Sub mEstadoSaldos()
        DivimgAlertaSaldosAmarillo.Style.Add("display", "none")
        DivimgAlertaSaldosVerde.Style.Add("display", "none")
        DivimgAlertaSaldosRojo.Style.Add("display", "none")


        Select Case mobj.pEstadoSaldos
            Case "A"
                DivimgAlertaSaldosAmarillo.Style.Add("display", "inline")
            Case "V"
                DivimgAlertaSaldosVerde.Style.Add("display", "inline")
            Case "R"
                DivimgAlertaSaldosRojo.Style.Add("display", "inline")
        End Select

    End Sub
    Private Sub mEspeciales()
        DivimgEspecVerde.Style.Add("display", "none")
        DivimgEspecRojo.Style.Add("display", "none")
        Select Case mobj.pEspeciales
            Case "V"
                DivimgEspecVerde.Style.Add("display", "inline")
            Case "R"
                DivimgEspecRojo.Style.Add("display", "inline")
        End Select
    End Sub
    Private Sub mProformaMensaje(ByRef pstrMensaje As String)
        Dim lstrMensaje As String
        mobj.pProformaAuto = False
        mEstadoDeSaldos()
        'a)el cliente no opera con cta cte (necesita autorizacion para generar la factura) 
        'b)opera en cta cte pero esta excedido (proforma automaticamente-no necesita autorizacion)
        If (mobj.pEstadoSaldosTodo(CamposEstado.lim_saldo) = "") Then
            lstrMensaje = "El cliente no tiene cuenta corriente."
            mobj.pProformaAuto = True
        End If
        If mobj.pEstadoSaldosTodo(CamposEstado.Saldo_excedido) <> "" Then
            lstrMensaje = lstrMensaje + "El cliente tiene excedido el saldo. "
            mobj.pProformaAuto = True
        End If
        pstrMensaje = lstrMensaje

    End Sub


#End Region

#Region "Funciones-Procedimientos varios"


    Private Sub mInicializarVariablesCalculo()
        hdnActivAnterior.Text = cmbActi.Valor
        hdnFValorAnterior.Text = txtFechaValor.Text
        hdnLPrecioAnterior.Text = cmbListaPrecios.Valor
        hdnFPagoAnterior.Text = IIf(cmbFormaPago.Valor Is DBNull.Value, "", cmbFormaPago.Valor)
        hdnCotiAnterior.Text = txtCotDolar.Valor
    End Sub
    Private Sub mAbrirVentana(ByVal pstrcompId As String, ByVal pTipoComprob As String, ByVal pstrCompGenera As String, ByVal pLetraComprob As String, ByVal pstrProf As String)
        Try
            If pstrcompId <> "" Then
                Dim ds As New DataSet
                Dim lstrComprobante As String
                Dim lsbPagina As New System.Text.StringBuilder
                Dim lstrfiltro As String = pstrcompId
                If pstrProf <> "" Then lstrfiltro = lstrfiltro + "," + clsSQLServer.gFormatArg(pstrProf, SqlDbType.VarChar)
                ds = clsSQLServer.gExecuteQuery(mstrConn, "exec  NroTramite_busq " + lstrfiltro)
                If ds.Tables(0).Rows.Count = 0 Then Exit Sub

                If pTipoComprob <> "Factura" Then pLetraComprob = ""
                lstrComprobante = pTipoComprob + " " + pLetraComprob + " " + pstrCompGenera
                lsbPagina.Append("NroTramite_pop.aspx?titulo=N�meros de tr�mite" & "&compId=" + pstrcompId & "&titu=" + lstrComprobante & "&prof=" + pstrProf)
                clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 600, 250, 100, 100)
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Function mCerrarProforma(ByVal pstrcompId As String) As String
        Try
            Dim lstr As String
            If pstrcompId <> "" Then
                Dim ds As New DataSet
                Dim lstrComprobante As String
                Dim lsbPagina As New System.Text.StringBuilder
                Dim lstrfiltro As String = pstrcompId

                ds = clsSQLServer.gExecuteQuery(mstrConn, "exec  proformas_cerradas_consul " + lstrfiltro)
                If ds.Tables(0).Rows.Count = 0 Then
                    lstr = ""
                Else
                    hdnProfCerrarId.Text = ds.Tables(0).Rows(0).Item("id").ToString
                    lstr = ds.Tables(0).Rows(0).Item("detalle").ToString
                End If
            End If
            Return lstr
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Function

    Private Sub mArancel()
            If cmbAran.Valor.ToString.Length > 0 Then
                mSetearDatosArancel("0")
                mCalcularPrecioUnitArancel()
                mCalcularImporteAranceles()
                mArancelExento()

            End If
    End Sub
    Private Sub mArancelExento()
        Dim pstrValor As String = mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_exen", cmbAran.Valor)
        If pstrValor = "" Or pstrValor = "0" Then
            chkExentoAran.Checked = False
            chkSCargoAran.Enabled = True
        Else
            chkExentoAran.Checked = True
            chkSCargoAran.Enabled = False
        End If
    End Sub
    Private Sub mValidarFechaRefeRRGG()
        If txtFechaAranRRGG.Text <> "" Then
            If txtFechaAranRRGG.Fecha > txtFechaValor.Fecha Then
                Throw New AccesoBD.clsErrNeg("La Fecha no puede ser mayor a la fecha valor.")
            End If
        End If
    End Sub
    Private Sub mControlesMostrarOcultar()
        If mobj.pLabora Then                  'actividad - laboratorio
            'oculto columnas
            mOcultarColumnasyControles(True, True)
        Else
            If mobj.pRRGG Then                  'actividad - RRGG
                'muestro columnas
                mOcultarColumnasyControles(False)
            Else
                If mobj.pAlum Then               'actividad - alumnos
                    'oculto columnas
                    mOcultarColumnasyControles(True)
                Else 'actividad - otras
                    'ocultar columnas
                    mOcultarColumnasyControles(True)
                End If
            End If

        End If
    End Sub
    Private Sub mDiv()
        '   panClieGene.Visible = usrClie.ClienteGenerico
        '   panTarjeta.Visible = IIf(cmbFormaPago.Valor Is DBNull.Value, 0, cmbFormaPago.Valor) = mintTarj
    End Sub
    Private Sub mPasarSalopaCab(ByVal pstrPos As String)
        hdnUbic.Text = pstrPos
        hdnActiId.Text = cmbActi.Valor
        hdnCotDolar.Text = txtCotDolar.Valor
        hdnFormaPago.Text = cmbFormaPago.Valor
        hdnConv.Text = cmbConv.Valor
        hdnClieDiscIVA.Text = usrClie.DiscrimIVA
        hdnClieId.Text = usrClie.Valor

    End Sub
    Private Sub mCliente()
        txtSocio.Valor = ""
            txtSocio.Valor = IIf(usrClie.SociNro Is DBNull.Value, "", usrClie.SociNro)
            mEstadoSaldos()
        mEspeciales()
        mCargarConvenios()
    End Sub
    Private Sub mOcultarColumnasyControles(ByVal pboolOcultar As Boolean, Optional ByVal pboolLab As Boolean = False)
        panDatosRRGG.Visible = False
        panDatosTurnos.Visible = False
        'solo de RRGG y Lab (no se usa)
        'lblCantSCAran.Visible = False
        'txtCantSCAran.Visible = False
        'solo RRGGG
        lblCantNoFacAranRRGG.Visible = False
        txtCantNoFacAranRRGG.Visible = False

        With grdDetalle
            If mobj.pRRGG Then
                .Columns(Columnas.desc_aran_concep).HeaderText = "Tipo/Cod./Descripci�n"
            Else
                .Columns(Columnas.desc_aran_concep).HeaderText = "Descripci�n"
            End If
            .Columns(Columnas.cant_sin_cargo).Visible = False
            .Columns(Columnas.anim_no_fact).Visible = Not pboolOcultar
            .Columns(Columnas.fecha).Visible = Not pboolOcultar
            .Columns(Columnas.inic_rp).Visible = Not pboolOcultar
            .Columns(Columnas.fina_rp).Visible = Not pboolOcultar
            .Columns(Columnas.tram).Visible = Not pboolOcultar
            .Columns(Columnas.acta).Visible = Not pboolOcultar

            .Columns(Columnas.tipo).Visible = pboolOcultar
            .Columns(Columnas.codi_aran_concep).Visible = pboolOcultar

            'laboratorio
            '.Columns(Columnas.cant_sin_cargo).Visible = pboolLab
            'lblCantSCAran.Visible = pboolLab
            'txtCantSCAran.Visible = pboolLab

            '---------
        End With
        If (pboolOcultar = False And pboolLab = False) Then   'RRGG
            panDatosRRGG.Visible = True
            ' lblCantSCAran.Visible = True
            ' txtCantSCAran.Visible = True
            lblCantNoFacAranRRGG.Visible = True
            txtCantNoFacAranRRGG.Visible = True
        End If
        If pboolLab Then     'LAB
            panDatosTurnos.Visible = True
        End If
    End Sub
    Private Sub mCalcularSaldoCuotas()
        lblImpoSaldo.Text = mobj.CalcularSaldoCuotas(mdsDatos, lblImpTotal.Text)
    End Sub
    Private Sub mCargarConvenios()
        Dim lstrFiltro As String
        If Not (cmbActi.Valor Is DBNull.Value Or cmbActi.Valor = 0) Then
            lstrFiltro = "@conv_clie_id=" & usrClie.Valor & ", @conv_acti_id=" & cmbActi.Valor & ", @conv_fecha=" & clsSQLServer.gFormatArg(txtFechaValor.Text, SqlDbType.SmallDateTime)
            clsWeb.gCargarRefeCmb(mstrConn, "convenios", cmbConv, "N", lstrFiltro)
        End If
    End Sub
    Private Sub mTituloCabecera()
        lblCabeCliente.Text = usrClie.txtApelExt.Text
        lblCabeActividad.Text = cmbActi.SelectedItem.Text
        lblCabeTPago.Text = cmbFormaPago.SelectedItem.Text
    End Sub
    Private Sub mValidarNavega()
        If cmbActi.Valor Is DBNull.Value Or cmbActi.Valor = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar la actividad.")
        End If
        If cmbFormaPago.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el tipo de pago.")
        End If
        If usrClie.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe seleccionar el cliente.")
        End If
        If usrClie.ClienteGenerico And cmbFormaPago.Valor <> mintContado Then
            Throw New AccesoBD.clsErrNeg("El Cliente es gen�rico,  solo puede facturar al contado.")
        End If
        If txtFechaIva.Fecha Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de referencia de I.V.A.")
        End If
        If txtFechaValor.Fecha Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha valor")
        End If
        ' Dario 2013-09-11 Control de actividad con raza obligatoria
        'If (cmbActi.Valor = Actividades.Laboratorio And _
        '    ((cmbRaza.Valor Is DBNull.Value Or cmbRaza.Valor = "") _
        '    And (txtCriaNume.Text.Trim.Length = 0))) Then
        '    Throw New AccesoBD.clsErrNeg("Debe seleccionar el nro. de criador o raza")
        'End If

    End Sub
    Private Sub mValidarCuotas()
        If txtCuotFecha.Fecha Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha.")
        End If

        If txtCuotImporte.Valor Is DBNull.Value And txtCuotPorc.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar el importe o porcentaje de la cuota.")
        End If

        ' toma el importe porque no ingreso el %
        If txtCuotPorc.Valor Is DBNull.Value Then
            If CType(txtCuotImporte.Valor, Double) > CType(lblImpoSaldo.Text, Double) Then
                Throw New AccesoBD.clsErrNeg("El importe de la cuota no puede superar al saldo.")
            End If
            If CType(txtCuotImporte.Valor, Double) = 0 Then
                Throw New AccesoBD.clsErrNeg("El importe de la cuota no puede ser cero (0).")
            End If
        End If
            If (txtCuotFecha.Text.Trim().Length > 0) Then
                If txtCuotFecha.Fecha < Date.Today Then
                    Throw New AccesoBD.clsErrNeg("La fecha no puede ser menor a la fecha de ingreso.")
                End If
            End If



        End Sub
    Private Sub mValidarConceptosAutomaticos(ByVal pintauto As Integer)
        If pintauto = 1 Then
            Throw New AccesoBD.clsErrNeg("No pueden modificarse descuentos autom�ticos, solo eliminarlos.")
        End If

    End Sub
    Private Sub mValidarConceptos(ByVal pstrTodo As String)


            If cmbConcep.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el concepto.")
            End If


            If txtImporteConcep.Valor.Length = 0 Then
                mLimpiarImportesConceptos()
                Throw New AccesoBD.clsErrNeg("Debe ingresar el valor de concepto.")
            End If

        If mobj.DistintoIva(mdsDatos, mobj.ObtenerValorCampo(mstrConn, "conceptosX", "tipo_tasa", cmbConcep.Valor.ToString)) Then
            Throw New AccesoBD.clsErrNeg("Se esta facturando con un tipo de I.V.A. distinto al del concepto seleccionado.")
        End If

        If pstrTodo = "T" Then
            ' es obligatorio cargar el centro de costo cuando el concepto

            '10/05/2011 - deberia preguntar si es ganadera, entonces volver a verificar con la cuenta secundaria
            If cmbActi.SelectedValue.ToString = "7" Then
                If mobj.ObtenerValorCampo(mstrConn, "Exposiciones", "expo_desde_fecha", cmbExpo.SelectedValue.ToString) > Today() Then ' Si actividad es Exposicion ganadera
                    If mobj.ObtenerValorCampo(mstrConn, "ConceptosX_GAN", "cta_resu", cmbConcep.Valor) = "1" Then
                            If cmbCCosConcep.Valor.Length = 0 Then
                                Throw New AccesoBD.clsErrNeg("Debe seleccionar el centro de costo (el concepto ingresado se asocia a un a cta. de resultado) ..")
                            End If

                    Else
                            If Not cmbCCosConcep.Valor.Length = 0 Then
                                Throw New AccesoBD.clsErrNeg("NO Debe seleccionar el centro de costo (el concepto ingresado NO se asocia a un a cta. de resultado).")
                            End If
                    End If
                Else
                    If mobj.ObtenerValorCampo(mstrConn, "ConceptosX", "cta_resu", cmbConcep.Valor) = 1 Then
                            ' tiene asociado una cta. contable de resultado
                            If cmbCCosConcep.Valor.Trim().Length = 0 Then
                                Throw New AccesoBD.clsErrNeg("Debe seleccionar el centro de costo (el concepto ingresado se asocia a un a cta. de resultado).")
                            End If
                        Else
                            If Not cmbCCosConcep.Valor.Trim().Length > 0 Then
                                Throw New AccesoBD.clsErrNeg("NO Debe seleccionar el centro de costo (el concepto ingresado NO se asocia a un a cta. de resultado).")
                            End If
                        End If
                End If
            Else
                If mobj.ObtenerValorCampo(mstrConn, "ConceptosX", "cta_resu", cmbConcep.Valor) = 1 Then
                    ' tiene asociado una cta. contable de resultado
                        If cmbCCosConcep.Valor.Length = 0 Then
                            Throw New AccesoBD.clsErrNeg("Debe seleccionar el centro de costo (el concepto ingresado se asocia a un a cta. de resultado) ...")
                        End If
                Else
                        If cmbCCosConcep.Valor.Trim().Length > 0 Then
                            Throw New AccesoBD.clsErrNeg("NO Debe seleccionar el centro de costo (el concepto ingresado NO se asocia a un a cta. de resultado).")
                        End If
                    End If
            End If

                If txtImporteConcep.Valor.Length = 0 Then
                    Throw New AccesoBD.clsErrNeg("No puede ingresar un concepto sin valor.")
                End If

        End If
    End Sub
    Private Sub mValidarAranceles(Optional ByVal pboolTodo As Boolean = True)
        If pboolTodo Then
            If txtCantAran.Valor Is DBNull.Value OrElse txtCantAran.Valor = 0 Then
                mLimpiarImportesAranceles(Not pboolTodo)

                Throw New AccesoBD.clsErrNeg("Debe ingresar la cantidad del arancel.")
            End If
            ' turnos 
            If mobj.pLabora And (hdnDatosPopTurnoBov.Text <> "" Or Me.hdnDatosPopTurnoEqu.Text <> "") Then

                Dim lintId As Integer
                Dim lstrTipo As String
                If (hdnDatosPopTurnoBov.Text <> "") Then
                    lintId = hdnDatosPopTurnoBov.Text
                    lstrTipo = "B"
                Else
                    lintId = hdnDatosPopTurnoEqu.Text
                    lstrTipo = "E"
                End If
                If mobj.TurnosCantFacturados(mdsDatos, txtCantAran.Valor, hdnDetaTempId.Text, lintId, lstrTipo) < 0 Then
                    Throw New AccesoBD.clsErrNeg("La cantidad supera los turnos pendientes a facturar.")
                End If
            End If

            If mobj.pLabora Then
                'desc. promocion/desc. cantidad
                If txtTurnosDC.Valor >= 1 Or Me.txtTurnosDP.Valor >= 1 Then
                    Throw New AccesoBD.clsErrNeg("Verifique los descuentos de turnos, deben ser menor a 0.")
                End If
                'recargo turno/recargo tram. urg./sobretasa palermo 
                If (txtTurnosRT.Valor > 0 And txtTurnosRT.Valor < 1) _
                      Or (txtTurnosRTU.Valor > 0 And txtTurnosRTU.Valor < 1) _
                       Or (txtTurnosSP.Valor > 0 And txtTurnosSP.Valor < 1) Then
                    Throw New AccesoBD.clsErrNeg("Verifique los recargos de turnos. Deben ser mayores a 1(uno) o igual a 0 (cero).")
                End If
            End If


            If mobj.pRRGG Then
                If mobj.pAranRRGGDescFecha <> "" AndAlso txtFechaAranRRGG.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de referencia")
                End If

                'If mobj.pAranRRGGActa And mobj.pAranRRGGDescFecha <> "" AndAlso txtActaAranRRGG.Valor = "" Then
                'Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de Acta.")
                'End If
                'If txtFechaValor.Text <> "" And txtFechaAranRRGG.Text <> "" Then
                '    'si paso un a�o del nacimiento solicitar nro de acta.
                '    If DateDiff(DateInterval.Day, txtFechaAranRRGG.Fecha, txtFechaValor.Fecha) > mintDiasAnio AndAlso txtActaAranRRGG.Valor = "" Then
                '        txtActaAranRRGG.Enabled = True
                '        txtActaAranRRGG.CssClass = "cuadrotexto"
                '        Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de Acta.")
                '    End If
                'End If

                If mobj.pAranRRGGActa And txtActaAranRRGG.Text = "" Then
                    txtActaAranRRGG.Enabled = True
                    txtActaAranRRGG.CssClass = "cuadrotexto"
                    Throw New AccesoBD.clsErrNeg("Debe ingresar el Nro. de Acta.")

                End If

                If mobj.pAranRRGGError And mobj.pAranRRGGDescFecha <> "" Then
                    Throw New AccesoBD.clsErrNeg("El Arancel no corresponde con la fecha de referencia.")
                End If
                If mobj.pAranRRGGRepresAnim And txtCriaNume.Valor Is DBNull.Value And cmbRaza.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe seleccionar el n�mero de criador o raza")
                End If
            End If
        End If


            If cmbAran.Valor.Length = 0 OrElse cmbAran.Valor = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el arancel.")
            End If

        If mobj.DistintoIva(mdsDatos, mobj.ObtenerValorCampo(mstrConn, "arancelesX", "tipo_tasa", cmbAran.Valor.ToString)) Then
            Throw New AccesoBD.clsErrNeg("Se esta facturando con un tipo de I.V.A. distinto al del arancel seleccionado.")
        End If
            If txtImpoAran.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("El arancel no tiene valor.")
            End If


    End Sub
    Private Sub mValidaDatosRRGG_Avisos()
        If mobj.pRRGG Then ' solo para RRGG
            Dim lstrAlerta As String

            ' si representa animales
            If txtRPAranDdeRRGG.Enabled Then
                '
                If txtRPAranDdeRRGG.Valor = "" And txtRPAranHtaRRGG.Valor <> "" And txtCantAran.Valor > 1 Then
                    lstrAlerta = "No ingres� el RP.(desde)"
                End If
                '15/11/06 se saco estos avisos
                'If txtRPAranDdeRRGG.Valor <> "" And txtRPAranHtaRRGG.Valor = "" And txtCantAran.Valor > 1 Then
                '   If lstrAlerta = "" Then lstrAlerta = "No ingres� el RP.(hasta)"
                'End If
                'If txtRPAranDdeRRGG.Valor = "" And txtRPAranHtaRRGG.Valor = "" Then
                '   If lstrAlerta = "" Then lstrAlerta = "No ingres� el rango de RP "
                'End If


                'antes se validaban porque eran nros.
                'If lintRPDesde > lintRPHasta Then
                'lstrAlerta = lstrAlerta + "El Nro. RP desde es mayor al Nro. RP hasta. "
                'End If
                'If (CType(txtCantAran.Valor, Integer) + CType(txtCantNoFacAranRRGG.Valor, Integer) + CType(txtCantSCAran.Valor, Integer)) > (lintRPHasta - lintRPDesde + 1) Then
                '   lstrAlerta = lstrAlerta + "La cantidad de productos es mayor a los RP ingresados. "
                'End If
                'If (txtCantAran.Valor + txtCantNoFacAranRRGG.Valor + txtCantSCAran.Valor) < (lintRPHasta - lintRPDesde) Then
                '   lstrAlerta = lstrAlerta + "La cantidad de productos es menor a los RP ingresados. "
                'End If
            End If
            AccesoBD.clsError.gGenerarMensajes(Me, lstrAlerta)
        End If

    End Sub
    Private Sub mCargarCuotaUnica()
        'si no es ctacte, eliminar los datos cargados (en el caso de cambiar el tipo de pago)
        If cmbFormaPago.Valor <> mintCtaCte Then
            mBorrarDatosCuotas()
        End If
        'Si es cta cte y no cargo las cuotas
        'Si el importe de la factura es 0
        'si no es cta cte y el importe es <> 0
        If (mobj.pPagoCtaCte And CType(lblImpoSaldo.Text, Double) <> 0) Or _
           txtTotNeto.Valor = 0 Or _
           (Not mobj.pPagoCtaCte And txtTotNeto.Valor <> 0) Then
            mActualizarCuotas(True)
        End If
        If cmbFormaPago.Valor = mintCtaCte Then ' and Not mobj.pProforma Then
            If CType(lblImpoSaldo.Text, Double) <> 0 Then
                Throw New AccesoBD.clsErrNeg("Verifique el valor de las cuotas, el saldo debe ser 0 (cero).")
            End If
        End If
    End Sub
    Private Sub mValidarDatos()
        ' valida los datos de la primer solapa
        mValidarNavega()
        mValidaSinCtaCte()
        mValidarArancelesRecalculados()


        If mobj.pLabora Then '(si es laboratorio - verifica turnos)
            If mobj.ExistenTurnosSinFacturar(mdsDatos) Then
                Throw New AccesoBD.clsErrNeg("Existen turnos sin facturar.")
            End If
        End If

        If Not mobj.ExistenItemsEnLaFactura(mdsDatos) Then
            Throw New AccesoBD.clsErrNeg("No existen items cargados.")
        End If

        'Si eligio tarjeta debe completar todos los datos 
        If cmbFormaPago.Valor = mintTarj Then
            ' datos de la tarjeta
                If cmbTarj.Valor.Length = 0 OrElse cmbTarj.Valor = 0 Then
                    Throw New AccesoBD.clsErrNeg("Debe seleccionar una tarjeta")
                End If
            '040107  no es obligatorio ingresar el nro de tarjeta
            'If Me.txtTarjNro.Text = "" Then
            'Throw New AccesoBD.clsErrNeg("Debe ingresar el n�mero de la tarjeta")
            'End If
        End If
        ' si el total da cero, no se valida si quedaron autorizaciones sin confirmar
        ' ni validaciones por tipo de pago en cta. cte. 
        ' 09/11/06 cuando es proforma tampoco se valida
        If txtTotNeto.Valor > 0 And mobj.pTipoComprobGenera = TipoComprobGene.Factura Then
            '0-quedan sin conf y no es la 7
            If mobj.ExistenAutorizacionesSinConf(mdsDatos) = 0 Then
                Throw New AccesoBD.clsErrNeg("Existen autorizaciones sin confirmar.")
            End If

            ' forma de pago con  cta cte 
            If mobj.pPagoCtaCte Then 'And Not mobj.pProforma Then
                ' no es RRGG ni lab , no tiene cta .cte. y '2-conf todas menos "no tiene cta. cte." 
                If mobj.pEstadoSaldosTodo(CamposEstado.lim_saldo) = "" And Not mobj.pProforma And mobj.ExistenAutorizacionesSinConf(mdsDatos) = 2 Then
                    Throw New AccesoBD.clsErrNeg("El cliente no tiene cta. cte. Debe autorizar la emisi�n de la factura.")
                End If
                If mobj.pEstadoSaldosTodo(CamposEstado.Saldo_excedido) <> "" And mobj.pTipoComprobGenera = TipoComprobGene.Factura Then
                    Throw New AccesoBD.clsErrNeg("Excede el l�mite de saldo.")
                End If
                'si tiene limite para usar en cta. cte
                'CamposEstado.lim_saldo = 0 ->> significa que no tiene limite, puede operar
                '                       = ""    No puede operar con Cta. cte.
                If (mobj.pEstadoSaldosTodo(CamposEstado.lim_saldo) <> "" AndAlso CType(mobj.pEstadoSaldosTodo(CamposEstado.lim_saldo), Double) > 0) And mobj.pTipoComprobGenera = TipoComprobGene.Factura Then
                    Dim ldouValor As Double = CType(mobj.pEstadoSaldosTodo(CamposEstado.lim_saldo), Double) - (CType(txtTotNeto.Valor, Double) + CType(mobj.pEstadoSaldosTodo(CamposEstado.saldo_cta_cte), Double))
                    If ldouValor < 0 Then
                        Throw New AccesoBD.clsErrNeg("Excede el l�mite de saldo.")
                    End If
                End If
            End If
        End If
        If mobj.pRRGG Then
            If mobj.ExistenArancRRGGRepresAnim(mstrConn, mdsDatos) And txtCriaNume.Valor Is DBNull.Value And cmbRaza.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar un n�mero de criador o raza. Existen aranceles que representan animales.")

            End If
        End If

        If cmbActi.Valor = Actividades.Expo Then
            If cmbExpo.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar la exposici�n.")
            End If
        End If

    End Sub
    Private Sub mValidaSinCtaCte()
        mobj.PagoCtaCte(cmbFormaPago.Valor)
        mobj.Proforma()
        If cmbFormaPago.Valor = mintCtaCte And mobj.pProforma = False Then
            If mobj.pEstadoSaldosTodo(CamposEstado.lim_saldo) = "" And Not mobj.pProforma And (mobj.ExistenAutorizacionesSinConf(mdsDatos) = 2 Or mobj.ExistenAutorizacionesSinConf(mdsDatos) = 4) Then
                AccesoBD.clsError.gGenerarMensajes(Me, "El cliente no tiene cta. cte. solo podr� facturar si es autorizado.")
            End If
            If mobj.pEstadoSaldosTodo(CamposEstado.Saldo_excedido) <> "" And mobj.pTipoComprobGenera = TipoComprobGene.Factura Then
                Throw New AccesoBD.clsErrNeg("Excede el l�mite de saldo. Debe seleccionar otra forma de pago.")
            End If
            If (mobj.pEstadoSaldosTodo(CamposEstado.lim_saldo) <> "" AndAlso CType(mobj.pEstadoSaldosTodo(CamposEstado.lim_saldo), Double) > 0) And mobj.pTipoComprobGenera = TipoComprobGene.Factura Then
                Dim ldouValor As Double = CType(mobj.pEstadoSaldosTodo(CamposEstado.lim_saldo), Double) - (CType(txtTotNeto.Valor, Double) + CType(mobj.pEstadoSaldosTodo(CamposEstado.saldo_cta_cte), Double))
                If ldouValor < 0 Then
                    Throw New AccesoBD.clsErrNeg("Excede el l�mite de saldo. Debe seleccionar otra forma de pago.")
                End If
            End If
        End If

    End Sub
    Private Sub mControlesIVA()
        If usrClie.DiscrimIVA Then
            panIVAConcep.Visible = True
            panIVAAran.Visible = True
        Else
            panIVAConcep.Visible = False
            panIVAAran.Visible = False
        End If

    End Sub

    Private Function mValor(ByVal pValor As String) As Integer
        If pValor.Trim() = "" Then
            Return 0
        Else
            Return Convert.ToInt32(pValor)
        End If
    End Function

    Private Sub mSetearRRGG(ByVal pstrSobreAuto As String)
        Dim lboolActi As Boolean = True
        If mobj.pRRGG Then
                If Not cmbAran.Valor.Length = 0 Then mobj.AranRRGG(mstrConn, cmbAran.Valor, pstrSobreAuto)
            'no habilita nro RP si el arancel no representa animales
            ' reemplazado clsWeb.gActivarControl(txtRPAranDdeRRGG, mobj.pAranRRGGRepresAnim)
            clsWeb.gActivarControl(txtRPAranDdeRRGG, True)
            clsWeb.gActivarControl(txtRPAranHtaRRGG, txtRPAranDdeRRGG.Enabled)

            If mobj.pAranRRGGDescFecha = "" Then lboolActi = False
            'pueden cargarla siempre, aunque no deba cargar sobretasa 
            ' clsWeb.gActivarControl(txtFechaAranRRGG, lboolActi)
            'clsWeb.gActivarControl(txtActaAranRRGG, (lboolActi Or mValor(txtActaAranRRGG.Text) <> 0))
            '25/08/2010
            clsWeb.gActivarControl(txtActaAranRRGG, (lboolActi Or txtActaAranRRGG.Text <> ""))
            lblFechaAranRRGG.Text = IIf(mobj.pAranRRGGDescFecha = "", "Fecha:", mobj.pAranRRGGDescFecha)
            DivAranNormal.Style.Add("display", "inline")
            DivAranSobretasa.Style.Add("display", "none")
            If mobj.pAranRRGGSobretasa Then
                ' es sobretasa solo puede eliminarse
                DivAranNormal.Style.Add("display", "none")
                DivAranSobretasa.Style.Add("display", "inline")
            End If

        End If

    End Sub
    Private Sub mSetearDatosArancel(ByVal pstrSobreAuto As String)
        mLimpiarTextRelaAran(False)

        mSetearRRGG(pstrSobreAuto)
    End Sub
    Private Sub mHabilitarCobranza(ByVal pbooAlta As Boolean)
        Dim ldouImp As Double
        btnAplicCred.Enabled = False
        btnCobra.Enabled = False
        If pbooAlta And mobj.pTipoComprobGenera = TipoComprobGene.Factura Then
            mobj.EstadoSaldos(mstrConn, usrClie.Valor, txtFechaValor.Fecha)
            If mobj.pEstadoSaldosTodo.Item(CamposEstado.imp_a_cta_cta_cte) = "" Then
                ldouImp = 0
            Else
                ldouImp = CType(mobj.pEstadoSaldosTodo.Item(CamposEstado.imp_a_cta_cta_cte), Double)
            End If

            If ldouImp >= CType(txtTotNeto.Valor, Double) Then
                btnAplicCred.Enabled = True
            End If
            If cmbFormaPago.Valor = mintTarj Or cmbFormaPago.Valor = mintContado Or cmbFormaPago.Valor = mintDif Then
                btnCobra.Enabled = True
            End If
        End If
    End Sub
    Private Sub mBloquearControlesCabe(ByVal pboolBloq As Boolean)
        cmbActi.Enabled = Not pboolBloq
        ' cmbFormaPago.Enabled = Not pboolBloq
        cmbConv.Enabled = Not pboolBloq
        txtFechaIva.Enabled = Not pboolBloq
        usrClie.Activo = Not pboolBloq
        txtSocio.Enabled = Not pboolBloq

        'no bloquearlo
        'txtFechaValor.Enabled = Not pboolBloq
        'chkTPers.Enabled = Not pboolBloq
        '  txtCotDolar.Enabled = Not pboolBloq
    End Sub
    Private Function mValorTasaIva(ByVal pstrArancel As String) As Double
        Return mobj.ObtenerValorCampo(mstrConn, "arancelesX", "valor_tasa", pstrArancel + "," + clsFormatear.gFormatFecha2DB(txtFechaIva.Fecha))
    End Function
    Private Function mValorTasaIva(ByVal pstrConcepId As String, ByVal pstrFechaIva As Date) As Double
        Return mobj.ObtenerValorCampo(mstrConn, "conceptosX", "valor_tasa", pstrConcepId + "," + clsFormatear.gFormatFecha2DB(pstrFechaIva))
    End Function
    Private Function mValorArancelAplicFormula(ByVal pintAranId As Integer, ByVal pdoubImporte As Double, ByVal pintCant As Integer) As Double
        Return mobj.AplicarFormula(mstrConn, pintAranId, pdoubImporte, pintCant)
    End Function
    Private Sub mCalcularPrecioUnitArancel()

        Dim ldouImporte, ldouTasaIVA As Double
        'If txtCantAran.Valor = 0 Then Exit Sub
        Dim lstrVacio As String = ""

        Dim lstrFiltro As String
        ' If hdnProformaId.Text = "" Then ' no es proforma
        ' lstrFiltro = cmbAran.Valor.ToString() + ";" + txtCantAran.Valor.ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + txtFechaValor.Fecha.ToString() + ";" + lstrVacio
            ' Else
            Dim sAran As String

            sAran = cmbAran.Valor.ToString()

            If cmbAran.Valor.ToString().Length = 0 Then
                sAran = "0"
            End If
            lstrFiltro = sAran + ";" + txtCantAran.Valor.ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + cmbListaPrecios.Valor.ToString() + ";" + lstrVacio

            'Si forma de pago es vacio (07/04/2011)
            If hdnFormaPago.Text = "" Then hdnFormaPago.Text = cmbFormaPago.Valor

            'End If
            Dim vsRet As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
            ' vsRet[0]; //PRECIO UNITARIO
            ' vsRet[5];//IMPORTE NETO (PRECIO UNITARIO CON IVA)
            'guarda el importe sin iva  (indistintamente de la condicion IVA del cliente)
            txtPUnitAranSIVA.Valor = vsRet(0)
            'muestra el precio unitario con o sin IVA segun la cond. del cliente 
            If usrClie.DiscrimIVA Then
                'vsRet(0) = iff(vsRet(0).Length > 0, vsRet(0), 0)
                txtPUnitAran.Valor = Format(CType(vsRet(0), Double), "###0.00")
            Else
                'vsRet(5) = IIf(vsRet(5).Length > 0, vsRet(5), 0)
                txtPUnitAran.Valor = Format(CType(vsRet(5), Double), "###0.00")
            End If

        End Sub
    Private Sub mCalcularImporteAranceles()
        'idem mCalcularImporteAranceles() en javascript
        If Not txtCantAran.Valor Is DBNull.Value Then
            Dim lstrVacio As String = ""
            Dim lstrFiltro As String
            'If hdnProformaId.Text = "" Then ' no es proforma
            'lstrFiltro = cmbAran.Valor.ToString() + ";" + txtCantAran.Valor.ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + txtFechaValor.Fecha.ToString() + ";" + lstrVacio
            'Else
            lstrFiltro = cmbAran.Valor.ToString() + ";" + txtCantAran.Valor.ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + cmbListaPrecios.Valor.ToString() + ";" + lstrVacio
            'End If
            Dim vsRet As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
            ' vsRet[0]; //PRECIO UNITARIO
            ' vsRet[1]; //IMPORTE APLICADA LA FORMULA 
            ' vsRet[2];//TASA IVA
            ' vsRet[3];//IMPORTE IVA
            ' vsRet[4];//IMPORTE NETO 
            ' vsRet[5];//IMPORTE P. UNIT C/IVA
            hdlImpoIvaAran.Text = vsRet(3)
            hdnTasaIVAAran.Text = vsRet(2)

            If usrClie.DiscrimIVA Then
                'tasa iva
                txtTasaIVAAran.Valor = Format(CType(vsRet(2), Double), "###0.00")
                'importe iva
                txtImpoIvaAran.Valor = Format(CType(vsRet(3), Double), "###0.00")
                ' valor bruto
                txtImpoAran.Valor = Format(CType(vsRet(1), Double), "###0.00")
            Else
                ' valor neto
                txtImpoAran.Valor = Format(CType(vsRet(4), Double), "###0.00")
            End If

        End If
    End Sub
    Private Sub mCalcularImporteConceptos(ByVal pstrOrigen As String)
        'idem mCalcularImporteConceptos() de javascript
        Dim ldouTasaIVA, ldouImporteiva As Double
        mValidarConceptos(pstrOrigen)

        If Not txtImporteConcep.Valor Is DBNull.Value Then

            Dim lstrFiltro As String = cmbConcep.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + txtImporteConcep.Valor
            Dim vsRet As String() = SRA_Neg.Utiles.CalculoConcepto(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
            ' vsRet[0];//TASA IVA   vsRet[1];//IMPORTE DEL IVA  'vsRet[2];//IMPORTE DEL CONCEPTO SIN IVA 'vsRet[3];//IMPORTE DEL CONCEPTO con IVA
            hdnTasaIVAConcep.Text = vsRet(0)
            hdnImpoIvaConcep.Text = vsRet(1)

            If usrClie.DiscrimIVA Then
                txtTasaIVAConcep.Valor = Format(CType(vsRet(0), Double), "###0.00")
                'txtImpoIvaConcep.Valor = Format(CType(vsRet(1), Double), "###0.00")
                'el importe ingresado por el usuario es el neto (incluye iva)

                '  txtImpoNConcep.Valor = Format(CType(vsRet(3), Double), "###0.00")
            End If
        End If


    End Sub
    Private Sub mCalcularTotal()
        ' ver de poner aca el calculo de las nc automaticas para mostrar en la simulacion
        ' para laboratorio
        mobj.CalcularTotales(mdsDatos, usrClie.DiscrimIVA, usrClie.SobretasaIVA)
        txtTotBruto.Valor = mobj.pTotalBrutoGral
        'ocultos
        txtTotIVA.Valor = mobj.pTotalIVAGral
        txtTotIVARedu.Valor = mobj.pTotalReduGral
        txtTotIVASTasa.Valor = mobj.pTotalSobreTGral
        '--
        txtTotNeto.Valor = mobj.pTotalNetoGral

        txtTotalIVA.Text = mobj.pTotalesIVA
        txtTotalST.Text = txtTotIVASTasa.Valor

        lblImpTotal.Text = txtTotNeto.Valor
        lblCabeTotalBruto.Text = txtTotBruto.Valor
        lblCabeTotalIVA.Text = txtTotalIVA.Valor
        lblCabeTotalSobre.Text = txtTotIVASTasa.Valor
        lblCabeTotalNeto.Text = txtTotNeto.Valor
        mCalcularSaldoCuotas()
        mCargarAutorizaciones()
        ' Dario 2013-10-23 fucion que determina si muestra el panel de 
        ' simulacion de nota de credito automatica
        mMuestraPalenNCAuto()
    End Sub
    ' Dario 2013-10-23 metodo que calucla el importe de la nota de credito que se generaria autometicamente
    ' si corresponde por bonificacion de laboratotio
    Private Sub mMuestraPalenNCAuto()

        ' ocultar panel de simulacion de NC
        Me.trSimulacionNCAuto.Style.Add("display", "none")
        Me.lblMensajeNC.Text = ""
        Me.hdnGeneraNCAuto.Text = "No"
        Me.hdnImporteNCAuto.Text = "0"
        Me.hdnImporteDescuentoAplicadoNCAuto.Text = "0"
        Me.hdnClieId.Text = usrClie.Valor
        Me.lblTotDescAplic.Text = ""
        Me.lblTotDescAplicMonto.Text = ""

        ' variable que se usa para determinar si se muestra o no, la simulacion de la NC
        Dim boolMostrarPanelNC As Boolean = False
        ' variable que me determina la cantidad a bonificar
        Dim cantidadaBonificar As Integer = 0
        ' valriables para el calculo del importe unitario
        Dim importe As Decimal = 0
        Try
            ' buscar la cantidad para aplicar el descuento y el % del mismo por la raza
            ' crea el objeto para contener el resultado de la busqueda de parametros
            Dim objParametosBonifLab As New Entities.Facturacion.ParametosBonifLabEntity
            ' crea el objeto para obtener el acumulado para esa especie para el cliente
            ' para ver si corresponde generar descuento automatico
            Dim objResultPanelLabBonif As New Entities.Facturacion.ResultPanelLabBonif
            '
            If (Not Me.cmbRaza.Valor Is DBNull.Value And Convert.ToInt16(cmbActi.Valor) = Actividades.Laboratorio) Then
                ' crea Objeto para obtener el dato de facturacion
                Dim _FacturacionBusiness As New Business.Facturacion.FacturacionBusiness
                    ' ejecuto la conslta que obtiene la parametria por especie de la raza pasada por parametro
                    If (Me.cmbRaza.Valor.Trim().ToString().Length > 0) Then
                        objParametosBonifLab = _FacturacionBusiness.GetParametosBonifLaboratorio(Me.cmbRaza.Valor)
                    End If

                    ' ejecuto el metodo que recupera el acumulado actual para el cliente por
                    ' actividad especie y fecha valor
                    objResultPanelLabBonif = _FacturacionBusiness.GetDatosLaboratorioAcumEspecie(Convert.ToInt32(cmbActi.Valor) _
                                                                                           , Convert.ToInt32(Me.hdnClieId.Text) _
                                                                                           , objParametosBonifLab.intIdEspecie _
                                                                                           , Convert.ToDateTime(Me.txtFechaValor.Fecha) _
                                                                                           , mdsDatos)

                        ' si resultado de la consulta de acumulado por cliente  tiene resultado para ese arencel actividad
                        ' y la cantidad desde la cual se bonivica es > 0 ahi calculo 
                        If (objResultPanelLabBonif.TituAcum.Length > 0 And objParametosBonifLab.intCantABonificar2 > 0) Then
                            ' si la cantidad es >= a la cantidad a bonificar ya se realizo la NC no hago nada y no muestro nada
                            If (objResultPanelLabBonif.Cantidad >= objParametosBonifLab.intCantABonificar2) Then
                                boolMostrarPanelNC = False
                            Else
                                '  controlo si la cantidad acumulada mas la de las factura es mayor que la cantidad desde la cual se bonifica
                                If ((objResultPanelLabBonif.Cantidad + objResultPanelLabBonif.CantidadFactura) >= objParametosBonifLab.intCantABonificar2) Then
                                    cantidadaBonificar = objParametosBonifLab.intCantABonificar2 - objResultPanelLabBonif.Cantidad
                                    importe = (objResultPanelLabBonif.Importe / objResultPanelLabBonif.CantidadFactura) * cantidadaBonificar
                                    boolMostrarPanelNC = True
                                End If
                            End If
                        End If
                    End If

                    If (boolMostrarPanelNC) Then
                ' mostrar panel de simulacion de NC
                ' crea Objeto para obtener el dato de facturacion
                Dim _LaboratorioBusiness As New Business.Laboratorio.LaboratorioBusiness
                ' ejecuto el metodo que recupera los datos a mostar en la grilla y en el encabezado
                Dim ds As DataSet = _LaboratorioBusiness.GetAllAnalisisAcumPorActividadCliente(Common.Actividades.Laboratorio _
                                                                                              , Convert.ToInt32(Me.hdnClieId.Text) _
                                                                                              , System.DateTime.Now _
                                                                                              )
                ' declato las variable que usaremos para hacer los calculos de los importes
                Dim importeAcum As Decimal = 0
                Dim importeDescAcum As Decimal = 0
                Dim decImporteCN As Decimal = 0
                Dim totalCN As Decimal = 0
                ' busco en el ds de respuesta de acumulados por cliente la especie que correspode a esta factra
                For Each ldrDatos As DataRow In ds.Tables(1).Select("IdEspecie= " + objParametosBonifLab.intIdEspecie.ToString)
                    With ldrDatos
                        ' tomo los importes totales de todos los analisis realizados y el descuento efectuado
                        importeAcum = importeAcum + .Item("Importe")
                        importeDescAcum = importeDescAcum + .Item("ImportConcep")
                    End With
                Next
                decImporteCN = ((importeAcum + importe) * objParametosBonifLab.decPorcBonifPorCantidad2)
                totalCN = decImporteCN - (importeDescAcum * -1)
                ' muestro el tr de los datos de la NC automatica
                Me.trSimulacionNCAuto.Style.Add("display", "inline")
                ' controlo si el valor es positivo negativo o igual para setear el mensaje correspondiente
                If (totalCN > 0) Then
                    Me.lblMensajeNC.Text = "Se generara Nota de Credito por los An�lisis(acum:" + objResultPanelLabBonif.Cantidad.ToString _
                                         + "- FacAct:" + cantidadaBonificar.ToString + ") " _
                                         + ". Monto sin descuento: $" + (importeAcum + importe).ToString("#,##0.00") _
                                         + ". Descuentos ya aplicados: $" + importeDescAcum.ToString("#,##0.00") _
                                         + ". Total Nota de Cretido: $" + totalCN.ToString("#,##0.00")
                    Me.hdnGeneraNCAuto.Text = "Si"
                    Me.hdnImporteNCAuto.Text = decImporteCN.ToString
                    Me.hdnImporteDescuentoAplicadoNCAuto.Text = importeDescAcum.ToString
                    Me.lblTotDescAplic.Text = "Total a Pagar: $"
                    Me.lblTotDescAplicMonto.Text = (Convert.ToDecimal(txtTotNeto.Valor) - totalCN).ToString("#,##0.00")
                ElseIf (totalCN = 0) Then
                    ' el calculo de descuento de los acumulados y los descuentos ya realizados son =
                    ' no se genera nota de credito ya que seria en 0
                    Me.lblMensajeNC.Text = "Los montos de la NC a generar y los descuentos ya realizados se cancelan, NO SE GENERA NC AUTOMATICA"
                ElseIf (totalCN < 0) Then
                    ' el calculo de descuento de los acumulados es menor que los descuentos ya realizados
                    ' no se genera nota de credito ya que seria en negativo
                    Me.lblMensajeNC.Text = "Los montos de la NC a generar es menor a los descuentos ya realizados, NO SE GENERA NC AUTOMATICA"
                End If
        End If
        Catch ex As Exception
            Dim msg As String
            msg = "Error Facturacion.vb.mMuestraPalenNCAuto()"
            clsError.gManejarError(msg, "", ex)
        End Try
    End Sub

    Private Sub mCotizacionDolar()
        mobj.CotizaDolar(mstrConn, txtFechaValor.Fecha)
        If hdnCotiProf.Text = "" Then
            txtCotDolar.Valor = mobj.pCotizaDolar
        End If
    End Sub

    Private Sub mCargarAranceles(Optional ByVal pintSobretasa As Integer = 0)
        Dim lintValor As Integer
        Dim lstrRaza As String
            If cmbAran.Valor.ToString.Length > 0 Then
                lintValor = cmbAran.Valor
            End If

        Dim lstrFiltro As String = cmbActi.Valor + ", @sobretasa =" + clsSQLServer.gFormatArg(pintSobretasa, SqlDbType.Int)

        If cmbActi.Valor = Actividades.RRGG Or cmbActi.Valor = Actividades.Laboratorio Then
            lstrRaza = mRaza()
            If lstrRaza <> "" Then lstrFiltro = lstrFiltro + ", @aran_raza_id=" + clsSQLServer.gFormatArg(lstrRaza, SqlDbType.Int)
        End If
        ' Dario 2014-01-02 corrige error en la carga de aranceles
        lstrFiltro = lstrFiltro + ", @aran_peli_id=" + clsSQLServer.gFormatArg(hdnListaPrecios.Text, SqlDbType.Int)

        If usrClie.ClienteGenerico() Then lstrFiltro = lstrFiltro + ", @aran_clie_gene= 1"
        clsWeb.gCargarRefeCmb(mstrConn, "precios_aran", cmbAran, "S", lstrFiltro)
        'si el arancel esta en el combo 
        If lintValor <> 0 Then
            lstrFiltro = lstrFiltro + ",@aran_id=" + clsSQLServer.gFormatArg(lintValor, SqlDbType.Int)
            Dim lDsDatos As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, "precios_aran_cargar", lstrFiltro)
            If lDsDatos.Tables(0).Rows.Count > 0 Then
                cmbAran.Valor = lintValor
            Else
                mLimpiarTextRelaAran()
            End If
        Else
            mLimpiarTextRelaAran()
        End If

    End Sub
    Private Function mRaza() As String
        If Not cmbRaza.Valor Is DBNull.Value Then
            Return cmbRaza.Valor
            Exit Function
        End If
        If Not txtCriaNume.Valor Is DBNull.Value Then
            Return mobj.ObtenerValorCampo(mstrConn, "criadores_razas", "raza_id", txtCriaNume.Valor.ToString())
            Exit Function
        End If
        Return ""

    End Function
    Private Sub mCargarArancelesyConceptos()
        Dim lstrFiltro As String = cmbActi.Valor
        mCargarAranceles()
        lstrFiltro = cmbActi.Valor
        If usrClie.ClienteGenerico() Then lstrFiltro = lstrFiltro + ", @conc_clie_gene= 1"

        clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConcep, "S", lstrFiltro)

        mArancel()

    End Sub
    Private Sub mCargarComboComprob()
        'si es proforma y no se cargo el combo 
        '06/11/06 se agrego el poder hacer una proforma cuando es al contado para RRGG y Lab
        '13/11/06 se puede generar proforma a partir de otra pendiente
        '14/11/06 se agrego Exposiciones a las activ autorizadas para generar proformas
        Dim lstrActivAutorizadas As Boolean
        If cmbActi.Valor = Actividades.RRGG _
             Or cmbActi.Valor = Actividades.Laboratorio _
             Or cmbActi.Valor = Actividades.Expo _
             Or cmbActi.Valor = Actividades.EstPreLimAntecGen Then _
                lstrActivAutorizadas = True
        If (mobj.pProforma And Not mobj.pProformaAutoExcSaldo And Not mobj.pProformaAuto) _
            Or (cmbFormaPago.Valor = mintContado And lstrActivAutorizadas) _
            Or (mobj.pProformaPend And cmbFormaPago.Valor = mintContado) Then
            If panRRGGoLab.Visible = False Then
                mRRGGoLabComprobVisible(True)
                clsWeb.gCargarRefeCmb(mstrConn, "comprob_tipos", cmbComp, "S", "@coti_ids='" & mobj.SeleccionComprobantes() & "'")
            End If
        Else
            mRRGGoLabComprobVisible(False)
        End If
        ' Dario 2013-07-02 Objeto para obtener el dato de actividad
        Dim _ActividadesBusiness As New Business.Facturacion.ActividadesBusiness
        Dim bitAdmiteProforma As Boolean = _ActividadesBusiness.GetAdmiteProforma(cmbActi.Valor)
        ' verifico si esta visible el objeto de seleccion de tipo de comprobante a generar
        ' y que la actividad no admite la generacion de proformas
        ' ahi oculto el panel
        If (panRRGGoLab.Visible = True And bitAdmiteProforma = False) Then
            mRRGGoLabComprobVisible(False)
        End If

    End Sub
    Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
        Dim lstrCta As String = ""
            If cmbConcep.Valor.Length > 0 Then
                lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbConcep.Valor)
                lstrCta = lstrCta.Substring(0, 1)
            End If
        If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
            If Not Request.Form("cmbCCosConcep") Is Nothing Or pbooEdit Then
                clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, cmbCCosConcep, "id", "descrip", "N")
                If Not Request.Form("cmbCCosConcep") Is Nothing Then
                    cmbCCosConcep.Valor = Request.Form("cmbCCosConcep").ToString
                End If
            End If
        Else
            cmbCCosConcep.Items.Clear()
        End If
    End Sub
    Private Sub mCargarCombos()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S", " @usua_id=" + Session("sUserId").ToString() + ",@factura = 1 ", False)
        ' Dario 2013-04-29 para que cargue el combo artividad default del usuario
        Dim ds As DataSet = cmbActi.DataSource

        For Each itemrow As DataRow In ds.Tables(0).Rows
            If (itemrow("defa") = "true") Then
                cmbActi.Valor() = itemrow("id")
                Exit For
            End If
        Next
        ' fin cambio Dario

        Dim sActividades As String = ""

        If cmbActi.Items.Count <> 1 Then
            For i As Integer = 0 To cmbActi.Items.Count - 1
                If i <> 0 Then
                    sActividades += cmbActi.Items.Item(i).Value + ","
                End If
            Next
            sActividades = sActividades.Remove(sActividades.LastIndexOf(","), 1)
            hdnActividades.Text = sActividades

        End If

        clsWeb.gCargarRefeCmb(mstrConn, "comprob_pagos_tipos", cmbFormaPago, "S")
        ' Dario 2013-10-03 se coloca filtro en la carga del combo "tarjetas", cmbTarj
        'clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "tarjetas", cmbTarj, "S", "@varTodos=N")

        If usrClie.ClienteGenerico Then cmbFormaPago.Valor = mintContado

    End Sub

#End Region

#Region "Limpiar"
    Private Sub mLimpiar()
        mLimpiaCabecera()

    End Sub
    Private Sub mLimpiar(ByVal pintProforma As Integer)
        mLimpiaTodo()
        mLimpiar(True)
    End Sub
    Private Sub mLimpiar(ByVal pboolsolapas As Boolean)

        mBloquearControlesCabe(False)
        hdnId.Text = ""
        txtObser.Text = ""
        lblImpTotal.Text = ""
        lblImpoSaldo.Text = ""
        lblCabeCliente.Text = ""
        lblCabeActividad.Text = ""
        lblCabeTPago.Text = ""
        lblCabeTotalBruto.Text = ""
        lblCabeTotalIVA.Text = ""
        lblCabeTotalNeto.Text = ""
        lblCabeTotalSobre.Text = ""

        hdnAutoriza.Text = ""

        mCrearDataSet("")

        mLimpiarDetaAran()
        mLimpiarDetaAranRRGG()
        mLimpiarDetaAranTurnos()
        mLimpiarDetaConcep()
        mLimpiarCuotas()

        cmbComp.Limpiar()
        hdnComp.Text = ""

        hdnIdGenerado.Text = ""
        hdnImprimio.Text = ""
        hdnImprimir.Text = ""
        hdnNoImprimir.Text = ""
        hdnNoImprime.Text = ""
        hdnProforma.Text = ""
        hdnComprobante.Text = ""
        hdnProfCerro.Text = ""
        hdnDatosPop.Text = ""
        hdnProformaId.Text = ""
        hdnDatosPopTurnoBov.Text = ""
        hdnDatosPopTurnoEqu.Text = ""
        mstrCobraId = 0
        mstrCobraTipo = ""
        mstrCobraOtro = ""
        hdnProfCobrar.Text = ""
        hdnProfCobro.Text = ""

        ' Dario 2013-10-23 oculto el control que muestra si corresponde 
        ' la simulacion de la NC por descuento del laboratorio
        Me.trSimulacionNCAuto.Style.Add("display", "none")
        Me.hdnGeneraNCAuto.Text = "No"
        Me.hdnImporteNCAuto.Text = "0"
        Me.hdnImporteDescuentoAplicadoNCAuto.Text = "0"

        mSetearEditor("", False)


    End Sub
    Private Sub mLimpiaCabecera()
        If cmbActi.Valor = Actividades.RRGG Then
            txtCriaNume.Enabled = True
            cmbRaza.Enabled = True
        End If
        txtCriaNume.Enabled = True
        cmbRaza.Enabled = True

        hdnActivAnterior.Text = ""
        hdnFValorAnterior.Text = ""
        hdnLPrecioAnterior.Text = ""
        hdnFPagoAnterior.Text = ""
        hdnCotiAnterior.Text = ""
        hdnCotiProf.Text = ""
        hdnConf.Text = ""

        cmbListaPrecios.Items.Clear()
        hdnListaPrecios.Text = ""

        ' Dario 2013-04-29 para que cargue el combo artividad default del usuario
        'cmbActi.Limpiar()
        clsWeb.gCargarRefeCmb(mstrConn, "actividades", cmbActi, "S", " @usua_id=" + Session("sUserId").ToString() + ",@factura = 1 ", False)
        Dim ds As DataSet = cmbActi.DataSource

        For Each itemrow As DataRow In ds.Tables(0).Rows
            If (itemrow("defa") = "true") Then
                cmbActi.Valor() = itemrow("id")
                Exit For
            End If
        Next

        ' fin cambio Dario


        cmbFormaPago.Valor = mintContado
        cmbExpo.Items.Clear()
        cmbConv.Items.Clear()
        chkTPers.Enabled = True
        cmbListaPrecios.Enabled = True
        txtFechaValor.Enabled = False

        cmbRaza.Limpiar()
        hdnCria.Text = ""

        ' Dario: 2013-04-16 no limpia bien la ganina 
        txtCriaNume.Text = ""
        hdnCriaNume.Text = ""
            ' fin Dario 2013-04-16

            txtFechaIva.Fecha = Today
            usrClie.Limpiar()
        hdnSocioId.Text = ""
        hdntexto.Text = ""

        hdnClieId.Text = ""
        txtNombApel.Valor = ""
        hdnClieDiscIVA.Text = ""
        txtSocio.Valor = ""
        hdnFechaValor.Text = ""
            '01/09/2010  Agregar limpieza de la cotizacion del dolar
            txtFechaValor.Text = Today.ToString("dd/MM/yyyy")

            DivimgTarj.Style.Add("display", "none")
        DivimgCtrlTurn.Style.Add("display", "none")
        divClieGene.Style.Add("display", "none")
        Me.DivimgCtrlAnalisis.Style.Add("display", "none")
        'lo muestra siempre(250407)
        'divCriador.Style.Add("display", "none")

        tblEstadoSaldos.Visible = clsSQLServer.gObtenerValorCampo(mstrConn, "emisores_ctros", SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request), "emct_datos_fact")
        hdnAntClieId.Text = ""

        DivimgAlertaSaldosAmarillo.Style.Add("display", "none")
        DivimgAlertaSaldosVerde.Style.Add("display", "none")
        DivimgAlertaSaldosRojo.Style.Add("display", "none")
        DivimgEspecVerde.Style.Add("display", "none")
        DivimgEspecRojo.Style.Add("display", "none")
        divExpo.Style.Add("display", "none")
        divEstadoSaldos.Style.Add("display", "none")
        divProformas.Style.Add("display", "none")

        hdnLoginPop.Text = ""
        lblAutoUsuaTit.Text = ""

        chkTPers.Checked = True
        'If hdnSelecProf.Text = "" Then mSetearTramitePersonal()

        mLimpiaTodo()
        mLimpiar(True)

        mSetearTramitePersonal()

    End Sub
    Private Sub mSetearTramitePersonal()
        If hdnProformaId.Text = "" Then
            'solo si no viene de proforma
            txtFechaValor.Enabled = Not chkTPers.Checked
            If Not txtFechaValor.Enabled Then
                    txtFechaValor.Fecha = Today
                    chkVtaTele.Checked = False
            End If
        End If
    End Sub
    Private Sub mLimpiaTodo()
        hdnActiId.Text = ""
        hdnFechaIva.Text = ""
        hdnFechaVigenciaProf.Text = ""

        txtTarjNro.Text = ""
        txtTarjCuo.Text = ""


        txtCotDolar.Text = ""

        mCotizacionDolar()

        '  imgAlertaSaldos.ImagesUrl = "imagenes/CircAzul.bmp"
        '      imgEspec.ImagesUrl = "imagenes/CircTrianAzul.bmp"


        grdDetalle.CurrentPageIndex = 0
        grdCuotas.CurrentPageIndex = 0
        hdnDatosTarjPop.Text = ""

    End Sub
    Private Sub mLimpiarTextRelaAran(Optional ByVal pbooltodo As Boolean = True)
        If pbooltodo Then txtCantAran.Text = ""
        txtPUnitAran.Text = ""
        txtPUnitAranSIVA.Text = ""
        txtImpoIvaAran.Text = ""
        txtTasaIVAAran.Text = ""
        txtImpoAran.Text = ""
        chkExentoAran.Checked = False

    End Sub

    Private Sub mLimpiarDetaAran(Optional ByVal pboolTodo As Boolean = True)
        If pboolTodo Then
            hdnDetaTempId.Text = ""

        End If
        mobj.pPrecioNosocio = False
        mLimpiarTextRelaAran()
        txtCantSCAran.Text = ""
        txtCantNoFacAranRRGG.Text = ""
        chkSCargoAran.Checked = False
        cmbAran.Limpiar()
        txtTramNume.Text = ""

        DivAranNormal.Style.Add("display", "inline")
        DivAranSobretasa.Style.Add("display", "none")
        ' Dario 2013-09-12
        divTotalizador.Style.Add("display", "none")
        Me.hdnMuestraContadorLabiratorio.Text = "No"
        Me.hdnIdEspecieLaboBonif.Text = ""

        'panAranNormal.Visible = True
        'panAranSobretasa.Visible = False
        hdnDatosPopTurnoBov.Text = ""
        hdnDatosPopTurnoEqu.Text = ""
        txtDescAmpliAran.Text = ""
        txtTasaSobreTasa.Text = ""
        txtTotalSobreTasa.Text = ""
        hdnTasaSobreTasa.Text = ""
        hdnTotalSobreTasa.Text = ""

        If mobj.pRRGG Then mLimpiarDetaAranRRGG()
        If mobj.pLabora Then mLimpiarDetaAranTurnos()

        mSetearEditor(mstrTablaDetaAran, True)
        mSetearEditor("", mobj.ExistenItemsEnLaFactura(mdsDatos))


    End Sub
    Private Sub mLimpiarDetaAranTurnos()

        hdnDetaAranId.Text = ""
        txtTurnosDC.Valor = ""
        txtTurnosDP.Valor = ""
        txtTurnosRT.Valor = ""
        txtTurnosRTU.Valor = ""
        txtTurnosSP.Valor = ""

    End Sub
    Private Sub mLimpiarDetaAranRRGG()

        hdnDetaAranId.Text = ""
        ' combo aranceles
        ' combo centro de costo
        txtCantAran.Text = ""
        txtCantSCAran.Text = ""


        txtCantNoFacAranRRGG.Text = ""
        lblFechaAranRRGG.Text = "Fecha:"
        txtFechaAranRRGG.Text = ""
        txtRPAranDdeRRGG.Text = ""
        txtRPAranHtaRRGG.Text = ""
        txtActaAranRRGG.Text = ""
        'clsWeb.gActivarControl(txtFechaAranRRGG, False)
        clsWeb.gActivarControl(txtActaAranRRGG, False)
        'clsWeb.gActivarControl(txtRPAranDdeRRGG, False)
        'clsWeb.gActivarControl(txtRPAranHtaRRGG, False)

        '   mSetearEditor(mstrTablaDetaAran, True)
    End Sub
    Private Sub mLimpiarDetaConcep(Optional ByVal pboolTodo As Boolean = True)
        If pboolTodo Then
            hdnDetaTempId.Text = ""
        End If
        cmbConcep.Limpiar()
        cmbCCosConcep.Items.Clear()

        txtImporteConcep.Text = ""
        txtImpoIvaConcep.Text = ""
        txtTasaIVAConcep.Text = ""
        '  txtImpoNConcep.Text = ""
        hdnTasaIVAConcep.Text = ""
        hdnImpoIvaConcep.Text = ""
        txtTasaSobreTasaC.Text = ""
        txtTotalSobreTasaC.Text = ""
        hdnTasaSobreTasaC.Text = ""
        hdnTotalSobreTasaC.Text = ""

        txtDescAmpliConcep.Text = ""
        mSetearEditor(mstrTablaDetaConcep, True)
        mSetearEditor("", mobj.ExistenItemsEnLaFactura(mdsDatos))
    End Sub
    Private Sub mLimpiarImportesConceptos()
        ' txtImpoIvaConcep.Valor = ""
        'txtImpoNConcep.Valor = ""
        txtTasaIVAConcep.Valor = ""
        hdnTasaIVAConcep.Text = ""
        hdnImpoIvaConcep.Text = ""

    End Sub
    Private Sub mLimpiarImportesAranceles(Optional ByVal pboolTodo As Boolean = True)
        If pboolTodo Then
            txtPUnitAran.Valor = ""
            txtPUnitAranSIVA.Valor = ""
        End If
        txtImpoAran.Valor = ""
        txtImpoIvaAran.Valor = ""
        txtTasaIVAAran.Valor = ""
        hdlImpoIvaAran.Text = ""
        hdnTasaIVAAran.Text = ""


    End Sub
    Private Sub mLimpiarCuotas()
        hdnCuotasId.Text = ""
        txtCuotFecha.Text = ""
        txtCuotPorc.Text = ""
        txtCuotImporte.Text = ""

        mSetearEditor(mstrTablaVtos, True)
    End Sub

#End Region

#Region "Recalculo de Aranceles"

    Private Sub mValidarArancelesRecalculados(Optional ByVal pboolSeguir As Boolean = False)

        If mobj.pRRGG Then
            mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null and temp_tipo='A'"
            For Each ldr As DataRowView In mdsDatos.Tables(mstrTablaTempDeta).DefaultView
                With ldr
                    If .Item("temp_fecha").ToString <> "" Then
                        If .Item("temp_fecha") > txtFechaValor.Fecha Then
                            If pboolSeguir Then
                                AccesoBD.clsError.gGenerarMensajes(Me, "Existe un arancel con fecha de referencia mayor a la fecha valor.")
                            Else
                                Throw New AccesoBD.clsErrNeg("Existe un arancel con fecha de referencia mayor a la fecha valor.")
                            End If
                        End If
                    End If
                    If Not .Item("temp_fecha") Is DBNull.Value Then
                            mobj.AranRRGGValidacionFecha(mstrConn, .Item("temp_aran_concep_id"), Convert.ToDateTime(.Item("temp_fecha")), Convert.ToDateTime(txtFechaValor.Fecha))
                            If Not .Item("temp_acta") Is DBNull.Value Then
                            'If mobj.pAranRRGGActa And mobj.pAranRRGGDescFecha <> "" AndAlso .Item("temp_acta") = "" Then

                                If DateDiff(DateInterval.Day, Convert.ToDateTime(.Item("temp_fecha")), Convert.ToDateTime(txtFechaValor.Fecha)) > mintDiasAnio AndAlso .Item("temp_acta") Is DBNull.Value Then
                                    If pboolSeguir Then
                                        AccesoBD.clsError.gGenerarMensajes(Me, "Para la fecha valor ingresada, el arancel ( " + mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_codi", .Item("temp_aran_concep_id").ToString) + ") necesita nro. de acta.")
                                    Else
                                        Throw New AccesoBD.clsErrNeg("Para la fecha valor ingresada, el arancel ( " + mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_codi", .Item("temp_aran_concep_id").ToString) + ") necesita nro. de acta.")
                                    End If
                                End If

                        End If
                        If mobj.pAranRRGGError And mobj.pAranRRGGDescFecha <> "" Then
                            If pboolSeguir Then
                                AccesoBD.clsError.gGenerarMensajes(Me, "Para la fecha valor ingresada, el arancel ( " + mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_codi", .Item("temp_aran_concep_id").ToString) + ") no corresponde con relacion a la fecha de referencia.")
                            Else
                                Throw New AccesoBD.clsErrNeg("Para la fecha valor ingresada, el arancel ( " + mobj.ObtenerValorCampo(mstrConn, "arancelesX", "aran_codi", .Item("temp_aran_concep_id").ToString) + ") no corresponde con relacion a la fecha de referencia.")
                            End If

                        End If
                    End If
                End With
            Next
        End If

    End Sub

    Private Sub mModifDatosDeta()
        Dim lintIdDeta As Integer
        Dim lstrFiltro As String

        mBorrarSobretasas_Conceptos()

        For Each ldrDatos As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select("temp_borra is null and temp_tipo='A'")
            With ldrDatos
                mobj.SobretasasDelArancelNormal(mstrConn, .Item("temp_aran_concep_id"), cmbActi.Valor, txtFechaIva.Fecha)
                mobj.AranRRGG(mstrConn, .Item("temp_aran_concep_id"), IIf(.Item("temp_auto") Is DBNull.Value, 0, 1))
                ' solo los aranceles que no son sobretasa automaticas
                If mobj.pAranRRGGSobretasa = 0 Then
                    '***************** calculos sobre el arancel ***************************
                    '*********************************************************************** 
                    Dim lstrFechaVig As String = ""
                    Dim lstrVacio As String = ""
                    'If hdnProformaId.Text = "" Then ' no es proforma
                    'lstrFiltro = .Item("temp_aran_concep_id").ToString() + ";" + .Item("temp_cant").ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + txtFechaValor.Fecha.ToString() + ";" + lstrVacio
                    'Else
                    If hdnFormaPago.Text = "" Then hdnFormaPago.Text = cmbFormaPago.Valor
                    lstrFiltro = .Item("temp_aran_concep_id").ToString() + ";" + .Item("temp_cant").ToString() + ";" + cmbActi.Valor.ToString() + ";" + hdnFechaIva.Text + ";" + hdnCotDolar.Text + ";" + hdnFormaPago.Text + ";" + cmbListaPrecios.Valor.ToString() + ";" + lstrVacio
                    'End If
                    Dim vsRet As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
                    'precio unitario del arancel
                    .Item("temp_unit_impo") = vsRet(0)
                    'precio unitario con iva
                    .Item("temp_unit_c_iva") = vsRet(5)
                    'tasa iva
                    .Item("temp_tasa_iva") = vsRet(2)

                    ' Dario 2013-06-10 corrige el proble de no calculo de iva
                    ' (pasa cuando se cabia la forma de pago, ahi no calcula el iva
                    ' ya que no mira la condicion del cliente si discrimina o no el iva)
                    If usrClie.DiscrimIVA Then
                        .Item("temp_impo") = vsRet(1) ' discrimina iva paso costo con recargo segun correspoda forma de pago
                    Else
                        .Item("temp_impo") = vsRet(4) ' no discrimina iva paso toral precio unitario, recargo si corresponde y iva todo junto
                    End If
                    ''importe total s/iva
                    '.Item("temp_impo") = vsRet(1)
                    ''importe total c/iva
                    .Item("temp_impo_ivai") = vsRet(4)
                    ' fin Dario 2013-06-10

                    'precio No Socio para luego generar el concepto adicional
                    mobj.pPrecioNosocio = True

                    Dim vsRets As String() = SRA_Neg.Utiles.CalculoArancel(Me, mstrConn, Session("sUserId").ToString(), lstrFiltro).Split(";")
                    mobj.pPrecioNosocio = False
                    If usrClie.DiscrimIVA Then
                        .Item("temp_impo_Nosocio_aran") = vsRets(1)
                    Else
                        .Item("temp_impo_Nosocio_aran") = vsRets(4)
                    End If
                    ' Dario 2013-04-26 incluyo adherente
                    .Item("temp_impo_Adhe_aran") = vsRet(8)

                    lintIdDeta = .Item("temp_id")

                    ' sobretasas - solo para RRGG 
                    If mobj.pRRGG Then
                        mSobretasas(lintIdDeta, .Item("temp_aran_concep_id"), .Item("temp_cant"), IIf(.Item("temp_fecha") Is DBNull.Value, Today.Date, .Item("temp_fecha")), IIf(hdnFechaVigenciaProf.Text = "", Today.Date, clsFormatear.gFormatFechaDateTime(hdnFechaVigenciaProf.Text)))
                    End If
                    ' conceptos autom�ticos
                    mConceptoAutoSinCargo(lintIdDeta, IIf(usrClie.DiscrimIVA, .Item("temp_impo"), .Item("temp_impo_ivai")), .Item("temp_aran_concep_id"))
                    mConceptoAutoLabTurnos(ldrDatos, lintIdDeta, .Item("temp_impo").ToString(), .Item("temp_aran_concep_id").ToString())
                    'elimino los conceptos automaticos que representan varios aranceles
                    mBorrar_Conceptos()
                    mConceptoAutoAdicionalNoSocio()
                    mConceptoAutoAdicionalConvenio()
                    mAdicionalesPreciosSobreTasas()
                End If 'aranceles que no son sobretasa
            End With

        Next

    End Sub

    ' Dario 2013-07-12 
    ' metodo que se usa para hacer el recalculo de las sobretasas 
    ' para los vencimientos futuros
    Private Sub mModifDatosDetaSoloSobretasaPorVto()
        Dim lintIdDeta As Integer
        Dim lstrFiltro As String
        Dim pstrIdDeta As String = String.Empty
        'elimina concep automaticos y sobretasas
        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.Sort = "temp_id"
        For Each ldr As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select("temp_rela_id is not null" & IIf(pstrIdDeta = "", "", " and temp_rela_id = " & pstrIdDeta), "temp_id desc")
            With mdsDatos.Tables(mstrTablaTempDeta)
                ' elimina definitivamente la fila (es para sobretasas o conceptos eliminados por el programa, no por el usuario)
                .Rows.Remove(.Select("temp_id=" & ldr.Item("temp_id").ToString())(0))
            End With
            ' recarga la grilla de la pagina para luego poder tomar los datos de ahi
            ' para el nuevo recalulo
            mdsDatos.Tables(mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
            grdDetalle.DataSource = mdsDatos.Tables(mstrTablaTempDeta).DefaultView
            grdDetalle.DataBind()
        Next
        mdsDatos.Tables(mstrTablaTempDeta).DefaultView.Sort = ""
        ' recorro los datos de los aranceles y recalculo las sobretasas para la nueva fecha calculada
        For Each ldrDatos As DataRow In mdsDatos.Tables(mstrTablaTempDeta).Select("temp_borra is null and temp_tipo='A'")
            With ldrDatos
                mobj.SobretasasDelArancelNormal(mstrConn, .Item("temp_aran_concep_id"), cmbActi.Valor, txtFechaIva.Fecha)
                mobj.AranRRGG(mstrConn, .Item("temp_aran_concep_id"), IIf(.Item("temp_auto") Is DBNull.Value, 0, 1))
                ' solo los aranceles que no son sobretasa automaticas
                If mobj.pAranRRGGSobretasa = 0 Then
                    lintIdDeta = .Item("temp_id")
                    ' Dario 2013-07-19 nuevo metodo para el calculo de sobretasas 
                    ' para pasar el ini rp y el fin rp que no pasan en la funcion normal
                    mSobretasasVtosRetrasados(lintIdDeta, .Item("temp_aran_concep_id"), .Item("temp_cant") _
                                             , IIf(.Item("temp_fecha") Is DBNull.Value, Today.Date, .Item("temp_fecha")) _
                                             , IIf(hdnFechaVigenciaProf.Text = "", Today.Date, clsFormatear.gFormatFechaDateTime(hdnFechaVigenciaProf.Text)) _
                                             , IIf(.Item("temp_inic_rp") Is DBNull.Value, "", .Item("temp_inic_rp")) _
                                             , IIf(.Item("temp_fina_rp") Is DBNull.Value, "", .Item("temp_fina_rp")))


                    mBorrar_Conceptos()
                    ' Dario ver 2013-08-21
                    mConceptoAutoAdicionalNoSocio()
                    mConceptoAutoAdicionalConvenio()
                    mAdicionalesPreciosSobreTasas()
                End If
            End With
        Next
    End Sub


    Private Sub mRecalculaAranceles(Optional ByVal pboolSeguir As Boolean = False)
        Try
            If hdnLPrecioAnterior.Text <> cmbListaPrecios.Valor Or _
                    hdnFValorAnterior.Text <> txtFechaValor.Text Or _
                    hdnCotiAnterior.Text <> txtCotDolar.Valor Or _
                    hdnActivAnterior.Text <> cmbActi.Valor Or _
                    hdnFPagoAnterior.Text <> IIf(cmbFormaPago.Valor Is DBNull.Value, "", cmbFormaPago.Valor) Then

                mInicializarVariablesCalculo()
                If cmbFormaPago.Valor Is DBNull.Value Or usrClie.Valor Is DBNull.Value Or txtFechaValor.Fecha Is DBNull.Value Then Exit Sub
                Dim lintId As Integer
                mValidarArancelesRecalculados(pboolSeguir)
                mModifDatosDeta()

                ' Dario 2014-02-07 nuevo concepto automatico bonif laboratorio
                Me.mConceptoAutoBonifLab()
                ' turnos de laboratorio
                mCalcularCantTurnos()
                mCargarGrillaArancelesConceptos()
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region


    Private Sub txtSocio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSocio.TextChanged

    End Sub

    Private Sub hdnNoImprime_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnNoImprime.TextChanged
        Try
            ' si no cancela la impresion
            ' y no es proforma (no es necesario controlar la impresi�n)
            'If CBool(CInt(hdnImprimio.Text)) And mobj.pTipoComprobGenera = TipoComprobGene.Factura Then
            '    mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, hdnImprimir.Text)

            '    With mdsDatos.Tables(0)
            '        .TableName = mstrTabla
            '        .Rows(0).Item("comp_impre") = CBool(CInt(hdnImprimio.Text))
            '    End With

            '    While mdsDatos.Tables.Count > 1
            '        mdsDatos.Tables.Remove(mdsDatos.Tables(1))
            '    End While

            '    Dim lobj As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            '    lobj.Modi()
            'End If
            ''ventana con tramites generados (tanto facturas como proformas)
            mobj.LetraComprob(mstrConn, usrClie.TipoIVAId)
            Dim lstrletra As String = mobj.pLetraComprob
            Dim lstrProf As String = ""
            If mobj.pTipoComprobGenera <> TipoComprobGene.Factura Then lstrProf = "S"
            mAbrirVentana(hdnIdGenerado.Text, hdnComprobante.Text, hdnCompGenera.Text, lstrletra, lstrProf)
            mAgregar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub hdnProfCerro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnProfCerro.TextChanged
        Dim consulta As String = "proformas_baja " + hdnProfCerrarId.Text + "," + Session("sUserId")
        clsSQLServer.gExecuteQuery(mstrConn, consulta)
        mAgregar()

    End Sub

    Private Sub hdnProfCobro_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnProfCobro.TextChanged
        Dim consulta As String = "proformas_baja " + hdnProfCerrarId.Text + "," + Session("sUserId")
        If hdnProfCobro.Text = 1 Then
            clsSQLServer.gExecuteQuery(mstrConn, consulta)
        End If
        hdnProfCobro.Text = ""
        'mAltaConCobranzas2(mstrCobraId, mstrCobraTipo, mstrCobraOtro)
        'mAgregar()
        'Dim lstrComprobante As String = lstrcomp + " " + lstrletra + " " + lstrCompGenera
        'lsbPagina.Append("Cobranzas.aspx?comp_id=" & idComp & "&tipo=" & pstrTipo & "&fac=" & pstrFac & "&titu=" & lstrComprobante)
        'clsWeb.gGenerarPopUp(Me, lsbPagina.ToString, 700, 600, 100, 100)
        If hdnCobranzaId.Text <> "" Then
            mAltaConCobranzas2(hdnCobranzaId.Text, hdnCobranzaTipo.Text, hdnCobranzaFac.Text)
        End If
    End Sub

        Private Function iff(p1 As Boolean, vsRet As String, p3 As Integer) As String
            Throw New NotImplementedException
        End Function

    End Class

End Namespace
