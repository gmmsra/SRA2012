<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.AlumFactCuotas" CodeFile="AlumFactCuotas.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD> 
		<title>Facturaci�n de Cuotas</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript" src="includes/impresion.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');Imprimir();" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server" width="391px">Facturaci�n de Cuotas</asp:label>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<asp:panel id="panDato" runat="server" width="100%" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px">
										<TABLE class="FdoFld" id="Table2" style="WIDTH: 100%; HEIGHT: 106px" cellPadding="0" align="left"
											border="0">
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
														<TABLE id="TableCabecera" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD></TD>
																<TD vAlign="top" align="right" height="5"></TD>
															</TR>
															<TR>
																<TD align="right" width="130">
																	<asp:Label id="lblMes" runat="server" cssclass="titulo">Mes:</asp:Label>&nbsp;
																</TD>
																<TD>
																	<cc1:combobox class="combo" id="cmbMes" runat="server" Width="160px"></cc1:combobox>&nbsp;
																	<asp:Label id="lblAnio" runat="server" cssclass="titulo">/</asp:Label>&nbsp;
																	<cc1:numberbox id="txtAnio" runat="server" cssclass="cuadrotexto" Width="35px" AceptaNull="False"
																		MaxLength="4" MaxValor="2090" CantMax="4"></cc1:numberbox></TD>
															</TR>
															<TR>
																<TD align="right" background="imagenes/formdivmed.jpg" colSpan="3" height="2"><IMG height="2" src="imagenes/formdivmed.jpg" width="1"></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:label id="lblAlum" runat="server" cssclass="titulo">Alumno:</asp:label>&nbsp;</TD>
																<TD>
																	<UC1:CLIE id="usrAlum" runat="server" AceptaNull="False" Saltos="1,1,1,1" Ancho="800" FilSociNume="True"
																		Tabla="Alumnos" FilFanta="true"></UC1:CLIE></TD>
															</TR>
															<TR>
																<TD align="right">
																	<asp:label id="lblFecha" runat="server" cssclass="titulo">Fecha Comp.:</asp:label>&nbsp;</TD>
																<TD>
																	<cc1:DateBox id="txtFecha" runat="server" cssclass="cuadrotexto" Width="80px" Enabled="False"
																		Obligatorio="true"></cc1:DateBox></TD>
															</TR>
															<TR height="30">
																<TD align="center" colSpan="2"><A id="editarCabe" name="editarCabe"></A>
																	<asp:Button id="btnGene" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
																		Text="Generar"></asp:Button></TD>
															</TR>
														</TABLE>
													</asp:panel>
												</TD>
											</TR>
											
											<!---Aqui iria el separador --->
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panDeta" runat="server" cssclass="titulo" Width="100%" visible="false">
														<TABLE id="TableDeta" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD height="1" style="WIDTH: 100%" bgcolor=DarkBlue></TD>
															</TR>
															<TR>
																<TD vAlign="top" align="right">
																	<asp:ImageButton id="imgClose" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="images\Close.bmp"></asp:ImageButton></TD>
															</TR>
															<TR>
																<TD>
																	<asp:Label id="lblPagos" runat="server" cssclass="titulo">Cuotas:</asp:Label>&nbsp;</TD>
															</TR>
															<TR>
																<TD vAlign="top" align="center" colSpan="2">
																	<asp:datagrid id="grdDeta" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnEditCommand="mEditarDatos"
																		OnPageindexChanged="grdDeta_Page" OnUpdateCommand="mEditarDatos" AutoGenerateColumns="False"
																		CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<Columns>
																			<asp:TemplateColumn>
																				<HeaderStyle Width="2%"></HeaderStyle>
																				<ItemTemplate>
																					<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																						<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																					</asp:LinkButton>
																				</ItemTemplate>
																			</asp:TemplateColumn>
																			<asp:BoundColumn Visible="False" DataField="faca_id"></asp:BoundColumn>
																			<asp:BoundColumn DataField="faca_carr_desc" HeaderText="Carrera"></asp:BoundColumn>
																			<asp:BoundColumn DataField="faca_papl_desc" HeaderText="Plan Pagos"></asp:BoundColumn>
																			<asp:BoundColumn DataField="faca_alum_lega" HeaderText="Legajo"></asp:BoundColumn>
																			<asp:BoundColumn DataField="faca_clie_apel" HeaderText="Alumno"></asp:BoundColumn>
																			<asp:BoundColumn DataField="faca_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid><A id="editarDeta" name="editarDeta"></A></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" align="center" colSpan="3">
													<asp:panel id="PanConc" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
														Width="85%" visible="false">
														<TABLE id="TableConc" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															
															<TR>
																<TD>
																	<asp:Label id="lblConc" runat="server" cssclass="titulo">Conceptos de la cuota:</asp:Label>&nbsp;</TD>
															</TR>
															<TR> <!--- detalle - Conceptos-->
																<TD>
																	<asp:datagrid id="grdConc" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageindexChanged="grdConc_Page"
																		AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Left"
																		AllowPaging="True" Visible="True">
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<Columns>
																			<asp:BoundColumn DataField="fade_conc_desc" HeaderText="Descripci�n"></asp:BoundColumn>
																			<asp:BoundColumn DataField="fade_impo" HeaderText="Importe" DataFormatString="{0:F2}" ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid><A id="editarConc" name="editarConc"></A></TD>
															</TR>
															<TR height="30">
																<TD align="center">
																	<asp:Button id="btnBajaDeta" runat="server" cssclass="boton" Width="130px" CausesValidation="False"
																		Text="No incluir cuota"></asp:Button></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
											<TR>
												<TD style="WIDTH: 100%" colSpan="3">
													<asp:panel id="panFact" runat="server" cssclass="titulo" Width="100%" visible="false">
														<TABLE id="tabFact" style="WIDTH: 100%" cellPadding="0" align="left" border="0">
															<TR>
																<TD>
																	<asp:Label id="lblFact" runat="server" cssclass="titulo">Facturas:</asp:Label>&nbsp;</TD>
															</TR>
															<TR>
																<TD vAlign="top" align="center" colSpan="2">
																	<asp:datagrid id="grdFact" runat="server" width="80%" BorderWidth="1px" BorderStyle="None" AutoGenerateColumns="False"
																		CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center" AllowPaging="False">
																		<FooterStyle CssClass="footer"></FooterStyle>
																		<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																		<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																		<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
																		<Columns>
																			<asp:BoundColumn DataField="comp_clie_id" HeaderText="Cliente"></asp:BoundColumn>
																			<asp:BoundColumn DataField="_comp_desc" HeaderText="Apellido / Raz�n Social"></asp:BoundColumn>
																			<asp:BoundColumn DataField="comp_neto" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
																				ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right"></asp:BoundColumn>
																		</Columns>
																		<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																	</asp:datagrid></TD>
															</TR>
														</TABLE>
													</asp:panel></TD>
											</TR>
										</TABLE>
									</asp:panel>
								</TD>
							</TR>
						</TABLE>
						<ASP:PANEL id="panBotones" Runat="server" Visible="False">
							<TABLE width="100%">
								<TR height="30">
									<TD align="center">
										<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" CausesValidation="False"
											Text="Grabar"></asp:Button></TD>
								</TR>
							</TABLE>
						</ASP:PANEL>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprId" runat="server"></asp:textbox>
				<asp:textbox id="hdnImprimir" runat="server"></asp:textbox>
				<OBJECT ID="RSClientPrint" CLASSID="CLSID:FA91DF8D-53AB-455D-AB20-F2F023E498D3" CODEBASE="<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>/includes/reportserver.cab#Version=2000,080,1038,000" VIEWASTEXT></OBJECT>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editarCabe"]!= null)
			document.location='#editarCabe';
		if (document.all["editarDeta"]!= null)
			document.location='#editarFact';
		if (document.all["editarConc"]!= null)
			document.location='#editarConc';
			
		function Imprimir()
		{
			var strNume;
			if(document.all("hdnImprimir").value!="")
			{
				var vFCs = document.all("hdnImprimir").value.split(",");
				document.all("hdnImprimir").value = "";

				if (window.confirm("�Desea imprimir las facturas generadas?"))
				{
					for(iFc=0; iFc<vFCs.length; iFc++)
					{
						strNume=LeerCamposXML("comprobantesX", vFCs[iFc], "numero");
						if (window.confirm("�Desea imprimir la factura " + strNume + "?"))
						{
							try{
								var sRet = mImprimirCopias(2,'O',vFCs[iFc],"Factura", "<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // original

								if(sRet=="0") return(sRet);
								
								if (window.confirm("�Se imprimio la factura correctamente?"))
								{
									var lstrCopias = LeerCamposXML("comprobantes_imprimir", vFCs[iFc], "copias");

									if (lstrCopias != "0" && lstrCopias != "1")
										sRet = mImprimirCopias(lstrCopias,'D',vFCs[iFc],"Factura","<%=Session("sImpreTipo")%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString%>","<%=System.Configuration.ConfigurationSettings.AppSettings("conRepoDire").ToString%>");  // copias
									
									EjecutarMetodoXML("Utiles.ModiImpreso", vFCs[iFc] + ";1");
								}
								else
									document.all("hdnImprId").value = '';
							}
							catch(e)
							{
								document.all("hdnImprId").value = '';
								alert("Error al intentar efectuar la impresi�n");
							}
						}
					}
				}
			}
		}
		</SCRIPT>
	</BODY>
</HTML>
