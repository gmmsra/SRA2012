Namespace SRA

Partial Class AlumFactManual
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblPagos As System.Web.UI.WebControls.Label


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
   Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
   Private mstrTablaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep
   Private mstrTablaVtos As String = SRA_Neg.Constantes.gTab_ComprobVtos
   Private mstrTablaPagosCuotas As String = SRA_Neg.Constantes.gTab_ComprobPagosCuotas
   Private mstrTablaAsiento As String = "facturacion_asiento"

   Private mstrParaPageSize As Integer
   Private mstrTipo As String
   Private mstrCompId As String
   Private mstrConn As String
   Private mdsDatos As DataSet
   Private mstrFac As String
   Private mstrInseId As String
   Private mdecGriTotal As Decimal

   Private Enum ColumnasDeta As Integer
      Id = 1
      Alumno = 2
      Carrera = 3
      Periodo = 4
      Importe = 5
   End Enum

   Private Enum ColumnasFact As Integer
      Id = 1
      Importe = 4
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mstrInseId = Request("fkvalor")

         usrAlum.FilInseId = mstrInseId

         If (Not Page.IsPostBack) Then
            Session(mSess(mstrTabla)) = Nothing
            mdsDatos = Nothing

            mAgregar()

            mCargarCombos()

            clsWeb.gInicializarControles(Me, mstrConn)

			Else
				mCargarCentroCostos(False)
				mdsDatos = Session(mSess(mstrTabla))
			End If

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub


	'   Private Sub mEstablecerPerfil()
	'      Dim lbooPermiAlta As Boolean
	'      Dim lbooPermiModi As Boolean

	'      'If (Not clsSQLServer.gMenuPermi(CType(Opciones.Conceptos, String), (mstrConn), (Session("sUserId").ToString()))) Then
	'      'Response.Redirect("noaccess.aspx")
	'      'End If

	'      'lbooPermiAlta = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Alta, String), (mstrConn), (Session("sUserId").ToString()))
	'      'btnAlta.Visible = lbooPermiAlta

	'      'btnBaja.Visible = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Baja, String), (mstrConn), (Session("sUserId").ToString()))

	'      'lbooPermiModi = clsSQLServer.gMenuPermi(CType(Opciones.Conceptos_Modificación, String), (mstrConn), (Session("sUserId").ToString()))
	'      'btnModi.Visible = lbooPermiModi

	'      'btnLimp.Visible = (lbooPermiAlta Or lbooPermiModi)
	'      'btnAgre.Visible = (lbooPermiAlta Or lbooPermiModi)
	'   End Sub

	Private Sub mCargarCombos()
		Dim lstrActiId As String
		lstrActiId = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", mstrInseId).Tables(0).Rows(0).Item("inse_acti_id")

		clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConc, "S", "@conc_acti_id=" + lstrActiId)
		clsWeb.gCargarRefeCmb(mstrConn, "centrosc", cmbCcos, "N")
	End Sub

	Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
		Dim lstrCta As String = ""
		Dim ban As Boolean = False
            If Not cmbConc.Valor Is DBNull.Value And cmbConc.Valor.ToString.Length > 0 Then
                lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbConc.Valor)
                lstrCta = lstrCta.Substring(0, 1)
            End If
		If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
			If Not Request.Form("cmbCCos") Is Nothing Or pbooEdit Then
				clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, cmbCcos, "id", "descrip", "N")
				If Not Request.Form("cmbCCos") Is Nothing Then
					cmbCcos.Valor = Request.Form("cmbCCos").ToString
				End If
			End If
		Else
			cmbCcos.Items.Clear()

		End If

	End Sub


#End Region

#Region "Operaciones sobre el DataGrid"
   Private Sub mConsultarCuotas()
      mdsDatos.Tables(mstrTablaPagosCuotas).DefaultView.Sort = "_carr_desc, _peri_desc"
      grdDeta.DataSource = mdsDatos.Tables(mstrTablaPagosCuotas)
      grdDeta.DataBind()
   End Sub

   Private Sub mConsultarConceptos()
      mdsDatos.Tables(mstrTablaConcep).DefaultView.Sort = "_desc, coco_desc_ampl"
      grdFact.DataSource = mdsDatos.Tables(mstrTablaConcep)
      grdFact.DataBind()
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mLimpiar()
      hdnFactClieId.Text = ""
      usrAlum.Limpiar()
      usrFactClie.Limpiar()

      mSetearAlumnoClienteFact()
      mSetearcmbClieAlum()
      mCargarAlumnos()

            txtFecha.Fecha = Today
            mCrearDataSet("")

      mLimpiarConc()

      mCargarCuotas()
      mConsultarConceptos()
      mConsultarCuotas()
   End Sub

   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function

   Private Sub mAgregar()
      mLimpiar()
   End Sub

   Private Function mAlumId() As Object
      If Not (usrAlum.Valor Is DBNull.Value) Then
         Return (usrAlum.Valor)
      End If

      If Not (cmbClieAlum.Valor Is DBNull.Value) Then
         Return (cmbClieAlum.Valor)
      End If

      Return (DBNull.Value)
	End Function

	Private Sub mCerrarConsul()
		mMostrarPanel(False)
	End Sub

	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		panFact.Visible = pbooVisi
		panBotones.Visible = pbooVisi
		PanConc.Visible = pbooVisi
		grdDeta.Visible = pbooVisi
		grdFact.Visible = pbooVisi

	End Sub
#End Region

#Region "Opciones de ABM"

   Private Sub mCrearDataSet(ByVal pstrId As String)
      Dim ldsAdic As DataSet
      Dim ldtAdic As DataTable

      mdsDatos = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, pstrId)

      mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id") = 0

      With mdsDatos.Tables.Add(mstrTablaAsiento)
         .Columns.Add("proc_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_comp_id", System.Type.GetType("System.Int32"))
         .Columns.Add("proc_audi_user", System.Type.GetType("System.Int32"))

         .Rows.Add(.NewRow)
         .Rows(0).Item("proc_id") = -1
      End With

      SRA_Neg.Utiles.gAgregarTabla(mstrConn, mdsDatos, mstrTablaPagosCuotas)

      If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
         mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
      End If

      mdsDatos.Tables(mstrTablaDeta).Rows.Add(mdsDatos.Tables(mstrTablaDeta).NewRow)

      With mdsDatos.Tables(mstrTablaPagosCuotas)
         .Columns.Add("_cuota_impo", System.Type.GetType("System.Double"))
         .Columns.Add("_cuota_coco_id", System.Type.GetType("System.Int32"))
         .Columns.Add("_beca_impo", System.Type.GetType("System.Double"))
         .Columns.Add("_beca_coco_id", System.Type.GetType("System.Int32"))
         .Columns.Add("_deau_impo", System.Type.GetType("System.Double"))
         .Columns.Add("_deau_coco_id", System.Type.GetType("System.Int32"))
         .Columns.Add("_soci_impo", System.Type.GetType("System.Double"))
         .Columns.Add("_soci_coco_id", System.Type.GetType("System.Int32"))
         .Columns.Add("_soci_susp_impo", System.Type.GetType("System.Double"))
         .Columns.Add("_soci_susp_coco_id", System.Type.GetType("System.Int32"))
         .Columns.Add("_inte_impo", System.Type.GetType("System.Double"))
         .Columns.Add("_inte_coco_id", System.Type.GetType("System.Int32"))
         .Columns.Add("_carr_desc", System.Type.GetType("System.String"))
         .Columns.Add("_peri_desc", System.Type.GetType("System.String"))
         .Columns.Add("_alum_desc", System.Type.GetType("System.String"))
      End With

      With mdsDatos.Tables(mstrTablaConcep)
         .Columns.Add("_auto", System.Type.GetType("System.Int32"))
      End With

      mConsultarConceptos()
      mConsultarCuotas()

      mSetearEditorConc(True)

      Session(mSess(mstrTabla)) = mdsDatos
   End Sub

   Private Sub mValidarDatos()
      If mAlumId() Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar el alumno.")
      End If

      If mdsDatos.Tables(mstrTablaPagosCuotas).Select.GetUpperBound(0) = -1 Then
         Throw New AccesoBD.clsErrNeg("Debe seleccionar una cuota.")
      End If
   End Sub

   Private Sub mAlta()
      Try
         Dim lstrId As String

         mValidarDatos()

         mGuardarDatos()

         Dim lobj As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)

         lstrId = lobj.Alta()
         hdnImprimir.Text = lstrId
         hdnFactNume.Text = clsSQLServer.gObtenerEstruc(mstrConn, "comprobantesX", lstrId).Tables(0).Rows(0).Item("numero").ToString

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub mBaja()
      Try
         'Dim lintPage As Integer = grdDato.CurrentPageIndex

         'Dim lobjCliente As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
         'lobjCliente.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

         'grdDato.CurrentPageIndex = 0

         'mConsultar(grdDato, True)

         'If (lintPage < grdDato.PageCount) Then
         '   grdDato.CurrentPageIndex = lintPage

         'End If

         'mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      Dim lstrHost, lstrCemiNume, lstrEmctId As String
      Dim ldecImpo As Decimal
      Dim ldrCovt As DataRow

      With mdsDatos.Tables(mstrTabla).Rows(0)
         .Item("comp_id") = -1
         .Item("comp_fecha") = Today
         .Item("comp_ingr_fecha") = .Item("comp_fecha")
         .Item("comp_clie_id") = hdnFactClieId.Text
         .Item("comp_dh") = 0    '0 = debe
         .Item("comp_cance") = 0 ' 0 = pendiente
         .Item("comp_mone_id") = SRA_Neg.Comprobantes.gMonedaPesos(mstrConn)
         .Item("comp_cs") = 1 ' cta cte
         .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Factura
         .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.Alumnos_FacturacionManual

         .Item("comp_letra") = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", .Item("comp_clie_id").ToString).Tables(0).Rows(0).Item("_ivap_letra")
         .Item("comp_cpti_id") = SRA_Neg.Constantes.ComprobPagosTipos.CtaCte
         .Item("comp_acti_id") = clsSQLServer.gObtenerEstruc(mstrConn, "institutos", mstrInseId).Tables(0).Rows(0).Item("inse_acti_id")

         Dim oFact As New SRA_Neg.Facturacion(Me, mstrConn, Session("sUserId").ToString())
         oFact.CentroEmisorNro(mstrConn)
         .Item("comp_cemi_nume") = oFact.pCentroEmisorNro
         .Item("comp_emct_id") = oFact.pCentroEmisorId
         .Item("comp_host") = oFact.pHost
      End With

      With mdsDatos.Tables(mstrTablaDeta).Rows(0)
         .Item("code_id") = -1
         .Item("code_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
         .Item("code_coti") = 1
         .Item("code_pers") = True
         .Item("code_impo_ivat_tasa") = 0
         .Item("code_impo_ivat_tasa_redu") = 0
         .Item("code_impo_ivat_tasa_sobre") = 0
         .Item("code_impo_ivat_perc") = 0 ' ver
      End With

      'COMPROB_VTOS
      ldrCovt = mdsDatos.Tables(SRA_Neg.Constantes.gTab_ComprobVtos).NewRow
      With ldrCovt
         .Item("covt_id") = clsSQLServer.gObtenerId(.Table, "covt_id")
         .Item("covt_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
         .Item("covt_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_acti_id"), mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_fecha"))
         .Item("covt_porc") = 100
         .Item("covt_cance") = 0

         .Table.Rows.Add(ldrCovt)

      End With
      'CONCEPTOS VARIOS
      For Each lDr As DataRow In mdsDatos.Tables(mstrTablaConcep).Select
         ldecImpo += lDr.Item("coco_impo")
      Next

      mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_neto") = ldecImpo
      mdsDatos.Tables(mstrTablaVtos).Rows(0).Item("covt_impo") = ldecImpo

      Return mdsDatos
   End Function
#End Region

#Region "Eventos"

	Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
		mCerrarConsul()
	End Sub
#End Region

	Private Sub hdnImprimio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnImprimio.TextChanged
		Try
			If hdnImprimio.Text <> "" Then
				Dim lobj As New SRA_Neg.Cobranza(mstrConn, Session("sUserId").ToString(), mstrTabla)

				If hdnImprimir.Text <> "" Then
					lobj.ModiImpreso(hdnImprimir.Text, CBool(CInt(hdnImprimio.Text)))
				End If
			End If

			mLimpiar()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		Finally
			hdnImprimio.Text = ""
			hdnImprimir.Text = ""
		End Try
	End Sub

	Private Sub usrAlum_Cambio(ByVal sender As Object) Handles usrAlum.Cambio
		Try
			mSetearAlumnoClienteFact()

			panFact.Visible = Not (usrAlum.Valor Is DBNull.Value)
			panBotones.Visible = Not (usrAlum.Valor Is DBNull.Value)

			mCargarCuotas()

			mCrearDataSet("")

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub usrFactClie_Cambio(ByVal sender As Object) Handles usrFactClie.Cambio
		Try
			mSetearAlumnoClienteFact()

			mCargarAlumnos()

			mSetearcmbClieAlum()

			mCrearDataSet("")

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub cmbClieAlum_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbClieAlum.SelectedIndexChanged
		Try
			mSetearcmbClieAlum()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub mSetearcmbClieAlum()
		panFact.Visible = Not (cmbClieAlum.Valor Is DBNull.Value)
		panBotones.Visible = Not (cmbClieAlum.Valor Is DBNull.Value)

		mCargarCuotas()
	End Sub

	Private Sub mSetearAlumnoClienteFact()
		usrFactClie.Activo = usrAlum.Valor Is DBNull.Value
		usrAlum.Activo = usrFactClie.Valor Is DBNull.Value

		If Not usrFactClie.Activo Then usrFactClie.Limpiar()
		If Not usrAlum.Activo Then usrAlum.Limpiar()

		grdDeta.Columns(ColumnasDeta.Alumno).Visible = Not (usrFactClie.Valor Is DBNull.Value)

        If mFormatearCadena(usrFactClie.Valor) = "" Then
            If mFormatearCadena(usrAlum.Valor) = "" Then
                usrFactClie.Limpiar()
            Else
                usrFactClie.Valor = clsSQLServer.gCampoValorConsul(mstrConn, "alumnos_consul @alum_id=" + usrAlum.Valor, "alum_fact_clie_id")
            End If
        End If

        mLimpiarConc()
 End Sub

 Private Function mFormatearCadena(ByVal pstrCadena As Object)
    If pstrCadena Is DBNull.Value Then
        Return ("")
    Else
        Return (pstrCadena)
    End If
 End Function

 Private Sub mCargarAlumnos()
  clsWeb.gCargarRefeCmb(mstrConn, "alumnosXcliente", cmbClieAlum, "", "@inse_id=" + mstrInseId + ",@clie_id=" + clsSQLServer.gFormatArg(usrFactClie.Valor.ToString(), SqlDbType.Int))
  cmbClieAlum.Enabled = Not (usrFactClie.Valor Is DBNull.Value)
 End Sub

 Private Sub mCargarCuotas()
  clsWeb.gCargarRefeCmb(mstrConn, "pagos_cuotas", cmbCuota, "", clsSQLServer.gFormatArg(mAlumId.ToString, SqlDbType.Int))
 End Sub

 Private Sub btnAltaDet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDet.Click
  Try
                Dim ldrCopc, ldrTemp, ldrBecaCoco() As DataRow

   Dim ldecImpo As Decimal

                If cmbCuota.Valor Is DBNull.Value Or cmbCuota.Valor.ToString.Length = 0 Then

                    Throw New AccesoBD.clsErrNeg("Debe seleccionar la cuota.")
                End If

   If mdsDatos.Tables(mstrTablaPagosCuotas).Select("copc_alum_id=" & mAlumId.ToString + " AND copc_plco_id=" & cmbCuota.Valor).GetUpperBound(0) <> -1 Then
    Throw New AccesoBD.clsErrNeg("Cuota ya ingresada.")
   End If

   ldrTemp = mObtenerDatosPlanCuota()

   hdnFactClieId.Text = ldrTemp.Item("insc_fact_clie_id")

   'COMPROB_PAGOS_CUOTAS
   ldrCopc = mdsDatos.Tables(mstrTablaPagosCuotas).NewRow
   ldrCopc.Item("copc_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaPagosCuotas), "copc_id")

                With ldrCopc
                    .Item("copc_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                    .Item("copc_plco_id") = cmbCuota.Valor
                    .Item("copc_alum_id") = mAlumId()
                    .Item("copc_insc_id") = ldrTemp.Item("insc_id")
                    .Item("copc_impo") = 0

                    .Item("_carr_desc") = ldrTemp.Item("carr_desc")
                    .Item("_peri_desc") = Month(ldrTemp.Item("plco_fecha")).ToString + "/" + Year(ldrTemp.Item("plco_fecha")).ToString & " - " & ldrTemp.Item("cuota") & "/" & ldrTemp.Item("cuotas")

                    'If Not cmbClieAlum.Valor Is DBNull.Value Then
                    If (cmbClieAlum.Text.Trim().Length > 0) Then
                        .Item("_alum_desc") = cmbClieAlum.SelectedItem.Text
                    End If
                    .Item("_cuota_impo") = 0
                    .Item("_beca_impo") = 0
                    .Item("_deau_impo") = 0
                    .Item("_soci_impo") = 0
                    .Item("_soci_susp_impo") = 0
                    .Item("_inte_impo") = 0

                    .Table.Rows.Add(ldrCopc)
                End With

                'COMPROB_CONCEP (CUOTA)
                mAcumularConcepto(ldrCopc, ldrTemp.Item("papl_cuot_conc_id"), ldrTemp.Item("papl_ccos_id"), ldrTemp.Item("plco_impo"), ldrTemp.Item("conc_desc"), "_cuota_impo", "_cuota_coco_id")
            mAcumularDescConcepto(ldrTemp.Item("papl_cuot_conc_id"))

   If ldrTemp.Item("meses_inte") = 0 And (Not ldrTemp.IsNull("tacl_id") And Not ldrTemp.IsNull("deau_conc_id")) AndAlso ldrTemp.Item("plco_deau_impo") <> 0 Then
    'COMPROB_CONCEP (D.A.)
    mAcumularConcepto(ldrCopc, ldrTemp.Item("deau_conc_id"), ldrTemp.Item("inse_deau_ccos_id"), -1 * ldrTemp.Item("plco_deau_impo"), ldrTemp.Item("deau_conc_desc"), "_deau_impo", "_deau_coco_id")
   End If

   If (Not ldrTemp.IsNull("soci_id") And Not ldrTemp.IsNull("soci_conc_id")) AndAlso ldrTemp.Item("plco_soci_impo") <> 0 Then
    'COMPROB_CONCEP (SOCIO)
    mAcumularConcepto(ldrCopc, ldrTemp.Item("soci_conc_id"), ldrTemp.Item("inse_soci_ccos_id"), -1 * ldrTemp.Item("plco_soci_impo"), ldrTemp.Item("soci_conc_desc"), "_soci_impo", "_soci_coco_id")

    If ldrTemp.Item("soci_suspe") = 1 And Not ldrTemp.IsNull("soci_susp_conc_id") Then
     'COMPROB_CONCEP (SOCIO SUSPENDIDO)
     mAcumularConcepto(ldrCopc, ldrTemp.Item("soci_susp_conc_id"), ldrTemp.Item("inse_soci_susp_ccos_id"), ldrTemp.Item("plco_soci_impo"), ldrTemp.Item("soci_susp_conc_desc"), "_soci_susp_impo", "_soci_susp_coco_id")
    End If
   End If

   '170507 --no se cobra interes por mes de mora 
   'If ldrTemp.Item("meses_inte") > 0 Then
   'INTERESES
   'mAcumularConcepto(ldrCopc, ldrTemp.Item("inse_inte_conc_id"), ldrTemp.Item("inse_inte_ccos_id"), ldrTemp.Item("inte_impo") * ldrTemp.Item("meses_inte"), ldrTemp.Item("inte_conc_desc"), "_inte_impo", "_inte_coco_id")
   'End If

            If Not ldrTemp.IsNull("beca_cuot_conc_id") Then
                'COMPROB_CONCEP (BECA)
                If ldrTemp.Item("beca_impo") <> 0 Then
                    ldecImpo = ldrTemp.Item("beca_impo")
                Else
                    ldecImpo = Math.Round((ldrCopc.Item("copc_impo") * ldrTemp.Item("beca_porc")) / 100, 2)
                End If

                mAcumularConcepto(ldrCopc, ldrTemp.Item("beca_cuot_conc_id"), ldrTemp.Item("beca_ccos_id"), -1 * ldecImpo, ldrTemp.Item("beca_conc_desc"), "_beca_impo", "_beca_coco_id")
            End If

            mConsultarConceptos()
            mConsultarCuotas()

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub

 Private Sub mAcumularConcepto(ByVal ldrCopc As DataRow, ByVal pintConcId As Integer, ByVal pintCcosId As Integer, ByVal pdecImpo As Decimal, ByVal pstrDesc As String, ByVal pstrCampoImpo As String, ByVal pstrCampoCoco As String)
  Dim ldrCoco As DataRow
  Dim lbooExisteConc As Boolean

  With mdsDatos.Tables(mstrTablaConcep).Select("coco_conc_id=" + pintConcId.ToString)
   lbooExisteConc = .GetUpperBound(0) > -1
   If Not lbooExisteConc Then
    ldrCoco = mdsDatos.Tables(mstrTablaConcep).NewRow
    With ldrCoco
     .Item("coco_id") = clsSQLServer.gObtenerId(.Table, "coco_id")
     .Item("coco_impo") = 0
     .Item("_auto") = 1
    End With
   Else
    ldrCoco = .GetValue(0)
   End If
  End With

  With ldrCoco
   .Item("coco_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
   .Item("coco_conc_id") = pintConcId
   .Item("coco_ccos_id") = pintCcosId
   .Item("coco_impo") += pdecImpo
   .Item("coco_impo_ivai") = .Item("coco_impo")

   .Item("_desc") = pstrDesc

   ldrCopc.Item(pstrCampoImpo) = pdecImpo
   ldrCopc.Item(pstrCampoCoco) = .Item("coco_id")
   ldrCopc.Item("copc_impo") += ldrCopc.Item(pstrCampoImpo)

   If Not lbooExisteConc Then
    .Table.Rows.Add(ldrCoco)
   End If
  End With
 End Sub

 Private Sub mAcumularDescConcepto(ByVal pintConcId As Integer)
  Dim lstrDesc As String

  For Each ldrCopc As DataRow In mdsDatos.Tables(mstrTablaPagosCuotas).Select("", "copc_plco_id")
   If lstrDesc <> "" Then
    lstrDesc += " - "
   Else
    lstrDesc = "CARRERA: " + ldrCopc.Item("_carr_desc") + " PERIODO/S: "
   End If
   lstrDesc += ldrCopc.Item("_peri_desc")
  Next

        'Roxi: 19/10/2007: no grabar la descripcion en las observaciones se arma al
        'imprimir las facturas para unificar que salga igual desde todos los lugares.
        'With mdsDatos.Tables(mstrTablaConcep).Select("coco_conc_id=" + pintConcId.ToString)(0)
            '.Item("coco_desc_ampl") = lstrDesc
        'End With
 End Sub

 Private Function mObtenerDatosPlanCuota() As DataRow
  Dim ldsTemp As DataSet
  Dim lstrFiltros As New System.text.StringBuilder
  Dim lvstrCuota(), lstrAnio, lstrMes As String

  If Not cmbCuota.Valor Is DBNull.Value Then
   With clsSQLServer.gObtenerEstruc(mstrConn, SRA_Neg.Constantes.gTab_PagosCuotas, cmbCuota.Valor).Tables(0).Rows(0)
    lstrAnio = CDate(.Item("plco_fecha")).Year
    lstrMes = CDate(.Item("plco_fecha")).Month
   End With
  End If

  With lstrFiltros
   .Append("exec alumnos_fact_busq")
   .Append(" @inse_id=")
   .Append(mstrInseId)
   .Append(",@alum_id=")
   .Append(clsSQLServer.gFormatArg(mAlumId.ToString(), SqlDbType.Int))
   .Append(",@anio=")
   .Append(clsSQLServer.gFormatArg(lstrAnio, SqlDbType.Int))
   .Append(",@mes=")
   .Append(clsSQLServer.gFormatArg(lstrMes, SqlDbType.Int))

   ldsTemp = clsSQLServer.gExecuteQuery(mstrConn, .ToString)
  End With

  Return (ldsTemp.Tables(0).Rows(0))

 End Function

 Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
  Try
   Dim ldrCopc As DataRow

   ldrCopc = mdsDatos.Tables(mstrTablaPagosCuotas).Select("copc_id=" & e.Item.Cells(ColumnasDeta.Id).Text)(0)

   mDescontarCuotaDeConcepto(ldrCopc.Item("_cuota_coco_id").ToString, ldrCopc.Item("_cuota_impo"))
   mDescontarCuotaDeConcepto(ldrCopc.Item("_beca_coco_id").ToString, ldrCopc.Item("_beca_impo"))
   mDescontarCuotaDeConcepto(ldrCopc.Item("_deau_coco_id").ToString, ldrCopc.Item("_deau_impo"))
   mDescontarCuotaDeConcepto(ldrCopc.Item("_soci_coco_id").ToString, ldrCopc.Item("_soci_impo"))
   mDescontarCuotaDeConcepto(ldrCopc.Item("_soci_susp_coco_id").ToString, ldrCopc.Item("_soci_susp_impo"))
   mDescontarCuotaDeConcepto(ldrCopc.Item("_inte_coco_id").ToString, ldrCopc.Item("_inte_impo"))

   ldrCopc.Delete()

   mConsultarConceptos()
   mConsultarCuotas()

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub

 Private Sub mDescontarCuotaDeConcepto(ByVal lstrCocoId As String, ByVal ldecImpo As Decimal)
  If lstrCocoId <> "" Then
   For Each ldrCoco As DataRow In mdsDatos.Tables(mstrTablaConcep).Select("coco_id=" & lstrCocoId)
    ldrCoco.Item("coco_impo") -= ldecImpo

    If ldrCoco.Item("coco_impo") = 0 Then
     ldrCoco.Delete()
    End If
   Next
  End If
 End Sub

 Public Sub mItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
  Try
   Select Case e.Item.ItemType
    Case ListItemType.Header
     mdecGriTotal = 0
    Case ListItemType.AlternatingItem, ListItemType.Item
     mdecGriTotal += CType(e.Item.DataItem, DataRowView).Row.Item("copc_impo")
    Case ListItemType.Footer
     e.Item.Cells(ColumnasDeta.Importe).Text = mdecGriTotal.ToString("######0.00")
   End Select

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub

 Public Sub mItemDataBoundFact(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
  Try
   Select Case e.Item.ItemType
    Case ListItemType.Header
     mdecGriTotal = 0
    Case ListItemType.AlternatingItem, ListItemType.Item
     With CType(e.Item.DataItem, DataRowView).Row
      mdecGriTotal += .Item("coco_impo")
      'solo permite borrar conceptos manuales
      e.Item.FindControl("lnkEdit").Visible = .Item("_auto") = 0
     End With
    Case ListItemType.Footer
     e.Item.Cells(ColumnasFact.Importe).Text = mdecGriTotal.ToString("######0.00")
   End Select

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub

 Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
  mAlta()
 End Sub

#Region "Conceptos Manuales"
 Private Sub mActualizarConc()
  Try
   mGuardarDatosConc()

   mConsultarConceptos()

   mLimpiarConc()

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub


 Private Sub mBajaConc()
  Try
   mdsDatos.Tables(mstrTablaConcep).Select("coco_id=" & hdnConcId.Text)(0).Delete()

   mConsultarConceptos()

   mLimpiarConc()

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub

        Private Sub mGuardarDatosConc()
            mValidarDatosConc()

            Dim ldrDatos As DataRow

            If hdnConcId.Text = "" Then
                ldrDatos = mdsDatos.Tables(mstrTablaConcep).NewRow
                ldrDatos.Item("coco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaConcep), "coco_id")
            Else
                ldrDatos = mdsDatos.Tables(mstrTablaConcep).Select("coco_id=" & hdnConcId.Text)(0)
            End If

            With ldrDatos
                .Item("coco_comp_id") = mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_id")
                .Item("coco_conc_id") = IIf(cmbConc.Valor.Length > 0, cmbConc.Valor, DBNull.Value)
                .Item("coco_ccos_id") = IIf(cmbCcos.Valor.Length > 0, cmbCcos.Valor, DBNull.Value)
                .Item("coco_impo") = txtImpo.Valor
                .Item("coco_impo_ivai") = .Item("coco_impo")
                .Item("_desc") = cmbConc.SelectedItem.Text
                .Item("_auto") = 0

                If Not cmbCcos.Valor Is DBNull.Value Then
                    .Item("_desc") += cmbCcos.SelectedItem.Text
                End If

                If hdnConcId.Text = "" Then
                    .Table.Rows.Add(ldrDatos)
                End If
            End With
        End Sub

        Private Sub mValidarDatosConc()
            If cmbConc.Valor Is DBNull.Value Or cmbConc.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el concepto.")
            End If

            If hdnConcId.Text = "" AndAlso mdsDatos.Tables(mstrTablaConcep).Select("coco_conc_id=" & cmbConc.Valor.ToString).GetUpperBound(0) <> -1 Then
                Throw New AccesoBD.clsErrNeg("Concepto ya ingresado.")
            End If

            If cmbCcos.Valor Is DBNull.Value Or cmbCcos.Valor.Length = 0 Then
                If clsSQLServer.gObtenerEstruc(mstrConn, "conceptosX", cmbConc.Valor.ToString).Tables(0).Rows(0).Item("cta_resu").ToString() = "1" Then
                    Throw New AccesoBD.clsErrNeg("La cuenta del concepto es de resultado, debe indicar el centro de costo.")
                End If
            End If

            If txtImpo.Valor Is DBNull.Value Or txtImpo.Valor.Length = 0 Then
                Throw New AccesoBD.clsErrNeg("Debe seleccionar el importe.")
            End If
        End Sub

 Public Sub mEditarDatosConc(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
  Try
   With mdsDatos.Tables(mstrTablaConcep).Select("coco_id=" & e.Item.Cells(ColumnasFact.Id).Text)(0)
    hdnConcId.Text = .Item("coco_id").ToString()

    cmbCcos.Enabled = True   'Pantanettig 
    cmbConc.Valor = .Item("coco_conc_id")
    mCargarCentroCostos(True)
    'Pantanettig						
    If .Item("coco_ccos_id") Is DBNull.Value Then
     cmbCcos.Enabled = False
    Else
     cmbCcos.Valor = .Item("coco_ccos_id")
    End If
    '-----------------------
    txtImpo.Valor = .Item("coco_impo")
   End With

   mSetearEditorConc(False)

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub

 Private Sub btnAltaConc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaConc.Click
  mActualizarConc()
 End Sub

 Private Sub btnBajaConc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaConc.Click
  mBajaConc()
 End Sub

 Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiConc.Click
  mActualizarConc()
 End Sub

 Private Sub btnLimpConc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpConc.Click
  mLimpiarConc()
 End Sub

 Private Sub mLimpiarConc()
  hdnConcId.Text = ""

  cmbConc.Limpiar()
  cmbCcos.Limpiar()
  txtImpo.Text = ""

  mSetearEditorConc(True)
 End Sub

 Private Sub mSetearEditorConc(ByVal pbooAlta As Boolean)
  btnBajaConc.Enabled = Not (pbooAlta)
  btnModiConc.Enabled = Not (pbooAlta)
  btnAltaConc.Enabled = pbooAlta
 End Sub
#End Region
End Class
End Namespace
