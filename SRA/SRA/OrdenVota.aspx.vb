Namespace SRA

Partial Class OrdenVota
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents btnBusc As System.Web.UI.WebControls.ImageButton


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_OrdenVota

   Private mstrConn As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet


   Private Enum Columnas As Integer
      Id = 1
      SoinId = 2
      SocmId = 3
      chkSel = 8
      cmbResu = 9
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If (Not Page.IsPostBack) Then
            mSetearEventos()

            mCargarCombos()

            clsWeb.gInicializarControles(Me, mstrConn)

            mMostrarPanel(False)
         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mCargarCombos()
        'clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsam, "S", "@FechaFin = 'S'")
        clsWeb.gCargarRefeCmb(mstrConn, "asambleas", cmbAsam, "S", "")

        mConsultar()
   End Sub
   Private Sub mSetearEventos()
      btnAlta.Attributes.Add("onclick", "return(mProcesar());")
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del Orden de Votaci�n?')) return false;")
   End Sub


#End Region

#Region "Generacion"
   Private Sub mAlta()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatosGene()

         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla + "_proceso", ldsEstruc)
         lobjGenerico.Alta()

         mListar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         clsSQLServer.gExecute(mstrConn, mstrTabla + "_proceso_baja " + cmbAsam.Valor.ToString)

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatosGene() As DataSet
      Dim ldsEsta As DataSet = CrearDataSetGene()

      With ldsEsta.Tables(0).Rows(0)
         .Item("orvo_asam_id") = cmbAsam.Valor
      End With

      Return ldsEsta
   End Function

   Private Function CrearDataSetGene() As DataSet
      Dim ldsEsta As New DataSet
      ldsEsta.Tables.Add(mstrTabla & "_proceso")
      With ldsEsta.Tables(0)
         .Columns.Add("orvo_id", System.Type.GetType("System.Int32"))
         .Columns.Add("orvo_asam_id", System.Type.GetType("System.Int32"))
         .Columns.Add("orvo_audi_user", System.Type.GetType("System.Int32"))
         .Rows.Add(.NewRow)
      End With
      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         Dim lds As DataSet
         lstrCmd.Append("exec " + mstrTabla + "_busq")
         lstrCmd.Append(" @orvo_asam_id=")

                If cmbAsam.Valor.Length > 0 Then
                    lstrCmd.Append(cmbAsam.Valor.ToString)
                Else
                    lstrCmd.Append("-1")
                End If

                lds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

                If lds.Tables(0).Rows.Count = 0 Then
                    btnAlta.Text = "Generar Orden"
                Else
                    btnAlta.Text = "Re-Generar Orden"
                End If

                'btnAlta.Enabled = Not cmbAsam.Valor Is DBNull.Value
                'btnBaja.Enabled = lds.Tables(0).Rows.Count > 0

                If cmbAsam.Valor Is DBNull.Value Then
                    btnAlta.Enabled = False
                    btnBaja.Enabled = False
                Else
                    Dim lstrCerr As String = clsSQLServer.gCampoValorConsul(mstrConn, "asambleas_consul @asam_id=" & IIf(cmbAsam.Valor.Length > 0, cmbAsam.Valor, "-1"), "asam_cerrada")
                    Dim lstrVenc As String = clsSQLServer.gCampoValorConsul(mstrConn, "asambleas_consul @asam_id=" & IIf(cmbAsam.Valor.Length > 0, cmbAsam.Valor, "-1"), "_vencida")
                    btnAlta.Enabled = (lstrCerr.ToUpper() = "FALSE" And lstrVenc.ToUpper() = "NO")
                    btnBaja.Enabled = (lstrCerr.ToUpper() = "FALSE" And lstrVenc.ToUpper() = "NO")
                End If

                lblSociFil.Visible = lds.Tables(0).Rows.Count > 0
                usrSociFil.Visible = lds.Tables(0).Rows.Count > 0
                btnList.Visible = True 'lds.Tables(0).Rows.Count > 0

            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
   End Sub

   Private Sub cmbAsam_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAsam.SelectedIndexChanged
      usrSociFil.Limpiar()
      mConsultar()
   End Sub
#End Region

   Private Sub mCargarDatos()
      Dim lstrCmd As New StringBuilder
      Dim ds As DataSet

      lstrCmd.Append("exec orden_vota_datos_consul")
      lstrCmd.Append(" @orvo_soci_id=")
      lstrCmd.Append(usrSociFil.Valor.ToString)
      lstrCmd.Append(", @orvo_asam_id=")
      lstrCmd.Append(cmbAsam.Valor.ToString)

      ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

      If ds.Tables(0).Rows.Count > 0 Then
         txtDatos.Text = ds.Tables(0).Rows(0).Item(0).ToString
         mMostrarPanel(True)
      Else
         txtDatos.Text = ""
         Throw New AccesoBD.clsErrNeg("El socio no est� incluido!")
      End If
   End Sub

   Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub mCerrar()
      mMostrarPanel(False)
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi

      btnAlta.Visible = Not panDato.Visible
      btnBaja.Visible = Not panDato.Visible

      panFiltro.Visible = Not panDato.Visible
        btnList.Visible = True 'Not panDato.Visible And btnBaja.Enabled

      panCabecera.Visible = True

      If Not pbooVisi Then
         mdsDatos = Nothing
         Session(mstrTabla) = Nothing
      End If
   End Sub

   Private Sub usrSociFil_Cambio(ByVal sender As Object) Handles usrSociFil.Cambio
      Try
         If Not cmbAsam.Valor Is DBNull.Value And Not usrSociFil.Valor Is DBNull.Value Then
            mCargarDatos()
            usrSociFil.Limpiar()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
      Try
         mListar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mListar()
      Dim lstrRptName As String = "OrdenVota"
      Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

      lstrRpt += "&asamblea=" + cmbAsam.Valor.ToString

      lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
      Response.Redirect(lstrRpt)
   End Sub
End Class

End Namespace
