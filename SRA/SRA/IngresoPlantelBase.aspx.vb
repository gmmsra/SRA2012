Imports System.Data.SqlClient


Namespace SRA


Partial Class IngresoPlantelBase
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

	'El Dise�ador de Web Forms requiere esta llamada.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

	Protected WithEvents lblCriadorFil As System.Web.UI.WebControls.Label
	'Controles Cabecera
	'Controles Detalle
	Protected WithEvents lblPref As System.Web.UI.WebControls.Label
	'Otros
	Protected WithEvents lblFechaPrese As System.Web.UI.WebControls.Label


	'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
	'No se debe eliminar o mover.

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
		'No la modifique con el editor de c�digo.
		InitializeComponent()
	End Sub

#End Region

#Region "Definici�n de Variables"
	Private mstrTabla As String = SRA_Neg.Constantes.gTab_Plantel_Base
	Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_Plantel_Base_Detalle
	Private mstrTablaPRDT As String = SRA_Neg.Constantes.gTab_Productos
	Private mstrCmd As String
	Private mstrConn As String
	Private mdsDatos As DataSet
	Private mstrTipo As String

#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)

            mstrTipo = "A" 'Request("tipo")
			If mstrTipo = "A" Then
				panAsoc.Visible = True
				panAsocFil.Visible = True
                lblTituAbm.Text = "Ingreso Plantel Base con Asociaci�n"
			End If
			mInicializar()
			If (Not Page.IsPostBack) Then
				Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

				mSetearMaxLength()
				mSetearEventos()
				mCargarCombos(1)
				mConsultar()
				clsWeb.gInicializarControles(Me, mstrConn)
			Else
				If cmbRaza.SelectedValue.ToString <> "" And panGeneral.Visible Then
					mCargarCombos(2)
				End If
				If panDato.Visible Then
					mdsDatos = Session(mstrTabla)
				End If

			End If
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mSetearEventos()
		btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
	End Sub
	Private Sub mSetearMaxLength()

		txtRP.MaxLength = 20
		txtNombre.MaxLength = 35
	End Sub
	Private Sub mCargarCombos(ByVal pint As Integer)
		If pint = 1 Then
            'clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip_codi", "T")
			clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
			clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar", cmbAsocFil, "id", "descrip", "T")
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar", cmbAsoc, "id", "descrip", "S")
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar", cmbAsocId, "id", "descrip", "S")
			clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar", cmbRegi, "id", "descrip", "S")
			clsWeb.gCargarCombo(mstrConn, "rg_px_cargar", cmbPX, "id", "descrip", "S")
			clsWeb.gCargarCombo(mstrConn, "prefijos_cargar", cmbPref, "id", "descrip", "S")
			clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar", cmbPelaje, "id", "descrip", "S")
			clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar", cmbVari, "id", "descrip", "S")
		Else
			clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id=" & cmbRaza.SelectedValue.ToString, cmbRegi, "id", "descrip", "S")
			clsWeb.gCargarCombo(mstrConn, "rg_px_cargar @pxrz_raza_id=" & cmbRaza.SelectedValue.ToString, cmbPX, "id", "descrip", "S")
			clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & cmbRaza.SelectedValue.ToString, cmbPelaje, "id", "descrip", "S")
			clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id=" & cmbRaza.SelectedValue.ToString, cmbVari, "id", "descrip", "S")
			usrPadre.Raza = cmbRaza.Valor
			usrMadre.Raza = cmbRaza.Valor
		End If

	End Sub
	Public Sub mInicializar()
		'usrCria.Tabla = SRA_Neg.Constantes.gTab_Criadores
		usrCriaFil.Criador = True
		usrCriaFil.AutoPostback = False
		usrCriaFil.FilClaveUnica = False
		usrCriaFil.ColClaveUnica = True
		usrCriaFil.Ancho = 790
		usrCriaFil.Alto = 510
		usrCriaFil.ColDocuNume = False
		usrCriaFil.ColCUIT = True
		usrCriaFil.ColCriaNume = True
		usrCriaFil.FilCUIT = True
		usrCriaFil.FilDocuNume = True
		usrCriaFil.FilAgru = False
		usrCriaFil.FilTarjNume = False


		usrCria.Criador = True
		usrCria.AutoPostback = False
		usrCria.FilClaveUnica = False
		usrCria.ColClaveUnica = True
		usrCria.Ancho = 790
		usrCria.Alto = 510
		usrCria.ColDocuNume = False
		usrCria.ColCUIT = True
		usrCria.ColCriaNume = True
		usrCria.FilCUIT = True
		usrCria.FilDocuNume = True
		usrCria.FilAgru = False
		usrCria.FilTarjNume = False

		usrPadre.Sexo = 1
		usrPadre.cmbSexoProdExt.Enabled = False
		usrMadre.Sexo = 0
		usrMadre.cmbSexoProdExt.Enabled = False

	End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
	Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			grdDato.EditItemIndex = -1
			If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
				grdDato.CurrentPageIndex = 0
			Else
				grdDato.CurrentPageIndex = E.NewPageIndex
			End If
			mConsultar()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Public Sub DataGridDeta_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			grdDeta.EditItemIndex = -1
			If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
				grdDeta.CurrentPageIndex = 0
			Else
				grdDeta.CurrentPageIndex = E.NewPageIndex
			End If
			grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
			grdDeta.DataBind()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Public Sub mConsultar()
		Try
			mstrCmd = "exec " + mstrTabla + "_consul"
			mstrCmd = mstrCmd + " @fecha_presen_desde=" + clsSQLServer.gFormatArg(txtFechaPresenDesdeFil.Text, SqlDbType.SmallDateTime)
			mstrCmd = mstrCmd + ",@fecha_presen_hasta=" + clsSQLServer.gFormatArg(txtFechaPresenHastaFil.Text, SqlDbType.SmallDateTime)
            mstrCmd = mstrCmd + ",@plba_raza_id=" + IIf(usrCriaFil.cmbCriaRazaExt.Valor.ToString = "", "0", usrCriaFil.cmbCriaRazaExt.Valor.ToString)
			mstrCmd = mstrCmd + ",@plba_cria_id=" + IIf(usrCriaFil.Valor.ToString = "", "0", usrCriaFil.Valor.ToString)
			If mstrTipo = "A" Then
				mstrCmd = mstrCmd + ",@plba_asoc_id=" + IIf(cmbAsocFil.Valor.ToString = "", "0", cmbAsocFil.Valor.ToString)
			End If

			mstrCmd = mstrCmd + ",@plba_tipo='" & mstrTipo & "'"
			clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
			mMostrarPanel(False)
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
        btnBaja.Enabled = Not (pbooAlta)
        btnModi.Enabled = Not (pbooAlta)
        btnAlta.Enabled = pbooAlta
    End Sub
    Private Sub mSetearEditorDeta(ByVal pbooAlta As Boolean)
        btnBajaDeta.Enabled = Not (pbooAlta)
        btnModiDeta.Enabled = Not (pbooAlta)
        btnAltaDeta.Enabled = pbooAlta
    End Sub

    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(1).Text)

        mCrearDataSet(hdnId.Text)

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                txtFecha.Fecha = .Item("plba_alta_fecha").ToString
                txtFechaPresen.Fecha = .Item("plba_presen_fecha").ToString
                cmbRaza.SelectedValue = .Item("plba_raza_id").ToString
                usrCria.Valor = .Item("plba_cria_id").ToString
                cmbAsoc.SelectedValue = IIf(.Item("plba_asoc_id").ToString = "", "", .Item("plba_asoc_id").ToString)

            End With

            txtFecha.Enabled = True
            txtFechaPresen.Enabled = True
            cmbRaza.Enabled = True
            usrCria.Activo() = True
            cmbAsoc.Enabled = True

           mSetearEditor(False)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub
    Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDeta As DataRow

            hdnDetaId.Text = E.Item.Cells(1).Text
            ldrDeta = mdsDatos.Tables(mstrTablaDeta).Select("pbde_id=" & hdnDetaId.Text)(0)

            With ldrDeta
                If .Item("pbde_sexo") Then
                    cmbSexo.SelectedValue = "1"
                Else
                    cmbSexo.SelectedValue = "0"
                End If
                cmbRegi.SelectedValue = IIf(.Item("pbde_regt_id").ToString = "", "", .Item("pbde_regt_id"))
                cmbPX.SelectedValue = IIf(.Item("pbde_px_id").ToString = "", "", .Item("pbde_px_id"))
                txtRP.Text = .Item("pbde_rp")
                txtRPnume.Text = .Item("pbde_rp_nume")
                cmbPref.SelectedValue = IIf(.Item("pbde_pref_id").ToString = "", "", .Item("pbde_pref_id"))
                txtNombre.Text = .Item("pbde_nomb")
                txtFechaNaci.Text = .Item("pbde_naci_fecha")
                txtAsocNume.Text = .Item("pbde_asoc_nume").ToString
                cmbPelaje.SelectedValue = IIf(.Item("pbde_pela_id").ToString = "", "", .Item("pbde_pela_id"))
                cmbVari.SelectedValue = IIf(.Item("pbde_vari_id").ToString = "", "", .Item("pbde_vari_id"))
                usrPadre.Valor = IIf(.Item("pbde_padre_id").ToString = "", "", .Item("pbde_padre_id"))
                usrMadre.Valor = IIf(.Item("pbde_madre_id").ToString = "", "", .Item("pbde_madre_id"))
            End With

            mSetearEditorDeta(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mCrearDataSet(ByVal pstrId As String)

        If pstrId <> "" Then
            pstrId = "@plba_tipo='" & mstrTipo & "',@plba_id=" & pstrId
        End If

        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDeta

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If
        grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
        grdDeta.DataBind()

        Session(mstrTabla) = mdsDatos
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        txtFecha.Fecha = Date.Now
        mMostrarPanel(True)
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        txtFechaPresen.Text = ""
        usrCria.Limpiar()
        lblBaja.Text = ""
        cmbRaza.Limpiar()
        cmbAsoc.Limpiar()

        mLimpiarDeta()

        grdDato.CurrentPageIndex = 0
        grdDeta.CurrentPageIndex = 0

        mCrearDataSet("")
        mShowTabs(1)
        mSetearEditor(True)
    End Sub
    Private Sub mLimpiarDeta()
        hdnDetaId.Text = ""
        cmbSexo.Limpiar()
        cmbRegi.Limpiar()
        cmbPX.Limpiar()
        txtRP.Text = ""
        txtRPnume.Text = ""
        cmbPref.Limpiar()
        txtNombre.Text = ""
        txtFechaNaci.Text = ""
        txtAsocNume.Text = ""
        cmbPelaje.Limpiar()
        cmbVari.Limpiar()
        lblBajaDeta.Text = ""
        usrMadre.Limpiar()
        usrPadre.Limpiar()
        mSetearEditorDeta(True)

    End Sub
    Private Sub mLimpiarFil()
        cmbAsocFil.Limpiar()
        'cmbRazaFil.Limpiar()
        usrCriaFil.Limpiar()
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub

    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub
    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        If (pbooVisi) Then
            hdnPage.Text = " "
        Else
            hdnPage.Text = ""
        End If
        panFiltro.Visible = Not pbooVisi
        panDato.Visible = pbooVisi
        panSolapas.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Enabled = Not (panDato.Visible)
    End Sub

    Private Function mDeterminarRP(ByVal pstrRP As String) As Integer
        Dim lstrRp As String
        Dim lstrChar As String

        If pstrRP <> "" Then
            Dim i As Integer
            For i = 0 To pstrRP.Length - 1
                lstrChar = pstrRP.Substring(i, 1)
                If IsNumeric(lstrChar) Then lstrRp = lstrRp + lstrChar
            Next
        End If

        Return (lstrRp)
    End Function


#End Region

#Region "Opciones de ABM"

 Private Function mObtenerParamABMCabe(ByVal pstrProcedure As String) As String
        Dim lstrCmd As String
        Dim ldr As DataRow = mdsDatos.Tables(0).Rows(0)

        lstrCmd = "exec " + pstrProcedure


        With ldr
            lstrCmd += " @plba_alta_fecha=" & clsSQLServer.gFormatArg(.Item("plba_alta_fecha").ToString, SqlDbType.SmallDateTime)
            lstrCmd += ",@plba_fecha_presen=" & clsSQLServer.gFormatArg(.Item("plba_presen_fecha").ToString, SqlDbType.SmallDateTime)
            lstrCmd += ",@plba_raza_id=" & clsSQLServer.gFormatArg(.Item("plba_raza_id").ToString, SqlDbType.Int)
            lstrCmd += ",@plba_cria_id=" & clsSQLServer.gFormatArg(.Item("plba_cria_id").ToString, SqlDbType.Int)
            If mstrTipo = "A" Then
                lstrCmd += ",@plba_asoc_id=" & clsSQLServer.gFormatArg(.Item("plba_asoc_id").ToString, SqlDbType.Int)
            Else
                lstrCmd += ",@plba_asoc_id=null"
            End If
            lstrCmd += ",@plba_audi_user=" & Session("sUserId").ToString()
            lstrCmd += ",@plba_tipo='" & mstrTipo & "'"
        End With

        Return (lstrCmd)
 End Function
 Function mObtenerParamABMDeta(ByVal pstrProcedure As String, ByVal plDr As DataRow, ByVal pstrCabeID As String) As String
        Dim lstrCmd As String

        lstrCmd = "exec " + pstrProcedure

        With plDr
            lstrCmd += " @pbde_plba_id=" & clsSQLServer.gFormatArg(pstrCabeID, SqlDbType.Int)
            lstrCmd += ",@pbde_sexo=" & clsSQLServer.gFormatArg(.Item("pbde_sexo").ToString, SqlDbType.Bit)
            lstrCmd += ",@pbde_regt_id=" & clsSQLServer.gFormatArg(.Item("pbde_regt_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_px_id=" & clsSQLServer.gFormatArg(.Item("pbde_px_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_rp=" & clsSQLServer.gFormatArg(.Item("pbde_rp").ToString, SqlDbType.VarChar)
            lstrCmd += ",@pbde_rp_nume=" & clsSQLServer.gFormatArg(.Item("pbde_rp_nume").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_pref_id=" & clsSQLServer.gFormatArg(.Item("pbde_pref_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_nomb=" & clsSQLServer.gFormatArg(.Item("pbde_nomb").ToString, SqlDbType.VarChar)
            lstrCmd += ",@pbde_naci_fecha=" & clsSQLServer.gFormatArg(.Item("pbde_naci_fecha").ToString, SqlDbType.SmallDateTime)
            lstrCmd += ",@pbde_asoc_nume=" & clsSQLServer.gFormatArg(.Item("pbde_asoc_nume").ToString, SqlDbType.VarChar)
            lstrCmd += ",@pbde_pela_id=" & clsSQLServer.gFormatArg(.Item("pbde_pela_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_vari_id=" & clsSQLServer.gFormatArg(.Item("pbde_vari_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_prdt_id=null"
            lstrCmd += ",@pbde_padre_id=" & clsSQLServer.gFormatArg(.Item("pbde_padre_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_madre_id=" & clsSQLServer.gFormatArg(.Item("pbde_madre_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_audi_user=" & Session("sUserId").ToString()
        End With
        Return (lstrCmd)
 End Function

 Function mbtenerParamABM_PRDT(ByVal pstrProcedure As String, ByVal plDr As DataRow, ByVal pDetaID As String) As String
        Dim lstrCmd As String

        lstrCmd = "exec " + pstrProcedure
        lstrCmd += " @pbde_id=" & pDetaID
        lstrCmd += ",@pbde_presen_fecha=" & clsSQLServer.gFormatArg(txtFechaPresen.Text, SqlDbType.SmallDateTime)
        lstrCmd += ",@pbde_raza_id=" & clsSQLServer.gFormatArg(cmbRaza.SelectedValue.ToString, SqlDbType.Int)
        lstrCmd += ",@pbde_cria_id=" & clsSQLServer.gFormatArg(usrCria.Valor.ToString, SqlDbType.Int)
        If mstrTipo = "A" Then
            lstrCmd += ",@pbde_asoc_id=" & clsSQLServer.gFormatArg(cmbAsoc.SelectedValue.ToString, SqlDbType.Int)
        End If

        With plDr
            lstrCmd += ",@pbde_sexo=" & clsSQLServer.gFormatArg(.Item("pbde_sexo").ToString, SqlDbType.Bit)
            lstrCmd += ",@pbde_regt_id=" & clsSQLServer.gFormatArg(.Item("pbde_regt_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_px=" & clsSQLServer.gFormatArg(.Item("_px_desc").ToString, SqlDbType.VarChar)
            lstrCmd += ",@pbde_rp=" & clsSQLServer.gFormatArg(.Item("pbde_rp").ToString, SqlDbType.VarChar)
            lstrCmd += ",@pbde_rp_nume=" & clsSQLServer.gFormatArg(.Item("pbde_rp_nume").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_pref_id=" & clsSQLServer.gFormatArg(.Item("pbde_pref_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_nomb=" & clsSQLServer.gFormatArg(.Item("pbde_nomb").ToString, SqlDbType.VarChar)
            lstrCmd += ",@pbde_naci_fecha=" & clsSQLServer.gFormatArg(.Item("pbde_naci_fecha").ToString, SqlDbType.SmallDateTime)
            If mstrTipo = "A" Then
                lstrCmd += ",@pbde_asoc_nume=" & clsSQLServer.gFormatArg(.Item("pbde_asoc_nume").ToString, SqlDbType.VarChar)
            End If
            lstrCmd += ",@pbde_pela_id=" & clsSQLServer.gFormatArg(.Item("pbde_pela_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_vari_id=" & clsSQLServer.gFormatArg(.Item("pbde_vari_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_padre_id=" & clsSQLServer.gFormatArg(.Item("pbde_padre_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_madre_id=" & clsSQLServer.gFormatArg(.Item("pbde_madre_id").ToString, SqlDbType.Int)
            lstrCmd += ",@pbde_audi_user=" & Session("sUserId").ToString()
        End With

        Return (lstrCmd)
 End Function

 Private Sub mAlta()
        'Dim lTransac As SqlClient.SqlTransaction
        'Dim myConnection As New SqlConnection(mstrConn)

        'mGuardarDatos()

        Try
            Dim lstrCmd As String
            Dim lstrID As String
            Dim lstrDetaID As String
            Dim lstrPrdtID As String

            'Falta Fecha Presentacion
            If txtFechaPresen.Fecha.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar la Fecha de Presentaci�n en el Panel General.")
            End If

            'Falta Raza
            If cmbRaza.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar Raza en el Panel General.")
            End If

            'Falta Criador
            If usrCria.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe ingresar Criador en el Panel General.")
            End If

            'Falta Asociaci�n
            If cmbAsoc.Valor.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la Asociaci�n.")
            End If

            Dim ldsDatos As DataSet = mGuardarDatos()

            Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsDatos)
            lobjGenericoRel.Alta()

            'myConnection.Open()

            'lTransac = myConnection.BeginTransaction()

            'lstrCmd = mObtenerParamABMCabe(mstrTabla & "_alta")
            'lstrID = clsSQLServer.gExecuteScalarTransParam(lTransac, myConnection, lstrCmd)

            'For Each ldrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
            ' lstrCmd = mObtenerParamABMDeta(mstrTablaDeta & "_alta", ldrDeta, lstrID)
            ' lstrDetaID = clsSQLServer.gExecuteScalarTransParam(lTransac, myConnection, lstrCmd)

            ' 'doy de alta el producto y obtengo el ID
            ' 'y Actualizo el Registro Detalle con el ID del nuevo Producto

            ' lstrCmd = mbtenerParamABM_PRDT(mstrTablaDeta & "_" & mstrTablaPRDT & "_alta", ldrDeta, lstrDetaID)
            ' lstrPrdtID = clsSQLServer.gExecuteScalarTransParam(lTransac, myConnection, lstrCmd)

            'Next

            'lTransac.Commit()
            'myConnection.Close()

            mConsultar()
            mMostrarPanel(False)

        Catch ex As Exception
            'lTransac.Rollback()
            'myConnection.Close()
            clsError.gManejarError(Me, ex)

        End Try
 End Sub
 Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos)

            lobjGenerica.Modi()

            mConsultar()

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
 End Sub
 Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
            End If

            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
 End Sub
Private Function mGuardarDatos() As DataSet

        With mdsDatos.Tables(0).Rows(0)
            .Item("plba_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("plba_alta_fecha") = txtFecha.Fecha

            .Item("plba_presen_fecha") = txtFechaPresen.Fecha
            .Item("plba_raza_id") = cmbRaza.Valor
            .Item("plba_cria_id") = usrCria.Valor
            .Item("plba_asoc_id") = cmbAsoc.Valor

        End With

        Return (mdsDatos)

 End Function
 'Seccion de prefijos
 Private Sub mActualizarDeta()
        Try
            mGuardarDatosDeta()

            mLimpiarDeta()
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
            grdDeta.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
 End Sub
 Private Sub mBajaDeta()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDeta).Rows(hdnDetaId.Text)
            row.Delete()
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
            grdDeta.DataBind()
            mLimpiarDeta()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
 End Sub
 Private Sub mGuardarDatosDeta()
        Dim ldrDeta As DataRow

        mValidarDatosDeta()

        If hdnDetaId.Text = "" Then
            ldrDeta = mdsDatos.Tables(mstrTablaDeta).NewRow
            ldrDeta.Item("pbde_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDeta), "pbde_id")
        Else
            ldrDeta = mdsDatos.Tables(mstrTablaDeta).Select("pbde_id=" & hdnDetaId.Text)(0)
        End If

        With ldrDeta
            .Item("pbde_sexo") = IIf(cmbSexo.Valor = "1", True, False)
            .Item("_sexo") = cmbSexo.SelectedItem.Text
            .Item("pbde_regt_id") = IIf(cmbRegi.SelectedValue.ToString = "", DBNull.Value, cmbRegi.Valor)
            .Item("_regt_desc") = IIf(cmbRegi.SelectedValue.ToString = "", "", cmbRegi.SelectedItem.Text)
            .Item("pbde_px_id") = IIf(cmbPX.SelectedValue.ToString = "", DBNull.Value, cmbPX.Valor)
            .Item("_px_desc") = IIf(cmbPX.SelectedValue.ToString = "", "", cmbPX.SelectedItem.Text)
            .Item("pbde_rp") = UCase(txtRP.Valor)
            .Item("pbde_rp_nume") = mDeterminarRP(txtRP.Valor)
            .Item("pbde_pref_id") = IIf(cmbPref.SelectedValue.ToString = "", DBNull.Value, cmbPref.Valor)
            .Item("_pref") = IIf(cmbPref.SelectedValue.ToString = "", "", cmbPref.SelectedItem.Text)
            .Item("pbde_nomb") = UCase(txtNombre.Valor)
                .Item("pbde_naci_fecha") = txtFechaNaci.Fecha
                If txtAsocNume.Valor.Length > 0 Then
                    .Item("pbde_asoc_nume") = txtAsocNume.Valor
                End If
                .Item("pbde_pela_id") = IIf(cmbPelaje.SelectedValue.ToString = "", DBNull.Value, cmbPelaje.Valor)
                .Item("_pelaje") = IIf(cmbPelaje.SelectedValue.ToString = "", "", cmbPelaje.SelectedItem.Text)
                .Item("pbde_vari_id") = IIf(cmbVari.SelectedValue.ToString = "", DBNull.Value, cmbVari.Valor)
                .Item("_variedad") = IIf(cmbVari.SelectedValue.ToString = "", "", cmbVari.SelectedItem.Text)
                .Item("pbde_padre_id") = IIf(usrPadre.Valor.ToString = "", "null", usrPadre.Valor)
                .Item("pbde_madre_id") = IIf(usrMadre.Valor.ToString = "", "null", usrMadre.Valor)
            End With

        If (hdnDetaId.Text = "") Then
            mdsDatos.Tables(mstrTablaDeta).Rows.Add(ldrDeta)
        End If

 End Sub
 Private Sub mValidarDatosDeta()
        'Falta Sexo
        If cmbSexo.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar Sexo en el Panel Detalle.")
        End If

        ''Falta Registro
        'If cmbRegi.Valor.ToString = "" Then
        '	Throw New AccesoBD.clsErrNeg("Debe indicar Tipo de Registro en el Panel Detalle.")
        'End If

        ''Falta Mocho
        'If cmbPX.Valor.ToString = "" Then
        '	Throw New AccesoBD.clsErrNeg("Debe indicar Caracteristicas de Mocho en el Panel Detalle.")
        'End If

        'Falta RP
        If txtRP.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar RP en el Panel Detalle.")
        End If

        ''Falta Prefijo
        'If cmbPref.Valor.ToString = "" Then
        '	Throw New AccesoBD.clsErrNeg("Debe indicar Prefijo en el Panel Detalle.")
        'End If

        'Falta Nombre
        If txtNombre.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar Nombre en el Panel Detalle.")
        End If

        'Falta Fecha Nacimiento
        If txtFechaNaci.Fecha.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar Fecha Nacimiento en el Panel Detalle.")
        End If

        'Falta Criador
        If usrCria.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar Criador en el Panel General.")
        End If

        ''Falta Pelaje
        'If cmbPelaje.Valor.ToString = "" Then
        '	Throw New AccesoBD.clsErrNeg("Debe indicar Pelaje en el Panel Detalle.")
        'End If

        ''Falta Variedad
        'If cmbVari.Valor.ToString = "" Then
        '	Throw New AccesoBD.clsErrNeg("Debe indicar Variedad en el Panel Detalle.")
        'End If

    End Sub
#End Region

#Region "Eventos de Controles"
 Private Sub mShowTabs(ByVal Tab As Byte)
  Try
   panGeneral.Visible = False
   lnkGeneral.Font.Bold = False
   panDeta.Visible = False
   lnkDetalle.Font.Bold = False
   Select Case Tab
    Case 1
     panGeneral.Visible = True
     lnkGeneral.Font.Bold = True
    Case 2
     If cmbRaza.Valor.ToString = "" Then
      Throw New AccesoBD.clsErrNeg("Debe ingresar Raza en el Panel General.")
     End If
     panDeta.Visible = True
     lnkDetalle.Font.Bold = True
     mLimpiarDeta()

     grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
     grdDeta.DataBind()
   End Select
  Catch ex As Exception
   clsError.gManejarError(Me, ex)
   mShowTabs(1)
  End Try
 End Sub
 Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnClose.Click
  mCerrar()
 End Sub
 Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
  mAgregar()
 End Sub
 Private Sub btnBuscar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
  grdDato.CurrentPageIndex = 0
  mConsultar()
 End Sub
 Private Sub btnLimpiarFil_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
  mLimpiarFil()
 End Sub
 Private Sub btnListar_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnListar.Click
  Try
   Dim lstrRptName As String
   Dim lintFechaD As Integer
   Dim lintFechaH As Integer

   lstrRptName = "IngresoPlantelBase"

   If txtFechaPresenDesdeFil.Fecha.ToString = "" Then
    lintFechaD = 0
   Else
    lintFechaD = CType(clsFormatear.gFormatFechaString(txtFechaPresenDesdeFil.Text, "Int32"), Integer)
   End If
   If txtFechaPresenHastaFil.Fecha.ToString = "" Then
    lintFechaH = 0
   Else
    lintFechaH = CType(clsFormatear.gFormatFechaString(txtFechaPresenHastaFil.Text, "Int32"), Integer)
   End If

   Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&plba_raza_id=" + IIf(usrCriaFil.cmbCriaRazaExt.SelectedValue.ToString = "", "0", usrCriaFil.cmbCriaRazaExt.SelectedValue.ToString)
            lstrRpt += "&plba_cria_id=" + IIf(usrCriaFil.Valor.ToString = "", "0", usrCriaFil.Valor.ToString)
   lstrRpt += "&plba_asoc_id=" + IIf(cmbAsocFil.SelectedValue.ToString = "", "0", cmbAsocFil.SelectedValue.ToString)
   lstrRpt += "&fecha_presen_desde=" & lintFechaD.ToString
   lstrRpt += "&fecha_presen_hasta=" & lintFechaH.ToString
   lstrRpt += "&plba_tipo=" & mstrTipo

   lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
   Response.Redirect(lstrRpt)

  Catch ex As Exception
   clsError.gManejarError(Me, ex)
  End Try
 End Sub
 'Botones Generales
 Private Sub btnAlta_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnAlta.Click
  mAlta()
 End Sub
 Private Sub btnBaja_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnBaja.Click
  mBaja()
 End Sub
 Private Sub btnModi_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnModi.Click
  mModi()
 End Sub
 Private Sub btnLimp_Click(ByVal Sender As System.Object, ByVal e As System.EventArgs) Handles btnLimp.Click
  mLimpiar()
 End Sub
 'Botones de Detalle
 Private Sub btnAltaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
  mActualizarDeta()
 End Sub
 Private Sub btnBajaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
  mBajaDeta()
 End Sub
 Private Sub btnModiDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
  mActualizarDeta()
 End Sub
 Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
  mLimpiarDeta()
 End Sub
 'Solapas
 Private Sub lnkGeneral_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkGeneral.Click
  mShowTabs(1)
 End Sub
 Private Sub lnkDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDetalle.Click
  mShowTabs(2)
 End Sub
#End Region

End Class

End Namespace
