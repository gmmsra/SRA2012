<%@ Reference Control="~/controles/usrprodderiv.ascx" %>
<%@ Reference Control="~/controles/usrcliederiv.ascx" %>
<%@ Reference Control="~/controles/usrproducto.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ExportacionSemen" CodeFile="ExportacionSemen.aspx.vb" %>
<%@ Register TagPrefix="uc1" TagName="PROH" Src="controles/usrProducto.ascx" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultpilontScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK rel="stylesheet" type="text/css" href="stylesheet/SRA.css">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
	
	function usrCriaComp_cmbRazaCria_onchange(pRaza)
		{
		
		
	   if (document.all('txtusrCriaComp:cmbRazaCria').value!=null)
		  {		
		  		mSetearRazasPadres(pRaza,'Padre');
		  	
		  		
		  		document.all('usrProductoPadre:cmbRazaCria').value =document.all(pRaza).value;
				document.all('usrProductoPadre:cmbRazaCria').onchange();
				
				document.all("usrProductoPadre:usrCriadorFil:cmbRazaCria").disabled = true;
				document.all("txtusrProductoPadre:usrCriadorFil:cmbRazaCria").disabled = true;
				
		   }
	}	
			
	
	
	function mSetearRazasPadres(pRaza,pControl)
	{
		document.all('usrProducto' + pControl + ':usrCriadorFil:cmbRazaCria').value=	document.all(pRaza).value;
		document.all('usrProducto' + pControl + ':usrCriadorFil:cmbRazaCria').onchange();
		
	}		
	
		
	
	
	
	function mSetearRaza(pRaza,pControl)
	{
		if (document.all('usrProd:usr' + pControl + ':cmbProdRaza').value != document.all(pRaza).value)
		{
			document.all('usrProd:usr' + pControl + ':cmbProdRaza').value =document.all(pRaza).value;
			document.all('usrProd:usr' + pControl + ':cmbProdRaza').onchange();
		//	document.all('usrCriaVend:cmbRazaCria').value =document.all(pRaza).value;
		//	document.all('usrCriaVend:cmbRazaCria').onchange();
		}
	}
	function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeFil')!=null)
				{
					if (strRet!='')
					{
	 					document.all('lblNumeFil').innerHTML = strRet+':';
	 					document.all('lblNumeDesdeFil').innerHTML = strRet+' Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = strRet+' Hasta:';
	 				}
	 				else
	 				{
	 					document.all('lblNumeFil').innerHTML = 'Nro.:';
	 					document.all('lblNumeDesdeFil').innerHTML = 'Nro. Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = 'Nro. Hasta:';
	 				}
	 			}
 			}
		}
		function cmbRazaFil_change()
		{
		    if(document.all("cmbRazaFil").value!="")
		    {
				document.all("usrCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = true;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = true;
				
				document.all("usrProdFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = true;
				document.all("txtusrProdFil:cmbProdRaza").disabled = true;
				
				
			}
			else
			{
				document.all("usrCriaFil:cmbRazaCria").value = ""
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = false;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = false;
				
				document.all("usrProdFil:cmbProdRaza").value = "";
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = false;
				document.all("txtusrProdFil:cmbProdRaza").disabled = false;
			
			}
		}
		
		function usrCriaComp_onchange()
		{
			if (document.all('usrCriaComp:txtId')!=null  && document.all('usrClieProp:txtId')!=null && document.all("cmbPais")!=null)
				mBuscarPaisDefa('usrCriaComp');
			var lstrRaza = '';
			if (document.all('usrProd:usrProducto:cmbProdRaza')!=null)
				lstrRaza = document.all('usrProd:usrProducto:cmbProdRaza').value;
			else
				lstrRaza = document.all('hdnRazaId').value;
		    if (lstrRaza != '' && document.all('usrCriaComp:txtId').value != '')
			{
				var sFiltro = '@cria_id='+document.all('usrCriaComp:txtId').value+',@raza_id='+lstrRaza;
				LoadComboXML("criadores_cliente_cargar", sFiltro, "cmbCriaComp", "S");
			}
			else
				LoadComboXML("criadores_cliente_cargar", "@raza_id=0", "cmbCriaComp", "S");
		}	
		
		
	function usrProd_usrProducto_cmbProdRaza_onchange(pRaza)
	{
		var sFiltro = document.all(pRaza).value;
		if(sFiltro != "")
		{
	 
			if (document.all("usrProd:usrCria:cmbRazaCria")!=null)
			{
				document.all("usrProd:usrCria:cmbRazaCria").value = document.all(pRaza).value;
				document.all("usrProd:usrCria:cmbRazaCria").onchange();
				document.all("usrProd:usrCria:cmbRazaCria").disabled = true;
				document.all("txtusrProd:usrCria:cmbRazaCria").disabled = true;
			}

	
	
			if (document.all("usrProd:usrCria:cmbRazaCria")!=null)
			{
				document.all("usrProd:usrCria:cmbRazaCria").value = document.all(pRaza).value;
				document.all("usrProd:usrCria:cmbRazaCria").onchange();
				document.all("usrProd:usrCria:cmbRazaCria").disabled = true;
				document.all("txtusrProd:usrCria:cmbRazaCria").disabled = true;
			}
			if (document.all("usrProd:usrPadre:cmbProdRaza")!=null)
			{
				document.all("usrProd:usrPadre:cmbProdRaza").value = document.all(pRaza).value;
				document.all("usrProd:usrPadre:cmbProdRaza").onchange();
				document.all("usrProd:usrPadre:cmbProdRaza").disabled = true;
				document.all("txtusrProd:usrPadre:cmbProdRaza").disabled = true;
			}
			
		}
		else
		{
			if (document.all("usrProd:usrCria:cmbRazaCria")!=null)
			{
				document.all("usrProd:usrCria:cmbRazaCria").value = "";
				document.all("usrProd:usrCria:cmbRazaCria").onchange();
			}
			
			if (document.all("usrProd:usrPadre:cmbProdRaza")!=null)
			{	
				document.all("usrProd:usrPadre:cmbProdRaza").value = "";
				document.all("usrProd:usrPadre:cmbProdRaza").onchange();
				
				if (document.all("usrProd:usrPadre:cmbProdAsoc").disabled==false)
				{
					document.all("usrProd:usrCria:cmbRazaCria").disabled = false;
					document.all("txtusrProd:usrCria:cmbRazaCria").disabled = false;
					
					document.all("usrProd:usrPadre:cmbProdRaza").disabled = false;
					document.all("txtusrProd:usrPadre:cmbProdRaza").disabled = false;
					
				}
				
				
			}
		}
	}
	
	
		function usrProd_usrPadre_cmbProdRaza_onchange(pRaza)
	{
		mSetearRazasPadres(pRaza,'Padre');
	}
	
	


	  	
		function usrCriaComp_onchange()
		{
			if (document.all('usrCriaComp:txtCodi')!=null  && document.all('usrClieProp:txtCodi')!=null && document.all("cmbPais")!=null)
				mBuscarPaisDefa('usrCriaComp');
			var lstrRaza = '';
			if (document.all('usrProd:usrProducto:cmbProdRaza')!=null)
				lstrRaza = document.all('usrProd:usrProducto:cmbProdRaza').value;
			else
				lstrRaza = document.all('hdnRazaId').value;
		   	}	
		
		
		function mVerErrores()
		{
			var lstrProc = '<%=mintProce%>';
			var lstrId = document.all("hdnId").value;
			
			gAbrirVentanas("errores.aspx?EsConsul=0&titulo=Errores&tabla=rg_denuncias_errores&proce="+lstrProc+"&id=" + lstrId, 1, "700","400","20","40");
		}
		
		
		function mVerStockSemen()
		{
		
			var lstrCriadorId =document.all('usrProductoPadre_usrCriadorFil_txtId').value;
			var lstrCriadorNomb = document.all('usrProductoPadre_usrCriadorFil_txtApel').value;
			gAbrirVentanas("Reportes/SaldoSemenStock.aspx?criadorID=" + lstrCriadorId + "&criadorNomb=" + lstrCriadorNomb, 1, "750","550","20","40");
		}

		
		
		function btnNuevoImpEmb_click()
		{
			document.all('hdnDatosPop').value = "-1";
 	        //gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=SELECCIONE UNA PLANTILLA&tabla=rg_tramites_planXrg_tramites_tipos&filtros=<%=mintTtraId%>", 8,650,350);
			return(false);
		}
		
		function btnModi_click(pstrEstaBaja)
		{
			if(document.all("cmbEsta").value == pstrEstaBaja)
			{
				if(!confirm('Est� dando de baja el Tr�mite, el cual ser� cerrado.�Desea continuar?'))
					return(false);
			}
			return(true);
		}
		 
		
		function mNumeDenoConsul(pRaza,pFiltro)
	   	{
	   		var strFiltro = pRaza.value;
		    var strRet = LeerCamposXML("razas_especie", strFiltro, "espe_nomb_nume");
		    if (!pFiltro)
		    {
 				if (document.all('lblSraNume')!=null)
				{
					if (strRet!='')
		 				document.all('lblSraNume').innerHTML = strRet+':';
	 				else
	 					document.all('lblSraNume').innerHTML = 'Nro.:';
	 			}
	 		}
	 		else
 			{
 				if (document.all('lblNumeFil')!=null)
				{
					if (strRet!='')
					{
	 					document.all('lblNumeFil').innerHTML = strRet+':';
	 					document.all('lblNumeDesdeFil').innerHTML = strRet+' Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = strRet+' Hasta:';
	 				}
	 				else
	 				{
	 					document.all('lblNumeFil').innerHTML = 'Nro.:';
	 					document.all('lblNumeDesdeFil').innerHTML = 'Nro. Desde:';
	 					document.all('lblNumeHastaFil').innerHTML = 'Nro. Hasta:';
	 				}
	 			}
 			}
		}
		
			
		
		
		function cmbRazaFil_change()
		{
		    if(document.all("cmbRazaFil").value!="")
		    {
				document.all("usrCriaFil:cmbRazaCria").value = document.all("cmbRazaFil").value;
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = true;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = true;
				
				document.all("usrProdFil:cmbProdRaza").value = document.all("cmbRazaFil").value;
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = true;
				document.all("txtusrProdFil:cmbProdRaza").disabled = true;
			}
			else
			{
				document.all("usrCriaFil:cmbRazaCria").value = ""
				document.all("usrCriaFil:cmbRazaCria").onchange();
				document.all("usrCriaFil:cmbRazaCria").disabled = false;
				document.all("txtusrCriaFil:cmbRazaCria").disabled = false;
				
				document.all("usrProdFil:cmbProdRaza").value = "";
				document.all("usrProdFil:cmbProdRaza").onchange();
				document.all("usrProdFil:cmbProdRaza").disabled = false;
				document.all("txtusrProdFil:cmbProdRaza").disabled = false;
			}
			 
		}
	
		function mCargarTE()
		{
		var lstrRazaCriaId =document.all('usrRazaCriadorLocal:cmbRazaCria').value;
			if (lstrRazaCriaId=='')
			{
				lstrRazaCriaId='0';
			}
		
			gAbrirVentanas("consulta_pop.aspx?EsConsul=0&titulo=Denuncias de TE&tabla=te_denun_tramite&hdn=hdnDatosTEPop&filtros=" + lstrRazaCriaId, 2, "700","400","100","50");
		}
				
			
		
		
		function mBuscarPaisDefa(pstrCon)
		{
			if (document.all(pstrCon + ':txtCodi').value != "")
				{
					var strFiltro = "@clie_id = " + document.all(pstrCon + ':txtCodi').value;
					var strRet = LeerCamposXML("dire_clientes", strFiltro, "_dicl_pais_id");
					
					strFiltro = "@pais_defa = 1";
					var strRet2 = LeerCamposXML("paises", strFiltro, "pais_id");
					if (strRet == strRet2)
						document.all("cmbPais").value = "";
					else
						document.all("cmbPais").value = strRet;
				}
			else
				document.all("cmbPais").value = "";
		}
				
		function expandir()
		{
			if(parent.frames.length>0) try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
		</script>
	</HEAD>
	<BODY class="pagina" onunload="gCerrarVentanas();" onload="gSetearTituloFrame('');" leftMargin="5"
		rightMargin="0" topMargin="5">
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table border="0" cellSpacing="0" cellPadding="0" width="97%" align="center">
				<tr>
					<td width="9"><IMG border="0" src="imagenes/recsupiz.jpg" width="9" height="10"></td>
					<td background="imagenes/recsup.jpg"><img border="0" src="imagenes/recsup.jpg" width="9" height="10"></td>
					<td width="13"><IMG border="0" src="imagenes/recsupde.jpg" width="13" height="10"></td>
				</tr>
				<tr>
					<td background="imagenes/reciz.jpg" width="9"><img border="0" src="imagenes/reciz.jpg" width="9" height="10"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE style="WIDTH: 100%" id="Table1" border="0" cellSpacing="0" cellPadding="0">
							<TBODY>
								<TR>
									<TD style="HEIGHT: 25px" height="25" vAlign="bottom" colSpan="3"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Exportaci�n de Semen</asp:label></TD>
								</TR>
								<TR>
									<TD vAlign="middle" colSpan="3" noWrap><asp:panel id="panFiltros" runat="server" cssclass="titulo" BorderStyle="none" BorderWidth="1px"
											width="100%">
											<TABLE id="Table3" border="0" cellSpacing="0" cellPadding="0" width="100%">
												<TR>
													<TD>
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%">
															<TR>
																<TD style="HEIGHT: 8px" width="24"></TD>
																<TD style="HEIGHT: 8px" width="42"></TD>
																<TD style="HEIGHT: 8px" width="26"></TD>
																<TD style="HEIGHT: 8px"></TD>
																<TD style="HEIGHT: 8px" width="26">
																	<CC1:BotonImagen id="btnBuscar" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnCons2.gif"
																		ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/btnImpr.jpg"></CC1:BotonImagen></TD>
																<TD style="HEIGHT: 8px" vAlign="middle" width="26">
																	<CC1:BotonImagen id="btnLimpiarFil" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnLimp2.gif"
																		ImageBoton="btnLimp.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/"
																		IncludesUrl="includes/" BackColor="Transparent" ImageUrl="imagenes/limpiar.jpg"></CC1:BotonImagen></TD>
															</TR>
															<TR>
																<TD style="HEIGHT: 8px" width="24"><IMG border="0" src="imagenes/formfle.jpg" width="24" height="25"></TD>
																<TD style="HEIGHT: 8px" width="42"><IMG border="0" src="imagenes/formtxfiltro.jpg" width="113" height="25"></TD>
																<TD style="HEIGHT: 8px" width="26"><IMG border="0" src="imagenes/formcap.jpg" width="26" height="25"></TD>
																<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG border="0" src="imagenes/formfdocap.jpg" height="25"></TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
												<TR>
													<TD style="HEIGHT: 50px">
														<TABLE border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
															<TR>
																<TD background="imagenes/formiz.jpg" width="3"><IMG border="0" src="imagenes/formiz.jpg" width="3" height="17"></TD>
																<TD style="HEIGHT: 100%"><!-- FOMULARIO -->
																	<TABLE class="FdoFld" border="0" cellSpacing="0" cellPadding="0" width="100%" height="100%">
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 15%" height="15" align="right">
																				<asp:label id="lblNumeFil" runat="server" cssclass="titulo"> Nro.Tr�mite:</asp:label>&nbsp;</TD>
																			<TD style="WIDTH: 85%">
																				<CC1:NUMBERBOX id="txtNumeFil" runat="server" cssclass="cuadrotexto" MaxValor="999999999" Width="140px"
																					AceptaNull="False"></CC1:NUMBERBOX></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"></TD>
																		</TR>
																		<TR>
																			<TD height="15" align="right">
																				<asp:Label id="lblEstadoFil" runat="server" cssclass="titulo">Estado:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox id="cmbEstadoFil" class="combo" runat="server" Width="200px" AceptaNull="False"
																					NomOper="estados_cargar"></cc1:combobox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR style="DISPLAY: inline" id="rowPaisFil" runat="server">
																			<TD align="right">
																				<asp:Label id="lblPaisFil" runat="server" cssclass="titulo">Pa�s:</asp:Label>&nbsp;</TD>
																			<TD>
																				<cc1:combobox id="cmbPaisFil" class="combo" runat="server" Width="280px" nomoper="paises_cargar"
																					aceptanull="false"></cc1:combobox></TD>
																		</TR>
																		<TR style="DISPLAY: inline" id="rowCriaFil" runat="server">
																			<TD style="WIDTH: 8.91%; HEIGHT: 24px" background="imagenes/formfdofields.jpg" align="right">
																				<asp:Label id="lblCriaFil" runat="server" cssclass="titulo" Width="58px">Exportador Raza/Criador: </asp:Label>&nbsp;</TD>
																			<TD style="WIDTH: 25%; HEIGHT: 24px" background="imagenes/formfdofields.jpg" align="left">
																				<UC1:CLIE id="usrCriaFil" runat="server" AceptaNull="false" MostrarBotones="False" Tabla="Criadores"
																					Saltos="1,2" FilSociNume="True" FilTipo="S" MuestraDesc="False" FilDocuNume="True" Criador="True"
																					CampoVal="Criador" FilClaveUnica="True" ColCriaNume="True" FilAgru="False" FilTarjNume="False"
																					ColCUIT="True" FilCUIT="True"></UC1:CLIE></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD height="15" align="right">
																				<asp:label id="lblFechaDesdeFil" runat="server" cssclass="titulo">Fecha Desde:</asp:label>&nbsp;</TD>
																			<TD>
																				<cc1:DateBox id="txtFechaDesdeFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox>&nbsp;
																				<asp:label id="lblFechaHastaFil" runat="server" cssclass="titulo">Hasta:&nbsp;</asp:label>
																				<cc1:DateBox id="txtFechaHastaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		</TR>
																		<TR>
																			<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" background="../imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																		<TR>
																			<TD style="WIDTH: 153px" height="15" align="right">
																				<asp:label id="lblProdFil" runat="server" cssclass="titulo">Macho:</asp:label>&nbsp;</TD>
																			<TD>
																				<UC1:PROH id="usrProdFil" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																					FilTipo="S" MuestraDesc="false" Ancho="800" EsPropietario="True" AutoPostBack="False"></UC1:PROH></TD>
																		</TR>
																		<TR>
																			<TD background="imagenes/formfdofields.jpg" noWrap align="right"></TD>
																			<TD background="imagenes/formfdofields.jpg" width="72%">
																				<asp:checkbox id="chkVisto" Text="Incluir Registros Vistos y/o Aprobados" CssClass="titulo" Runat="server"></asp:checkbox></TD>
																		</TR>
																		<TR>
																			<TD style="HEIGHT: 10px" background="imagenes/formfdofields.jpg" colSpan="2" align="right"></TD>
																		</TR>
																	</TABLE>
																</TD>
															</TR>
														</TABLE>
													</TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<tr> 
									<td height="10" colSpan="3"></td>
								</tr>
								<TR>
									<TD vAlign="top" colSpan="3" align="right"><asp:datagrid id="grdDato" runat="server" BorderStyle="None" BorderWidth="1px" width="100%" AllowPaging="True"
											HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" AutoGenerateColumns="False" OnUpdateCommand="mEditarDatos"
											OnPageIndexChanged="grdDato_Page">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="Linkbutton5" runat="server" CausesValidation="false" CommandName="Update">
															<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />
															<!--<img src='imagenes/<%#DataBinder.Eval(Container, "DataItem.img_visto")%>' border="0" alt="<%#DataBinder.Eval(Container, "DataItem.text_alt")%>" style="cursor:hand;" />--></asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="tram_id"></asp:BoundColumn>
												<asp:BoundColumn DataField="tram_inic_fecha" HeaderText="Fecha Inic." DataFormatString="{0:dd/MM/yyyy}">
													<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="tram_nume" HeaderText="N&#186; Tr&#225;mite">
													<HeaderStyle HorizontalAlign="Center" Width="5%"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="tram_vend_clie_id" HeaderText="Vendedor">
													<HeaderStyle HorizontalAlign="Center" Width="3%"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="true" DataField="prdt_nomb" HeaderText="Macho">
													<HeaderStyle HorizontalAlign="Center" Width="25%"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn Visible="False" DataField="tram_comp_clie_id" HeaderText="Comprador">
													<HeaderStyle HorizontalAlign="Center" Width="3%"></HeaderStyle>
													<ItemStyle HorizontalAlign="Center"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="raza" HeaderText="Raza">
													<HeaderStyle HorizontalAlign="Center" Width="20%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="_cria_nume" HeaderText="Nro. Criador">
													<HeaderStyle HorizontalAlign="Center" Width="3%"></HeaderStyle>
													<ItemStyle HorizontalAlign="Right"></ItemStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="clie_apel" ReadOnly="True" HeaderText="Comprador Apellido/Raz&#243;n Social">
													<HeaderStyle Width="40%"></HeaderStyle>
												</asp:BoundColumn>
												<asp:BoundColumn DataField="esta_desc" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10" colSpan="3"></td>
								</tr>
								<TR>
									<TD vAlign="top" colSpan="3" align="left"><CC1:BOTONIMAGEN id="btnNuevoImpEmb" runat="server" BorderStyle="None" ForeColor="Transparent" ImageOver="btnNuev2.gif"
											ImageBoton="btnNuev.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" BackColor="Transparent"
											ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una nueva Exportaci�n" ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN></TD>
								</TR>
								<TR>
									<TD colSpan="3" align="center">
										<asp:panel id="panLinks" runat="server" cssclass="titulo" width="100%" Visible="False" Height="124%">
											<TABLE id="TablaSolapas" border="0" cellSpacing="0" cellPadding="0" width="100%" runat="server">
												<TR>
													<TD style="WIDTH: 0.24%"><IMG border="0" src="imagenes/tab_a.bmp" width="9" height="27"></TD>
													<TD background="imagenes/tab_b.bmp"><IMG border="0" src="imagenes/tab_b.bmp" width="8" height="27"></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_c.bmp" width="31" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkCabecera" runat="server"
															cssclass="solapa" Width="80px" Height="21px" CausesValidation="False"> Tr�mite </asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkRequ" runat="server" cssclass="solapa"
															Width="70px" Height="21px" CausesValidation="False"> Requisitos</asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkDocu" runat="server" cssclass="solapa"
															Width="70px" Height="21px" CausesValidation="False"> Documentaci�n </asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_f.bmp" width="27" height="27"></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkObse" runat="server" cssclass="solapa"
															Width="70px" Height="21px" CausesValidation="False"> Comentarios</asp:linkbutton></TD>
													<TD><!--IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"--></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkVend" runat="server" cssclass="solapa"
															Width="70px" Height="21px" Visible="False" CausesValidation="False"> Propietarios </asp:linkbutton></TD>
													<TD background="imagenes/tab_fondo.bmp"><!--IMG height="27" src="imagenes/tab_f.bmp" width="27" border="0"--></TD>
													<TD background="imagenes/tab_fondo.bmp" width="1">
														<asp:linkbutton style="TEXT-ALIGN: center; TEXT-DECORATION: none" id="lnkComp" runat="server" cssclass="solapa"
															Width="70px" Height="21px" Visible="False" CausesValidation="False"> Compradores </asp:linkbutton></TD>
													<TD width="1"><IMG border="0" src="imagenes/tab_fondo_fin.bmp" width="31" height="27"></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD colSpan="3" align="center">
										<asp:panel id="panDato" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
											width="100%" Visible="False" Height="116px">
											<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="tabDato" class="FdoFld" border="0" cellPadding="0"
												align="left">
												<TR>
													<TD height="5">
														<asp:Label id="lblTitu" runat="server" cssclass="titulo" width="100%"></asp:Label></TD>
													<TD vAlign="top" align="right">
														<asp:ImageButton id="imgClose" runat="server" ImageUrl="images\Close.bmp" ToolTip="Cerrar" CausesValidation="False"></asp:ImageButton></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 100%" colSpan="2" align="center">
														<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
															<TABLE style="WIDTH: 100%" id="tabCabecera" border="0" cellPadding="0" align="left">
																<TR>
																	<TD width="18%" noWrap align="right">
																		<asp:Label id="lblInicFecha1" runat="server" cssclass="titulo">Fecha Carga:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:DateBox id="txtInicFecha" runat="server" cssclass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																		<asp:Label id="lblFinaFecha" runat="server" cssclass="titulo">Fecha Cierre:</asp:Label>
																		<cc1:DateBox id="txtFinaFecha" runat="server" cssclass="cuadrotexto" Width="68px" Enabled="False"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD width="18%" noWrap align="right">
																		<asp:Label id="lblFechaExportacion" runat="server" cssclass="titulo">Fecha de Exportaci�n:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:DateBox id="txtFechaExportacion" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox>&nbsp;&nbsp;&nbsp;
																	</TD>
																<TR>
																	<TD noWrap align="right">
																		<asp:Label id="lblFechaTram1" runat="server" cssclass="titulo">Fecha Recepci�n Tramite:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:DateBox id="txtFechaTram" runat="server" cssclass="cuadrotexto" Width="68px"></cc1:DateBox></TD>
																</TR>
																<TR>
																	<TD width="18%" align="right">
																		<asp:Label id="lblNroControl" runat="server" cssclass="titulo">Nro. de Control:</asp:Label></TD>
																	<TD>
																		<cc1:numberbox id="txtNroControl" runat="server" cssclass="cuadrotexto" MaxValor="999999999" Width="81px"
																			Visible="true" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox></TD>
																</TR>
																<TR>
																	<TD align="right">
																		<asp:Label id="lblEsta" runat="server" cssclass="titulo">Estado:&nbsp;</asp:Label></TD>
																	<TD>
																		<cc1:combobox id="cmbEsta" class="combo" runat="server" cssclass="cuadrotexto" Width="180px" Enabled="False"></cc1:combobox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblCriaComp" runat="server" cssclass="titulo">Comprador Raza/Criador:</asp:Label>&nbsp;</TD>
																	<TD>
																		<UC1:CLIE id="usrCriaComp" runat="server" AceptaNull="False" Tabla="Criadores" Saltos="1,2"
																			FilDocuNume="True" Ancho="800" criador="true" MuestraDesc="false"></UC1:CLIE></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR>
																	<TD style="WIDTH: 10%; HEIGHT: 25px" vAlign="top" background="imagenes/formfdofields.jpg"
																		align="right">
																		<asp:Label id="lblProductoPadre" runat="server" cssclass="titulo">Macho:</asp:Label>&nbsp;</TD>
																	<TD style="WIDTH: 80%; HEIGHT: 17px" background="imagenes/formfdofields.jpg">
																		<UC1:PROH id="usrProductoPadre" runat="server" AceptaNull="false" Tabla="productos" Saltos="1,2"
																			FilTipo="S" MuestraDesc="false" Ancho="800" EsPropietario="True" AutoPostBack="False"></UC1:PROH></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
																<TR id="rowCantEmbrSemen" runat="server">
																	<TD colSpan="2">
																		<TABLE style="WIDTH: 100%" border="0" cellPadding="0" align="left">
																			<TR>
																				<TD width="18%" align="right">
																					<asp:Label id="lblDosiCantExpoSemen" runat="server" cssclass="titulo"> Dosis de Semen:</asp:Label></TD>
																				<TD>
																					<cc1:numberbox id="txtDosiCantExpoSemen" runat="server" cssclass="cuadrotexto" MaxValor="999999999"
																						Width="81px" Visible="true" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox></TD>
																			</TR>
																			<TR>
																				<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																</TR>
																<TR style="DISPLAY: inline" id="rowProp" runat="server">
																	<TD vAlign="top" align="right">
																		<asp:Label id="lblClieProp" runat="server" cssclass="titulo">Propietario Actual:&nbsp;</asp:Label></TD>
																	<TD>
																		<UC1:CLIE id="usrClieProp" runat="server" AceptaNull="true" Tabla="Clientes" Saltos="1,2" MuestraDesc="false"
																			FilDocuNume="True" Activo="False"></UC1:CLIE></TD>
																</TR>
																<TR style="DISPLAY: none" id="rowVendedor" runat="server">
																	<TD colSpan="2">
																		<TABLE style="WIDTH: 100%" border="0" cellPadding="0" align="left">
																			<TBODY>
																				<TR>
																					<TD align="right">
																						<asp:Label id="lblEXPstockCantVendedor" runat="server" cssclass="titulo">Cant. Dosis Vendedor:</asp:Label></TD>
																					<TD align="left">
																						<cc1:numberbox id="txtstockCantVendedor" runat="server" cssclass="cuadrotexto" MaxValor="999999999"
																							Width="81px" Visible="true" Enabled="False" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox></TD>
																	</TD>
																	<TD>
																		<asp:Label id="lblEXPStockCantComprador" runat="server" cssclass="titulo">Cant. Dosis Comprador:</asp:Label>
																		<cc1:numberbox id="txtstockCantComprador" runat="server" cssclass="cuadrotexto" MaxValor="999999999"
																			Width="81px" Visible="true" Enabled="False" esdecimal="False" EsPorcen="False" MaxLength="3"></cc1:numberbox></TD>
																</TR>
																<TR>
																	<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																</TR>
															</TABLE></TD>
												</TR>
												<TR style="DISPLAY: inline" id="rowExpo" runat="server">
													<TD vAlign="top" align="right">
														<asp:Label id="lblExpo" runat="server" cssclass="titulo">Despachante:&nbsp;</asp:Label></TD>
													<TD>
														<cc1:combobox id="cmbExpo" class="combo" runat="server" Width="280px"></cc1:combobox></TD>
												</TR>
												<TR style="DISPLAY: inline" id="rowPais" runat="server">
													<TD vAlign="top" align="right">
														<asp:Label id="lblPais" runat="server" cssclass="titulo">Pa�s:&nbsp;</asp:Label></TD>
													<TD>
														<cc1:combobox id="cmbPais" class="combo" runat="server" Width="280px" nomoper="paises_cargar"></cc1:combobox></TD>
												</TR>
												<TR style="DISPLAY: inline" id="rowDivPais" runat="server">
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
											</TABLE>
										</asp:panel>
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%" colSpan="2">
										<asp:panel id="panRequ" runat="server" cssclass="titulo" Width="100%" Visible="False">
											<TABLE style="WIDTH: 100%" id="tabRequ" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%" colSpan="2">
														<asp:datagrid id="grdRequ" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdRequ_Page"
															AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
															AllowPaging="True" OnEditCommand="mEditarDatosRequ">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer"></FooterStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="20px"></HeaderStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="Linkbutton3" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																			<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																		</asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn Visible="False" DataField="trad_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
																	<HeaderStyle Width="40%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="_pend" HeaderText="Pendiente"></asp:BoundColumn>
																<asp:BoundColumn DataField="_manu" HeaderText="Manual"></asp:BoundColumn>
																<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblRequRequ" runat="server" cssclass="titulo">Requisito:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:combobox id="cmbRequRequ" class="combo" runat="server" cssclass="cuadrotexto" Width="80%"
															NomOper="rg_requisitos_cargar" MostrarBotones="False" filtra="true"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right"></TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<asp:CheckBox id="chkRequPend" Text="Pendiente" CssClass="titulo" Runat="server" Checked="False"></asp:CheckBox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblRequManuTit" runat="server" cssclass="titulo">Manual:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblRequManu" runat="server" cssclass="Desc">Si</asp:Label></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblRequFinaFechaTit" runat="server" cssclass="titulo">Fecha Finalizaci�n:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<asp:Label id="lblRequFinaFecha" runat="server" cssclass="Desc"></asp:Label></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD height="30" vAlign="middle" colSpan="2" align="center">
														<asp:Button id="btnAltaRequ" runat="server" cssclass="boton" Width="100px" Text="Agregar Req."></asp:Button>&nbsp;
														<asp:Button id="btnBajaRequ" runat="server" cssclass="boton" Width="100px" Text="Eliminar Req."></asp:Button>&nbsp;
														<asp:Button id="btnModiRequ" runat="server" cssclass="boton" Width="100px" Text="Modificar Req."></asp:Button>&nbsp;
														<asp:Button id="btnLimpRequ" runat="server" cssclass="boton" Width="100px" Text="Limpiar Req."></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel>
									</TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%" vAlign="top" colSpan="2" align="center">
										<asp:panel id="panDocu" runat="server" cssclass="titulo" Width="100%" Visible="False">
											<TABLE style="WIDTH: 100%" id="tabDocu" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%" colSpan="2" align="center">
														<asp:datagrid id="grdDocu" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdDocu_Page"
															OnUpdateCommand="mEditarDatosDocu" AutoGenerateColumns="False" CellPadding="1" GridLines="None"
															CellSpacing="1" HorizontalAlign="Center" AllowPaging="True">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer"></FooterStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="15px"></HeaderStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="Linkbutton2" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
																			<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
																		</asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn Visible="False" DataField="trdo_id" HeaderText="trdo_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="trdo_path" HeaderText="Documento">
																	<HeaderStyle Width="50%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="trdo_refe" HeaderText="Referencia">
																	<HeaderStyle Width="50%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblDocuDocu" runat="server" cssclass="titulo">Documento:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtDocuDocu" runat="server" cssclass="cuadrotexto" Width="340px" ReadOnly="True"></CC1:TEXTBOXTAB><IMG style="CURSOR: hand" id="imgDelDocuDoc" onclick="javascript:mLimpiarPath('txtDocuDocu','imgDelDocuDoc');"
															alt="Limpiar" src="imagenes\del.gif" runat="server">&nbsp;<BR>
														<INPUT style="WIDTH: 340px; HEIGHT: 22px" id="filDocuDocu" name="File1" size="49" type="file"
															runat="server">
													</TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblDocuObse" runat="server" cssclass="titulo">Referencia:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtDocuObse" runat="server" cssclass="cuadrotexto" Width="80%" AceptaNull="False"
															Height="41px" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD height="34" vAlign="middle" colSpan="2" align="center">
														<asp:Button id="btnAltaDocu" runat="server" cssclass="boton" Width="110px" Text="Agregar Docum."></asp:Button>&nbsp;
														<asp:Button id="btnBajaDocu" runat="server" cssclass="boton" Width="110px" Text="Eliminar Docum."
															Visible="true"></asp:Button>&nbsp;
														<asp:Button id="btnModiDocu" runat="server" cssclass="boton" Width="110px" Text="Modificar Docum."
															Visible="true"></asp:Button>&nbsp;
														<asp:Button id="btnLimpDocu" runat="server" cssclass="boton" Width="110px" Text="Limpiar Docum."
															Visible="true"></asp:Button>&nbsp;&nbsp;<BUTTON style="WIDTH: 110px; FONT-WEIGHT: bold" id="btnDescDocu" class="boton" onclick="javascript:mDescargarDocumento('hdnDocuId','tramites_docum','txtDocuDocu');"
															runat="server" value="Descargar">Descargar</BUTTON></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 100%" colSpan="2">
										<asp:panel id="panObse" runat="server" cssclass="titulo" Width="100%" Visible="False">
											<TABLE style="WIDTH: 100%" id="tabObse" border="0" cellPadding="0" align="left">
												<TR>
													<TD style="WIDTH: 100%" colSpan="2">
														<asp:datagrid id="grdObse" runat="server" width="100%" BorderWidth="1px" BorderStyle="None" OnPageIndexChanged="grdObse_Page"
															AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
															AllowPaging="True" OnEditCommand="mEditarDatosObse">
															<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
															<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
															<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
															<FooterStyle CssClass="footer"></FooterStyle>
															<Columns>
																<asp:TemplateColumn>
																	<HeaderStyle Width="20px"></HeaderStyle>
																	<ItemTemplate>
																		<asp:LinkButton id="Linkbutton4" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit">
																			<img src="images/edit.gif" border="0" alt="Editar Registro" style="cursor:hand;" />
																		</asp:LinkButton>
																	</ItemTemplate>
																</asp:TemplateColumn>
																<asp:BoundColumn Visible="False" DataField="trao_id"></asp:BoundColumn>
																<asp:BoundColumn DataField="trao_fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center"
																	ItemStyle-HorizontalAlign="Center" HeaderText="Fecha"></asp:BoundColumn>
																<asp:BoundColumn DataField="_requ_desc" HeaderText="Requisito">
																	<HeaderStyle Width="50%"></HeaderStyle>
																</asp:BoundColumn>
																<asp:BoundColumn DataField="trao_obse" HeaderText="Comentarios">
																	<HeaderStyle Width="50%"></HeaderStyle>
																</asp:BoundColumn>
															</Columns>
															<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
														</asp:datagrid></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblObseFecha" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:DateBox id="txtObseFecha" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblObseRequ" runat="server" cssclass="titulo">Requisito:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<cc1:combobox id="cmbObseRequ" class="combo" runat="server" cssclass="cuadrotexto" Width="80%"
															NomOper="rg_requisitos_cargar" MostrarBotones="False" filtra="true"></cc1:combobox></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD style="WIDTH: 25%; HEIGHT: 14px" background="imagenes/formfdofields.jpg" align="right">
														<asp:Label id="lblObseObse" runat="server" cssclass="titulo">Comentario:</asp:Label>&nbsp;</TD>
													<TD style="WIDTH: 75%; HEIGHT: 14px" background="imagenes/formfdofields.jpg">
														<CC1:TEXTBOXTAB id="txtObseObse" runat="server" cssclass="cuadrotexto" Width="80%" Height="41px"
															TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
												</TR>
												<TR>
													<TD height="2" background="imagenes/formdivmed.jpg" colSpan="2" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
												</TR>
												<TR>
													<TD height="30" vAlign="middle" colSpan="2" align="center">
														<asp:Button id="btnAltaObse" runat="server" cssclass="boton" Width="100px" Text="Agregar Com."></asp:Button>&nbsp;
														<asp:Button id="btnBajaObse" runat="server" cssclass="boton" Width="100px" Text="Eliminar Com."></asp:Button>&nbsp;
														<asp:Button id="btnModiObse" runat="server" cssclass="boton" Width="100px" Text="Modificar Com."></asp:Button>&nbsp;
														<asp:Button id="btnLimpObse" runat="server" cssclass="boton" Width="100px" Text="Limpiar Com."></asp:Button></TD>
												</TR>
											</TABLE>
										</asp:panel></TD>
								</TR>
							</TBODY>
						</TABLE>
						</asp:panel>
						<DIV style="DISPLAY: inline" id="divgraba" runat="server">
							<ASP:PANEL id="panBotones" Runat="server">
								<TABLE width="100%">
									<TR>
										<TD align="center">
											<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
									</TR>
									<TR height="30">
										<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
											<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnBaja" runat="server" cssclass="boton" Width="80px" Text="Baja" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnModi" runat="server" cssclass="boton" Width="80px" Text="Modificar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnLimp" runat="server" cssclass="boton" Width="80px" Text="Limpiar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnErr" runat="server" cssclass="boton" Width="80px" Text="Ver Errores" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
											<asp:Button id="btnSemen" runat="server" cssclass="boton" Width="110px" Text="Stock Semen" Visible="false"
												CausesValidation="False"></asp:Button></TD>
									</TR>
								</TABLE>
							</ASP:PANEL>
						</DIV>
						<DIV style="DISPLAY: none" id="divproce" runat="server">
							<asp:panel id="panproce" runat="server" cssclass="titulo" width="100%">
<asp:Image id="Image1" runat="server" ImageUrl="imagenes/rptproxy.gif" AlternateText="Procesando datos"></asp:Image>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
							</asp:panel>
						</DIV>
					</td>
				</tr>
			</table> <!--- FIN CONTENIDO ---> </TD>
			<td background="imagenes/recde.jpg" width="13"><IMG border="0" src="imagenes/recde.jpg" width="13" height="10"></td>
			</TR>
			<tr>
				<td width="9"><IMG border="0" src="imagenes/recinfiz.jpg" width="9" height="15"></td>
				<td background="imagenes/recinf.jpg"><IMG border="0" src="imagenes/recinf.jpg" width="13" height="15"></td>
				<td width="13"><IMG border="0" src="imagenes/recinfde.jpg" width="13" height="15"></td>
			</tr>
			</TBODY></TABLE> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnRazaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnRequId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDetaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnDocuId" runat="server"></asp:textbox>
				<asp:textbox id="hdnObseId" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosPop" runat="server" AutoPostBack="True"></asp:textbox>
				<asp:textbox id="hdnCompId" runat="server"></asp:textbox>
				<asp:textbox id="lblAltaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnMultiProd" runat="server"></asp:textbox>
				<asp:textbox id="hdnTramNro" runat="server"></asp:textbox>
				<asp:textbox id="hdnCompOrig" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaComp" runat="server"></asp:textbox>
				<asp:textbox id="hdnVendOrig" runat="server"></asp:textbox>
				<asp:textbox id="hdnMsgError" runat="server"></asp:textbox>
				<asp:textbox id="hdnDatosTEPop" runat="server"></asp:textbox>
				<asp:textbox id="hdnTEId" runat="server"></asp:textbox><asp:textbox id="hdnOperacion" runat="server"></asp:textbox></DIV>
			<script language="JavaScript">
				function mPorcPeti()
				{
					document.all("divgraba").style.display ="none";
					document.all("divproce").style.display ="inline";
				}			
				if (document.all('lblAltaId')!=null && document.all('lblAltaId').value!='')
				{
					alert('Se ha asignado el nro de tramite '+document.all('lblAltaId').value);
					document.all('lblAltaId').value = '';
				}
				if ('<%=mintTtraId%>'=='11' || '<%=mintTtraId%>'=='30' || '<%=mintTtraId%>'=='31')
				{
					if (document.all('usrProd:usrProducto:txtSexoId')!=null)
						document.all('usrProd:usrProducto:txtSexoId').value='1';
				}
			</script>
		</form>
	</BODY>
</HTML>
