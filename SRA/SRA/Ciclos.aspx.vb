Namespace SRA

Partial Class Ciclos
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Ciclos
   Private mstrTablaCicloMate As String = SRA_Neg.Constantes.gTab_CiclosMaterias
   Private mstrConn As String
   Private mstrParaPageSize As Integer
   Private mdsDatos As DataSet
   Private mintInse As Integer
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      mintInse = Convert.ToInt32(Request.QueryString("fkvalor"))
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            mSetearEventos()
            mSetearMaxLength()

            txtAnio.Valor = Today.Year
            txtAnioFil.Valor = Today.Year
            mCargarCombos()
            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
   Private Sub mSetearMaxLength()
      Dim lstrLong As Object

      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtAnio.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cicl_anio")
      txtCiclo.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "cicl_ciclo")
   End Sub
   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "turnos_curso", cmbTucu, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "carreras", cmbCarr, "S", "@carr_inse_id = " + mintInse.ToString())
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnBaja.Enabled = Not (pbooAlta)
      btnModi.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub
   Public Sub mCargarDatos(ByVal pstrId As String)
      mCrearDataSet(pstrId)
      mConsultarMaterias()

      With mdsDatos.Tables(mstrTabla).Rows(0)
         hdnId.Text = .Item("cicl_id").ToString()
         txtAnio.Valor = .Item("cicl_anio")
         txtCiclo.Valor = .Item("cicl_ciclo")
         cmbCarr.Valor = .Item("cicl_carr_id")
         txtCursInicFecha.Fecha = .Item("cicl_curs_inic_fecha")
         txtCursFinaFecha.Fecha = .Item("cicl_curs_fina_fecha")
         cmbTucu.Valor = .Item("cicl_tucu_id")

         If Not .IsNull("cicl_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("cicl_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If

         cmbCarr.Enabled = grdMate.Items.Count = 0
      End With

      lblTitu.Text = "Registro Seleccionado: " + txtAnio.Text + " - " + txtCiclo.Text + " - " + cmbCarr.SelectedItem.Text

      mSetearEditor("", False)
      mMostrarPanel(True)
   End Sub
   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @cicl_anio=" + clsSQLServer.gFormatArg(txtAnioFil.Valor, SqlDbType.Int))
         lstrCmd.Append(", @cicl_inse_id=" + mintInse.ToString)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      mMostrarPanel(True)
   End Sub
   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""

      txtAnio.Text = Today.Year
      cmbCarr.Limpiar()
      cmbCarr.Enabled = True
      txtCiclo.Text = ""
      txtCursInicFecha.Text = ""
      txtCursFinaFecha.Text = ""
      cmbTucu.Limpiar()

      lblBaja.Text = ""

      grdMate.CurrentPageIndex = 0

      mCrearDataSet("")

      mSetearEditor("", True)
      mShowTabs(1)
   End Sub
   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub
   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         mShowTabs(1)

         grdDato.DataSource = Nothing
         grdDato.DataBind()
      Else
         Session(mstrTabla) = Nothing
         mdsDatos = Nothing
      End If

      tabLinks.Visible = pbooVisi
   End Sub
   Private Sub mShowTabs(ByVal origen As Byte)
      panDato.Visible = True
      panBotones.Visible = True
      panMate.Visible = False
      panCabecera.Visible = False

      lnkCabecera.Font.Bold = False
      lnkMate.Font.Bold = False

      Select Case origen
         Case 1
            panCabecera.Visible = True
            lnkCabecera.Font.Bold = True
            lblTitu.Text = "Datos del Ciclo "
         Case 2
            panMate.Visible = True
            lnkMate.Font.Bold = True
            lblTitu.Text = "Materias del Ciclo: " & txtAnio.Text + "-" + txtCiclo.Text + "-" + cmbCarr.SelectedItem.Text
            grdMate.Visible = True

            If grdMate.Items.Count = 0 And Not cmbCarr.Valor Is DBNull.Value Then
               mCargarTablaMaterias()
               mCompletarMaterias(False, hdnId.Text)
               mConsultarMaterias()
               cmbCarr.Enabled = grdMate.Items.Count = 0
            End If
      End Select
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         lobjGenericoRel.Alta()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mModi()
      Try
         mGuardarDatos()

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         lobjGenericoRel.Modi()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
        Private Sub mGuardarDatos()

            mValidarDatos()

            With mdsDatos.Tables(mstrTabla).Rows(0)
                .Item("cicl_anio") = IIf(txtAnio.Valor.Length > 0, txtAnio.Valor, DBNull.Value)
                .Item("cicl_carr_id") = IIf(cmbCarr.Valor.Length > 0, cmbCarr.Valor, DBNull.Value)
                .Item("cicl_ciclo") = IIf(txtCiclo.Valor.Length > 0, txtCiclo.Valor, DBNull.Value)
                .Item("cicl_curs_inic_fecha") = IIf(txtCursInicFecha.Fecha.Length > 0, txtCursInicFecha.Fecha, DBNull.Value)
                .Item("cicl_curs_fina_fecha") = IIf(txtCursFinaFecha.Fecha.Length > 0, txtCursFinaFecha.Fecha, DBNull.Value)
                .Item("cicl_inse_id") = IIf(mintInse.ToString.Length > 0, mintInse, DBNull.Value)
                .Item("cicl_tucu_id") = IIf(cmbTucu.Valor.Length > 0, cmbTucu.Valor, DBNull.Value)
                .Item("cicl_baja_fecha") = DBNull.Value
            End With

            mGuardarDatosCima()
        End Sub
   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrTablaCicloMate

      With mdsDatos.Tables(mstrTabla).Rows(0)
         If .IsNull("cicl_id") Then
            .Item("cicl_id") = -1
         End If
      End With

      mConsultarMaterias()
      mCompletarMaterias(True, pstrId)

      Session(mstrTabla) = mdsDatos
   End Sub
   Public Sub mCargarTablaMaterias()
      Dim ldsCiMa As DataSet
      Dim ldrCima As DataRow

      ldsCiMa = clsSQLServer.gObtenerEstruc(mstrConn, mstrTablaCicloMate, "@carr_id=" & cmbCarr.Valor.ToString)

      For Each ldrOri As DataRow In ldsCiMa.Tables(0).Select
         ldrCima = mdsDatos.Tables(mstrTablaCicloMate).NewRow
         With ldrCima
            .ItemArray = ldrOri.ItemArray

            .Item("cima_id") = clsSQLServer.gObtenerId(.Table, "cima_id")

            .Table.Rows.Add(ldrCima)
         End With
      Next
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Detalle"
   Private Sub grdMate_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles grdMate.ItemDataBound
      Try
         If e.Item.ItemIndex <> -1 Then
            Dim oFechaInic As NixorControls.DateBox = e.Item.FindControl("txtInicFecha")
            Dim oFechaFina As NixorControls.DateBox = e.Item.FindControl("txtFinaFecha")

            With CType(e.Item.DataItem, DataRowView).Row
               oFechaInic.Fecha = .Item("cima_inic_fecha")
               oFechaFina.Fecha = .Item("cima_fina_fecha")
            End With
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Sub mGuardarDatosCima()
      Dim ldrDatos As DataRow
      Dim ldrCicl As DataRow = mdsDatos.Tables(mstrTabla).Rows(0)

      If grdMate.Items.Count = 0 And Not cmbCarr.Valor Is DBNull.Value Then
         mCargarTablaMaterias()
         mCompletarMaterias(True, hdnId.Text)
         mConsultarMaterias()
         cmbCarr.Enabled = grdMate.Items.Count = 0
      Else
         mCompletarMaterias(True, hdnId.Text)
      End If

      For Each oItem As DataGridItem In grdMate.Items
         ldrDatos = mdsDatos.Tables(mstrTablaCicloMate).Select("cima_id=" & oItem.Cells(0).Text)(0)

         With ldrDatos
            .Item("cima_inic_fecha") = DirectCast(oItem.FindControl("txtInicFecha"), NixorControls.DateBox).Fecha
            .Item("cima_fina_fecha") = DirectCast(oItem.FindControl("txtFinaFecha"), NixorControls.DateBox).Fecha

            If .IsNull("cima_inic_fecha") Then
               Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de inicio para la materia " & .Item("_mate_desc"))
            End If

            If .IsNull("cima_fina_fecha") Then
               Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de inicio para la materia " & .Item("_mate_desc"))
            End If

            If .Item("cima_inic_fecha") > .Item("cima_fina_fecha") Then
               Throw New AccesoBD.clsErrNeg("La fecha de inicio debe ser menor que la fecha de finalización. Materia " & .Item("_mate_desc"))
            End If

            If .Item("cima_inic_fecha") < ldrCicl.Item("cicl_curs_inic_fecha") Then
               Throw New AccesoBD.clsErrNeg("La fecha de inicio debe ser mayor que la fecha de inicio del ciclo. Materia " & .Item("_mate_desc"))
            End If

            If .Item("cima_fina_fecha") > ldrCicl.Item("cicl_curs_fina_fecha") Then
               Throw New AccesoBD.clsErrNeg("La fecha de finalización debe ser menor que la fecha de finalización del ciclo. Materia " & .Item("_mate_desc"))
            End If
         End With
      Next
   End Sub
   Private Sub mConsultarMaterias()
      grdMate.DataSource = mdsDatos.Tables(mstrTablaCicloMate)
      grdMate.DataBind()
   End Sub
   Private Sub mCompletarMaterias(ByVal pbooObtenerDeDataset As Boolean, ByVal pstrId As String)
      For Each lDr As DataRow In mdsDatos.Tables(mstrTablaCicloMate).Select
         If lDr.Item("cima_id") = -1 Then
            lDr.Item("cima_id") = clsSQLServer.gObtenerId(lDr.Table, "cima_id")
         End If

         If lDr.IsNull("cima_inic_fecha") And pstrId = "" Then
            If pbooObtenerDeDataset Then
               lDr.Item("cima_inic_fecha") = mdsDatos.Tables(mstrTabla).Rows(0).Item("cicl_curs_inic_fecha")
            Else
               lDr.Item("cima_inic_fecha") = txtCursInicFecha.Fecha
            End If
         End If

         If lDr.IsNull("cima_fina_fecha") And pstrId = "" Then
            If pbooObtenerDeDataset Then
               lDr.Item("cima_fina_fecha") = mdsDatos.Tables(mstrTabla).Rows(0).Item("cicl_curs_fina_fecha")
            Else
               lDr.Item("cima_fina_fecha") = txtCursFinaFecha.Fecha
            End If
         End If
      Next
   End Sub
#End Region

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub
   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub
   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub
   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub
   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub
   Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
      mShowTabs(1)
   End Sub
   Private Sub lnkmate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkMate.Click
      mShowTabs(2)
   End Sub
   Private Overloads Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      Try
         mAgregar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
End Class
End Namespace
