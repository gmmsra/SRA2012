<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="PROD" Src="controles/usrProdDeriv.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Register TagPrefix="uc1" TagName="CLIE" Src="controles/usrClieDeriv.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.Importadores" CodeFile="Importadores.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Importadores</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultpilontScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
		<script language="JavaScript">
		function expandir()
		{
			try{ parent.frames("menu").CambiarExp();}catch(e){;}
		}
	    function mConsultarCriador(pCria, pRaza, txtId, txtDesc)
	    {
	   	  if (pCria.value != '' && pRaza.value != '')
		  {
			 var vstrRet = LeerCamposXML("CriadoresBuscar", "@raza_id="+pRaza.value+",@cria_nume="+pCria.value, "").split("|");
			 if(vstrRet!='')
			 {
			 	 txtId.value = vstrRet[0];
			 	 txtDesc.value = vstrRet[1];				
			 }
			 else
			 {
				 txtId.value = '';
				 txtDesc.value = '';
			 }
		  }
		  else
		  {
			 txtId.value = '';
			 txtDesc.value = '';
		  }
	    }
	    function mCargarProvincias(pstrTipo)
		{
		    var sFiltro = "0" + document.all("cmb" + pstrTipo + "DirePais").value;
		    LoadComboXML("provincias_cargar", sFiltro, "cmb" + pstrTipo + "DirePcia", "S");
			if (document.all("cmb" + pstrTipo + "DirePais").value == '<%=Session("sPaisDefaId")%>')
				document.all('hdnValCodPostalPais').value = 'S';
			else
				document.all('hdnValCodPostalPais').value = 'N';		    
			document.all("cmb" + pstrTipo + "DirePcia").onchange();
		}
		
		function btnLoca_click(pstrTipo)
	   	{
	   	  if (document.all("cmb" + pstrTipo + "DirePais").value == "" || document.all("cmb" + pstrTipo + "DirePcia").value == "" )
		   {
		    alert('Debe seleccionar Pa�s y Provincia.')
	       }	  
		  else	
	      {
	   	   gAbrirVentanas("localidades.aspx?EsConsul=1&t=popup&tipo=" + pstrTipo + "&pa=" + document.all("cmb" + pstrTipo + "DirePais").value + "&pr=" + document.all("cmb" + pstrTipo + "DirePcia").value, 1);
		  }
		 
		}
		
		function mCargarLocalidades(pstrTipo)
		{
		    var sFiltro = "0" + document.all("cmb" + pstrTipo + "DirePcia").value 
		    LoadComboXML("localidades_cargar", sFiltro, "cmb" + pstrTipo + "DireLoca", "S");
		}
		
		function mSelecCP(pstrTipo)
		{	
			document.all["txt" + pstrTipo + "DireCP"].value = LeerCamposXML("localidades", document.all["cmb" + pstrTipo + "DireLoca"].value, "loca_cpos");
		}	
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0" >
		<form id="frmABM" onsubmit="gCerrarVentanas();" method="post" runat="server" >
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" cellSpacing="0" cellPadding="0" border="0" width="100%">
							<TBODY>
								<TR>
									<TD width="100%" colSpan="3"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px; HEIGHT: 25px" height="25"></TD>
									<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="2" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion"> Importadores</asp:label></TD>
								</TR>
								<TR>
									<TD height="10" style="WIDTH: 2px"></TD>
									<TD vAlign="top" align="right" colSpan="2" height="10"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px"></TD>
									<TD vAlign="top" align="left" colSpan="2"><asp:datagrid id="grdDato" runat="server" BorderWidth="1px" BorderStyle="None" width="99%" AutoGenerateColumns="False"
											OnUpdateCommand="mEditarDatos" OnPageIndexChanged="DataGrid_Page" CellPadding="1" GridLines="None" CellSpacing="1" HorizontalAlign="Center"
											AllowPaging="True">
											<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
											<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
											<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
											<FooterStyle CssClass="footer"></FooterStyle>
											<Columns>
												<asp:TemplateColumn>
													<HeaderStyle Width="15px"></HeaderStyle>
													<ItemTemplate>
														<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Update">
															<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" />
														</asp:LinkButton>
													</ItemTemplate>
												</asp:TemplateColumn>
												<asp:BoundColumn Visible="False" DataField="impr_id" ReadOnly="True" HeaderText="Id"></asp:BoundColumn>
												<asp:BoundColumn DataField="impr_codi" HeaderText="C�digo"></asp:BoundColumn>
												<asp:BoundColumn DataField="impr_deno" HeaderText="Denominaci�n"></asp:BoundColumn>
												<asp:BoundColumn DataField="_estado" HeaderText="Estado"></asp:BoundColumn>
											</Columns>
											<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
										</asp:datagrid></TD>
								</TR>
								<tr>
									<td height="10" style="WIDTH: 2px"></td>
								</tr>
								<TR>
									<TD style="WIDTH: 2px"></TD>
									<TD vAlign="middle" width="100%">
										<TABLE id="Table4" cellPadding="0" align="left" border="0" width="100%">
											<TR>
												<TD align="left"></TD>
												<TD><CC1:BOTONIMAGEN id="btnAgre" runat="server" BorderStyle="None" ImageOver="btnNuev2.gif" ImageBoton="btnNuev.gif"
														BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/"
														BackColor="Transparent" ForeColor="Transparent" ImageUrl="imagenes/btnImpr.jpg" ToolTip="Agregar una Nuevo Turno"
														ImageDisable="btnNuev0.gif"></CC1:BOTONIMAGEN>&nbsp;</TD>
												<TD align="right">&nbsp;</TD>
											</TR>
										</TABLE>
									</TD>
									<TD align="right"></TD>
								</TR>
								<TR>
									<TD style="WIDTH: 2px" width="2"></TD>
									<TD align="center" colSpan="2">
										<DIV style="WIDTH: 99%"><asp:panel id="panDato" runat="server" cssclass="titulo" BorderWidth="1px" BorderStyle="Solid"
												width="99%" Height="116px" Visible="False">
												<TABLE style="WIDTH: 100%; HEIGHT: 106px" id="Table2" class="FdoFld" border="0" cellPadding="0"
													align="left">
													<TR>
														<TD></TD>
														<TD height="5">
															<asp:Label id="lblTitu" runat="server" cssclass="titulo"></asp:Label></TD>
														<TD vAlign="top" align="right">&nbsp;
															<asp:ImageButton id="imgClose" runat="server" ToolTip="Cerrar" ImageUrl="images\Close.bmp" CausesValidation="False"></asp:ImageButton></TD>
													</TR>
													<TR>
														<TD style="WIDTH: 100%" colSpan="3">
															<asp:panel id="panCabecera" runat="server" cssclass="titulo" Width="100%">
																<TABLE style="WIDTH: 100%" id="TableCabecera" border="0" cellPadding="0" align="left">
																	<TR>
																		<TD style="WIDTH: 119px" vAlign="middle" align="right">
																			<asp:Label id="lblCodi" runat="server" cssclass="titulo">C�digo:&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<cc1:numberbox id="txtCodi" runat="server" cssclass="cuadrotexto" Width="89px" esdecimal="False"
																				MaxValor="9999999999999" obligatorio="True"></cc1:numberbox></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px" vAlign="middle" noWrap align="right">
																			<asp:Label id="lblDeno" runat="server" cssclass="titulo">Denominaci�n:&nbsp;</asp:Label></TD>
																		<TD colSpan="2">
																			<CC1:TEXTBOXTAB id="txtDeno" runat="server" cssclass="cuadrotexto" Width="405px" obligatorio="True"
																				EnterPorTab="False" MaxLength="20"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px; HEIGHT: 20px" vAlign="middle" align="right">
																			<asp:Label style="Z-INDEX: 0" id="lblDefaDire" runat="server" cssclass="titulo">Direcci�n:</asp:Label></TD>
																		<TD style="HEIGHT: 20px" colSpan="2">
																			<CC1:TEXTBOXTAB style="Z-INDEX: 0" id="txtDire" runat="server" cssclass="cuadrotexto" Width="744px"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px; HEIGHT: 20px" vAlign="middle" align="right">
																			<asp:Label style="Z-INDEX: 0" id="lblDefaDirePais" runat="server" cssclass="titulo">Pa�s:</asp:Label></TD>
																		<TD style="HEIGHT: 20px" colSpan="2">
																			<cc1:combobox style="Z-INDEX: 0" id="cmbDefaDirePais" class="combo" runat="server" Width="272px"
																				onchange="mCargarProvincias('Defa')"></cc1:combobox>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			<asp:Label style="Z-INDEX: 0" id="lblDefaDirePcia" runat="server" cssclass="titulo" Width="40px">Provincia:</asp:Label>
																			<cc1:combobox style="Z-INDEX: 0" id="cmbDefaDirePcia" class="combo" runat="server" Width="208px"
																				onchange="mCargarLocalidades('Defa')" NomOper="provincias_cargar"></cc1:combobox></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px; HEIGHT: 30px" vAlign="middle" align="right">
																			<asp:Label style="Z-INDEX: 0" id="lblDefaDireLoca" runat="server" cssclass="titulo">Localidad:</asp:Label></TD>
																		<TD style="HEIGHT: 30px" colSpan="2">
																			<cc1:combobox style="Z-INDEX: 0" id="cmbDefaDireLoca" class="combo" runat="server" Height="22px"
																				Width="216px" onchange="mSelecCP('Defa');" NomOper="localidades_cargar"></cc1:combobox>&nbsp;&nbsp;<BUTTON style="WIDTH: 25.47%; HEIGHT: 20px" id="btnDefaLoca" class="boton" onclick="btnLoca_click('Defa');"
																				runat="server" value="Detalles">Localidades</BUTTON>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																			<asp:Label style="Z-INDEX: 0" id="lblDefaDireCP" runat="server" cssclass="titulo" Width="97px">C�digo Postal:</asp:Label>&nbsp;&nbsp;
																			<CC1:CODPOSTALBOX style="Z-INDEX: 0" id="txtDefaDireCP" runat="server" cssclass="cuadrotexto" Width="80px"></CC1:CODPOSTALBOX></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px; HEIGHT: 20px" vAlign="middle" align="right"></TD>
																		<TD style="HEIGHT: 20px" colSpan="2"></TD>
																	</TR>
																	<TR>
																		<TD height="2" background="imagenes/formdivmed.jpg" colSpan="3" align="right"><IMG src="imagenes/formdivmed.jpg" width="1" height="2"></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 119px; HEIGHT: 20px" vAlign="middle" align="right">
																			<asp:Label id="lblObse" runat="server" cssclass="titulo">Observaciones:&nbsp;</asp:Label></TD>
																		<TD style="HEIGHT: 20px" colSpan="2">
																			<CC1:TEXTBOXTAB id="txtObse" runat="server" cssclass="cuadrotexto" Height="50px" Width="405px" Obligatorio="false"
																				EnterPorTab="False" MaxLength="20" TextMode="MultiLine"></CC1:TEXTBOXTAB></TD>
																	</TR>
																	<TR>
																		<TD vAlign="middle" colSpan="3">
																			<DIV id="panServ">&nbsp;</DIV>
																		</TD>
																	</TR>
																</TABLE>
															</asp:panel></TD>
													</TR>
												</TABLE>
											</asp:panel><ASP:PANEL id="panBotones" Runat="server" Width="100%">
												<TABLE width="100%">
													<TR>
														<TD align="center">
															<asp:Label id="lblBaja" runat="server" cssclass="titulo" ForeColor="Red"></asp:Label></TD>
													</TR>
													<TR height="30">
														<TD vAlign="middle" align="center"><A id="editar" name="editar"></A>
															<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="80px" Text="Alta"></asp:Button>&nbsp;
															<asp:Button id="btnBaja" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Baja"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnModi" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Modificar"></asp:Button>&nbsp;&nbsp;
															<asp:Button id="btnLimp" runat="server" cssclass="boton" CausesValidation="False" Width="80px"
																Text="Limpiar"></asp:Button></TD>
													</TR>
												</TABLE>
											</ASP:PANEL></DIV>
									</TD>
								</TR>
							</TBODY>
						</TABLE>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table> <!----------------- FIN RECUADRO -----------------><BR>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
				<asp:textbox id="hdnId" runat="server"></asp:textbox>
				<asp:textbox id="hdnValorId" runat="server"></asp:textbox>
				<asp:textbox id="hdnModi" runat="server"></asp:textbox>
				<asp:textbox id="hdnSess" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaId" runat="server"></asp:textbox>
				<asp:textbox id="hdnCriaFilId" runat="server"></asp:textbox>
				<asp:textbox id="hdnLocaPop" runat="server" AutoPostBack="True"></asp:textbox>
			</DIV>
			<DIV style="DISPLAY: none">&nbsp;</DIV>
		</form>
		<SCRIPT language="javascript">
		//if (document.all["editar"]!= null)
			//document.location='#editar';
		gSetearTituloFrame('Importadores');
		</SCRIPT>
	</BODY>
</HTML>
