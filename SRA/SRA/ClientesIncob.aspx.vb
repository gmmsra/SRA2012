Namespace SRA

Partial Class ClientesIncob
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ClientesIncob
   Private mstrIncobDeta As String = SRA_Neg.Constantes.gTab_ClientesIncobDeta

   Private mstrConn As String
   Private mstrEtapa As String
   Private mstrTipo As String
   Public mstrTitulo As String
   Private mstrParaPageSize As Integer

   Private mdsDatos As DataSet

   Private Enum ColumnasDeta As Integer
      Id = 1
      Impo = 5
      Deuda = 6
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
      grdDeta.PageSize = Convert.ToInt32(mstrParaPageSize)

      mstrEtapa = Request("etapa")
      mstrTipo = Request("tipo")

      If mstrEtapa = "G" Then
         If mstrTipo = "S" Then
            mstrTitulo = "Socios Incobrables"
         Else
            mstrTitulo = "Clientes Incobrables"
         End If
         btnAgre.Visible = True
         panClie.Visible = True
         btnAlta.Visible = True
         btnBaja.Visible = True
         btnModi.Visible = True
         btnLimp.Visible = True
         btnGeneLista.Visible = True
         btnBajaListaTodos.Visible = True
         grdDeta.Columns(ColumnasDeta.Deuda).Visible = False
         grdDeta.Columns(ColumnasDeta.Impo).Visible = True
      Else
         If mstrTipo = "S" Then
            mstrTitulo = "Socios Incobrables - Baja"
         Else
            mstrTitulo = "Clientes Incobrables - Baja"
         End If
         usrClie.EstaId = SRA_Neg.Constantes.Estados.Socios_Suspendido_Moroso
         btnAgre.Visible = False
         panClie.Visible = False
         btnAlta.Visible = False
         btnBaja.Visible = False
         btnModi.Visible = False
         btnLimp.Visible = False
         btnGeneLista.Visible = False
         btnBajaListaTodos.Visible = False
         grdDeta.Columns(0).Visible = False
         grdDeta.Columns(ColumnasDeta.Deuda).Visible = True
         grdDeta.Columns(ColumnasDeta.Impo).Visible = False
      End If

      lblTituAbm.Text = mstrTitulo
      hdnEtapa.Text = mstrEtapa
   End Sub
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            Session(mstrTabla) = Nothing
            mdsDatos = Nothing

            mSetearMaxLength()
            mSetearEventos()
                    txtFecha.Fecha = Today
                    txtFecha.Enabled = False
            txtCierreFecha.Enabled = False
            txtAnioFil.Valor = Today.Year
            mCargarCombos()

            mConsultar()
            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mdsDatos = Session(mstrTabla)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnCerrar.Attributes.Add("onclick", "if(!confirm('Confirma el cierre del registro?')) return false;")
   End Sub

   Private Sub mSetearMaxLength()
      Dim lstrLong As Object
      lstrLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
      txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrLong, "lsin_obs")
   End Sub

   Private Sub mCargarCombos()
      With cmbTipo
         '.Items.Clear()
         '.Items.Add("(Todos)")
         '.Items(.Items.Count - 1).Value = ""
         If mstrTipo = "S" Then
            .Items.Add("Deuda Social")
            .Items(.Items.Count - 1).Value = "S"
            lblImpoClieDeuda.Text = "Deuda Social:"
         Else
            .Items.Add("Deuda Cta.Cte.")
            .Items(.Items.Count - 1).Value = "C"
            lblImpoClieDeuda.Text = "Deuda Cta Cte:"
         End If
      End With
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean, Optional ByVal pbooCerrado As Boolean = False)
      Select Case pstrTabla
         Case mstrIncobDeta
            btnBajaDeta.Enabled = Not (pbooAlta) And Not pbooCerrado And Not (usrClie.Valor Is DBNull.Value)
            btnModiDeta.Enabled = Not (pbooAlta) And Not pbooCerrado And Not (usrClie.Valor Is DBNull.Value)
            btnAltaDeta.Enabled = pbooAlta And Not pbooCerrado

         Case Else
            btnBaja.Enabled = Not (pbooAlta) And Not pbooCerrado
            btnModi.Enabled = Not (pbooAlta) And Not pbooCerrado
            btnCerrar.Visible = Not (pbooAlta) And Not pbooCerrado
            btnAlta.Enabled = pbooAlta And Not pbooCerrado
            btnList.Visible = Not pbooAlta
            btnListResu.Visible = Not pbooAlta

            btnGeneLista.Disabled = pbooCerrado
            btnBajaListaTodos.Enabled = Not pbooCerrado

            If mstrEtapa = "G" Then
                grdDeta.Columns(0).Visible = Not pbooCerrado
                panClie.Visible = Not pbooCerrado
            End If

      End Select
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim lintComiCant As Integer
      Dim lbooCerrada As Boolean

      mCrearDataSet(pstrId)

      With mdsDatos.Tables(mstrTabla).Rows(0)
         hdnId.Text = .Item("lsin_id").ToString()
         txtFecha.Fecha = .Item("lsin_fecha")
                txtObse.Valor = IIf(.Item("lsin_obse") Is DBNull.Value, "", .Item("lsin_obse"))

                txtCierreFecha.Fecha = IIf(.Item("lsin_cierre_fecha") Is DBNull.Value, "", .Item("lsin_cierre_fecha"))

                If mstrEtapa = "G" Then
            lbooCerrada = Not .IsNull("lsin_cierre_fecha")
         Else
            lbooCerrada = .Item("_estado") = "S"
         End If

         If Not .IsNull("lsin_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("lsin_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With

      lblTitu.Text = "Registro Seleccionado: " + CDate(txtFecha.Fecha).ToString("dd/MM/yyyy") + " - " + Left(txtObse.Text, 30)

      mSetearEditor("", False, lbooCerrada)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnBaja.Enabled = False
      btnModi.Enabled = False
      btnCerrar.Visible = False
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""
            txtFecha.Fecha = Today
            txtObse.Text = ""
      hdnSociLsinId.Text = ""
      txtCierreFecha.Text = ""

      lblBaja.Text = ""

      grdDeta.CurrentPageIndex = 0

      txtObse.Enabled = True

      mCrearDataSet("")

      mLimpiarDeta()

      mSetearEditor("", True, False)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      Dim lbooVisiOri As Boolean = panDato.Visible

      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible And mstrEtapa = "G"
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()

      Else
         Session(mstrTabla) = Nothing
         mdsDatos = Nothing
      End If

      If lbooVisiOri And Not pbooVisi Then
         txtAnioFil.Valor = Today.Year
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         mGuardarDatos()

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos.Copy)
         lobjGenericoRel.Alta()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         mGuardarDatos()

         Dim ldsTmp As DataSet = mdsDatos.Copy

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTmp)
         lobjGenericoRel.Modi()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCerrarInco()
      Try
                txtCierreFecha.Fecha = Today
                mGuardarDatos()

         Dim ldsTmp As DataSet = mdsDatos.Copy

         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsTmp)
         lobjGenericoRel.Modi()

         mListar("D")

      Catch ex As Exception
         txtCierreFecha.Text = ""
         clsError.gManejarError(Me, ex)
      End Try
   End Sub


   Private Sub mBaja()
      Try
         Dim lobjGenericoRel As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenericoRel.Baja(hdnId.Text)

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mGuardarDatos()
      mValidarDatos()

      If mdsDatos Is Nothing Then
         mdsDatos = Session(mstrTabla)
      End If

      With mdsDatos.Tables(mstrTabla).Rows(0)
                .Item("lsin_fecha") = IIf(txtFecha.Text.Trim().Length = 0, DBNull.Value, txtFecha.Fecha)
                .Item("lsin_obse") = IIf(txtObse.Text.Trim().Length = 0, DBNull.Value, txtObse.Valor)
                .Item("lsin_tipo") = IIf(mstrTipo = "S", 1, 0)
                .Item("lsin_cierre_fecha") = IIf(txtCierreFecha.Text.Trim().Length = 0, DBNull.Value, txtCierreFecha.Fecha)
                .Item("lsin_lsin_id") = IIf(hdnSociLsinId.Text = "", 0, hdnSociLsinId.Text)
         .Item("etapa") = mstrEtapa
      End With
   End Sub

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub

   Public Sub mCrearDataSet(ByVal pstrId As String)

      mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      mdsDatos.Tables(0).TableName = mstrTabla
      mdsDatos.Tables(1).TableName = mstrIncobDeta

      With mdsDatos.Tables(mstrTabla).Rows(0)
         If .IsNull("lsin_id") Then
            .Item("lsin_id") = -1
         End If
      End With

      mConsultarDeta()
      Session(mstrTabla) = mdsDatos
   End Sub

   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnCerrar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
      mCerrarInco()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub txtAnioFil_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtAnioFil.TextChanged
      Try
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
         lstrCmd.Append(" @anio=" + clsSQLServer.gFormatArg(txtAnioFil.Text, SqlDbType.Int))
         lstrCmd.Append(",@etapa=" + clsSQLServer.gFormatArg(mstrEtapa, SqlDbType.VarChar))
         lstrCmd.Append(",@tipo=" + clsSQLServer.gFormatArg(IIf(mstrTipo = "S", 1, 0), SqlDbType.Int))

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Detalle"
   Public Sub grdDeta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDeta.EditItemIndex = -1
         If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
            grdDeta.CurrentPageIndex = 0
         Else
            grdDeta.CurrentPageIndex = E.NewPageIndex
         End If
         mConsultarDeta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnBajaDetaTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaListaTodos.Click
      Try

         For Each ldrinco As DataRow In mdsDatos.Tables(mstrIncobDeta).Select
            ldrinco.Delete()
         Next

         grdDeta.CurrentPageIndex = 0
         mConsultarDeta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub hdnDatosPop_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles hdnDatosPop.TextChanged
      Try
         If (hdnDatosPop.Text <> "") Then
            Dim lvrDatos() As String = hdnDatosPop.Text.Split(Chr(5))
            Dim lvrDato() As String
            Dim ldrDatos As DataRow
            Dim ldrDeta As DataRow

            For i As Integer = 0 To lvrDatos.GetUpperBound(0)
               lvrDato = lvrDatos(i).Split(Chr(6))   'clie_id(6)soci_id(6)clie_apel(6)soci_nume(6)deuda(5)lsin(6)...

               If mdsDatos.Tables(mstrIncobDeta).Select("inco_clie_id=" + lvrDato(0)).GetUpperBound(0) = -1 Then
                  ldrDatos = mdsDatos.Tables(mstrIncobDeta).NewRow
                  ldrDatos.Item("inco_id") = clsSQLServer.gObtenerId(ldrDatos.Table, "inco_id")

                  With ldrDatos
                     .Item("inco_clie_id") = lvrDato(0)
                     .Item("_clie_apel") = lvrDato(2)
                     .Item("inco_impo") = lvrDato(4)

                     .Item("inco_fecha") = Today
                     .Item("inco_manu") = False

                     If IsNumeric(lvrDato(1)) Then
                        .Item("inco_soci_id") = lvrDato(1)
                        .Item("_soci_nume") = lvrDato(3)
                     End If

                     If IsNumeric(lvrDato(5)) Then
                        hdnSociLsinId.Text = lvrDato(5)
                     End If
                  End With

                  ldrDatos.Table.Rows.Add(ldrDatos)
               End If
            Next

            hdnDatosPop.Text = ""
            mConsultarDeta()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mConsultarDeta()
      With mdsDatos.Tables(mstrIncobDeta)
         .DefaultView.RowFilter = ""
         .DefaultView.Sort = "_clie_apel"
         grdDeta.DataSource = .DefaultView
         grdDeta.DataBind()
      End With
   End Sub

   Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try
         Dim ldrDeta As DataRow

         hdnIncoId.Text = E.Item.Cells(1).Text
         ldrDeta = mdsDatos.Tables(mstrIncobDeta).Select("inco_id=" & hdnIncoId.Text)(0)

         With ldrDeta
            usrClie.Valor = .Item("inco_clie_id")
            txtImpoClie.Valor = .Item("inco_impo")

            If Not .IsNull("inco_soci_id") Then
               hdnSociId.Text = .Item("inco_soci_id")
               txtSociNume.Valor = .Item("_soci_nume")

               hdnSociNume.Text = txtSociNume.Text
               hdnDeuda.Text = txtImpoClie.Text
            End If
         End With

         mSetearEditor(mstrIncobDeta, False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mActualizarDeta()
      Try
         mGuardarDatosDeta()

         mLimpiarDeta()
         mConsultarDeta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mNoSocio(ByVal pstrClie As String) As Boolean
        Dim lbooNoSocio As Boolean = False
        Dim lstrRet As String = ""
        Try
            lstrRet = clsSQLServer.gCampoValorConsul(mstrConn, "NoSocioActivo " + pstrClie, "NoSocio")
            Return (lstrRet = "1")
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            Return (False)
        End Try
   End Function

   Private Sub mGuardarDatosDeta()
      Dim ldrDatos As DataRow

      If usrClie.Valor Is DBNull.Value Then
         Throw New AccesoBD.clsErrNeg("Debe indicar el Cliente.")
      End If

      If mdsDatos.Tables(mstrIncobDeta).Select("inco_clie_id=" & usrClie.Valor).GetUpperBound(0) <> -1 Then
         Throw New AccesoBD.clsErrNeg("Socio existente.")
      End If

        'If hdnDeuda.Text = "" Or hdnDeuda.Text = "0" Then
        '   Throw New AccesoBD.clsErrNeg("El cliente no posee deuda.")
        'End If

      If mstrTipo <> "S" Then
         If Not mNoSocio(usrClie.Valor) Then
            Throw New AccesoBD.clsErrNeg("Solo pueden agregarse socios dados de baja.")
         End If
      End If

      If hdnIncoId.Text = "" Then
         ldrDatos = mdsDatos.Tables(mstrIncobDeta).NewRow
         ldrDatos.Item("inco_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrIncobDeta), "inco_id")
      Else
         ldrDatos = mdsDatos.Tables(mstrIncobDeta).Select("inco_id=" & hdnIncoId.Text)(0)
      End If

      With ldrDatos
         .Item("inco_clie_id") = usrClie.Valor
         .Item("inco_soci_id") = usrClie.SociId
         .Item("_clie_apel") = usrClie.Apel
         .Item("inco_impo") = hdnDeuda.Text

         .Item("inco_fecha") = Today
         .Item("inco_manu") = True

         If IsNumeric(hdnSociId.Text) Then
            .Item("inco_soci_id") = hdnSociId.Text
            .Item("_soci_nume") = hdnSociNume.Text
         End If
      End With

      If hdnIncoId.Text = "" Then
         mdsDatos.Tables(mstrIncobDeta).Rows.Add(ldrDatos)
      End If
   End Sub

   Private Sub mLimpiarDeta()
      hdnIncoId.Text = ""
      usrClie.Limpiar()
      txtImpoClie.Text = ""
      hdnSociId.Text = ""
      hdnSociNume.Text = ""
      hdnDeuda.Text = ""
      txtSociNume.Text = ""

      mSetearEditor(mstrIncobDeta, True, Not (txtCierreFecha.Fecha Is DBNull.Value))
   End Sub

   Private Sub btnLimpDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
      mLimpiarDeta()
   End Sub

   Private Sub btnBajaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
      Try
         mdsDatos.Tables(mstrIncobDeta).Select("inco_id=" & hdnIncoId.Text)(0).Delete()
         mConsultarDeta()
         mLimpiarDeta()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnAltaDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
      mActualizarDeta()
   End Sub

   Private Sub btnModiDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
      mActualizarDeta()
   End Sub
#End Region

   Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
      Try
         mAgregar()
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mListar(ByVal pstrTipo As String)
      Dim lstrRptName As String = "ClientesIncob"
      Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

      lstrRpt += "&lsin_id=" + hdnId.Text
      lstrRpt += "&tipo=" + pstrTipo  'detallado/resumido
      lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
      Response.Redirect(lstrRpt)
   End Sub

   Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnList.Click
      Try
         mListar("D")
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnListResu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnListResu.Click
      Try
         mListar("R")
      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

End Class
End Namespace
