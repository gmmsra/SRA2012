Namespace SRA

Partial Class ChequesAdmin
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   Protected WithEvents cmbCuba As NixorControls.ComboBox



   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Cheques
   Private mstrTablaMovim As String = SRA_Neg.Constantes.gTab_ChequesMovim

   Private mstrConn As String
   Private mstrParaPageSize As Integer

   Private Enum Columnas As Integer
      Id = 0
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            hdnEmctId.Text = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
            mCargarCombos()
            cmbEmctFil.Valor = hdnEmctId.Text

            ' mConsultar()
            mMostrarPanel(False)

            clsWeb.gInicializarControles(Me, mstrConn)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbEmctFil, "@ori_emct_id=" + hdnEmctId.Text)
      'clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbEmct, "@ori_emct_id=" + hdnEmctId.Text)
      clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBancFil, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "bancos", cmbBanc, "S")
      clsWeb.gCargarRefeCmb(mstrConn, "clearings", cmbClea, "")
      clsWeb.gCargarRefeCmb(mstrConn, "cheques_tipos", cmbChti, "S")
      clsWeb.gCargarComboBool(cmbPlaza, "S")
      cmbPlaza.Items.FindByValue("0").Text = "Extranjera"
      cmbPlaza.Items.FindByValue("").Text = "Local"
      cmbPlaza.Items.Remove(cmbPlaza.Items.FindByValue("1"))
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnModi.Enabled = Not (pbooAlta)
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)

      With ldsEsta.Tables(0).Rows(0)
         hdnId.Text = .Item("cheq_id").ToString()
         txtImpo.Valor = .Item("cheq_impo")
         cmbMone.Valor = .Item("cheq_mone_id")
         'cmbEmct.Valor = .Item("cheq_emct_id")
         cmbBanc.Valor = .Item("cheq_banc_id")
         usrClie.Valor = .Item("_comp_clie_id")
         txtNume.Valor = .Item("cheq_nume")
         txtReceFecha.Fecha = .Item("cheq_rece_fecha")
            '15/09/2010 txtTeorDepoFecha.Fecha = .Item("cheq_teor_depo_fecha")
                txtCheqFecha.Fecha = .Item("cheq_fecha")
                If .Item("cheq_plaza").ToString.Length > 0 Then
                    cmbPlaza.ValorBool = .Item("cheq_plaza")
                Else
                    cmbPlaza.ValorBool = False
                End If
                cmbClea.Valor = .Item("cheq_clea_id")
                cmbChti.Valor = .Item("cheq_chti_id")
            End With

      grdDetalle.Visible = False

      mSetearEditor("", False)
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""

      txtImpo.Text = ""
      cmbBanc.Limpiar()

      txtNume.Text = ""
      txtReceFecha.Text = ""
        '15/09/2010 txtTeorDepoFecha.Text = ""
        txtCheqFecha.Text = ""
      cmbPlaza.Limpiar()
      cmbClea.Limpiar()
      cmbChti.Limpiar()
      cmbMone.Limpiar()
      'cmbEmct.Limpiar()

      grdDetalle.DataSource = Nothing
      grdDetalle.DataBind()
      grdDetalle.Visible = False

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
      End If
   End Sub

   Private Sub mLimpiarFiltros()
      txtReceFechaFil.Text = ""
        '15/09/2010 txtTeorDepoFechaFil.Text = ""
        txtCheqFechaFil.Text = ""
      cmbBancFil.Limpiar()
      cmbEmctFil.Valor = hdnEmctId.Text
      txtNumeFil.Text = ""
      usrClieFil.Limpiar()
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = CrearDataSet(hdnId.Text)
      Dim lDrCheq As DataRow
      Dim lDrMovim As DataRow

      lDrCheq = ldsEsta.Tables(mstrTabla).Rows(0)

      'solo agrega el nuevo, borro los existentes
      While ldsEsta.Tables(mstrTablaMovim).Rows.Count > 0
         ldsEsta.Tables(mstrTablaMovim).Rows.Remove(ldsEsta.Tables(mstrTablaMovim).Rows(0))
      End While

      'guardo los datos anteriores
      lDrMovim = ldsEsta.Tables(mstrTablaMovim).NewRow
      With lDrMovim
         .Item("mchq_id") = -1
         .Item("mchq_cheq_id") = lDrCheq.Item("cheq_id")
         .Item("mchq_rece_fecha") = lDrCheq.Item("cheq_rece_fecha")
            .Item("mchq_teor_depo_fecha") = lDrCheq.Item("cheq_teor_depo_fecha")
            .Item("mchq_clea") = lDrCheq.Item("cheq_clea_id")
         .Item("mchq_emct_id") = lDrCheq.Item("cheq_emct_id")
         .Table.Rows.Add(lDrMovim)
      End With

      'actualizo con los datos nuevos
      With lDrCheq
         .Item("cheq_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("cheq_rece_fecha") = txtReceFecha.Fecha
            '15/09/2010 .Item("cheq_teor_depo_fecha") = txtTeorDepoFecha.Fecha
            .Item("cheq_fecha") = txtCheqFecha.Fecha
            .Item("cheq_plaza") = cmbPlaza.ValorBool
         .Item("cheq_clea_id") = cmbClea.Valor
         .Item("cheq_chti_id") = cmbChti.Valor
         '.Item("cheq_emct_id") = cmbEmct.Valor
      End With

      Return ldsEsta
   End Function

   Private Function CrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      ldsEsta.Tables(0).TableName = mstrTabla
      ldsEsta.Tables(1).TableName = mstrTablaMovim

      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec cheques_busq")
         lstrCmd.Append(" @cheq_emct_id =" + cmbEmctFil.Valor.ToString)
            lstrCmd.Append(",@cheq_rece_fecha=" & clsFormatear.gFormatFecha2DB(txtReceFechaFil.Fecha))             'controlar con cheq
            '15/09/2010 lstrCmd.Append(",@cheq_teor_depo_fecha=" & clsFormatear.gFormatFecha2DB(txtTeorDepoFechaFil.Fecha))
            lstrCmd.Append(",@cheq_fecha=" & clsFormatear.gFormatFecha2DB(txtCheqFechaFil.Fecha))           'controlar con cheq_fecha
            lstrCmd.Append(",@cheq_banc_id =" + cmbBancFil.Valor.ToString)
         lstrCmd.Append(",@cheq_nume =" + clsSQLServer.gFormatArg(txtNumeFil.Valor.ToString, SqlDbType.VarChar))
         lstrCmd.Append(",@clie_id =" + usrClieFil.Valor.ToString)
         lstrCmd.Append(",@depositado=0")

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
	End Sub

	Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
		Try
			mCargarDatos(e.Item.Cells(1).Text)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region

#Region "Eventos"
   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mCerrar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

   Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
      Try
         grdDato.CurrentPageIndex = 0
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltros()
   End Sub

   Private Sub btnDeta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeta.Click
      Try
         Dim ldsEsta As DataSet = CrearDataSet(hdnId.Text)
         grdDetalle.DataSource = ldsEsta.Tables(mstrTablaMovim)
         grdDetalle.DataBind()
         grdDetalle.Visible = True

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

End Class
End Namespace
