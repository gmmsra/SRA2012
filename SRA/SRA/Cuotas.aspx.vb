Namespace SRA

Partial Class Cuotas
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents tabLinks As System.Web.UI.HtmlControls.HtmlTable
   Protected WithEvents btnLimpFil As NixorControls.BotonImagen
   Protected WithEvents panApod As System.Web.UI.WebControls.Panel


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTable As String = "cuotas_estru"


   Private mstrConn As String
   Private mstrParaPageSize As Integer





   Private Enum Columnas As Integer
      Id = 1
      SoinId = 2
      SocmId = 3
      chkSel = 8
      cmbResu = 9
   End Enum
#End Region

#Region "Inicializaci�n de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdCuotas.PageSize = Convert.ToInt32(mstrParaPageSize)
      lblMens.Text = ""
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then

            mMostrarBotones(True)
            mConsultarCuotas()
            clsWeb.gInicializarControles(Me, mstrConn)
            mSetearEventos()
         End If


      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub



#End Region

   Public Sub grdCuotas_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs) Handles grdCuotas.PageIndexChanged
      Try
         grdCuotas.EditItemIndex = -1

         If (grdCuotas.CurrentPageIndex < 0 Or grdCuotas.CurrentPageIndex >= grdCuotas.PageCount) Then
            grdCuotas.CurrentPageIndex = 0
         Else
            grdCuotas.CurrentPageIndex = E.NewPageIndex
         End If

         mConsultarCuotas()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Try

         Dim lDsDatos As DataSet
         Dim lstrPre As String

         hdnAnoId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
         lDsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTable, hdnAnoId.Text)
         'lstrPre = Left("", 5)

         mLimpiarCuotas()
         If lDsDatos.Tables(mstrTable).Rows.Count > 0 Then
            With lDsDatos.Tables(mstrTable).Rows(0)
               hdnAnoId.Text = .Item("cues_id")
               txtAno.Text = .Item("cues_anio")
               cmbPeriTipo.Valor = .Item("cues_peti_id")
               txtPeri.Text = .Item("cues_perio")
               txtDeve.Fecha = .Item("cues_deve_fecha")
               txtVenc.Fecha = .Item("cues_venc_fecha")
               txtPeriInic.Fecha = .Item("cues_perio_inic_fecha")
               txtPeriFin.Fecha = .Item("cues_perio_fina_fecha")
            End With
         End If
         mMostrarBotones(False)
         mostrarPanel(True)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

        Private Sub mConsultarCuotas()
            Try
                Dim anio As String = ""

                If Not (txtAnoFil.Text = "") Then
                    anio = anio + " @cues_anio = " + txtAnoFil.Text
                End If
                Dim mstrCmd As String = "exec " + mstrTable + "_consul" + anio

                Dim ds As New DataSet
                ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
                grdCuotas.DataSource = ds
                grdCuotas.DataBind()
                ds.Dispose()


            Catch ex As Exception
                clsError.gManejarError(Me, ex)
            End Try
        End Sub

   Private Sub mLimpiarCuotas()
      clsWeb.gCargarRefeCmb(mstrConn, "periodo_tipos", cmbPeriTipo, "S")
      hdnAnoId.Text = ""
      txtAno.Text = ""
      cmbPeriTipo.Limpiar()
      txtPeri.Text = ""
      txtDeve.Text = ""
      txtVenc.Text = ""
      txtPeriInic.Text = ""
      txtPeriFin.Text = ""
      mMostrarBotones(True)
   End Sub

   Private Sub mMostrarBotones(ByVal pOk As Boolean)
      Me.BtnAltaCuota.Enabled = pOk
      Me.btnBajaCuota.Enabled = Not pOk
      Me.btnModiCuota.Enabled = Not pOk
   End Sub

   Private Sub mEliminarCuota()
      Try
         Dim lintPage As Integer = grdCuotas.CurrentPageIndex

         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTable)
         lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnAnoId.Text, SqlDbType.Int))

         grdCuotas.CurrentPageIndex = 0

         mConsultarCuotas()

         If (lintPage < grdCuotas.PageCount) Then
            grdCuotas.CurrentPageIndex = lintPage
         End If

         mLimpiarCuotas()
         mostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mSetearEventos()
      btnBajaCuota.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
      btnAuto.Attributes.Add("onclick", "if(!confirm('Esta seguro que quiere generar el a�o?')) return false;")
   End Sub

   Private Function mGuardarCuota() As Data.DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTable, hdnAnoId.Text)
      Dim asam As Int64
      With ldsEsta.Tables(0).Rows(0)
         .Item("cues_anio") = txtAno.Text
         .Item("cues_peti_id") = cmbPeriTipo.Valor
         .Item("cues_perio") = txtPeri.Text
         .Item("cues_deve_fecha") = txtDeve.Fecha
         .Item("cues_venc_fecha") = txtVenc.Fecha
         .Item("cues_perio_inic_fecha") = txtPeriInic.Fecha
         .Item("cues_perio_fina_fecha") = txtPeriFin.Fecha
      End With
      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)

      If (txtAno.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el a�o")
      End If

      If (cmbPeriTipo.Valor Is DBNull.Value) Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el tipo de periodo")
      End If

      If (txtPeri.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar el periodo")
      End If

      If (txtDeve.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de devengado")
      End If

      If (txtVenc.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de vencimiento")
      End If

      If (txtPeriInic.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de inicio del periodo")
      End If

      If (txtPeriFin.Text = "") Then
         Throw New AccesoBD.clsErrNeg("Debe ingresar la fecha de fin del periodo")
      End If

      If (txtDeve.Fecha > txtVenc.Fecha) Then
         Throw New AccesoBD.clsErrNeg("la fecha de vencimiento debe ser mayor o igual que la de devengar")
      End If

      If (txtPeriInic.Fecha > txtPeriFin.Fecha) Then
         Throw New AccesoBD.clsErrNeg("La fecha de inicio del periodo debe ser mayor o igual a la del fin")
      End If
    
   End Sub

   Private Sub mAltaCuota()
      Try

         mValidarDatos()

         Dim mdsDatos As DataSet = mGuardarCuota()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTable, mdsDatos)

         lobjGenerica.Alta()


         mConsultarCuotas()

         mLimpiarCuotas()
         mostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModiCuota()
      Try

         mValidarDatos()

         Dim mdsDatos As DataSet = mGuardarCuota()
         Dim lobjGenerica As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTable, mdsDatos)

         lobjGenerica.Modi()

         mLimpiarCuotas()
         mConsultarCuotas()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub BtnAltaCuota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAltaCuota.Click
      mAltaCuota()
   End Sub

   Private Sub btnBajaCuota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaCuota.Click
      mEliminarCuota()
   End Sub

   Private Sub btnModiCuota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiCuota.Click
      mModiCuota()
   End Sub

   Private Sub btnLimpCuota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpCuota.Click
      mLimpiarCuotas()
   End Sub

   Private Sub NuevaCuota_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevaCuota.Click
      mLimpiarCuotas()
      mostrarPanel(True)
   End Sub

   Private Sub mostrarPanel(ByVal pOk As Boolean)
      PanDetalle.Visible = pOk
      btnNuevaCuota.Enabled = Not pOk
   End Sub

   Private Sub imgClose_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mLimpiarCuotas()
      mostrarPanel(False)
   End Sub


   Private Sub Botonimagen1_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles Botonimagen1.Click
      mConsultarCuotas()
   End Sub

   Private Sub btnAuto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAuto.Click
      mGenerarNuevoAnio()
      mConsultarCuotas()
      grdCuotas.CurrentPageIndex = 0
   End Sub

   Private Sub mGenerarNuevoAnio()
      clsSQLServer.gExecuteQuery(mstrConn, "cuotas_estru_generar")
   End Sub


   Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
      mLimpiarFiltro()
   End Sub

   Private Sub mLimpiarFiltro()
      txtAnoFil.Text = ""
   End Sub
End Class

End Namespace
