' Dario 2014-07-11 se conecta a nuevas reglas
Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports SRA_Entidad


Namespace SRA


Partial Class DenunciaServicio
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
    'Protected WithEvents usrCria As usrClieDeriv 
    'Protected WithEvents usrCriador As usrClieDeriv
    'Protected WithEvents cmbRazaFil As NixorControls.ComboBox
   Protected WithEvents lblRaza As System.Web.UI.WebControls.Label

   Protected WithEvents lblCria As System.Web.UI.WebControls.Label
    '' Protected WithEvents btnBaja As System.Web.UI.WebControls.Button
   Protected WithEvents lblPadres As System.Web.UI.WebControls.Label
    ' Protected WithEvents txtNroControl As NixorControls.NumberBox



   'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
   'No se debe eliminar o mover.

   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
      'No la modifique con el editor de c�digo.
      InitializeComponent()
   End Sub

#End Region

#Region "Definici�n de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_ServiDenuncias
   Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ServiDenunciasDeta
   Private mstrTablaPadres As String = SRA_Neg.Constantes.gTab_ServiDenunciasDetaPadres
   Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
   Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
   Private mstrProductos As String = SRA_Neg.Constantes.gTab_Productos
   Private mstrParaPageSize As Integer
   Private mstrCmd As String
   Private mdsDatos As DataSet
   Private dsVali As DataSet
   Private mstrConn As String
   Public mstrEstaId As String
   Public mintCantServBovi As Integer
   Public mintCantServCapr As Integer
   'grilla de detalles
   Public mintgrdDetaEdit As Integer = 0
   Public mintgrdDetaId As Integer = 1
   Public mintgrdDetaItem As Integer = 2
   Public mintgrdDetaMadreRp As Integer = 3
   Public mintgrdDetaMadreNro As Integer = 4
   Public mintgrdDetaFechaD As Integer = 5
   Public mintgrdDetaFechaH As Integer = 6
   Public mintgrdDetaTipo As Integer = 7
   Public mintgrdDetaPadreNro As Integer = 8
   Public mintgrdDetaPadreRP As Integer = 9
   Public mintgrdDetaToroNro As Integer = 10
   Public mintgrdDetaToroRp As Integer = 11
   Public mintgrdDetaVacaTatu As Integer = 12
   Public mintgrdDetaEsta As Integer = 13

   Private Enum Columnas As Integer
		EstaVisto = 0
		EstaEdit = 1
		EstaId = 2
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Dim lstrFil As String
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then

            Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")

                Session("rg_servi_denuncias") = Nothing

            mSetearMaxLength()
            mSetearEventos()
            mCargarCombos()
            mMostrarPanel(False)

            mstrEstaId = Request.QueryString("id")
            If mstrEstaId <> "" Then
                mCargarDatos(mstrEstaId)
                mMostrarPanel(True)
                mSetearEditor(mstrTabla, False)
            End If
         Else
            If panDato.Visible Then
                    mdsDatos = Session("rg_servi_denuncias")
               Dim x As String = Session.SessionID
               'If Not cmbEspe.Valor Is DBNull.Value Then
                  'mCargarRazas(cmbEspe.Valor)
                    'usrCria.RazaId = hdnRaza.Text
                    'End If
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarCombos()

        'clsWeb.gCargarCombo(mstrConn, "especies_cargar @espe_genealo=1", cmbEspe, "id", "descrip", "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_servi_tipos", cmbSeti, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_servi_tipos", cmbSetiDeta, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_registros_tipos", cmbRegiFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_registros_tipos", cmbRegi, "S")
        'mCargarRazas(cmbEspe.Valor.ToString)
    End Sub
    Private Sub mCargarRazas(ByVal pstrEspe As String)
        'If pstrEspe = "" Then
        '   cmbRaza.Items.Clear()
        'Else
        '   clsWeb.gCargarCombo(mstrConn, "razas_cargar @raza_espe_id=" & pstrEspe, cmbRaza, "id", "descrip_codi", "S")
        'End If
    End Sub

    Private Sub mSetearEventos()
        'btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnBajaDeta.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
        btnErr.Attributes.Add("onclick", "mVerErrores(); return false;")
    End Sub
    Private Sub mSetearMaxLength()

        Dim lstrDenuLong As Object
        Dim lstrDetaLong As Object

        lstrDenuLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        '  txtNroControl.MaxLength = clsSQLServer.gObtenerLongitud(lstrDenuLong, "sede_nro_control")

        txtFolio.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtLinea.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero

        lstrDetaLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTablaDeta)
        txtObse.MaxLength = clsSQLServer.gObtenerLongitud(lstrDetaLong, "sdde_obser")

        txtFolioFil.MaxLength = txtFolio.MaxLength
        txtLineFil.MaxLength = txtLinea.MaxLength

    End Sub
#End Region

#Region "Inicializacion de Variables"
    Public Function mTipoPlanilla(ByVal pstrEspe As String) As Integer
        Select Case pstrEspe
            Case SRA_Neg.Constantes.Especies.Bovinos
                Return (1)  'BOBINOS
            Case SRA_Neg.Constantes.Especies.Ovinos, SRA_Neg.Constantes.Especies.Caprinos
                Return (2)  'OVINOS/CAPRINOS
            Case Else
                Return (3)  'EQUIDOS/CAMELIDOS
        End Select
    End Function
    Public Sub mSetearColumnasDetalle(ByVal pstrEspe As String)
        Select Case mTipoPlanilla(pstrEspe)
            Case 1   'BOBINOS
                grdDeta.Columns(mintgrdDetaMadreRp).Visible = False
                grdDeta.Columns(mintgrdDetaMadreNro).Visible = False
                grdDeta.Columns(mintgrdDetaFechaD).Visible = True
                grdDeta.Columns(mintgrdDetaFechaH).Visible = True
                grdDeta.Columns(mintgrdDetaTipo).Visible = True
                grdDeta.Columns(mintgrdDetaPadreNro).Visible = False
                grdDeta.Columns(mintgrdDetaPadreRP).Visible = False
                grdDeta.Columns(mintgrdDetaToroNro).Visible = True
                grdDeta.Columns(mintgrdDetaToroRp).Visible = True
                grdDeta.Columns(mintgrdDetaVacaTatu).Visible = True
            Case 2   'OVINOS/CAPRINOS
                grdDeta.Columns(mintgrdDetaMadreRp).Visible = True
                grdDeta.Columns(mintgrdDetaMadreNro).Visible = True
                grdDeta.Columns(mintgrdDetaFechaD).Visible = False
                grdDeta.Columns(mintgrdDetaFechaH).Visible = False
                grdDeta.Columns(mintgrdDetaTipo).Visible = False
                grdDeta.Columns(mintgrdDetaPadreNro).Visible = False
                grdDeta.Columns(mintgrdDetaPadreRP).Visible = False
                grdDeta.Columns(mintgrdDetaToroNro).Visible = False
                grdDeta.Columns(mintgrdDetaToroRp).Visible = False
                grdDeta.Columns(mintgrdDetaVacaTatu).Visible = False
            Case 3   'EQUIDOS/CAMELIDOS
                grdDeta.Columns(mintgrdDetaMadreRp).Visible = True
                grdDeta.Columns(mintgrdDetaMadreNro).Visible = True
                grdDeta.Columns(mintgrdDetaFechaD).Visible = True
                grdDeta.Columns(mintgrdDetaFechaH).Visible = True
                grdDeta.Columns(mintgrdDetaTipo).Visible = True
                grdDeta.Columns(mintgrdDetaPadreNro).Visible = True
                grdDeta.Columns(mintgrdDetaPadreRP).Visible = True
                grdDeta.Columns(mintgrdDetaToroNro).Visible = False
                grdDeta.Columns(mintgrdDetaToroRp).Visible = False
                grdDeta.Columns(mintgrdDetaVacaTatu).Visible = False
        End Select
        mSetearTitulosGrilla(hdnEspe.Text)
    End Sub
    Public Sub mSetearTitulosGrilla(ByVal pstrEspe As String)
        Select Case pstrEspe
            Case SRA_Neg.Constantes.Especies.Equinos
                grdDeta.Columns(mintgrdDetaMadreNro).HeaderText = "SBA Madre"
                grdDeta.Columns(mintgrdDetaPadreNro).HeaderText = "SBA Padrillo"
                grdDeta.Columns(mintgrdDetaPadreRP).HeaderText = "RP Padrillo"
            Case SRA_Neg.Constantes.Especies.Camelidos
                grdDeta.Columns(mintgrdDetaMadreNro).HeaderText = "RILL RIA Madre"
                grdDeta.Columns(mintgrdDetaPadreNro).HeaderText = "RILL RIA Ja�acho"
                grdDeta.Columns(mintgrdDetaPadreRP).HeaderText = "RP Ja�acho"
            Case SRA_Neg.Constantes.Especies.Ovinos, SRA_Neg.Constantes.Especies.Caprinos
                grdDeta.Columns(mintgrdDetaMadreNro).HeaderText = "RIL Madre"
        End Select
    End Sub

    Public Sub mInicializar()

        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)

        mintCantServBovi = clsSQLServer.gCampoValorConsul(mstrConn, "parametros_consul", "para_servi_bovi_maxi_cant")
        mintCantServCapr = clsSQLServer.gCampoValorConsul(mstrConn, "parametros_consul", "para_servi_capr_maxi_cant")

        '''''usrCriador.cmbCriaRazaExt.Valor = usrCriador.RazaId
        '''''usrPadre.cmbProdRazaExt.Valor = usrCriador.RazaId

        usrPadre.FilProp = True
        usrPadreDeta.FilProp = True
        usrMadreDeta.FilProp = True
        usrMadreDetaBov.FilProp = True

        usrPadreFil.FilRpNume = True
        usrMadreFil.FilRpNume = True
        usrPadre.FilRpNume = True
        usrPadreDeta.FilRpNume = True
        usrMadreDeta.FilRpNume = True
        usrMadreDetaBov.FilRpNume = True

        usrPadre.ValiExis = 0
        usrPadreDeta.ValiExis = 0
        usrMadreDeta.ValiExis = 0
        usrMadreDetaBov.ValiExis = 0

        mSetearColumnasDetalle(hdnEspe.Text)

    End Sub
#End Region

#Region "Operaciones sobre el DataGrid"
    Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mConsultar()
        Try
            Dim lstrCmd As String

            lstrCmd = "exec " & mstrTabla & "_busq"
            lstrCmd = lstrCmd & " @clie_id=" & clsSQLServer.gFormatArg(usrClieFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@cria_id=" & clsSQLServer.gFormatArg(usrCriaFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@padre_id=" & clsSQLServer.gFormatArg(usrPadreFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@madre_id=" & clsSQLServer.gFormatArg(usrMadreFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@folio_nume=" & clsSQLServer.gFormatArg(txtFolioFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@linea_nume=" & clsSQLServer.gFormatArg(txtLineFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@fecha_desde=" & clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@fecha_hasta=" & clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime)
            lstrCmd = lstrCmd & ",@raza_id=" & clsSQLServer.gFormatArg(usrCriaFil.RazaId, SqlDbType.Int)
            lstrCmd = lstrCmd & ",@sede_regt_id=" & clsSQLServer.gFormatArg(cmbRegiFil.Valor.ToString, SqlDbType.Int)
            lstrCmd = lstrCmd & " ,@mostrar_vistos=" + IIf(chkVisto.Checked, "1", "0")
            clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato)
            'grdDato.Visible = True
            mMostrarPanel(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Seteo de Controles"
    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTablaDeta
                btnBajaDeta.Enabled = Not (pbooAlta)
                btnModiDeta.Enabled = Not (pbooAlta)
                btnAltaDeta.Enabled = pbooAlta
                btnErr.Visible = Not (pbooAlta)
            Case Else
                ' btnBaja.Enabled = Not (pbooAlta)
                btnModi.Enabled = Not (pbooAlta)
                btnAlta.Enabled = pbooAlta
        End Select
    End Sub
    Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mCargarDatos(E.Item.Cells(Columnas.EstaId).Text)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub mCargarDatos(ByVal pstrId As String)
        mLimpiar()

        hdnId.Text = clsFormatear.gFormatCadena(pstrId)

        mCrearDataSet(hdnId.Text)

        grdDeta.CurrentPageIndex = 0

        If mdsDatos.Tables(0).Rows.Count > 0 Then
            With mdsDatos.Tables(0).Rows(0)
                hdnId.Text = .Item("sede_id")
                txtFecha.Fecha = .Item("sede_fecha")
                '   txtNroControl.Text = ValidarNulos(.Item("sede_nro_control"), False)
                    If .Item("sede_presen_fecha").ToString.Length > 0 Then
                        txtPresFecha.Fecha = .Item("sede_presen_fecha")
                    End If
                    'cmbEspe.Valor = .Item("sede_espe_id")
                    hdnEspe.Text = .Item("sede_espe_id")
                    'mCargarRazas(cmbEspe.Valor.ToString)
                    hdnRaza.Text = .Item("sede_raza_id")
                    usrCriador1.RazaId = .Item("sede_raza_id")
                    usrClie.Valor = .Item("sede_clie_id")
                    usrCriador1.Valor = .Item("sede_cria_id")
                    txtFolio.Valor = .Item("sede_folio_nume")
                    If .Item("sede_servi_desde").ToString.Length > 0 Then
                        txtServDesd.Fecha = .Item("sede_servi_desde")
                    End If
                    If .Item("sede_servi_hasta").ToString.Length > 0 Then
                        txtServHast.Fecha = .Item("sede_servi_hasta")
                    End If
                    usrPadre.Valor = .Item("sede_padre_id")
                    If usrPadre.Valor.ToString.Length > 0 Then
                        usrPadre.txtRPExt.Text = IIf(.IsNull("sede_padre_rp"), "", .Item("sede_padre_rp"))
                        usrPadre.txtSraNumeExt.Valor = .Item("sede_padre_sra_nume")
                        usrPadre.txtProdNombExt.Valor = "---- Error: Producto inexistente ----"
                    End If
                    If usrPadre.cmbProdRazaExt.Valor.Length = 0 Then
                        usrPadre.cmbProdRazaExt.Valor = usrCriador1.RazaId
                    End If
                    If usrCriador1.cmbCriaRazaExt.Valor.Length = 0 Then
                        usrCriador1.cmbCriaRazaExt.Valor = usrCriador1.RazaId
                    End If

                    cmbSeti.Valor = .Item("sede_seti_id")
                    clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_cargar @regt_raza_id=" & hdnRaza.Text, cmbRegi, "id", "descrip", "S")
                    cmbRegi.Valor = .Item("sede_regt_id").ToString

                    If Not .IsNull("sede_baja_fecha") Then
                        lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("sede_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                    Else
                        lblBaja.Text = ""
                    End If

                    lblTitu.Text = "Denuncia: Raza: " & .Item("_raza") & " - Cliente: " & .Item("sede_clie_id") & "-" & .Item("_cliente") & " - Criador: " & .Item("_criador") & "- Folio: " & .Item("sede_folio_nume")
                    lblTitu.Visible = False
                End With

            hdnDetaCarga.Text = "S"

            mSetearEditor("", False)
            mSetearEditor(mstrTablaDeta, True)
            mMostrarPanel(True)
            mShowTabs(1)
        End If
    End Sub
    Private Sub mAgregar()
        mLimpiar()
        ' btnBaja.Enabled = False
        btnModi.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiarFiltros()
        hdnValorId.Text = "-1"  'para que ignore el id que vino del control
        txtFolioFil.Text = ""
        txtLineFil.Text = ""
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        cmbRegiFil.Items.Clear()
        usrClieFil.Limpiar()
        usrCriaFil.Limpiar()
        usrPadreFil.Limpiar()
        usrMadreFil.Limpiar()
        '  cmbRazaFil.Limpiar()
        grdDato.Visible = False
    End Sub
    Private Sub mLimpiar()
        hdnId.Text = ""
        hdnDetaCarga.Text = ""
        hdnRaza.Text = ""
        lblBaja.Text = ""
        '  txtNroControl.Text = ""

        'cmbEspe.Limpiar()
        '   cmbRaza.Limpiar()
        cmbSeti.Limpiar()
        txtFecha.Text = ""
        txtPresFecha.Text = ""
        txtServDesd.Text = ""
        txtServHast.Text = ""
        txtFolio.Text = ""
        cmbRegi.Items.Clear()
        usrClie.Limpiar()
        'aaaaaaaaaaaaausrCria.Limpiar()
        usrPadre.Limpiar()

        mLimpiarDetalle()
        grdDeta.CurrentPageIndex = 0
        grdPadresDeta.CurrentPageIndex = 0

        mCrearDataSet("")

        lblTitu.Text = ""

        grdDeta.CurrentPageIndex = 0

        mSetearEditor("", True)

        If Not Session("sEspeId") Is Nothing Then
            hdnEspe.Text = Session("sEspeId").ToString
        End If
        'mCargarRazas(cmbEspe.Valor.ToString)
        'If Not Session("sRazaId") Is Nothing Then
        '   usrCria.RazaId = Session("sRazaId").ToString
        '   hdnRaza.Text = Session("sRazaId").ToString
        'End If
        mShowTabs(1)

    End Sub
    Private Sub mCerrar()
        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        btnAgre.Visible = Not panDato.Visible

        lnkCabecera.Font.Bold = True
        lnkDeta.Font.Bold = False

        panDato.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panCabecera.Visible = True
        panDeta.Visible = False

        tabLinks.Visible = pbooVisi

        lnkCabecera.Font.Bold = True
        lnkDeta.Font.Bold = False
    End Sub
    Private Sub mShowTabs(ByVal origen As Byte)
        Try
            If origen = 2 And usrCriador1.RazaId.ToString = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar la raza de la denuncia.")
            End If

            Dim lstrTitu As String
            If lblTitu.Text <> "" Then
                lstrTitu = lblTitu.Text.Substring(lblTitu.Text.IndexOf(":") + 2)
            End If

            panDato.Visible = True
            panBotones.Visible = True
            btnAgre.Visible = False
            panDeta.Visible = False
            panCabecera.Visible = False

            lnkCabecera.Font.Bold = False
            lnkDeta.Font.Bold = False

            Select Case origen
                Case 1
                    panCabecera.Visible = True
                    lnkCabecera.Font.Bold = True
                    lblTitu.Visible = False
                Case 2
                    panDeta.Visible = True
                    lnkDeta.Font.Bold = True
                    lblTitu.Text = "Servicios de la Denuncia: " & lstrTitu
                    lblTitu.Visible = True
                    grdDeta.Visible = True
                    usrPadreDeta.cmbProdRazaExt.Valor = usrCriador1.RazaId    'bbbbbbbbbbbaa ok
                    usrPadreDeta.cmbProdRazaExt.Enabled = False
                    usrPadreDeta.hdnProdProp.Text = usrClie.txtCodiExt.Text
                    usrMadreDeta.cmbProdRazaExt.Valor = usrCriador1.RazaId 'bbbbbbbbbbbbb ok
                    usrMadreDeta.cmbProdRazaExt.Enabled = False
                    usrMadreDeta.hdnProdProp.Text = usrClie.txtCodiExt.Text
                    usrMadreDetaBov.cmbProdRazaExt.Valor = usrCriador1.RazaId  'bbbbbbbbbbbb ok
                    usrMadreDetaBov.cmbProdRazaExt.Enabled = False
                    usrMadreDetaBov.hdnProdProp.Text = usrClie.txtCodiExt.Text
                    If hdnEspe.Text = SRA_Neg.Constantes.Especies.Bovinos Then
                        usrMadreDetaBov.Visible = True
                        lblMadreDetaBov.Visible = True
                        usrMadreDeta.Visible = False
                        lblMadreDeta.Visible = False
                    Else
                        usrMadreDetaBov.Visible = False
                        lblMadreDetaBov.Visible = False
                        usrMadreDeta.Visible = True
                        lblMadreDeta.Visible = True
                    End If
            End Select
            hdnFiltroProdCriaId.Text = usrCriador1.Valor.ToString  'bbbbbbbbbbbbbbbbbbbbok

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
#End Region

#Region "Opciones de ABM"
    Private Sub mAlta()
        Try
            Dim lstrDenuId As String

            mGuardarDatos()
            Dim lobjServiDenun As New SRA_Neg.Denuncias(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaDeta, mstrTablaPadres, mdsDatos, Server)

            lstrDenuId = lobjServiDenun.Alta()

            ' Dario 2014-07-11 en denuncias de servicio no se aplican reglas
            mAplicarReglas(lstrDenuId)

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mModi()
        Try
            mGuardarDatos()
            Dim lobjServiDenun As New SRA_Neg.Denuncias(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaDeta, mstrTablaPadres, mdsDatos, Server)

            lobjServiDenun.Modi()

            mAplicarReglas(hdnId.Text)

            mConsultar()

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            Dim lobjServiDenun As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
            lobjServiDenun.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))

            grdDato.CurrentPageIndex = 0

            mConsultar()

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar()
            End If

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub mValidarDatos()
        Dim drTramites As DataRow
        Dim iCriador As Integer

        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)

        Dim lintServCanti As Integer

        'If usrClie.Valor Is DBNull.Value And usrCria.Valor Is DBNull.Value Then
        '    Throw New AccesoBD.clsErrNeg("Debe indicar el cliente o el criador.")
        'End If


        If usrCriador1.RazaId.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la Raza del Producto")
        End If


        Select Case hdnEspe.Text
            Case SRA_Neg.Constantes.Especies.Ovinos, SRA_Neg.Constantes.Especies.Caprinos
                If txtServDesd.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de entrada del servicio.")
                End If
                'If usrPadre.Valor Is DBNull.Value Then
                '   Throw New AccesoBD.clsErrNeg("Debe indicar el padre.")
                'End If
                If cmbSeti.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el tipo de servicio.")
                End If
        End Select

        lintServCanti = mdsDatos.Tables(mstrTablaDeta).Select("sdde_baja_fecha is null").GetUpperBound(0) + 1

        If lintServCanti = 0 Then
            Throw New AccesoBD.clsErrNeg("Debe indicar al menos un servicio para la denuncia.")
        End If

        Select Case hdnEspe.Text
            Case SRA_Neg.Constantes.Especies.Caprinos, SRA_Neg.Constantes.Especies.Ovinos
                If lintServCanti > mintCantServCapr Then
                    Throw New AccesoBD.clsErrNeg("No puede cargar mas de " & mintCantServCapr.ToString & " servicios para la denuncia.")
                End If
            Case Else
                If lintServCanti > mintCantServBovi Then
                    Throw New AccesoBD.clsErrNeg("No puede cargar mas de " & mintCantServBovi & " servicios para la denuncia.")
                End If
        End Select


        'If ValidarNulos(txtNroControl.Valor, False) = 0 Then
        '    Throw New AccesoBD.clsErrNeg("El Nro de Control debe ser distinto de 0.")
        'End If
        Dim oCriador As New SRA_Neg.Criadores(mstrConn, Session("sUserId").ToString())

        iCriador = ValidarNulos(usrCriador1.Valor, False)



        'Dim oTramites As New SRA_Neg.Tramites(mstrConn, Session("sUserId").ToString())
        'drTramites = oTramites.GetCantFacturadoByNroControlRazaCriador(txtNroControl.Valor, _
        '              usrPadre.cmbProdRazaExt.Valor.ToString(), _
        '             iCriador.ToString())

        'If IsNothing(drTramites) Then
        '    Throw New AccesoBD.clsErrNeg("No se encontraron comprobantes ,ni proforma con el Nro.Control " & txtNroControl.Valor)
        'Else

        'End If
    End Sub
    Private Sub mAplicarReglas(ByVal pstrDenuId As String)
        Try
            Dim lstrError As String
            Dim lstrFecha As String

            'CABECERA
            ''dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.Servicios.ToString(), "W")

            ' Dario 2014-07-11 se conecta a las nuevas reglas
            dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.Servicios.ToString(), "W", txtFecha.Text, txtFecha.Text)

            'lstrError = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, pstrDenuId, usrCria.cmbCriaRazaExt.Valor.ToString, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Servicios.ToString(), 0, 0)
            lstrError = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTabla, pstrDenuId, usrCriador1.RazaId.ToString, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Servicios.ToString(), 0, 0)

            mstrCmd = "exec " & mstrTablaDeta & "_consul @sdde_sede_id = " & pstrDenuId

            dsVali.Clear()

            Dim dr As SqlDataReader = clsSQLServer.gExecuteQueryDR(mstrConn, mstrCmd)

            'DETALLE
            Do While dr.Read
                'Aplicar reglas (si el registro no esta dado de baja)
                If dr.GetValue(dr.GetOrdinal("sdde_baja_fecha")) Is DBNull.Value Then
                    'Determinar las reglas vigentes.
                    ' Dario 2014-07-11 se conecta a nuevas reglas
                    'lstrFecha = dr.GetValue(dr.GetOrdinal("sdde_servi_desde"))
                    'dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.Servicios.ToString(), "W", lstrFecha, lstrFecha)
                    'Limpiar errores anteriores.
                    ReglasValida.Validaciones.gLimpiarErrores(mstrConn, mstrTablaDeta, dr.GetValue(dr.GetOrdinal("sdde_id")))
                    'Aplicar Reglas.
                    ' Dario 2014-07-11 se conecta a nuevas reglas
                    'hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, mstrTablaDeta, dr.GetValue(dr.GetOrdinal("sdde_id")), usrCria.cmbCriaRazaExt.Valor.ToString, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Servicios.ToString(), 0, 0)
                    hdnMsgError.Text = ReglasValida.Validaciones.gValidarReglas(mstrConn, mstrTablaDeta, dr.GetValue(dr.GetOrdinal("sdde_id")), _
                    usrCriador1.RazaId, "", Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Servicios.ToString(), 0, 0)

                End If
            Loop
            dr.Close()
            If lstrError <> "" Then
                AccesoBD.clsError.gGenerarMensajes(Me, lstrError)
            Else
                If hdnMsgError.Text <> "" Then
                    AccesoBD.clsError.gGenerarMensajes(Me, hdnMsgError.Text)
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mGuardarDatos() As DataSet

        mValidarDatos()

        With mdsDatos.Tables(0).Rows(0)
            .Item("sede_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("sede_fecha") = txtFecha.Fecha
            .Item("sede_presen_fecha") = txtPresFecha.Fecha
            .Item("sede_espe_id") = hdnEspe.Text
            '.Item("sede_raza_id") = usrCria.RazaId

            .Item("sede_raza_id") = usrCriador1.RazaId
            .Item("sede_clie_id") = usrClie.Valor
            '  .Item("sede_cria_id") = usrCria.Valor
            .Item("sede_cria_id") = usrCriador1.Valor
                .Item("sede_folio_nume") = txtFolio.Valor
                If txtServDesd.Fecha.Length > 0 Then
                    .Item("sede_servi_desde") = txtServDesd.Fecha
                End If
                If txtServHast.Fecha.Length > 0 Then
                    .Item("sede_servi_hasta") = txtServHast.Fecha
                End If
                .Item("sede_padre_id") = usrPadre.Valor
                .Item("sede_padre_rp") = usrPadre.txtRPExt.Text
                If usrPadre.txtSraNumeExt.Text.Length = 0 Then
                    .Item("sede_padre_sra_nume") = DBNull.Value
                Else
                    .Item("sede_padre_sra_nume") = usrPadre.txtSraNumeExt.Text
                End If
                .Item("sede_seti_id") = cmbSeti.Valor
                If cmbRegi.Valor.Length > 0 Then
                    .Item("sede_regt_id") = cmbRegi.Valor
                End If
                ' .Item("sede_nro_control") = ValidarNulos(txtNroControl.Text, False)
            End With
        Return mdsDatos
    End Function

    Private Sub mEliminarPadre()
        Try
            If hdnPadreId.Text = "" Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el padre a eliminar.")
            End If
            With mdsDatos.Tables(mstrTablaPadres).Select("sdpa_id=" & hdnPadreId.Text)(0)
                .Delete()
                hdnPadreId.Text = ""
            End With
            mGuardarDatosDeta(False)
            usrPadreDeta.Limpiar()
            mCargarGrillaPadres()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mAgregarPadre(ByVal pbooActuDeta As Boolean)

        Try

            Dim ldrDatos As DataRow
            Dim lstrCmd As String

            If Not cmbSetiDeta.Valor Is DBNull.Value Then
                If cmbSetiDeta.Valor = SRA_Neg.Constantes.TipoServicios.SiembraMultiple Then
                    lstrCmd = "sdpa_padre_rp=" & clsSQLServer.gFormatArg(usrPadreDeta.txtRPExt.Text, SqlDbType.VarChar)
                    If usrPadreDeta.txtSraNumeExt.Text <> "" Then
                        lstrCmd = lstrCmd & " and sdpa_padre_sra_nume = " & usrPadreDeta.txtSraNumeExt.Text
                    End If
                    If mdsDatos.Tables(mstrTablaPadres).Select().GetUpperBound(0) <> -1 Then
                        If mdsDatos.Tables(mstrTablaPadres).Select(lstrCmd).GetUpperBound(0) <> -1 Then
                            Throw New AccesoBD.clsErrNeg("El padre indicado ya se encuentra asociado.")
                        End If
                    End If
                End If
            End If

            'If hdnPadreId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaPadres).NewRow
            ldrDatos.Item("sdpa_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaPadres), "sdpa_id")
            'Else
            '    ldrDatos = mdsDatos.Tables(mstrTablaPadres).Select("sdpa_id=" & hdnPadreId.Text)(0)
            'End If

            With ldrDatos

                If hdnDetaId.Text <> "" Then
                    .Item("sdpa_sdde_id") = hdnDetaId.Text
                End If
                .Item("_padre") = usrPadreDeta.txtProdNombExt.Valor
                .Item("sdpa_padre_id") = usrPadreDeta.Valor
                .Item("sdpa_padre_rp") = usrPadreDeta.txtRPExt.Text
                If usrPadreDeta.txtSraNumeExt.Text = "" Then
                    .Item("sdpa_padre_sra_nume") = DBNull.Value
                Else
                    .Item("sdpa_padre_sra_nume") = usrPadreDeta.txtSraNumeExt.Text
                End If
            End With

            'If hdnPadreId.Text = "" Then
            mdsDatos.Tables(mstrTablaPadres).Rows.Add(ldrDatos)
            'End If

            If pbooActuDeta Then
                mGuardarDatosDeta(False)
            End If

            usrPadreDeta.Limpiar()

            mCargarGrillaPadres()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Public Sub mCrearDataSet(ByVal pstrId As String)
        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaDeta
        mdsDatos.Tables(2).TableName = mstrTablaPadres

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
        grdDeta.DataBind()
        txtLinea.Valor = mdsDatos.Tables(mstrTablaDeta).Rows.Count + 1

        mCargarGrillaPadres()

        Session("rg_servi_denuncias") = mdsDatos
    End Sub

    Private Sub mCargarGrillaPadres()

        Dim dt As New DataTable
        Dim dr As DataRow

        dt.Columns.Add(New DataColumn("sdpa_id", GetType(String)))
        dt.Columns.Add(New DataColumn("sdpa_sdde_id", GetType(String)))
        dt.Columns.Add(New DataColumn("sdpa_padre_id", GetType(Integer)))
        dt.Columns.Add(New DataColumn("sdpa_padre_rp", GetType(String)))
        dt.Columns.Add(New DataColumn("sdpa_padre_sra_nume", GetType(Integer)))
        dt.Columns.Add(New DataColumn("sdpa_audi_user", GetType(Integer)))
        dt.Columns.Add(New DataColumn("_padre", GetType(String)))

        If hdnDetaId.Text <> "" Then
            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaPadres).Select("sdpa_sdde_id=" & hdnDetaId.Text)
                With lDr
                    dr = dt.NewRow()
                    dr("sdpa_id") = lDr.Item("sdpa_id")
                    dr("sdpa_sdde_id") = lDr.Item("sdpa_sdde_id")
                    dr("sdpa_padre_id") = lDr.Item("sdpa_padre_id")
                    dr("sdpa_padre_rp") = lDr.Item("sdpa_padre_rp")
                    dr("sdpa_padre_sra_nume") = lDr.Item("sdpa_padre_sra_nume")
                    dr("sdpa_audi_user") = lDr.Item("sdpa_audi_user")
                    dr("_padre") = lDr.Item("_padre")
                    dt.Rows.Add(dr)
                End With
            Next
        End If

        If Not dt Is Nothing Then
            grdPadresDeta.DataSource = dt
        Else
            'If mdsDatos.Tables(mstrTablaPadres).Rows.Count = 0 Then
            '   mdsDatos.Tables(mstrTablaPadres).Rows.Add(mdsDatos.Tables(mstrTablaPadres).NewRow)
            'End If
            grdPadresDeta.DataSource = mdsDatos.Tables(mstrTablaPadres)
        End If
        grdPadresDeta.DataBind()

    End Sub


#End Region

#Region "Detalle"

    Public Sub mEditarPadre(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try

            Dim ldrPad As DataRow

            hdnPadreId.Text = E.Item.Cells(1).Text
            ldrPad = mdsDatos.Tables(mstrTablaPadres).Select("sdpa_id=" & hdnPadreId.Text)(0)

            With ldrPad
                usrPadreDeta.Valor = .Item("sdpa_padre_id")
                If usrPadreDeta.cmbProdRazaExt.Valor Is DBNull.Value Then
                    usrPadreDeta.cmbProdRazaExt.Valor = usrCriador1.RazaId 'bbbbbbOK
                End If
                If usrPadreDeta.Valor Is DBNull.Value Then
                    usrPadreDeta.txtRPExt.Text = IIf(.IsNull("sdpa_padre_rp"), "", .Item("sdpa_padre_rp"))
                    usrPadreDeta.txtSraNumeExt.Valor = IIf(.IsNull("sdpa_padre_sra_nume"), "", .Item("sdpa_padre_sra_nume"))
                    usrPadreDeta.txtProdNombExt.Valor = "---- Error: Producto inexistente ----"
                End If
            End With

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Sub mEditarDatosDeta(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDeta As DataRow

            hdnDetaId.Text = E.Item.Cells(1).Text
            ldrDeta = mdsDatos.Tables(mstrTablaDeta).Select("sdde_id=" & hdnDetaId.Text)(0)

            With ldrDeta
                usrPadreDeta.Valor = .Item("sdde_padre_id")
                If usrPadreDeta.Valor Is DBNull.Value Then
                    usrPadreDeta.txtRPExt.Text = IIf(.IsNull("sdde_padre_rp"), "", .Item("sdde_padre_rp"))
                    usrPadreDeta.txtSraNumeExt.Valor = .Item("sdde_padre_sra_nume")
                    usrPadreDeta.txtProdNombExt.Valor = "---- Error: Producto inexistente ----"
                End If
                If hdnEspe.Text = SRA_Neg.Constantes.Especies.Bovinos Then
                    usrMadreDetaBov.Valor = .Item("sdde_madre_id")
                    If usrMadreDetaBov.Valor Is DBNull.Value Then
                        usrMadreDetaBov.txtRPExt.Text = IIf(.IsNull("sdde_madre_rp"), "", .Item("sdde_madre_rp"))
                        usrMadreDetaBov.txtSraNumeExt.Valor = .Item("sdde_madre_sra_nume")
                        usrMadreDetaBov.txtProdNombExt.Valor = "---- Error: Producto inexistente ----"
                    End If
                Else
                    usrMadreDeta.Valor = .Item("sdde_madre_id")
                    If usrMadreDeta.Valor Is DBNull.Value Then
                        usrMadreDeta.txtRPExt.Text = IIf(.IsNull("sdde_madre_rp"), "", .Item("sdde_madre_rp"))
                        usrMadreDeta.txtSraNumeExt.Valor = .Item("sdde_madre_sra_nume")
                        usrMadreDeta.txtProdNombExt.Valor = "---- Error: Producto inexistente ----"
                    End If
                End If
                txtObse.Valor = .Item("sdde_obser")
                txtServDesdeDeta.Fecha = .Item("sdde_servi_desde")
                txtServHastaDeta.Fecha = .Item("sdde_servi_hasta")
                cmbSetiDeta.Valor = .Item("sdde_seti_id")
                txtLinea.Valor = .Item("sdde_item_nume")
                If usrPadreDeta.cmbProdRazaExt.Valor Is DBNull.Value Then
                    usrPadreDeta.cmbProdRazaExt.Valor = usrCriador1.RazaId 'bbbbbbbb ok
                End If
                If usrMadreDeta.cmbProdRazaExt.Valor Is DBNull.Value Then
                    usrMadreDeta.cmbProdRazaExt.Valor = usrCriador1.RazaId 'bbbbbbb ok
                End If
                If usrMadreDetaBov.cmbProdRazaExt.Valor Is DBNull.Value Then
                    usrMadreDetaBov.cmbProdRazaExt.Valor = usrCriador1.RazaId 'bbbbbbbbbbb ok
                End If
            End With

            mCargarGrillaPadres()

            mSetearEditor(mstrTablaDeta, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdDeta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDeta.EditItemIndex = -1
            If (grdDeta.CurrentPageIndex < 0 Or grdDeta.CurrentPageIndex >= grdDeta.PageCount) Then
                grdDeta.CurrentPageIndex = 0
            Else
                grdDeta.CurrentPageIndex = E.NewPageIndex
            End If
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
            grdDeta.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Public Sub grdPadresDeta_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdPadresDeta.EditItemIndex = -1
            If (grdPadresDeta.CurrentPageIndex < 0 Or grdPadresDeta.CurrentPageIndex >= grdPadresDeta.PageCount) Then
                grdPadresDeta.CurrentPageIndex = 0
            Else
                grdPadresDeta.CurrentPageIndex = E.NewPageIndex
            End If
            grdPadresDeta.DataSource = mdsDatos.Tables(mstrTablaPadres)
            grdPadresDeta.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarDetalle(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosDeta(pbooAlta)

            mLimpiarDetalle()
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
            grdDeta.DataBind()

            txtLinea.Valor = grdDeta.Items.Count + 1

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Function mCalcularNroLinea() As String
        Dim lintLinea As Integer = 0
        For Each ldr As DataRow In mdsDatos.Tables(mstrTablaDeta).Select
            With ldr
                If .Item("sdde_item_nume") > lintLinea Then
                    lintLinea = .Item("sdde_item_nume")
                End If
            End With
        Next
        Return (Convert.ToString(lintLinea + 1))
    End Function
    Private Sub mGuardarDatosDeta(ByVal pbooAlta As Boolean)

        Dim ldrDatos As DataRow
        Dim lstrProdRP As String
        Dim lintProdNume As Integer
        Dim lintDetaId As Integer

        lintDetaId = 0

        If hdnDetaId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaDeta).NewRow
            lintDetaId = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDeta), "sdde_id")
            ldrDatos.Item("sdde_id") = lintDetaId
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaDeta).Select("sdde_id=" & hdnDetaId.Text)(0)
        End If

        'If hdnEspe.Text = SRA_Neg.Constantes.Especies.Bovinos Then
        '   If usrMadreDetaBov.Valor.ToString = "" Then
        '       Throw New AccesoBD.clsErrNeg("Debe indicar la madre.")
        '   End If
        'Else
        '   If usrMadreDeta.Valor.ToString = "" Then
        '       Throw New AccesoBD.clsErrNeg("Debe indicar la madre.")
        '   End If
        'End If

        If txtLinea.Valor.ToString = "" Then
            'Throw New AccesoBD.clsErrNeg("Debe indicar el N� de L�nea.")
            txtLinea.Valor = mCalcularNroLinea()
        End If

        Select Case hdnEspe.Text
            Case SRA_Neg.Constantes.Especies.Bovinos, SRA_Neg.Constantes.Especies.Equinos, SRA_Neg.Constantes.Especies.Camelidos
                'If usrPadreDeta.Valor Is DBNull.Value Then
                '   Throw New AccesoBD.clsErrNeg("Debe indicar el padre.")
                'End If
                If cmbSetiDeta.Valor Is DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar el tipo de servicio.")
                End If
                If txtServDesdeDeta.Text = "" Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de entrada al servicio.")
                End If
        End Select

        With ldrDatos
            If lintDetaId = 0 Then
                lintDetaId = .Item("sdde_id")
            End If
            .Item("sdde_padre_id") = usrPadreDeta.Valor
            If hdnEspe.Text = SRA_Neg.Constantes.Especies.Bovinos Then
                .Item("sdde_madre_id") = usrMadreDetaBov.Valor
            Else
                .Item("sdde_madre_id") = usrMadreDeta.Valor
            End If
            .Item("sdde_obser") = txtObse.Valor.ToString
            .Item("sdde_servi_desde") = txtServDesdeDeta.Fecha
            .Item("sdde_servi_hasta") = txtServHastaDeta.Fecha
            .Item("sdde_seti_id") = cmbSetiDeta.Valor
            .Item("sdde_item_nume") = txtLinea.Valor
            .Item("_tipo") = cmbSetiDeta.SelectedItem.Text
            .Item("sdde_baja_fecha") = System.DBNull.Value
            .Item("baja_fecha") = System.DBNull.Value
            .Item("_estado") = "Activo"
            If hdnEspe.Text = SRA_Neg.Constantes.Especies.Bovinos Then
                .Item("_madre_rp") = usrMadreDetaBov.txtRPExt.Text
                .Item("_vaca_tatu") = usrMadreDetaBov.txtRPExt.Text
                .Item("sdde_madre_rp") = usrMadreDetaBov.txtRPExt.Text
                If usrMadreDetaBov.txtSraNumeExt.Text = "" Then
                    .Item("_madre_nume") = DBNull.Value
                    .Item("sdde_madre_sra_nume") = DBNull.Value
                Else
                    .Item("_madre_nume") = Convert.ToInt32(usrMadreDetaBov.txtSraNumeExt.Text)
                    .Item("sdde_madre_sra_nume") = Convert.ToInt32(usrMadreDetaBov.txtSraNumeExt.Text)
                End If
            Else
                .Item("_madre_rp") = usrMadreDeta.txtRPExt.Text
                .Item("_vaca_tatu") = usrMadreDeta.txtRPExt.Text
                .Item("sdde_madre_rp") = usrMadreDeta.txtRPExt.Text
                If usrMadreDeta.txtSraNumeExt.Text = "" Then
                    .Item("_madre_nume") = DBNull.Value
                    .Item("sdde_madre_sra_nume") = DBNull.Value
                Else
                    .Item("_madre_nume") = Convert.ToInt32(usrMadreDeta.txtSraNumeExt.Text)
                    .Item("sdde_madre_sra_nume") = Convert.ToInt32(usrMadreDeta.txtSraNumeExt.Text)
                End If
            End If
            If Not cmbSetiDeta.Valor Is DBNull.Value Then
                If cmbSetiDeta.Valor <> SRA_Neg.Constantes.TipoServicios.SiembraMultiple Then
                    .Item("_padre_rp") = usrPadreDeta.txtRPExt.Text
                    .Item("_toro_rp") = usrPadreDeta.txtRPExt.Text
                    .Item("sdde_padre_rp") = usrPadreDeta.txtRPExt.Text
                    If usrPadreDeta.txtSraNumeExt.Text = "" Then
                        .Item("_padre_nume") = DBNull.Value
                        .Item("_toro_nume") = DBNull.Value
                        .Item("sdde_padre_sra_nume") = DBNull.Value
                    Else
                        .Item("_padre_nume") = Convert.ToInt32(usrPadreDeta.txtSraNumeExt.Text)
                        .Item("_toro_nume") = Convert.ToInt32(usrPadreDeta.txtSraNumeExt.Text)
                        .Item("sdde_padre_sra_nume") = Convert.ToInt32(usrPadreDeta.txtSraNumeExt.Text)
                    End If
                End If
            End If

            If Not cmbSetiDeta.Valor Is DBNull.Value Then
                If cmbSetiDeta.Valor = SRA_Neg.Constantes.TipoServicios.SiembraMultiple Then
                    .Item("sdde_padre_id") = DBNull.Value
                    .Item("sdde_padre_sra_nume") = DBNull.Value
                    .Item("sdde_padre_rp") = DBNull.Value
                    .Item("sdde_padre2_id") = DBNull.Value
                    .Item("sdde_padre2_sra_nume") = DBNull.Value
                    .Item("sdde_padre2_rp") = DBNull.Value
                    .Item("sdde_padre3_id") = DBNull.Value
                    .Item("sdde_padre3_sra_nume") = DBNull.Value
                    .Item("sdde_padre3_rp") = DBNull.Value
                    Dim lintI As Integer = 1
                    For Each lDrPadre As DataRow In mdsDatos.Tables(mstrTablaPadres).Select("sdpa_sdde_id=" & lintDetaId)
                        Select Case lintI
                            Case 1
                                .Item("sdde_padre_id") = lDrPadre.Item("sdpa_padre_id")
                                .Item("sdde_padre_sra_nume") = lDrPadre.Item("sdpa_padre_sra_nume")
                                .Item("sdde_padre_rp") = lDrPadre.Item("sdpa_padre_rp")
                            Case 2
                                .Item("sdde_padre2_id") = lDrPadre.Item("sdpa_padre_id")
                                .Item("sdde_padre2_sra_nume") = lDrPadre.Item("sdpa_padre_sra_nume")
                                .Item("sdde_padre2_rp") = lDrPadre.Item("sdpa_padre_rp")
                            Case 3
                                .Item("sdde_padre3_id") = lDrPadre.Item("sdpa_padre_id")
                                .Item("sdde_padre3_sra_nume") = lDrPadre.Item("sdpa_padre_sra_nume")
                                .Item("sdde_padre3_rp") = lDrPadre.Item("sdpa_padre_rp")
                        End Select
                        lintI = lintI + 1
                    Next
                End If
            End If
        End With

        If hdnDetaId.Text = "" Then
            mdsDatos.Tables(mstrTablaDeta).Rows.Add(ldrDatos)
            mdsDatos.Tables(mstrTablaDeta).DefaultView.Sort() = "sdde_item_nume"
            hdnDetaId.Text = lintDetaId
        End If

        If Not cmbSetiDeta.Valor Is DBNull.Value Then
            If cmbSetiDeta.Valor <> SRA_Neg.Constantes.TipoServicios.SiembraMultiple Then
                For Each lDr As DataRow In mdsDatos.Tables(mstrTablaPadres).Select("sdpa_sdde_id=" & lintDetaId)
                    With lDr
                        .Delete()
                    End With
                Next
                If usrPadreDeta.txtRPExt.Text <> "" Or usrPadreDeta.txtSraNumeExt.Text <> "" Then
                    mAgregarPadre(False)
                End If
            End If
        End If

        hdnDetaCarga.Text = "S"

    End Sub

    Private Sub mObtenerDatosProducto(ByVal pstrProdId As String, ByRef pstrProdRP As String, ByRef pintProdNume As Integer)
        Try
            Dim lstrCmd As String
            Dim ldsDatos As DataSet

            lstrCmd = "exec productos_consul " & pstrProdId
            ldsDatos = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

            If ldsDatos.Tables(0).Rows.Count = 0 Then
                pintProdNume = ""
                pstrProdRP = ""
            Else
                With ldsDatos.Tables(0).Rows(0)
                    pintProdNume = .Item("prdt_sra_nume")
                    pstrProdRP = .Item("prdt_rp").ToString
                End With
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiarDetalle()
        hdnDetaId.Text = ""
        hdnPadreId.Text = ""
        hdnFiltroProdCriaId.Text = ""
        hdnItem.Text = ""
        txtServDesdeDeta.Text = ""
        txtServHastaDeta.Text = ""
        txtObse.Text = ""
        cmbSetiDeta.Limpiar()
        txtLinea.Text = ""
        usrPadreDeta.Limpiar()
        usrMadreDeta.Limpiar()
        usrMadreDetaBov.Limpiar()
        mCargarGrillaPadres()
        mSetearEditor(mstrTablaDeta, True)
    End Sub
#End Region

#Region "Eventos de Controles"
    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub
    'Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
    '    mBaja()
    'End Sub
    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi()
    End Sub
    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub
    Private Sub lnkCabecera_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub
    Private Sub lnkDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDeta.Click
        mShowTabs(2)
    End Sub
    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        mLimpiarFiltros()
    End Sub

    Private Sub btnLimpDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDeta.Click
        mLimpiarDetalle()
    End Sub
    Private Sub btnBajaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDeta.Click
        Try
            With mdsDatos.Tables(mstrTablaDeta).Select("sdde_id=" & hdnDetaId.Text)(0)
                .Item("sdde_baja_fecha") = System.DateTime.Now.ToString
                .Item("baja_fecha") = System.DateTime.Now.ToString
                .Item("_estado") = "Inactivo"
            End With
            grdDeta.DataSource = mdsDatos.Tables(mstrTablaDeta)
            grdDeta.DataBind()
            mLimpiarDetalle()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
    Private Sub btnAltaDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDeta.Click
        mActualizarDetalle(True)
    End Sub
    Private Sub btnModiDeta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDeta.Click
        mActualizarDetalle(False)
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        mConsultar()
    End Sub
    Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
        mAgregar()
    End Sub
#End Region

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub btnAltaPadre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaPadre.Click
        mAgregarPadre(True)
    End Sub

    Private Sub btnBajaPadre_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaPadre.Click
        mEliminarPadre()
    End Sub
End Class
End Namespace
