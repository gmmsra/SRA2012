Namespace SRA

Partial Class FallaCaja
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrTabla As String = SRA_Neg.Constantes.gTab_Fallas

   Private mstrConn As String
   Private mstrParaPageSize As Integer

   Private Enum Columnas As Integer
      Id = 0
   End Enum
#End Region

#Region "Inicialización de Variables"
   Private Sub mInicializar()
      Dim x As String = clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
      grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
   End Sub

#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         mInicializar()
         If (Not Page.IsPostBack) Then
            mSetearEventos()
            hdnEmctId.Text = SRA_Neg.Comprobantes.gCentroEmisorId(mstrConn, Request)
            mCargarCombos()
            cmbEmctFil.Valor = hdnEmctId.Text

            mConsultar()

            clsWeb.gInicializarControles(Me, mstrConn)
         Else
            mCargarCentroCostos(False)
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mCargarCombos()
      clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbEmctFil, "@ori_emct_id=" + hdnEmctId.Text)
        clsWeb.gCargarRefeCmb(mstrConn, "emisores_ctros", cmbEmct, "@ori_emct_id=" + hdnEmctId.Text)
        clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMone, "")
		clsWeb.gCargarRefeCmb(mstrConn, "monedas", cmbMonedaFil, "")
		clsWeb.gCargarRefeCmb(mstrConn, "conceptos", cmbConc, "S", "@conc_acti_id=1")

   End Sub

   Private Sub mCargarCentroCostos(ByVal pbooEdit As Boolean)
      Dim lstrCta As String = ""
            If cmbConc.Valor.ToString.Length > 0 Then
                lstrCta = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "conceptos", "_cuenta", "@conc_id=" & cmbConc.Valor)
                lstrCta = lstrCta.Substring(0, 1)
            End If
      If lstrCta <> "" And lstrCta <> "1" And lstrCta <> "2" And lstrCta <> "3" Then
         If Not Request.Form("cmbCcos") Is Nothing Or pbooEdit Then
            clsWeb.gCargarCombo(mstrConn, "centrosc_cuenta_cargar @cuenta=" & lstrCta, cmbCcos, "id", "descrip", "N")
            If Not Request.Form("cmbCcos") Is Nothing Then
                cmbCcos.Valor = Request.Form("cmbCcos").ToString
            End If
         End If
      Else
         cmbCcos.Items.Clear()
      End If
   End Sub

   Private Sub mSetearEventos()
      btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
   End Sub
#End Region

#Region "Seteo de Controles"
   Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
      btnModi.Enabled = Not (pbooAlta)
      btnBaja.Enabled = Not (pbooAlta)
      btnAlta.Enabled = pbooAlta
   End Sub

   Public Sub mCargarDatos(ByVal pstrId As String)
      Dim ldsEsta As DataSet = CrearDataSet(pstrId)

      With ldsEsta.Tables(0).Rows(0)
         hdnId.Text = .Item("fall_id").ToString()
         txtImpo.Valor = .Item("fall_impo")
         cmbMone.Valor = .Item("fall_mone_id")
         cmbEmct.Valor = .Item("fall_emct_id")
         txtFecha.Fecha = .Item("fall_fecha")
         cmbConc.Valor = .Item("fall_conc_id")
         mCargarCentroCostos(True)
         cmbCcos.Valor = .Item("fall_ccos_id")
         If Not .IsNull("fall_baja_fecha") Then
            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("fall_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
         Else
            lblBaja.Text = ""
         End If
      End With

      lblTitu.Text = "Registro Seleccionado: " + cmbEmct.SelectedItem.Text + " - " + txtFecha.Text

      mSetearEditor("", False)
      mMostrarPanel(True)
   End Sub

   Private Sub mAgregar()
      mLimpiar()
      btnModi.Enabled = False
      mMostrarPanel(True)
   End Sub

   Private Sub mLimpiar()
      hdnId.Text = ""
      lblTitu.Text = ""

      txtImpo.Text = ""
      lblBaja.Text = ""
      cmbEmct.Valor = hdnEmctId.Text
      cmbMone.Limpiar()
            txtFecha.Fecha = Today
            cmbCcos.Limpiar()
      cmbConc.Limpiar()

      mSetearEditor("", True)
   End Sub

   Private Sub mCerrar()
      mLimpiar()
      mConsultar()
   End Sub

   Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
      panDato.Visible = pbooVisi
      panBotones.Visible = pbooVisi
      panFiltro.Visible = Not panDato.Visible
      btnAgre.Visible = Not panDato.Visible
      grdDato.Visible = Not panDato.Visible

      If pbooVisi Then
         grdDato.DataSource = Nothing
         grdDato.DataBind()
      End If
   End Sub
#End Region

#Region "Opciones de ABM"
   Private Sub mAlta()
      Try
         Dim lstrId As String
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lstrId = lobjGenerico.Alta()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mModi()
      Try
         Dim ldsEstruc As DataSet = mGuardarDatos()

         Dim lobjGenerico As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, ldsEstruc)
         lobjGenerico.Modi()

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Sub mBaja()
      Try
         Dim lobjGenerico As New SRA_Neg.Generica(mstrConn, Session("sUserId").ToString(), mstrTabla)
         lobjGenerico.Baja(hdnId.Text)

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mGuardarDatos() As DataSet
      mValidarDatos()

      Dim ldsEsta As DataSet = CrearDataSet(hdnId.Text)

      With ldsEsta.Tables(0).Rows(0)
         .Item("fall_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
         .Item("fall_impo") = txtImpo.Valor
         .Item("fall_fecha") = txtFecha.Fecha
         .Item("fall_emct_id") = cmbEmct.Valor
         .Item("fall_mone_id") = cmbMone.Valor
         .Item("fall_ccos_id") = cmbCcos.Valor
         .Item("fall_conc_id") = cmbConc.Valor
      End With
      Return ldsEsta
   End Function

   Private Function CrearDataSet(ByVal pstrId As String) As DataSet
      Dim ldsEsta As DataSet = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

      ldsEsta.Tables(0).TableName = mstrTabla

      Return ldsEsta
   End Function

   Private Sub mValidarDatos()
      clsWeb.gInicializarControles(Me, mstrConn)
      clsWeb.gValidarControles(Me)
   End Sub
#End Region

#Region "Operacion Sobre la Grilla"
   Public Sub mConsultar()
      Try
         Dim lstrCmd As New StringBuilder
         lstrCmd.Append("exec " + mstrTabla + "_consul")
			lstrCmd.Append(" @fall_emct_id=" + cmbEmctFil.Valor.ToString)
			lstrCmd.Append(", @fechadesde=" & clsFormatear.gFormatFecha2DB(txtFechaDesdeFil.Fecha))
			lstrCmd.Append(", @fechahasta=" & clsFormatear.gFormatFecha2DB(txtFechaHastaFil.Fecha))
			lstrCmd.Append(", @mone_id=" + cmbMonedaFil.Valor.ToString)

         clsWeb.gCargarDataGrid(mstrConn, lstrCmd.ToString, grdDato)

         mMostrarPanel(False)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdDato.CurrentPageIndex = E.NewPageIndex
         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
      Try
         mCargarDatos(e.Item.Cells(1).Text)

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub
#End Region

#Region "Eventos"
   Private Sub btnAlta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
      mAlta()
   End Sub

   Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
      mBaja()
   End Sub

   Private Sub btnModi_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
      mModi()
   End Sub

   Private Sub btnLimp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
      mLimpiar()
   End Sub

   Private Sub imgClose_Click(ByVal Sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
      mCerrar()
   End Sub

	Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFiltro()
	End Sub

	Private Sub mLimpiarFiltro()
		txtFechaDesdeFil.Text = ""
		txtFechaHastaFil.Text = ""
		cmbMonedaFil.Limpiar()
		mConsultar()
	End Sub

	Private Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
		Try
			mAgregar()
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnBusc_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
		Try
			grdDato.CurrentPageIndex = 0
			mConsultar()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	'Pantanettig 11/10/2007
	Private Sub btnList_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnList.Click
		Try
			Dim lstrRptName As String
			Dim lintFechaDesde As Integer
			Dim lintFechaHasta As Integer

			lstrRptName = "FallaCaja"

			If txtFechaDesdeFil.Fecha.ToString = "" Then
				lintFechaDesde = 0
			Else
				lintFechaDesde = CType(clsFormatear.gFormatFechaString(txtFechaDesdeFil.Text, "Int32"), Integer)
			End If

			If txtFechaHastaFil.Fecha.ToString = "" Then
				lintFechaHasta = 0
			Else
				lintFechaHasta = CType(clsFormatear.gFormatFechaString(txtFechaHastaFil.Text, "Int32"), Integer)
			End If

			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			lstrRpt += "&fall_emct_id=" + cmbEmctFil.Valor.ToString
			lstrRpt += "&fechadesde=" + lintFechaDesde.ToString
			lstrRpt += "&fechahasta=" + lintFechaHasta.ToString
			lstrRpt += "&mone_id=" + cmbMonedaFil.Valor.ToString

			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region
End Class
End Namespace
