Namespace SRA

Partial Class Consulta_Tramites_pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

   'This call is required by the Web Form Designer.
   <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub
   Protected WithEvents lblTotal As System.Web.UI.WebControls.Label
   Protected WithEvents txtTotal As NixorControls.NumberBox
   Protected WithEvents lblTotalSel As System.Web.UI.WebControls.Label
   Protected WithEvents txtTotalSel As NixorControls.NumberBox


   Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
      'CODEGEN: This method call is required by the Web Form Designer
      'Do not modify it using the code editor.
      InitializeComponent()
   End Sub

#End Region

#Region "Definición de Variables"
    Private mstrConn As String
    Private mstrTitulo As String
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaComprobACuenta As String = SRA_Neg.Constantes.gTab_ComprobACuenta
    Public mstrProductoId As String
    Public mstrCriaId As String
    Private mstrParaPageSize As Integer

   Private Enum Columnas As Integer
      Chk = 0
      SactId = 1
      Numero = 2
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)
         If Not Page.IsPostBack Then
            mCargarCombos()
            mConsultar()
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Sub mCargarCombos()
      clsWeb.gCargarCombo(mstrConn, "rg_tramites_tipos_cargar", cmbTipoFil, "id", "descrip", "T")
   End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         grdConsulta.CurrentPageIndex = E.NewPageIndex

         mConsultar()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
    End Sub
   
    Public Sub mConsultar()
        Dim lstrCmd As New StringBuilder
        Dim ds As DataSet
        Dim ldsDatos As DataSet

        mstrProductoId = Request.QueryString("prdt_id")
        mstrCriaId = Request.QueryString("prdt_cria_id")


        lstrCmd.Append("exec tramitesPorProducto_busq ")
        lstrCmd.Append("@trpr_prdt_id=" + clsSQLServer.gFormatArg(mstrProductoId, SqlDbType.Int))

        If mstrCriaId <> "" Then
            lstrCmd.Append(",@cria_id=" + clsSQLServer.gFormatArg(mstrCriaId, SqlDbType.Int))
        End If



        If txtFechaDesdeFil.Text <> "" Then
            lstrCmd.Append(",@fecha_desde=" + clsSQLServer.gFormatArg(txtFechaDesdeFil.Text, SqlDbType.SmallDateTime))
        End If
        If txtFechaHastaFil.Text <> "" Then
            lstrCmd.Append(",@fecha_hasta=" + clsSQLServer.gFormatArg(txtFechaHastaFil.Text, SqlDbType.SmallDateTime))
        End If
        If cmbTipoFil.Valor.ToString <> "" Then
            lstrCmd.Append(",@tipo=" + clsSQLServer.gFormatArg(cmbTipoFil.Valor.ToString, SqlDbType.Int))
        End If
        If cmbEstaFil.Valor.ToString <> "" Then
            lstrCmd.Append(",@estado=" + clsSQLServer.gFormatArg(cmbEstaFil.Valor.ToString, SqlDbType.VarChar))
        End If

        ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

        grdConsulta.DataSource = ds
        grdConsulta.DataBind()
        ds.Dispose()
    End Sub

    Sub mLimpiarFiltros()
        txtFechaDesdeFil.Text = ""
        txtFechaHastaFil.Text = ""
        cmbEstaFil.Limpiar()
        cmbTipoFil.Limpiar()
    End Sub

#End Region

Private Sub btnLimpFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpFil.Click
    mLimpiarFiltros()
End Sub

Private Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
    mConsultar()
End Sub

    Private Sub btnList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnList.Click
        Try
            Dim lstrRptName As String = "ListadoTramites"
            Dim mstrFiltros As String = Request.QueryString("prdt_id")
            Dim vStrFiltros() As String
            Dim sFil As String

            sFil = mstrFiltros.Replace("'", "")
            vStrFiltros = sFil.Split(",")
            Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

            lstrRpt += "&trpr_prdt_id=" + vStrFiltros(0)
            If txtFechaDesdeFil.Fecha.ToString <> "" Then
                lstrRpt += "&fecha_desde=" + txtFechaDesdeFil.Fecha
            End If
            If txtFechaHastaFil.Fecha.ToString <> "" Then
                lstrRpt += "&fecha_hasta=" + txtFechaHastaFil.Fecha
            End If
            lstrRpt += "&tipo=" + IIf(cmbTipoFil.Valor.ToString = "", "0", cmbTipoFil.Valor.ToString)
            'lstrRpt += "&estado=" + IIf(cmbEstaFil.Valor.ToString = "T", "0", IIf(cmbEstaFil.Valor.ToString = "I", "1", "2"))
            lstrRpt += "&estado=" + cmbEstaFil.Valor.ToString

            If txtFechaDesdeFil.Fecha.ToString <> "" Or txtFechaHastaFil.Fecha.ToString <> "" Then
                If txtFechaDesdeFil.Fecha.ToString > txtFechaHastaFil.Fecha.ToString Then
                    Throw New AccesoBD.clsErrNeg("La fecha hasta debe ser superior  a la fecha desde.")
                End If
            End If




            lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
            Response.Redirect(lstrRpt)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class

End Namespace
