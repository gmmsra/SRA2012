<%@ Reference Control="~/controles/usrsociosfiltro.ascx" %>
<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="uc1" TagName="SOCFIL" Src="controles/usrSociosFiltro.ascx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.EnvioMails" CodeFile="EnvioMails.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Env�o de Mails a Socios</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript">
		function mModelos(pMode)
		{
			document.all("txtAsun").value = LeerCamposXML("mails_modelos",pMode.value,"mamo_asun");
		}
		function mRptFiltrosLimpiar()
		{
			EjecutarMetodoXML("Utiles.RptFiltrosLimpiar",document.all("hdnRptId").value);
		}		
		</script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');" rightMargin="0"
		onunload="javascript:mRptFiltrosLimpiar();">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<TBODY>
					<tr>
						<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
						<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
						<td vAlign="middle" align="center">
							<!----- CONTENIDO ----->
							<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" cellSpacing="0" cellPadding="0" border="0">
								<TBODY>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<TR>
										<TD style="HEIGHT: 25px" vAlign="bottom" colSpan="3" height="25"><asp:label id="lblTituAbm" runat="server" cssclass="opcion">Env�o de Mails a Socios</asp:label></TD>
									</TR>
									<TR>
										<TD width="100%" colSpan="3"></TD>
									</TR>
									<!--- filtro --->
									<TR>
										<TD vAlign="top" colSpan="3"><asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" Width="100%" BorderWidth="0"
												BorderStyle="Solid">
												<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
													<TR>
														<TD style="WIDTH: 100%">
															<TABLE id="Table3" cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD style="HEIGHT: 8px" width="24"></TD>
																	<TD style="HEIGHT: 8px" width="42"></TD>
																	<TD style="HEIGHT: 8px" width="26"></TD>
																	<TD style="HEIGHT: 8px"></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnCons" runat="server" BorderStyle="None" ImageUrl="imagenes/btnImpr.jpg" BackColor="Transparent"
																			ImageOver="btnCons2.gif" ImageBoton="btnCons.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																			ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BotonImagen></TD>
																	<TD style="HEIGHT: 8px" width="26">
																		<CC1:BotonImagen id="btnLimpFil" runat="server" BorderStyle="None" ImageUrl="imagenes/limpiar.jpg"
																			BackColor="Transparent" ImageOver="btnLimp2.gif" ImageBoton="btnLimp.gif" BtnImage="edit.gif"
																			OutImage="del.gif" CambiaValor="False" ImagesUrl="imagenes/" IncludesUrl="includes/" ForeColor="Transparent"></CC1:BotonImagen></TD>
																</TR>
																<TR>
																	<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
																	<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
																	<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" width="7" border="0"></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
													<TR>
														<TD>
															<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																<TR>
																	<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
																	<TD><!-- FOMULARIO -->
																		<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			</TR>
																			<TR>
																				<TD style="WIDTH: 100%; HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg">
																					<UC1:SOCFIL id="usrSocFil" runat="server"></UC1:SOCFIL></TD>
																			</TR>
																			<TR>
																				<TD style="HEIGHT: 10px" align="right" background="imagenes/formfdofields.jpg"></TD>
																			</TR>
																		</TABLE>
																	</TD>
																	<TD width="2" background="imagenes/formde.jpg"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
																</TR>
																<TR>
																	<TD colSpan="3" height="10"></TD>
																</TR>
																<TR>
																	<TD vAlign="top" align="center" colSpan="3"><!--DIV id="panGrid" style="BORDER-RIGHT: #003784 0px solid; BORDER-TOP: #003784 0px solid; OVERFLOW: auto; BORDER-LEFT: #003784 0px solid; WIDTH: 560px; BORDER-BOTTOM: #003784 0px solid; HEIGHT: 100%"-->
																		<asp:datagrid id="grdCons" runat="server" BorderStyle="None" BorderWidth="1px" AutoGenerateColumns="False"
																			ItemStyle-Height="5px" PageSize="10" OnPageIndexChanged="DataGrid_Page" GridLines="None" HorizontalAlign="Center"
																			AllowPaging="True" width="100%" CellPadding="1" CellSpacing="1">
																			<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
																			<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
																			<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
																			<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
																			<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
																			<FooterStyle CssClass="footer"></FooterStyle>
																			<Columns>
																				<asp:TemplateColumn Visible="false">
																					<HeaderStyle Width="5%"></HeaderStyle>
																					<ItemTemplate>
																						<asp:LinkButton id="LinkButton1" runat="server" Text="Editar" CausesValidation="false" CommandName="Edit"
																							Height="5">
																							<img src='images/edit.gif' border="0" alt="Editar Registro" style="cursor:hand;" vspace="0" />
																						</asp:LinkButton>
																					</ItemTemplate>
																				</asp:TemplateColumn>
																			</Columns>
																			<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
																		</asp:datagrid></TD>
																</TR>
																<TR>
																	<TD colSpan="3" height="10"></TD>
																</TR>
																<TR>
																	<TD vAlign="middle" align="left" colSpan="3"><A id="editar" name="editar"></A>
																		<asp:panel id="panGrab" runat="server" cssclass="titulo" BorderStyle="Solid" BorderWidth="1px"
																			Width="100%" Visible="False" BorderColor="#003784">
																			<TABLE id="Table4" style="WIDTH: 99%" cellSpacing="0" cellPadding="0" width="535" align="center"
																				border="0">
																				<TR>
																					<TD style="WIDTH: 100%" colSpan="2" height="8"></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 100%" align="left" colSpan="2">
																						<asp:label id="lblMode" runat="server" cssclass="titulo">Modelo de Mail</asp:label></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 100%" align="left" colSpan="2">
																						<cc1:combobox class="combo" id="cmbMode" runat="server" Width="331px" Obligatorio="False"></cc1:combobox></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 100%" colSpan="2" height="8"></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 100%" align="left" colSpan="2">
																						<asp:label id="lblAsun" runat="server" cssclass="titulo">Asunto</asp:label></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 100%" align="left" colSpan="2">
																						<cc1:TextBoxtab id="txtAsun" runat="server" cssclass="cuadrotexto" Width="80%"></cc1:TextBoxtab></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 100%" colSpan="2" height="8"></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 85%" align="left"></TD>
																					<TD style="WIDTH: 15%" vAlign="bottom" align="center">
																						<CC1:BotonImagen id="btnEnvi" runat="server" BorderStyle="None" ImageUrl="imagenes/btnEnvi.gif" BackColor="Transparent"
																							ImageOver="btnEnvi2.gif" ImageBoton="btnEnvi.gif" BtnImage="edit.gif" OutImage="del.gif" CambiaValor="False"
																							ImagesUrl="imagenes/" IncludesUrl="../includes/" ForeColor="Transparent" ImageDisable="btnEnvi0.gif"></CC1:BotonImagen></TD>
																				</TR>
																				<TR>
																					<TD style="WIDTH: 100%" colSpan="2" height="8"></TD>
																				</TR>
																			</TABLE>
																		</asp:panel></TD>
																</TR>
															</TABLE>
														</TD>
													</TR>
												</TABLE>
											</asp:panel></TD>
									</TR>
									<!---fin filtro --->
									<TR>
										<TD vAlign="middle" colSpan="3"></TD>
									</TR>
								</TBODY>
							</TABLE>
							<!--- FIN CONTENIDO --->
						</td>
						<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
					</tr>
					<tr>
						<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
						<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
						<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
					</tr>
				</TBODY>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server" Width="140px"></ASP:TEXTBOX>
				<ASP:TEXTBOX id="hdnRptId" runat="server" Width="140px"></ASP:TEXTBOX></DIV>
		</form>
		</TR></TBODY></TABLE></TR></TBODY></TABLE></TR></TBODY>
		<P></P>
		<DIV></DIV>
		</TR></TBODY></TABLE></TR></TBODY></TABLE>
		<script language="javascript">
		if (document.all["editar"]!= null && document.all("panGrab")!=null )
			document.location='#editar';
		</script>
		</FORM>
	</BODY>
</HTML>
