<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.rangos_Pop" CodeFile="Rangos_Pop.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>Rangos</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/SRA.css" type="text/css" rel="stylesheet">
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 100%" border="0">
				<TR height="100%">
					<TD vAlign="top" colSpan="2"><asp:datagrid id="grdConsulta" runat="server" width="100%" BorderStyle="None" AllowPaging="true"
							BorderWidth="1px" HorizontalAlign="Center" CellSpacing="1" GridLines="None" CellPadding="1" PageSize="500" ItemStyle-Height="5px"
							AutoGenerateColumns="False">
							<SelectedItemStyle Font-Size="X-Small"></SelectedItemStyle>
							<EditItemStyle Font-Size="X-Small" Wrap="False"></EditItemStyle>
							<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
							<ItemStyle CssClass="item2"></ItemStyle>
							<HeaderStyle Font-Size="X-Small" Height="20px" CssClass="header"></HeaderStyle>
							<FooterStyle CssClass="footer"></FooterStyle>
							<Columns>
								<asp:TemplateColumn>
									<HeaderStyle Width="3%"></HeaderStyle>
									<ItemTemplate>
										<asp:CheckBox ID="chkSel" Runat="server"></asp:CheckBox>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:BoundColumn Visible="False" DataField="raac_id"></asp:BoundColumn>
								<asp:BoundColumn DataField="raac_desc" HeaderText="Descripción"></asp:BoundColumn>
							</Columns>
							<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
						</asp:datagrid></TD>
				</TR>
				<TR>
					<TD style="WIDTH: 100%"></TD>
					<TD style="WIDTH: 100%" align="right"><asp:button id="btnAceptar" runat="server" cssclass="boton" Width="80px" Text="Aceptar"></asp:button>&nbsp;
					</TD>
				</TR>
			</TABLE>
			<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
		</form>
	</BODY>
</HTML>
