<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Register TagPrefix="cc1" Namespace="NixorControls" Assembly="NixorControls" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.ChequesPasaje" CodeFile="ChequesPasaje.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>PASAJE CHEQUES POSTDATADOS</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultgrupntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
	</HEAD>
	<BODY onload="gSetearTituloFrame('');" class="pagina" leftMargin="5" topMargin="5" rightMargin="0">
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="100%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%; HEIGHT: 130px" border="0" cellpadding="0" cellspacing="0">
							<TR>
								<TD width="100%" colSpan="3"></TD>
							</TR>
							<TR>
								<TD width="5"></TD>
								<TD colSpan="2" style="HEIGHT: 25px" vAlign="bottom" height="25">
									<asp:label cssclass="opcion" id="lblTituAbm" runat="server" width="391px">Cheques Postdatados</asp:label>
								</TD>
							</TR>
							<TR>
								<TD></TD>
								<TD vAlign="top" colSpan="2">
									<asp:panel id="panFiltro" runat="server" cssclass="titulo" Visible="True" BorderWidth="0" Width="100%"
										BorderStyle="Solid">
										<TABLE id="TableFil" style="WIDTH: 100%" cellSpacing="0" cellPadding="0" align="left" border="0">
											<TR>
												<TD style="WIDTH: 100%">
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD style="HEIGHT: 8px" width="24"></TD>
															<TD style="HEIGHT: 8px" width="42"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px"></TD>
															<TD style="HEIGHT: 8px" width="26"></TD>
															<TD style="HEIGHT: 8px" width="50"></TD>
														</TR>
														<TR>
															<TD style="HEIGHT: 8px" width="24"><IMG height="25" src="imagenes/formfle.jpg" width="24" border="0"></TD>
															<TD style="HEIGHT: 8px" width="42"><IMG height="25" src="imagenes/formtxfiltro.jpg" width="113" border="0"></TD>
															<TD style="HEIGHT: 8px" width="26"><IMG height="25" src="imagenes/formcap.jpg" width="26" border="0"></TD>
															<TD style="HEIGHT: 8px" background="imagenes/formfdocap.jpg" colSpan="3"><IMG height="25" src="imagenes/formfdocap.jpg" width="7" border="0"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
											<TR>
												<TD>
													<TABLE cellSpacing="0" cellPadding="0" width="100%" border="0">
														<TR>
															<TD width="3" background="imagenes/formiz.jpg"><IMG height="30" src="imagenes/formiz.jpg" width="3" border="0"></TD>
															<TD><!-- FOMULARIO -->
																<TABLE cellSpacing="0" cellPadding="0" width="100%" background="imagenes/formfdofields.jpg"
																	border="0">
																	<TR>
																		<TD style="WIDTH: 10%; HEIGHT: 14px" vAlign="top" align="right" background="imagenes/formfdofields.jpg">
																			<asp:Label id="lblFechaFil" runat="server" cssclass="titulo">Fecha:</asp:Label>&nbsp;
																		</TD>
																		<TD style="HEIGHT: 27px" colSpan="2">
																			<cc1:DateBox id="txtFechaFil" runat="server" cssclass="cuadrotexto" Width="70px"></cc1:DateBox></TD>
																		<TD style="HEIGHT: 27px" align="right">
																			<asp:Button id="btnBuscar" tabIndex="2" runat="server" cssclass="boton" Width="80px" text="Buscar"></asp:Button></TD>
																	</TR>
																	<TR>
																		<TD style="WIDTH: 1.81%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 12.69%" align="right" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																		<TD style="WIDTH: 88%" width="8" background="imagenes/formdivfin.jpg" height="2"><IMG height="2" src="imagenes/formdivfin.jpg" width="1"></TD>
																	</TR>
																</TABLE>
															</TD>
															<TD width="2" background="imagenes/formde.jpg" height="10"><IMG height="2" src="imagenes/formde.jpg" width="2" border="0"></TD>
														</TR>
														<TR>
															<TD width="3" height="10"></TD>
															<TD align="right" height="10"></TD>
															<TD width="2" height="10"></TD>
														</TR>
													</TABLE>
												</TD>
											</TR>
										</TABLE> <!---fin filtro --->
									</asp:panel>
								</TD>
							</TR>
							<TR>
								<TD colSpan="3" height="10"></TD>
							</TR>
							<!---fin filtro --->
							<TR>
								<TD></TD>
								<TD vAlign="top" align="center" colSpan="2"><asp:datagrid id="grdDato" runat="server" width="100%" BorderWidth="1px" BorderStyle="None"
										OnPageIndexChanged="DataGrid_Page" AutoGenerateColumns="False" CellPadding="1" GridLines="None" CellSpacing="1"
										HorizontalAlign="Center" AllowPaging="True">
										<FooterStyle CssClass="footer"></FooterStyle>
										<AlternatingItemStyle CssClass="item"></AlternatingItemStyle>
										<ItemStyle Height="5px" CssClass="item2"></ItemStyle>
										<HeaderStyle Height="20px" CssClass="header"></HeaderStyle>
										<Columns>
											<asp:TemplateColumn HeaderText="Incl.">
												<HeaderStyle Width="3%"></HeaderStyle>
												<ItemTemplate>
													<asp:CheckBox ID="chkSel" Runat="server"></asp:CheckBox>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:BoundColumn Visible="False" DataField="cheq_id"></asp:BoundColumn>
											<asp:BoundColumn DataField="banc_desc" HeaderText="Banco"></asp:BoundColumn>
											<asp:BoundColumn DataField="cheq_nume" HeaderText="Nro.Cheque"></asp:BoundColumn>
											<asp:BoundColumn DataField="cheq_teor_depo_fecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="5%"></asp:BoundColumn>
											<asp:BoundColumn DataField="clie_apel" HeaderText="Cliente"></asp:BoundColumn>
											<asp:BoundColumn DataField="comp_nume" HeaderText="Acuse"></asp:BoundColumn>
											<asp:BoundColumn DataField="comp_fecha" HeaderText="Fecha Comp" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="5%"></asp:BoundColumn>
											<asp:BoundColumn DataField="paco_impo" HeaderText="Importe" DataFormatString="{0:F2}" HeaderStyle-HorizontalAlign="Right"
												ItemStyle-HorizontalAlign="Right"></asp:BoundColumn>
										</Columns>
										<PagerStyle Font-Size="X-Small" HorizontalAlign="Right" CssClass="pager" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></TD>
							</TR>
							<TR height="10">
								<TD></TD>
							</TR>
							<TR>
								<TD></TD>
								<TD align="center" colSpan="2">
									<DIV>
										<ASP:PANEL id="panBotones" Runat="server">
											<TABLE width="100%">
												<TR height="30">
													<TD align="center"><A id="editar" name="editar"></A>
														<asp:Button id="btnAlta" runat="server" cssclass="boton" Width="90px" Text="Procesar" CausesValidation="False"></asp:Button>&nbsp;&nbsp;
													</TD>
												</TR>
											</TABLE>
										</ASP:PANEL>
									</DIV>
								</TD>
							</TR>
						</TABLE>
						<!--- FIN CONTENIDO --->
					</td>
					<td width="13" background="imagenes/recde.jpg"><img src="imagenes/recde.jpg" border="0" WIDTH="13" HEIGHT="10"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
			<!----------------- FIN RECUADRO ----------------->
			<DIV style="DISPLAY: none">
				<ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX>
			</DIV>
		</form>
		<SCRIPT language="javascript">
		if (document.all["editar"]!= null)
			document.location='#editar';
		</SCRIPT>
	</BODY>
</HTML>
