<%@ Reference Page="~/FormGenerico.aspx" %>
<%@ Page Language="vb" AutoEventWireup="false" Inherits="SRA.consulta_arbol" CodeFile="consulta_arbol.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>
			<%=mstrTitulo%>
		</title>
		<meta content="Microsoft Visual Studio 7.0" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaumltconcntScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="stylesheet/sra.css" type="text/css" rel="stylesheet">
		<script language="JavaScript" src="includes/utiles.js"></script>
		<script language="JavaScript" src="includes/paneles.js"></script>
	</HEAD>
	<BODY class="pagina" leftMargin="5" topMargin="5" onload="gSetearTituloFrame('');gAbrirArbol();"
		rightMargin="0">
		<SCRIPT src="includes/treeview.js"></SCRIPT>
		<form id="frmABM" method="post" runat="server">
			<!------------------ RECUADRO ------------------->
			<table cellSpacing="0" cellPadding="0" width="97%" align="center" border="0">
				<tr>
					<td width="9"><IMG height="10" src="imagenes/recsupiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recsup.jpg"><IMG height="10" src="imagenes/recsup.jpg" width="9" border="0"></td>
					<td width="13"><IMG height="10" src="imagenes/recsupde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9" background="imagenes/reciz.jpg"><IMG height="10" src="imagenes/reciz.jpg" width="9" border="0"></td>
					<td vAlign="middle" align="center">
						<!----- CONTENIDO ----->
						<TABLE id="Table1" style="WIDTH: 100%" border="0">
							<TR>
								<TD vAlign="middle" align="right"><asp:imagebutton id="Imagebutton1" runat="server" CausesValidation="False" ToolTip="Cerrar" ImageUrl="imagenes/close3.bmp"></asp:imagebutton>&nbsp;
								</TD>
							</TR>
							<TR>
								<TD vAlign="top" height="30"><asp:label id="lblTituAbm" runat="server" cssclass="titulo"></asp:label></TD>
							</TR>
							<TR height="100%">
								<TD vAlign="top">
									<SCRIPT language="javascript">
									<% mArmarArbolPedigree %>									
									</SCRIPT>
									<A href="http://www.treemenu"></A>
									<SCRIPT>
									initializeDocument()								
									</SCRIPT>
								</TD>
							</TR>
							<TR>
								<TD vAlign="bottom" align="right" height="40"><BUTTON class="boton" id="btnAbrir" style="WIDTH: 90px" type="button" runat="server" value="Abrir Todo">Abrir 
										Todo</BUTTON>&nbsp;&nbsp; <BUTTON class="boton" id="btnCerrar" style="WIDTH: 90px" type="button" runat="server" value="Cerrar Todo">
										Cerrar Todo</BUTTON>&nbsp;&nbsp;&nbsp;
								</TD>
							</TR>
						</TABLE>
						<DIV style="DISPLAY: none"><ASP:TEXTBOX id="lblMens" runat="server"></ASP:TEXTBOX></DIV>
						<!--- FIN CONTENIDO ---></td>
					<td width="13" background="imagenes/recde.jpg"><IMG height="10" src="imagenes/recde.jpg" width="13" border="0"></td>
				</tr>
				<tr>
					<td width="9"><IMG height="15" src="imagenes/recinfiz.jpg" width="9" border="0"></td>
					<td background="imagenes/recinf.jpg"><IMG height="15" src="imagenes/recinf.jpg" width="13" border="0"></td>
					<td width="13"><IMG height="15" src="imagenes/recinfde.jpg" width="13" border="0"></td>
				</tr>
			</table>
		</form>
	</BODY>
</HTML>
