'===========================================================================
' Este archivo se modific� como parte de una conversi�n de proyecto Web de ASP.NET 2.0.
' El nombre de clase se cambi� y la clase se modific� para heredar dgLeerControle la clase base abstracta 
' en el archivo 'App_Code\Migrated\Stub_Productos_aspx_vb.vb'.
' Durante el tiempo de ejecuci�n, esto permite que otras clases de la aplicaci�n Web se enlacen y obtengan acceso
' a la p�gina de c�digo subyacente ' mediante la clase base abstracta.
' La p�gina de contenido asociada 'Productos.aspx' tambi�n se modific� para hacer referencia al nuevo nombre de clase.
' Para obtener m�s informaci�n sobre este modelo de c�digo, consulte la p�gina http://go.microsoft.com/fwlink/?LinkId=46995 
'===========================================================================
' Dario 2015-01-23 para que no pise el rp 
' Dario 2015-02-26 Se elimina la tabla mstrTablaAnalisis del dataset a modificar para que no genere filas en analisis vacias
' GSZ 2015-04 se agrego chkDescorne y chkCastrado
Imports System.Data.SqlClient
Imports ReglasValida.Validaciones
Imports SRA_Neg.Utiles


Namespace SRA



'Partial Class Productos
Partial Class Migrated_Productos

Inherits Productos

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents lblProv As System.Web.UI.WebControls.Label
    Protected WithEvents cmbActiProp As NixorControls.ComboBox
    Protected WithEvents lblTarj As System.Web.UI.WebControls.Label

    Protected WithEvents txtsraNumeFil As NixorControls.TextBoxTab
    Protected WithEvents btnAsoc As System.Web.UI.WebControls.Button
    Protected WithEvents btnRela As System.Web.UI.WebControls.Button
    Protected WithEvents lblApodo As System.Web.UI.WebControls.Label
    'Protected WithEvents btnEstados As System.Web.UI.HtmlControls.HtmlButton

    Protected WithEvents lnkDocu As System.Web.UI.WebControls.LinkButton
    Protected WithEvents lblDocumRefer As System.Web.UI.WebControls.Label
    Protected WithEvents txtDocumRefer As NixorControls.TextBoxTab
    Protected WithEvents btnRelaPadre As System.Web.UI.WebControls.Button
    Protected WithEvents trSepara As System.Web.UI.HtmlControls.HtmlTableRow
    Protected WithEvents lblTram As System.Web.UI.WebControls.Label
    Protected WithEvents lblRecuFecha As System.Web.UI.WebControls.Label
    Protected WithEvents lblClieNume As System.Web.UI.WebControls.Label
    Protected WithEvents ObservacionFil As NixorControls.TextBoxTab

    Protected WithEvents hndRazaId As System.Web.UI.WebControls.TextBox
    Protected WithEvents lblRP As System.Web.UI.WebControls.Label

    Protected WithEvents lblTipoAnal As System.Web.UI.WebControls.Label
    Protected WithEvents Label12 As System.Web.UI.WebControls.Label




    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

#Region "Definici�n de Variables"
    Private mstrTabla As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrTablaAnalisis As String = SRA_Neg.Constantes.gTab_ProductosAnalisis
    Private mstrTablaAsociaciones As String = SRA_Neg.Constantes.gTab_ProductosNumeros
    Private mstrTablaDocum As String = SRA_Neg.Constantes.gTab_ProductosDocum
    Private mstrCriadores As String = SRA_Neg.Constantes.gTab_Criadores
    Private mstrClientes As String = SRA_Neg.Constantes.gTab_Clientes
    Private mstrParaPageSize As Integer
    Private mstrCmd As String
    Private mdsDatos As DataSet
    Private mstrds As String
    Private mdsDatosAux As DataSet
    Private mstrConn As String
'    Public mstrProdCtrl As String
    Private mstrProdId As String
    Private mstrValorId As String
'    Public mbooActi As Boolean
    Private mbooInclDeshab As Boolean
    Private mbooSoloBusq As Boolean
    Private mbooValiExis As Boolean
    Private mbooNewCtrlProducto As Boolean
    Private mbooBloqCriaFil As Boolean
    Private mintRazaCruza As Integer
    Private mintInscrip As Integer
'    Public mintProce As Integer
'    Public mintPdinId As Integer
    'campos a filtrar
    Private mstrFilProp As String
    Private mstrFilSraNume As String
    Private mstrFilCriaNume As String
    Private mstrFilRpNume As String
    Private mstrFilNoCriaNume As String
    Private mstrFilCuit As String
    Private mstrFilLaboNume As String
    Private mstrFilTarj As String
    Private mstrFilEnti As String
    Private mstrFilMedioPago As String
    Private mstrFilAgru As String
    Private mstrFilEmpr As String
    Private mstrFilNaciFecha As String
    Private mstrFilRespa As String
    Private mstrFilProd As String
    Private mstrFilMadre As String
    Private mstrFilPadre As String
    Private mstrFilRece As String
    Private mstrFilSexo As String
    Private mstrFilHabiRaza As String
    Private mstrFilEsProp As String
    Private mstrFilEsExtranjero As String
    'valores de los filtros 
    Private mstrClieNume As String
    Private mstrNume As String
    Private mstrNomb As String
    Private mstrRaza As String
    Private mstrCriaNume As String
    Private mstrSexo As String
    'columnas a mostrar 
    Private mstrColNomb As String
    Private mstrColSraNume As String
    Private mstrColRpNume As String
    Private mstrColCriaNume As String
    Private mstrColLaboNume As String
    Private mstrColAsocNume As String
    Private mstrColNaciFecha As String
    Private mstrColRazaSoloCodi As String
    'columnas de la grilla de resultados
    Private mintGrdProdSele As Integer = 0
    Private mintGrdProdEdit As Integer = 1
    Private mintGrdProdId As Integer = 2
    Private mintGrdProdRaza As Integer = 3
    Private mintGrdProdRazaCodi As Integer = 4
    Private mintGrdProdSexo As Integer = 5
    Private mintGrdProdNomb As Integer = 6
    Private mintGrdProdAsoc As Integer = 7
    Private mintGrdProdRegi As Integer = 8
    Private mintGrdProdNume As Integer = 9
    Private mintGrdProdDesc As Integer = 10
    Private mintGrdProdRpNume As Integer = 11
    Private mintGrdProdCria As Integer = 12
    Private mintGrdProdNaci As Integer = 13
    Private mintGrdProdEsta As Integer = 14
    Private mboolSoloConsulta As Boolean = False

    Private vSexo As String
    Private EspecieId As String
    Private Enum Columna As Integer
        ProdSele = 0 'Seleccione
        ProdEdit = 1 'Editar
        ProdId = 2 'Producto ID
        ProdNombre = 7 'Producto Nombre.
        ProdAsocId = 8 'Asociaci�n ID
        ProdNroExtr = 9 'Nro. Extranjero

        ProdRPExtr = 11 'Producto Extranjero.
        ProdSraNume = 13 'Producto SraNume.
        ProdDescrip = 14 'Producto Descripci�n. antes regis = 11

        ProdRP = 15 'Producto RP.
        ProdRazaId = 19 'Raza ID
        ProdRazaCodigo = 20 'Raza C�digo.
        ProdSexo = 21 'Producto Sexo.

    End Enum

#End Region

#Region "Operaciones sobre la Pagina"

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim lstrFil As String
        Try

            mstrConn = clsWeb.gVerificarConexion(Me)

            ' Me.btnPostback.Attributes.Add("style", "VISIBILITY=hidden")
            mInicializarControl()
            mInicializar()
            If (Not Page.IsPostBack) Then
                If mstrProdCtrl = "" Then
                    Page.RegisterClientScriptBlock("expandir", "<SCRIPT>expandir();</SCRIPT>")
                End If

                Session("MilSecc") = Now.Millisecond.ToString
                Session(mstrTabla) = Nothing

                mEstablecerPerfil()
                mSetearMaxLength()
                mSetearEventos()
                mCargarCombos()

                If mstrProdId <> "" Then
                    mCargarDatos(mstrProdId)
                Else
                    If mstrProdCtrl <> "" Then
                        mMostrarPanel(False)
                        lstrFil = usrCriaFil.Valor.ToString & txtNombFil.Text & txtAsoNumeFil.Text & _
                        txtRPFil.Text & txtNaciFechaDesdeFil.Text & txtNaciFechaHastaFil.Text & _
                        txtLaboNumeFil.Text & cmbRazaFil.SelectedValue.ToString & _
                        cmbSexoFil.SelectedValue.ToString & cmbAsocFil.SelectedValue.ToString
                        If lstrFil <> "" And lstrFil <> "0" Then
                            mConsultar(True)
                        End If
                    Else
                        mMostrarPanel(False)
                    End If
                    divBusqAvanzada.Style.Add("display", IIf(usrMadreFil.Visible Or usrPadreFil.Visible Or usrReceFil.Visible, "inline", "none"))

                End If
                clsWeb.gInicializarControles(Me, mstrConn)
            Else
                mCargarFiltrosConsulta()
                If hdnCambioTatuajePop.Text <> "" Then
                    txtRP.Text = hdnCambioTatuajePop.Text
                    txtRPNume.Text = QuitarLetras(hdnCambioTatuajePop.Text)
                End If
                If panDato.Visible Then
                    mdsDatos = Session(mstrTabla)
                    Dim x As String = Session.SessionID
                End If
            End If

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mEstablecerPerfil()
        btnProce.Visible = clsSQLServer.gConsultarPermisoBoton(CType(Opciones.ProcesamientoLote_Procesar, String), (mstrConn), (Session("sUserId").ToString()))
    End Sub

    Private Sub mCargarCombos()

        If mintRazaCruza <> 0 And Not cmbSexoFil.Valor Is DBNull.Value Then
            clsWeb.gCargarCombo(mstrConn, "razas_padres_cargar " & mintRazaCruza & "," & cmbSexoFil.Valor.ToString(), cmbRaza, "id", "descrip", "S")
        Else
            clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip", "S")
        End If
        SRA_Neg.Utiles.gSetearRaza(cmbRaza)
        SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
        clsWeb.gCargarRefeCmb(mstrConn, "implante_ctros", cmbCtroFil, "T")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_cria_tipos", cmbMelli, "S")
        clsWeb.gCargarCombo(mstrConn, "baja_motivos_cargar", cmbBaja, "id", "descrip_codi", "S")
        clsWeb.gCargarRefeCmb(mstrConn, "productos_estados", cmbEsta, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "productos_estados_especificos", cmbEstaFil, "T")

        If cmbAsocFil.Valor = "0" Then
            clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0", cmbAsocFil, "id", "descrip", "T")
        End If
        clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0", cmbAsociacion, "id", "descrip", "T")

        clsWeb.gCargarRefeCmb(mstrConn, "rg_analisis_resul", cmbAnaResul, "S")
        clsWeb.gCargarRefeCmb(mstrConn, "rg_referencias", cmbReferencia, "S")

        'inicializar valores
        If mValorParametro(Request.QueryString("Raza")) <> "" Then
            cmbRazaFil.Valor = mValorParametro(Request.QueryString("Raza"))
            usrCriaFil.cmbCriaRazaExt.Valor = mValorParametro(Request.QueryString("Raza"))
        End If

        If mValorParametro(Request.QueryString("Sexo")) <> "" Then
            cmbSexoFil.Valor = mValorParametro(Request.QueryString("Sexo"))
        End If
    End Sub

    Private Sub mSetearEventos()
        btnProp.Attributes.Add("onclick", "mPropietarios();return false;")
        btnProce.Attributes.Add("onclick", "return(mPorcPeti());")
        If (hdnSoloConsulta.Text.ToLower() = "true" And Session("sUserId").ToString() <> "1") Then
            btnModi.Attributes.Add("onclick", "return(mPorcPeti());")
            btnAlta.Attributes.Add("onclick", "return(mPorcPeti());")
            btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')){return false;} else {mPorcPeti();}")

        End If


        btnInscribirExtr.Attributes.Add("onclick", "return(mPorcPeti());")

        btnCriaCopropiedad.Attributes.Add("onclick", "mCriaCopropiedad();return false;")
        btnPropCopropiedad.Attributes.Add("onclick", "mPropCopropiedad();return false;")
        'btnRelaPadre.Attributes.Add("onclick", "mRelaciones(0);return false;")
        btnRelaHijo.Attributes.Add("onclick", "mRelaciones(1);return false;")
        btnErr.Attributes.Add("onclick", "mErrores();return false;")



    End Sub

    Private Sub mSetearMaxLength()
        Dim lstrProdLong As Object
        Dim lintCol As Integer

        lstrProdLong = clsSQLServer.gCargarLongitudes(mstrConn, mstrTabla)
        txtSraNume.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtNomb.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_nomb")
        txtApod.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_apodo")
        txtRP.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_rp")
        txtCuig.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_cuig")
        txtRPNume.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtDona.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        'txtPesoNacer.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        'txtPesoDeste.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        'txtPesoFinal.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        'txtEpdNacer.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_epd_nacer")
        'txtEpdDeste.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_epd_deste")
        'txtEpdFinal.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_epd_final")
        txtNumeHastaFil.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtNumeDesdeFil.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtNombFil.MaxLength = txtNomb.MaxLength
        txtCuigFil.MaxLength = txtCuig.MaxLength
        txtNumeFil.MaxLength = txtSraNume.MaxLength
        txtRPFil.MaxLength = clsSQLServer.gObtenerLongitud(lstrProdLong, "prdt_rp")
        txtLaboNumeFil.MaxLength = SRA_Neg.Constantes.LongitudesTipos.Entero
        txtAsoNumeFil.MaxLength = txtRP.MaxLength
        txtAnaNume.MaxLength = txtLaboNumeFil.MaxLength
    End Sub

#End Region

#Region "Inicializacion de Variables"

    Public Overrides Sub mInicializar()

        mintProce = ReglasValida.Validaciones.Procesos.Nacimientos
        If Request.QueryString("SoloConsulta") <> "" And Session("sUserId").ToString() <> "1" Then
            mboolSoloConsulta = Convert.ToBoolean(Request.QueryString("SoloConsulta"))
        End If

        hdnSoloConsulta.Text = mboolSoloConsulta.ToString()
        hdnBloqueoCambioTatuaje.Text = mboolSoloConsulta.ToString()

        clsSQLServer.gParaFieldConsul((mstrConn), mstrParaPageSize)
        grdDato.PageSize = Convert.ToInt32(mstrParaPageSize)
        hdnSRA.Text = clsSQLServer.gParametroValorConsul(mstrConn, "para_sra_asoc_id")

        usrPropCriaFil.Tabla = mstrCriadores
        usrPropCriaFil.AutoPostback = False
        usrPropCriaFil.FilClaveUnica = False
        usrPropCriaFil.ColClaveUnica = False
        usrPropCriaFil.Ancho = 790
        usrPropCriaFil.Alto = 510
        usrPropCriaFil.ColDocuNume = True
        usrPropCriaFil.ColCUIT = True
        usrPropCriaFil.ColClaveUnica = True
        usrPropCriaFil.FilClaveUnica = True
        usrPropCriaFil.FilCUIT = True
        usrPropCriaFil.FilDocuNume = True
        usrPropCriaFil.FilTarjNume = False
        usrPropCriaFil.Criador = True
        usrPropCriaFil.FilTipo = "T"

        usrMadreFil.Tabla = mstrTabla
        usrMadreFil.AutoPostback = False
        usrMadreFil.Ancho = 790
        usrMadreFil.Alto = 510
        usrMadreFil.FilSexo = False
        usrMadreFil.Sexo = 0
        usrMadreFil.Inscrip = mintInscrip

        usrPadreFil.Tabla = mstrTabla
        usrPadreFil.AutoPostback = False
        usrPadreFil.Ancho = 790
        usrPadreFil.Alto = 510
        usrPadreFil.FilSexo = False
        usrPadreFil.Sexo = 1
        usrPadreFil.Inscrip = mintInscrip

        usrReceFil.Tabla = mstrTabla
        usrReceFil.AutoPostback = False
        usrReceFil.Ancho = 790
        usrReceFil.Alto = 510
        usrReceFil.FilSexo = False
        usrReceFil.Sexo = 0
        usrReceFil.Inscrip = mintInscrip
    End Sub

    Public Overrides Sub mInicializarControl()
        Dim lstrCriaId As String
        Try

            Dim lstrInclDeshab As String = mValorParametro(Request.QueryString("InclDeshab"))

            hdnBloqueoCambioTatuaje.Text = "false"

            hdnSoloConsulta.Text = "false"
            hdnSRANoInscribe.Text = "false"
            hdnExtranjero.Text = "false"
            hdnMensajeConsulta.Text = ""


            mstrProdCtrl = mValorParametro(Request.QueryString("ctrlId"))
            mbooSoloBusq = mValorParametro(Request.QueryString("SoloBusq")) = "1"
            mbooValiExis = mValorParametro(Request.QueryString("ValiExis")) = "1"
            mbooBloqCriaFil = Not mValorParametro(Request.QueryString("BloqCriaFil")) = "1"
            mbooInclDeshab = IIf(lstrInclDeshab = "1", True, False)


            If Not Request.QueryString("SoloConsulta") Is Nothing And Session("sUserId").ToString() <> "1" Then
                If Request.QueryString("SoloConsulta") <> "" Then
                    mboolSoloConsulta = Convert.ToBoolean(mValorParametro(Request.QueryString("SoloConsulta")))
                    hdnSoloConsulta.Text = mboolSoloConsulta.ToString()

                    If mboolSoloConsulta Then
                        lblMensaje.Visible = True
                        If hdnMensajeConsulta.Text = "" Then
                            lblMensaje.Text = "Pantalla de consulta de Producto"
                            hdnMensajeConsulta.Text = "Pantalla de consulta de Producto"
                        Else
                            lblMensaje.Text = hdnMensajeConsulta.Text
                        End If
                        BloquearBotones(True)
                    Else
                        BloquearBotones(False)
                    End If
                End If
            End If

            mbooNewCtrlProducto = mValorParametro(Request.QueryString("pstNewCtrlProducto")) = "1"

            'If Not Page.IsPostBack Then chkBaja.Checked = mbooInclDeshab
            'RFGG 27/11/2014 - El chkBaja se setea por default en true.
            chkBaja.Checked = True

            If Not Request.QueryString("RazaCruza") Is Nothing Then
                If Request.QueryString("RazaCruza") <> "" Then
                    mintRazaCruza = Convert.ToInt32(mValorParametro(Request.QueryString("RazaCruza")))
                End If
            End If

            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim oEspecies As New SRA_Neg.Especie(mstrConn, Session("sUserId").ToString())
            Dim dtProducto As DataTable
            Dim strEstado As String
            Dim strTipoHBA As String

            If hdnMensajeConsulta.Text <> "" Then
                BloquearBotones(True)
            End If

            mstrValorId = mValorParametro(Request.QueryString("ValorId"))
            mstrProdId = mValorParametro(Request.QueryString("ProdId"))
            mintInscrip = IIf(mValorParametro(Request.QueryString("Inscrip")) <> "1", "0", "1")
            If hdnValorId.Text = "" Then
                hdnValorId.Text = mstrValorId 'pone el id seleccionado en el control de b�squeda
            ElseIf hdnValorId.Text = "-1" Then  'si ya se limpiaron los filtros, ignora el id con el que vino
                mstrValorId = ""
            End If

            mbooActi = mValorParametro(Request.QueryString("a") = "s")

            'campos a filtrar
            If mstrProdCtrl <> "" Then
                mstrFilProp = mValorParametro(Request.QueryString("FilProp"))
                mstrFilSraNume = mValorParametro(Request.QueryString("FilSraNume"))
                mstrFilCriaNume = mValorParametro(Request.QueryString("FilCriaNume"))
                mstrFilRpNume = mValorParametro(Request.QueryString("FilRpNume"))
                mstrFilLaboNume = mValorParametro(Request.QueryString("FilLaboNume"))
                mstrFilNaciFecha = mValorParametro(Request.QueryString("FilNaciFecha"))
                mstrFilMadre = mValorParametro(Request.QueryString("FilMadre"))
                mstrFilPadre = mValorParametro(Request.QueryString("FilPadre"))
                mstrFilRece = mValorParametro(Request.QueryString("FilRece"))
                mstrFilSexo = mValorParametro(Request.QueryString("FilSexo"))
                mstrFilHabiRaza = mValorParametro(Request.QueryString("FilHabiRaza"))
                mstrFilEsProp = mValorParametro(Request.QueryString("EsProp"))
                mstrFilEsExtranjero = mValorParametro(Request.QueryString("EsExtr"))
            Else
                mstrFilProp = "1"
                mstrFilSraNume = "1"
                mstrFilCriaNume = "1"
                mstrFilRpNume = "1"
                mstrFilLaboNume = "1"
                mstrFilNaciFecha = "1"
                mstrFilMadre = "1"
                mstrFilPadre = "1"
                mstrFilRece = "1"
                mstrFilSexo = "1"
                mstrFilHabiRaza = "1"
                mstrFilEsProp = "1"
            End If

            'valores de los filtros
            If Not Page.IsPostBack Then

                'cargar las razas antes de inicializar los valores de los filtros.
                cmbSexoFil.Valor = mValorParametro(Request.QueryString("Sexo"))
                If mintRazaCruza <> 0 And Not cmbSexoFil.Valor Is DBNull.Value Then
                    clsWeb.gCargarCombo(mstrConn, "razas_padres_cargar " & mintRazaCruza & "," & cmbSexoFil.Valor.ToString(), cmbRazaFil, "id", "descrip", "S")
                Else
                    clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip", "S")
                End If

                'hdnCriaFilId.Text = mValorParametro(Request.QueryString("pCriaId"))
                usrCriaFil.Valor = mValorParametro(Request.QueryString("pCriaId"))
                Dim lstrCriaNume As String = mValorParametro(Request.QueryString("pCriaNume"))
                If mValorParametro(Request.QueryString("CriaNume")) <> "" Then
                    lstrCriaNume = mValorParametro(Request.QueryString("CriaNume"))
                End If
                If mValorParametro(Request.QueryString("Raza")) <> "" And lstrCriaNume <> "" And usrCriaFil.Valor.ToString = "" Then
                    lstrCriaId = clsSQLServer.gCampoValorConsul(mstrConn, "criadores_busq @clie_raza=" + mValorParametro(Request.QueryString("Raza")) + ", @clie_cria_nume=" + lstrCriaNume, "cria_id")
                    usrCriaFil.Valor = lstrCriaId
                End If
                txtNombFil.Valor = mValorParametro(Request.QueryString("Nomb"))
                txtAsoNumeFil.Valor = mValorParametro(Request.QueryString("CodiNume"))
                txtRPExtranjeroFil.Valor = mValorParametro(Request.QueryString("RpExtr"))
                txtRPFil.Valor = mValorParametro(Request.QueryString("Rp"))
                'txtRPNumeDesdeFil.Valor = mValorParametro(Request.QueryString("RpNume"))
                'txtRPNumeHastaFil.Valor = mValorParametro(Request.QueryString("RpNume"))
                txtNaciFechaDesdeFil.Text = mValorParametro(Request.QueryString("NaciFecha"))
                txtNaciFechaHastaFil.Text = mValorParametro(Request.QueryString("NaciFecha"))
                cmbRazaFil.Valor = mValorParametro(Request.QueryString("Raza"))
                usrCriaFil.cmbCriaRazaExt.Valor = mValorParametro(Request.QueryString("Raza"))
                clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @mostrar_sra=0", cmbAsocFil, "id", "descrip", "T")
                cmbAsocFil.Valor = mValorParametro(Request.QueryString("Asoc"))
                txtLaboNumeFil.Valor = mValorParametro(Request.QueryString("LaboNume"))
                txtDadorNroSRAFil.Valor = mValorParametro(Request.QueryString("NroDona"))
                txtNumeDesdeFil.Valor = mValorParametro(Request.QueryString("SraNume"))
                txtNumeHastaFil.Valor = mValorParametro(Request.QueryString("SraNume"))
                txtNumeFil.Valor = mValorParametro(Request.QueryString("SraNume"))

                If mstrFilEsProp <> "" Then
                    If mstrFilEsProp = "1" Then
                        usrPropCriaFil.Valor = mValorParametro(Request.QueryString("CriaOrPropId"))
                    Else
                        usrCriaFil.Valor = mValorParametro(Request.QueryString("CriaOrPropId"))
                    End If
                End If

                If mstrFilEsExtranjero = "1" Then
                    cmbNaciFil.Valor = "E" 'Extranjero.
                Else
                    cmbNaciFil.Limpiar()
                End If
            End If

            'columnas a mostrar
            mstrColNomb = mValorParametro(Request.QueryString("ColNomb"))
            mstrColSraNume = mValorParametro(Request.QueryString("ColSraNume"))
            mstrColRpNume = mValorParametro(Request.QueryString("ColRpNume"))
            mstrColCriaNume = mValorParametro(Request.QueryString("ColCriaNume"))
            mstrColLaboNume = mValorParametro(Request.QueryString("ColLaboNume"))
            mstrColAsocNume = mValorParametro(Request.QueryString("ColAsocNume"))
            mstrColNaciFecha = mValorParametro(Request.QueryString("ColNaciFecha"))
            mstrColRazaSoloCodi = mValorParametro(Request.QueryString("ColRazaSoloCodi"))

            If mbooSoloBusq Then
                grdDato.Columns(1).Visible = False
                'btnAgre.Enabled = False
            End If

            'seteo de controles segun filtros
            grdDato.Columns(0).Visible = (mstrProdCtrl <> "")

            If mstrProdCtrl <> "" Then
                grdDato.Columns(mintGrdProdNomb).Visible = (mstrColNomb = "1")
                grdDato.Columns(mintGrdProdNume).Visible = (mstrColSraNume = "1")
                grdDato.Columns(mintGrdProdRpNume).Visible = (mstrColRpNume = "1")
                grdDato.Columns(mintGrdProdCria).Visible = (mstrColCriaNume = "1")
                grdDato.Columns(mintGrdProdAsoc).Visible = (mstrColAsocNume = "1")
                grdDato.Columns(mintGrdProdNaci).Visible = (mstrColNaciFecha = "1")
                grdDato.Columns(mintGrdProdEsta).Visible = (mstrProdCtrl = "")
                If mstrColRazaSoloCodi = "1" Then
                    grdDato.Columns(mintGrdProdRazaCodi).Visible = True
                    grdDato.Columns(mintGrdProdRaza).Visible = False
                End If
            End If

            If mstrProdCtrl <> "" Then
                usrPropCriaFil.Visible = True
                lblPropFil.Visible = True
                usrCriaFil.Visible = True
                lblCriaFil.Visible = True
                txtRPFil.Visible = True
                lblRPNumeFil.Visible = True
                txtLaboNumeFil.Visible = True
                lblLaboNume1Fil.Visible = True
                txtNaciFechaDesdeFil.Visible = (mstrFilNaciFecha = "1")
                txtNaciFechaHastaFil.Visible = (mstrFilNaciFecha = "1")
                lblNaciFechaFil.Visible = (mstrFilNaciFecha = "1")
                lblFechaHastaFil.Visible = (mstrFilNaciFecha = "1")
                usrMadreFil.Visible = (mstrFilMadre = "1")
                lblMadreFil.Visible = (mstrFilMadre = "1")
                usrPadreFil.Visible = (mstrFilPadre = "1")
                lblPadreFil.Visible = (mstrFilPadre = "1")
                cmbSexoFil.Enabled = (mstrFilSexo = "1")
                lblSexoFil.Visible = (mstrFilSexo = "1")
                If cmbRazaFil.Valor <> 0 Then
                    mstrFilHabiRaza = 0
                End If
                cmbRazaFil.Enabled = (mstrFilHabiRaza = "1")
                If mstrFilEsProp <> "" Then
                    usrPropCriaFil.Enabled = (mstrFilEsProp = "0")
                    usrCriaFil.Enabled = (mstrFilEsProp = "1")

                    If mstrFilEsProp = "1" Then
                        usrPropCriaFil.Valor = mValorParametro(Request.QueryString("CriaOrPropId"))
                    Else
                        usrCriaFil.Valor = mValorParametro(Request.QueryString("CriaOrPropId"))
                    End If
                End If
                cmbAsocFil.Enabled = (mValorParametro(Request.QueryString("Asoc")) = "")

                cmbNaciFil.Enabled = Not (mstrFilEsExtranjero = "1")
            Else
                'usrPropClieFil.Visible = True
                usrPropCriaFil.Visible = True
                usrCriaFil.Visible = True
                'optClie.Visible = True
                'optCria.Visible = True
                lblPropFil.Visible = True
                'txtNumeFil.Visible = True
                'lblNumeFil.Visible = True
                usrCriaFil.Visible = True
                lblCriaFil.Visible = True
                txtRPFil.Visible = True
                lblRPNumeFil.Visible = True
                txtLaboNumeFil.Visible = True
                lblLaboNume1Fil.Visible = True
                txtNaciFechaDesdeFil.Visible = True
                txtNaciFechaHastaFil.Visible = True
                lblNaciFechaFil.Visible = True
                lblFechaHastaFil.Visible = True
                usrMadreFil.Visible = True
                lblMadreFil.Visible = True
                usrPadreFil.Visible = True
                lblPadreFil.Visible = True
                'usrReceFil.Visible = True
                'lblReceFil.Visible = True
                cmbSexoFil.Enabled = True
                lblSexoFil.Visible = True
                cmbRazaFil.Enabled = True
                'usrCriaFil.cmbCriaRazaExt.Enabled = True
            End If
            'controles a deshabilitar/habilitar
            usrCriaFil.Activo = mbooBloqCriaFil

            If mstrFilHabiRaza = "1" Then
                cmbRazaFil.Enabled = mbooBloqCriaFil
                'usrCriaFil.cmbCriaRazaExt.Enabled = mbooBloqCriaFil
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Function mValorParametro(ByVal pstrPara As String) As String
        Dim lstrPara As String
        If pstrPara Is Nothing Then
            lstrPara = ""
        Else
            If pstrPara Is System.DBNull.Value Then
                lstrPara = ""
            Else
                lstrPara = pstrPara
            End If
        End If
        Return (lstrPara)
    End Function

#End Region

#Region "Operaciones sobre el DataGrid"

    Public Overrides Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDato.EditItemIndex = -1
            If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
                grdDato.CurrentPageIndex = 0
            Else
                grdDato.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultar(True)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mConsultar(ByVal pbooVisi As Boolean)
        Try
            Dim lstrCmd As String
            Dim lstrPropClie As String
            Dim lstrPropCria As String

            If usrPropCriaFil.Valor.ToString <> "" Then
                lstrPropCria = usrPropCriaFil.Valor.ToString
            Else
                lstrPropCria = ""
            End If

            lstrCmd = ObtenerSql(Session("sUserId").ToString(), IIf(chkBusc.Checked, 1, 0), usrCriaFil.Valor.ToString, _
                     IIf(chkBaja.Checked, 1, 0), mstrProdCtrl, txtNombFil.Text, cmbRazaFil.Valor.ToString, _
                     "", txtRPFil.Text, cmbSexoFil.Valor.ToString, txtNaciFechaDesdeFil.Text, _
                     txtNaciFechaHastaFil.Text, cmbAsocFil.Valor.ToString, txtAsoNumeFil.Text, txtLaboNumeFil.Text, _
                     usrMadreFil.Valor.ToString, usrPadreFil.Valor.ToString, usrReceFil.Valor.ToString, txtObservacionFil.Valor.ToString, _
                     txtNumeDesdeFil.Valor.ToString, txtNumeHastaFil.Valor.ToString, _
                     txtApodoFil.Text, mintInscrip.ToString, txtCuigFil.Text, _
                     txtTramNumeFil.Text, txtInscFechaDesdeFil.Text, txtInscFechaHastaFil.Text, _
                     txtRPNumeDesdeFil.Text, txtRPNumeHastaFil.Text, lstrPropClie, lstrPropCria, _
                     cmbEstaFil.Valor, cmbNaciFil.Valor, mbooInclDeshab, IIf(cmbRegiTipoFil.Valor Is DBNull.Value, 0, cmbRegiTipoFil.Valor), _
                     cmbOrdenarPor.Valor, txtCondicionalDesdeFil.Valor.ToString, txtCondicionalHastaFil.Valor.ToString, _
                     txtRPExtranjeroFil.Valor.ToString, _
                     rbtnAsc.Checked, txtDadorNroSRAFil.Valor.ToString, txtDadorNroSenasaFil.Valor.ToString, _
                     cmbCastradoFil.Valor.ToString(), cmbDescorneFil.Valor.ToString())

            clsWeb.gCargarDataGrid(mstrConn, lstrCmd, grdDato, 9999999)

            grdDato.Visible = pbooVisi

            If Not Page.IsPostBack And grdDato.Items.Count = 1 And mstrProdCtrl <> "" Then
                If mstrValorId = "" Then
                    Dim lstrProdId As String, lstrProdRaza As String, lstrProdAsocId As String, lstrProdDesc As String, lstrProdrp As String
                    Dim lstrProdNumeExtr As String, lstrProdNomb As String, lstrProdSexoId As String, lstrProdRazaCodi As String, lstrProdSraNume As String, lstrProdRPExtr As String

                    lstrProdId = grdDato.Items(0).Cells(Columna.ProdId).Text.Trim 'Producto ID.
                    lstrProdRaza = grdDato.Items(0).Cells(Columna.ProdRazaId).Text.Trim 'Raza ID.
                    lstrProdAsocId = Replace(grdDato.Items(0).Cells(Columna.ProdAsocId).Text.Trim, "&nbsp;", "") 'Asociaci�n ID.
                    lstrProdNumeExtr = Replace(grdDato.Items(0).Cells(Columna.ProdNroExtr).Text.Trim, "&nbsp;", "") 'Nro. Extranjero.
                    lstrProdRPExtr = Replace(grdDato.Items(0).Cells(Columna.ProdRPExtr).Text.Trim, "&nbsp;", "") 'RP Extranjero.
                    If grdDato.Items(0).Cells(Columna.ProdSexo).Text.Trim <> "" Then
                        lstrProdSexoId = IIf(grdDato.Items(0).Cells(Columna.ProdSexo).Text.Trim = "True", "1", "0") 'Sexo.
                    End If
                    lstrProdNomb = Replace(grdDato.Items(0).Cells(Columna.ProdNombre).Text.Trim, "&nbsp;", "") 'Nombre.
                    lstrProdDesc = Replace(grdDato.Items(0).Cells(Columna.ProdDescrip).Text.Trim, "&nbsp;", "") 'Descripci�n.
                    lstrProdRazaCodi = Replace(grdDato.Items(0).Cells(Columna.ProdRazaCodigo).Text.Trim, "&nbsp;", "") 'Raza C�digo.
                    lstrProdrp = Replace(grdDato.Items(0).Cells(Columna.ProdRP).Text.Trim, "&nbsp;", "") 'RP.
                    lstrProdSraNume = Replace(grdDato.Items(0).Cells(Columna.ProdSraNume).Text.Trim, "&nbsp;", "") 'SraNume.

                    lstrProdNomb = Replace(lstrProdNomb, "'", "~") 'Nombre: reemplazo caracter especial.
                    mProducto(lstrProdId, lstrProdRaza, lstrProdSexoId, lstrProdAsocId, lstrProdNumeExtr, lstrProdNomb, lstrProdDesc, lstrProdRazaCodi, lstrProdrp, lstrProdSraNume, lstrProdRPExtr)
                    lstrProdNomb = Replace(lstrProdNomb, "'", "~") 'Nombre: vuelvo al caracter especial.
                Else
                    mCargarDatos(mstrValorId)
                    Return
                End If
            End If
            mMostrarPanel(False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

#End Region

#Region "Seteo de Controles"

    Private Sub mSetearEditor(ByVal pstrTabla As String, ByVal pbooAlta As Boolean)
        Select Case pstrTabla
            Case mstrTabla
                btnAlta.Enabled = pbooAlta
                If hdnSoloConsulta.Text.ToLower() <> "true" Then
                    btnBaja.Enabled = Not (pbooAlta)
                    btnModi.Enabled = Not (pbooAlta)
                End If

                If hdnSoloConsulta.Text.ToLower() = "true" Then
                    btntatu.Disabled = True
                    btntatu.Visible = False
                    hdnBloqueoCambioTatuaje.Text = "true"
                End If


                btnProce.Enabled = (Not pbooAlta And hdnPdinId.Text <> "")
                btnProp.Enabled = Not (pbooAlta)
                btnRela.Enabled = Not (pbooAlta)
                btnAsoc.Enabled = Not (pbooAlta)
                btnTramites.Disabled = pbooAlta
                btnServicios.Disabled = pbooAlta
                btnParejas.Disabled = pbooAlta
                btnPremios.Disabled = pbooAlta
                btnPerform.Disabled = pbooAlta
                ' btnEstados.Disabled = pbooAlta
                Select Case hdnEspe.Text
                    Case SRA_Neg.Constantes.Especies.Bovinos, SRA_Neg.Constantes.Especies.Camelidos, SRA_Neg.Constantes.Especies.Caprinos, SRA_Neg.Constantes.Especies.Equinos, SRA_Neg.Constantes.Especies.Ovinos
                        btnPedigree.Disabled = pbooAlta
                    Case Else
                        btnPedigree.Disabled = True
                End Select
            Case mstrTablaAnalisis
                btnAltaAna.Enabled = pbooAlta
                btnBajaAna.Enabled = Not (pbooAlta)
                btnModiAna.Enabled = Not (pbooAlta)
        End Select
    End Sub

    Public Overrides Sub mSeleccionarProducto(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Dim lstrProdId As String, lstrProdRaza As String, lstrProdAsocId As String, lstrProdDesc As String, lstrProdrp As String
        Dim lstrProdNumeExtr As String, lstrProdNomb As String, lstrProdSexoId As String, lstrProdRazaCodi As String, lstrProdSraNume As String, lstrProdRPExtr As String

        lstrProdId = E.Item.Cells(Columna.ProdId).Text.Trim 'Producto ID.
        lstrProdRaza = E.Item.Cells(Columna.ProdRazaId).Text.Trim 'Raza ID.
        lstrProdAsocId = Replace(E.Item.Cells(Columna.ProdAsocId).Text.Trim, "&nbsp;", "") 'Asociaci�n ID.
        lstrProdNumeExtr = Replace(E.Item.Cells(Columna.ProdNroExtr).Text.Trim, "&nbsp;", "") 'Nro. Extranjero.
        lstrProdRPExtr = Replace(E.Item.Cells(Columna.ProdRPExtr).Text.Trim, "&nbsp;", "") 'RP Extranjero.
        If E.Item.Cells(Columna.ProdSexo).Text.Trim <> "" Then
            lstrProdSexoId = IIf(E.Item.Cells(Columna.ProdSexo).Text.Trim = "True", "1", "0") 'Sexo.
        End If
        lstrProdNomb = Replace(E.Item.Cells(Columna.ProdNombre).Text.Trim, "&nbsp;", "") 'Nombre.
        lstrProdDesc = Replace(E.Item.Cells(Columna.ProdDescrip).Text.Trim, "&nbsp;", "") 'Descripci�n.
        lstrProdRazaCodi = Replace(E.Item.Cells(Columna.ProdRazaCodigo).Text.Trim, "&nbsp;", "") 'Raza C�digo.
        lstrProdrp = Replace(E.Item.Cells(Columna.ProdRP).Text.Trim, "&nbsp;", "") 'RP.
        lstrProdSraNume = Replace(E.Item.Cells(Columna.ProdSraNume).Text.Trim, "&nbsp;", "") 'SraNume.

        lstrProdNomb = Replace(lstrProdNomb, "'", "~") 'Nombre: reemplazo caracter especial.
        mProducto(lstrProdId, lstrProdRaza, lstrProdSexoId, lstrProdAsocId, lstrProdNumeExtr, lstrProdNomb, lstrProdDesc, lstrProdRazaCodi, lstrProdrp, lstrProdSraNume, lstrProdRPExtr)
        lstrProdNomb = Replace(lstrProdNomb, "~", "'") 'Nombre: vuelvo al caracter especial.
    End Sub

    Private Sub mProducto(ByVal pstrProdId As String, ByVal pstrProdRaza As String, ByVal pstrProdSexo As String, ByVal pstrProdAsoc As String, ByVal pstrProdNumeExtr As String, ByVal pstrProdNomb As String, ByVal pstrProdDesc As String, ByVal pstrRazaCodi As String, ByVal pstrProdRP As String, ByVal pstrProdSraNume As String, ByVal pstrProdRPExtr As String)
        Dim lsbMsg As New StringBuilder
        Dim lstrAsocNume As String = ""

        If (mbooNewCtrlProducto = False) Then
            lsbMsg.Append("<SCRIPT language='javascript'>")

            'Asociaci�n ID.
            lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbProdAsoc'].value='{1}';", Request.QueryString("ctrlId"), pstrProdAsoc))
            'Asociaci�n_onchange().
            lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbProdAsoc'].onchange();", Request.QueryString("ctrlId")))
            'Nro. Extranjero.
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtCodi'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdNumeExtr)))
            'RP Extranjero.
            lsbMsg.Append(String.Format("if(window.opener.document.all['{0}:txtRPExtr'] != null)", Request.QueryString("ctrlId")))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtRPExtr'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdRPExtr)))
            'Producto ID.
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtId'].value='{1}';", Request.QueryString("ctrlId"), pstrProdId))
            'RP.
            lsbMsg.Append(String.Format("if(window.opener.document.all['{0}:txtRP'] != null)", Request.QueryString("ctrlId")))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtRP'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdRP)))
            'SraNume.
            lsbMsg.Append(String.Format("if(window.opener.document.all['{0}:txtSraNume'] != null)", Request.QueryString("ctrlId")))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtSraNume'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdSraNume)))
            'Nombre.
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtProdNomb'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdNomb)))
            'Descipci�n.
            lsbMsg.Append(String.Format("if(window.opener.document.all['{0}:txtDesc'] != null)", Request.QueryString("ctrlId")))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtDesc'].value='{1}';", Request.QueryString("ctrlId"), clsFormatear.gFormatCadena(pstrProdDesc)))

            'usrProdDeriv (Control viejo) - Borrar cuando todos los controles hayan sido reemplazados por el nuevo.
            If mstrFilEsProp = "" And mstrFilEsExtranjero = "" Then
                lsbMsg.Append(String.Format("if (window.opener.document.all['txt{0}:cmbProdRaza'].value != '{1}')", Request.QueryString("ctrlId"), pstrRazaCodi))
                lsbMsg.Append("{")
                lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbProdRaza'].value='{1}';", Request.QueryString("ctrlId"), pstrRazaCodi))
                lsbMsg.Append(String.Format("window.opener.document.all['txt{0}:cmbProdRaza'].onchange();", Request.QueryString("ctrlId")))
                lsbMsg.Append("}")
                lsbMsg.Append(String.Format("if (window.opener.document.all['{0}:cmbProdSexo']!=null)", Request.QueryString("ctrlId")))
                lsbMsg.Append("{")
                Select Case pstrProdSexo.ToUpper
                    Case "TRUE"    'macho
                        lsbMsg.Append(String.Format("window.opener.document.all['{0}:cmbProdSexo'].selectedIndex = 2;", Request.QueryString("ctrlId")))
                    Case "FALSE"   'hembra
                        lsbMsg.Append(String.Format("window.opener.document.all['{0}:cmbProdSexo'].selectedIndex = 1;", Request.QueryString("ctrlId")))
                    Case Else
                        lsbMsg.Append(String.Format("window.opener.document.all['{0}:cmbProdSexo'].selectedIndex = 0;", Request.QueryString("ctrlId")))
                End Select
                lsbMsg.Append("}")

                'usrProducto y usrProductoExtranjero.
            Else
                lsbMsg.Append(String.Format("if (window.opener.document.all['{0}:cmbProdSexo']!=null)", Request.QueryString("ctrlId")))
                lsbMsg.Append("{")
                Select Case pstrProdSexo
                    Case 1 'Macho.
                        lsbMsg.Append(String.Format("window.opener.document.all['{0}:cmbProdSexo'].value = 1;", Request.QueryString("ctrlId")))
                    Case 0 'Hembra.
                        lsbMsg.Append(String.Format("window.opener.document.all['{0}:cmbProdSexo'].value = 0;", Request.QueryString("ctrlId")))
                    Case Else
                        lsbMsg.Append(String.Format("window.opener.document.all['{0}:cmbProdSexo'].value = '';", Request.QueryString("ctrlId")))
                End Select
                lsbMsg.Append("}")
            End If

            'SraNume.onchange().
            lsbMsg.Append(String.Format("if (window.opener.document.all['{0}:txtSraNume']!=null && window.opener.document.all['{0}:txtSraNume'].value!='')", Request.QueryString("ctrlId")))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtSraNume'].onchange();", Request.QueryString("ctrlId")))
            lsbMsg.Append("else{")
            'RP.onchange().
            lsbMsg.Append(String.Format("if (window.opener.document.all['{0}:txtRP']!=null && window.opener.document.all['{0}:txtRP'].value!='')", Request.QueryString("ctrlId")))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtRP'].onchange();", Request.QueryString("ctrlId")))
            lsbMsg.Append("else{")
            'RPExtr.onchange().
            lsbMsg.Append(String.Format("if (window.opener.document.all['{0}:txtRPExtr']!=null && window.opener.document.all['{0}:txtRPExtr'].value!='')", Request.QueryString("ctrlId")))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtRPExtr'].onchange();", Request.QueryString("ctrlId")))
            lsbMsg.Append("}")
            lsbMsg.Append("}")
            ' Dario 2014-06-13 para forzar el evento cambio del control producto
            lsbMsg.Append(String.Format("window.opener.__doPostBack(window.opener.document.all['{0}:txtId'].name,'');", Request.QueryString("ctrlId")))
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
        Else
            lsbMsg.Append("<SCRIPT language='javascript'>")
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtId'].value='{1}';", Request.QueryString("ctrlId"), pstrProdId))
            lsbMsg.Append(String.Format("window.opener.document.all['{0}:txtIdEvent'].value='{1}';", Request.QueryString("ctrlId"), pstrProdId))
            lsbMsg.Append(String.Format("window.opener.__doPostBack(window.opener.document.all['{0}:txtIdEvent'].name,'');", Request.QueryString("ctrlId")))
            lsbMsg.Append("window.close();")
            lsbMsg.Append("</SCRIPT>")
        End If

        Response.Write(lsbMsg.ToString)
    End Sub

    Public Overrides Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            mCargarDatos(E.Item.Cells(Columna.ProdId).Text)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mSetearEditorObse(ByVal pbooAlta As Boolean)
        btnBajaObse.Enabled = Not (pbooAlta)
        btnModiObse.Enabled = Not (pbooAlta)
        btnAltaObse.Enabled = pbooAlta
    End Sub

    Private Sub mSetearEditorDocum(ByVal pbooAlta As Boolean)
        btnBajaDocum.Enabled = Not (pbooAlta)
        btnModiDocum.Enabled = Not (pbooAlta)
        btnAltaDocum.Enabled = pbooAlta
    End Sub

    Private Sub mSetearEditorAsoc(ByVal pbooAlta As Boolean)
        btnBajaAsociacion.Enabled = Not (pbooAlta)
        btnModiAsociacion.Enabled = Not (pbooAlta)
        btnAltaAsociacion.Enabled = pbooAlta
    End Sub

    Private Sub mCargarPelajes(ByVal pRaza As String)
        If pRaza <> "" Then
            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=1", cmbPelaA, "id", "descrip", "S")
            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=2", cmbPelaB, "id", "descrip", "S")
            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=3", cmbPelaC, "id", "descrip", "S")
            clsWeb.gCargarCombo(mstrConn, "rg_pelajes_cargar @pela_raza_id=" & pRaza & ",@pela_rgub_id=4", cmbPelaD, "id", "descrip", "S")
        Else
            cmbPelaA.Limpiar()
            cmbPelaB.Limpiar()
            cmbPelaC.Limpiar()
            cmbPelaD.Limpiar()
        End If
    End Sub
 

    Public Overrides Sub mCargarDatos(ByVal pstrId As String)
        Try
            mLimpiar()

            Dim strFechaIni As String
            Dim strEstado As String
            Dim strTipoHBA As String
            Dim oEspecies As New SRA_Neg.Especie(mstrConn, Session("sUserId").ToString())
            Dim oRazas As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim oDenuBajas As New SRA_Neg.Denuncias(mstrConn, Session("sUserId").ToString())
            Dim oProductoInscrip As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())

                Dim boolPermiteDescorne As Boolean
                Dim boolraza_InscribeSRA As Boolean

                '            pantalla de errores si consulta, no insribe  o es extranjero no se puede a modificar errores lo mando 
                'a la pantalla como solo consulta desabilitando bootnes de grabacion

                'si esta en tramite se puede modificar errores

                hdnId.Text = clsFormatear.gFormatCadena(pstrId)

            mCrearDataSet(hdnId.Text)

            If mdsDatos.Tables(0).Rows.Count > 0 Then
                mboolSoloConsulta = False
                hdnSoloConsulta.Text = ""
                hdnMensajeConsulta.Text = ""
                hdnBloqueoCambioTatuaje.Text = "false"
                hdnExtranjero.Text = "false"

                EspecieId = mdsDatos.Tables(0).Rows(0).Item("_espe")
                cmbSexo.Valor = IIf(mdsDatos.Tables(0).Rows(0).Item("prdt_sexo").ToString, "1", "0")

                ' GSZ 03/12/2015 Se agrego  carga de datos de sexo,  y especie
                If Not Request.QueryString("SoloConsulta") Is Nothing Then
                    If Request.QueryString("SoloConsulta") <> "" Then
                        mboolSoloConsulta = Convert.ToBoolean(mValorParametro(Request.QueryString("SoloConsulta")))
                        hdnSoloConsulta.Text = mboolSoloConsulta.ToString()

                        If mboolSoloConsulta Then
                            lblMensaje.Visible = True
                            If hdnMensajeConsulta.Text = "" Then
                                lblMensaje.Text = "Pantalla de consulta de Producto"
                                hdnMensajeConsulta.Text = "Pantalla de consulta de Producto"
                                btntatu.Disabled = True
                                btntatu.Visible = False
                                btnAltaObse.Enabled = False
                                btnModiObse.Enabled = False
                                btnBajaObse.Enabled = False

                                hdnBloqueoCambioTatuaje.Text = "true"
                            Else
                                lblMensaje.Text = hdnMensajeConsulta.Text
                            End If
                            BloquearBotones(True)
                        Else
                            BloquearBotones(False)
                        End If
                    End If
                End If

                With mdsDatos.Tables(0).Rows(0)
                    strEstado = .Item("prdt_esta_id").ToString()

                        cmbRaza.Valor = .Item("prdt_raza_id")

                        boolraza_InscribeSRA = .Item("_raza_InscribeSRA")
                        If (boolraza_InscribeSRA = False) Then
                            usrCria.Enabled = True
                            usrCria.RazaId = cmbRaza.Valor
                            usrProp.Enabled = True
                            usrProp.RazaId = cmbRaza.Valor
                            cmbAsociacion.Enabled = True
                            txtAsoNume.Enabled = True
                            txtRPExtranjero.Enabled = True
                        Else
                            usrCria.Enabled = False
                            usrProp.Enabled = False
                            cmbAsociacion.Enabled = False
                            txtAsoNume.Enabled = False
                            txtRPExtranjero.Enabled = False
                        End If
                        strTipoHBA = oEspecies.GetEspecieTipoHBADescByRazaId(.Item("prdt_raza_id"))

                    If (cmbRaza.Valor = "3") Then
                        lblCondGenet.Enabled = True
                        lblCondGenet.Visible = True
                        txtCondGeneticas.Enabled = True
                        txtCondGeneticas.Visible = True
                        trCondGenet.Visible = True
                    Else
                        lblCondGenet.Enabled = False
                        lblCondGenet.Visible = False
                        txtCondGeneticas.Enabled = False
                        txtCondGeneticas.Visible = False
                        trCondGenet.Visible = False
                    End If

                    boolPermiteDescorne = oRazas.GetRazaPermiteDescorne(.Item("prdt_raza_id"))
                    If boolPermiteDescorne Then
                        chkDescorne.Enabled = False
                    Else
                        chkDescorne.Enabled = False
                    End If


                    EspecieId = (.Item("_espe"))


                    'GSZ 14/04/2015 se agrego descorne y castrado
                        chkDescorne.Checked = .Item("prdt_descorne")
                        chkCastrado.Checked = .Item("prdt_castrado")
                        txtCastradoFecha.Fecha = .Item("prdt_Fecha_castrado").ToString

                    If strEstado = "101" Or strEstado = "102" Or strEstado = "97" Then
                        lblMensaje.Text = "Producto en TRAMITE,cualquier Modificaci�n hasta el otorgamiento del " + strTipoHBA.Trim() + " se debe hacer en el modulo MASIVO -Opciones/Denuncia de Nacimiento"
                        lblMensaje.Visible = True
                        hdnMensajeConsulta.Text = lblMensaje.Text
                        mboolSoloConsulta = True
                        '  hdnSoloConsulta.Text = "true" ' se usa solo para errores
                        BloquearBotonCerficado(True)
                    Else
                        If Not mboolSoloConsulta Then
                            lblMensaje.Text = ""
                            lblMensaje.Visible = False
                            hdnMensajeConsulta.Text = ""
                            mboolSoloConsulta = False
                            hdnSoloConsulta.Text = "false"
                        End If
                    End If

                    hdnPdinId.Text = IIf(.IsNull("_reprocesar"), "", .Item("_reprocesar"))
                        txtRP.Valor = IIf(.IsNull("prdt_rp"), "", .Item("prdt_rp"))
                        txtNomb.Valor = IIf(.IsNull("prdt_nomb"), "", .Item("prdt_nomb"))
                        txtCondGeneticas.Valor = IIf(.IsNull("prdt_Cond_Geneticas"), "", .Item("prdt_Cond_Geneticas"))
                        If Not .Item("_pref_desc") Is DBNull.Value Then
                        If .Item("_esta_desc") <> "Vigente" Then
                            trPrefijo.Style.Add("display", "inline")
                            lblPrefijoDato.Text = .Item("_pref_desc")
                            lblPrefijoDato.Visible = True
                            lblPrefijo.Visible = True
                        Else
                            trPrefijo.Style.Add("display", "none")
                            lblPrefijoDato.Text = String.Empty
                            lblPrefijoDato.Visible = False
                            lblPrefijo.Visible = False
                        End If
                    Else
                        trPrefijo.Style.Add("display", "none")
                        lblPrefijoDato.Visible = False
                        lblPrefijo.Visible = False
                    End If

                        txtCondicional.Valor = .Item("prdt_condicional").ToString

                        txtCuig.Valor = .Item("prdt_cuig").ToString
                        txtNaciFecha.Fecha = IIf(.IsNull("prdt_naci_fecha"), "", .Item("prdt_naci_fecha").ToString())
                        txtFalleFecha.Fecha = IIf(.IsNull("prdt_fall_fecha"), "", .Item("prdt_fall_fecha").ToString())
                        hdnCriaId.Text = IIf(.IsNull("prdt_cria_id"), "", .Item("prdt_cria_id").ToString())
                        usrCria.RazaId = .Item("prdt_raza_id")
                        usrCria.Valor = IIf(.IsNull("prdt_cria_id"), "", .Item("prdt_cria_id"))
                        usrProp.RazaId = .Item("prdt_raza_id")
                        usrProp.Valor = .Item("prdt_prop_cria_id")
                        txtCria.Valor = .Item("_cria_nume").ToString
                        txtCriaDesc.Valor = .Item("_criador").ToString
                        txtSraNume.Valor = .Item("prdt_sra_nume").ToString
                        mintPdinId = IIf(.IsNull("prdt_pdin_id"), "", .Item("prdt_pdin_id"))
                        Dim oEmbargo As New SRA_Neg.Embargos(mstrConn, Session("sUserId").ToString())
                        Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())

                        If oEmbargo.GetEstaEmbargoByProdId(hdnId.Text) = True Then
                            strFechaIni = oEmbargo.GetFechaEmbargoIniByProdId(.Item("prdt_id"))
                            lblEmbargado.Text = .Item("_embargado").ToString & "-" & strFechaIni.ToString()
                        End If

                        'Fecha del Tr�mite de Transferencia.
                        txtFechaTransferencia.Fecha = .Item("_fecha_tramite").ToString

                        'N�mero del Tr�mite de Transferencia.
                        txtNroTransf.Valor = .Item("_tram_nume").ToString
                        'N�mero de Control del Tr�mite de Transferencia.
                        txtNroControlTransf.Valor = .Item("_nro_control_tramite").ToString

                        'Denunucia de Nacimientos.
                        txtTram.Valor = .Item("_pdin_tede_nume").ToString
                        txtTramFecha.Fecha = .Item("_pdin_fecha").ToString
                        txtTramNroControl.Valor = .Item("_pdin_nro_control").ToString

                        lblApro.Text = .Item("_aprobado").ToString
                        cmbBaja.Valor = .Item("prdt_bamo_id").ToString
                        If Not .IsNull("prdt_sexo") Then
                            cmbSexo.Valor = IIf(.Item("prdt_sexo").ToString, "1", "0")
                        Else
                            cmbSexo.Limpiar()
                        End If
                        txtRece.Text = .Item("_receptora").ToString

                        txtServFecha.Text = String.Format("{0:dd/MM/yyyy}", .Item("_servi_fecha"))
                        If txtServFecha.Text = "" Then
                            txtServFecha.Text = String.Format("{0:dd/MM/yyyy}", .Item("_servi_fecha_producto"))
                        End If

                        txtImplFecha.Text = String.Format("{0:dd/MM/yyyy}", .Item("_impla_fecha"))
                        If Not .Item("_servi_tipo_te") Is DBNull.Value Then
                            txtServTipo.Text = .Item("_servi_tipo_te").ToString
                        Else
                            txtServTipo.Text = .Item("_servi_tipo").ToString
                        End If

                        Dim strTieneObs As String = ""
                        strTieneObs = oProductoInscrip.GetObsProdInscByRazaCriaRPSexo(usrCria.RazaId.ToString(), usrCria.Valor.ToString(), _
                        cmbSexo.Valor.ToString(), txtRP.Valor.ToString())

                        If strTieneObs <> "" Then
                            ' GSZ 29/04/2015  
                            lblObservProdInsc.Text = "Observaciones en la inscripci�n: " + strTieneObs
                        End If

                        ' Dario 2014-11-06
                        btnSemenStock.Visible = False
                        If Not cmbSexo.Valor Is DBNull.Value Then
                            Select Case cmbSexo.Valor
                                Case 0 'Hembra.
                                    trDonante.Style.Add("display", "inline")
                                    trDador.Style.Add("display", "none")
                                Case 1 'Macho.
                                    trDonante.Style.Add("display", "none")
                                    trDador.Style.Add("display", "inline")
                                    btnSemenStock.Visible = True
                                Case Else 'Otro.
                                    lblDador.Text = "Donante/Dador:"
                            End Select
                        End If
                        hdnExtranjero.Text = "false"
                        If .IsNull("prdt_ndad") Then
                            lblNdad.Text = ""
                        Else
                            Select Case .Item("prdt_ndad")
                                Case "I"
                                    lblNdad.Text = "Importado"
                                    ' Dario 2015-05-27 cambio para que se puedan modificar los I o E sin pdin_id y sin hba
                                    If (mintPdinId <= 0 And strEstado <> "73") Then
                                        lblMensaje.Text = ""
                                        lblMensaje.Visible = False
                                        hdnMensajeConsulta.Text = ""
                                        mboolSoloConsulta = False
                                        hdnSoloConsulta.Text = "false"
                                    End If
                                Case "E"
                                    Select Case .Item("_HabilitaNacionalizacionProducto")
                                        Case "N"
                                            ' Modificado : GSZ 23/04/2015
                                            lblNdad.Text = "Extranjero"
                                            btnInscribirExtr.Visible = False
                                            BloquearBotonCerficado(True)
                                            hdnExtranjero.Text = "true"
                                            lblMensaje.Text = ""
                                            lblMensaje.Visible = False
                                            hdnMensajeConsulta.Text = ""
                                            mboolSoloConsulta = False
                                            hdnSoloConsulta.Text = "false"
                                        Case "S"
                                            lblNdad.Text = "Asociacion"
                                            btnInscribirExtr.Visible = True
                                    End Select
                                Case Else
                                    lblNdad.Text = ""
                                    btnInscribirExtr.Visible = False
                            End Select
                        End If

                        If cmbRaza.Valor.ToString <> "" Then
                            clsWeb.gCargarCombo(mstrConn, "rg_variedades_cargar @vari_raza_id=" & cmbRaza.Valor.ToString, cmbVari, "id", "descrip", "N")
                        Else
                            cmbVari.Limpiar()
                        End If
                        cmbVari.Valor = .Item("prdt_vari_id").ToString
                        If cmbRaza.Valor.ToString <> "" Then
                            clsWeb.gCargarCombo(mstrConn, "rg_registros_tipos_productos_cargar @regt_raza_id=" & cmbRaza.Valor.ToString, cmbRegiTipo, "id", "descrip", "N")
                        Else
                            cmbRegiTipo.Limpiar()
                        End If
                        cmbRegiTipo.Valor = .Item("prdt_regt_id").ToString

                        If Not .IsNull("_asoc_codi") Then
                            If .Item("_asoc_codi") <> 0 Then
                                cmbAsociacion.Valor = .Item("prdt_ori_asoc_id").ToString
                            End If
                        End If

                        txtAsoNume.Valor = .Item("prdt_ori_asoc_nume").ToString
                        txtRPExtranjero.Valor = .Item("prdt_rp_extr").ToString

                        If SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "razas", "raza_color", cmbRaza.Valor.ToString) = "0" Then
                            trColorCabeza.Style.Add("display", "none")
                            trCuerpoMiembros.Style.Add("display", "none")
                            'trSepara.Style.Add("display", "none")
                        Else
                            trColorCabeza.Style.Add("display", "inline")
                            trCuerpoMiembros.Style.Add("display", "inline")
                            'trSepara.Style.Add("display", "inline")
                            If Not cmbRaza.Valor Is DBNull.Value Then
                                mCargarPelajes(cmbRaza.Valor)
                            Else
                                mCargarPelajes("")
                            End If
                            cmbPelaA.Valor = .Item("prdt_a_pela_id").ToString
                            cmbPelaB.Valor = .Item("prdt_b_pela_id").ToString
                            cmbPelaC.Valor = .Item("prdt_c_pela_id").ToString
                            cmbPelaD.Valor = .Item("prdt_d_pela_id").ToString
                            cmbVari.Valor = .Item("prdt_vari_id").ToString
                        End If

                        If .IsNull("prdt_tran_fecha") Then
                            txtTranFecha.Text = ""
                        Else
                            txtTranFecha.Text = CDate(.Item("prdt_tran_fecha")).ToString("dd/MM/yyyy")
                        End If

                        txtPX.Valor = .Item("prdt_px").ToString
                        txtApod.Valor = .Item("prdt_apodo").ToString
                        txtDona.Valor = .Item("prdt_dona_nume").ToString
                        txtDonante.Valor = .Item("prdt_dona_nume").ToString

                        'Estado del Dador: Para que est� "Habilitado" tiene que tener un Nro de Dador SRA.
                        '                  De lo contrario, est� "En Tr�mite"
                        If Not .Item("prdt_dona_nume") Is DBNull.Value Then
                            lblDadorEstado.Text = "Habilitado"

                        Else
                            lblDadorEstado.Text = "" 'lblDadorEstado.Text = "En Tr�mite"
                        End If
                        txtNroSenasa.Valor = .Item("prdt_dona_sena_nume").ToString
                        txtDadorDonanteFechaDesde.Fecha = .Item("prdt_dona_fecha").ToString
                        txtHembraDonanteFechaDesde.Fecha = .Item("prdt_dona_fecha").ToString
                        txtDadorDonanteNroControl.Valor = .Item("_dona_nro_control").ToString
                        'txtPesoNacer.Valor = .Item("prdt_peso_nacer")
                        'txtPesoDeste.Valor = .Item("prdt_peso_deste")
                        'txtPesoFinal.Valor = .Item("prdt_peso_final")
                        ' GSZ 2014-07-16
                        txtFechaInspeccionDeVida.Fecha = .Item("prdt_Fecha_InspecMayor25").ToString

                        If .Item("prdt_Resul_InspecMayor25") Is System.DBNull.Value Then
                            cmbResultadoInspecdeVida.SelectedValue = 0
                        Else
                            cmbResultadoInspecdeVida.SelectedValue = .Item("prdt_Resul_InspecMayor25")
                        End If

                        txtInscFecha.Fecha = .Item("prdt_insc_fecha").ToString
                        'txtEpdNacer.Valor = .Item("prdt_epd_nacer")
                        'txtEpdDeste.Valor = .Item("prdt_epd_deste")
                        'txtEpdFinal.Valor = .Item("prdt_epd_final")
                        txtRPNume.Valor = .Item("prdt_rp_nume").ToString
                        cmbMelli.Valor = .Item("prdt_melli").ToString
                        If Not .IsNull("prdt_siete") Then
                            chkSiete.Checked = .Item("prdt_siete")
                        Else
                            chkSiete.Checked = False
                        End If
                        hdnEspe.Text = .Item("_espe").ToString
                        If Not .IsNull("prdt_te") Then
                            chkTranEmbr.Checked = clsWeb.gFormatCheck(.Item("prdt_te"), "True")
                        Else
                            chkTranEmbr.Checked = False
                        End If
                        If Not .IsNull("_esta_desc") Then
                            cmbEsta.Valor = .Item("prdt_esta_id")
                        Else
                            cmbEsta.Limpiar()
                        End If
                        Dim strMotivoBaja As String = ""

                        If Not .IsNull("prdt_baja_fecha") Then
                            If (oDenuBajas.GetDenuBajaDetalleByProdId(.Item("prdt_id")).ToString() <> "") Then
                                strMotivoBaja = oDenuBajas.GetDenuBajaDetalleByProdId(.Item("prdt_id")).ToString()
                            End If

                            lblBaja.Text = "Registro dado de baja en fecha: " & CDate(.Item("prdt_baja_fecha")).ToString("dd/MM/yyyy HH:mm") + _
                            IIf(strMotivoBaja <> "", " - " + strMotivoBaja.Trim, "")

                            btnRevertirBaja.Enabled = False
                            btnRevertirBaja.Visible = True
                        Else
                            lblBaja.Text = ""
                            btnRevertirBaja.Enabled = False
                            btnRevertirBaja.Visible = False
                        End If

                        'lblObservProdInsc.Text = IIf(oProducto.GetObsProdInscDetalleByProdId(.Item("prdt_id")) <> "", "Observacones:" + oProducto.GetObsProdInscDetalleByProdId(.Item("prdt_id")), "")

                        'Arma el t�tulo.
                        If Not .IsNull("prdt_sexo") Then
                            lblTitu.Text = "Datos del Producto: " & "  Raza: " & Trim(.Item("_raza_desc")) & " - " & Trim(.Item("_sexo"))
                        Else
                            lblTitu.Text = "Datos del Producto: " & "  Raza: " & Trim(.Item("_raza_desc")) & " - "
                        End If

                        If Not .IsNull("prdt_sra_nume") Then
                            lblTitu.Text += " - SRA Nro.: " & Trim(.Item("prdt_sra_nume"))
                        End If
                        If Not .IsNull("prdt_nomb") Then
                            lblTitu.Text += " - Nombre: " & Trim(.Item("prdt_nomb"))
                        End If
                        ' 2021-05-17 1520 Dario cambio por angus espa�a
                        If Not .IsNull("prdt_factura") Then
                            lblFactura.Text = .Item("prdt_factura")
                        End If

                        'datos de la ultima calificacion
                        txtCali.Valor = .Item("_calif").ToString
                        txtCaliFenotipica.Valor = .Item("_califFenotipica").ToString

                        'Pesasdas - A�o Corrida
                        If Not .IsNull("prdt_anio_deps_Nac") Then txtAnioCorridaNacimiento.Valor = .Item("prdt_anio_deps_Nac").ToString
                        If Not .IsNull("prdt_anio_deps_Dest") Then txtAnioCorridaDestete.Valor = .Item("prdt_anio_deps_Dest").ToString
                        If Not .IsNull("prdt_anio_deps_PosDest") Then txtAnioCorridaPosDestete.Valor = .Item("prdt_anio_deps_PosDest").ToString
                        If Not .IsNull("prdt_anio_deps_PosDest") Then txtAnioCorridaCircEscr.Valor = .Item("prdt_anio_deps_PosDest").ToString
                        'Pesasdas - Fecha 'GSZ Se le dio formato  dd/mm/yyyy
                        If Not .IsNull("prdt_naci_fecha") Then txtFechaPesadasNacimiento.Text = String.Format("{0:dd/MM/yyyy}", .Item("prdt_naci_fecha"))
                        If Not .IsNull("prdt_FechaDestete") Then txtFechaPesadasDestete.Text = String.Format("{0:dd/MM/yyyy}", .Item("prdt_FechaDestete"))
                        If Not .IsNull("prdt_FechaPesoPostDestete") Then txtFechaPesadasPosDestete.Text = String.Format("{0:dd/MM/yyyy}", .Item("prdt_FechaPesoPostDestete"))
                        If Not .IsNull("prdt_FechaCirEscrotal") Then txtFechaPesadasCircEscr.Text = String.Format("{0:dd/MM/yyyy}", .Item("prdt_FechaCirEscrotal"))
                        'Pesasdas - Medici�n
                        If Not .IsNull("prdt_peso_nacer") Then txtMedicionNacimiento.Valor = .Item("prdt_peso_nacer")
                        If Not .IsNull("prdt_peso_deste") Then txtMedicionDestete.Valor = .Item("prdt_peso_deste")
                        If Not .IsNull("prdt_peso_final") Then txtMedicionPosDestete.Valor = .Item("prdt_peso_final")
                        If Not .IsNull("prdt_CirEscrotal") Then txtMedicionCircEscr.Valor = .Item("prdt_CirEscrotal")
                        'Pesasdas - Manejo
                        If Not .IsNull("prdt_ManejoDest") Then txtManejoDestete.Valor = .Item("prdt_ManejoDest")
                        If Not .IsNull("prdt_ManejoPosDest") Then txtManejoPosDestete.Valor = .Item("prdt_ManejoPosDest")
                        'Pesasdas - OBS
                        If Not .IsNull("prdt_ObsDestete") Then txtOBSDestete.Valor = .Item("prdt_ObsDestete")
                        If Not .IsNull("prdt_ObsPostDestete") Then txtOBSPosDestete.Valor = .Item("prdt_ObsPostDestete")

                        'Deps - Fecha
                        If Not .IsNull("Prdt_Deps_Fecha") Then txtDEPSFecha.Text = .Item("Prdt_Deps_Fecha")
                        'Deps - EPD
                        If Not .IsNull("prdt_epd_nacer") Then txtEPDNacimiento.Valor = .Item("prdt_epd_nacer")
                        If Not .IsNull("prdt_epd_deste") Then txtEPDDestete.Valor = .Item("prdt_epd_deste")
                        If Not .IsNull("prdt_epd_final") Then txtEPDPosDestete.Valor = .Item("prdt_epd_final")
                        If Not .IsNull("Prdt_Epd_CirEscrotal") Then txtEPDCircEscr.Valor = .Item("Prdt_Epd_CirEscrotal")
                        If Not .IsNull("Prdt_Epd_Lech") Then txtEPDLecheMaterna.Valor = .Item("Prdt_Epd_Lech")
                        If Not .IsNull("Prdt_Epd_LYC") Then txtEPDLecheYCrianza.Valor = .Item("Prdt_Epd_LYC")
                        'Deps - Exactitud
                        If Not .IsNull("Prdt_Exa_Nacer") Then txtExactitudNacimiento.Valor = .Item("Prdt_Exa_Nacer")
                        If Not .IsNull("Prdt_Exa_Destete") Then txtExactitudDestete.Valor = .Item("Prdt_Exa_Destete")
                        If Not .IsNull("Prdt_Exa_Final") Then txtExactitudPosDestete.Valor = .Item("Prdt_Exa_Final")
                        If Not .IsNull("Prdt_Exa_CirEscrotal") Then txtExactitudCircEscr.Valor = .Item("Prdt_Exa_CirEscrotal")
                        If Not .IsNull("Prdt_Exa_Lech") Then txtExactitudLecheMaterna.Valor = .Item("Prdt_Exa_Lech")

                        txtResulSNPSExtranjero.Valor = IIf(Not .IsNull("_resulSNPSExtranj"), .Item("_resulSNPSExtranj"), "")
                        lblDonanteEstado.Text = IIf(.Item("_Esdonante"), "Habilitado", "En Tr�mite")
                    End With

                'datos del �ltimo analisis
                If mdsDatos.Tables(mstrTablaAnalisis).Rows.Count > 0 Then
                    mdsDatos.Tables(mstrTablaAnalisis).DefaultView.Sort() = "prta_fecha DESC"
                    With mdsDatos.Tables(mstrTablaAnalisis).Select()(0)
                        'N�mero - Resultado - Pureza.
                            txtNroAnal.Valor = .Item("prta_nume").ToString
                            txtResulAnal.Valor = .Item("_resul_codi").ToString & " - " & .Item("_resul").ToString
                        txtPureza.Valor = IIf(Not .IsNull("_pureza"), .Item("_pureza"), "")
                        txtTipoAnal.Valor = IIf(Not .IsNull("prta_tipo_analisis"), .Item("prta_tipo_analisis"), "")
                        txtFechaAnal.Text = IIf(Not .IsNull("prta_resul_fecha"), .Item("prta_resul_fecha"), "")
                        txtEstadoAnal.Valor = IIf(Not .IsNull("_EstadoAnalisis"), .Item("_EstadoAnalisis"), "")

                        'ADN.
                        If Not .IsNull("_tiene_adn") Then
                            If .Item("_tiene_adn") Then
                                txtTieneADN.Valor = "SI"
                            Else
                                txtTieneADN.Valor = "NO"
                            End If
                        End If
                        txtResulADN.Valor = IIf((Not .IsNull("_resuladn_codi")) And (Not .IsNull("_resuladn")), (.Item("_resuladn_codi") & " - " & .Item("_resuladn")), "")
                        'TS.
                        If Not .IsNull("_tiene_ts") Then
                            If .Item("_tiene_ts") Then
                                txtTieneTS.Valor = "SI"
                            Else
                                txtTieneTS.Valor = "NO"
                            End If
                        End If
                        txtResulTS.Valor = IIf((Not .IsNull("_results_codi")) And (Not .IsNull("_results")), (.Item("_results_codi") & " - " & .Item("_results")), "")
                        'SNPS.
                        If Not .IsNull("_tiene_snps") Then
                            If .Item("_tiene_snps") Then
                                txtTieneSNPS.Valor = "SI"
                            Else
                                txtTieneSNPS.Valor = "NO"
                            End If
                        End If
                        txtResulSNPS.Valor = IIf((Not .IsNull("_resulsnps_codi")) And _
                        (Not .IsNull("_resulsnps")), (.Item("_resulsnps_codi") & " - " & .Item("_resulsnps")), "")

                        'Pelo. 
                        If Not .IsNull("_tiene_pelo") Then
                            If .Item("_tiene_pelo") Then
                                txtTienePelo.Valor = "SI"
                            Else
                                txtTienePelo.Valor = "NO"
                            End If
                        End If
                        'Estado del Donante: Para que est� "Habilitado" tiene que tener HBA y el An�lisis Resultado ser igual a 10.
                        '                    De lo contrario, est� "En Tr�mite"
                        ' Dario 2015-03-13 se cambia para que muestra la fecha de recepcion del ultimo analisis
                        ' antes mostraba el resul fecha
                            txtFechaUltAnalisis.Fecha = .Item("prta_fecha").ToString
                        ' Dario 2015-08-26 muestra el estudio si tiene
                        If Not .IsNull("_Estudio") Then
                            lblEstudio.Text = .Item("_Estudio")
                        End If

                        'If Not txtSraNume.Valor Is DBNull.Value And Not txtDonante.Valor Is DBNull.Value Then
                        '    If txtSraNume.Valor <> "" Then
                        '        If ValidarNulos(.Item("_resul_codi"), False) = 10 Then

                        '            lblDonanteEstado.Text = "Habilitado"
                        '        Else
                        '            lblDonanteEstado.Text = "En Tr�mite"
                        '        End If
                        '    End If
                        'End If

                    End With
                    mdsDatos.Tables(mstrTablaAnalisis).DefaultView.RowFilter = ""
                End If
            End If
            mSetearEditor("", False)
            btnProce.Enabled = (hdnPdinId.Text <> "")
            mMostrarPanel(True)

            If hdnMensajeConsulta.Text <> "" Or hdnSoloConsulta.Text.ToLower() = "true" Then
                BloquearBotones(True)
                If hdnSoloConsulta.Text.ToLower() = "true" And hdnMensajeConsulta.Text = "" Then
                    lblMensaje.Text = "Ingreso por la Pantalla solo consulta"
                    lblMensaje.Visible = True

                End If
            Else
                BloquearBotones(False)
            End If
            If cmbRaza.Valor.ToString <> "" Then
                If mValiInscribeSRA(cmbRaza.Valor) = "0" Then
                    BloquearBotonCerficado(True)
                    hdnSoloConsulta.Text = "true"
                End If
            End If

            mCargarPadreyMadre()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try

    End Sub

    Private Sub mCargarPadreyMadre()
        Dim lstrCmd As New StringBuilder
        lstrCmd.Append("exec PadreyMadre_consul ")
        lstrCmd.Append("@prdt_id=")
        lstrCmd.Append(hdnId.Text)

        Dim lDsDatos As DataSet
        lDsDatos = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd.ToString)

        txtPadre.Valor = ""
        txtRpPadre.Valor = ""
        txtPxPadre.Valor = ""
        txtNombrePadre.Valor = ""
        txtMadre.Valor = ""
        txtRpMadre.Valor = ""
        txtPxMadre.Valor = ""
        txtNombreMadre.Valor = ""
        txtPadreDesc.Valor = ""
        txtMadreDesc.Valor = ""

        If lDsDatos.Tables(0).Rows.Count > 0 Then
            If lDsDatos.Tables(0).Rows(0).Item("Padre") <> 0 Then
                    txtPadre.Valor = lDsDatos.Tables(0).Rows(0).Item("prdt_sra_nume").ToString
                    txtRpPadre.Valor = lDsDatos.Tables(0).Rows(0).Item("prdt_rp").ToString
                    txtPxPadre.Valor = lDsDatos.Tables(0).Rows(0).Item("prdt_px").ToString
                    txtNombrePadre.Valor = lDsDatos.Tables(0).Rows(0).Item("prdt_nomb").ToString
                    txtPadreDesc.Valor = lDsDatos.Tables(0).Rows(0).Item("_prdt_desc").ToString
            ElseIf lDsDatos.Tables(0).Rows(0).Item("Madre") <> 0 Then
                    txtMadre.Valor = lDsDatos.Tables(0).Rows(0).Item("prdt_sra_nume").ToString
                    txtRpMadre.Valor = lDsDatos.Tables(0).Rows(0).Item("prdt_rp").ToString
                    txtPxMadre.Valor = lDsDatos.Tables(0).Rows(0).Item("prdt_px").ToString
                    txtNombreMadre.Valor = lDsDatos.Tables(0).Rows(0).Item("prdt_nomb").ToString
                    txtMadreDesc.Valor = lDsDatos.Tables(0).Rows(0).Item("_prdt_desc").ToString
            End If

            If lDsDatos.Tables(0).Rows.Count > 1 Then
                If lDsDatos.Tables(0).Rows(1).Item("Padre") <> 0 Then
                        txtPadre.Valor = lDsDatos.Tables(0).Rows(1).Item("prdt_sra_nume").ToString
                        txtRpPadre.Valor = lDsDatos.Tables(0).Rows(1).Item("prdt_rp").ToString
                        txtPxPadre.Valor = lDsDatos.Tables(0).Rows(1).Item("prdt_px").ToString
                        txtNombrePadre.Valor = lDsDatos.Tables(0).Rows(1).Item("prdt_nomb").ToString
                        txtPadreDesc.Valor = lDsDatos.Tables(0).Rows(1).Item("_prdt_desc").ToString
                ElseIf lDsDatos.Tables(0).Rows(1).Item("Madre") <> 0 Then
                        txtMadre.Valor = lDsDatos.Tables(0).Rows(1).Item("prdt_sra_nume").ToString
                        txtRpMadre.Valor = lDsDatos.Tables(0).Rows(1).Item("prdt_rp").ToString
                        txtPxMadre.Valor = lDsDatos.Tables(0).Rows(1).Item("prdt_px").ToString
                        txtNombreMadre.Valor = lDsDatos.Tables(0).Rows(1).Item("prdt_nomb").ToString
                        txtMadreDesc.Valor = lDsDatos.Tables(0).Rows(1).Item("_prdt_desc").ToString
                End If
            End If
        Else
            Dim oProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString())
            Dim oRaza As New SRA_Neg.Razas(mstrConn, Session("sUserId").ToString())
            Dim dtProducto As DataTable

            dtProducto = oProducto.GetProductoById(hdnId.Text, "")



            If dtProducto.Rows.Count > 0 Then

                txtPadre.Valor = ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_hbap"), False)
                txtRpPadre.Valor = ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RpPd"), False)
                If ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazP"), False) <> "0" Then
                    txtPadreDesc.Valor = "Raza:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazP"), False) + "-" + _
                                        oRaza.GetRazaByRazaCodigo(ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazP"), False)).Trim + _
                                   " | HBA:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_hbap"), False) + _
                                   " | RP:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RpPd"), False)

                End If


                If ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazM"), False) <> "0" Then
                    txtMadre.Valor = ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_hbam"), False)
                    txtRpMadre.Valor = ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RpMd"), False)
                    txtMadreDesc.Valor = "Raza:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazM"), False) + "-" + _
                    oRaza.GetRazaByRazaCodigo(ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazM"), False)).Trim() + _
                     " | HBA:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_hbam"), False) + _
                     " | RP:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RpMd"), False)
                End If



            End If
        End If




    End Sub

    Private Sub mAgregar()
        mLimpiar()
        btnBaja.Enabled = False
        btnModi.Enabled = False
        btnProce.Enabled = False
        btnAlta.Enabled = True
        mMostrarPanel(True)
    End Sub

    Private Sub mLimpiarFiltros()
        hdnValorId.Text = "-1"  'para que ignore el id que vino del control
        mstrValorId = ""

        txtNumeFil.Text = ""
        txtTramNumeFil.Text = ""
        txtCuigFil.Text = ""
        txtNombFil.Text = ""
        txtRPNumeDesdeFil.Text = ""
        txtRPNumeHastaFil.Text = ""
        txtNumeDesdeFil.Text = ""
        txtNumeHastaFil.Text = ""
        txtApodoFil.Text = ""
        txtAsoNumeFil.Text = ""
        txtRPFil.Text = ""
        txtLaboNumeFil.Text = ""
        txtCtrolFactuNumeFil.Text = ""
        txtNaciFechaDesdeFil.Text = ""
        txtNaciFechaHastaFil.Text = ""
        txtInscFechaDesdeFil.Text = ""
        txtInscFechaHastaFil.Text = ""
        txtCondicionalDesdeFil.Text = ""
        txtCondicionalHastaFil.Text = ""
        If mstrProdCtrl = "" Then
            If mstrFilEsProp <> "" Then
                If mstrFilEsProp = "1" Then
                    usrPropCriaFil.Limpiar()
                Else
                    usrCriaFil.Limpiar()
                End If
            End If
        End If
        'If usrCriaFil.Activo Then usrCriaFil.Limpiar()
        txtObservacionFil.Text = ""
        usrMadreFil.Limpiar()
        usrPadreFil.Limpiar()
        usrReceFil.Limpiar()

        chkBusc.Checked = False
        chkBaja.Checked = False

        If cmbRazaFil.Enabled Then
            cmbRazaFil.Limpiar()
        End If

        cmbRegiTipoFil.Limpiar()
        cmbAsocFil.Limpiar()
        If Not cmbSexoFil.Enabled Then
            cmbSexoFil.Valor = mValorParametro(Request.QueryString("Sexo"))
        Else
            cmbSexoFil.Limpiar()
        End If
        cmbCtroFil.Limpiar()
        cmbEstaFil.Limpiar()
        cmbNaciFil.Limpiar()

        grdDato.Visible = False
    End Sub

    Public Overrides Sub grdAna_PageChanged(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdAna.EditItemIndex = -1
            If (grdAna.CurrentPageIndex < 0 Or grdAna.CurrentPageIndex >= grdAna.PageCount) Then
                grdAna.CurrentPageIndex = 0
            Else
                grdAna.CurrentPageIndex = E.NewPageIndex
            End If
            grdAna.DataSource = mdsDatos.Tables(mstrTablaAnalisis)
            grdAna.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub BloquearBotonCerficado(ByVal boolEstado As Boolean)

        If boolEstado Then

            btnCertInsc.Enabled = False

        End If
    End Sub

    Public Overrides Sub BloquearBotones(ByVal boolEstado As Boolean)
        btnModi.Enabled = Not boolEstado
        '   btnAltaObse.Enabled = Not boolEstado
        ' se debe permitir modificar porque aun no esta en el masivo
        btnAlta.Enabled = Not boolEstado
        btnAltaAsociacion.Enabled = Not boolEstado
        btnAltaDocum.Enabled = Not boolEstado
        btnAltaAna.Enabled = Not boolEstado
        '  btnBajaObse.Enabled = Not boolEstado
        btnBaja.Enabled = Not boolEstado
        btnBajaAsociacion.Enabled = Not boolEstado
        '  btnBajaDocum.Enabled = Not boolEstado
        btnBajaAna.Enabled = Not boolEstado
        '     btnModiObse.Enabled = Not boolEstado
        btnModi.Enabled = Not boolEstado
        btnModiAsociacion.Enabled = Not boolEstado
        '  btnModiDocum.Enabled = Not boolEstado
        btnModiAna.Enabled = Not boolEstado
        btnRevertirBaja.Enabled = False 'Not boolEstado


        ' btnProce.Enabled = Not boolEstado
        If boolEstado Then
            'cmbRegiTipo.Enabled = False Dario 2015-05-05
            hdnBloqueoCambioTatuaje.Text = "true"
            txtNaciFecha.Enabled = False
            txtFechaInspeccionDeVida.Enabled = False
            cmbResultadoInspecdeVida.Enabled = False
            cmbPelaA.Enabled = False
            cmbPelaB.Enabled = False
            txtNomb.Enabled = False
            txtInscFecha.Enabled = False

            ' EL boton de errorres dse  maneja aparte debido aaq ue se bloquea no se bloques en el caso de tramite el resto si

            btnCertInsc.Enabled = False
            chkSiete.Enabled = False
            cmbMelli.Enabled = False
            chkTranEmbr.Enabled = False
            cmbVari.Enabled = False
            cmbPelaC.Enabled = False
            cmbPelaD.Enabled = False
            'GSZ 14/04/2015 SE agrego descorne y castrado
            chkDescorne.Enabled = False
            chkCastrado.Enabled = False
            txtCastradoFecha.Enabled = False
        Else

            'GSZ 28/04/2015 SE agrego 
            'cmbRegiTipo.Enabled = True Dario 2015-05-05
            hdnBloqueoCambioTatuaje.Text = "false"
            txtNaciFecha.Enabled = True
            txtFechaInspeccionDeVida.Enabled = True
            cmbResultadoInspecdeVida.Enabled = True
            cmbPelaA.Enabled = True
            cmbPelaB.Enabled = True
            txtNomb.Enabled = True
            txtInscFecha.Enabled = True
            btnCertInsc.Enabled = True
            chkSiete.Enabled = True
            cmbMelli.Enabled = True
            chkTranEmbr.Enabled = True
            cmbVari.Enabled = True
            cmbPelaC.Enabled = True
            cmbPelaD.Enabled = True
            chkDescorne.Enabled = False
            chkCastrado.Enabled = False
            txtCastradoFecha.Enabled = True
            btnAltaObse.Enabled = True
            btnModiObse.Enabled = True
            btnBajaObse.Enabled = True

        End If

            If Not (chkCastrado.Checked) Then
                txtCastradoFecha.Enabled = False
            Else
                If chkCastrado.Enabled Then
                    txtCastradoFecha.Enabled = False
                End If
            End If

            If EspecieId = "2" And IIf(cmbSexo.Valor Is DBNull.Value, "0", cmbSexo.Valor) <> "1" Then
                chkCastrado.Enabled = True
                txtCastradoFecha.Enabled = True
            Else
                chkCastrado.Enabled = False
                txtCastradoFecha.Enabled = False
            End If

        End Sub

    Public Overrides Sub DataGridObse_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdObse.EditItemIndex = -1
            If (grdObse.CurrentPageIndex < 0 Or grdObse.CurrentPageIndex >= grdObse.PageCount) Then
                grdObse.CurrentPageIndex = 0
            Else
                grdObse.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarDocumentos(grdObse, True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub DataGridDocum_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdDocum.EditItemIndex = -1
            If (grdDocum.CurrentPageIndex < 0 Or grdDocum.CurrentPageIndex >= grdDocum.PageCount) Then
                grdDocum.CurrentPageIndex = 0
            Else
                grdDocum.CurrentPageIndex = E.NewPageIndex
            End If
            mConsultarDocumentos(grdDocum, False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub DataGridAsociacion_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
        Try
            grdAsociacion.EditItemIndex = -1
            If (grdAsociacion.CurrentPageIndex < 0 Or grdAsociacion.CurrentPageIndex >= grdAsociacion.PageCount) Then
                grdAsociacion.CurrentPageIndex = 0
            Else
                grdAsociacion.CurrentPageIndex = E.NewPageIndex
            End If
            grdAsociacion.DataSource = mdsDatos.Tables(mstrTablaAsociaciones)
            grdAsociacion.DataBind()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

        Private Sub mLimpiarAnalisis()
            hdnAnaId.Text = ""
            txtAnaNume.Text = ""
            cmbAnaTipo.Limpiar()
            txtAnaFecha.Text = ""
            txtAnaFechaResul.Text = ""
            cmbAnaResul.Limpiar()
            txtPureza.Text = ""
            txtTieneADN.Text = ""
            txtResulADN.Text = ""
            txtTieneTS.Text = ""
            txtResulTS.Text = ""
            txtTieneSNPS.Text = ""
            txtResulSNPS.Text = ""
            txtTienePelo.Text = ""
            txtFechaUltAnalisis.Fecha = System.DBNull.Value.ToString
            lblEstudio.Text = ""
            mSetearEditor(mstrTablaAnalisis, True)
        End Sub

    Private Sub mLimpiarAsoc()
        hdnAsocId.Text = ""
        cmbAsoc.Limpiar()
        txtNroAsoc.Text = ""
    End Sub

    Private Sub mLimpiarPesadasDeps()
        'Pesasdas - A�o Corrida
        txtAnioCorridaNacimiento.Valor = String.Empty
        txtAnioCorridaDestete.Valor = String.Empty
        txtAnioCorridaPosDestete.Valor = String.Empty
        txtAnioCorridaCircEscr.Valor = String.Empty
        'Pesasdas - Fecha
        txtFechaPesadasNacimiento.Text = String.Empty
        txtFechaPesadasDestete.Text = String.Empty
        txtFechaPesadasPosDestete.Text = String.Empty
        txtFechaPesadasCircEscr.Text = String.Empty
        'Pesasdas - Medici�n
        txtMedicionNacimiento.Valor = String.Empty
        txtMedicionDestete.Valor = String.Empty
        txtMedicionPosDestete.Valor = String.Empty
        txtMedicionCircEscr.Valor = String.Empty
        'Pesasdas - Manejo
        txtManejoDestete.Valor = String.Empty
        txtManejoPosDestete.Valor = String.Empty
        'Pesasdas - OBS
        txtOBSDestete.Valor = String.Empty
        txtOBSPosDestete.Valor = String.Empty

        'Deps - Fecha
        txtDEPSFecha.Text = String.Empty
        'Deps - EPD
        txtEPDNacimiento.Valor = String.Empty
        txtEPDDestete.Valor = String.Empty
        txtEPDPosDestete.Valor = String.Empty
        txtEPDCircEscr.Valor = String.Empty
        txtEPDLecheMaterna.Valor = String.Empty
        txtEPDLecheYCrianza.Valor = String.Empty
        'Deps - Exactitud
        txtExactitudNacimiento.Valor = String.Empty
        txtExactitudDestete.Valor = String.Empty
        txtExactitudPosDestete.Valor = String.Empty
        txtExactitudCircEscr.Valor = String.Empty
        txtExactitudLecheMaterna.Valor = String.Empty
    End Sub

    Private Sub mLimpiarDocum()
        hdnDocumId.Text = ""
        chkEsImg.Checked = False
        cmbReferencia.Limpiar()
        txtFoto.Text = ""
        lblBajaDocum.Text = ""
        mSetearEditorDocum(True)
    End Sub

    Private Sub mLimpiarObse()
        hdnObseId.Text = ""
        txtObseRefe.Text = ""
        txtObseDocu.Text = ""
        mSetearEditorObse(True)
    End Sub

        Private Sub mLimpiar()
            hdnId.Text = ""
            hdnPdinId.Text = ""
            lblBaja.Text = ""
            lblEmbargado.Text = ""
            cmbMelli.Limpiar()
            cmbEsta.Limpiar()
            hdnEspe.Text = ""
            txtCali.Text = ""
            txtCuig.Text = ""
            txtPropClie.Text = ""
            lblNdad.Text = ""
            txtSraNume.Text = ""
            txtTram.Text = ""
            txtRPNume.Text = ""
            txtRP.Text = ""
            txtNomb.Text = ""
            txtApod.Text = ""
            txtDona.Text = ""
            txtTranFecha.Text = ""
            txtNaciFecha.Text = ""
            txtInscFecha.Text = ""
            ' GSZ 2014-07-16
            txtFechaInspeccionDeVida.Fecha = System.DBNull.Value.ToString
            txtFalleFecha.Text = ""
            'txtPesoNacer.Text = ""
            'txtPesoDeste.Text = ""
            'txtPesoFinal.Text = ""
            'txtEpdNacer.Text = ""
            'txtEpdDeste.Text = ""
            'txtEpdFinal.Text = ""
            txtPX.Text = ""
            txtRece.Text = ""
            txtServFecha.Text = ""
            txtImplFecha.Text = ""
            txtServTipo.Text = ""
            txtProp.Valor = ""
            txtPropClie.Valor = ""
            txtPropDesc.Valor = ""
            txtPadreDesc.Valor = ""
            txtMadreDesc.Valor = ""
            txtPadre.Valor = ""
            txtRpPadre.Valor = ""
            txtPxPadre.Valor = ""
            txtNombrePadre.Valor = ""
            txtMadre.Valor = ""
            txtRpMadre.Valor = ""
            txtPxMadre.Valor = ""
            txtNombreMadre.Valor = ""
            txtAsoNume.Valor = ""
            txtRPExtranjero.Valor = ""
            txtCondicional.Text = ""
            txtFechaTransferencia.Fecha = System.DBNull.Value.ToString
            txtNroAnal.Valor = ""
            txtResulAnal.Valor = ""
            txtPureza.Valor = ""
            txtTieneADN.Valor = ""
            txtTieneTS.Valor = ""
            txtTienePelo.Valor = ""
            txtTieneSNPS.Valor = ""
            txtResulADN.Valor = ""
            txtResulTS.Valor = ""
            txtResulSNPS.Valor = ""
            txtCondGeneticas.Valor = ""

            cmbRaza.Limpiar()
            cmbSexo.Limpiar()
            cmbPelaA.Limpiar()
            cmbPelaB.Limpiar()
            cmbPelaC.Limpiar()
            cmbPelaD.Limpiar()
            cmbRegiTipo.Limpiar()
            cmbBaja.Limpiar()
            cmbVari.Limpiar()
            cmbAsociacion.Limpiar()

            chkSiete.Checked = False
            chkTranEmbr.Checked = False

            'GSZ 10-04-2015 
            chkDescorne.Checked = False
            chkCastrado.Checked = False
            txtCastradoFecha.Text = ""
            txtCria.Text = ""
            txtCriaDesc.Text = ""

            hdnCriaId.Text = ""
            ' Dario 2015-01-23 para que no pise el rp 
            hdnCambioTatuajePop.Text = ""
            hdnBloqueoCambioTatuaje.Text = "false"

            hdnSoloConsulta.Text = "false"
            hdnSRANoInscribe.Text = "false"
            hdnExtranjero.Text = "false"
            hdnMensajeConsulta.Text = ""

            lblApro.Text = ""
            ' mCrearDataSet("")

            lblTitu.Text = ""
            lblFactura.Text = ""
            mLimpiarAnalisis()
            mLimpiarAsoc()
            mLimpiarDocum()
            mSetearEditor("", True)
            mLimpiarFiltrosConsulta()
            mLimpiarPesadasDeps()


        End Sub

    Private Sub mCerrar()

        mMostrarPanel(False)
    End Sub

    Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)

        lnkCabecera.Font.Bold = True
        lnkAso.Font.Bold = False
        lnkDocumentos.Font.Bold = False
        lnkObse.Font.Bold = False
        lnkOtrosDatos.Font.Bold = False
        lnkPesadasDeps.Font.Bold = False

        panDato.Visible = pbooVisi
        panBotones.Visible = pbooVisi
        panFiltros.Visible = Not panDato.Visible
        grdDato.Visible = Not panDato.Visible

        panCabecera.Visible = True
        panAna.Visible = False
        panAsociacion.Visible = False
        panDocumentos.Visible = False
        panObse.Visible = False
        panOtrosDatos.Visible = False
        panPesadasDeps.Visible = False

        tabLinks.Visible = pbooVisi
    End Sub

    Private Sub mShowTabs(ByVal origen As Byte)
        Dim lstrTitu As String
        If lblTitu.Text <> "" Then
            lstrTitu = lblTitu.Text.Substring(lblTitu.Text.IndexOf(":") + 2)
        End If

        panDato.Visible = True
        panBotones.Visible = True
        panAna.Visible = False
        panCabecera.Visible = False

        lnkCabecera.Font.Bold = False

        panAsociacion.Visible = False
        lnkAso.Font.Bold = False

        panDocumentos.Visible = False
        lnkDocumentos.Font.Bold = False

        panObse.Visible = False
        lnkObse.Font.Bold = False

        panOtrosDatos.Visible = False
        lnkOtrosDatos.Font.Bold = False

        panPesadasDeps.Visible = False
        lnkPesadasDeps.Font.Bold = False

        Select Case origen
            Case 1
                panCabecera.Visible = True
                lnkCabecera.Font.Bold = True
            Case 2
                panAna.Visible = True
                lblTitu.Text = "Producto: " & lstrTitu
                lblTitu.Visible = True
                grdAna.Visible = True
            Case 3
                panAsociacion.Visible = True
                lnkAso.Font.Bold = True
                clsWeb.gCargarCombo(mstrConn, "asociaciones_cargar @asoc_raza_id=" & cmbRaza.Valor.ToString & ",@mostrar_sra=0", cmbAsoc, "id", "descrip", "S")
            Case 4
                panDocumentos.Visible = True
                lnkDocumentos.Font.Bold = True
            Case 5
                panOtrosDatos.Visible = True
                lnkOtrosDatos.Font.Bold = True
            Case 6
                panObse.Visible = True
                lnkObse.Font.Bold = True
            Case 7
                panPesadasDeps.Visible = True
                lnkPesadasDeps.Font.Bold = True
        End Select
    End Sub

#End Region

#Region "Opciones de ABM"

    Private Sub mAlta()
        Try
            Dim lstrProdId As String

            mGuardarDatos(False)
            'Dim lobjProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaAnalisis, mdsDatos)
            'lstrProdId = lobjProducto.Alta()

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
            lobjGenerica.Alta()

            If mstrProdCtrl <> "" Then
                mProducto(lstrProdId, "", "", "", "", "", "", "", "", "", "")
                Return
            End If

            mConsultar(True)

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mProcesar()
        Try
            Dim lstrMsgError As String
            Dim lstrProdId As String
            Dim lstrRazaId As String
            Dim lstrSexo As String
            Dim lstrId As String
            'Dim dsVali As DataSet = New DataSet

            'MODIFICA EL PRODUCTO  
            If Not mModi(True) Then
                Return
            End If

            'REPROCESAR EL PRODUCTO
            ''dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.Nacimientos.ToString())
            'dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), "W", txtNaciFecha.Text, txtNaciFecha.Text)

            If (txtSraNume.Text = "") Then
                lstrProdId = hdnId.Text
                lstrRazaId = cmbRaza.Valor.ToString
                lstrSexo = cmbSexo.Valor
                lstrId = hdnPdinId.Text

                'Limpiar errores anteriores.
                ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, lstrId)

                'Aplicar Reglas.
                lstrMsgError = ReglasValida.Validaciones.gValidarReglas(mstrConn, SRA_Neg.Constantes.gTab_ProductosInscrip, lstrId, lstrRazaId, lstrSexo, Session("sUserId").ToString(), True, ReglasValida.Validaciones.Procesos.Nacimientos.ToString(), 0, 0)

                'mConsultar(False)
                'mMostrarPanel(False)
                mCargarDatos(Convert.ToInt32(lstrProdId))
                If lstrMsgError <> "" Then
                    AccesoBD.clsError.gGenerarMensajes(Me, "Se detectaron errores validatorios, por favor verifiquelo.")
                End If
            End If
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub

    Private Function mModi(ByVal pbooReproceso As Boolean) As Boolean
        Try
            If txtFalleFecha.Text <> "" And cmbBaja.Valor Is DBNull.Value Then
                Throw New AccesoBD.clsErrNeg("Debe indicar el motivo de la baja.")
            End If

            mGuardarDatos(pbooReproceso)
            'Dim lobjProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaAnalisis, mdsDatos)
            'lobjProducto.Modi()
            mdsDatos.Tables.Remove(mdsDatos.Tables(mstrTablaAnalisis))

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
            lobjGenerica.Modi()

            If mstrValorId <> "" Then
                mProducto(hdnId.Text, "", "", "", "", "", "", "", "", "", "")
                Return True
            End If

            If Not pbooReproceso Then
                mConsultar(False)
                mMostrarPanel(False)
            End If

            hdnModi.Text = "S"

            Return True

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
            Return False
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Function

    Private Sub mBaja()
        Try
            Dim lintPage As Integer = grdDato.CurrentPageIndex

            'Dim lobjProducto As New SRA_Neg.Productos(mstrConn, Session("sUserId").ToString(), mstrTabla, mstrTablaAnalisis, mdsDatos)
            'lobjProducto.Baja(hdnId.Text)

            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
            lobjGenerica.Baja(clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int))
            grdDato.CurrentPageIndex = 0

            mConsultar(True)

            If (lintPage < grdDato.PageCount) Then
                grdDato.CurrentPageIndex = lintPage
                mConsultar(True)
            End If

            mMostrarPanel(False)

            hdnModi.Text = "S"

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub

    Private Sub mGuardarDatos(ByVal pbooReproceso As Boolean)

        mValidarDatos()

            With mdsDatos.Tables(0).Rows(0)
                .Item("prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("prdt_rp") = txtRP.Valor
                .Item("reproceso") = pbooReproceso
                .Item("prdt_nomb") = txtNomb.Valor
                .Item("prdt_Cond_Geneticas") = txtCondGeneticas.Valor
                .Item("prdt_naci_fecha") = IIf(txtNaciFecha.Fecha.ToString.Trim.Length > 0, txtNaciFecha.Fecha, DBNull.Value)
                If txtFalleFecha.Fecha.ToString.Length > 0 Then
                    .Item("prdt_fall_fecha") = txtFalleFecha.Fecha
                End If
                .Item("prdt_tram_nume") = IIf(txtTram.Valor.ToString.Trim.Length > 0, txtTram.Valor, DBNull.Value)
                If hdnCriaId.Text <> "" Then
                    .Item("prdt_cria_id") = hdnCriaId.Text
                Else
                    .Item("prdt_cria_id") = DBNull.Value
                End If
                If cmbBaja.Valor.ToString.Length > 0 Then
                    .Item("prdt_bamo_id") = cmbBaja.Valor
                End If
                .Item("prdt_raza_id") = cmbRaza.Valor
                .Item("prdt_esta_id") = cmbEsta.Valor
                .Item("prdt_sra_nume") = IIf(txtSraNume.Valor.ToString.Trim.Length > 0, txtSraNume.Valor, DBNull.Value)
                .Item("prdt_sexo") = IIf(cmbSexo.Valor = "1", True, False)
                If cmbPelaA.Valor.ToString.Length > 0 Then
                    .Item("prdt_a_pela_id") = cmbPelaA.Valor
                Else
                    .Item("prdt_a_pela_id") = DBNull.Value
                End If
                If cmbPelaB.Valor.ToString.Length > 0 Then
                    .Item("prdt_b_pela_id") = cmbPelaB.Valor
                Else
                    .Item("prdt_b_pela_id") = DBNull.Value
                End If
                If cmbPelaC.Valor.ToString.Length > 0 Then
                    .Item("prdt_c_pela_id") = cmbPelaC.Valor
                Else
                    .Item("prdt_c_pela_id") = DBNull.Value
                End If
                If cmbPelaD.Valor.ToString.Length > 0 Then
                    .Item("prdt_d_pela_id") = cmbPelaD.Valor
                Else
                    .Item("prdt_d_pela_id") = DBNull.Value
                End If
                If cmbVari.Valor.ToString.Length > 0 Then
                    .Item("prdt_vari_id") = cmbVari.Valor
                End If
                If txtCuig.Valor.Length > 0 Then
                    .Item("prdt_cuig") = txtCuig.Valor
                End If
                If cmbRegiTipo.Valor.Length > 0 Then
                    .Item("prdt_regt_id") = cmbRegiTipo.Valor
                End If
                .Item("prdt_px") = IIf(txtPX.Valor.ToString.Trim.Length > 0, txtPX.Valor, DBNull.Value)
                .Item("prdt_apodo") = IIf(txtApod.Valor.ToString.Trim.Length > 0, txtApod.Valor, DBNull.Value)
                .Item("prdt_siete") = chkSiete.Checked
                If cmbMelli.Valor.ToString.Length > 0 Then
                    .Item("prdt_melli") = cmbMelli.Valor
                End If
                .Item("prdt_te") = chkTranEmbr.Checked
                If txtDona.Valor.Length > 0 Then
                    .Item("prdt_dona_nume") = txtDona.Valor
                End If
                If txtNroSenasa.Valor.Length > 0 Then
                    .Item("prdt_dona_sena_nume") = txtNroSenasa.Valor
                End If
                ' .Item("prdt_dona_fecha") = txtDadorDonanteFechaDesde.Fecha
                '.Item("prdt_peso_nacer") = txtPesoNacer.Valor
                '.Item("prdt_peso_deste") = txtPesoDeste.Valor
                '.Item("prdt_peso_final") = txtPesoFinal.Valor
                If txtInscFecha.Fecha.ToString.Length > 0 Then
                    .Item("prdt_insc_fecha") = txtInscFecha.Fecha
                End If
                If txtFechaInspeccionDeVida.Fecha.ToString.Length > 0 Then
                    .Item("prdt_Fecha_InspecMayor25") = txtFechaInspeccionDeVida.Fecha
                End If
                If cmbResultadoInspecdeVida.SelectedValue.ToString.Length > 0 Then
                    .Item("prdt_Resul_InspecMayor25") = cmbResultadoInspecdeVida.SelectedValue
                End If

                '.Item("prdt_epd_nacer") = txtEpdNacer.Valor
                '.Item("prdt_epd_deste") = txtEpdDeste.Valor
                '.Item("prdt_epd_final") = txtEpdFinal.Valor
                .Item("prdt_rp") = txtRP.Valor
                If txtRPNume.Valor.ToString.Length Then
                    .Item("prdt_rp_nume") = txtRPNume.Valor.ToString
                End If
                .Item("prdt_descorne") = chkDescorne.Checked
                .Item("prdt_castrado") = chkCastrado.Checked

                If chkCastrado.Checked And txtCastradoFecha.Fecha Is System.DBNull.Value Then
                    Throw New AccesoBD.clsErrNeg("Debe indicar la Fecha de Castramiento")
                End If

                If txtCastradoFecha.Fecha.ToString.Length > 0 Then
                    .Item("prdt_Fecha_castrado") = txtCastradoFecha.Fecha
                End If

                If txtRPExtranjero.Text.Trim.Length > 0 Then
                    .Item("prdt_rp_extr") = txtRPExtranjero.Text.Trim
                End If
                If cmbAsociacion.Valor.Trim.Length > 0 Then
                    .Item("prdt_ori_asoc_id") = cmbAsociacion.Valor.Trim
                End If

                If txtAsoNume.Text.Trim.Length > 0 Then
                    .Item("prdt_ori_asoc_nume") = txtAsoNume.Text.Trim
                End If


            End With


        End Sub

    Private Sub mValidarDatos()
        clsWeb.gInicializarControles(Me, mstrConn)
        clsWeb.gValidarControles(Me)
    End Sub

    Public Overrides Sub mCrearDataSet(ByVal pstrId As String)
        Dim lstrOpci As String


        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        mdsDatos.Tables(0).TableName = mstrTabla
        mdsDatos.Tables(1).TableName = mstrTablaAnalisis
        mdsDatos.Tables(2).TableName = mstrTablaAsociaciones
        mdsDatos.Tables(3).TableName = mstrTablaDocum

        If mdsDatos.Tables(mstrTabla).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTabla).Rows.Add(mdsDatos.Tables(mstrTabla).NewRow)
        End If

        If mdsDatos.Tables(mstrTablaAnalisis).Rows.Count = 0 Then
            mdsDatos.Tables(mstrTablaAnalisis).Rows.Add(mdsDatos.Tables(mstrTablaAnalisis).NewRow)
        End If


        Session(mstrTabla) = mdsDatos

        'Se coment� porque anteriormente se grababan los an�lisis, ahora es solo consulta.
        'grdAna.DataSource = mdsDatos.Tables(mstrTablaAnalisis)
        'grdAna.DataBind()
        'grdAna.Visible = True

        grdAsociacion.DataSource = mdsDatos.Tables(mstrTablaAsociaciones)
        grdAsociacion.DataBind()
        grdAsociacion.Visible = True

        mConsultarDocumentos(grdDocum, False)
        mConsultarDocumentos(grdObse, True)
    End Sub

    'Seccion de los An�lisis
    Public Overrides Sub mEditarDatosAnalisis(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            'Se coment� porque anteriormente se grababan los an�lisis, ahora es solo consulta.

            'Dim ldrAsoc As DataRow
            'Dim lstrActiProp As String
            'Dim lstrActiNoProp As String
            'Dim lbooGene As Boolean

            'hdnAnaId.Text = E.Item.Cells(1).Text
            'ldrAsoc = mdsDatos.Tables(mstrTablaAnalisis).Select("prta_id=" & hdnAnaId.Text)(0)

            'With ldrAsoc
            '    txtAnaNume.Valor = .Item("prta_nume")
            '    'cmbAnaTipo.Valor = .Item("prta_tipo")
            '    cmbAnaResul.Valor = .Item("prta_ares_id")
            '    txtAnaFecha.Fecha = .Item("prta_fecha")
            '    txtAnaFechaResul.Fecha = .Item("prta_resul_fecha")
            'End With

            'mSetearEditor(mstrTablaAnalisis, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarAnalisis(ByVal pbooAlta As Boolean)
        Try
            mGuardarDatosAnalisis(pbooAlta)

            mLimpiarAnalisis()
            grdAna.DataSource = mdsDatos.Tables(mstrTablaAnalisis)
            grdAna.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosAnalisis(ByVal pbooAlta As Boolean)
        Dim ldrDatos As DataRow

        If txtAnaNume.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el N� de An�lisis.")
        End If

        If cmbAnaTipo.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el Tipo de An�lisis.")
        End If

        If txtAnaFecha.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la Fecha de An�lisis.")
        End If

        If cmbAnaResul.Valor Is DBNull.Value Then
            Throw New AccesoBD.clsErrNeg("Debe indicar el Resultado.")
        End If

        If txtAnaFechaResul.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe indicar la Fecha de Resultado.")
        End If

        If hdnAnaId.Text = "" Then
            ldrDatos = mdsDatos.Tables(mstrTablaAnalisis).NewRow
            ldrDatos.Item("prta_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaAnalisis), "prta_id")
        Else
            ldrDatos = mdsDatos.Tables(mstrTablaAnalisis).Select("prta_id=" & hdnAnaId.Text)(0)
        End If

        With ldrDatos
            .Item("prta_prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("prta_nume") = txtAnaNume.Valor
            .Item("prta_ares_id") = cmbAnaResul.Valor
            .Item("_resul") = cmbAnaResul.SelectedItem.Text
            .Item("prta_fecha") = txtAnaFecha.Fecha
            .Item("prta_baja_fecha") = DBNull.Value
            .Item("prta_resul_fecha") = txtAnaFechaResul.Fecha
        End With

        If hdnAnaId.Text = "" Then
            mdsDatos.Tables(mstrTablaAnalisis).Rows.Add(ldrDatos)
        End If
    End Sub

    'Seccion de los Asociacion
    Private Sub mActualizarAsoc()
        Try
            mGuardarDatosAsoc()

            mLimpiarAsoc()
            grdAsociacion.DataSource = mdsDatos.Tables(mstrTablaAsociaciones)
            grdAsociacion.DataBind()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBajaAsoc()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaAsociaciones).Select("ptnu_id=" & hdnAsocId.Text)(0)
            row.Delete()
            grdAsociacion.DataSource = mdsDatos.Tables(mstrTablaAsociaciones)
            grdAsociacion.DataBind()
            mLimpiarAsoc()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosAsoc()
        Dim ldrAsoc As DataRow

        If cmbAsoc.Valor.ToString = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar una Asociaci�n")
        End If
        If hdnAsocId.Text = "" Then
            ldrAsoc = mdsDatos.Tables(mstrTablaAsociaciones).NewRow
            ldrAsoc.Item("ptnu_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaAsociaciones), "ptnu_id")
        Else
            ldrAsoc = mdsDatos.Tables(mstrTablaAsociaciones).Select("ptnu_id=" & hdnAsocId.Text)(0)
        End If

        With ldrAsoc
            .Item("ptnu_asoc_id") = cmbAsoc.Valor
            .Item("ptnu_prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("ptnu_nume") = txtNroAsoc.Valor
            .Item("_asoc_deno") = cmbAsoc.SelectedItem.Text
        End With
        If (hdnAsocId.Text = "") Then
            mdsDatos.Tables(mstrTablaAsociaciones).Rows.Add(ldrAsoc)
        End If
    End Sub

    Public Overrides Sub mEditarDatosAsociacion(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrAsoc As DataRow
            Dim lstrActiProp As String
            Dim lstrActiNoProp As String
            Dim lbooGene As Boolean

            hdnAsocId.Text = E.Item.Cells(1).Text
            ldrAsoc = mdsDatos.Tables(mstrTablaAsociaciones).Select("ptnu_id=" & hdnAsocId.Text)(0)

            With ldrAsoc
                txtNroAsoc.Text = .Item("ptnu_nume")
                cmbAsoc.Valor = .Item("ptnu_asoc_id")
                .Item("_asoc_codi") = cmbAsoc.Valor
                .Item("_asoc_deno") = cmbAsoc.SelectedItem.Text
            End With



            mSetearEditor(mstrTablaAsociaciones, False)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    'Seccion de Documentos
    Public Overrides Sub mEditarDatosDocum(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrDocum As DataRow
            Dim lstrCarpeta As String

            lstrCarpeta = System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString()
            lstrCarpeta += "/" + clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_raza_path") + "/"

            hdnDocumId.Text = E.Item.Cells(1).Text
            ldrDocum = mdsDatos.Tables(mstrTablaDocum).Select("prdo_id=" & hdnDocumId.Text)(0)

            With ldrDocum
                cmbReferencia.Valor = .Item("prdo_refe")
                txtFoto.Valor = .Item("prdo_path")
                chkEsImg.Checked = .Item("prdo_imag")

                If Not .IsNull("prdo_baja_fecha") Then
                    lblBajaDocum.Text = "Documento dado de baja en fecha: " & CDate(.Item("prdo_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaDocum.Text = ""
                End If

                'GetObsProdInscDetalleByProdId

                If .IsNull("prdo_path") Then
                    txtFoto.Valor = ""
                    imgDelFoto.Visible = False
                Else
                    imgDelFoto.Visible = True
                    txtFoto.Valor = .Item("prdo_path")
                    btnFotoVer.Attributes.Add("onclick", String.Format("gAbrirVentanas('{0}', 14, '200','200');", lstrCarpeta + hdnDocumId.Text + "_" + mstrTablaDocum + "_" + txtFoto.Text.Substring(txtFoto.Text.LastIndexOf("\") + 1)))
                End If

                .Item("prdo_prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("prdo_imag") = chkEsImg.Checked
                .Item("prdo_audi_user") = Session("sUserId").ToString()
                .Item("prdo_baja_fecha") = DBNull.Value
                .Item("_estado") = "Activo"
            End With
            mSetearEditorDocum(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Public Overrides Sub mEditarDatosObse(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
        Try
            Dim ldrObse As DataRow
            Dim lstrCarpeta As String

            lstrCarpeta = System.Configuration.ConfigurationSettings.AppSettings("conUrlSitio").ToString()
            lstrCarpeta += "/" + clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_raza_path") + "/"

            hdnObseId.Text = E.Item.Cells(1).Text
            ldrObse = mdsDatos.Tables(mstrTablaDocum).Select("prdo_id=" & hdnObseId.Text)(0)

            With ldrObse
                txtObseRefe.Valor = .Item("prdo_refe")
                txtObseDocu.Valor = .Item("prdo_path")

                If Not .IsNull("prdo_baja_fecha") Then
                    lblBajaObse.Text = "Observaci�n dado de baja en fecha: " & CDate(.Item("prdo_baja_fecha")).ToString("dd/MM/yyyy HH:mm")
                Else
                    lblBajaObse.Text = ""
                End If

                If .IsNull("prdo_path") Then
                    txtObseDocu.Valor = ""
                    imgDelDocu.Visible = False
                Else
                    imgDelDocu.Visible = True
                    txtObseDocu.Valor = .Item("prdo_path")
                    btnDocuVer.Attributes.Add("onclick", String.Format("gAbrirVentanas('{0}', 14, '200','200');", lstrCarpeta + hdnObseId.Text + "_" + mstrTablaDocum + "_" + txtObseDocu.Text.Substring(txtObseDocu.Text.LastIndexOf("\") + 1)))
                End If

                .Item("prdo_prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
                .Item("prdo_audi_user") = Session("sUserId").ToString()
                .Item("prdo_baja_fecha") = DBNull.Value
                .Item("_estado") = "Activo"
            End With
            mSetearEditorObse(False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mActualizarObse()
        Try
            mGuardarDatosObse()
            mLimpiarObse()
            mConsultarDocumentos(grdObse, True)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mConsultarDocumentos(ByVal pobjGrilla As Web.UI.WebControls.DataGrid, ByVal pbooObse As Boolean)
        Dim ldvDocu As New DataView

        ldvDocu.Table = mdsDatos.Tables(mstrTablaDocum)
        If pbooObse Then
            ldvDocu.RowFilter = "isnull(prdo_obse,0)=1"
        Else
            ldvDocu.RowFilter = "isnull(prdo_obse,0)=0"
        End If
        pobjGrilla.DataSource = ldvDocu
        pobjGrilla.DataBind()
        pobjGrilla.Visible = True
    End Sub

    Private Sub mActualizarDocum()
        Try
            mGuardarDatosDocum()
            mLimpiarDocum()
            mConsultarDocumentos(grdDocum, False)
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBajaDocum()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDocum).Select("prdo_id=" & hdnDocumId.Text)(0)
            row.Delete()
            mConsultarDocumentos(grdDocum, False)
            mLimpiarDocum()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mBajaObse()
        Try
            Dim row As Data.DataRow = mdsDatos.Tables(mstrTablaDocum).Select("prdo_id=" & hdnObseId.Text)(0)
            row.Delete()
            mConsultarDocumentos(grdObse, True)
            mLimpiarObse()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mImprimirCertificado()
        Try
            Dim params As String
            Dim lstrRptName As String
            Dim lstrRpt As String
            'GSZ:  29/10/2014 Se agrego reg_tipo  para que imprima solo el certificado solicitado
            params += "&hba_desde=" + txtSraNume.Text
            params += "&hba_hasta=" + txtSraNume.Text
            params += "&sexo=" + IIf(cmbSexo.Valor = 0, "False", "True")

            If cmbRaza.Valor.ToString <> "" Then
                params += "&raza_id=" + cmbRaza.Valor.ToString
            Else

                params += "&raza_id=" & IIf(usrCriaFil.RazaId.ToString = "", "0", _
                                                 usrCriaFil.RazaId.ToString)

            End If

            params += "&criador_id=" & IIf(usrCriaFil.Codi = "", "0", usrCriaFil.Valor)
            params += "&FilFechaInscripcion=" & True
            params += "&FechaDesde=" & IIf(txtInscFechaDesdeFil.Fecha.ToString = "", "0", _
                    mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime( _
                    txtInscFechaDesdeFil.Text))))


            params += "&FechaHasta=" & IIf(txtInscFechaHastaFil.Fecha.ToString = "", "0", _
              mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtInscFechaHastaFil.Text))))

            params += "&tipoCertificado= 0"
            If cmbRegiTipoFil.Valor.ToString() Then
                params += "&reg_tipo=" + cmbRegiTipoFil.SelectedItem.Text
            End If
            params += "&audi_user=" & Session("sUserId").ToString()


            Select Case hdnEspe.Text
                Case SRA_Neg.Constantes.Especies.Ovinos, SRA_Neg.Constantes.Especies.Caprinos
                    'GSZ 2015-06-19 Se modifico por pedido de MF porque si el criador  es igual al propietario se
                    'debe imprimir el de inscripcion
                    If ValidarNulos(usrProp.Codi, False) = usrCriaFil.Codi Then
                        If cmbSexo.Valor = 0 Then
                            'Hembra.
                            lstrRptName = "CertificadoRegIndivLanarHembra"
                            params += "&prdt_id=" & hdnId.Text
                            'GSZ 2015-07-27 Se agrego producto 
                        Else
                            'Macho.
                            lstrRptName = "CertificadoRegIndivLanar"
                            params += "&prdt_id=" & hdnId.Text
                            'GSZ 2015-07-27 Se agrego producto 
                        End If
                    Else
                        If cmbSexo.Valor = 0 Then
                            'Hembra.
                            lstrRptName = "CertificadoRegPropLanarHembra"
                            params += "&prdt_id=" & hdnId.Text
                            'GSZ 2015-07-27 Se agrego producto 

                        Else
                            'Macho.
                            lstrRptName = "CertificadoRegPropLanar"
                            params += "&prdt_id=" & hdnId.Text
                            'GSZ 2015-07-27 Se agrego producto 
                        End If
                    End If

                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)

                Case SRA_Neg.Constantes.Especies.Bovinos
                    ' GSZ 26/11/2014 Se saco el txtprop dado que no se usa mas con el control nuevo
                    If ValidarNulos(usrProp.Codi, False) <> "0" Then
                        params += "&propie=" + ValidarNulos(usrProp.Codi, False) + " - " + ValidarNulos(usrProp.Apel, False)
                    Else
                        params += "&propie=" + " - "
                    End If


                    'GSZ 2015-06-19 Se modifico por pedido de MF porque si el criador  es igual al propietario se
                    'debe imprimir el de inscripcion
                    If ValidarNulos(usrProp.Codi, False) = usrCriaFil.Codi Then
                        lstrRptName = "CertificadoRegIndivBovinos"
                    Else
                        ' MF 14/01/2014 Sergio de la Iglesia, pidio que se emita el certificado de propiedad y no de inscripcion 
                        lstrRptName = "CertificadoRegPropBovinos"
                    End If

                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)

                Case SRA_Neg.Constantes.Especies.Peliferos
                    'GSZ 2015-06-19 Se modifico por pedido de MF porque si el criador  es igual al propietario se
                    'debe imprimir el de inscripcion
                    If ValidarNulos(usrProp.Codi, False) = usrCriaFil.Codi Then
                        lstrRptName = "CertificadoRegIndivPeliferos"
                    Else
                        lstrRptName = "CertificadoRegPropPeliferos"
                    End If

                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)

                Case SRA_Neg.Constantes.Especies.Equinos
                    'GSZ 06/03/2015 Se modifico para llamar equinos siempre vaya  a cert de propiedad
                    params = "&hba_desde=" & txtSraNume.Text
                    params += "&hba_hasta=" & txtSraNume.Text
                    params += "&sexo=" + IIf(cmbSexo.Valor = 0, "False", "True")
                    params += "&raza_id=" & cmbRaza.Valor.ToString
                    params += "&criador_id=" & IIf(usrCriaFil.Codi = "", "0", usrCriaFil.Valor)
                    params += "&FilFechaInscripcion=" & False
                    params += "&FechaDesde=" & IIf(txtInscFechaDesdeFil.Fecha.ToString = "", "0", _
                   mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime( _
                   txtInscFechaDesdeFil.Text))))

                    params += "&FechaHasta=" & IIf(txtInscFechaHastaFil.Fecha.ToString = "", "0", _
                               mQuitarComillas(clsFormatear.gFormatFecha2DB(clsFormatear.gFormatFechaDateTime(txtInscFechaHastaFil.Text))))

                    params += "&tipoCertificado=0"

                    params += "&audi_user=" & Session("sUserId").ToString()
                    If Not cmbRegiTipo.Valor Is System.DBNull.Value Then
                        params += "&reg_tipo=" + cmbRegiTipo.SelectedItem.Text.Trim
                    End If

                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)

                    'GSZ 2015-06-19 Se modifico por pedido de MF porque si el criador  es igual al propietario se
                    'debe imprimir el de inscripcion
                    If ValidarNulos(usrProp.Codi, False) = usrCriaFil.Codi Then
                        lstrRptName = "CertificadoRegIndivEquino"
                    Else
                        lstrRptName = "CertificadoRegPropEquino"
                    End If

                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)

                Case SRA_Neg.Constantes.Especies.Camelidos
                    'GSZ 2015-06-22 Se modifico por pedido de MF porque si el criador  es igual al propietario se
                    'debe imprimir el de inscripcion
                    If ValidarNulos(usrProp.Codi, False) = usrCriaFil.Codi Then
                        lstrRptName = "CertificadoRegIndivCamelidos"
                    Else
                        lstrRptName = "CertificadoRegPropCamelidos"
                    End If

                    lstrRpt = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
                    lstrRpt += params
                    lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
                    Response.Redirect(lstrRpt)

                Case Else
                    Return
            End Select

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mGuardarDatosDocum()
        Dim ldrDocum As DataRow
        Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_clie_raza_path")

        If txtFoto.Valor.ToString = "" And filDocumDoc.Value = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar una Foto.")
        End If

        If IsDBNull(cmbReferencia.Valor) Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar una Referencia.")
        End If

        If hdnDocumId.Text = "" Then
            ldrDocum = mdsDatos.Tables(mstrTablaDocum).NewRow
            ldrDocum.Item("prdo_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocum), "prdo_id")
        Else
            ldrDocum = mdsDatos.Tables(mstrTablaDocum).Select("prdo_id=" & hdnDocumId.Text)(0)
        End If

        With ldrDocum
            .Item("prdo_refe") = cmbReferencia.Valor
            .Item("prdo_obse") = False

            If filDocumDoc.Value <> "" Then
                .Item("prdo_path") = filDocumDoc.Value
            Else
                .Item("prdo_path") = txtFoto.Valor
            End If
            If Not .IsNull("prdo_path") Then
                .Item("prdo_path") = .Item("prdo_path").Substring(.Item("prdo_path").LastIndexOf("\") + 1)
            End If
            .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocum + "_" + Session("MilSecc") + "_" + Replace(.Item("prdo_id"), "-", "m") + "__" + .Item("prdo_path")

            .Item("prdo_prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("prdo_imag") = chkEsImg.Checked
            .Item("_prdo_imag") = IIf(chkEsImg.Checked, "S�", "No")
            .Item("prdo_audi_user") = Session("sUserId").ToString()
            .Item("prdo_baja_fecha") = DBNull.Value
            .Item("_estado") = "Activo"

            If chkEsImg.Checked Then
                'Dim str As String = filDocumDoc.Value.Replace("\", "\\")
                'Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
                'Dim foto As New System.IO.FileStream(str, IO.FileMode.OpenOrCreate, IO.FileAccess.ReadWrite)

                'FileStream foto = new FileStream(ruta, FileMode.OpenOrCreate,
            End If


            SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filDocumDoc)
        End With
        If (hdnDocumId.Text = "") Then
            mdsDatos.Tables(mstrTablaDocum).Rows.Add(ldrDocum)
        End If
    End Sub

    Private Function ArrayAImagen(ByVal arrBite As Byte()) As System.Drawing.Image
        Try
            Dim ms As New System.IO.MemoryStream(arrBite)
            Dim returnImage = System.Drawing.Image.FromStream(ms)

            Return returnImage

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Function

    Private Function ImagenAArray(ByVal imagen As System.Drawing.Image) As Byte()
        Try
            Dim ms As New System.IO.MemoryStream
            imagen.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg)
            Return ms.ToArray()

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Function

    Private Sub mGuardarDatosObse()
        Dim ldrObse As DataRow
        Dim lstrCarpeta As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_clie_raza_path")

        If txtObseRefe.Text = "" Then
            Throw New AccesoBD.clsErrNeg("Debe ingresar una Observaci�n.")
        End If

        If hdnObseId.Text = "" Then
            ldrObse = mdsDatos.Tables(mstrTablaDocum).NewRow
            ldrObse.Item("prdo_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDocum), "prdo_id")
        Else
            ldrObse = mdsDatos.Tables(mstrTablaDocum).Select("prdo_id=" & hdnObseId.Text)(0)
        End If

        With ldrObse
            .Item("prdo_refe") = txtObseRefe.Valor
            .Item("prdo_obse") = True
            If filObseDocu.Value <> "" Then
                .Item("prdo_path") = filObseDocu.Value
            Else
                .Item("prdo_path") = txtObseDocu.Valor
            End If
            If Not .IsNull("prdo_path") Then
                .Item("prdo_path") = .Item("prdo_path").Substring(.Item("prdo_path").LastIndexOf("\") + 1)
            End If
            .Item("_parampath") = lstrCarpeta + "_" + mstrTablaDocum + "_" + Session("MilSecc") + "_" + Replace(.Item("prdo_id"), "-", "m") + "__" + .Item("prdo_path")

            .Item("prdo_prdt_id") = clsSQLServer.gFormatArg(hdnId.Text, SqlDbType.Int)
            .Item("prdo_imag") = False
            '.Item("_prdo_imag") = IIf(chkEsImg.Checked, "S�", "No")
            .Item("prdo_audi_user") = Session("sUserId").ToString()
            .Item("prdo_baja_fecha") = DBNull.Value
            .Item("_estado") = "Activo"

            SRA_Neg.Utiles.mGuardarArchivosTemp(mstrConn, .Item("_parampath"), Context, filObseDocu)
        End With
        If (hdnObseId.Text = "") Then
            mdsDatos.Tables(mstrTablaDocum).Rows.Add(ldrObse)
        End If
    End Sub

#End Region

#Region "Eventos de Controles"

    Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
        mAlta()
    End Sub

    Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
        mBaja()
    End Sub

    Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
        mModi(False)
    End Sub

    Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
        mLimpiar()
    End Sub

    Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
        mCerrar()
    End Sub

    'Botones An�lisis
    Private Sub btnAltaAna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaAna.Click
        mActualizarAnalisis(True)
    End Sub

    Private Sub btnLimpAna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpAna.Click
        mLimpiarAnalisis()
    End Sub

    Private Sub btnBajaAna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaAna.Click
        Try
            With mdsDatos.Tables(mstrTablaAnalisis).Select("prta_id=" & hdnAnaId.Text)(0)
                .Item("prta_baja_fecha") = System.DateTime.Now.ToString
            End With
            grdAna.DataSource = mdsDatos.Tables(mstrTablaAnalisis)
            grdAna.DataBind()
            mLimpiarAnalisis()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnModiAna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiAna.Click
        mActualizarAnalisis(False)
    End Sub

    'Botones Asociacion
    Private Sub btnAltaAsociacion_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAltaAsociacion.Click
        mActualizarAsoc()
    End Sub

    Private Sub btnBajaAsociacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaAsociacion.Click
        mBajaAsoc()
    End Sub

    Private Sub btnModiAsociacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiAsociacion.Click
        mActualizarAsoc()
    End Sub

    Private Sub btnLimpAsociacion_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpAsociacion.Click
        cmbAsoc.Limpiar()
        txtNroAsoc.Text = ""
    End Sub

    'Botones Documentos
    Private Sub btnAltaDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaDocum.Click
        mActualizarDocum()
    End Sub

    Private Sub btnBajaDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaDocum.Click
        mBajaDocum()
    End Sub

    Private Sub btnModiDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiDocum.Click
        mActualizarDocum()
    End Sub

    Private Sub btnLimpDocum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpDocum.Click
        mLimpiarDocum()
    End Sub

    Private Sub lnkCabecera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkCabecera.Click
        mShowTabs(1)
    End Sub

    Private Sub lnkAso_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkAso.Click
        mShowTabs(3)
    End Sub

    Private Sub lnkDocumentos_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkDocumentos.Click
        mShowTabs(4)
    End Sub

    Private Sub lnkOtrosDatos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkOtrosDatos.Click
        mShowTabs(5)
    End Sub

    Private Sub lnkPesadasYDeps_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPesadasDeps.Click
        mShowTabs(7)
    End Sub

    Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
        'Si la p�gina de Productos es llamada desde un control de Productos, no ejecuta el limpiar.
        'If mstrProdCtrl = "" Then
        mLimpiarFiltros()
        'End If
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBuscar.Click
        grdDato.CurrentPageIndex = 0
        'mFiltrosConsulta()
        mConsultar(True)
        'mCargarFiltrosConsulta()
    End Sub

#End Region

    Private Sub mFiltrosConsulta()
        Try
            Me.Session.Add("FiltroIdCriador", usrCriaFil.Valor)
            Me.Session.Add("FiltroIdPropietario", usrPropCriaFil.Valor)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mLimpiarFiltrosConsulta()
        Try
            Me.Session.Remove("FiltroIdCriador")
            Me.Session.Remove("FiltroIdPropietario")

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub mCargarFiltrosConsulta()
        Try
            Dim FiltroIdCriador As String = Me.Session("FiltroIdCriador")
            Dim FiltroIdPropietario As String = Me.Session("FiltroIdPropietario")

            'Criador
            If Not IsNothing(FiltroIdCriador) Then
                If (FiltroIdCriador.Length > 0 And usrCriaFil.Valor.ToString.Length = 0) Then
                    usrCriaFil.Valor = FiltroIdCriador
                End If
            End If

            'Propietario
            If Not IsNothing(FiltroIdPropietario) Then
                'If (FiltroIdPropietario.Length > 0 And usrPropCriaFil.Valor.ToString.Length = 0) Then
                usrPropCriaFil.Valor = FiltroIdPropietario
                'End If
            End If


        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub


'    Public Shared Function ObtenerSql( _
'       ByVal pstrUserId As String, _
'       ByVal pintBusc As Integer, _
'       ByVal pstrCriaId As String, _
'       ByVal pintBaja As Integer, _
'       ByVal pstrCriaCtrl As String, _
'       ByVal pstrNomb As String, _
'       ByVal pstrRaza As String, _
'       ByVal pstrSraNume As String, _
'       ByVal pstrRP As String, _
'       ByVal pstrSexo As String, _
'       ByVal pstrNaciDesde As String, _
'       ByVal pstrNaciHasta As String, _
'       ByVal pstrAsoc As String, _
'       ByVal pstrAsocNume As String, _
'       ByVal pstrLaboNume As String, _
'       ByVal pstrMadre As String, _
'       ByVal pstrPadre As String, _
'       ByVal pstrRece As String, _
'       ByVal pstrObservacion As String, _
'       ByVal pstrSraNumDesde As String, _
'       ByVal pstrSraNumHasta As String, _
'       ByVal pstrApodo As String, _
'       ByVal pintInscrip As Integer, _
'       Optional ByVal pstrCuig As String = "", _
'       Optional ByVal pstrTram As String = "", _
'       Optional ByVal pstrInscDesde As String = "", _
'       Optional ByVal pstrInscHasta As String = "", _
'       Optional ByVal pstrRPDesde As String = "", _
'       Optional ByVal pstrRPHasta As String = "", _
'       Optional ByVal pstrPropClie As String = "", _
'       Optional ByVal pstrPropCria As String = "", _
'       Optional ByVal pstrEsta As String = "", _
'       Optional ByVal pstrNacio As String = "", _
'       Optional ByVal mbooInclDeshab As Boolean = False, _
'       Optional ByVal pstrRegiTipo As Integer = 0, _
'       Optional ByVal pstrOrdenarPor As Integer = 0, _
'       Optional ByVal pstrCondicionalDesde As String = "", _
'       Optional ByVal pstrCondicionalHasta As String = "", _
'       Optional ByVal pstrRpExtranjero As String = "", _
'       Optional ByVal pboolAscendente As Boolean = True, _
'       Optional ByVal pstrDadorNroSra As String = "", _
'       Optional ByVal pstrDadorNroSenasa As String = "", _
'        Optional ByVal pstrCastrado As String = "", _
'         Optional ByVal pstrDescorne As String = "" _
') As String
'
'        Dim lstrCmd As New StringBuilder
'        'GSZ 20/04/2015  se agrego Castrado  y descorne
'        'esta funci�n se llama tambi�n desde clsXMLHTTP
'        lstrCmd.Append("exec productos_busq ")
'        lstrCmd.Append(" @buscar_en=" + pintBusc.ToString)
'        lstrCmd.Append(" , @prdt_cria_id=" + clsSQLServer.gFormatArg(pstrCriaId, SqlDbType.Int))
'        If mbooInclDeshab Then
'            lstrCmd.Append(" , @incluir_bajas = 1")
'        Else
'            lstrCmd.Append(" , @incluir_bajas = " + pintBaja.ToString)
'        End If
'        pstrNomb = Replace(pstrNomb, "'", "''")
'        pstrNomb = Replace(pstrNomb, "~", "''")
'
'        lstrCmd.Append(" , @prdt_nomb=" + clsSQLServer.gFormatArg(pstrNomb, SqlDbType.VarChar))
'        lstrCmd.Append(" , @prdt_apodo=" + clsSQLServer.gFormatArg(pstrApodo, SqlDbType.VarChar))
'        lstrCmd.Append(" , @prdt_sra_nume=" + clsSQLServer.gFormatArg(pstrSraNume, SqlDbType.VarChar))
'        lstrCmd.Append(" , @prdt_rp=" + clsSQLServer.gFormatArg(pstrRP, SqlDbType.VarChar))
'        lstrCmd.Append(" , @prdt_cuig=" + clsSQLServer.gFormatArg(pstrCuig, SqlDbType.VarChar))
'        lstrCmd.Append(" , @rp_desde=" + clsSQLServer.gFormatArg(pstrRPDesde, SqlDbType.VarChar))
'        lstrCmd.Append(" , @rp_hasta=" + clsSQLServer.gFormatArg(pstrRPHasta, SqlDbType.VarChar))
'        lstrCmd.Append(" , @prop_clie_id=" + clsSQLServer.gFormatArg(pstrPropClie, SqlDbType.Int))
'        lstrCmd.Append(" , @prop_cria_id=" + clsSQLServer.gFormatArg(pstrPropCria, SqlDbType.Int))
'        lstrCmd.Append(" , @esta_id=" + clsSQLServer.gFormatArg(pstrEsta, SqlDbType.Int))
'        If pstrNacio <> "T" Then
'            lstrCmd.Append(" , @prdt_ndad=" + clsSQLServer.gFormatArg(pstrNacio, SqlDbType.VarChar))
'        End If
'        If pstrSexo = "" Then
'            lstrCmd.Append(" , @prdt_sexo=null")
'        Else
'            If pstrSexo.IndexOf(",") <> -1 Then
'                pstrSexo = pstrSexo.Substring(pstrSexo.IndexOf(",") + 1)
'            End If
'            lstrCmd.Append(" , @prdt_sexo=" + clsSQLServer.gFormatArg(pstrSexo, SqlDbType.Int))
'        End If
'
'        lstrCmd.Append(" , @prdt_naci_fecha_desde=" + clsSQLServer.gFormatArg(pstrNaciDesde, SqlDbType.SmallDateTime))
'        lstrCmd.Append(" , @prdt_naci_fecha_hasta=" + clsSQLServer.gFormatArg(pstrNaciHasta, SqlDbType.SmallDateTime))
'        lstrCmd.Append(" , @prdt_insc_fecha_desde=" + clsSQLServer.gFormatArg(pstrInscDesde, SqlDbType.SmallDateTime))
'        lstrCmd.Append(" , @prdt_insc_fecha_hasta=" + clsSQLServer.gFormatArg(pstrInscHasta, SqlDbType.SmallDateTime))
'        lstrCmd.Append(" , @prdt_asoc_id=" + clsSQLServer.gFormatArg(pstrAsoc, SqlDbType.Int))
'        lstrCmd.Append(" , @prdt_asoc_nume=" + clsSQLServer.gFormatArg(pstrAsocNume, SqlDbType.VarChar))
'        lstrCmd.Append(" , @prta_nume=" + clsSQLServer.gFormatArg(pstrLaboNume, SqlDbType.Int))
'        lstrCmd.Append(" , @madre_prdt_id=" + clsSQLServer.gFormatArg(pstrMadre, SqlDbType.Int))
'        lstrCmd.Append(" , @padre_prdt_id=" + clsSQLServer.gFormatArg(pstrPadre, SqlDbType.Int))
'        lstrCmd.Append(" , @rece_prdt_id=" + clsSQLServer.gFormatArg(pstrRece, SqlDbType.Int))
'        lstrCmd.Append(" , @prdt_raza_id=" + clsSQLServer.gFormatArg(pstrRaza, SqlDbType.Int))
'        lstrCmd.Append(" , @prdo_refe=" + clsSQLServer.gFormatArg(pstrObservacion, SqlDbType.VarChar))
'        lstrCmd.Append(" , @inscrip = " + pintInscrip.ToString)
'        lstrCmd.Append(" , @tram_nume=" + clsSQLServer.gFormatArg(pstrTram, SqlDbType.Int))
'        lstrCmd.Append(" , @prdt_sra_nume_desde=" + clsSQLServer.gFormatArg(pstrSraNumDesde, SqlDbType.Int))
'        lstrCmd.Append(" , @prdt_sra_nume_hasta=" + clsSQLServer.gFormatArg(pstrSraNumHasta, SqlDbType.Int))
'        lstrCmd.Append(" , @prdt_regt_id=" + clsSQLServer.gFormatArg(pstrRegiTipo, SqlDbType.Int))
'        lstrCmd.Append(" , @ordenar_por=" + clsSQLServer.gFormatArg(pstrOrdenarPor, SqlDbType.Int)) '0: HBA / 1: RP / 2: Criador.
'        lstrCmd.Append(" , @prdt_condicional_desde=" + clsSQLServer.gFormatArg(pstrCondicionalDesde, SqlDbType.Int)) 'Condicional Desde.
'        lstrCmd.Append(" , @prdt_condicional_hasta=" + clsSQLServer.gFormatArg(pstrCondicionalHasta, SqlDbType.Int)) 'Condicional Hasta.
'        lstrCmd.Append(" , @prdt_rp_extr=" + clsSQLServer.gFormatArg(pstrRpExtranjero, SqlDbType.VarChar)) 'RP Extranjero.
'        lstrCmd.Append(" , @Ascendente=" + clsSQLServer.gFormatArg(pboolAscendente, SqlDbType.Bit)) 'Ordena por: Ascendente / Descendente.
'        lstrCmd.Append(" , @prdt_dona_nume=" + clsSQLServer.gFormatArg(pstrDadorNroSra, SqlDbType.Int)) 'Nro.Dador SRA
'        lstrCmd.Append(" , @prdt_dona_sena_nume=" + clsSQLServer.gFormatArg(pstrDadorNroSenasa, SqlDbType.VarChar)) 'Nro.Dador Senansa
'
'
'        If pstrCastrado.ToLower() = "(todos)" Then
'            lstrCmd.Append(" , @prdt_castrado=null")
'        Else
'            If pstrCastrado.IndexOf(",") <> -1 Then
'                pstrCastrado = pstrCastrado.Substring(pstrCastrado.IndexOf(",") + 1)
'            End If
'            lstrCmd.Append(" , @prdt_castrado=" + clsSQLServer.gFormatArg(pstrCastrado, SqlDbType.Int))
'        End If
'
'        If pstrDescorne.ToLower() = "(todos)" Then
'            lstrCmd.Append(" , @prdt_descorne=null")
'        Else
'            If pstrDescorne.IndexOf(",") <> -1 Then
'                pstrDescorne = pstrDescorne.Substring(pstrDescorne.IndexOf(",") + 1)
'            End If
'            lstrCmd.Append(" , @prdt_descorne=" + clsSQLServer.gFormatArg(pstrDescorne, SqlDbType.Int))
'        End If
'
'
'
'
'        Return (lstrCmd.ToString)
'    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub btnCertInsc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCertInsc.Click
        Try
            If txtSraNume.Valor.ToString = "" Then
                Dim lstrMens As String = ""

                If cmbRaza.Valor.ToString <> "" Then
                    lstrMens = Trim(clsSQLServer.gObtenerValorCampo(mstrConn, "razas_especie", cmbRaza.Valor.ToString, "espe_nomb_nume").ToString)
                End If

                Throw New AccesoBD.clsErrNeg("El producto no posee " & lstrMens & " asignado.")
            End If
            mImprimirCertificado()
        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub

    Private Sub btnAltaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAltaObse.Click
        mActualizarObse()
        If Not mboolSoloConsulta Then
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
            lobjGenerica.Modi()
        End If

    End Sub

    Private Sub btnBajaObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBajaObse.Click
        mBajaObse()
        If Not mboolSoloConsulta Then
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
            lobjGenerica.Modi()
        End If

    End Sub

    Private Sub btnModiObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModiObse.Click
        mActualizarObse()
        If Not mboolSoloConsulta Then
            Dim lobjGenerica As New SRA_Neg.GenericaRel(mstrConn, Session("sUserId").ToString(), mstrTabla, mdsDatos, Context)
            lobjGenerica.Modi()
        End If

    End Sub

    Private Sub btnLimpObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLimpObse.Click
        mLimpiarObse()
    End Sub

    Private Sub lnkObse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkObse.Click
        mShowTabs(6)
    End Sub

    Private Sub btnProce_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProce.Click
        mProcesar()
    End Sub

    Private Sub btnPostback_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPostback.Click

    End Sub

    ' Evento del boton revertir que borra la baja del producto
    ' Dario 2011-11-06
    Private Sub btnRevertirBaja_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRevertirBaja.Click
        ' solo actua si la leyenda de baja de producto esta con contenido
        ' se coloca esta validacion por las dudas
        'Try
        '    If (lblBaja.Text.Length > 0) Then
        '        ' hdnId.Text
        '        ' Session("sUserId").ToString()
        '        Dim objProductosBusiness As New Business.Productos.ProductosBusiness
        '        'ejecuto meto de reversion de la baja 
        '        Dim resu As Boolean = objProductosBusiness.RevertirBajaProducto(hdnId.Text, Session("sUserId").ToString())
        '        If (resu = False) Then
        '            Throw New AccesoBD.clsErrNeg("No se pudo revertir la baja del producto")
        '        End If

        '        mCargarDatos(hdnId.Text)
        '    End If
        'Catch ex As Exception
        '    clsError.gManejarError(Me, ex)
        'Finally
        '    Me.panBotones.Style.Add("display", "inline")
        '    Me.divproce.Style.Add("display", "none")
        'End Try
    End Sub

    ' Evento que realiza na nacionalizacion de productos E que no estan en ningun tramite
    Private Sub btnInscribirExtr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInscribirExtr.Click
        Try
            Dim objProductosBusiness As New Business.Productos.ProductosBusiness
            'ejecuto meto de reversion de la baja 
            Dim resu As Int32 = objProductosBusiness.NacionalizarExtranjero(hdnId.Text, Session("sUserId").ToString())
            If (resu = -1) Then
                Throw New AccesoBD.clsErrNeg("No se pudo Nacionalizar el producto")
            Else
                hdnId.Text = resu
            End If

            mCargarDatos(hdnId.Text)

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        Finally
            Me.panBotones.Style.Add("display", "inline")
            Me.divproce.Style.Add("display", "none")
        End Try
    End Sub

    Private Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Unload

    End Sub

    Private Function mValiInscribeSRA(ByVal mstrRazaId As String) As String

        Dim FacturacionBusiness As New Business.Facturacion.FacturacionBusiness
        Dim pstrArgs As String
        Dim boolInscribeSRA As Boolean
        Dim strInscribeSRA As String

        pstrArgs = "@raza_id=" + mstrRazaId

        boolInscribeSRA = FacturacionBusiness.GetRazaInscribeEnSRA(pstrArgs)
        If boolInscribeSRA Then
            strInscribeSRA = "1"
        Else
            strInscribeSRA = "0"
        End If
        Return strInscribeSRA
    End Function

    Public Sub chkCastrado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkCastrado.CheckedChanged
        If (chkCastrado.Checked) Then
            If chkCastrado.Enabled Then
                If chkCastrado.Enabled Then
                        txtCastradoFecha.Enabled = True
                    End If
            Else


            End If
        Else
            txtCastradoFecha.Enabled = False
            txtCastradoFecha.Text = ""
        End If
    End Sub
End Class

End Namespace
