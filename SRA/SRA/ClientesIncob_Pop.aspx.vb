Namespace SRA

Partial Class ClientesIncob_Pop
    Inherits FormGenerico

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

   End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

#Region "Definición de Variables"
   Private mstrConn As String
   Private mdsDatos As DataSet

   Private Enum Columnas As Integer
      Chk = 0
      ClieId = 1
      SociId = 2
      ClieApel = 3
      SociNume = 4
      Cuotas = 5
      VigenciaSoc = 6
      DeudaSocial = 7
      VigenciaNoSoc = 8
      DeudaNoSocial = 9
      Soci_Lsin_Id = 10
   End Enum
#End Region

#Region "Operaciones sobre la Pagina"
   Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
      Try
         mstrConn = clsWeb.gVerificarConexion(Me)

         If Not Page.IsPostBack Then
            Session(mSess(0)) = Nothing
            hdnTipo.Text = Request("tipo")

            Select Case Request("tipo")
               Case "C"
                  grdConsulta.Columns(Columnas.Cuotas).Visible = False
                  grdConsulta.Columns(Columnas.VigenciaSoc).Visible = False
                  grdConsulta.Columns(Columnas.DeudaSocial).Visible = False
               Case "S"
                  grdConsulta.Columns(Columnas.VigenciaNoSoc).Visible = False
                  grdConsulta.Columns(Columnas.DeudaNoSocial).Visible = False
            End Select

            mConsultar()

            mChequearTodos()

            If txtTotal.Text = "" Then
                mCalcularTotales()
            End If
         Else
            mdsDatos = Session(mSess(0))
            If hdnTotal.Text <> "" Then
               txtTotal.Text = hdnTotal.Text
            End If
         End If

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Private Function mSess(ByVal pstrTabla As String) As String
      If hdnSess.Text = "" Then
         hdnSess.Text = pstrTabla & Now.Millisecond.ToString
      End If
      Return (hdnSess.Text)
   End Function

#End Region

#Region "Operaciones sobre el DataGrid"
   Private Sub mGuardarTodosIds()
      Dim lstrId As New System.Text.StringBuilder
      mLimpiarSession()

      For i As Integer = 0 To grdConsulta.PageCount - 1
         lstrId.Length = 0
         grdConsulta.CurrentPageIndex = i
         grdConsulta.DataBind()

         Session("mulPag" & i.ToString) = mGuardarDatos(True)
      Next

      grdConsulta.CurrentPageIndex = 0
      grdConsulta.DataBind()
   End Sub

   Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
      Try
         Dim lstrId As String

         lstrId = mGuardarDatos()

         Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) = lstrId.ToString

         grdConsulta.CurrentPageIndex = E.NewPageIndex

         mConsultar()

         mChequearGrilla()

      Catch ex As Exception
         clsError.gManejarError(Me, ex)
      End Try
   End Sub

   Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
      Dim lsbMsg As New StringBuilder
      lsbMsg.Append("<SCRIPT language='javascript'>")
      lsbMsg.Append(String.Format("window.opener.opener.document.all['hdnDatosPop'].value='{0}';", E.Item.Cells(Columnas.ClieId).Text))
      lsbMsg.Append("window.opener.opener.__doPostBack('hdnDatosPop','');")
      lsbMsg.Append("window.opener.close();")
      lsbMsg.Append("window.close();")
      lsbMsg.Append("</SCRIPT>")
      Response.Write(lsbMsg.ToString)
   End Sub

   Public Sub mConsultar()
      Dim lstrCmd As String = ""
      Dim ds As DataSet

      lstrCmd = "exec incobrables_busq"
      lstrCmd = lstrCmd & " @vigencia=" & clsSQLServer.gFormatArg(Request("vigencia"), SqlDbType.Int)
      lstrCmd = lstrCmd & ",@deuda=" & clsSQLServer.gFormatArg(Request("deuda"), SqlDbType.Int)
      lstrCmd = lstrCmd & ",@incl= " & clsSQLServer.gFormatArg(Request("incl"), SqlDbType.Int)
      lstrCmd = lstrCmd & ",@orden=" & clsSQLServer.gFormatArg(Request("orden"), SqlDbType.VarChar)
      lstrCmd = lstrCmd & ",@etapa=" & clsSQLServer.gFormatArg(Request("etapa"), SqlDbType.VarChar)
      lstrCmd = lstrCmd & ",@tipo=" & clsSQLServer.gFormatArg(Request("tipo"), SqlDbType.VarChar)
      lstrCmd = lstrCmd & ",@acti=" & clsSQLServer.gFormatArg(Request("acti"), SqlDbType.Int)
      lstrCmd = lstrCmd & ",@comi_anio=" & clsSQLServer.gFormatArg(Request("comi_anio"), SqlDbType.Int)
      lstrCmd = lstrCmd & ",@soci_lsin_id=" & clsSQLServer.gFormatArg(Request("soci_lsin_id"), SqlDbType.Int)
      If Request("peri_desde") <> "" Then
         lstrCmd = lstrCmd & ",@peri_desde=" & clsSQLServer.gFormatArg(Request("peri_desde"), SqlDbType.SmallDateTime)
      End If
      If Request("peri_hasta") <> "" Then
         lstrCmd = lstrCmd & ",@peri_hasta=" & clsSQLServer.gFormatArg(Request("peri_hasta"), SqlDbType.SmallDateTime)
      End If

      If Request("tipo") <> "C" Then
         lstrCmd = lstrCmd & ",@peti_id=" & clsSQLServer.gFormatArg(Request("peti_id"), SqlDbType.Int)
         lstrCmd = lstrCmd & ",@anio=" & clsSQLServer.gFormatArg(Request("anio"), SqlDbType.Int)
         lstrCmd = lstrCmd & ",@peri=" & clsSQLServer.gFormatArg(Request("peri"), SqlDbType.Int)
         lstrCmd = lstrCmd & ",@cate=" & clsSQLServer.gFormatArg(Request("cate"), SqlDbType.Int)
         lstrCmd = lstrCmd & ",@esta=" & clsSQLServer.gFormatArg(Request("esta"), SqlDbType.Int)
      End If

      ds = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

      Session(mSess(0)) = ds

      grdConsulta.DataSource = ds
      grdConsulta.DataBind()
      ds.Dispose()

      Session("mulPaginas") = grdConsulta.PageCount
   End Sub
#End Region

   Private Sub btnAceptar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
      Dim lstrId As New StringBuilder
      Dim lstrClieAnt, lstrBuqeAnt As String
      Try
         lstrId.Append(mGuardarDatos)

         For i As Integer = 0 To Session("mulPaginas")
            If Not Session("mulPag" & i.ToString) Is Nothing Then
               Dim lstrIdsSess As String = Session("mulPag" & i.ToString)
               If i <> grdConsulta.CurrentPageIndex And lstrIdsSess <> "" Then
                  If lstrId.Length > 0 Then lstrId.Append(Chr(5))
                  lstrId.Append(lstrIdsSess)
               End If
               Session("mulPag" & i.ToString) = Nothing
            End If
         Next

         grdConsulta.DataSource = Nothing
         grdConsulta.DataBind()

         Dim lsbMsg As New StringBuilder
         lsbMsg.Append("<SCRIPT language='javascript'>")
         lsbMsg.Append(String.Format("window.opener.opener.document.all['hdnDatosPop'].value='{0}';", lstrId.ToString.Replace("'", "´")))
         lsbMsg.Append("window.opener.opener.__doPostBack('hdnDatosPop','');")
         lsbMsg.Append("window.opener.close();")
         lsbMsg.Append("window.close();")
         lsbMsg.Append("</SCRIPT>")
         Response.Write(lsbMsg.ToString)

      Catch ex As Exception
         clsError.gManejarError(Me.Page, ex)
      End Try
   End Sub

   Private Sub btnCerrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCerrar.Click
      Response.Write("<Script>window.close();</script>")
   End Sub

   Private Function mGuardarDatos(Optional ByVal pbooTodos As Boolean = False) As String
      Dim lstrId As New StringBuilder

      For Each oDataItem As DataGridItem In grdConsulta.Items
         If DirectCast(oDataItem.FindControl("chkSel"), CheckBox).Checked Or pbooTodos Then
            If lstrId.Length > 0 Then lstrId.Append(Chr(5))
            lstrId.Append(oDataItem.Cells(Columnas.ClieId).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.SociId).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.ClieApel).Text)
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.SociNume).Text)
            lstrId.Append(Chr(6))
            lstrId.Append((CDbl(oDataItem.Cells(Columnas.DeudaSocial).Text) + CDbl(oDataItem.Cells(Columnas.DeudaNoSocial).Text)).ToString("#######0.00"))
            lstrId.Append(Chr(6))
            lstrId.Append(oDataItem.Cells(Columnas.Soci_Lsin_Id).Text)
         End If
      Next

      Return (lstrId.ToString)
   End Function

   Private Sub mChequearGrilla()
      Dim lstrIdSess As String
      If Not (Session("mulPag" & grdConsulta.CurrentPageIndex.ToString) Is Nothing) Then
         lstrIdSess = Session("mulPag" & grdConsulta.CurrentPageIndex.ToString)
      End If

      For Each oDataItem As DataGridItem In grdConsulta.Items
         CType(oDataItem.FindControl("chkSel"), System.Web.UI.WebControls.CheckBox).Checked = (Chr(5) & lstrIdSess).IndexOf(Chr(5) & oDataItem.Cells(Columnas.ClieId).Text & Chr(6)) <> -1
      Next
   End Sub

   Private Sub mCalcularTotales()
      Dim dTotal As Decimal = 0
      Dim lDr As DataRow

      mdsDatos = Session(mSess(0))

      For Each lDr In mdsDatos.Tables(0).Rows
         If Request("tipo") = "C" Then
            If Not lDr.IsNull("deuda_no_social") Then
                dTotal += lDr.Item("deuda_no_social")
            End If
         Else
            If Not lDr.IsNull("deuda_social") Then
                dTotal += lDr.Item("deuda_social")
            End If
         End If
      Next

      txtTotal.Valor = dTotal
   End Sub

   Private Sub btnTodos_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTodos.Click
      mConsultar()

      mChequearTodos()
   End Sub

   Private Sub btnNinguno_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNinguno.Click
      mLimpiarSession()
      mChequearGrilla()
      txtTotal.Text = "0.00"
   End Sub

   Private Sub mLimpiarSession()
      Dim k As Integer

      While Not Session("mulPag" & k.ToString) Is Nothing
         Session("mulPag" & k.ToString) = Nothing
         k += 1
      End While
   End Sub

   Private Sub mChequearTodos()
      mGuardarTodosIds()
      mChequearGrilla()
      mCalcularTotales()
   End Sub
End Class

End Namespace
