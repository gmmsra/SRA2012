function gValDate(txtstr){
	TF = false
	if (txtstr.value != ''){		     
		if (isdate(txtstr.value)=='N') {
		TF = false;
		}
		else {
		TF = true;
		};		               
		if (TF == false){
		alert ('' + txtstr.value  + ' formato de fecha inv' + String.fromCharCode(225) + 'lido.\n\nPor favor, ingrese el formato "dd/mm/aaaa" '); 
		txtstr.value = ''
		txtstr.focus();
		txtstr.select();
		return true;
		}
		else {
		txtstr.value=isdate(txtstr.value);
		};
	}
	return false;
}

function gValDateTransferencia(txtstr, method) {
    TF = false
    if (txtstr.value != '') {
        if (isdate(txtstr.value) == 'N') {
            TF = false;
        }
        else {
            TF = true;
        };
        if (TF == false) {
            alert('' + txtstr.value + ' formato de fecha inv' + String.fromCharCode(225) + 'lido.\n\nPor favor, ingrese el formato "dd/mm/aaaa" ');
            txtstr.value = ''
            txtstr.focus();
            txtstr.select();
            return true;
        }
        else {
            txtstr.value = isdate(txtstr.value);
            if (method != "False") {
                method();
            }
        };
    }
    return false;
}

function isdate(astr){
var mm,dd,aa 
var dateOK=true
var pos = 0
var pos1=1
if (astr.charAt(pos1) == "/" || astr.charAt(pos1) == "-"){
	dd = astr.charAt(pos)
	pos = pos1 + 1
	pos1 = pos + 1
} else {
	pos1+=1 
	if (astr.charAt(pos1) == "/" || astr.charAt(pos1) == "-"){
		dd=astr.substring(pos, pos1)
		pos = pos1 + 1
		pos1 = pos + 1
	} else {
		dateOK = false
	}
}
if (astr.charAt(pos1) == "/" || astr.charAt(pos1) == "-"){
	mm=astr.charAt(pos)
	pos= pos1 + 1
} else {
	pos1 += 1
	if (astr.charAt(pos1) == "/" || astr.charAt(pos1) == "-"){
		mm=astr.substring(pos, pos1)
		pos = pos1 + 1 
	} else {
		dateOK = false;
	}
}
aa=astr.substring( pos, astr.length)
if (dateOK){dateOK=isnum(dd)}
if (dateOK){dateOK=isnum(mm)}
if (dateOK){dateOK=isnum(aa)}
if (dateOK){
	if ((aa > 100 && aa < 1910) || (aa > 2078)){
		dateOK = false
	}
	if ((mm < 1) || mm > 12){
		dateOK = false;
	}      
	if (dd < 1 || dd > 31){
		dateOK = false
	}
	if ((mm  == 4 || mm  == 6 || mm  == 9 || mm  == 11 ) && (dd > 30)){
		dateOK = false
	}
	if (mm == 2 && dd > 29){
		dateOK = false
	}
	if (mm == 2 && dd == 29 && (aa % 4 != 0)){
		dateOK = false
	}
};
if (dateOK) 
{
	var fmtFecha, fmtDia, fmtMes;     
	fmtDia = '00' + dd;
	fmtMes = '00' + mm;
	fmtFecha = fmtDia.substring( fmtDia.length - 2 , fmtDia.length );
	fmtFecha = fmtFecha + '/' + fmtMes.substring( fmtMes.length - 2 , fmtMes.length );
	fmtFecha = fmtFecha + '/' + aa;
	return fmtFecha;
}
else
{
	return 'N';
};
}
function isnum(numstr)
{
	var isit=false
	var nstr="0123456789" 
	var bstr="";
	for (i = 0; i < numstr.length; i++)
		{ cc=numstr.substring(i,i+1);
		for (j = 0 ; j < nstr.length ; j++) {
				dd=nstr.substring(j,j+1)
				isit=false
				if (cc==dd) {
					isit=true
					bstr += cc
					break
					}
			} 
		if (isit == false)
			break;
		else continue  
		}
	return isit
}

function gMascaraFecha(pobjTxt)
{
	var lstrCade = pobjTxt.value;
	var lstrLong = lstrCade.length;

	if (lstrLong > 10)
		return;

	if ((lstrLong == 2||lstrLong == 5) && event.keyCode!=47)
		pobjTxt.value = pobjTxt.value + "/";
}

function ActivarFecha(pstrCtrl, pbooActivo)
{
	if(pbooActivo)
	{
		document.all[pstrCtrl].disabled=false;
		document.all[pstrCtrl].parentElement.children[1].href = document.all[pstrCtrl].parentElement.children[1].href.replace("false","true");
		document.all[pstrCtrl].parentElement.children[1].children[0].src=document.all[pstrCtrl].parentElement.children[1].children[0].src.replace("icodate_des.gif","icodate.gif");
		document.all[pstrCtrl].className = "cuadrotexto";
	}
	else
	{
		document.all[pstrCtrl].disabled=true;
		document.all[pstrCtrl].parentElement.children[1].href = document.all[pstrCtrl].parentElement.children[1].href.replace("true","false");
		document.all[pstrCtrl].parentElement.children[1].children[0].src=document.all[pstrCtrl].parentElement.children[1].children[0].src.replace("icodate.gif","icodate_des.gif");
		document.all[pstrCtrl].className = "cuadrotextodeshab";
	}
}

function gFechaDate(astr){
var mm,dd,aa 
var dateOK=true
var pos = 0
var pos1=1
var oFecha

if (astr.charAt(pos1) == "/" || astr.charAt(pos1) == "-"){
	dd = astr.charAt(pos)
	pos = pos1 + 1
	pos1 = pos + 1
} else {
	pos1+=1 
	if (astr.charAt(pos1) == "/" || astr.charAt(pos1) == "-"){
		dd=astr.substring(pos, pos1)
		pos = pos1 + 1
		pos1 = pos + 1
	} else {
		dateOK = false
	}
}
if (astr.charAt(pos1) == "/" || astr.charAt(pos1) == "-"){
	mm=astr.charAt(pos)
	pos= pos1 + 1
} else {
	pos1 += 1
	if (astr.charAt(pos1) == "/" || astr.charAt(pos1) == "-"){
		mm=astr.substring(pos, pos1)
		pos = pos1 + 1 
	} else {
		dateOK = false;
	}
}
aa=astr.substring( pos, astr.length)
if (dateOK){dateOK=isnum(dd)}
if (dateOK){dateOK=isnum(mm)}
if (dateOK){dateOK=isnum(aa)}
if (dateOK){
	if ((aa > 100 && aa < 1910) || (aa > 2078)){
		dateOK = false
	}
	if ((mm < 1) || mm > 12){
		dateOK = false;
	}      
	if (dd < 1 || dd > 31){
		dateOK = false
	}
	if ((mm  == 4 || mm  == 6 || mm  == 9 || mm  == 11 ) && (dd > 30)){
		dateOK = false
	}
	if (mm == 2 && dd > 29){
		dateOK = false
	}
	if (mm == 2 && dd == 29 && (aa % 4 != 0)){
		dateOK = false
	}
};
if (dateOK) 
{
	aa=parseInt(aa);
	if (aa < 20)
		aa=aa + 2000;

	oFecha = new Date();
	oFecha.setFullYear(aa,mm,dd);
	return(oFecha);
}
else
{
	return null;
};
}
function gArg(pstrValor)
{
	var lstrFecha = '';
	lstrFecha = "'" + pstrValor.substring(3,5) + "/" + pstrValor.substring(0,2) + "/" + pstrValor.substring(6,10) + "'";
	return(lstrFecha);
}
