function CodiClieDeriv_change(strId, strAutoPB, strArgs, strCampoVal)
{
	var oCoti = document.all(strId + ":txtCodi");
	var sFiltro = oCoti.value;

	if (!booIsNumber(sFiltro)||(sFiltro==""))
	{
		document.all(strId + ":txtApel").value = "";
		document.all(strId + ":txtId").value = "";
		
		if (document.all[strId + ":txtDesc"]!= null)
			document.all(strId + ":txtDesc").value = "";
	}
	else	
	{
		sFiltro += ";" + strArgs;
		var sRet=EjecutarMetodoXML("Utiles.BuscarClieDeriv", sFiltro);
		if(sRet=="")
		{
			alert(strCampoVal + " inexistente")
			document.all(strId + ":txtApel").value = "";
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = "";
			document.all(strId + ":txtId").value = "";
			oCoti.focus();
		}
		else
		{
			var vsRet=sRet.split("|");
			document.all(strId + ":txtApel").value = vsRet[2];
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = vsRet[3];
			document.all(strId + ":txtId").value = vsRet[0];
			
			if(strAutoPB=="1")
				__doPostBack(strId + ":txtId",'');
		}
	}
	
	try{eval(strId + "_onchange();")}
	catch(e){;}
}

function imgBuscClieDeriv_click(strId,pstrParams,pstrSession,pstrEstilo,pstrPagina)
{
	var sFiltro = "";
	sFiltro += "ctrlId=" + strId + "&" + pstrParams;

	sFiltro += agregarParam(strId, "txtApel", "apel");
	sFiltro += agregarParam(strId, "txtNomb", "nomb");
	sFiltro += agregarParam(strId, "chkFanta", "Fanta");
	sFiltro += agregarParam(strId, "txtSociNume", "SociNume");
	sFiltro += agregarParam(strId, "cmbCriaNumeRaza", "Raza");
	sFiltro += agregarParam(strId, "txtCriaNume", "CriaNume");
	sFiltro += agregarParam(strId, "txtCUIT", "Cuit");
	sFiltro += agregarParam(strId, "cmbDocuNumeTido", "DocuTipo");
	sFiltro += agregarParam(strId, "txtDocuNume", "DocuNume");
	sFiltro += agregarParam(strId, "cmbEntidad", "Enti");
	sFiltro += agregarParam(strId, "cmbMedioPago", "MedioPago");
	sFiltro += agregarParam(strId, "chkAgrupa", "Agru");
	sFiltro += agregarParam(strId, "cmbEmpresa", "Empr");
	sFiltro += agregarParam(strId, "txtClave", "ClaveUnica");
   	sFiltro += agregarParam(strId, "txtCodi", "ClieNume");
	sFiltro += agregarParam(strId, "txtNoCriaNume", "NoCriaNume");
	sFiltro += agregarParam(strId, "txtLegaNume", "LegaNume");

	Ventana = window.open (pstrPagina + "?" + sFiltro, pstrSession, pstrEstilo);

	window.attachEvent('onfocus',window_onfocus);
	window.document.body.attachEvent('onfocus',window_onfocus);
}