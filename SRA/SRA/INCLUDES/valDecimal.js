function gValDecimal(pobjField,pintEnte, pintValorMax, pAutoPostBack)
{
  if (pobjField.value == ''){	
	return true;
  }
  if (booIsDecimal(pobjField.value,pintEnte))
  {
	if (pAutoPostBack == 1) {
		__doPostBack(pobjField.id,'');
	}
		return false;
  }
  else
  {
    alert('N' + String.fromCharCode(250) + 'mero inv' + String.fromCharCode(225) + 'lido '+ pobjField.value+'\nIngrese un n' + String.fromCharCode(250) + 'mero decimal (de hasta '+pintEnte+' posiciones enteras)');
    pobjField.value = ''
    pobjField.focus(); 
    pobjField.select();
    return true
  }
}
function booIsDecimal(astrValue,astrEnte)
{
	if (astrValue == '.')
	{
		return false
	}

	if (astrValue.substring( 0, 1 )=="-")
	{
		astrValue = astrValue.substring(1);
	}

	var lintComa = 0;
	var lintPunt = 0;
	var lintPuntPosi = 0;
	for ( var i = 0 ; i < astrValue.length ; i++ )
	{
		var ch = astrValue.substring( i, i + 1 )
		if ( (ch < "0" || "9" < ch) && ch != '.' && ch != ',')  {
			return false
		}
		if (ch == '.')  {
			lintPunt = lintPunt + 1;
			lintPuntPosi = i;
		}
		if (ch == ',')  {
			lintComa = lintComa + 1;
			lintPuntPosi = i;
		}
	}	
	if (lintComa == 0 && lintPunt == 0)
	{
		lintPuntPosi = astrValue.length;
	}
	if (lintPunt != 0 && lintComa != 0)  {
		return false
	}
	if (lintPunt > 1 || lintComa > 1)  {
		return false
	}
	if (lintPuntPosi > astrEnte)
	{
		return false
	}
	return true
}
function gRedondear(number,X) 
{ 
	// rounds number to X decimal places, defaults to 2
	X = (!X ? 2 : X); 
	return Math.round(number*Math.pow(10,X))/Math.pow(10,X); 
} 
function gConvertDecimal(number) 
{ 
	if (number != "")
	{
		return parseFloat(number)
	}
	else
	{
		return 0
	}
}