function gValPorcen(pobjField)
{
  if (pobjField.value == ''){	
	return true;
  }
  if (!booIsNumber(pobjField.value))
  {
	  return false;
  }
  else
  {
    if (pobjField.value < 0 || pobjField.value > 100)
	{
		alert('Porcentaje inv' + String.fromCharCode(225) + 'lido '+ pobjField.value + '.\nDebe ingresar un n' + String.fromCharCode(250) + 'mero entre 0 y 100.');
		pobjField.value = ''
		pobjField.focus(); 
		pobjField.select();
		return true;
	}
	else
	{
		return true;
	}
  }
}
function booIsNumber(astrValue)
{
	for ( var i = 0 ; i < astrValue.length ; i++ )
	{
		var ch = astrValue.substring( i, i + 1 )
		if ( ( ch < "0" || "9" < ch ))
		{
			return false
		}
	}	
	return true
}