﻿$(function () {
    if (document.all("usrProd_aux_lblCriaOrProp") != null) {
        document.all("usrProd_aux_lblCriaOrProp").innerText = "Raza:"
    }
});

function buscarVendedores() {
    if ($('#hdnOperacion').val() == 'A') {
        if ($("#txtFechaTransferencia_aux").val() != $("#hdnFechaTransferencia").val()) {
            $("#hdnFechaTransferencia").val($("#txtFechaTransferencia_aux").val());
            cambiarEstadoCtrolProductoATransferir(true);
        }
    }
}

function myFunction() {
    if ($('#hdnOperacion').val() == 'A') {
        if ($("#txtFechaTransferencia_aux").val().length == 0) {
            cambiarEstadoCtrolProductoATransferir(false);
        }
        else {
            if (!gValDateLocal($("#txtFechaTransferencia_aux"))) {
                if ($("#txtFechaTransferencia_aux").val() != $("#hdnFechaTransferencia").val()) {
                    $("#hdnFechaTransferencia").val($("#txtFechaTransferencia_aux").val());
                    cambiarEstadoCtrolProductoATransferir(true);
                }
            }
        }
    }
}

function gValDateLocal(txtstr) {
    TF = false
    if (txtstr[0].value != '') {
        if (isdate(txtstr[0].value) == 'N') {
            TF = false;
        }
        else {
            TF = true;
        };
        if (TF == false) {
            $("#hdnFechaTransferencia").val('');
            cambiarEstadoCtrolProductoATransferir(false);
            return true;
        }
        else {
            txtstr[0].value = isdate(txtstr[0].value);
        };
    }
    return false;
}

function cambiarEstadoCtrolProductoATransferir(estado) {

    document.all('usrProd_aux:usrCriadorFil:cmbRazaCria').value = '';
    document.all('usrProd_aux:usrCriadorFil:cmbRazaCria').onchange();
    limpiarCtrolPropductoATransferir();
    setearCmbRazaCriaVendedorComprador();

    var rowsVendedores = document.getElementById("gridVendedores").rows.length - 1;
    var rowsCompradores = document.getElementById("gridCompradores").rows.length - 1;

    if (rowsVendedores > 1 || rowsCompradores > 1) {
        $('#btnLimpiarGrids').click();
    }
    else {
        setearCmbRazaCriaVendedorComprador();
    }

    if (estado == false) {
        deshabilitarCtrolProductoATransferir();
    }
    else {
        habilitarCtrolProductoATransferir();
    }
}

function limpiarCtrolPropductoATransferir() {
    document.all('usrProd_aux:cmbProdSexo').value = '';
    document.all('usrProd_aux:cmbProdSexo').onchange();
    document.all('usrProd_aux:txtRP').value = '';
    document.all('usrProd_aux:txtSraNume').value = '';
    document.all('usrProd_aux:cmbProdAsoc').value = '';
    document.all('usrProd_aux:cmbProdAsoc').onchange();
    document.all('usrProd_aux:txtCodi').value = '';
    document.all('usrProd_aux:txtProdNomb').value = '';
    $('#hdnProductoId').val('');
}

function deshabilitarCtrolProductoATransferir() {
    document.all('txtusrProd_aux:usrCriadorFil:cmbRazaCria').disabled = true;
    document.all('usrProd_aux:usrCriadorFil:cmbRazaCria').disabled = true;

    document.all('usrProd_aux:cmbProdSexo').disabled = true;
    document.all('usrProd_aux:txtRP').disabled = true;
    document.all('usrProd_aux:txtSraNume').disabled = true;

    document.all('txtusrProd_aux:cmbProdAsoc').disabled = true;
    document.all('usrProd_aux:cmbProdAsoc').disabled = true;

    document.all('usrProd_aux:txtCodi').disabled = true;
    document.all('usrProd_aux:txtProdNomb').disabled = true;

    document.all('usrProd_aux_imgBusc').disabled = true;
    document.all('usrProd_aux_imgLimp').disabled = true;
}

function habilitarCtrolProductoATransferir() {
    document.all('txtusrProd_aux:usrCriadorFil:cmbRazaCria').disabled = false;
    document.all('usrProd_aux:usrCriadorFil:cmbRazaCria').disabled = false;

    document.all('usrProd_aux:cmbProdSexo').disabled = false;
    document.all('usrProd_aux:txtRP').disabled = false;
    document.all('usrProd_aux:txtSraNume').disabled = false;

    document.all('txtusrProd_aux:cmbProdAsoc').disabled = false;
    document.all('usrProd_aux:cmbProdAsoc').disabled = false;

    document.all('usrProd_aux:txtCodi').disabled = false;
    document.all('usrProd_aux:txtProdNomb').disabled = false;

    document.all('usrProd_aux_imgBusc').disabled = false;
    document.all('usrProd_aux_imgLimp').disabled = false;
}

function usrProd_aux_onchange() {
    if ($('#hdnProductoId').val() != document.all('usrProd_aux:txtId').value) {
        $('#hdnProductoId').val(document.all('usrProd_aux:txtId').value);
        var prdt_id = document.all('usrProd_aux:txtId').value;
        //alert('Buscar vendedor ' + prdt_id);
        $('#btnBuscarPropietariosQueVenden').click();
    }
}

function usrProd_aux_usrCriadorFil_onchange() {
    var rowsVendedores = document.getElementById("gridVendedores").rows.length - 1;
    var rowsCompradores = document.getElementById("gridCompradores").rows.length - 1;

    if (rowsVendedores > 1 || rowsCompradores > 1) {
        $('#btnLimpiarGrids').click();
    }
    else {
        setearCmbRazaCriaVendedorComprador();
        limpiarCtrolPropductoATransferir()
    }
}

function setearCmbRazaCriaVendedorComprador() {
    var id = document.all('usrProd_aux:usrCriadorFil:cmbRazaCria').value;

    document.all('usrCriaVend_aux:cmbRazaCria').value = id;
    document.all('usrCriaVend_aux:cmbRazaCria').onchange();
    document.all('usrCriaVend_aux:txtCodi').value = '';
    document.all('usrCriaVend_aux:txtCodi').onchange();

    document.all('usrCriaComp_aux:cmbRazaCria').value = id;
    document.all('usrCriaComp_aux:cmbRazaCria').onchange()
    document.all('usrCriaComp_aux:txtCodi').value = '';
    document.all('usrCriaComp_aux:txtCodi').onchange();

    $('#hdnProductoId').val(document.all('usrProd_aux:txtId').value);
    $('#hdnVendedorId').val(id);
    $('#hdnCompradorId').val(id);
}

function usrProd_aux_cmbProdSexo_onchange() {
    var sexo = document.all("usrProd_aux:cmbProdSexo");
    setControlesSegunSexo(true);
}

function setControlesSegunSexo(limpiarControles)
{
    // Si es macho.
    if (document.all("usrProd_aux:cmbProdSexo").selectedIndex != "2") {
        document.all("chkReservaCrias").disabled = true;
        document.all("txtCantCriasReser").disabled = true;
        document.all("chkReservaEmbriones").disabled = false;
        document.all("txtCantReservaEmbriones").disabled = false;
    }
    else {
        document.all("chkReservaCrias").disabled = false;
        document.all("txtCantCriasReser").disabled = false;
        document.all("chkReservaEmbriones").disabled = true;
        document.all("txtCantReservaEmbriones").disabled = true;
    }

    if (limpiarControles) {
        $("#txtCantCriasReser").val('');
        $("#txtCantReservaEmbriones").val('');
        $("#chkReservaCrias").attr('checked', false);
        $("#chkReservaEmbriones").attr('checked', false);
    }
}

function expandir() {
    if (parent.frames.length > 0) try { parent.frames("menu").CambiarExp(); } catch (e) {; }
}

function agregarVendedor() {
    if ($('#hdnSumaPorcentajeVenta').val() >= 100) {
        alert('La suma de porcentajes ya ha llegado al 100%');
        return false;
    }

    if ($('#hdnProductoId').val().length == 0) {
        alert('Seleccione Producto a transferir');
        return false;
    }

    if ($('#usrCriaVend_aux_txtCodi').val().length == 0) {
        alert('Seleccione un criador');
        return false;
    }

    var cria_nume = document.all('usrCriaVend_aux:txtCodi').value;
    var table = document.getElementById("gridVendedores");
    var rows = table.rows.length - 1
    if (rows > 1) {
        rows -= 1;
        for (var r = 1; r <= rows; r++) {
            if (table.rows[r].cells[2].innerText == cria_nume) {
                alert('Ya existe Nº Prop ' + cria_nume);
                return false;
            }
        }
    }
   return true;
}

function modificarVendedor() {
    if ($('#usrCriaVend_aux_txtCodi').val().length == 0) {
        alert('Seleccione un criador');
        return false;
    }
    return true;
}

function agregarComprador() {
    if ($('#hdnSumaPorcentajeCompra').val() >= 100) {
        alert('La suma de porcentajes ya ha llegado al 100%');
        return false;
    }

    if ($('#hdnProductoId').val().length == 0) {
        alert('Seleccione Producto a transferir');
        return false;
    }

    if ($('#usrCriaComp_aux_txtCodi').val().length == 0) {
        alert('Seleccione un criador');
        return false;
    }

    if ($('#txtPorcentajeCompra').val().length == 0) {
        alert('Ingrese porcentaje de compra');
        $('#txtPorcentajeCompra').focus();
        return false;
    }

    if ($('#txtPorcentajeCompra').val() > 100) {
        alert('Porcentaje no puede ser mayo a 100');
        $('#txtPorcentajeCompra').focus();
        return false;
    }

    var sumaPorcentajes = 0;
    var cria_nume = document.all('usrCriaComp_aux:txtCodi').value;
    var table = document.getElementById("gridCompradores");
    var rows = table.rows.length - 1
    if (rows > 1) {
        rows -= 1;
        for (var r = 1; r <= rows; r++) {
            if (table.rows[r].cells[2].innerText == cria_nume) {
                alert('Ya existe Nº Prop ' + cria_nume);
                return false;
            }
            sumaPorcentajes += Number(table.rows[r].cells[4].innerText);
        }
    }

    sumaPorcentajes += Number($('#txtPorcentajeCompra').val());

    if (sumaPorcentajes > 100) {
        alert('La suma de porcentajes supera el 100%');
        return false;
    }

    $('#hdnSumaPorcentajeCompra').val(sumaPorcentajes);

    return true;
}

function modificarComprador() {
    if ($('#usrCriaComp_aux_txtCodi').val().length == 0) {
        alert('Seleccione un criador');
        return false;
    }

    if ($('#txtPorcentajeCompra').val().length == 0) {
        alert('Ingrese porcentaje de compra');
        $('#txtPorcentajeCompra').focus();
        return false;
    }

    if ($('#txtPorcentajeCompra').val() > 100) {
        alert('Porcentaje no puede ser mayo a 100');
        $('#txtPorcentajeCompra').focus();
        return false;
    }

    var sumaPorcentajes = Number($('#hdnSumaPorcentajeCompra').val() - $('#hdnPorcCompra').val()) + Number($('#txtPorcentajeCompra').val()) ;

    if (sumaPorcentajes > 100) {
        alert('La suma de porcentajes ha superado el 100%');
        $('#txtPorcentajeCompra').focus();
        return false;
    }

    $('#hdnSumaPorcentajeCompra').val(sumaPorcentajes);

    return true;
}

function validarAM() {
    if ($('#txtFechaTransferencia_aux').val().length == 0) {
        alert('Ingrese fecha de transferencia');
        return false;
    }

    if ($('#hdnProductoId').val().length == 0) {
        alert('Seleccione Producto a transferir');
        return false;
    }

    if (document.all("usrProd_aux:cmbProdSexo").selectedIndex != "2" && document.all("usrProd_aux:cmbProdSexo").selectedIndex != "1") {
        alert('Seleccione el sexo del producto a transferir');
        return false;
    }

    if (document.all('usrProd_aux:txtRP').value.length == 0) {
        alert('Ingrese RP');
        return false;
    }

    if (document.all('usrProd_aux:txtSraNume').value.length == 0) {
        alert('Ingrese HBA');
        return false;
    }

    var rowsVendedores = document.getElementById("gridVendedores").rows.length - 1;
    var rowsCompradores = document.getElementById("gridCompradores").rows.length - 1;

    if (rowsVendedores < 2 || rowsCompradores < 2) {
        alert('Debe ingresar al menos un Vendedor y un Comprador');
        return false; 
    }

    if ($('#hdnSumaPorcentajeCompra').val() != 100) {
        alert('La suma de porcentajes de compra debe ser del 100%');
        return false;
    }
}