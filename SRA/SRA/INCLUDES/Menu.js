var Menu_expandedObjects = new Array(); 
var Menu_expandCount = 0; 
var Menu_expandTimerID = 0; 
var Menu_collapseTimerID = 0; 
var Menu_collapseAllTimerID = 0; 
var Menu_expandingGroup = ''; 
var Menu_collapsingGroup = ''; 
var Menu_restoredGroup = ''; 
var Menu_collapsingAll = false; 
var Menu_curItem = ''; 
var Menu_hideSelectElems = true; 
var Menu_shadows = new Array(); 
var Menu_shadowEnabled = false; 
var Menu_shadowColor = '#777777'; 
var Menu_shadowOffest = 4; 
var Menu_contextUp = false; 

function Menu_itemMsOver(item, subGroup, expandDirection, horAdj, verAdj, expandDelay, effect) 
{
  var newLeft = 0; 
  var newTop = 0; 
  var oItem = document.all[item]; 
  var oSubGroup = document.all[subGroup]; 
    
  if (Menu_curItem != item)
  {
    Menu_curItem = item; 
  
    switch (expandDirection)
    {
      case 'belowleft': 
        newLeft = Menu_pageX(oItem); 
        if (newLeft + oSubGroup.offsetWidth > window.document.body.clientWidth)
          newLeft = Menu_pageX(oItem) + oItem.offsetWidth - oSubGroup.offsetWidth; 
        newTop = Menu_pageY(oItem) + oItem.offsetHeight; 
        break; 
      case 'belowright': 
        newLeft = Menu_pageX(oItem) + oItem.offsetWidth - oSubGroup.offsetWidth; 
        newTop =  Menu_pageY(oItem) + oItem.offsetHeight; 
        break; 
      case 'aboveleft': 
        newLeft = Menu_pageX(oItem); 
        newTop =  Menu_pageY(oItem) - oSubGroup.offsetHeight; 
        break; 
      case 'aboveright': 
        newLeft = Menu_pageX(oItem) + oItem.offsetWidth - oSubGroup.offsetWidth; 
        newTop =  Menu_pageY(oItem) - oSubGroup.offsetHeight; 
        break; 
      case 'rightdown': 
        newLeft = Menu_pageX(oItem) + oItem.offsetWidth; 
        if (newLeft + oSubGroup.offsetWidth > window.document.body.clientWidth)
          newLeft = Menu_pageX(oItem) - oSubGroup.offsetWidth; 
        newTop = Menu_pageY(oItem); 
        if (newTop + oSubGroup.offsetHeight > window.document.body.clientHeight)
          newTop = Menu_pageY(oItem) - oSubGroup.offsetHeight + oItem.offsetHeight; 
        break; 
      case 'rightup': 
        newLeft = Menu_pageX(oItem) + oItem.offsetWidth; 
        newTop = Menu_pageY(oItem) - oSubGroup.offsetHeight + oItem.offsetHeight; 
        break; 
      case 'leftdown': 
        newLeft = Menu_pageX(oItem) - oSubGroup.offsetWidth; 
        newTop = Menu_pageY(oItem); 
        break; 
      case 'leftup': 
        newLeft = Menu_pageX(oItem) - oSubGroup.offsetWidth; 
        newTop = Menu_pageY(oItem) - oSubGroup.offsetHeight + oItem.offsetHeight; 
        break; 
      default: 
        newLeft = Menu_pageX(oItem) + oItem.offsetWidth; 
        newTop = Menu_pageY(oItem); 
        break; 
    }  
    newLeft += horAdj; 
    newTop += verAdj; 
    oSubGroup.style.left = newLeft; 
    oSubGroup.style.top = newTop; 

    Menu_startExpand(subGroup, expandDelay, effect); 
  }
}

// If the mouse pointer is not over the given item or its subGroup, 
// this function initiates the collapse of the subGroup. 
function Menu_itemMsOut(item, group, subGroup, expandDelay, effect)
{
  if ((!(Menu_isMouseOnObject(item))) && subGroup) 
    if (!(Menu_isMouseOnObject(subGroup)))
    {
      Menu_curItem = ''; 
      Menu_startCollapse(subGroup, expandDelay, effect);
    }  
}

// This event handler is only called on expandable groups. If collapseAll is pending, it sets the 
// global variable Menu_restoredGroup to the given group, so that the group and its parent groups 
// are not collapsed. It also stops the collapse if it has been issued for this group. 
function Menu_groupMsOver(group)
{
  if (Menu_collapsingAll) Menu_restoredGroup = group; 

  if (Menu_collapsingGroup == group) 
  {
    Menu_stopCollapse(); 
    Menu_stopExpand(); 
  }
}

// If the mouse pointer is on the given group, its subGroup, or its parent item this function 
// does nothing. If the pointer is over the parent group, but outside the parent item, then it
// initiates the collapse of itself and its subGroup (if any). 
// Otherwise, the pointer is outside the menu structure and it initiates the collapse of all 
// expanded objects. 
function Menu_groupMsOut(group, parentItem, parentGroup, expandDelay, effect)
{ 
  if (!(Menu_isMouseOnObject(group)))
  {
    Menu_curItem = ''; 

    var subGroup = Menu_expandedObjects[Menu_expandCount]; 
    if (subGroup == group) subGroup = null; 

    if (parentItem == null && parentGroup == null && !(Menu_isMouseOnObject(group)))
      Menu_startCollapseAll(expandDelay, effect);     
    else if (Menu_isMouseOnObject(group) || Menu_isMouseOnObject(subGroup) || Menu_isMouseOnObject(parentItem))
      ; // do nothing 
    else if (Menu_isMouseOnObject(parentGroup))
    {
      Menu_startCollapse(group, expandDelay, effect); 
      Menu_startCollapse(subGroup, expandDelay, effect); 
    }
    else
      Menu_startCollapseAll(expandDelay, effect);     
  }
}

// Expand/collapse timer functinos ----------------------------------------------------------------

// Initiates the expand of the given group. 
function Menu_startExpand(group, interval, effect)
{
  if (group == Menu_collapsingGroup) Menu_stopCollapse(); 
  if (group != Menu_expandingGroup) Menu_stopExpand();  
  
  Menu_restoredGroup = group; 
  
  Menu_expandingGroup = group; 
  if (group) group += '.id'; 
  if (effect) effect = "'" + effect + "'";  
  Menu_expandTimerID = setTimeout('Menu_expand(' + group + ', ' + effect + ')', interval); 
}

// Initiates the collapse of the given group. 
function Menu_startCollapse(group, interval, effect)
{
  if (group == Menu_expandingGroup) Menu_stopExpand(); 

  if (group) 
    if (document.all[group].style.visibility == 'visible') 
    {
      Menu_collapsingGroup = group; 
      group += '.id'; 
      if (effect) effect = "'" + effect + "'";  
      Menu_collapseTimerID = setTimeout('Menu_collapse(' + group + ', ' + effect + ')', interval); 
    }  
}

// Initiates the collapse of all expanded objects. 
function Menu_startCollapseAll(interval, effect)
{
  Menu_stopCollapse(); 
  Menu_stopExpand(); 
  Menu_stopCollapseAll(); 

  Menu_collapsingAll = true; 
  if (effect) 
  {
    effect = "'" + effect + "'";  
    Menu_collapseAllTimerID = setTimeout('Menu_collapseAll(' + effect + ')', interval); 
  }
  else
    Menu_collapseAllTimerID = setTimeout('Menu_collapseAll(null)', interval); 
}

// Stops the expand of the currently expanding group. 
function Menu_stopExpand()
{
  clearTimeout(Menu_expandTimerID); 
  Menu_expandingGroup = ''; 
}

// Stops the collapse of the currently collapsing group. 
function Menu_stopCollapse()
{
  clearTimeout(Menu_collapseTimerID); 
  Menu_collapsingGroup = ''; 
}

// Stops the collapse of all currently expanding objects. 
function Menu_stopCollapseAll()
{
  clearTimeout(Menu_collapseAllTimerID); 
  Menu_restoredGroup = '';
}


// Core functions ---------------------------------------------------------------------------------

// Expands the given menu group 
function Menu_expand(group, effect)
{
  if (document.all[group].style.visibility != 'visible')
  {
    Menu_hideSelectElements(group); 
    if (effect) 
    {
      document.all[group].style.filter = effect; 
      document.all[group].filters[0].Apply(); 
    }  
    document.all[group].style.visibility = 'visible'; 
    Menu_makeDropShadow(group); 
    if (effect) document.all[group].filters[0].Play(); 
    Menu_expandCount++; 
    Menu_expandedObjects[Menu_expandCount] = group; 
  }  
}


// Collapses the given menu group 
function Menu_collapse(group, effect)
{
  if (group) 
  {
    if (document.all[group].style.visibility != 'hidden')
    {
      if (effect)
      {
        document.all[group].style.filter = effect; 
        document.all[group].filters[0].Apply(); 
      }
      document.all[group].style.visibility = 'hidden';     
      if (effect) document.all[group].filters[0].Play(); 
      Menu_expandCount--; 
      Menu_clearDropShadow(group); 
    }      
  }
  if (!(Menu_contextUp) && Menu_expandCount == 0) 
    Menu_restoreSelectElements(); 
}

// Collapses all expanded menu groups 
function Menu_collapseAll(effect)
{
  for (var i = Menu_expandCount; i >= 1; i--)
  {
    if (Menu_expandedObjects[i] == Menu_restoredGroup) break; 

    if (effect)
    {
      document.all[Menu_expandedObjects[i]].style.filter = effect; 
      document.all[Menu_expandedObjects[i]].filters[0].Apply(); 
    }
    document.all[Menu_expandedObjects[i]].style.visibility = 'hidden';
    Menu_clearDropShadow(Menu_expandedObjects[i]); 
    if (effect) document.all[Menu_expandedObjects[i]].filters[0].Play(); 
  }

  Menu_collapsingAll = false; 
  Menu_expandCount = i;
  Menu_restoredGroup = ''; 
  if (!(Menu_contextUp) && Menu_expandCount == 0) 
    Menu_restoreSelectElements(); 
}

// Hides all menu groups prior to calling ClientSideOnClick event handler
function Menu_hideAllGroups()
{
  Menu_curItem = ''; 
  Menu_restoredGroup = ''; 
  Menu_collapseAll(null); 
}

// Utilities --------------------------------------------------------------------------------------

// Updates menu item class, left icon, and right icon 
function Menu_updateCell(Element, NewClassName, LeftImage, LeftImageSrc, RightImage, RightImageSrc, direction)
{  
  if (direction == 'out' && Menu_isMouseOnObject(Element))    
    ;   
  else  
  {    
    if (Element != null & NewClassName != '') document.all[Element].className = NewClassName;
    if (LeftImage != null  && LeftImageSrc != '') document.images[LeftImage].src = LeftImageSrc;     
    if (RightImage != null && RightImageSrc != '') document.images[RightImage].src = RightImageSrc;   
  }
}

// Determines whether the mouse pointer is currently over the given object 
function Menu_isMouseOnObject(objName)
{
  if (objName)
  {
    var objLeft = Menu_pageX(document.all[objName]) - window.document.body.scrollLeft + 1; 
    var objTop = Menu_pageY(document.all[objName]) - window.document.body.scrollTop + 1; 
    var objRight = objLeft + document.all[objName].offsetWidth - 1; 
    var objBottom = objTop + document.all[objName].offsetHeight - 1;
  
    if ((event.x > objLeft) && (event.x < objRight) && 
        (event.y > objTop) && (event.y < objBottom))
      return true; 
    else  
      return false; 
  }
  else
    return false; 
}

// Calculates the absolute page x coordinate of any element
function Menu_pageX(element)
{
  var x = 0;
  do 
  {
    if (element.style.position == 'absolute') 
    {
      return x + element.offsetLeft; 
    }
    else
    {
      x += element.offsetLeft;
      if (element.offsetParent) 
        if (element.offsetParent.tagName == 'TABLE') 
          if (parseInt(element.offsetParent.border) > 0)
          {
            x += 1; 
          }
    }
  }
  while ((element = element.offsetParent));
  return x; 
}

// Calculates the absolute page y coordinate of any element
function Menu_pageY(element)
{
  var y = 0;
  do 
  {
    if (element.style.position == 'absolute') 
    {
      return y + element.offsetTop; 
    }
    else
    {
      y += element.offsetTop;
      if (element.offsetParent) 
        if (element.offsetParent.tagName == 'TABLE') 
          if (parseInt(element.offsetParent.border) > 0)
          {
            y += 1; 
          }
    }
  }
  while ((element = element.offsetParent));
  return y; 
}


// Hides HTML select elements that are overlapping the given menu group 
function Menu_hideSelectElements(group)
{
  if (document.getElementsByTagName) 
  {
    var arrElements = document.getElementsByTagName('select'); 
    if (Menu_hideSelectElems) 
    {
      for (var i = 0; i < arrElements.length; i++) 
        if (Menu_objectsOverlapping(document.all[group], arrElements[i]))
        {
		  if (arrElements[i].tag == 'S')
			arrElements[i].tag = 'N';
          arrElements[i].style.visibility = 'hidden';          
        }
    }
  }
}

// Restores all HTML select elements on the page 
function Menu_restoreSelectElements()
{
  if (document.getElementsByTagName) 
  {
    var arrElements = document.getElementsByTagName('select'); 
    if (Menu_hideSelectElems) 
      for (var i = 0; i < arrElements.length; i++) 
      {
        if (arrElements[i].tag != 'S')
			arrElements[i].style.visibility = 'visible'; 
      }
  }
}

// Whether the given objects are overlapping 
function Menu_objectsOverlapping(obj1, obj2)
{
  var result = true; 
  var obj1Left = Menu_pageX(obj1) - window.document.body.scrollLeft; 
  var obj1Top = Menu_pageY(obj1) - window.document.body.scrollTop; 
  var obj1Right = obj1Left + obj1.offsetWidth; 
  var obj1Bottom = obj1Top + obj1.offsetHeight;
  var obj2Left = Menu_pageX(obj2) - window.document.body.scrollLeft; 
  var obj2Top = Menu_pageY(obj2) - window.document.body.scrollTop; 
  var obj2Right = obj2Left + obj2.offsetWidth; 
  var obj2Bottom = obj2Top + obj2.offsetHeight;
  if (obj1Right <= obj2Left || obj1Bottom <= obj2Top || 
      obj1Left >= obj2Right || obj1Top >= obj2Bottom) 
    result = false; 
  return result; 
}

// Creates a drop shadow for an object 
function Menu_makeDropShadow(objName)
{
  if (Menu_shadowEnabled) 
  {
    Menu_shadows[objName] = new Array(); 
	  for (var i = Menu_shadowOffest; i > 0; i--)
	  {
	    var obj = document.all[objName]; 
		  var rect = document.createElement('div');
		  var rs = rect.style
		  rs.position = 'absolute';
		  rs.left = (obj.style.posLeft + i) + 'px';
		  rs.top = (obj.style.posTop + i) + 'px';
		  rs.width = obj.offsetWidth + 'px';
		  rs.height = obj.offsetHeight + 'px';
		  rs.zIndex = obj.style.zIndex - i;
		  rs.backgroundColor = Menu_shadowColor;
		  var opacity = 1 - i / (i + 1);
		  rs.filter = 'alpha(opacity=' + (100 * opacity) + ')';
		  obj.insertAdjacentElement('afterEnd', rect);
		  Menu_shadows[objName][Menu_shadows[objName].length] = rect; 
	  }
	}
}

// Clears the drop shadow for the given object 
function Menu_clearDropShadow(objName)
{
  if (Menu_shadowEnabled) 
  {
    var curShadow; 
    for (var i = 0; i < Menu_shadows[objName].length; i++)
    {
      curShadow = Menu_shadows[objName][i]; 
      curShadow.style.filter = 'alpha(opacity=0)'; 
      curShadow.removeNode(true); 
    }
  }  
}

// Positions the menu based on the alignment, offsetX, and offsetY properties
function Menu_positionMenu(menu, alignment, offsetX, offsetY)
{
  var scrlLeft = 0; 
  var scrlTop = 0;
  var clientW = window.document.body.clientWidth; 
  var clientH = window.document.body.clientHeight; 
  var menuWidth = menu.offsetWidth; 
  var menuHeight = menu.offsetHeight; 
  var newLeft = 0; 
  var newTop = 0; 

  switch (alignment)
  {
    case 'topleft': 
      newLeft = scrlLeft;
      newTop = scrlTop;
      break; 
    case 'topmiddle': 
      newLeft = (clientW - menuWidth) / 2 + scrlLeft;
      newTop = scrlTop;
      break; 
    case 'topright': 
      newLeft = clientW + scrlLeft - menuWidth;
      newTop = scrlTop;
      break; 
    case 'bottomleft': 
      newLeft = scrlLeft;
      newTop = clientH + scrlTop - menuHeight;
      break; 
    case 'bottommiddle': 
      newLeft = (clientW - menuWidth) / 2 + scrlLeft;
      newTop = clientH + scrlTop - menuHeight;
      break; 
    case 'bottomright': 
      newLeft = clientW + scrlLeft - menuWidth;
      newTop = clientH + scrlTop - menuHeight;
      break; 
    default: 
      newLeft = clientW + scrlLeft;
      newTop = clientH + scrlTop;
      break; 
  }    
  
  newLeft += offsetX; 
  newTop += offsetY; 
  menu.style.left = newLeft; 
  menu.style.top = newTop; 
}

function Menu_PopupMenu(Url, Target, QueMenu, JSFunction) {
  var theform = document.Form1;
if (JSFunction!="") eval(JSFunction);
  if (Url.slice(0,8) == 'Funcion:') { 
    setTimeout(Url.slice(8), 1, 'JScript'); 
  } else { 
    if (Url.slice(0,6) == 'Click:') { 
      theform.__Menu_PopupMenu_Url_State.value = Url;
      theform.__Menu_PopupMenu_Target_State.value = Target;
      __doPostBack(QueMenu,'');
    } else {
		location.href=Url;
		//window.open(Url,'_self'); 
    }
  }
}