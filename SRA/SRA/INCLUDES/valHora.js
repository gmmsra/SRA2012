function gValHora(pobjField)
{
  if (pobjField.value == ''){	
	return true;
  }
  if (booIsHora(pobjField.value))
  {
	return false
  }
  else
  {
    alert('Hora inv' + String.fromCharCode(225) + 'lida '+ pobjField.value+'\nPor favor, ingrese el formato hh:mm');
    pobjField.value = ''
    pobjField.focus(); 
    pobjField.select();
    return true
  }
}
function booIsHora(astrValue)
{
	var lintPunt = 0;
	var lintPuntPosi = 0;
	var lstrHora = '';
	var lstrMinu = '';
	for ( var i = 0 ; i < astrValue.length ; i++ )
	{
		var ch = astrValue.substring( i, i + 1 )
		if ( (ch < "0" || "9" < ch) && ch != ':')  {
			return false
		}
		if (ch == ':')  {
			lintPunt = lintPunt + 1;
			lintPuntPosi = i;
		}
		if (lintPunt == 0) {
			lstrHora = lstrHora + ch;
		}
		if (lintPunt != 0 && ch != ':') {
			lstrMinu = lstrMinu + ch;
		}
	}	
	if (lintPunt != 1 || lintPuntPosi == 0 || lintPuntPosi == astrValue.length )  {
		return false
	}
	if (lstrHora < "00" || "23" < lstrHora || lstrHora.length > 2)  {
		return false
	}
	if (lstrMinu < "00" || "59" < lstrMinu || lstrMinu.length > 2)  {
		return false
	}
	return true
}
function gMascaraHora(pobjTxt)
{
	var lstrCade = pobjTxt.value;
	var lstrLong = lstrCade.length;

	if (lstrLong > 4)
		return;

	if (lstrLong == 2 && event.keyCode!=58)
		pobjTxt.value = pobjTxt.value + ":";
}