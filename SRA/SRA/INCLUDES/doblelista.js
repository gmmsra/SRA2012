function dlAgregar(dlName)
{
	dlMover(document.all[dlName + "_ORI"], document.all[dlName + "_DES"],false, document.all["hdn" + dlName], "+");
}

function dlSacar(dlName)
{
	dlMover(document.all[dlName + "_DES"], document.all[dlName + "_ORI"],false, document.all["hdn" + dlName], "-");
}

function dlAgregarMul(dlName)
{
	dlMover(document.all[dlName + "_ORI"], document.all[dlName + "_DES"],true, document.all["hdn" + dlName], "+");
}

function dlSacarMul(dlName)
{
	dlMover(document.all[dlName + "_DES"], document.all[dlName + "_ORI"], true, document.all["hdn" + dlName], "-");
}

function dlMover(dlOri, dlDes, bTodos,hdnMovi,sSigno)
{
	var oItem;
	for (var i=dlOri.options.length-1;i>=0;i--)
	{
		oItem = dlOri.options[i];
		if(oItem.selected || bTodos)
		{
			oItem.selected=false;
			dlOri.options.remove(i);
			dlDes.options.add(oItem,0);
			hdnMovi.value = hdnMovi.value + sSigno + oItem.value + ',';
		}
	}
}