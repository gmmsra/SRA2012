function gMostrarPanelMenu (pobjPanel,pstrX,pstrY,pstrVisi)
{
	if (pstrVisi == 'visible')
	{
		document.all[pobjPanel].style.position = 'absolute';
		document.all[pobjPanel].style.left = pstrX;
		document.all[pobjPanel].style.top = document.body.scrollTop+pstrY+16;
	}
	document.all[pobjPanel].style.visibility = pstrVisi;
}
function gMostrarPanelUsrProd (pobjPanel,pstrVisi,pstrVal, pstrCtrl)
{ 
	if (pstrVisi == 'visible')
	{
		document.all[pobjPanel].style.position = 'absolute';
		mOcultarControles(pobjPanel,'hidden');
	}
	else
	{
		mOcultarControles(pobjPanel,'visible');
		if (pstrVal == '1')	
		{
			document.all[pstrCtrl + ":txtId"].value="";
			document.all[pstrCtrl + ":txtCodi"].value="";
			document.all[pstrCtrl + ":txtProdNomb"].value="";
			document.all[pstrCtrl + ":cmbProdAsoc"].selectedIndex=0;
			document.all[pstrCtrl + ":cmbProdRaza"].selectedIndex=0;
			if (document.all[pstrCtrl + ":cmbProdSexo"]!=null)
				document.all[pstrCtrl + ":cmbProdSexo"].selectedIndex=0;
			document.all[pstrCtrl + "_imgBusc"].onclick();
		}
	}	
	document.all[pobjPanel].style.visibility = pstrVisi;
}
function gMostrarPanelUsr (pobjPanel,pstrVisi,pstrVal, pstrCtrl)
{
	if (pstrVisi == 'visible')
	{
		document.all[pobjPanel].style.position = 'absolute';
		mOcultarControles(pobjPanel,'hidden');		
	}
	else
	{
		mOcultarControles(pobjPanel,'visible');

		if (pstrVal == '1')	
		{
			document.all[pstrCtrl + ":txtId"].value="";
			document.all[pstrCtrl + ":txtCodi"].value="";
			document.all[pstrCtrl + ":txtApel"].value="";			
			document.all[pstrCtrl + "_imgBusc"].onclick();
		}
	}	
	document.all[pobjPanel].style.visibility = pstrVisi;
}
function gMostrarPanelCriaUsr (pobjPanel,pstrVisi,pstrVal, pstrCtrl)
{
	if (pstrVisi == 'visible')
	{
		document.all[pobjPanel].style.position = 'absolute';
		mOcultarControles(pobjPanel,'hidden');		
	}
	else
	{
		mOcultarControles(pobjPanel,'visible');

		if (pstrVal == '1')	
		{
			document.all[pstrCtrl + ":txtId"].value="";
			document.all[pstrCtrl + ":txtCodi"].value="";
			document.all[pstrCtrl + ":txtCriaNomb"].value="";			
			document.all[pstrCtrl + "_imgBusc"].onclick();
		}
	}	
	document.all[pobjPanel].style.visibility = pstrVisi;
}
function mBuscSele(pstrValor,pobjBusc) 
{
	document.all[pobjBusc].value = pstrValor;
}
function mBuscMostrarCriaUsr(pobjPanel,pstrVisi,pstrVal,pstrCtrl)
{
	gMostrarPanelCriaUsr (pobjPanel,pstrVisi,pstrVal,pstrCtrl);
}
function mBuscMostrarUsr(pobjPanel,pstrVisi,pstrVal,pstrCtrl)
{
	gMostrarPanelUsr (pobjPanel,pstrVisi,pstrVal,pstrCtrl);
}
function mBuscMostrarProdUsr(pobjPanel,pstrVisi,pstrVal,pstrCtrl)
{
	gMostrarPanelUsrProd (pobjPanel,pstrVisi,pstrVal,pstrCtrl);
}
function mOcultarControles(pobjPanel,pstrHide)
{

	if (document.getElementsByTagName) 
	{
		var arrPaneles = document.getElementsByTagName('div'); 
		for (var i = 0; i < arrPaneles.length; i++) 
		{
			if (arrPaneles[i].id.indexOf("panBuscAvan")!=-1 && arrPaneles[i].id != document.all[pobjPanel].id)
			{
				arrPaneles[i].style.visibility = 'hidden';
			}
		}
		var arrElements = document.getElementsByTagName('select'); 
		for (var i = 0; i < arrElements.length; i++) {
			if (Menu_objectsOverlapping(document.all[pobjPanel], arrElements[i]))
			{
				if (arrElements[i].tag != 'N')
				{
					arrElements[i].style.visibility = pstrHide;
				}
			}
		}	
	}
}
function gNoOcultar(pCombo)
{
	if (document.all[pCombo])
		document.all[pCombo].tag = 'N';
}
function gCentrarPanel(pPanel)
{
	var lstrPanel = document.all[pPanel].style.width;
	var lstrScreen = screen.width;
	lstrPanel = lstrPanel.replace("px","")
	var lstrLeft = (lstrScreen-lstrPanel)/2;
	document.all[pPanel].style.left = lstrLeft;	
}
function gOcultarCombos (pPanel)
{
	if (document.all[pPanel])
	{
		gCentrarPanel(pPanel);
		if (document.getElementsByTagName) 
		{
			var arrElements = document.getElementsByTagName('select'); 
			for (var i = 0; i < arrElements.length; i++) {
				if (Menu_objectsOverlapping(document.all[pPanel], arrElements[i]))
				{				
					if (arrElements[i].tag != 'N')
					{
						arrElements[i].style.visibility = 'hidden';						
						arrElements[i].tag = 'S';
					}
				}
			}
		}
	}	
}

function Menu_objectsOverlapping(obj1, obj2)
{
  var result = true; 
  var obj1Left = Menu_pageX(obj1) - window.document.body.scrollLeft; 
  var obj1Top = Menu_pageY(obj1) - window.document.body.scrollTop; 
  var obj1Right = obj1Left + obj1.offsetWidth; 
  var obj1Bottom = obj1Top + obj1.offsetHeight;
  var obj2Left = Menu_pageX(obj2) - window.document.body.scrollLeft; 
  var obj2Top = Menu_pageY(obj2) - window.document.body.scrollTop; 
  var obj2Right = obj2Left + obj2.offsetWidth; 
  var obj2Bottom = obj2Top + obj2.offsetHeight;
  if (obj1Right <= obj2Left || obj1Bottom <= obj2Top || 
      obj1Left >= obj2Right || obj1Top >= obj2Bottom) 
    result = false; 
  return result; 
}

// Calculates the absolute page x coordinate of any element
function Menu_pageX(element)
{
  var x = 0;
  do 
  {
    if (element.style.position == 'absolute') 
    {
      return x + element.offsetLeft; 
    }
    else
    {
      x += element.offsetLeft;
      if (element.offsetParent) 
        if (element.offsetParent.tagName == 'TABLE') 
          if (parseInt(element.offsetParent.border) > 0)
          {
            x += 1; 
          }
    }
  }
  while ((element = element.offsetParent));
  return x; 
}

// Calculates the absolute page y coordinate of any element
function Menu_pageY(element)
{
  var y = 0;
  do 
  {
    if (element.style.position == 'absolute') 
    {
      return y + element.offsetTop; 
    }
    else
    {
      y += element.offsetTop;
      if (element.offsetParent) 
        if (element.offsetParent.tagName == 'TABLE') 
          if (parseInt(element.offsetParent.border) > 0)
          {
            y += 1; 
          }
    }
  }
  while ((element = element.offsetParent));
  return y; 
}
