var Ventana;
var mbooParam;


var myLoc = document.location; 
var myurlParts = myLoc.pathname.split("/"); 
var url = "/" +  myurlParts[1]+ "/"+ "XMLHTTP.aspx";
var baseUrl = "/" +  myurlParts[1]+ "/"

// Dario 2013-12-20 Se agrenan los js de jquery para los bloqueos de pantalla
//addJavascript('includes/jquery-1.7.js'); 
//addJavascript('includes/jQuery.blockUI.js'); 
//addJavascript('/includes/prototype.js'); 

		//<script language="JavaScript" src="includes/jquery-1.7.js"></script>
		//<script language="JavaScript" src="includes/jQuery.blockUI.js"></script>

function addJavascript(jsname) {
	var th = document.getElementsByTagName('head')[0];
	var s = document.createElement('script');
	s.setAttribute('type', 'text/javascript');
	s.setAttribute('src', baseUrl + jsname);
	th.appendChild(s);
}
/*Dario 2014-06-05 funcion que solo deja ingresar numeros,
	se coloca aca ya que algun boludo la tiene resparramada por todos lados y en cada aspx
 */
function SoloNumeros(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
		return false;
	
return true;
}

function mBotonBusquedaAvanzada(pTabla,pCampo,pCmb,pTitu,pFiltro)
{
	if (!document.all(pCmb).disabled)
		gAbrirVentanas("consulta_combo_Pop.aspx?EsConsul=0&titulo=" + pTitu + "&tabla=" + pTabla + "&filtros=" + pFiltro + "&campoDesc=" + pCampo + "&combo=" + pCmb, 1, "700","520");
}

function mBotonBusquedaAvanzadaRepo(pTabla,pCampo,pCmb,pTitu,pFiltro)
{
	if (!document.all(pCmb).disabled)
		gAbrirVentanas("../consulta_combo_Pop.aspx?EsConsul=0&titulo=" + pTitu + "&tabla=" + pTabla + "&filtros=" + pFiltro + "&campoDesc=" + pCampo + "&combo=" + pCmb, 1, "700","520");
}

function FechaSQL (pfecha) 
{
	// devuelve la fecha formateado mm/dd/aa 
	// para pasarla correctamente a un SP 
	var vsRet;
	vsRet = pfecha.split("/");
	vsRet = vsRet[1] + "/" + vsRet[0] + "/" + vsRet[2];
	return vsRet 

} 

function NumDecimal(Num,cantDec) 
	{
		var Numero=String(Num); 
	
		if(0.9>1)
			Numero=String(Numero).replace(".",",");//tiene coma
		
		if(isNaN(eval(Numero).toFixed(cantDec)))
			return 0;
		else
			return eval(Numero).toFixed(cantDec);
	}

function gConvertNumber(number) 
{ 
	if (number != "")
	{
		return parseFloat(number)
	}
	else
	{
		return 0
	}
}

function LoadComboXML(Oper,Args,comboName,Opc,DescCode)
{
	//alert(String.fromCharCode(225));
	//alert(String.fromCharCode(233));
	//alert(String.fromCharCode(237));
	//alert(String.fromCharCode(243));
	//alert(String.fromCharCode(250));
	//alert(String.fromCharCode(241));

	var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
	xmlDoc.async = false;
	var CallString=url+"?Oper="+Oper+"&cmb="+comboName+"&Opc="+Opc+"&Args="+Args;
	xmlDoc.load(CallString);
	var vCombos = comboName.split(";")
	
	if (DescCode==null)
		DescCode=false;
		
	for (j=0; j < vCombos.length;j++)
	{
		CargarComboXML(vCombos[j], xmlDoc.firstChild.childNodes[j].firstChild.childNodes,DescCode);
	}
}

function LimpiarCombo(pCombo)
{
	for(i=pCombo.length;i > -1;i--)
		pCombo.remove(i);
}
		
function CargarComboXML (comboName, rs,DescCode)
{
	var i=0;
	var combo = document.all(comboName);

	for(i=combo.length;i>-1;i--)
		combo.remove(i);

	for (i=0;i < rs.length;i++)
	 {
		var It =document.createElement("option");
		var unr=rs[i];
		for(var j=0;j<unr.childNodes.length;j++)
		{
		   switch(unr.childNodes[j].nodeName.toLowerCase())
		      {
		      case "id":
		         {
		         It.value=unr.childNodes[j].text;
		         break;
		         }
		      case "descrip":
		         {
					if (DescCode==false)
					{
						It.text=unr.childNodes[j].text;
						break;
					}
		         }
		      case "descrip_codi":
		         {
					if (DescCode==true)
					{
						It.text=unr.childNodes[j].text;
						break;
					}
		         }
		      }
		   }
		  //if(It.value=="-1" && It.text=="") It.text="(Todos)";
 		  combo.add(It);
		}
		
		if(combo.length>0&&combo.item(0).value!="")
		{
			combo.selectedIndex=0;
			//combo.value=0;
		}

		var combo = document.all(comboName);
		
		if (document.all("txt"+comboName)!=null)
			document.all("txt"+comboName).value="";
			
		if (combo.length == 1 &&(combo.item(0).value!=""))// || ((combo.length == 2)&&(combo.item(0).value=="-1")))
		{
		combo.value=combo.item(combo.length-1).value;
		//try{eval(comboName + "_onchange();")}
		try{combo.onchange();}
		catch(e){;}
		}
}

function EjecutarMetodoXML(Oper,Args)
{
	try
	{
		this.xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		this.xmlDoc.async = false;
		var CallString=url+"?Oper="+Oper+"&Args="+Args;
		this.xmlDoc.load(CallString) ;
		
		return(this.xmlDoc.lastChild.lastChild.text);
	}
	catch(e)
	{
			return('');
	}
	finally{;}
		
}

function LeerCamposXML(pstrTabla,pstrId,pstrCampos)
{
try
	{
	this.xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		this.xmlDoc.async = false;
		var CallString=url+"?Oper="+pstrTabla+"&Args="+pstrId+"&Opc="+pstrCampos;
		this.xmlDoc.load(CallString) ;
		
		return(this.xmlDoc.lastChild.lastChild.text);
	}
	catch(e)
	{
			return('');
	}
	finally{;}
}

function ActivarControl(oCtrl, bActivo)
{
	if(!bActivo)
	{
		oCtrl.disabled=true;
		oCtrl.value="";
		oCtrl.className = "cuadrotextodeshab";
	}
	else
	{
		oCtrl.disabled=false;
		oCtrl.className = "cuadrotexto";
	}
}

function ActivarControlFech(ctrlIni, pstrCtrlDest)
{
	var bCheck = document.all[ctrlIni.id + "_0"].checked;

	if(!bCheck)
	{
		document.all[pstrCtrlDest].value="";
		document.all[pstrCtrlDest].className = "cuadrotextodeshab";
		ActivarFecha(pstrCtrlDest,false);
	}
	else
	{
		document.all[pstrCtrlDest].className = "cuadrotexto";
		ActivarFecha(pstrCtrlDest,true);
	}
}

function agregarParam(pstrId, pstrCtrol, pstrParam) 
{
	var sRet="";
	var vsRet;	

	if (document.all[pstrId + ":" + pstrCtrol]!= null)
	{	
		if(document.all(pstrId + ":" + pstrCtrol).type=="checkbox") 
		{
			if(document.all(pstrId + ":" + pstrCtrol).checked)
				sRet = "1";
			else
				sRet = "0";
			document.all(pstrId + ":" + pstrCtrol).checked=false;
		}
		else
		{
			sRet=document.all(pstrId + ":" + pstrCtrol).value;

			if((pstrCtrol!="txtId")&&(pstrCtrol!="txtCodi")&&(pstrCtrol!="txtApel")&&(pstrCtrol!="txtCriaNomb")&&(pstrCtrol!="cmbRazaCria")&&(pstrCtrol!="txtProdNomb")&&(pstrCtrol!="cmbProdAsoc")&&(pstrCtrol!="txtSexoId")&&(pstrCtrol!="cmbProdRaza")&&(pstrCtrol!="txtCriaId")&&(pstrCtrol!="txtSraNume")&&(pstrCtrol!="hdnRazaCruza")&&(pstrCtrol!="usrCriadorFil:cmbRazaCria")&&(pstrCtrol!="usrCriadorFil:txtId"))
				document.all(pstrId + ":" + pstrCtrol).value="";

			mbooParam = (mbooParam || (sRet!="" && sRet!="0"));
		}
		sRet=sRet.replace(/;/gi,"").replace(/&/gi,"").replace(/'/gi," ").replace(String.fromCharCode(209),"%C3%91").replace(String.fromCharCode(241),"%C3%B1");
		// Dario 2013-11-18 se vuelve a la version original ya que se rompe con replace("'","~")		
		//sRet=sRet.replace(";","").replace("&","").replace("'","~").replace(String.fromCharCode(209),"%C3%91").replace(String.fromCharCode(241),"%C3%B1");
    // gsz  sRet=sRet.replace(";","").replace("&","").replace("'"," ").replace(String.fromCharCode(209),"%C3%91").replace(String.fromCharCode(241),"%C3%B1");

		if(sRet!="")
		{	
			vsRet=sRet.split(",");
			sRet=vsRet[0];
			if(vsRet.length>1)
				sRet=sRet+"&Nomb=" + vsRet[1].replace(/(^\s*)|(\s*$)/g, "");
		}
		return("&" + pstrParam + "=" + sRet);
	}
	else
		return("");
}

function limpiarParam(pstrId, pstrCtrol) 
{
	var lCtrl = document.all[pstrId + ":" + pstrCtrol];
	if (lCtrl!= null)
	{	
		switch(lCtrl.type)
		{
			case "checkbox":
		        {
		        lCtrl.checked=false;
		        break;
		        }
		    case "select-one":
		        {
		        lCtrl.selectedIndex = 0;
		        break;
		        }
		    default:
				{
				lCtrl.value = "";
		        break;
		        }
		}
	}
}

function window_onfocus() 
{
	if (Ventana!= null)
	{
		try
		{
			Ventana.focus();
		}
		catch(e)
		{
			Ventana=null;
		}
		finally{;}
	}
	
	for(i=0;i<16;i++)
	{
		if(gVentanas[i]!=null)
		{
			try{gVentanas[i].focus();}
			catch(e){gVentanas[i]=null;}
			finally{;}
		}
	}
}

function gHabilitarControl(pObj,pHabi)
{
	if (pObj != null)
	{
		if (pHabi)
		{
			pObj.disabled = false;
			pObj.className = 'cuadrotexto';
		}
		else
		{
			pObj.disabled = true;
			pObj.className = 'cuadrotextodeshab';
			pObj.value='';
		}		
	}
}

function gHabilitarControles(pObj,pHabi,iVal)
{
	if (pObj != null)
	{
		if (pHabi)
		{
			pObj.disabled = false;
			pObj.className = 'cuadrotexto';
			pObj.value = iVal;
		}
		else
		{
			pObj.disabled = true;
			pObj.className = 'cuadrotextodeshab';
			pObj.value='';
		}		
	}
}

function gSetearTituloFrame(pstrTitle)
{
	if (pstrTitle == '' || pstrTitle == null)
		pstrTitle = document.title;
	window.parent.document.title = pstrTitle;
}

function gBuscarCombo(pstrTexto, pcmbCombo)
{
	var lintIndi;
	for (var lintIndi = 0; lintIndi < pcmbCombo.length; lintIndi++)
	{
		if (pcmbCombo.options[lintIndi].value == pstrTexto)
		{
			pcmbCombo.selectedIndex = lintIndi;
		}

	}
}

function gBuscarValorCombo(pstrTexto, pcmbCombo)
{
	var lintIndi;
	for (var lintIndi = 0; lintIndi < pcmbCombo.length; lintIndi++)
	{
		if (pcmbCombo.options[lintIndi].value == pstrTexto)
		{
			pcmbCombo.selectedIndex = lintIndi;
		}
	}
}

function gBuscarTextoCombo(pstrTexto, pcmbCombo)
{
	var lintIndi;
	for (var lintIndi = 0; lintIndi < pcmbCombo.length; lintIndi++)
	{
		if (pcmbCombo.options[lintIndi].text == pstrTexto)
		{
			pcmbCombo.selectedIndex = lintIndi;
		}
	}
}

function gMascaraCunica(pobjTxt)
{
	var lstrCade = pobjTxt.value;
	var lstrLong = lstrCade.length;
	if (lstrLong == 2 && event.keyCode!=58)
		pobjTxt.value = pobjTxt.value + "-";
	if (lstrLong == 11 && event.keyCode!=58)
		pobjTxt.value = pobjTxt.value + "-";
}

function mBuscarIdXCodi(comboName, Oper, Dato)
{
	var Args;
	var booExito=false;
	
	if(Dato=="Id")
		Args = document.all("txt" + comboName).value;
	else
		Args = document.all(comboName).value;
	
	if(Args!="")
	{
		if(Dato=="Id")
			Args = "@codi='" + Args.replace("'","") + "'";
		else
			Args = "@id=" + Args;

		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = false;
		var CallString=url+"?Oper="+Oper+"&cmb="+comboName+"&Opc=C"+"&Args="+Args;
		xmlDoc.load(CallString);
		
		if (xmlDoc.firstChild != null && xmlDoc.firstChild.nodeTypedValue != "") {
		    for (var k = 0; k < xmlDoc.firstChild.childNodes[0].firstChild.childNodes.length; k++) {
		        var unr = xmlDoc.firstChild.childNodes[0].firstChild.childNodes[k];

		        for (var j = 0; j < unr.childNodes.length; j++) {
		            if (unr.childNodes[j].nodeName == "id" && Dato == "Id") {
		                document.all(comboName).value = unr.childNodes[j].text;
		                if (document.all(comboName).value == unr.childNodes[j].text) {
		                    booExito = true;
		                    break;
		                }
		            }

		            if (unr.childNodes[j].nodeName == "codi" && Dato == "Codi")
		                document.all("txt" + comboName).value = unr.childNodes[j].text;
		        }
		        if (booExito)
		            break;
		    }

		    if (Dato == "Id" && booExito)
		        document.all(comboName).onchange();
		}
		else {
		    document.all(comboName).value = "";
		    document.all("txt" + comboName).value = "";
		    if (Dato == "Id")
		        document.all(comboName).onchange();
		}
	}
	else
	{
	document.all(comboName).value="";
	document.all("txt" + comboName).value="";
	if(Dato=="Id")
		document.all(comboName).onchange();
	}
}

var gVentanas = new Array(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null);

function gAbrirVentanas(pstrPagina, pintOrden, pstrAncho, pstrAlto, pstrLeft, pstrTop, pbooMax)
{
	var booHacerFoco = (pstrLeft == "" || pstrLeft == null);

	if (pbooMax != null && pbooMax == true)
	{
		pstrAncho = screen.availWidth - 10;
		pstrAlto = screen.availHeight - 20;
	
		pstrLeft = 0;
		pstrTop = 0;
	}
	else
	{
		if (pstrAncho == "" || pstrAncho == null)
			pstrAncho = (screen.availWidth - 50)
		if (pstrAlto == "" || pstrAlto == null)
			pstrAlto = (screen.availHeight - 150)
		
		if (pstrLeft == "" || pstrLeft == null)
			pstrLeft = (screen.availWidth - pstrAncho)/2
		if (pstrTop == "" || pstrTop == null)
			pstrTop = (screen.availHeight - pstrAlto)/2
	}	
		
	gVentanas[pintOrden] = window.open(pstrPagina, "", "location=no,menubar=no,scrollbars=yes,status=yes,titlebar=no,toolbar=no,width=" + pstrAncho + "px,height=" + pstrAlto + "px,left=" + pstrLeft + "px,top=" + pstrTop + "px");
	
	for(i=0;i<16;i++)
	{
		if(gVentanas[i]!=null)
		{
			try{gVentanas[i].focus();}
			catch(e){gVentanas[i];}
			finally{;}
		}
	}
	
	gVentanas[pintOrden].focus();
	
	if(booHacerFoco)
	{
		window.attachEvent('onfocus',window_onfocus);
		window.document.body.attachEvent('onfocus',window_onfocus);
	}
}

function gCerrarVentanas()
{ 
 	try 
 	{   
 	for(i=0;i  <16;i++)
	{
		if(gVentanas[i]!=null)
 		{
 			gVentanas[i].close();
 			gVentanas[i]=null;
		}
	}
	}
	catch(e){}
}

function mDescargarDocumento(pId, pTabla, pControl)
{
	if (document.all(pControl).value != "")
		window.location.href="Descarga.aspx?tabla=" + pTabla + "&id=" + document.all(pId).value;
		//gAbrirVentanas(document.all(pId).value + "_" + pTabla + "_" + document.all(pControl).value, 10, '200','200');
}

function mLimpiarPath(pCont,pImg)
{
	document.all(pCont).value = '';
	if (document.all(pImg)!=null)
		document.all(pImg).style.visibility="hidden";;
}


/* Dario 2013-11-23 se saca propotype y se coloca jquery*/
function RazaOrCriador(pRazaId , pCriador)
{
	var vRazaId;
	var  vCriador;
	// vRazaId=  $$('[id=' & pRazaId & ']')[0].value;
	// vCriador = $$('[id=' & pCriador & ']')[0].value;
	vRazaId = $(pRazaId).value
	vCriador= $(pCriador).value
	if ((vRazaId != "" && vCriador == "") || (vRazaId == "" && vCriador != ""))
		return true;
    else
		return false; 

	}



/****** CONTROL CLIENTES *******/


function CodiRazaCriador_change(strId, strAutoPB, strArgs, strCampoVal, pRaza, pCriador)
{
	var oCoti = document.all(strId + ":txtCodi");
	var opc = oCoti.value;
	if (opc == -1)
		oCoti.value=0;
		
	if (oCoti.value==0)
		document.all(strId + ":txtCodi").value = '';

	var sFiltro = oCoti.value;
	var sIdOri = document.all(strId + ":txtId").value;

	document.all(strId + ":txtApel").value = "";
	document.all(strId + ":txtId").value = "";
       
    var boolResp= RazaOrCriador(pRaza,pCriador);
	
	
	if (!booIsNumber(sFiltro)||(sFiltro=="")||(boolResp==true) )
	{
		if (document.all[strId + ":txtDesc"]!= null)
			document.all(strId + ":txtDesc").value = "";

		if (document.all[strId + ":cmbRazaCria"]!= null && !document.all[strId + ":cmbRazaCria"].disabled)
		{
			if (opc != -1)
			{
				document.all(strId + ":cmbRazaCria").value = document.all(strId + ":txtRazaSessId").value;	   
				document.all(strId + ":cmbRazaCria").onchange();
			}
		}

		if(strAutoPB=="1" && sIdOri!="")
		__doPostBack(strId + ":txtId",'');
	}
	else	
	{
		if (document.all[strId + ":cmbRazaCria"]!= null)
		strArgs = strArgs + ";" + document.all[strId + ":cmbRazaCria"].value;
		    	
		sFiltro += ";" + strArgs;

		var sRet=EjecutarMetodoXML("Utiles.BuscarClieDeriv", sFiltro);
			
		if(sRet=="")
		{
			alert("Criador inexistente")
			document.all(strId + ":txtApel").value = "";
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = "";
			document.all(strId + ":txtCodi").value = '';

			oCoti.focus();
			
			if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
		}
		else
		{
			var vsRet=sRet.split("|");
			document.all(strId + ":txtApel").value = vsRet[2];
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = vsRet[3];
			if (document.all[strId + ":cmbRazaCria"]!= null && !document.all[strId + ":cmbRazaCria"].disabled)
			{
				document.all(strId + ":cmbRazaCria").value =  vsRet[4];	   
				document.all(strId + ":cmbRazaCria").onchange();
			}
			document.all(strId + ":txtId").value = vsRet[0];

			try{eval(strId + "Id_onchange();")}
			catch(e){;}
			
			if(strAutoPB=="1")
				__doPostBack(strId + ":txtId",'');
		}
	}
	if (document.getElementById(strId + ":txtApel").value!="")
	{
		var range = document.selection.createRange();
		document.getElementById(strId + ":txtApel").focus();
		if (window.event!=null)
			window.event.returnValue = false;
	}
	
	try{eval(strId + "_onchange();")}
	catch(e){;}
}


function CodiCriador_change(strId, strAutoPB, strArgs, strCampoVal)
{
	var oCoti = document.all(strId + ":txtCodi");
	var opc = oCoti.value;
	if (document.all(strId + ":btnCopropiedad") != null)
		{
			document.all(strId + ":btnCopropiedad").disabled = true; //-Oculta/Muestra Bot�n de Copropiedad.
		}
	if (opc == -1)
		oCoti.value=0;
		
	if (oCoti.value==0)
		document.all(strId + ":txtCodi").value = '';

	var sFiltro = oCoti.value;
	var sIdOri = document.all(strId + ":txtId").value;

	document.all(strId + ":txtApel").value = "";
	document.all(strId + ":txtId").value = "";
	
	    
	if (!booIsNumber(sFiltro)||(sFiltro=="") )
	{
		if (document.all[strId + ":txtDesc"]!= null)
			document.all(strId + ":txtDesc").value = "";

		/*if (document.all[strId + ":cmbRazaCria"]!= null && !document.all[strId + ":cmbRazaCria"].disabled)
		{
			if (opc != -1)
			{
				document.all(strId + ":cmbRazaCria").value = document.all(strId + ":txtRazaSessId").value;	   
				document.all(strId + ":cmbRazaCria").onchange();
			}
		}*/

		if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
	}
	else	
	{
		if (document.all[strId + ":cmbRazaCria"]!= null)
			if (document.all[strId + ":cmbRazaCria"].value != '')
			{
				strArgs = strArgs + ";" + document.all[strId + ":cmbRazaCria"].value;
			}
			else
			{
				alert('Debe indicar una Raza.');
				document.all(strId + ":txtCodi").value = '';
				return;
			}
				
			    
		sFiltro += ";" + strArgs;      

		
		var sRet=EjecutarMetodoXML("Utiles.BuscarCriadorDeriv", sFiltro);
			
		if(sRet=="")
		{
			alert("Criador inexistente")
			document.all(strId + ":txtApel").value = "";
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = "";
			document.all(strId + ":txtCodi").value = '';

			oCoti.focus();
			
			if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
		}
		else
		{
			var vsRet=sRet.split("|");
			document.all(strId + ":txtApel").value = vsRet[2];
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = vsRet[3];
			if (document.all[strId + ":cmbRazaCria"]!= null && !document.all[strId + ":cmbRazaCria"].disabled)
			{
				document.all(strId + ":cmbRazaCria").value =  vsRet[4];	   
				document.all(strId + ":cmbRazaCria").onchange();
			}
			document.all(strId + ":txtId").value = vsRet[0];
			if (document.all(strId + ":btnCopropiedad") != null)
			{
				//vsRet[5] devuelve "true" si Deshabilita el bot�n Copropiedad, de lo contrario devuelve "false".
				if (vsRet[5] == "True")
					document.all(strId + ":btnCopropiedad").disabled = true;
				else
					document.all(strId + ":btnCopropiedad").disabled = false;
			}

			try{eval(strId + "Id_onchange();")}
			catch(e){;}
			
			if(strAutoPB=="1")
				__doPostBack(strId + ":txtId",'');
		}
	}

	if (document.getElementById(strId + ":txtApel").value!="")
	{
		var range = document.selection.createRange();
		document.getElementById(strId + ":txtApel").focus();
		if (window.event!=null)
			window.event.returnValue = false;
	}
	
	try{eval(strId + "_onchange();")}
	catch(e){;}
}


function CodiClieDeriv_change(strId, strAutoPB, strArgs, strCampoVal)
{
	//$.blockUI({ message: '<h1><img src="imagenes/loader.gif" /> Espere por favor...</h1>' });
	var oCoti = document.all(strId + ":txtCodi");
	var opc = oCoti.value;
	if (opc == -1)
		oCoti.value=0;
		
	if (oCoti.value==0)
		document.all(strId + ":txtCodi").value = '';

	var sFiltro = oCoti.value;
	var sIdOri = document.all(strId + ":txtId").value;

	document.all(strId + ":txtApel").value = "";
	document.all(strId + ":txtId").value = "";
  
       
	if (!booIsNumber(sFiltro)||(sFiltro=="") )
	{
		if (document.all[strId + ":txtDesc"]!= null)
			document.all(strId + ":txtDesc").value = "";

		if (document.all[strId + ":cmbRazaCria"]!= null && !document.all[strId + ":cmbRazaCria"].disabled)
		{
			if (opc != -1)
			{
				document.all(strId + ":cmbRazaCria").value = document.all(strId + ":txtRazaSessId").value;	   
				document.all(strId + ":cmbRazaCria").onchange();
			}
		}

		if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
	}
	else	
	{
		if (document.all[strId + ":cmbRazaCria"]!= null)
		strArgs = strArgs + ";" + document.all[strId + ":cmbRazaCria"].value;
		    	
		sFiltro += ";" + strArgs;
		

        

		
		var sRet=EjecutarMetodoXML("Utiles.BuscarClieDeriv", sFiltro);
			
		if(sRet=="")
		{
			alert(strCampoVal + " inexistente")
			document.all(strId + ":txtApel").value = "";
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = "";
			document.all(strId + ":txtCodi").value = '';

			oCoti.focus();
			
			if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
		}
		else
		{
			var vsRet=sRet.split("|");
			document.all(strId + ":txtApel").value = vsRet[2];
			if (document.all[strId + ":txtDesc"]!= null)
				document.all(strId + ":txtDesc").value = vsRet[3];
			if (document.all[strId + ":cmbRazaCria"]!= null && !document.all[strId + ":cmbRazaCria"].disabled)
			{
				document.all(strId + ":cmbRazaCria").value =  vsRet[4];	   
				document.all(strId + ":cmbRazaCria").onchange();
			}
			document.all(strId + ":txtId").value = vsRet[0];

			try{eval(strId + "Id_onchange();")}
			catch(e){;}
			
			if(strAutoPB=="1")
				__doPostBack(strId + ":txtId",'');
		}
	}
	if (document.getElementById(strId + ":txtApel").value!="")
	{
		var range = document.selection.createRange();
		document.getElementById(strId + ":txtApel").focus();
		if (window.event!=null)
			window.event.returnValue = false;
	}
	
	try{eval(strId + "_onchange();")}
	catch(e){;}
	
	//$.unblockUI();
}

function IdClieDeriv_change(strId)
{
	try{eval(strId + "Id_onchange();")}
	catch(e){;}
}

function imgBuscClieDeriv_click(pCriador, strId,pstrParams,pstrSession,pstrEstilo,pstrPagina)
{
	var sFiltro = "";
	sFiltro += "ctrlId=" + strId + "&" + pstrParams;
	mbooParam = false;
	sFiltro += agregarParam(strId, "txtApel", "apel");
	sFiltro += agregarParam(strId, "txtNomb", "nomb");
	sFiltro += agregarParam(strId, "txtSociNume", "SociNume");
	sFiltro += agregarParam(strId, "cmbInse", "Inse");
	sFiltro += agregarParam(strId, "txtExpo", "Expo");
	sFiltro += agregarParam(strId, "txtClieNume", "ClieNume");	
	if (document.all(strId+':cmbRazaCria') != null && document.all(strId+':cmbRazaCria').value != '')
		sFiltro += agregarParam(strId, "cmbRazaCria", "Raza");
	else	
		sFiltro += agregarParam(strId, "cmbCriaNumeRaza", "Raza");
	if (pCriador == 'S')
		sFiltro += agregarParam(strId, "txtCodi", "CriaNume");
	else
		sFiltro += agregarParam(strId, "txtCriaNume", "CriaNume");
	sFiltro += agregarParam(strId, "txtCUIT", "Cuit");
	sFiltro += agregarParam(strId, "cmbDocuNumeTido", "DocuTipo");
	sFiltro += agregarParam(strId, "txtDocuNume", "DocuNume");
	sFiltro += agregarParam(strId, "cmbTarjTipo", "TarjTipo");
	sFiltro += agregarParam(strId, "txtTarjNume", "TarjNume");
	sFiltro += agregarParam(strId, "cmbEntidad", "Enti");
	sFiltro += agregarParam(strId, "cmbMedioPago", "MedioPago");
	sFiltro += agregarParam(strId, "chkAgrupa", "Agru");
	sFiltro += agregarParam(strId, "cmbEmpresa", "Empr");
	sFiltro += agregarParam(strId, "txtClave", "ClaveUnica");
   	sFiltro += agregarParam(strId, "txtCodi", "CodiNume");
	sFiltro += agregarParam(strId, "txtNoCriaNume", "NoCriaNume");
	sFiltro += agregarParam(strId, "txtLegaNume", "LegaNume");
	sFiltro += agregarParam(strId, "txtId", "ValorId");
	sFiltro += agregarParam(strId, "cmbPaisFil", "FilPais");
    sFiltro += agregarParam(strId, "cmbProvFil", "Filprov");

	if (document.all(strId+':chkAgrupa')!=null)
		document.all(strId+':chkAgrupa').checked = true;

	var sRet="";

	if(mbooParam && document.all(strId + ":txtId").value == "")
		sRet=EjecutarMetodoXML("BuscarClieDerivCons", pstrPagina + ";" + sFiltro);

	if(sRet!="")
	{
		if (pCriador == "S")
		{
			vsRet=sRet.split("|");
			document.all(strId + ":txtCodi").value = vsRet[0];
			document.all(strId + ":txtApel").value = vsRet[1];
			if (!document.all[strId + ":cmbRazaCria"].disabled)
				gBuscarTextoCombo (vsRet[2],document.all(strId + ":cmbRazaCria"));
			document.all(strId + ":txtId").value = vsRet[3];
		}
		else
		{
			document.all(strId + ":txtCodi").value = sRet;
			document.all(strId + ":txtCodi").onchange();
		}
	}
	else
	{	
		Ventana = 1;
		gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);

		window.attachEvent('onfocus',window_onfocus);
		window.document.body.attachEvent('onfocus',window_onfocus);
	}
	if (pCriador == "S") 
	{
	   try{eval(strId + "Id_onchange();")}
       catch(e){};
    }
}

/*Nueva funcion que utiliza los nuevos controles*/
function imgBuscClieDerivNew_click(pCriador, strId,pstrParams,pstrSession,pstrEstilo,pstrPagina)
{
	var sFiltro = "";
	sFiltro += "ctrlId=" + strId + "&" + pstrParams;
	mbooParam = false;
	sFiltro += agregarParam(strId, "txtApel", "apel");
	sFiltro += agregarParam(strId, "txtNomb", "nomb");
	sFiltro += agregarParam(strId, "txtSociNume", "SociNume");
	sFiltro += agregarParam(strId, "cmbInse", "Inse");
	sFiltro += agregarParam(strId, "txtExpo", "Expo");
	sFiltro += agregarParam(strId, "txtClieNume", "ClieNume");	
	if (document.all(strId+':cmbRazaCria') != null && document.all(strId+':cmbRazaCria').value != '')
		sFiltro += agregarParam(strId, "cmbRazaCria", "Raza");
	else	
		sFiltro += agregarParam(strId, "cmbCriaNumeRaza", "Raza");
	if (pCriador == 'S')
		sFiltro += agregarParam(strId, "txtCodi", "CriaNume");
	else
		sFiltro += agregarParam(strId, "txtCriaNume", "CriaNume");
	sFiltro += agregarParam(strId, "txtCUIT", "Cuit");
	sFiltro += agregarParam(strId, "cmbDocuNumeTido", "DocuTipo");
	sFiltro += agregarParam(strId, "txtDocuNume", "DocuNume");
	sFiltro += agregarParam(strId, "cmbTarjTipo", "TarjTipo");
	sFiltro += agregarParam(strId, "txtTarjNume", "TarjNume");
	sFiltro += agregarParam(strId, "cmbEntidad", "Enti");
	sFiltro += agregarParam(strId, "cmbMedioPago", "MedioPago");
	sFiltro += agregarParam(strId, "chkAgrupa", "Agru");
	sFiltro += agregarParam(strId, "cmbEmpresa", "Empr");
	sFiltro += agregarParam(strId, "txtClave", "ClaveUnica");
   	sFiltro += agregarParam(strId, "txtCodi", "CodiNume");
	sFiltro += agregarParam(strId, "txtNoCriaNume", "NoCriaNume");
	sFiltro += agregarParam(strId, "txtLegaNume", "LegaNume");
	sFiltro += agregarParam(strId, "txtId", "ValorId");
	sFiltro += agregarParam(strId, "cmbPaisFil", "FilPais");
    sFiltro += agregarParam(strId, "cmbProvFil", "Filprov");
    
	if (document.all(strId+':chkAgrupa')!=null)
		document.all(strId+':chkAgrupa').checked = true;

	var sRet="";

	if(mbooParam && document.all(strId + ":txtId").value == "")
		sRet=EjecutarMetodoXML("BuscarClieDerivCons", pstrPagina + ";" + sFiltro);

	if(sRet!="")
	{
		if (pCriador == "S")
		{
			vsRet=sRet.split("|");
			document.all(strId + ":txtCodi").value = vsRet[0];
			document.all(strId + ":txtApel").value = vsRet[1];
			if (!document.all[strId + ":cmbRazaCria"].disabled)
				gBuscarTextoCombo (vsRet[2],document.all(strId + ":cmbRazaCria"));
			document.all(strId + ":txtId").value = vsRet[3];
		}
		else
		{
			document.all(strId + ":txtCodi").value = sRet;
			document.all(strId + ":txtCodi").onchange();
		}
	}
	else
	{	
		Ventana = 1;
		gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);

		window.attachEvent('onfocus',window_onfocus);
		window.document.body.attachEvent('onfocus',window_onfocus);
	}
	if (pCriador == "S") 
	{
	   try{eval(strId + "Id_onchange();")}
       catch(e){};
    }
}

function imgLimpClieDeriv_click(strId,strOpc)
{
	if (strOpc != null)
		document.all(strId + ":txtCodi").value = "-1";
	else
		document.all(strId + ":txtCodi").value = "";
	document.all(strId + ":txtCodi").onchange();
    
	limpiarParam(strId, "txtSociNume");
	limpiarParam(strId, "txtExpo");
	limpiarParam(strId, "txtClieNume");
	limpiarParam(strId, "cmbCriaNumeRaza");
	limpiarParam("txt" + strId, "cmbCriaNumeRaza");
	limpiarParam(strId, "txtCriaNume");
	limpiarParam(strId, "txtCUIT");
	limpiarParam(strId, "cmbDocuNumeTido");
	limpiarParam(strId, "cmbTarjTipo");
	limpiarParam(strId, "txtTarjNume");
	limpiarParam(strId, "cmbEntidad");
	limpiarParam(strId, "cmbMedioPago");
	//limpiarParam(strId, "chkAgrupa");
	if (document.all('chkAgrupa')!=null)
		document.all('chkAgrupa').checked = true;
	limpiarParam(strId, "cmbEmpresa");
	limpiarParam(strId, "txtClave");
	limpiarParam(strId, "txtNoCriaNume");
	limpiarParam(strId, "txtLegaNume");
}

function imgLimpClieDerivNew_click(strId,strOpc)
{
	if (strOpc != null)
		document.all(strId + ":txtCodi").value = "-1";
	else
		document.all(strId + ":txtCodi").value = "";
	document.all(strId + ":txtCodi").onchange();
    
	limpiarParam(strId, "txtSociNume");
	limpiarParam(strId, "txtExpo");
	limpiarParam(strId, "txtClieNume");
	limpiarParam(strId, "cmbCriaNumeRaza");
	limpiarParam("txt" + strId, "cmbCriaNumeRaza");
	limpiarParam(strId, "txtCriaNume");
	limpiarParam(strId, "txtCUIT");
	limpiarParam(strId, "cmbDocuNumeTido");
	limpiarParam(strId, "cmbTarjTipo");
	limpiarParam(strId, "txtTarjNume");
	limpiarParam(strId, "cmbEntidad");
	limpiarParam(strId, "cmbMedioPago");
	//limpiarParam(strId, "chkAgrupa");
	if (document.all('chkAgrupa')!=null)
		document.all('chkAgrupa').checked = true;
	limpiarParam(strId, "cmbEmpresa");
	limpiarParam(strId, "txtClave");
	limpiarParam(strId, "txtNoCriaNume");
	limpiarParam(strId, "txtLegaNume");
}

function imgLimpCriador_click(strId,strOpc)
{
	if (strOpc != null)
		document.all(strId + ":txtCodi").value = "-1";
	else
		document.all(strId + ":txtCodi").value = "";
	document.all(strId + ":txtCodi").onchange();
    
	limpiarParam(strId, "txtSociNume");
	limpiarParam(strId, "txtExpo");
	limpiarParam(strId, "txtClieNume");
	//limpiarParam(strId, "cmbCriaNumeRaza");
	//limpiarParam("txt" + strId, "cmbCriaNumeRaza");
	limpiarParam(strId, "txtCriaNume");
	limpiarParam(strId, "txtCUIT");
	limpiarParam(strId, "cmbDocuNumeTido");
	limpiarParam(strId, "cmbTarjTipo");
	limpiarParam(strId, "txtTarjNume");
	limpiarParam(strId, "cmbEntidad");
	limpiarParam(strId, "cmbMedioPago");
	//limpiarParam(strId, "chkAgrupa");
	if (document.all('chkAgrupa')!=null)
		document.all('chkAgrupa').checked = true;
	limpiarParam(strId, "cmbEmpresa");
	limpiarParam(strId, "txtClave");
	limpiarParam(strId, "txtNoCriaNume");
	limpiarParam(strId, "txtLegaNume");
}

/***** CONTROL PRODUCTOS *****/
function GenerarEventos(strId,pTipo)
{
	if (pTipo != null)
	{
		try
		{
			eval(strId + "_onchange('" + pTipo + "');");
			return;
		}
		catch(e){;}
	}
	
	try{eval(strId + "_onchange();")}
	catch(e){;}
}

function CodiProdDeriv_change(strId, strAutoPB, strArgs, strCampoVal,pstrAsociId,pTipo)
{
	if (document.all[strId + ":cmbProdRaza"].value!="" && document.all[strId + ":txtSexoId"].value!="" && (document.all[strId + ":txtSraNume"].value!="" || (document.all[strId + ":txtRP"]!= null && document.all[strId + ":txtRP"].value!="")))
		//if (document.all[strId + ":txtRP"]!= null && document.all[strId + ":txtRP"].value!="")
			BuscarProducto(strId, strAutoPB, strArgs, strCampoVal,pstrAsociId,pTipo);
		//else
		//	BuscarProducto(strId, strAutoPB, strArgs, strCampoVal,pstrAsociId,pTipo);
	
	//if (document.all[strId + ":txtRP"]!= null)
	//	if (document.all[strId + ":cmbProdRaza"].value!="" && document.all[strId + ":txtSexoId"].value!="" && document.all[strId + ":txtRP"].value!="")
	//		BuscarProducto(strId, strAutoPB, strArgs, strCampoVal,pstrAsociId,pTipo);

	if (document.all[strId + ":cmbProdRaza"].value=="" && document.all[strId + ":txtSexoId"].value=="" && document.all[strId + ":txtSraNume"].value=="")
		imgLimpProdDeriv_click(strId);
}

// Control usrProducto (Raza - Criador/Propietario)
function CodiProducto_change(strId, strAutoPB, strArgs, strCampoVal, pstrAsociId, pTipo, pstrParams, pstrSession, pstrEstilo, pstrPagina)
{
	//$.blockUI({ message: '<h1><img src="imagenes/loader.gif" /> Espere por favor...</h1>' });
	//Verifico que est�n completos los campos: Raza, Sexo, (HBA y/o RP).
	if (document.all(strId + ":usrCriadorFil:cmbRazaCria").value != "" && document.all[strId + ":txtSexoId"].value != "" && (document.all[strId + ":txtSraNume"].value != "" || (document.all[strId + ":txtRP"]!= null && document.all[strId + ":txtRP"].value != "")))
			BuscarProducto_usrProducto(strId, strAutoPB, strArgs, strCampoVal, pstrAsociId, pTipo, pstrParams, pstrSession, pstrEstilo, pstrPagina); //Busco el Producto.
	
	//Verifico si est�n vac�os los campos: Raza, Sexo, HBA.		
	if (document.all(strId + ":usrCriadorFil:cmbRazaCria").value == "" && document.all[strId + ":txtSexoId"].value == "" && document.all[strId + ":txtSraNume"].value == "")
		imgLimpProducto_click(strId); //Limpio el control de Productos.
	//$.unblockUI();
}

// Control usrProductoExtranjero (Raza)
function CodiProductoExtranjero_change(strId, strAutoPB, strArgs, strCampoVal, pstrAsociId, pTipo, pstrParams, pstrSession, pstrEstilo, pstrPagina)
{
	// Dario 2013-12-19 se agrega la leyenda espere por favor 
	//document.all(strId + "_DivEsperePorFavor").innerText = "Espere por favor...";
	//$("DivEsperePorFavor").innerHTML = "<td colspan='2'><b>Espere por favor...</b></td>";
	//$.blockUI({ message: '<h1><img src="imagenes/loader.gif" /> Espere por favor...</h1>' });
	//Verifico que est�n completos los campos: Raza, Sexo, (NumeExtr y/o RPExtr).
	if (document.all(strId + ":cmbProdRaza").value != "" && document.all[strId + ":txtSexoId"].value != "" && (document.all[strId + ":txtCodi"].value != "" || (document.all[strId + ":txtRPExtr"]!= null && document.all[strId + ":txtRPExtr"].value != "")))
	{
		BuscarProducto_usrProductoExtranjero(strId, strAutoPB, strArgs, strCampoVal, pstrAsociId, pTipo, pstrParams, pstrSession, pstrEstilo, pstrPagina); //Busco el Producto.
	}
	
	//Verifico si est�n vac�os los campos: Raza, Sexo, NumeExtr.
	if (document.all(strId + ":cmbProdRaza").value == "" && document.all[strId + ":txtSexoId"].value == "" && document.all[strId + ":txtCodi"].value == "")
		imgLimpProducto_click(strId); //Limpio el control de Productos.
		
	// Dario 2013-12-19 se agrega la leyenda espere por favor 
	//document.all(strId + "_DivEsperePorFavor").innerText = "";
	//$(strId + "_DivEsperePorFavor").innerText = "";
	//$.unblockUI();
}

// Control usrProducto (Raza - Criador/Propietario)
function BuscarProducto_usrProducto(strId, strAutoPB, strArgs, strCampoVal, pstrAsociId, pTipo, pstrParams, pstrSession, pstrEstilo, pstrPagina)
{
	var oCoti = document.all(strId + ":txtCodi");
	var oNume = document.all(strId + ":txtSraNume");
	var oRP = document.all[strId + ":txtRP"];
	var sFiltro = "";
	var sFiltroSinCriador = "";
	var sIdOri = document.all(strId + ":txtId").value;
	
	var opc = oCoti.value;
	if (opc == -1)
		oCoti.value=0;

	var opc = oNume.value;
	if (opc == -1)
		oNume.value=0;
				
	sFiltro = oNume.value;
	sFiltroSinCriador = oNume.value;

	if (!booIsNumber(oCoti.value) || !booIsNumber(oNume.value) || (oCoti.value=="" && oNume.value=="" && oRP.value==""))
	{
		if (document.all(strId + ":usrCriadorFil:cmbRazaCria") != null) //&& !document.all[strId + ":cmbProdRaza"].disabled)
		{	
			if (opc != -1)
			{
				document.all(strId + ":usrCriadorFil:cmbRazaCria").value = "";
				document.all(strId + ":usrCriadorFil:cmbRazaCria").onchange();
			}
        }
        if (document.all[strId + ":cmbProdAsoc"] != null)
		{
			var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
			document.all(strId + ":cmbProdAsoc").value=lstrCons;
		}
		if (document.all[strId + ":txtCodi"]!= null)
			document.all(strId + ":txtCodi").value = "";
		if (document.all[strId + ":txtRP"]!= null)
			document.all(strId + ":txtRP").value = "";
		if (document.all[strId + ":txtProdNomb"]!= null)
			document.all(strId + ":txtProdNomb").value = "";
		if (document.all[strId + ":txtDesc"]!= null)
			document.all(strId + ":txtDesc").value = "";
		if (document.all[strId + ":txtSraNume"]!= null)
			document.all(strId + ":txtSraNume").value = "";
		if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
			document.all("txt"+strId+":cmbProdAsoc").value = "";
		document.all(strId + ":txtId").value = "";
						
		if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
	}
	else	
	{
		if (document.all(strId + ":usrCriadorFil:cmbRazaCria").value != "" && document.all(strId + ":txtSexoId").value != "")
		{
			//Si tiene ID de Producto, lo busco directamente por el prdt_id.
			//De lo contrario, busco por los filtros que tengo.
			if (sIdOri != "")
			{
				sFiltro +=";productos_control";
				sFiltro +=";prdt_id";
				sFiltro +=";" + sIdOri;
			}
			else
			{
				sFiltro += ";" + strArgs;
				sFiltro += ";" + document.all(strId + ":usrCriadorFil:cmbRazaCria").value;
				sFiltro += ";" + document.all[strId + ":txtSexoId"].value;
				sFiltro += ";" + document.all[strId + ":txtCodi"].value;
				sFiltro += ";" + document.all[strId + ":cmbProdAsoc"].value;
				if (document.all[strId + ":txtRP"]!= null)
					sFiltro += ";" + document.all[strId + ":txtRP"].value;
				sFiltro += ";" + document.all(strId + ":usrCriadorFil:txtId").value;
				/*if (document.all["hdnFiltroProdCriaId"]!= null)
					sFiltro += document.all["hdnFiltroProdCriaId"].value + ";"*/
				sFiltro += ";";	
				
				if (document.all(strId + ":usrCriadorFil:txtId").value != null)
				{
					sFiltroSinCriador += ";" + strArgs;
					sFiltroSinCriador += ";" + document.all(strId + ":usrCriadorFil:cmbRazaCria").value;
					sFiltroSinCriador += ";" + document.all[strId + ":txtSexoId"].value;
					sFiltroSinCriador += ";" + document.all[strId + ":txtCodi"].value;
					sFiltroSinCriador += ";" + document.all[strId + ":cmbProdAsoc"].value;
					if (document.all[strId + ":txtRP"]!= null)
						sFiltroSinCriador += ";" + document.all[strId + ":txtRP"].value;
						sFiltroSinCriador += ";" + "0";
					sFiltroSinCriador += ";";	
				
				}
				
					
			}	
			var sRet=EjecutarMetodoXML("Utiles.BuscarProdDeriv", sFiltro);

			if(sRet=="")
			{
				if (document.all[strId + ":txtIgnoraInexist"].value != "1" && pTipo != "asoc_nume")
				{
					if (document.all(strId + ':hdnValiExis').value != 0)
					{
						var sRetSinCriador=EjecutarMetodoXML("Utiles.BuscarProdDeriv", sFiltroSinCriador);
					
						if(sRetSinCriador=="")
						{
								alert(strCampoVal + " inexistente.");
						}
						else
						{
								alert("Animal con otro propietario");
						}
						
						if (document.all(strId + ":usrCriadorFil:cmbRazaCria") != null) //&& !document.all[strId + ":cmbProdRaza"].disabled)
						{	
							document.all(strId + ":usrCriadorFil:cmbRazaCria").value = "";
							document.all(strId + ":usrCriadorFil:cmbRazaCria").onchange();
						}
						if (document.all[strId + ":cmbProdAsoc"]!= null)
						{
							var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
							document.all(strId + ":cmbProdAsoc").value=lstrCons;
						}
						if (document.all[strId + ":cmbProdSexo"]!= null && !document.all[strId + ":cmbProdSexo"].disabled)
						{
							document.all(strId + ":cmbProdSexo").value = "";
							document.all(strId + ":cmbProdSexo").onchange();
						}
						if (document.all[strId + ":txtCodi"]!= null)
							document.all(strId + ":txtCodi").value = "";
						if (document.all[strId + ":txtRP"]!= null)
							document.all(strId + ":txtRP").value = "";
						if (document.all[strId + ":txtSraNume"]!= null)
							document.all(strId + ":txtSraNume").value = "";
						if (document.all[strId + ":txtProdNomb"]!= null)
							document.all(strId + ":txtProdNomb").value = "";
						if (document.all[strId + ":txtDesc"]!= null)
							document.all(strId + ":txtDesc").value = "";
						if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
							document.all("txt"+strId+":cmbProdAsoc").value = "";
						document.all(strId + ":txtId").value = "";
						
						if (pTipo == "sra_nume")
							oNume.focus();
						else
							oCoti.focus();
						
						if(strAutoPB=="1" && sIdOri!="")
							__doPostBack(strId + ":txtId",'');
					}
					else
					{
						if (document.all[strId + ":txtId"]!= null)
							document.all(strId + ":txtId").value = "";
						if (document.all[strId + ":txtProdNomb"]!= null)
							document.all(strId + ":txtProdNomb").value = "---- Error: Producto inexistente ----";
						if (document.all[strId + ":txtDesc"]!= null)
							document.all(strId + ":txtDesc").value = "";
					}
				}
			}
			else
			{	
				if (sRet == "AbrirPopupPaginaProductos")
					imgBuscProducto_click(strId, pstrParams, pstrSession, pstrEstilo, pstrPagina, true)
				else
				{			
					var vsRet=sRet.split("|");
					
					document.all(strId + ":txtId").value = vsRet[0];
					if (document.all[strId + ":cmbProdSexo"]!= null)
						if (vsRet[8] == "True")
							document.all[strId + ":cmbProdSexo"].value = 1;
						else
							document.all[strId + ":cmbProdSexo"].value = 0;
					if (document.all[strId + ":txtRP"]!= null)
						document.all(strId + ":txtRP").value = vsRet[6];	
					if (document.all[strId + ":txtProdNomb"]!= null)
						document.all(strId + ":txtProdNomb").value = vsRet[3];
					if (document.all[strId + ":txtDesc"]!= null)
						document.all(strId + ":txtDesc").value = vsRet[5];
					if (document.all[strId + ":txtSraNume"]!= null)
						document.all(strId + ":txtSraNume").value = vsRet[9];

					if (pTipo == "sra_nume")
					{
						if (document.all[strId + ":txtCodi"]!= null)
							document.all(strId + ":txtCodi").value = vsRet[2];						
						if (document.all[strId + ":cmbProdAsoc"]!= null)
						{
							gBuscarCombo(vsRet[7], document.all[strId + ":cmbProdAsoc"]);
							document.all['txt'+strId+":cmbProdAsoc"].value = LeerCamposXML("asociaciones", vsRet[7], "asoc_codi");
						}	
					}	
					else
					{
						if (document.all[strId + ":txtSraNume"]!= null)
							document.all(strId + ":txtSraNume").value = vsRet[9];
					}
					
					if(strAutoPB=="1")
						__doPostBack(strId + ":txtId",'');
				}
			}
		}
	}
	
	if (document.getElementById(strId + ":txtProdNomb").value!="")
	{
		var range = document.selection.createRange();
		document.getElementById(strId + ":txtProdNomb").focus();
		if (window.event!=null)
			window.event.returnValue = false;
	}
	
	GenerarEventos(strId,pTipo);
}

// Control usrProductoExtranjero (Raza)
function BuscarProducto_usrProductoExtranjero(strId, strAutoPB, strArgs, strCampoVal, pstrAsociId, pTipo, pstrParams, pstrSession, pstrEstilo, pstrPagina)
{
	var oCoti = document.all(strId + ":txtCodi");
	var oRPExtr = document.all[strId + ":txtRPExtr"];
	var sFiltro = "";
	var sIdOri = document.all(strId + ":txtId").value;
	
	var opc = oCoti.value;
	if (opc == -1)
		oCoti.value=0;
		
	var opc = oRPExtr.value;
	if (opc == -1)
		oRPExtr.value=0;
			
	if (!booIsNumber(oCoti.value) || (oCoti.value=="" && oRPExtr.value==""))
	{
        if (document.all[strId + ":cmbProdRaza"]!= null && !document.all[strId + ":cmbProdRaza"].disabled)
		{	
			if (opc != -1)
			{
				document.all(strId + ":cmbProdRaza").value = document.all(strId + ":txtRazaSessId").value;
				document.all(strId + ":cmbProdRaza").onchange();
			}
        }
        if (document.all[strId + ":cmbProdAsoc"] != null)
		{
			var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
			document.all(strId + ":cmbProdAsoc").value=lstrCons;
		}
		if (document.all[strId + ":txtCodi"]!= null)
			document.all(strId + ":txtCodi").value = "";
		if (document.all[strId + ":txtRPExtr"]!= null)
			document.all(strId + ":txtRPExtr").value = "";
		if (document.all[strId + ":txtProdNomb"]!= null)
			document.all(strId + ":txtProdNomb").value = "";
		if (document.all[strId + ":txtDesc"]!= null)
			document.all(strId + ":txtDesc").value = "";
		if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
			document.all("txt"+strId+":cmbProdAsoc").value = "";
		document.all(strId + ":txtId").value = "";
						
		if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
	}
	else	
	{
		if (document.all(strId + ":cmbProdRaza").value != "" && document.all(strId + ":txtSexoId").value != "")
		{
			//Si tiene ID de Producto, lo busco directamente por el prdt_id.
			//De lo contrario, busco por los filtros que tengo.
			if (sIdOri != "")
			{
				sFiltro +=";productos_control";
				sFiltro +=";prdt_id";
				sFiltro +=";" + sIdOri;
			}
			else
			{
				sFiltro += ";" + strArgs;
				sFiltro += ";" + document.all(strId + ":cmbProdRaza").value;
				sFiltro += ";" + document.all[strId + ":txtSexoId"].value;
				sFiltro += ";" + document.all[strId + ":txtCodi"].value;
				sFiltro += ";" + document.all[strId + ":cmbProdAsoc"].value;
				sFiltro += ";";
				sFiltro += ";";
				sFiltro += ";";
				if (document.all[strId + ":txtRPExtr"]!= null)
					sFiltro += document.all[strId + ":txtRPExtr"].value + ";";
			}	
			var sRet=EjecutarMetodoXML("Utiles.BuscarProdDeriv", sFiltro);
			
			if(sRet=="")
			{
				if (document.all[strId + ":txtIgnoraInexist"].value != "1" && pTipo != "asoc_nume")
				{
					if (document.all(strId + ':hdnValiExis').value != 0)
					{
						alert(strCampoVal + " inexistente.")
						
						if (document.all(strId + ":cmbProdRaza") != null && !document.all[strId + ":cmbProdRaza"].disabled)
						{	
							document.all(strId + ":cmbProdRaza").value = "";
							document.all(strId + ":cmbProdRaza").onchange();
						}
						if (document.all[strId + ":cmbProdAsoc"]!= null)
						{
							var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
							document.all(strId + ":cmbProdAsoc").value=lstrCons;
						}
						if (document.all[strId + ":cmbProdSexo"]!= null && !document.all[strId + ":cmbProdSexo"].disabled)
						{
							document.all(strId + ":cmbProdSexo").value = "";
							document.all(strId + ":cmbProdSexo").onchange();
						}
						if (document.all[strId + ":txtCodi"]!= null)
							document.all(strId + ":txtCodi").value = "";
						if (document.all[strId + ":txtRPExtr"]!= null)
							document.all(strId + ":txtRPExtr").value = "";
						if (document.all[strId + ":txtProdNomb"]!= null)
							document.all(strId + ":txtProdNomb").value = "";
						if (document.all[strId + ":txtDesc"]!= null)
							document.all(strId + ":txtDesc").value = "";
						if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
							document.all("txt"+strId+":cmbProdAsoc").value = "";
						document.all(strId + ":txtId").value = "";
						
						if (pTipo == "sra_nume")
							oRPExtr.focus();
						else
							oCoti.focus();
						
						if(strAutoPB=="1" && sIdOri!="")
							__doPostBack(strId + ":txtId",'');
					}
					else
					{
						if (document.all[strId + ":txtId"]!= null)
							document.all(strId + ":txtId").value = "";
						if (document.all[strId + ":txtProdNomb"]!= null)
							document.all(strId + ":txtProdNomb").value = "---- Error: Producto inexistente ----";
						if (document.all[strId + ":txtDesc"]!= null)
							document.all(strId + ":txtDesc").value = "";
					}
				}
			}
			else
			{
				if (sRet == "AbrirPopupPaginaProductos")
					imgBuscProductoExtranjero_click(strId, pstrParams, pstrSession, pstrEstilo, pstrPagina, true)
				else
				{					
					var vsRet=sRet.split("|");
					
					//Producto ID.			
					document.all(strId + ":txtId").value = vsRet[0];
					//Sexo.
					if (document.all[strId + ":cmbProdSexo"]!= null)
						if (vsRet[8] == "True")
							document.all[strId + ":cmbProdSexo"].value = 1;
						else
							document.all[strId + ":cmbProdSexo"].value = 0;
					//RP Extranjero.
					if (document.all[strId + ":txtRPExtr"]!= null)
						document.all(strId + ":txtRPExtr").value = vsRet[11];
						
					//Nombre.
					if (document.all[strId + ":txtProdNomb"]!= null)
						document.all(strId + ":txtProdNomb").value = vsRet[3];
					//Producto Descripci�n.
					if (document.all[strId + ":txtDesc"]!= null)
						document.all(strId + ":txtDesc").value = 'HBA: ' + vsRet[9] + ' **  '+	vsRet[5];
					//Nro. Extranjero.
					if (document.all[strId + ":txtCodi"]!= null)
						document.all(strId + ":txtCodi").value = vsRet[2];
					//Asociaci�n.
					if (document.all[strId + ":cmbProdAsoc"]!= null)
					{
						gBuscarCombo(vsRet[7], document.all[strId + ":cmbProdAsoc"]);
						document.all['txt'+strId+":cmbProdAsoc"].value = LeerCamposXML("asociaciones", vsRet[7], "asoc_codi");
					}
					
					if(strAutoPB=="1")
						__doPostBack(strId + ":txtId",'');
				}
			}
		}
	}
	
	if (document.getElementById(strId + ":txtProdNomb").value!="")
	{
		var range = document.selection.createRange();
		document.getElementById(strId + ":txtProdNomb").focus();
		if (window.event!=null)
			window.event.returnValue = false;
	}
	
	GenerarEventos(strId,pTipo);
}

function BuscarProducto(strId, strAutoPB, strArgs, strCampoVal,pstrAsociId,pTipo)
{
	var oCoti = document.all(strId + ":txtCodi");
	var oNume = document.all(strId + ":txtSraNume");
	var oRP = document.all[strId + ":txtRP"];
	var sFiltro = "";
	var sIdOri = document.all(strId + ":txtId").value;
	
	var opc = oCoti.value;
	if (opc == -1)
		oCoti.value=0;

	var opc = oNume.value;
	if (opc == -1)
		oNume.value=0;
				
	sFiltro = oNume.value;

	if (!booIsNumber(oCoti.value) || !booIsNumber(oNume.value) || (oCoti.value=="" && oNume.value=="" && oRP.value==""))
	{
		if (document.all[strId + ":cmbProdRaza"]!= null && !document.all[strId + ":cmbProdRaza"].disabled)
		{	
			if (opc != -1)
			{
				document.all(strId + ":cmbProdRaza").value = document.all(strId + ":txtRazaSessId").value;
				document.all(strId + ":cmbProdRaza").onchange();
			}
        }
        if (document.all[strId + ":cmbProdAsoc"]!= null)
		{
			var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
			document.all(strId + ":cmbProdAsoc").value=lstrCons;
		}
		if (document.all[strId + ":txtCodi"]!= null)
			document.all(strId + ":txtCodi").value = "";
		if (document.all[strId + ":txtRP"]!= null)
			document.all(strId + ":txtRP").value = "";
		if (document.all[strId + ":txtProdNomb"]!= null)
			document.all(strId + ":txtProdNomb").value = "";
		if (document.all[strId + ":txtDesc"]!= null)
			document.all(strId + ":txtDesc").value = "";
		if (document.all[strId + ":txtSraNume"]!= null)
			document.all(strId + ":txtSraNume").value = "";
		if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
			document.all("txt"+strId+":cmbProdAsoc").value = "";
		document.all(strId + ":txtId").value = "";
						
		if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
	}
	else	
	{
		if (document.all(strId + ":cmbProdRaza").value!="" && document.all(strId + ":txtSexoId").value!="")
		{
			sFiltro += ";" + strArgs;
			sFiltro += ";" + document.all[strId + ":cmbProdRaza"].value;
			sFiltro += ";" + document.all[strId + ":txtSexoId"].value;
			sFiltro += ";" + document.all[strId + ":txtCodi"].value;
			sFiltro += ";" + document.all[strId + ":cmbProdAsoc"].value;
			sFiltro += ";";
			if (document.all[strId + ":txtRP"]!= null)
				sFiltro += document.all[strId + ":txtRP"].value + ";";
			if (document.all["hdnFiltroProdCriaId"]!= null)
				sFiltro += document.all["hdnFiltroProdCriaId"].value + ";"
			var sRet=EjecutarMetodoXML("Utiles.BuscarProdDeriv", sFiltro);

			if(sRet=="")
			{
				if (document.all[strId + ":txtIgnoraInexist"].value != "1" && pTipo != "asoc_nume")
				{
				    if (document.all(strId + ':hdnValiExis').value != 0)
					{
						alert(strCampoVal + " inexistente.")
						if (document.all[strId + ":cmbProdRaza"]!= null && !document.all[strId + ":cmbProdRaza"].disabled)
						{	
							document.all(strId + ":cmbProdRaza").value = document.all(strId + ":txtRazaSessId").value;
							document.all(strId + ":cmbProdRaza").onchange();
						}
						if (document.all[strId + ":cmbProdAsoc"]!= null)
						{
							var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
							document.all(strId + ":cmbProdAsoc").value=lstrCons;
						}
						if (document.all[strId + ":cmbProdSexo"]!= null)
						{
							document.all(strId + ":cmbProdSexo").value = "";
							document.all(strId + ":cmbProdSexo").onchange();
						}
						if (document.all[strId + ":txtCodi"]!= null)
							document.all(strId + ":txtCodi").value = "";
						if (document.all[strId + ":txtRP"]!= null)
							document.all(strId + ":txtRP").value = "";
						if (document.all[strId + ":txtSraNume"]!= null)
							document.all(strId + ":txtSraNume").value = "";
						if (document.all[strId + ":txtProdNomb"]!= null)
							document.all(strId + ":txtProdNomb").value = "";
						if (document.all[strId + ":txtDesc"]!= null)
							document.all(strId + ":txtDesc").value = "";
						if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
							document.all("txt"+strId+":cmbProdAsoc").value = "";
						document.all(strId + ":txtId").value = "";
						
						if (pTipo == "sra_nume")
							oNume.focus();
						else
							oCoti.focus();
						
						if(strAutoPB=="1" && sIdOri!="")
							__doPostBack(strId + ":txtId",'');
					}
					else
					{
						if (document.all[strId + ":txtId"]!= null)
							document.all(strId + ":txtId").value = "";
						if (document.all[strId + ":txtProdNomb"]!= null)
							document.all(strId + ":txtProdNomb").value = "---- Error: Producto inexistente ----";
						if (document.all[strId + ":txtDesc"]!= null)
							document.all(strId + ":txtDesc").value = "";
					}
				}
			}
			else
			{
				var vsRet=sRet.split("|");
				
				if (!document.all[strId + ":cmbProdRaza"].disabled && !document.all[strId + ":cmbProdRaza"].disabled)
				{
					document.all(strId + ":cmbProdRaza").value=vsRet[1];
					document.all(strId + ":cmbProdRaza").onchange();
				}
				document.all(strId + ":txtId").value = vsRet[0];
				if (document.all[strId + ":txtRP"]!= null)
					document.all(strId + ":txtRP").value = vsRet[6];	
				if (document.all[strId + ":txtProdNomb"]!= null)
					document.all(strId + ":txtProdNomb").value = vsRet[3];
				if (document.all[strId + ":txtDesc"]!= null)
					document.all(strId + ":txtDesc").value = 'HBA: ' + vsRet[9] + ' ** ' + vsRet[5];
				if (document.all[strId + ":txtSraNume"]!= null)
					document.all(strId + ":txtSraNume").value = vsRet[9];

				if (pTipo == "sra_nume")
				{
					if (document.all[strId + ":txtCodi"]!= null)
						document.all(strId + ":txtCodi").value = vsRet[2];						
					if (document.all[strId + ":cmbProdAsoc"]!= null)
					{
						gBuscarCombo(vsRet[7], document.all[strId + ":cmbProdAsoc"]);
						document.all['txt'+strId+":cmbProdAsoc"].value = LeerCamposXML("asociaciones", vsRet[7], "asoc_codi");
					}	
				}	
				else
				{
					if (document.all[strId + ":txtSraNume"]!= null)
						document.all(strId + ":txtSraNume").value = vsRet[9];
				}
				
				if(strAutoPB=="1")
					__doPostBack(strId + ":txtId",'');
			}
		}
	}
	
	if (document.getElementById(strId + ":txtProdNomb").value!="")
	{
		var range = document.selection.createRange();
		document.getElementById(strId + ":txtProdNomb").focus();
		if (window.event!=null)
			window.event.returnValue = false;
	}
	
	GenerarEventos(strId,pTipo);
}

function cmbProdSexo_change(pCtrl)
{
	var oper = pCtrl.id;
	var nomb = pCtrl.id.split("_");
	var valor = pCtrl.value;
	
	document.all(oper.replace("_cmbProdSexo","") + "_txtSexoId").value = pCtrl.value;
	
	if(document.all(nomb[nomb.length - 2] + ":txtId")!=null && document.all(nomb[nomb.length - 2] + ":txtId").value != "")
	{
		imgLimpProdDeriv_click(nomb[nomb.length - 2]);
		pCtrl.value = valor;
		pCtrl.onchange();
	}
	
	try{eval(oper + "_onchange('"+ oper +"');")}
	catch(e){;}
}

function cmbRaza_change(pCtrl)
{
	//$.blockUI({ message: '<h1><img src="imagenes/loader.gif" /> Espere por favor...</h1>' });
	var oper = pCtrl.id;
	var nomb = pCtrl.id.split("_");
	var valor = pCtrl.value;
	var ctrlCompleto = oper.replace(/_/gi,":"); //Reemplazo el "_" por ":" en toda la cadena.
	var lastChar = ctrlCompleto.lastIndexOf(":"); //Obtengo la posici�n del �ltimo ":".
	var control = ctrlCompleto.substring(0,lastChar); //Obtengo la ubicaci�n del control sin el combo desde donde se ejecut�.
	var sRet = LeerCamposXML("razas_especie", valor, "espe_nomb_nume");
	var firstChar = ctrlCompleto.indexOf(":"); //Obtengo la posici�n del primer ":".
	var ctrlById = ctrlCompleto.substring(0,firstChar); //Obtengo la ubicaci�n del control.
	var ctrlSraNume = ctrlById + "_lblSraNume";
	
	//Verifico que encuentre el label de SraNume.
	if (document.getElementById(ctrlSraNume) != null)
	{
		if (sRet != "")
	 		document.all(ctrlSraNume).innerText = sRet + ":"; //Le asigno el texto que corresponde dependiendo de la raza.
 		else
 			document.all(ctrlSraNume).innerText = "Nro.:"; //Le asigno el texto "Nro.:" si no encontr� la raza.
	}
	
	//Verifico que tenga datos el control.
	if(document.all(control + ":txtId")!=null && document.all(control + ":txtId").value != "")
	{
		if (pCtrl.id.indexOf("cmbProdRaza") >= 0)
			imgLimpProdDeriv_click(control);
		else
			imgLimpClieDeriv_click(control,'1');
		
		pCtrl.value = valor;
		pCtrl.onchange();
	}
	
	/*
	if(document.all(nomb[nomb.length - 2] + ":txtId")!=null && document.all(nomb[nomb.length - 2] + ":txtId").value != "")
	{
		if (pCtrl.id.indexOf("cmbProdRaza") >= 0)
			imgLimpProdDeriv_click(nomb[nomb.length - 2]);
		else
			imgLimpClieDeriv_click(nomb[nomb.length - 2],'1');
		
		pCtrl.value = valor;
		pCtrl.onchange();
	}
		
	try{eval(nomb[nomb.length - 1] + "_onchange('"+ oper +"');")}
	catch(e){;}*/
	
	var controlOnChange = control.replace(/:/gi,"_"); //Reemplazo el ":" por "_" en toda la cadena.
	try{eval(controlOnChange + "_onchange('"+ oper +"');")} //Ejecuto el OnChange del control.
	catch(e){;}
		
	try{eval(oper + "_onchange('"+ oper +"');")}
	catch(e){;}
	
	//$.unblockUI();
}


function Combo_change(pCtrl)
{
	try{eval(pCtrl.id + "_onchange('"+ pCtrl.id +"');")}
	catch(e){;}
}

function imgLimpProdDeriv_click(strId)
{
	document.all(strId + ":txtId").value = "";
	
	if (document.all[strId + ":cmbProdRaza"]!= null && !document.all[strId + ":cmbProdRaza"].disabled)
	{	
		document.all(strId + ":cmbProdRaza").value = document.all(strId + ":txtRazaSessId").value;
		document.all(strId + ":cmbProdRaza").onchange();
	}
	if (document.all[strId + ":cmbProdAsoc"]!= null)
	{
		var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
		document.all(strId + ":cmbProdAsoc").value=lstrCons;
	}
	if (document.all[strId + ":cmbProdSexo"]!= null)
	{
		document.all(strId + ":cmbProdSexo").value = "";
		document.all(strId + ":cmbProdSexo").onchange();
	}
	if (document.all[strId + ":txtCodi"]!= null)
		document.all(strId + ":txtCodi").value = "";
		
	if (document.all[strId + ":txtRP"]!= null)
		document.all(strId + ":txtRP").value = "";
		
	if (document.all[strId + ":txtSraNume"]!= null)
		document.all(strId + ":txtSraNume").value = "";
		
	if (document.all[strId + ":txtProdNomb"]!= null)
		document.all(strId + ":txtProdNomb").value = "";
		
	if (document.all[strId + ":txtDesc"]!= null)
		document.all(strId + ":txtDesc").value = "";
		
	if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
		document.all("txt"+strId+":cmbProdAsoc").value = "";

	try{document.all(strId + ":txtProdNomb").focus();}
	catch(e){;}

	GenerarEventos(strId);
 
}

// Control usrProducto (Raza - Criador/Propietario)
function imgLimpProducto_click(strId)
{
	document.all(strId + ":txtId").value = "";
	
	if (document.all("txt" + strId + ":usrCriadorFil:cmbRazaCria") != null)
	{	
		document.all("txt" + strId + ":usrCriadorFil:cmbRazaCria").value = "";
		document.all("txt" + strId + ":usrCriadorFil:cmbRazaCria").onchange();
	}
	if (document.all[strId + ":cmbProdAsoc"]!= null)
	{
		var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
		document.all(strId + ":cmbProdAsoc").value=lstrCons;
	}
	if (document.all[strId + ":cmbProdSexo"]!= null && !document.all[strId + ":cmbProdSexo"].disabled)
	{
		document.all(strId + ":cmbProdSexo").value = "";
		document.all(strId + ":cmbProdSexo").onchange();
	}
	if (document.all[strId + ":txtCodi"]!= null)
		document.all(strId + ":txtCodi").value = "";
		
	if (document.all[strId + ":txtRP"]!= null)
		document.all(strId + ":txtRP").value = "";
		
	if (document.all[strId + ":txtSraNume"]!= null)
		document.all(strId + ":txtSraNume").value = "";
		
	if (document.all[strId + ":txtProdNomb"]!= null)
		document.all(strId + ":txtProdNomb").value = "";
		
	if (document.all[strId + ":txtDesc"]!= null)
		document.all(strId + ":txtDesc").value = "";
		
	if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
		document.all("txt"+strId+":cmbProdAsoc").value = "";

	try{document.all("txt" + strId + ":usrCriadorFil:cmbRazaCria").focus();}
	catch(e){;}

	GenerarEventos(strId);
 
}

// Control usrProducto (Raza - Criador/Propietario)
function imgLimpProductoNew_click(strId)
{
	document.all(strId + ":txtId").value = "";
	
	if (document.all("txt" + strId + ":usrCriadorFil:cmbRazaCria") != null)
	{	
		document.all("txt" + strId + ":usrCriadorFil:cmbRazaCria").value = "";
		document.all("txt" + strId + ":usrCriadorFil:cmbRazaCria").onchange();
	}
	if (document.all[strId + ":cmbProdAsoc"]!= null)
	{
		var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
		document.all(strId + ":cmbProdAsoc").value=lstrCons;
	}
	if (document.all[strId + ":cmbProdSexo"]!= null && !document.all[strId + ":cmbProdSexo"].disabled)
	{
		document.all(strId + ":cmbProdSexo").value = "";
		document.all(strId + ":cmbProdSexo").onchange();
	}
	if (document.all[strId + ":txtCodi"]!= null)
		document.all(strId + ":txtCodi").value = "";
		
	if (document.all[strId + ":txtRP"]!= null)
		document.all(strId + ":txtRP").value = "";
		
	if (document.all[strId + ":txtSraNume"]!= null)
		document.all(strId + ":txtSraNume").value = "";
		
	if (document.all[strId + ":txtProdNomb"]!= null)
		document.all(strId + ":txtProdNomb").value = "";
		
	if (document.all[strId + ":txtDesc"]!= null)
		document.all(strId + ":txtDesc").value = "";
		
	if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
		document.all("txt"+strId+":cmbProdAsoc").value = "";

	try{document.all("txt" + strId + ":usrCriadorFil:cmbRazaCria").focus();}
	catch(e){;}
	GenerarEventos(strId);

}


// Control usrProductoExtranjero (Raza)
function imgLimpProductoExtranjero_click(strId)
{
	document.all(strId + ":txtId").value = "";
	if (document.all[strId + ":cmbProdRaza"]!= null && !document.all[strId + ":cmbProdRaza"].disabled)
	{	
		document.all(strId + ":cmbProdRaza").value = document.all(strId + ":txtRazaSessId").value;
		document.all(strId + ":cmbProdRaza").onchange();
	}
	if (document.all[strId + ":cmbProdAsoc"]!= null)
	{
		var lstrCons = LeerCamposXML("parametros", null, "para_sra_asoc_id");
		document.all(strId + ":cmbProdAsoc").value=lstrCons;
	}
	if (document.all[strId + ":cmbProdSexo"]!= null && !document.all[strId + ":cmbProdSexo"].disabled)
	{
		document.all(strId + ":cmbProdSexo").value = "";
		document.all(strId + ":cmbProdSexo").onchange();
	}
	if (document.all[strId + ":txtCodi"]!= null)
		document.all(strId + ":txtCodi").value = "";
		
	if (document.all[strId + ":txtRPExtr"]!= null)
		document.all(strId + ":txtRPExtr").value = "";
			
	if (document.all[strId + ":txtProdNomb"]!= null)
		document.all(strId + ":txtProdNomb").value = "";
		
	if (document.all[strId + ":txtDesc"]!= null)
		document.all(strId + ":txtDesc").value = "";
		
	if (document.all("txt"+strId+":cmbProdAsoc")!=null)    
		document.all("txt"+strId+":cmbProdAsoc").value = "";

	/*Dario 2013-12-26 se agrega nuevo campo*/
	if (document.all(strId+':txtApodo') != null )
		document.all(strId+":txtApodo").value = "";
		
	/*Dario 2014-08-26 se agregan nuevos campos*/
	if (document.all(strId+':hidRPNac') != null )
		document.all(strId+":hidRPNac").value = "";		
	
	if (document.all(strId+':hidTipoRegistro') != null )
		document.all(strId+":hidTipoRegistro").value = "";		
	
	if (document.all(strId+':hidVariedad') != null )
		document.all(strId+":hidVariedad").value = "";	

	if (document.all(strId+':hidPelaAPeli') != null )
		document.all(strId+":hidPelaAPeli").value = "";									

	try{document.all("txt" + strId + ":cmbProdRaza").focus();}
	catch(e){;}

	GenerarEventos(strId);
 
}

function imgBuscProdDeriv_click(strId,pstrParams,pstrSession,pstrEstilo,pstrPagina)
{
	var sFiltro = "";
	sFiltro += "ctrlId=" + strId + "&" + pstrParams;
	mbooParam = false;

	if (document.all(strId+':txtProdNomb') != null && document.all(strId+':txtProdNomb').value != '')
		sFiltro += agregarParam(strId, "txtProdNomb", "Nomb");
	if (document.all(strId+':txtSraNume') != null && document.all(strId+':txtSraNume').value != '')
		sFiltro += agregarParam(strId, "txtSraNume", "SraNume");
	if (document.all(strId+':cmbProdAsoc') != null && document.all(strId+':cmbProdAsoc').value != '')
		sFiltro += agregarParam(strId, "cmbProdAsoc", "Asoc");
	if (document.all(strId+':txtCodi') != null && document.all(strId+':txtCodi').value != '')
		sFiltro += agregarParam(strId, "txtCodi", "AsocNume");
	if (document.all(strId+':txtSexoId').value != '')
		sFiltro += agregarParam(strId, "txtSexoId", "Sexo");
	if (document.all(strId+':txtDonaFil') != null && document.all(strId+':txtDonaFil').value != '')
		sFiltro += agregarParam(strId, "txtDonaFil", "NroDona");
	if (document.all(strId+':cmbProdRaza') != null && document.all(strId+':cmbProdRaza').value != '')
		sFiltro += agregarParam(strId, "cmbProdRaza", "Raza");
	else
		sFiltro += agregarParam(strId, "cmbCriaNumeRaza", "Raza");	

	if (document.all(strId+':txtRP') != null && document.all(strId+':txtRP').value != '')
		sFiltro += agregarParam(strId, "txtRP", "RPNume");
	else
		sFiltro += agregarParam(strId, "txtRpNume", "RpNume");
	sFiltro += agregarParam(strId, "hdnCriaNume", "pCriaNume");
	if (document.all["hdnFiltroProdCriaId"]!= null)
		sFiltro += '&pCriaId=' + document.all["hdnFiltroProdCriaId"].value;
	else
		sFiltro += agregarParam(strId, "txtCriaId", "pCriaId");		
		
	sFiltro += agregarParam(strId, "txtCodi", "CodiNume");
	sFiltro += agregarParam(strId, "txtNaciFecha", "NaciFecha");
	sFiltro += agregarParam(strId, "txtLaboNume", "LaboNume");
	sFiltro += agregarParam(strId, "txtId", "ValorId");
	sFiltro += agregarParam(strId, "hdnProp", "Prop");
	sFiltro += agregarParam(strId, "hdnRazaCruza", "RazaCruza");
	
	if (document.all(strId+':cmbProdRaza') != null && document.all(strId+':cmbProdRaza').disabled)
		sFiltro += "&FilHabiRaza=0";
	else
		sFiltro += "&FilHabiRaza=1";
		
	var sRet="";

	if(mbooParam && document.all(strId + ":txtId").value == "")
		sRet=EjecutarMetodoXML("BuscarProdDerivCons", pstrPagina + ";" + sFiltro);

	if(sRet!="")
	{
		vsRet=sRet.split("|");
		document.all(strId + ":txtSraNume").value = vsRet[0];
		document.all(strId + ":txtProdNomb").value = vsRet[1];
		if (!document.all[strId + ":cmbProdRaza"].disabled)
		{
		    gBuscarValorCombo (vsRet[2],document.all(strId + ":cmbProdRaza"));
			document.all['txt'+strId+":cmbProdRaza"].value = LeerCamposXML("razas", vsRet[2], "raza_codi");
		}
		var lstrSexoCodi = '';
		if (vsRet[3]=='H') lstrSexoCodi='0';
		if (vsRet[3]=='M') lstrSexoCodi='1';
		if (document.all(strId + ":cmbProdSexo")!=null && !document.all(strId + ":cmbProdSexo").disabled)
			gBuscarValorCombo (lstrSexoCodi,document.all(strId + ":cmbProdSexo"));
		if (document.all(strId+':txtSexoId').value != '')
			gBuscarTextoCombo (vsRet[3],document.all(strId + ":txtSexoId"));		
		gBuscarTextoCombo (vsRet[4],document.all(strId + ":cmbProdAsoc"));				
		document.all(strId + ":txtId").value = vsRet[5];		
		if (document.all(strId + ":txtDesc")!=null)
			document.all(strId + ":txtDesc").value = vsRet[6];
	}
	else
	{	
		Ventana = 3;
		gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);
		
		window.attachEvent('onfocus',window_onfocus);
		window.document.body.attachEvent('onfocus',window_onfocus);
	}
}

// Control usrProducto (Raza - Criador/Propietario)
function imgBuscProducto_click(strId, pstrParams, pstrSession, pstrEstilo, pstrPagina, pbooOmiteBusqueda)
{
	var sFiltro = "";
	sFiltro += "ctrlId=" + strId + "&" + pstrParams;
	mbooParam = false;

	//Nombre del Producto.
	if (document.all(strId+':txtProdNomb') != null && document.all(strId+':txtProdNomb').value != '')
		sFiltro += agregarParam(strId, "txtProdNomb", "Nomb");
	//HBA.
	if (document.all(strId+':txtSraNume') != null && document.all(strId+':txtSraNume').value != '')
		sFiltro += agregarParam(strId, "txtSraNume", "SraNume");
	//Asociaci�n.
	if (document.all(strId+':cmbProdAsoc') != null && document.all(strId+':cmbProdAsoc').value != '')
		sFiltro += agregarParam(strId, "cmbProdAsoc", "Asoc");
	//Nro. Criador/Propietario.
	if (document.all(strId+':txtCodi') != null && document.all(strId+':txtCodi').value != '')
		sFiltro += agregarParam(strId, "txtCodi", "AsocNume");
	//Sexo
	if (document.all(strId+':txtSexoId').value != '')
		sFiltro += agregarParam(strId, "txtSexoId", "Sexo");
	//Dona.
	if (document.all(strId+':txtDonaFil') != null && document.all(strId+':txtDonaFil').value != '')
		sFiltro += agregarParam(strId, "txtDonaFil", "NroDona");
	//Raza.
	if (document.all(strId+':usrCriadorFil:cmbRazaCria') != null && document.all(strId+':usrCriadorFil:cmbRazaCria').value != '')
		sFiltro += agregarParam(strId, "usrCriadorFil:cmbRazaCria", "Raza");
	//Id Criador/Propietario.
	if (document.all(strId+':usrCriadorFil:txtId') != null && document.all(strId+':usrCriadorFil:txtId').value != '')
		sFiltro += agregarParam(strId, "usrCriadorFil:txtId", "CriaOrPropId");	
	//RP.
	if (document.all(strId+':txtRP') != null && document.all(strId+':txtRP').value != '')
		sFiltro += agregarParam(strId, "txtRP", "Rp");
	else
		sFiltro += agregarParam(strId, "txtRpNume", "RpNume");

	sFiltro += agregarParam(strId, "hdnCriaNume", "pCriaNume");
	if (document.all["hdnFiltroProdCriaId"]!= null)
		sFiltro += '&pCriaId=' + document.all["hdnFiltroProdCriaId"].value;
	else
		sFiltro += agregarParam(strId, "txtCriaId", "pCriaId");		
		
	sFiltro += agregarParam(strId, "txtCodi", "CodiNume");
	sFiltro += agregarParam(strId, "txtNaciFecha", "NaciFecha");
	sFiltro += agregarParam(strId, "txtLaboNume", "LaboNume");
	sFiltro += agregarParam(strId, "txtId", "ValorId");
	sFiltro += agregarParam(strId, "hdnCriaOrProp", "CriaOrProp");
	sFiltro += agregarParam(strId, "hdnRazaCruza", "RazaCruza");
	
	if (document.all(strId+':usrCriadorFil:cmbRazaCria') != null && document.all(strId+':usrCriadorFil:cmbRazaCria').disabled)
		sFiltro += "&FilHabiRaza=0";
	else
		sFiltro += "&FilHabiRaza=1";
		
	var sRet="";

	// Verifico si omite la b�squeda.
	// Si NO la omite, consulto la base.
	// Caso contrario, abro la p�gina Productos.aspx.
	if (pbooOmiteBusqueda == "false")
	{
		if(mbooParam && document.all(strId + ":txtId").value == "")
			// Realizo la consulta a la base.
			sRet=EjecutarMetodoXML("BuscarProdDerivCons", pstrPagina + ";" + sFiltro);

		// Si trae datos, los pego en el control.
		// Caso contrario, abro la p�gina Productos.aspx.
		if(sRet!="")
		{
			vsRet=sRet.split("|");
			document.all(strId + ":txtSraNume").value = vsRet[0];
			document.all(strId + ":txtProdNomb").value = vsRet[1];
		    /*Dario 2021-04-21*/
		    try {
		        if (!document.all[strId + ":cmbProdRaza"].disabled) {
		            gBuscarValorCombo(vsRet[2], document.all(strId + ":cmbProdRaza"));
		            document.all['txt' + strId + ":cmbProdRaza"].value = LeerCamposXML("razas", vsRet[2], "raza_codi");
		        }
		    }
		    catch (err) { }
            /*
			if (!document.all[strId + ":cmbProdRaza"].disabled)
			{
				gBuscarValorCombo (vsRet[2],document.all(strId + ":cmbProdRaza"));
				document.all['txt'+strId+":cmbProdRaza"].value = LeerCamposXML("razas", vsRet[2], "raza_codi");
			}
            */
			var lstrSexoCodi = '';
			if (vsRet[3]=='H') lstrSexoCodi='0';
			if (vsRet[3]=='M') lstrSexoCodi='1';
			if (document.all(strId + ":cmbProdSexo")!=null && !document.all(strId + ":cmbProdSexo").disabled)
				gBuscarValorCombo (lstrSexoCodi,document.all(strId + ":cmbProdSexo"));
			if (document.all(strId+':txtSexoId').value != '')
				gBuscarTextoCombo (vsRet[3],document.all(strId + ":txtSexoId"));		
			gBuscarTextoCombo (vsRet[4],document.all(strId + ":cmbProdAsoc"));				
			document.all(strId + ":txtId").value = vsRet[5];		
			if (document.all(strId + ":txtDesc")!=null)
				document.all(strId + ":txtDesc").value = vsRet[6];
			if (document.all(strId + ":txtRP")!=null)
				document.all(strId + ":txtRP").value = vsRet[7];
		}
		else
		{	
			// Abro p�gina Productos.aspx.
			Ventana = 3;
			gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
	}
	else
	{
		// Abro p�gina Productos.aspx.
		Ventana = 3;
		gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);
		
		window.attachEvent('onfocus',window_onfocus);
		window.document.body.attachEvent('onfocus',window_onfocus);	
	}
}

// Control usrProducto (Raza - Criador/Propietario)
function imgBuscProductoNew_click(strId, pstrParams, pstrSession, pstrEstilo, pstrPagina, pbooOmiteBusqueda)
{
	var sFiltro = "";
	sFiltro += "ctrlId=" + strId + "&" + pstrParams;
	mbooParam = false;

	//Nombre del Producto.
	if (document.all(strId+':txtProdNomb') != null && document.all(strId+':txtProdNomb').value != '')
		sFiltro += agregarParam(strId, "txtProdNomb", "Nomb");
	//HBA.
	if (document.all(strId+':txtSraNume') != null && document.all(strId+':txtSraNume').value != '')
		sFiltro += agregarParam(strId, "txtSraNume", "SraNume");
	//Asociaci�n.
	if (document.all(strId+':cmbProdAsoc') != null && document.all(strId+':cmbProdAsoc').value != '')
		sFiltro += agregarParam(strId, "cmbProdAsoc", "Asoc");
	//Nro. Criador/Propietario.
	if (document.all(strId+':txtCodi') != null && document.all(strId+':txtCodi').value != '')
		sFiltro += agregarParam(strId, "txtCodi", "AsocNume");
	//Sexo
	if (document.all(strId+':txtSexoId').value != '')
		sFiltro += agregarParam(strId, "txtSexoId", "Sexo");
	//Dona.
	if (document.all(strId+':txtDonaFil') != null && document.all(strId+':txtDonaFil').value != '')
		sFiltro += agregarParam(strId, "txtDonaFil", "NroDona");
	//Raza.
	if (document.all(strId+':usrCriadorFil:cmbRazaCria') != null && document.all(strId+':usrCriadorFil:cmbRazaCria').value != '')
		sFiltro += agregarParam(strId, "usrCriadorFil:cmbRazaCria", "Raza");
	//Id Criador/Propietario.
	if (document.all(strId+':usrCriadorFil:txtId') != null && document.all(strId+':usrCriadorFil:txtId').value != '')
		sFiltro += agregarParam(strId, "usrCriadorFil:txtId", "CriaOrPropId");	
	//RP.
	if (document.all(strId+':txtRP') != null && document.all(strId+':txtRP').value != '')
		sFiltro += agregarParam(strId, "txtRP", "Rp");
	else
		sFiltro += agregarParam(strId, "txtRpNume", "RpNume");

	sFiltro += agregarParam(strId, "hdnCriaNume", "pCriaNume");
	if (document.all["hdnFiltroProdCriaId"]!= null)
		sFiltro += '&pCriaId=' + document.all["hdnFiltroProdCriaId"].value;
	else
		sFiltro += agregarParam(strId, "txtCriaId", "pCriaId");		
		
	sFiltro += agregarParam(strId, "txtCodi", "CodiNume");
	sFiltro += agregarParam(strId, "txtNaciFecha", "NaciFecha");
	sFiltro += agregarParam(strId, "txtLaboNume", "LaboNume");
	sFiltro += agregarParam(strId, "txtId", "ValorId");
	sFiltro += agregarParam(strId, "hdnCriaOrProp", "CriaOrProp");
	sFiltro += agregarParam(strId, "hdnRazaCruza", "RazaCruza");
	
	if (document.all(strId+':usrCriadorFil:cmbRazaCria') != null && document.all(strId+':usrCriadorFil:cmbRazaCria').disabled)
		sFiltro += "&FilHabiRaza=0";
	else
		sFiltro += "&FilHabiRaza=1";
		
	var sRet="";

	// Verifico si omite la b�squeda.
	// Si NO la omite, consulto la base.
	// Caso contrario, abro la p�gina Productos.aspx.
	if (pbooOmiteBusqueda == "false")
	{
		if(mbooParam && document.all(strId + ":txtId").value == "")
			// Realizo la consulta a la base.
			sRet=EjecutarMetodoXML("BuscarProdDerivCons", pstrPagina + ";" + sFiltro);

		// Si trae datos, los pego en el control.
		// Caso contrario, abro la p�gina Productos.aspx.
		if(sRet!="")
		{
			vsRet=sRet.split("|");
			document.all(strId + ":txtSraNume").value = vsRet[0];
			document.all(strId + ":txtProdNomb").value = vsRet[1];
			if (!document.all[strId + ":cmbProdRaza"].disabled)
			{
				gBuscarValorCombo (vsRet[2],document.all(strId + ":cmbProdRaza"));
				document.all['txt'+strId+":cmbProdRaza"].value = LeerCamposXML("razas", vsRet[2], "raza_codi");
			}
			var lstrSexoCodi = '';
			if (vsRet[3]=='H') lstrSexoCodi='0';
			if (vsRet[3]=='M') lstrSexoCodi='1';
			if (document.all(strId + ":cmbProdSexo")!=null && !document.all(strId + ":cmbProdSexo").disabled)
				gBuscarValorCombo (lstrSexoCodi,document.all(strId + ":cmbProdSexo"));
			if (document.all(strId+':txtSexoId').value != '')
				gBuscarTextoCombo (vsRet[3],document.all(strId + ":txtSexoId"));		
			gBuscarTextoCombo (vsRet[4],document.all(strId + ":cmbProdAsoc"));				
			document.all(strId + ":txtId").value = vsRet[5];		
			if (document.all(strId + ":txtDesc")!=null)
				document.all(strId + ":txtDesc").value = vsRet[6];
			if (document.all(strId + ":txtRP")!=null)
				document.all(strId + ":txtRP").value = vsRet[7];
		}
		else
		{	
			// Abro p�gina Productos.aspx.
			Ventana = 3;
			gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
	}
	else
	{
		// Abro p�gina Productos.aspx.
		Ventana = 3;
		gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);
		
		window.attachEvent('onfocus',window_onfocus);
		window.document.body.attachEvent('onfocus',window_onfocus);	
	}
}

// Control usrProductoExtranjero (Raza)
function imgBuscProductoExtranjero_click(strId, pstrParams, pstrSession, pstrEstilo, pstrPagina, pbooOmiteBusqueda)
{
	var sFiltro = "";
	sFiltro += "ctrlId=" + strId + "&" + pstrParams;
	mbooParam = false;

	// Nombre del Producto.
	if (document.all(strId+':txtProdNomb') != null && document.all(strId+':txtProdNomb').value != '')
		sFiltro += agregarParam(strId, "txtProdNomb", "Nomb");
	// Asociaci�n.
	if (document.all(strId+':cmbProdAsoc') != null && document.all(strId+':cmbProdAsoc').value != '')
		sFiltro += agregarParam(strId, "cmbProdAsoc", "Asoc");
	// Sexo
	if (document.all(strId+':txtSexoId').value != '')
		sFiltro += agregarParam(strId, "txtSexoId", "Sexo");
	// Dona.
	if (document.all(strId+':txtDonaFil') != null && document.all(strId+':txtDonaFil').value != '')
		sFiltro += agregarParam(strId, "txtDonaFil", "NroDona");
	// Raza.
	if (document.all(strId+':cmbProdRaza') != null && document.all(strId+':cmbProdRaza').value != '')
		sFiltro += agregarParam(strId, "cmbProdRaza", "Raza");
	// RP Extranjero.
	if (document.all(strId+':txtRPExtr') != null && document.all(strId+':txtRPExtr').value != '')
		sFiltro += agregarParam(strId, "txtRPExtr", "RpExtr");
	// Nro. Extranjero.		
	sFiltro += agregarParam(strId, "txtCodi", "CodiNume");
	
	sFiltro += agregarParam(strId, "txtId", "ValorId");
	sFiltro += agregarParam(strId, "hdnRazaCruza", "RazaCruza");	
	
	if (document.all(strId+':cmbProdRaza') != null && document.all(strId+':cmbProdRaza').disabled)
		sFiltro += "&FilHabiRaza=0";
	else
		sFiltro += "&FilHabiRaza=1";
		
	var sRet="";

	// Verifico si omite la b�squeda.
	// Si NO la omite, consulto la base.
	// Caso contrario, abro la p�gina Productos.aspx.
	if (pbooOmiteBusqueda == "false")
	{
		if(mbooParam && document.all(strId + ":txtId").value == "")
			// Realizo la consulta a la base.
			sRet=EjecutarMetodoXML("BuscarProdDerivCons", pstrPagina + ";" + sFiltro);

		// Si trae datos, los pego en el control.
		// Caso contrario, abro la p�gina Productos.aspx.
		if(sRet!="")
		{
			vsRet=sRet.split("|");
			document.all(strId + ":txtProdNomb").value = vsRet[1];
			if (!document.all[strId + ":cmbProdRaza"].disabled)
			{
				gBuscarValorCombo (vsRet[2],document.all(strId + ":cmbProdRaza"));
				document.all['txt'+strId+":cmbProdRaza"].value = LeerCamposXML("razas", vsRet[2], "raza_codi");
			}
			var lstrSexoCodi = '';
			if (vsRet[3]=='H') lstrSexoCodi='0';
			if (vsRet[3]=='M') lstrSexoCodi='1';
			if (document.all(strId + ":cmbProdSexo")!=null && !document.all(strId + ":cmbProdSexo").disabled)
				gBuscarValorCombo (lstrSexoCodi,document.all(strId + ":cmbProdSexo"));
			if (document.all(strId+':txtSexoId').value != '')
				gBuscarTextoCombo (vsRet[3],document.all(strId + ":txtSexoId"));		
			gBuscarTextoCombo (vsRet[4],document.all(strId + ":cmbProdAsoc"));				
			document.all(strId + ":txtId").value = vsRet[5];			
			if (document.all(strId + ":txtDesc")!=null)
				document.all(strId + ":txtDesc").value = vsRet[6];			
			if (document.all(strId + ":txtRPExtr")!=null)
				document.all(strId + ":txtRPExtr").value = vsRet[8];
		}
		else
		{	
			// Abro p�gina Productos.aspx.
			Ventana = 3;
			gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);
			
			window.attachEvent('onfocus',window_onfocus);
			window.document.body.attachEvent('onfocus',window_onfocus);
		}
	}
	else
	{
		// Abro p�gina Productos.aspx.
		Ventana = 3;
		gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);
			
		window.attachEvent('onfocus',window_onfocus);
		window.document.body.attachEvent('onfocus',window_onfocus);
	}
}

function imgAddProductoImportado_click(strId, pstrParams, IdCtrlPropietario)
{
// debugger;
	var sFiltro = "";
	//Agrego al filtro los par�metros que vienen desde la p�gina.
	sFiltro += "ctrlId=" + strId + "&" + pstrParams;
	//Raza.
	if (document.all(strId+':cmbProdRaza') != null && document.all(strId+':cmbProdRaza').value != '')
		sFiltro += agregarParam(strId, "cmbProdRaza", "Raza");
	//Sexo.
	if (document.all(strId+':txtSexoId').value != '')
		sFiltro += agregarParam(strId, "txtSexoId", "Sexo");	
	//Asociaci�n.
	if (document.all(strId+':cmbProdAsoc') != null && document.all(strId+':cmbProdAsoc').value != '')
		sFiltro += agregarParam(strId, "cmbProdAsoc", "Asoc");	
	
	// Dario 2013-12-19 se agregan mas datos para auto cargar el pop up
	//Nro Extranjero.
	if (document.all(strId+':txtCodi') != null && document.all(strId+':txtCodi').value != '')
		sFiltro += agregarParam(strId, "txtCodi", "NroExtr");	
	//RP Extranjero.
	if (document.all(strId+':txtRPExtr') != null && document.all(strId+':txtRPExtr').value != '')
		sFiltro += agregarParam(strId, "txtRPExtr", "RPExtr");			
	//Nombre.
	if (document.all(strId+':txtProdNomb') != null && document.all(strId+':txtProdNomb').value != '')
		sFiltro += agregarParam(strId, "txtProdNomb", "NombreProd");			
	//Apodo.
	if (document.all(strId+':txtApodo') != null && document.all(strId+':txtApodo').value != '')
		sFiltro += agregarParam(strId, "txtApodo", "ApodoProd");			
	//txtdesc.
	if (document.all(strId+':txtDesc') != null && document.all(strId+':txtDesc').value != '')
		sFiltro += agregarParam(strId, "txtDesc", "DescPord");	
	// Dario 2014-07-03	
	//fecha naci
	if (document.all(strId+':hidFechaNaci') != null && document.all(strId+':hidFechaNaci').value != '')
		sFiltro += agregarParam(strId, "hidFechaNaci", "FechaNaci");			
	//rp nacional
	if (document.all(strId+':hidRPNac') != null && document.all(strId+':hidRPNac').value != '')
		sFiltro += agregarParam(strId, "hidRPNac", "RPNac");					
	//tipo registro
	if (document.all(strId+':hidTipoRegistro') != null && document.all(strId+':hidTipoRegistro').value != '')
		sFiltro += agregarParam(strId, "hidTipoRegistro", "TipoRegistro");	
	//variedad
	if (document.all(strId+':hidVariedad') != null && document.all(strId+':hidVariedad').value != '')
		sFiltro += agregarParam(strId, "hidVariedad", "Variedad");								
	//variedad
	if (document.all(strId+':hidPelaAPeli') != null && document.all(strId+':hidPelaAPeli').value != '')
		sFiltro += agregarParam(strId, "hidPelaAPeli", "PelaAPeli");	
	// fin cambio 2014-07-03					
	// auto genera nro HBA 
	if (document.all(strId+':txtAutoGeneraHBA') != null && document.all(strId+':txtAutoGeneraHBA').value != '')
		sFiltro += agregarParam(strId, "txtAutoGeneraHBA", "GenHBA");		
	// nacionalidad 
	if (document.all(strId+':txtNacionalidad') != null && document.all(strId+':txtNacionalidad').value != '')
		sFiltro += agregarParam(strId, "txtNacionalidad", "Ndad");	
	// fecha parametro $('#'+document.all(strId+':'+'txtFechaParametro').value).val()
	if(document.all(strId+':'+'txtFechaParametro').value != null && document.all(strId+':'+'txtFechaParametro').value != '')
		sFiltro += "&FParam=" + document.all(document.all(strId+':'+'txtFechaParametro').value).value;	
	// id de control de propietario para hacer validacion
	var idCriadorPorpietario = '0';
	if(IdCtrlPropietario != '')
	{
		if(document.all(IdCtrlPropietario+':'+'txtId').value != null && document.all(IdCtrlPropietario+':'+'txtId').value != '')
		sFiltro += "&IdCtrlPropietarioParam=" + document.all(IdCtrlPropietario +':'+'txtId').value;	
	}
						
	//Abro ProductoImportado_pop.aspx.
	gAbrirVentanas("ProductoImportado_pop.aspx?" + sFiltro, 1, "850", "400");
	
	window.attachEvent('onfocus',window_onfocus);
	window.document.body.attachEvent('onfocus',window_onfocus);
}

/**** CONTROL CRIADEROS****/
function CodiCriaDeriv_change(strId, strAutoPB, strArgs, strCampoVal)
{
	var oCoti = document.all(strId + ":txtCodi");
	var sFiltro = oCoti.value;
	var sIdOri = document.all(strId + ":txtId").value;
	
	document.all(strId + ":txtCriaNomb").value = "";
	document.all(strId + ":txtId").value = "";

	if (!booIsNumber(sFiltro)||(sFiltro==""))
	{
		if (document.all[strId + ":txtDesc"]!= null)
			document.all(strId + ":txtDesc").value = "";
		
		if(strAutoPB=="1" && sIdOri!="")
				__doPostBack(strId + ":txtId",'');
	}
	else	
	{
			sFiltro += ";" + strArgs;
			var sRet=EjecutarMetodoXML("Utiles.BuscarCriaDeriv", sFiltro);
			if(sRet=="")
			{
				alert(strCampoVal + " inexistente")
				document.all(strId + ":txtCriaNomb").value = "";
				if (document.all[strId + ":txtDesc"]!= null)
					document.all(strId + ":txtDesc").value = "";
				oCoti.focus();
				
				if(strAutoPB=="1" && sIdOri!="")
					__doPostBack(strId + ":txtId",'');
			}
			else
			{
				var vsRet=sRet.split("|");
				document.all(strId + ":txtCriaNomb").value = vsRet[2];
				if (document.all[strId + ":txtDesc"]!= null)
					document.all(strId + ":txtDesc").value = vsRet[3];
				document.all(strId + ":txtId").value = vsRet[0];
				
				if(strAutoPB=="1")
					__doPostBack(strId + ":txtId",'');
		}
	}
	
	try{eval(strId + "_onchange();")}
	catch(e){;}
}

function imgBuscCriaDeriv_click(strId,pstrParams,pstrSession,pstrEstilo,pstrPagina)
{
	var sFiltro = "";
	sFiltro += "ctrlId=" + strId + "&" + pstrParams;
	mbooParam = false;
	sFiltro += agregarParam(strId, "txtCriaNomb", "nomb");
	sFiltro += agregarParam(strId, "txtNume", "Nume");
	sFiltro += agregarParam(strId, "cmbRaza", "Raza");
	sFiltro += agregarParam(strId, "cmbPais", "Pais");
	sFiltro += agregarParam(strId, "cmbProv", "Prov");
	sFiltro += agregarParam(strId, "cmbLoca", "Loca");
	sFiltro += agregarParam(strId, "txtClieNume", "ClieNume");	

	var sRet="";

	if(mbooParam && document.all(strId + ":txtId").value == "")
		sRet=EjecutarMetodoXML("BuscarCriaDerivCons", pstrPagina + ";" + sFiltro);

	if(sRet!="")
	{
		document.all(strId + ":txtCodi").value = sRet;
		document.all(strId + ":txtCodi").onchange();
	}
	else
	{	
		Ventana = 2;
		gAbrirVentanas(pstrPagina + "?" + sFiltro, Ventana);
		
		window.attachEvent('onfocus',window_onfocus);
		window.document.body.attachEvent('onfocus',window_onfocus);
	}
}

function BusqCombo(pstrTabla,pstrFiltro,pstrCampo,pstrCombo,pTitu)
{
   gAbrirVentanas("consulta_combo_Pop.aspx?EsConsul=0&titulo="+pTitu+"&tabla=" + pstrTabla + "&filtros=" + pstrFiltro + "&campoDesc=" + pstrCampo + "&combo=" + pstrCombo, 1, "600","300");
}

function ReplaceComillaPorSignoEspecial(pCadena)
{
   Replace(cadena,"'","~");
   return pCadena;
}

function ReplaceComillaSimpleTo2Comillas(pCadena)
{
   Replace(cadena,"'","''");
   return pCadena;
}

function ReplaceTo2ComillasToComillaSimple(pCadena)
{
   Replace(cadena,"''","'");
   return pCadena;
}

function ReplaceSignoEspecialPorComilla(pCadena)
{
   Replace(cadena,"~","'");
   return pCadena;
}

// Dario se comenta para sacar el prototype   
// Elimina los espacios iniciales de una cadena
//String.prototype.LTrim = function()
//{    
//    return this.replace(/\s*((\S+\s*)*)/, "$1");
//}

// Elimina los espacios finales de una cadena
//String.prototype.RTrim = function()
//{   
//    return this.replace(/((\s*\S+)*)\s*/, "$1");
//}

// Elimina los espacios iniciales y finales de una cadena
//String.prototype.Trim = function()
//{   
//    return this.LTrim(this.RTrim());   
//}

// Convierte el primer car�cter de cada cadena en may�sculas
//String.prototype.Capitalize = function()
//{
//    return this.replace(/\w+/g, function(a)
//    {
//        return a.charAt(0).toUpperCase() + a.slice(1).toLowerCase();
//    });
//};

// Descodifica una cadena en URL
//String.prototype.UrlDecode = function()
//{
//    var str = this.replace(new RegExp('\\+','g'),' ');
//    return unescape(str);
//};
// C�difica una cadena en URL
//String.prototype.UrlEncode = function()
//{
//    var str = escape(this);
//    str = str.replace(new RegExp('\\+','g'),'%2B');

//    return str.replace(new RegExp('%20','g'),'+');
//};

//Convierte una cadena a entero desp�es de eliminar sus espacios iniciales y finales
//String.prototype.ToInteger = function()
//{
//    var str = parseInt(this.Trim());
//    return str;
//};

// Formatea un n�mero poniendole 0 delante y lo devuelve en string
//String.prototype.FormatNum = function(Length)
//{
//    var Num = "" + this;
//    var Diff = parseInt(Length) - parseInt(Num.length);
//    var Numero = parseInt(Num);
//    var NumberStr = "" + Num + "";

//    if (Diff > 0)
//    {
//       for (var i=0; i < Diff; i++)
//       {
//          NumberStr = "0" + NumberStr;
//       }
//    }

//    return NumberStr;
//}

// Formatea un alfanum�rico poniendole espacios detr�s y lo devuelve en string
//String.prototype.FormatAlf = function(Length)
//{
//    var Txt = "" + this;
//    var Diff = parseInt(Length) - parseInt(Txt.length);
//    var Str = Txt;

//    if (Diff > 0)
//    {
//        for (var i = 0; i < Diff; i++)
//        {
//            Str += " ";
//        }
//    }
//    return Str;
//}

// Formatea un alfanum�rico poniendole guiones bajos detr�s y lo devuelve en string
//String.prototype.FormatAlfUnderline = function(Length)
//{
//    var Txt = "" + this;
//    var Diff = parseInt(Length) - parseInt(Txt.length);
//    var Str = Txt;

//    if (Diff > 0)
//    {
//        for (var i = 0; i < Diff; i++)
//        {
//            Str += "_";
//        }
//    }

//    return Str;
//}

// Sustituye las comillas simples por dos comillas dobles
//String.prototype.Escape = function()
//{
//    var Txt = fReplace(this, "'", "''");
//    return Txt.Trim();
//}

// A�ade barras para escapar caracteres especiales
//String.prototype.AddSlashes = function()
//{
//    var str = this.replace(/\\/g,'\\\\');
//    str = str.replace(/\'/g,'\\\'');
//    str = str.replace(/\"/g,'\\"');
//    str = str.replace(/\0/g,'\\0');

//    return str;
//}

// Quita las barras para escapar caracteres especiales
//String.prototype.StripSlashes = function()
//{
//    var str = this.replace(/\\'/g,'\'');
//    str = str.replace(/\\"/g,'"');
//    str = str.replace(/\\0/g,'\0');
//    str = str.replace(/\\\\/g,'\\');
//    return str;
//}

//Comprime en JAVASCRIPT
//String.prototype.Compress = function()
//{
//    var q = {}, ret = "";

//    this.replace(/([^=&]+)=([^&]*)/g, function(m, key, value)
//    {
//        q[key] = (q[key] ? q[key] + "," : "") + value;
//    });

//    for ( var key in q )
//        ret = (ret ? ret + "&" : "") + key + "=" + q[key];

//    return ret;
//}

