function gValNumber(pobjField, pintValorMax, pAutoPostBack)
{
  if (pobjField.value == ''){	
  return true;
  }
  if (booIsNumber(pobjField.value))
  {
	if (gValMaxValor(pobjField, pintValorMax)) {
		pobjField.value = ''
		return true;
	}
	else
	{
		if (pAutoPostBack == 1) {
			__doPostBack(pobjField.id,'');
		}
		
		return false;
	}
		
  }
  else
  {
    alert('N' + String.fromCharCode(250) + 'mero inv' + String.fromCharCode(225) + 'lido '+ pobjField.value);
    pobjField.value = ''
    pobjField.focus(); 
    pobjField.select();
    return true
  }
}
function booIsNumber(astrValue)
{
	for ( var i = 0 ; i < astrValue.length ; i++ )
	{
		var ch = astrValue.substring( i, i + 1 )
		if ( ( ch < "0" || "9" < ch ))
		{
			return false
		}
	}	
	return true
}

function gValMaxValor(pobjField, pintValorMax)
{
 
    if (pobjField.value > pintValorMax)
	{
		alert('Valor inv' + String.fromCharCode(225) + 'lido '+ pobjField.value + '.\nDebe ingresar un n' + String.fromCharCode(250)+ 'mero menor a ' + pintValorMax);
		pobjField.focus(); 
		pobjField.select();
		return true;
	}
	else
	{
		return false;
	}
}
