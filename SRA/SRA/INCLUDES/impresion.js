function mImprimirCopias(pCanti,pEsta,pCompId,pRepo,pImpreTipo,pUrl,pDire)
{
	var lstrEstaPara = ""; 
	var lstrEstaValor = "";
	if (pEsta != 'O')
	{
		lstrEstaPara = ";estado";
		lstrEstaValor = ";Duplicado";
	}
	for (var lintI = 1; lintI < pCanti; lintI++)	
	{
		var sRet = ImprimirReporte(pRepo, "comp_id" + lstrEstaPara + ";random", pCompId + lstrEstaValor + ";0", pImpreTipo,pCompId,pUrl,pDire);
	}	
	return(sRet)
}

function ImprimirReporte(lstrRpt, NomParam, ValParams, pstrTipo, ValParam, UrlSitio, RepoDire) 
{
if(pstrTipo=="C")
{
	document.all("RSClientPrint").MarginLeft = 3;
	document.all("RSClientPrint").MarginRight = (1150/100);//(675/100);
    document.all("RSClientPrint").MarginBottom = 5;
	document.all("RSClientPrint").MarginTop = 0;
	document.all("RSClientPrint").PageHeight = 297;
	document.all("RSClientPrint").PageWidth = 210;

	document.all("RSClientPrint").Culture = 3082;
	document.all("RSClientPrint").UICulture = 10;
	

	var ldecRand = Math.random();
	ldecRand = ldecRand * 1000;
	ldecRand = Math.round(ldecRand)

	var vNomParams = NomParam.split(";");
	var vValParams = ValParams.split(";");
	var strValParam="";

	for(xz=0; xz<vNomParams.length; xz++)
	{
		if (vNomParams[xz].toUpperCase()=="RANDOM")
			vValParams[xz]=ldecRand;
		strValParam=strValParam + "&" + vNomParams[xz] + "=" + vValParams[xz];
	}

	var lstrRand = new Number(ldecRand).toString();
	try{
		document.all("RSClientPrint").Print(UrlSitio + "/rptproxy.aspx", "%2f" + RepoDire + "%2f" + lstrRpt + strValParam, lstrRpt)
		return("1");
	}						
	catch(e)
	{
	alert("ERROR AL EFECTUAR LA IMPRESION ");
	return("0");
	}
}
else
{
	var lstrImprId="";
	if (document.all["hdnImprId"]!=null && document.all("hdnImprId").value!="")
			lstrImprId=document.all("hdnImprId").value;

	var lstrRet;
	var lstrRet=EjecutarMetodoXML("ImprimirReporte", lstrRpt + "|" + lstrImprId + "|" + NomParam + "|" + ValParams);

	if(lstrRet=="0")
	{
		alert("No existe una impresora configurada para imprimir este reporte");
		return("0");
	}

	if(lstrRet=="1")
	{
		return("1");
	}

	if (document.all("hdnImprId") != null)
	{
		if (document.all("hdnImprId").value == "")
			lstrImprId = window.showModalDialog("/sra/impresoras.htm", lstrRet, "dialogHeight:100px;dialogWidth:250px;status=no,resizable=no");
		else
			lstrImprId = document.all("hdnImprId").value;
	}
	else
		lstrImprId = window.showModalDialog("/sra/impresoras.htm", lstrRet, "dialogHeight:100px;dialogWidth:250px;status=no,resizable=no");

    if (document.all("hdnImprId") != null)
		document.all("hdnImprId").value = lstrImprId;

	if(lstrImprId!=""&&lstrImprId!=undefined)
	{

		lstrRet=EjecutarMetodoXML("ImprimirReporte", lstrRpt + "|" + lstrImprId + "|" + NomParam + "|" + ValParams);
		if(lstrRet=="ERROR")
		{
			alert("ERROR AL EFECTUAR LA IMPRESION");
			return("0");
		}	
		else
			return("1");
	}
	else
		return("0");
}
}

function ObtenerImpresora(lstrRpt) {
	var lstrRet;
	var lstrRet=EjecutarMetodoXML("ObtenerImpresora", lstrRpt);

	if(lstrRet=="0")
	{
		alert("No existe una impresora configurada para imprimir este reporte");
		return(false);
	}
	
	lstrImprId = window.showModalDialog("/sra/impresoras.htm", lstrRet, "dialogHeight:100px;dialogWidth:250px;status=no,resizable=no");

	if(lstrImprId!=""&&lstrImprId!=undefined)
	{
		if (document.all["hdnImprId"]!=null)
			document.all("hdnImprId").value = lstrImprId;
		return(true);
	}
	else
		return("0");
}

function ImprimirTalones(pstrReciId, pstrTipo, UrlSitio, RepoDire)
{
	if(pstrReciId!="")
	{
		var lstrRet = "";
		lstrRet = LeerCamposXML("recibo_talones_rpt", pstrReciId, "cate_desc");
		if (lstrRet=="")
			return;

		if (window.confirm("Desea imprimir los talones de las cuotas sociales?"))
		{
			try{
				var sRet = ImprimirReporte("Recibo_talones", "comp_id", pstrReciId, pstrTipo, pstrReciId, UrlSitio, RepoDire);

				if(sRet=="0") return(sRet);

				if (!window.confirm("Se imprimieron los talones correctamente?"))
					ImprimirTalones(pstrReciId, pstrTipo, UrlSitio, RepoDire);
			}
			catch(e)
			{
				alert("Error al intentar efectuar la impresion");
			}
		}
	}
}

function ImprimirReporteNueva(lstrRpt, NomParam, ValParams, pstrTipo, ValParam, UrlSitio, RepoDire) 
{
if(pstrTipo=="C")
{
	document.all("RSClientPrint").MarginLeft = 3;
	document.all("RSClientPrint").MarginRight = (1150/100);//(675/100);
    document.all("RSClientPrint").MarginBottom = 5;
	document.all("RSClientPrint").MarginTop = 0;
	document.all("RSClientPrint").PageHeight = 297;
	document.all("RSClientPrint").PageWidth = 210;

	document.all("RSClientPrint").Culture = 3082;
	document.all("RSClientPrint").UICulture = 10;
	

	var ldecRand = Math.random();
	ldecRand = ldecRand * 1000;
	ldecRand = Math.round(ldecRand)

	var vNomParams = NomParam.split(";");
	var vValParams = ValParams.split(";");
	var strValParam="";

	for(xz=0; xz<vNomParams.length; xz++)
	{
		if (vNomParams[xz].toUpperCase()=="RANDOM")
			vValParams[xz]=ldecRand;
		strValParam=strValParam + "&" + vNomParams[xz] + "=" + vValParams[xz];
	}

	var lstrRand = new Number(ldecRand).toString();
	try{
		window.print();
		//var pepe = window.open('ReportViewer.aspx','Popup','toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=1000,height=600,left=10,top=10')
		var pepe = window.showModalDialog('ReportViewer.aspx?Param='+ lstrRpt + '&vNomParams='+ vNomParams + '&vValParams='+ vValParams, lstrRpt, "dialogHeight:100px;dialogWidth:250px;status=no,resizable=no");                                                                        
		//document.all("RSClientPrint").Print(UrlSitio + "/rptproxy.aspx", "%2f" + RepoDire + "%2f" + lstrRpt + strValParam, lstrRpt)
		return("1");
	}						
	catch(e)
	{
	alert("ERROR AL EFECTUAR LA IMPRESION ");
	return("0");
	}
}
else
{
	var lstrImprId="";
	if (document.all["hdnImprId"]!=null && document.all("hdnImprId").value!="")
			lstrImprId=document.all("hdnImprId").value;

	var lstrRet;
	var lstrRet=EjecutarMetodoXML("ImprimirReporte", lstrRpt + "|" + lstrImprId + "|" + NomParam + "|" + ValParams);

	if(lstrRet=="0")
	{
		alert("No existe una impresora configurada para imprimir este reporte");
		return("0");
	}

	if(lstrRet=="1")
	{
		return("1");
	}

	if (document.all("hdnImprId") != null)
	{
		if (document.all("hdnImprId").value == "")
			lstrImprId = window.showModalDialog("/sra/impresoras.htm", lstrRet, "dialogHeight:100px;dialogWidth:250px;status=no,resizable=no");
		else
			lstrImprId = document.all("hdnImprId").value;
	}
	else
		lstrImprId = window.showModalDialog("/sra/impresoras.htm", lstrRet, "dialogHeight:100px;dialogWidth:250px;status=no,resizable=no");

    if (document.all("hdnImprId") != null)
		document.all("hdnImprId").value = lstrImprId;

	if(lstrImprId!=""&&lstrImprId!=undefined)
	{

		lstrRet=EjecutarMetodoXML("ImprimirReporte", lstrRpt + "|" + lstrImprId + "|" + NomParam + "|" + ValParams);
		if(lstrRet=="ERROR")
		{
			alert("ERROR AL EFECTUAR LA IMPRESION");
			return("0");
		}	
		else
			return("1");
	}
	else
		return("0");
}
}
