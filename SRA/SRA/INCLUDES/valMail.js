function gValMail(ptxtMail)
{
  if (ptxtMail.value == '')
  {
	return true;	
  }
  if (!isEmail(ptxtMail.value))
  {
	ptxtMail.value = ''
    alert('Ingrese una direcci' + String.fromCharCode(243) + 'n de email v' + String.fromCharCode(225) + 'lida.');
    ptxtMail.focus();
    return false;
  }
  return true;
}

function isEmail(str) 
{
 var vsStr = str.split(';');
 
 for(i=0;i<vsStr.length;i++)
 {
	if (!isEmailInd(vsStr[i]))
	{
		return false;
	}
 }
 return true;
}


function isEmailInd(str)
 {

    var emailRegEx = /^([a-zA-Z0-9])([a-zA-Z0-9\._-])*@(([a-zA-Z0-9_-])+(\.))+([a-zA-Z]{2,4})+$/ ;

	var strMail = str;

	if(strMail.search(emailRegEx)==-1)
		return false;
	else
		return true;
}
