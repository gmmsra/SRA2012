Namespace SRA

Partial Class CalculoMuestreo
    Inherits FormGenerico

#Region " C�digo generado por el Dise�ador de Web Forms "

	'El Dise�ador de Web Forms requiere esta llamada.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

	End Sub
	Protected WithEvents btnList As NixorControls.BotonImagen

	'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
	'No se debe eliminar o mover.

	Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
		'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
		'No la modifique con el editor de c�digo.
		InitializeComponent()
	End Sub

#End Region

#Region "Definici�n de Variables"
	Private mstrTabla As String = "rg_muestreo_filtros"
	Private mstrTablaMues As String = "rg_muestreo"
	Private mstrTablaMuesDeta As String = "rg_muestreo_deta"
	Private mstrCmd As String
	Private mstrConn As String
	Private mintinsti As Int32
	Private mstrTipoPro As String

	Private Enum Columnas As Integer
		Id = 1
	End Enum
#End Region

#Region "Operaciones sobre la Pagina"
	Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
		Try
			mstrConn = clsWeb.gVerificarConexion(Me)
			mInicializar()
			If (Not Page.IsPostBack) Then
				mSetearEventos()

				mCargarCombos()
				mConsultar()

				clsWeb.gInicializarControles(Me, mstrConn)
			Else
				If panDato.Visible = False Then
					If cmbRazaFil.SelectedValue.ToString <> "" Then
						cmbEspecieFil.SelectedValue = clsSQLServer.gObtenerValorCampo(mstrConn, "razas", cmbRazaFil.SelectedValue.ToString, "raza_espe_id")
					End If
					If Request.Form(cmbCriadorFil.UniqueID) Is Nothing Then
						clsWeb.gCargarCombo(mstrConn, "criadores_cargar" & IIf(cmbRazaFil.SelectedValue.ToString = "", "", " @raza_id=" & cmbRazaFil.SelectedValue.ToString), cmbCriadorFil, "id", "descrip", "T")
					End If

				Else
					If cmbRaza.SelectedValue.ToString <> "" Then
						cmbEspecie.SelectedValue = clsSQLServer.gObtenerValorCampo(mstrConn, "razas", cmbRaza.SelectedValue.ToString, "raza_espe_id")
					End If
					If Request.Form(cmbCriador.UniqueID) Is Nothing Then
						clsWeb.gCargarCombo(mstrConn, "criadores_cargar" & IIf(cmbRaza.SelectedValue.ToString = "", "", " @raza_id=" & cmbRaza.SelectedValue.ToString), cmbCriador, "id", "descrip", "T")
					End If
				End If
			End If
		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mCargarCombos()
		clsWeb.gCargarCombo(mstrConn, "especies_cargar", cmbEspecieFil, "id", "descrip", "T")
		clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRazaFil, "id", "descrip_codi", "T")
		clsWeb.gCargarCombo(mstrConn, "criadores_cargar", cmbCriadorFil, "id", "descrip", "T")
		clsWeb.gCargarCombo(mstrConn, "especies_cargar", cmbEspecie, "id", "descrip", "S")
		clsWeb.gCargarCombo(mstrConn, "razas_cargar", cmbRaza, "id", "descrip_codi", "S")
		clsWeb.gCargarCombo(mstrConn, "criadores_cargar", cmbCriador, "id", "descrip", "S")
		SRA_Neg.Utiles.gSetearRaza(cmbRaza)
		SRA_Neg.Utiles.gSetearRaza(cmbRazaFil)
	End Sub
	Private Sub mSetearEventos()
		btnBaja.Attributes.Add("onclick", "if(!confirm('Confirma la baja del registro?')) return false;")
	End Sub

	Private Sub mInicializar()
		mstrTipoPro = Request.QueryString("tipo")

		If mstrTipoPro = "P" Then
			panCria.Visible = False
			panCriaFil.Visible = False
			grdDato.Columns(11).Visible = False
			grdDato.Columns(13).Visible = False
			lblTituAbm.Text = "Calculo Muestreo"
			'cmbRazaFil.Attributes.Add("AutoPostBack", "False")
		Else
			panCria.Visible = True
			panCriaFil.Visible = True
			grdDato.Columns(11).Visible = True
			grdDato.Columns(13).Visible = True
			lblTituAbm.Text = "Calculo Muestreo por Comisi�n"
			'cmbRazaFil.Attributes.Add("AutoPostBack", "True")
		End If

	End Sub

#End Region

#Region "Operaciones sobre el DataGrid"
	Public Sub DataGrid_Page(ByVal Sender As Object, ByVal E As DataGridPageChangedEventArgs)
		Try
			grdDato.EditItemIndex = -1
			If (grdDato.CurrentPageIndex < 0 Or grdDato.CurrentPageIndex >= grdDato.PageCount) Then
				grdDato.CurrentPageIndex = 0
			Else
				grdDato.CurrentPageIndex = E.NewPageIndex
			End If
			mConsultar()

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Function mObtenerFiltrosConsul(ByVal pstrTabla As String, ByVal pintOpci As Integer) As String
		Dim lstrCmd As String

		lstrCmd = "exec " + pstrTabla & "_consul"

		Select Case pintOpci
			Case 1
				lstrCmd += " @anio=" & clsSQLServer.gFormatArg(txtAnio.Text, SqlDbType.Int)
				lstrCmd += ",@fecha_control_desde=" & clsSQLServer.gFormatArg(txtPeriodoDesde.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@fecha_control_hasta=" & clsSQLServer.gFormatArg(txtPeriodoHasta.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@tipo_fecha='" & cmbTipoFecha.Valor.ToString & "'"
				lstrCmd += ",@espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString)
				lstrCmd += ",@raza_id=" & IIf(cmbRaza.Valor.ToString = "", "null", cmbRaza.Valor.ToString)
				If mstrTipoPro = "C" Then
					lstrCmd += ",@cria_id=" & IIf(cmbCriador.Valor.ToString = "", "null", cmbCriador.Valor.ToString)
				End If
				lstrCmd += ",@fecha_inicio_ctrol=" & clsSQLServer.gFormatArg(txtFechaIniCtrol.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@incluir_te=" & clsSQLServer.gFormatArg(cmbIncluyeTE.Valor.ToString, SqlDbType.Bit)
				lstrCmd += ",@tipo_proce='" & mstrTipoPro & "'"
			Case 2
				lstrCmd += " @anio=" & clsSQLServer.gFormatArg(txtAnioFil.Text, SqlDbType.Int)
				lstrCmd += ",@fecha_control_desde=" & clsSQLServer.gFormatArg(txtPeriodoDesdeFil.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@fecha_control_hasta=" & clsSQLServer.gFormatArg(txtPeriodoHastaFil.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@tipo_fecha='" & clsSQLServer.gFormatArg(cmbTipoFechaFil.Valor.ToString, SqlDbType.Char) & "'"
				lstrCmd += ",@espe_id=" & IIf(cmbEspecieFil.Valor.ToString = "", "null", cmbEspecieFil.Valor.ToString)
				lstrCmd += ",@raza_id=" & IIf(cmbRazaFil.Valor.ToString = "", "null", cmbRazaFil.Valor.ToString)
				If mstrTipoPro = "C" Then
					lstrCmd += ",@cria_id=" & IIf(cmbCriadorFil.Valor.ToString = "", "null", cmbCriadorFil.Valor.ToString)
				End If
				lstrCmd += ",@fecha_inicio_ctrol=" & clsSQLServer.gFormatArg(txtFechaIniCtrolFil.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@incluir_te=" & IIf(cmbIncluyeTEFil.Valor.ToString = "", "null", clsSQLServer.gFormatArg(cmbIncluyeTEFil.Valor.ToString, SqlDbType.Bit))
				lstrCmd += ",@tipo_proce='" & mstrTipoPro & "'"
				
		End Select

		Return lstrCmd

	End Function

	Public Sub mConsultar()
		Try
			mstrCmd = mObtenerFiltrosConsul(mstrTabla, 2)

			clsWeb.gCargarDataGrid(mstrConn, mstrCmd, grdDato)
			mMostrarPanel(False)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
#End Region

#Region "Seteo de Controles"
	Private Sub mSetearEditor(ByVal pbooAlta As Boolean)
		btnBaja.Enabled = Not (pbooAlta)
		btnModi.Enabled = Not (pbooAlta)
		btnAlta.Enabled = pbooAlta
		btnDetalle.Visible = Not (pbooAlta)
	End Sub
	Public Sub mEditarDatos(ByVal Sender As Object, ByVal E As DataGridCommandEventArgs)
		hdnId.Text = clsFormatear.gFormatCadena(E.Item.Cells(Columnas.Id).Text)
		mstrCmd = "exec " + mstrTabla + "_consul"
		mstrCmd += " " + hdnId.Text

		Dim ldsDatos As DataSet = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)
		If ldsDatos.Tables(0).Rows.Count > 0 Then
			With ldsDatos.Tables(0).Rows(0)
				txtAnio.Text = .Item("mufi_anio").ToString
				txtPeriodoDesde.Fecha = .Item("mufi_fecha_desde").ToString
				txtPeriodoHasta.Fecha = .Item("mufi_fecha_hasta").ToString
				cmbTipoFecha.Valor = .Item("mufi_tipo_fecha").ToString
				cmbEspecie.Valor = .Item("mufi_espe_id").ToString
				cmbRaza.Valor = .Item("mufi_raza_id").ToString
				cmbCriador.Valor = .Item("mufi_cria_id").ToString
				txtFechaIniCtrol.Fecha = .Item("mufi_fecha_ctrol_ini").ToString
				If .Item("mufi_ctrola_te") Then
					cmbIncluyeTE.Valor = 1
				Else
					cmbIncluyeTE.Valor = 0
				End If
			End With

			mSetearEditor(False)
			mMostrarPanel(True)
		End If
	End Sub
	Private Sub mAgregar()
		mLimpiar()
		btnBaja.Enabled = False
		btnModi.Enabled = False
		btnAlta.Enabled = True
        mMostrarPanel(True)
        txtAnio.Text = Date.Now.Year
	End Sub
	Private Sub mLimpiar()
		hdnId.Text = ""
		txtAnio.Text = ""
		txtPeriodoDesde.Text = ""
		txtPeriodoHasta.Text = ""
		cmbTipoFecha.Limpiar()
		cmbEspecie.Limpiar()
		cmbRaza.Limpiar()
		cmbCriador.Limpiar()
		txtFechaIniCtrol.Text = ""
		cmbIncluyeTE.Limpiar()

		mSetearEditor(True)
	End Sub

	Private Sub mCerrar()
		mMostrarPanel(False)
	End Sub
	Private Sub mMostrarPanel(ByVal pbooVisi As Boolean)
		panDato.Visible = pbooVisi
		btnAgre.Enabled = Not (panDato.Visible)

	End Sub
#End Region

#Region "Opciones de ABM"
	Private Sub mValidarDatos()
		If txtAnio.Text = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar el A�o a procesar.")
		End If
		If txtPeriodoDesde.Text = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar la Fecha de Inicio del Periodo a Controlar.")
		End If

		If txtPeriodoHasta.Text = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar la Fecha de Fin del Periodo a Controlar.")
		End If
		If txtPeriodoDesde.Fecha > txtPeriodoHasta.Fecha Then
			Throw New AccesoBD.clsErrNeg("La Fecha de inicio debe ser Menor a la Fecha de Fin del Periodo a Controlar.")
		End If
		If cmbEspecie.Valor.ToString = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar la Especie a procesar.")
		End If
		If cmbRaza.Valor.ToString = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar la Raza a procesar.")
		End If
		If mstrTipoPro = "C" Then
			If cmbCriador.Valor.ToString = "" Then
				Throw New AccesoBD.clsErrNeg("Debe indicar un Criador a procesar.")
			End If
			If txtPorc.Valor.ToString = "" Then
				Throw New AccesoBD.clsErrNeg("Debe indicar el Porcentaje de productos a Procesar.")
			End If
		End If
		If txtFechaIniCtrol.Text = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar la fecha de inicio de Control.")
		End If
		If cmbIncluyeTE.Valor.ToString = "" Then
			Throw New AccesoBD.clsErrNeg("Debe indicar si quiere incluir en el calculo del muestreo" & vbCrLf & "los nacimientos de T/E")
		End If


	End Sub

	Private Function mObtenerFiltrosABM(ByVal pstrProcedure As String, ByVal pintOpci As Integer, Optional ByVal pstrCria_id As String = "0") As String
		Dim lstrCmd As String

		Select Case pintOpci
			Case 1
				lstrCmd = "exec " + pstrProcedure
				lstrCmd += " @anio=" & clsSQLServer.gFormatArg(txtAnio.Text, SqlDbType.Int)
				lstrCmd += ",@fecha_control_desde=" & clsSQLServer.gFormatArg(txtPeriodoDesde.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@fecha_control_hasta=" & clsSQLServer.gFormatArg(txtPeriodoHasta.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@tipo_fecha='" & cmbTipoFecha.Valor.ToString & "'"
				lstrCmd += ",@espe_id=" & IIf(cmbEspecie.Valor.ToString = "", "null", cmbEspecie.Valor.ToString)
				lstrCmd += ",@raza_id=" & IIf(cmbRaza.Valor.ToString = "", "null", cmbRaza.Valor.ToString)
				lstrCmd += ",@fecha_inicio_ctrol=" & clsSQLServer.gFormatArg(txtFechaIniCtrol.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@incluir_te=" & clsSQLServer.gFormatArg(cmbIncluyeTE.Valor.ToString, SqlDbType.Bit)
				lstrCmd += ",@tipo_proce='" & mstrTipoPro & "'"
				If mstrTipoPro = "C" Then
					lstrCmd += ",@cria_id=" & IIf(cmbCriador.Valor.ToString = "", "null", cmbCriador.Valor.ToString)
					lstrCmd += ",@mufi_porc=" & IIf(txtPorc.Valor.ToString = "", "null", txtPorc.Valor.ToString)
				End If
				lstrCmd += ",@audi_user=" & Session("sUserId").ToString()
			Case 2
				lstrCmd = "exec " + pstrProcedure
				lstrCmd += " @fecha_control_desde=" & clsSQLServer.gFormatArg(txtPeriodoDesde.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@fecha_control_hasta=" & clsSQLServer.gFormatArg(txtPeriodoHasta.Text, SqlDbType.SmallDateTime)
				lstrCmd += ",@tipo_fecha='" & cmbTipoFecha.Valor.ToString & "'"
				lstrCmd += ",@cria_id=" & pstrCria_id
				lstrCmd += ",@incluir_te=" & clsSQLServer.gFormatArg(cmbIncluyeTE.Valor.ToString, SqlDbType.Bit)

		End Select

		Return lstrCmd

	End Function

	Private Sub mAlta()
		Try
			Dim lstrMufiID As String = ""
			Dim lstrCamuID As String = ""
			Dim lstrCamuDetaID As String = ""
			Dim lstrCriaID As String = ""
			Dim ldsCamu As DataSet
			Dim ldsCamuDeta As DataSet
			Dim lstrCmd As String = ""
			Dim lintCantRows As Integer			'Cantidad de Rows que va a tener ldsCamuDeta.Tables(0)
			Dim lintPrdt As Integer			'cantidad de Productos que tengo que grabar en el Detalle
			Dim Random As New Random
			Dim i As Integer
			Dim lintRan As Integer
			Dim lbooGrabo As Boolean = False

			mValidarDatos()


			'Preparo la consulta para Verificar que no exista los filtros a cargar
			lstrCmd = mObtenerFiltrosConsul(mstrTabla, 1)

			i = clsSQLServer.gExecute(mstrConn, lstrCmd)
			If i <> 1 Then

				lstrCmd = mObtenerFiltrosABM(mstrTabla & "_alta", 1)
				lstrMufiID = clsSQLServer.gExecuteScalar(mstrConn, lstrCmd)
				lstrCmd = ""
				lstrCmd = mObtenerFiltrosABM(mstrTablaMues & "_calculo", 1)
				ldsCamu = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

				For Each ldr As DataRow In ldsCamu.Tables(0).Select()
					With ldr
						lstrCmd = "exec " + mstrTablaMues & "_alta @mufi_id=" & lstrMufiID
						lstrCmd += ",@camu_cria_id=" & clsSQLServer.gFormatArg(.Item(0).ToString, SqlDbType.Int)
						lstrCmd += ",@camu_raza_id=" & clsSQLServer.gFormatArg(.Item(1).ToString, SqlDbType.Int)
						lstrCmd += ",@camu_crias_total=" & clsSQLServer.gFormatArg(.Item(2).ToString, SqlDbType.Int)
						lstrCmd += ",@camu_crias_mues=" & clsSQLServer.gFormatArg(.Item(3).ToString, SqlDbType.Int)
						lstrCmd += ",@audi_user=" & Session("sUserId").ToString()

						'para calcular los productos que van a ir a Muestreo
						lintCantRows = clsSQLServer.gFormatArg(.Item(2).ToString, SqlDbType.Int)
						lintPrdt = clsSQLServer.gFormatArg(.Item(3).ToString, SqlDbType.Int)
						lstrCriaID = .Item(0).ToString

					End With

					'Doy de alta el ldr actual
					lstrCamuID = clsSQLServer.gExecuteScalar(mstrConn, lstrCmd)

					''obtengo los productos del criador
					'lstrCmd = mObtenerFiltrosABM(mstrTablaMuesDeta & "_consul", 2, lstrCriaID)
					'ldsCamuDeta = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

					''Recorro los productos segun la cantidad que tenga que seleccionar
					'For i = 1 To lintPrdt
					'	lintRan = Random.Next(0, lintCantRows - 1)
					'	'Los Selecciono con Random
					'	Dim ldrDeta As DataRow = ldsCamuDeta.Tables(0).Rows(lintRan)

					'	lstrCmd = "exec " + mstrTablaMuesDeta & "_alta @camu_id=" & lstrCamuID
					'	lstrCmd += ",@cmud_cria_id=" & clsSQLServer.gFormatArg(ldrDeta.Item(1).ToString, SqlDbType.Int)
					'	lstrCmd += ",@cmud_prdt_id=" & clsSQLServer.gFormatArg(ldrDeta.Item(2).ToString, SqlDbType.Int)
					'	lstrCmd += ",@audi_user=" & Session("sUserId").ToString()
					'	'Grabo el Producto Seleccionado
					'	lstrCamuDetaID = clsSQLServer.gExecuteScalar(mstrConn, lstrCmd)
					'Next
				Next

				mConsultar()
				mMostrarPanel(False)

			Else
				Throw New AccesoBD.clsErrNeg("ya existe.")

			End If

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mModi()
		Try
			Dim lstrMufiID As String = ""
			Dim lstrCamuID As String = ""
			Dim lstrCamuDetaID As String = ""
			Dim lstrCriaID As String = ""
			Dim ldsCamu As DataSet
			Dim ldsCamuDeta As DataSet
			Dim lstrCmd As String = ""
			Dim lintCantRows As Integer			'Cantidad de Rows que va a tener ldsCamuDeta.Tables(0)
			Dim lintPrdt As Integer			'cantidad de Productos que tengo que grabar en el Detalle
			Dim Random As New Random
			Dim i As Integer
			Dim lintRan As Integer

			mValidarDatos()
			'Modifica y limpio los detalles calculados anteriormente
			lstrCmd = mObtenerFiltrosABM(mstrTabla & "_modi @mufi_id=" & hdnId.Text & ",", 1)
			lstrMufiID = clsSQLServer.gExecuteScalar(mstrConn, lstrCmd)

			lstrCmd = mObtenerFiltrosABM(mstrTablaMues & "_calculo", 1)
			ldsCamu = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

			For Each ldr As DataRow In ldsCamu.Tables(0).Select()
				With ldr
					lstrCmd = "exec " + mstrTablaMues & "_alta @mufi_id=" & lstrMufiID
					lstrCmd += ",@camu_cria_id=" & clsSQLServer.gFormatArg(.Item(0).ToString, SqlDbType.Int)
					lstrCmd += ",@camu_raza_id=" & clsSQLServer.gFormatArg(.Item(1).ToString, SqlDbType.Int)
					lstrCmd += ",@camu_crias_total=" & clsSQLServer.gFormatArg(.Item(2).ToString, SqlDbType.Int)
					lstrCmd += ",@camu_crias_mues=" & clsSQLServer.gFormatArg(.Item(3).ToString, SqlDbType.Int)
					lstrCmd += ",@audi_user=" & Session("sUserId").ToString()

					'para calcular los productos que van a ir a Muestreo
					lintCantRows = clsSQLServer.gFormatArg(.Item(2).ToString, SqlDbType.Int)
					lintPrdt = clsSQLServer.gFormatArg(.Item(3).ToString, SqlDbType.Int)
					lstrCriaID = .Item(0).ToString

				End With

				'Doy de alta el ldr actual
				lstrCamuID = clsSQLServer.gExecuteScalar(mstrConn, lstrCmd)

				''obtengo los productos del criador
				'lstrCmd = mObtenerFiltrosABM(mstrTablaMuesDeta & "_consul", 2, lstrCriaID)
				'ldsCamuDeta = clsSQLServer.gExecuteQuery(mstrConn, lstrCmd)

				'For i = 1 To lintPrdt
				'	lintRan = Random.Next(0, lintCantRows - 1)
				'	Dim ldrDeta As DataRow = ldsCamuDeta.Tables(0).Rows(lintRan)

				'	lstrCmd = "exec " + mstrTablaMuesDeta & "_alta @camu_id=" & lstrCamuID
				'	lstrCmd += ",@cmud_cria_id=" & clsSQLServer.gFormatArg(ldrDeta.Item(1).ToString, SqlDbType.Int)
				'	lstrCmd += ",@cmud_prdt_id=" & clsSQLServer.gFormatArg(ldrDeta.Item(2).ToString, SqlDbType.Int)
				'	lstrCmd += ",@audi_user=" & Session("sUserId").ToString()
				'	lstrCamuDetaID = clsSQLServer.gExecuteScalar(mstrConn, lstrCmd)
				'Next

			Next
			mConsultar()
			mMostrarPanel(False)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub
	Private Sub mBaja()
		Try
			Dim lintMufiID As Integer
			Dim lstrCmd As String = ""
			Dim lintPage As Integer = grdDato.CurrentPageIndex

			lstrCmd = "exec " & mstrTabla & "_baja @mufi_id=" & hdnId.Text
			lintMufiID = clsSQLServer.gExecute(mstrConn, lstrCmd)

			mConsultar()

			If (lintPage < grdDato.PageCount) Then
				grdDato.CurrentPageIndex = lintPage
				mConsultar()
			End If

			mMostrarPanel(False)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub


#End Region

#Region "Eventos de Controles"

	Private Sub mLimpiarFil()
		txtAnioFil.Text = ""
		txtPeriodoDesdeFil.Text = ""
		txtPeriodoHastaFil.Text = ""
		cmbTipoFechaFil.Limpiar()
		cmbEspecieFil.Limpiar()
		cmbRazaFil.Limpiar()
		cmbCriadorFil.Limpiar()
		txtFechaIniCtrolFil.Text = ""
		cmbIncluyeTEFil.Limpiar()

		mConsultar()
	End Sub

	Private Sub btnAlta_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnAlta.Click
		mAlta()
	End Sub

	Private Sub btnBaja_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnBaja.Click
		mBaja()
	End Sub

	Private Sub btnModi_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnModi.Click
		mModi()
	End Sub

	Private Sub btnLimp_Click(ByVal Sender As Object, ByVal e As System.EventArgs) Handles btnLimp.Click
		mLimpiar()
	End Sub

	Private Sub imgClose_Click(ByVal Sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles imgClose.Click
		mCerrar()
	End Sub

	Private Sub cmbFK_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		grdDato.CurrentPageIndex = 0
		mConsultar()
	End Sub

	Private Overloads Sub btnBusc_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnBusc.Click
		grdDato.CurrentPageIndex = 0
		mConsultar()
	End Sub

	Private Sub btnLimpiarFil_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLimpiarFil.Click
		mLimpiarFil()
	End Sub

	Private Overloads Sub btnAgre_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnAgre.Click
		mAgregar()
	End Sub

	Private Sub btnImprimir_Click(ByVal sender As System.Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnImprimir.Click
		Try
			Dim lintFechaD As Integer
			Dim lintFechaH As Integer
			Dim lintFechaI As Integer
			Dim lstrRptName As String = "MuestreoFiltros"
			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)

			If txtPeriodoDesdeFil.Fecha.ToString = "" Then
				lintFechaD = 0
			Else
				lintFechaD = CType(clsFormatear.gFormatFechaString(txtPeriodoDesdeFil.Text, "Int32"), Integer)
			End If
			If txtPeriodoHastaFil.Fecha.ToString = "" Then
				lintFechaH = 0
			Else
				lintFechaH = CType(clsFormatear.gFormatFechaString(txtPeriodoHastaFil.Text, "Int32"), Integer)
			End If
			If txtFechaIniCtrolFil.Fecha.ToString = "" Then
				lintFechaI = 0
			Else
				lintFechaI = CType(clsFormatear.gFormatFechaString(txtFechaIniCtrolFil.Text, "Int32"), Integer)
			End If

			lstrRpt += IIf(txtAnioFil.Text = "", "", "&anio=" & txtAnioFil.Text)
			lstrRpt += "&fecha_control_desde=" & lintFechaD.ToString
			lstrRpt += "&fecha_control_hasta=" & lintFechaH.ToString
			lstrRpt += IIf(cmbTipoFechaFil.Valor.ToString = "", "", "&tipo_fecha=" & cmbTipoFechaFil.Valor.ToString)
			lstrRpt += IIf(cmbEspecieFil.Valor.ToString = "", "", "&espe_id=" & cmbEspecieFil.Valor.ToString)
			lstrRpt += IIf(cmbRazaFil.Valor.ToString = "", "", "&raza_id=" & cmbRazaFil.Valor.ToString)
			lstrRpt += "&fecha_inicio_ctrol=" & lintFechaI.ToString
			lstrRpt += IIf(cmbIncluyeTEFil.Valor.ToString = "", "", "&incluir_te=" & IIf(cmbIncluyeTEFil.Valor.ToString = "0", "false", "true"))
			lstrRpt += "&tipo_proce=" & mstrTipoPro


			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

	Private Sub btnDetalle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDetalle.Click
		Try
			Dim lstrRptName As String = "MuestreoCalculo"
			Dim lstrRpt As String = clsReportingServices.clsReportingProxy.gIniciarReporte(Request, lstrRptName)
			lstrRpt += "&camu_mufi_id=" + IIf(hdnId.Text = "", "0", hdnId.Text)

			lstrRpt = clsReportingServices.clsReportingProxy.gSetearOpcionesReporte(lstrRpt)
			Response.Redirect(lstrRpt)

		Catch ex As Exception
			clsError.gManejarError(Me, ex)
		End Try
	End Sub

#End Region

End Class
End Namespace
