' Dario 2013-09-19 se agrega dato centro emisor al usuario
Imports System.Data.SqlClient
Imports Generico


Namespace SRA

' Dario 2013-07-30 Se agrega el  mail en retorno para no ejecutar una consulta al pedo luego a la hora de mandar mails
Partial Class login

    Inherits System.Web.UI.Page

#Region " C�digo generado por el Dise�ador de Web Forms "

    'El Dise�ador de Web Forms requiere esta llamada.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTA: el Dise�ador de Web Forms necesita la siguiente declaraci�n del marcador de posici�n.
    'No se debe eliminar o mover.

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: el Dise�ador de Web Forms requiere esta llamada de m�todo
        'No la modifique con el editor de c�digo.
        InitializeComponent()
    End Sub

#End Region

    Protected WithEvents Form1 As System.Web.UI.HtmlControls.HtmlForm
    Protected WithEvents Form2 As System.Web.UI.HtmlControls.HtmlForm

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cmdLogin.Attributes.Add("onclick", "if(document.frmLogin.UserName.value == '') {alert('Debe ingresar su nombre de usuario.'); document.frmLogin.UserName.focus(); return false;} ")
        If Not Page.IsPostBack Then
            Dim lstrTrustConn As String = System.Configuration.ConfigurationSettings.AppSettings("conTrustConn").ToString()
            If (lstrTrustConn = "1") Then

                Dim lstrLogin As String = HttpContext.Current.User.Identity.Name
                Dim lintUserPosi As Integer = lstrLogin.IndexOf("\\")
                lstrLogin = lstrLogin.Substring(lintUserPosi + 1)
                If (mVerificarConexion()) Then

                    If (mVerificarUsuario(lstrLogin, "", "S")) Then
                        ' Dario se comenta mObtenerCentroEmisor() los datos se obtienen en el mVerificarUsuario()
                        ' mObtenerCentroEmisor()
                        mObtenerPais()
                        If System.Configuration.ConfigurationSettings.AppSettings("SelectRaza") <> "" Then
                            Dim oRaza As New SRA_Neg.Razas(lstrTrustConn, Session("sUserId").ToString())
                            Dim oEspecie As New SRA_Neg.Especie(lstrTrustConn, Session("sUserId").ToString())

                            Session("sRazaId") = System.Configuration.ConfigurationSettings.AppSettings("SelectRaza")
                            Session("sRazaDesc") = oRaza.GetRazaDescripcionById(Session("sRazaId"))
                            Session("sEspeId") = System.Configuration.ConfigurationSettings.AppSettings("SelectEspecie")
                            Session("sEspeDesc") = oEspecie.GetEspecieDescripcionById(Session("sEspeId"))


                        End If
                        Response.Redirect("default.aspx")
                    Else

                        Response.Redirect("noaccess.aspx")
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub cmdLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLogin.Click
        If (Page.IsPostBack) Then
                If (Page.IsValid) Then
                    'SRA_Neg.Encript.gDesencriptar(UserPass.Text)
                    If (mVerificarConexion()) Then
                        clsError.gLoggear("Usuario: " + UserName.Text + " - pws:" + UserPass.Text)
                        If (mVerificarUsuario(UserName.Text, SRA_Neg.Encript.gEncriptar(UserPass.Text), "N")) Then
                            ' Dario se comenta mObtenerCentroEmisor() los datos se obtienen en el mVerificarUsuario()
                            ' mObtenerCentroEmisor()
                            mObtenerPais()
                            AuditarLogueoXUsuario()
                            If System.Configuration.ConfigurationSettings.AppSettings("SelectRaza") <> "" Then
                                Dim oRaza As New SRA_Neg.Razas(Page.Session("sConn"), Session("sUserId").ToString())
                                Dim oEspecie As New SRA_Neg.Especie(Page.Session("sConn"), Session("sUserId").ToString())

                                Session("sRazaId") = System.Configuration.ConfigurationSettings.AppSettings("SelectRaza")
                                Session("sRazaDesc") = oRaza.GetRazaDescripcionById(Session("sRazaId"))
                                Session("sEspeId") = System.Configuration.ConfigurationSettings.AppSettings("SelectEspecie")
                                Session("sEspeDesc") = oEspecie.GetEspecieDescripcionById(Session("sEspeId"))


                            End If
                            Response.Redirect("default.aspx")
                        End If
                    End If
                End If
            Else
            UserName.Text = ""
            UserPass.Text = ""
        End If
    End Sub

    Private Function mVerificarConexion() As Boolean
        Dim dstAppCode As String = System.Configuration.ConfigurationSettings.AppSettings("AppCode").ToString()
        Dim lstrServ As String = clsFormatear.gDesencriptar(System.Configuration.ConfigurationSettings.AppSettings("conServ").ToString(), dstAppCode)
        Dim lstrBase As String = clsFormatear.gDesencriptar(System.Configuration.ConfigurationSettings.AppSettings("conBase").ToString(), dstAppCode)
        Dim lstrUser As String = clsFormatear.gDesencriptar(System.Configuration.ConfigurationSettings.AppSettings("conUser").ToString(), dstAppCode)
        Dim lstrPass As String = clsFormatear.gDesencriptar(System.Configuration.ConfigurationSettings.AppSettings("conPass").ToString(), dstAppCode)
        Dim lstrTimeOut As String = System.Configuration.ConfigurationSettings.AppSettings("conTimeOut").ToString()

        Dim lstrConn As String = ""

        Try
            lstrConn = "server=" + lstrServ + ";database=" + lstrBase
            lstrConn += ";uid=" + lstrUser + ";pwd=" + lstrPass + ""
            'lstrConn = lstrConn + "Integrated Security=SSPI"
            lstrConn += ";Connection Timeout=" + lstrTimeOut
            lstrConn += ";Persist Security Info=True"

            Dim myConnection As New SqlConnection(lstrConn)
            Page.Session("sConn") = lstrConn
            myConnection.Open()
            myConnection.Close()
            Session("sBase") = lstrServ + "\\" + lstrBase
            Return (True)
        Catch

            Msg.Text = "Error al conectarse a la base de datos." + lstrConn
            Return (False)
        End Try
    End Function

    Public Sub mObtenerCentroEmisor()

        Dim lstrHost As String
        Try
            lstrHost = System.Net.Dns.GetHostByAddress(Request.ServerVariables("REMOTE_ADDR")).HostName
        Catch ex As Exception
            lstrHost = "NO IDENTIFICADA"
        End Try

        Session("sHost") = lstrHost

        Dim lstrCmd As New StringBuilder
        lstrCmd.Append("exec emisores_ctros_busq")
        lstrCmd.Append(" @hsts_desc=")
        lstrCmd.Append(clsSQLServer.gFormatArg(lstrHost, SqlDbType.VarChar))

        Dim lDsDatos As DataSet
        lDsDatos = clsSQLServer.gExecuteQuery(Session("sConn").ToString(), lstrCmd.ToString)
        If lDsDatos.Tables(0).Rows.Count = 0 Then
            Msg.Text = "La m�quina " & lstrHost & " no tiene un centro de emisi�n asociado."
        Else
            Session("sCentroEmisorId") = lDsDatos.Tables(0).Rows(0).Item("emct_id")
            Session("sCentroEmisorNro") = lDsDatos.Tables(0).Rows(0).Item("emct_codi")
            Session("sCentroEmisorCentral") = lDsDatos.Tables(0).Rows(0).Item("central")
        End If
    End Sub

    Public Sub mObtenerPais()
        Dim lDsDatos As DataSet = clsSQLServer.gExecuteQuery(Session("sConn").ToString(), "paises_consul @pais_defa=1")
        If lDsDatos.Tables(0).Rows.Count > 0 Then
            Session("sPaisDefaId") = lDsDatos.Tables(0).Rows(0).Item("pais_id")
        End If
    End Sub

    Private Function mVerificarUsuario(ByVal pstrUser As String, ByVal pstrPass As String, ByVal pstrTrust As String) As Boolean
        Try
            ' Dario 2013-09-19 Obtengo si puedo el host del usuario que se intenta logear
            Dim lstrHost As String
            Try
                lstrHost = System.Net.Dns.GetHostByAddress(Request.ServerVariables("REMOTE_ADDR")).HostName
            Catch ex As Exception
                lstrHost = "NO IDENTIFICADA"
            End Try

            Session("sHost") = lstrHost

            Dim lstrCmd As String = ""

            lstrCmd = "exec login_consul"
            lstrCmd += " " + clsSQLServer.gFormatArg(pstrUser, SqlDbType.VarChar)
            lstrCmd += "," + clsSQLServer.gFormatArg(pstrPass, SqlDbType.VarChar)
            lstrCmd += "," + clsSQLServer.gFormatArg(pstrTrust, SqlDbType.VarChar)

            Session("sUser") = pstrUser
            Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(Session("sConn").ToString(), lstrCmd)
            While (dr.Read())
                Session("sUserId") = dr.GetValue(0).ToString().Trim()
                Session("sImpreTipo") = dr.GetValue(1).ToString().Trim()
                Session("sUserName") = pstrUser
                ' Dario 2013-07-30 Se agrega el  mail en retorno para no ejecutar
                ' una consulta al pedo luego a la hora de mandar mails
                ' dr.GetValue(2) = usua_email
                If (Not dr.GetValue(2) Is DBNull.Value) Then
                    Session("sUsuaEmail") = dr.GetValue(2).ToString().Trim()
                Else
                    Session("sUsuaEmail") = ""
                End If
                ' Dario 2013-09-19 se agrega dato centro emisor al usuario
                ' dr.GetValue(3) = emct_id
                If (Not dr.GetValue(3) Is DBNull.Value) Then
                    Session("sCentroEmisorId") = dr.GetValue(3).ToString().Trim() ' emct_id
                    Session("sCentroEmisorNro") = dr.GetValue(4).ToString().Trim() ' emct_codi
                    Session("sCentroEmisorCentral") = dr.GetValue(5).ToString().Trim() ' central
                Else
                    Session("sCentroEmisorId") = Nothing  ' emct_id
                    Session("sCentroEmisorNro") = Nothing ' emct_codi
                    Session("sCentroEmisorCentral") = Nothing ' central
                    Msg.Text = "El Usuario " & pstrUser & " no tiene un centro de emisi�n asociado."
                End If

            End While
            dr.Close()
            'conexion exitosa
            If (Not clsSQLServer.gMenuPermi("SRA", Session("sConn").ToString(), (Session("sUserId").ToString()))) Then

                If (pstrTrust = "S") Then
                    Session("sConn") = Nothing
                    Return (False)
                Else

                    Msg.Text = "No tiene permisos para acceder a este sistema."
                    Session("sConn") = Nothing
                    Return (False)
                End If
            Else

                Return (True)
            End If

        Catch ex As Exception
            Msg.Text = "Error: " + ex.Message
            Session("sConn") = Nothing
            Return (False)
        End Try
    End Function

    Private Sub AuditarLogueoXUsuario()
        Try
            Dim strIP As String
            Try
                'IP de la P�gina.
                strIP = System.Net.Dns.GetHostByAddress(Request.ServerVariables("REMOTE_ADDR")).HostName

            Catch ex As Exception
                'Si da error.
                strIP = "NO IDENTIFICADA"
            End Try

            'Instancio el objeto AuditoriaLogUser
            Dim objAudi As New Objetos.AuditoriaLogUser

            'Objeto Auditoria.
            objAudi.UserID = Session("sUserId")
            objAudi.IP = strIP

            'Audito el logueo por usuario.
            clsAuditoria.AuditoriaLogueoXUsuario(objAudi, Session("sConn").ToString())

        Catch ex As Exception
            clsError.gManejarError(Me, ex)
        End Try
    End Sub
End Class
End Namespace
