using System;

namespace Objetos
{
	/// <summary>
	/// Objeto Auditoria Sesiones por Usuario.
	/// </summary>
	public class Auditoria
	{
		private int userID;
		private System.DateTime fechaIngreso;
		private string nombreForm;
		private string modulo;
		private string sistema;
		
		public int UserID
		{
			get{return userID;}
			set{userID = value;}
		}

		public System.DateTime FechaIngreso
		{
			get{return fechaIngreso;}
			set{fechaIngreso = value;}
		}
		
		public string NombreForm
		{
			get{return nombreForm;}
			set{nombreForm = value;}
		}

		public string Modulo
		{
			get{return modulo;}
			set{modulo = value;}
		}

		public string Sistema
		{
			get{return sistema;}
			set{sistema = value;}
		}
	}


	/// <summary>
	/// Objeto Auditoria Logueo por Usuario
	/// </summary>
	public class AuditoriaLogUser
	{
		private int userID;
		private System.DateTime fechaIngreso;
		private string ip;

		public int UserID
		{
			get{return userID;}
			set{userID = value;}
		}

		public System.DateTime FechaIngreso
		{
			get{return fechaIngreso;}
			set{fechaIngreso = value;}
		}
		
		public string IP
		{
			get{return ip;}
			set{ip = value;}
		} 
	}
}
