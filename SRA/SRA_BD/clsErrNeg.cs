using System;

namespace AccesoBD
{
	/// <summary>
	/// Clase que contiene los errores de validacion. Es distinta para poder distinguirla de los errores no controlados.
	/// </summary>
	public class clsErrNeg:Exception
	{
		public clsErrNeg(string pstrMsg):base(pstrMsg)
		{
		}

	}
}
