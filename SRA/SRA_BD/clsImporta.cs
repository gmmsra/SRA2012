using System;
using System.Data;
using System.Data.OleDb;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Globalization;

namespace clsImporta
{
	/// <summary>
	/// Summary description for interfaz.
	/// </summary>
	/// 
	public class clsAbrirArchivo : System.Web.UI.Page
	{
		public static System.Data.DataSet mImportarExcel(string pstrFile,string pstrCampos,string pstrCondicion)
		{
			return(mImportarExcel(pstrFile,pstrCampos,pstrCondicion,""));
		}

		public static System.Data.DataSet mImportarExcel(string pstrFile,string pstrCampos,string pstrCondicion,string pstrTabla)
		{
			OleDbConnection con = null;
			DataSet myDataSet = new DataSet();
			try
			{
				string strConn;

				strConn = "Provider=Microsoft.Jet.OLEDB.4.0;" +
					"Data Source=" + pstrFile +
					";Extended Properties=Excel 8.0;";

				con = new OleDbConnection(strConn);
				con.Open();
				object[] objArrRestrict;
				objArrRestrict = new object[] {null, null, null, null};
				DataTable tbl;
				tbl = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, objArrRestrict);

				string comm = "";
				if (pstrTabla != "")
					comm = "SELECT " + pstrCampos + " FROM [" + pstrTabla + "$] " + pstrCondicion;
				else
					comm = "SELECT " + pstrCampos + " FROM [" + tbl.Rows[0]["TABLE_NAME"] + "$] " + pstrCondicion;
								
				OleDbDataAdapter myCommand = new System.Data.OleDb.OleDbDataAdapter(comm, con);
				
				myCommand.Fill(myDataSet , "ExcelInfo");
				con.Close();
				return myDataSet;
				
			}	
			catch (Exception ex)
			{
				if(con!=null)
					con.Close();
				throw(ex);
			}
		}
		
		public static System.Data.DataSet mImportarExcelAsoc(string pstrFile,string pstrCampos,string pstrCondicion,string pstrTabla)
		{
			OleDbConnection con = null;
			DataSet myDataSet = new DataSet();
			try
			{
				string strConn;

				strConn = "Provider=Microsoft.Jet.OLEDB.4.0;" +
					"Data Source=" + pstrFile +
					";Extended Properties=Excel 8.0;";

				con = new OleDbConnection(strConn);
				con.Open();
				object[] objArrRestrict;
				objArrRestrict = new object[] {null, null, null, null};
				DataTable tbl;
				tbl = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, objArrRestrict);

				string comm = "";
				if (pstrTabla != "")
					comm = "SELECT " + pstrCampos + " FROM [" + pstrTabla + "$] " + pstrCondicion;
				else
					comm = "SELECT " + pstrCampos + " FROM [" + tbl.Rows[0]["TABLE_NAME"] + "$] " + pstrCondicion;
								
				OleDbDataAdapter myCommand = new System.Data.OleDb.OleDbDataAdapter(comm, con);
				
				myCommand.Fill(myDataSet , pstrTabla);
				con.Close();
				return myDataSet;
				
			}	
			catch (Exception ex)
			{
				if(con!=null)
					con.Close();
				throw(ex);
			}
		}

		public static System.Data.DataSet mImportarDBF(string pstrArch,string pstrCampos,string pstrCondicion,string pstrPath)
		{
			OleDbConnection con = null;
			DataSet myDataSet = new DataSet();
			try
			{
				string strConn;

				strConn = "Provider=Microsoft.Jet.OLEDB.4.0;" +
					"Data Source=" + pstrPath +
					";Extended Properties=dBase IV;";

				con = new OleDbConnection(strConn);
				con.Open();
				object[] objArrRestrict;
				objArrRestrict = new object[] {null, null, null, null};
				DataTable tbl;
				tbl = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, objArrRestrict);

				string comm = "SELECT " + pstrCampos + " FROM " + pstrArch + " " + pstrCondicion;
				
				OleDbDataAdapter myCommand = new System.Data.OleDb.OleDbDataAdapter(comm, con);
				
				myCommand.Fill(myDataSet);
				con.Close();
				return myDataSet;
				
			}	
			catch (Exception ex)
			{
				if(con!=null)
					con.Close();
				throw(ex);
			}
		}	
	}
}
