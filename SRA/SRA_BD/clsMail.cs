using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.Mail;
using System.Web;
using System.IO;
using System.Configuration;

namespace AccesoBD
{
	/// <summary>
	/// Summary description for clsMail.
	/// </summary>
	public class clsMail
	{
		public static void gEnviarMail(string pstrPara, string pstrAsunto, string pstrMensaje)
		{
			//string lstrMailMode = System.Configuration.ConfigurationSettings.AppSettings["conMailMode"].ToString();
            string lstrMailMode = ConfigurationManager.AppSettings["conMailMode"].ToString();
            gEnviarMail(pstrPara, pstrAsunto, pstrMensaje, lstrMailMode);
		}
		public static void gEnviarMail(string pstrPara, string pstrAsunto, string pstrMensaje, string pstrTemplate)
		{
			//string fichero = @"d:\wwwroot\gsviajes\templates\template.htm";
			//System.IO.StreamReader sr = new System.IO.StreamReader(fichero);
			//string texto = sr.ReadToEnd();
			//sr.Close();

			//string lstrMailServ = System.Configuration.ConfigurationSettings.AppSettings["conMailServ"].ToString();
			//string lstrMailFrom = System.Configuration.ConfigurationSettings.AppSettings["conMailFrom"].ToString();
            string lstrMailServ = ConfigurationManager.AppSettings["conMailServ"].ToString();
            string lstrMailFrom = ConfigurationManager.AppSettings["conMailFrom"].ToString();
            string lstrMailMode = pstrTemplate;
			MailMessage lmsgMail = new MailMessage();

			// Activar Autenticación
			lmsgMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", System.Configuration.ConfigurationSettings.AppSettings["smtpauthenticate"].ToString());
			// Nombre de usuario
			lmsgMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", System.Configuration.ConfigurationSettings.AppSettings["sendusername"].ToString());
			// Contraseña
			lmsgMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationSettings.AppSettings["sendpassword"].ToString());

			lmsgMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", System.Configuration.ConfigurationSettings.AppSettings["smtpPuerto"].ToString());

			lmsgMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2");

			lmsgMail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", System.Configuration.ConfigurationSettings.AppSettings["EnableSsl"].ToString());


			lmsgMail.From = lstrMailFrom;
			lmsgMail.To = pstrPara;
			lmsgMail.Cc = "";
			lmsgMail.Bcc = "";
			lmsgMail.Subject = pstrAsunto;			
			lmsgMail.Body = gLeerTemplate(HttpContext.Current.Server.MapPath(lstrMailMode),pstrMensaje);
			//lmsgMail.BodyEncoding = Encoding.ASCII.EncodingName;
			//lmsgMail.Attachments.Add(myAttachment)
			lmsgMail.BodyFormat = MailFormat.Html;
			lmsgMail.Priority = MailPriority.Normal;
			SmtpMail.SmtpServer = lstrMailServ;
			SmtpMail.Send(lmsgMail);
		}
		public static string gLeerTemplate(string pstrFile, string pstrMens)
		{
			System.IO.StreamReader sr = new System.IO.StreamReader(pstrFile);
			string lstrFile = sr.ReadToEnd();
			sr.Close();
			lstrFile = lstrFile.Replace("<!-- [mensaje] -->", pstrMens);
			return(lstrFile);
		}
	}
}
