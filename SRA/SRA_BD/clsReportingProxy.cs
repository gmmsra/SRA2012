using System;
using AccesoBD;
using System.Configuration;
namespace clsReportingServices
{
	/// <summary>
	/// Descripción breve de clsReportingProxy.
	/// </summary>
	public class clsReportingProxy
	{
		public static string gReportServer()
		{
			//string lstrRepo =  System.Configuration.ConfigurationSettings.AppSettings["conRepo"].ToString();
            string lstrRepo =  ConfigurationManager.AppSettings["conRepo"].ToString();
            return (lstrRepo);
		}

		private static string gReportFolder()
		{
			//string lstrRepo = System.Configuration.ConfigurationSettings.AppSettings["conRepoDire"].ToString();
            string lstrRepo = ConfigurationManager.AppSettings["conRepoDire"].ToString();
            return (lstrRepo);
		}

		public static string gIniciarReporte(System.Web.HttpRequest oRequest, string pstrRptName)
		{
			string lstrRpt = "";

			lstrRpt = "http://" + gReportServer() + GetApplicationPath();  //oRequest.ApplicationPath;
			lstrRpt += "/rptproxy.aspx?%2f" + gReportFolder() + "%2f" + pstrRptName;
			lstrRpt += "&rs%3aClearSession=true&rs%3aFormat=HTML4.0&rs%3aCommand=Render";
			clsError.gGenerarMensajes("gIniciarReporte",  "", lstrRpt);
			return(lstrRpt);
		}

		private static string GetApplicationPath()
        {
			return ConfigurationManager.AppSettings["applicationPath"].ToString();
        }

		public static string gSetearOpcionesReporte(string pstrRpt)
		{

			return(gSetearOpcionesReporte(pstrRpt,true));
		}

		public static string gSetearOpcionesReporte(string pstrRpt, bool pbooTolbar)
		{

			string lstrRpt = pstrRpt;
			lstrRpt += "&rc%3aParameters=false&rc%3aLinkTarget=_top";
			lstrRpt += "&rc%3aToolbar=" + (pbooTolbar?"True" : "False") + "&rc%3aReplacementRoot=http%3a%2f%2f";
			lstrRpt += gReportServer() + "%2fReports%2fPages%2fReport.aspx%3fServerUrl%3d";
			clsError.gGenerarMensajes("gSetearOpcionesReporte",  "", lstrRpt);
			return(lstrRpt);
		}
	}
}
