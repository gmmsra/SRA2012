using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration; 

namespace AccesoBD
{
	public sealed class clsSQLServer
	{

		#region Constructor
		public clsSQLServer()
		{
			//
			// TODO: Add constructor logic here
			//
//			if (SqlConn == null)
//			{
//				SqlConn = new SqlConnection();
//				//SqlConn.ConnectionString = ConfigurationManager.ConnectionStrings["SqlProvider"].ConnectionString;
//			}

		}
		#endregion

		#region PrivateVariables
		//private SqlConnection SqlConn;
		#endregion

		#region PrivateMethods

		private void OpenSqlConnection()
		{
//			if (SqlConn.State != ConnectionState.Open)
//			{
//				SqlConn.Open();
//			}
		}


		private void CloseSqlConnection()
		{
//			if (SqlConn.State == ConnectionState.Open)
//			{
//				SqlConn.Close();
//			}
		}

		#endregion

		#region PublicMethods

		public static bool gMenuPermi(string pstrOpcion, string pstrConn, string pstrUserId)
		{
			string lstrCmd = "exec opcion_permi_consul ";
			lstrCmd = lstrCmd + " " + gFormatArg(pstrOpcion,SqlDbType.VarChar);
			lstrCmd = lstrCmd + "," + gFormatArg(pstrUserId,SqlDbType.Int);

			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			cmdExec.CommandText = lstrCmd;
			SqlDataReader dr = cmdExec.ExecuteReader();	
			while (dr.Read())
			{
				if (dr.GetValue(0).ToString().Trim() == "S")
				{
					dr.Close();	
					myConnection.Close();			
					return(true);
				}
			}
			dr.Close();	
			myConnection.Close();			
			//return(false);
			return(true);
		}			

		public static bool gConsultarPermisoBoton(string pstrOpcion, string pstrConn, string pstrUserId)
		{
			string lstrCmd = "exec opcion_permi_boton_consul ";
			lstrCmd = lstrCmd + " " + gFormatArg(pstrOpcion,SqlDbType.Int);
			lstrCmd = lstrCmd + "," + gFormatArg(pstrUserId,SqlDbType.Int);

			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			cmdExec.CommandText = lstrCmd;
			SqlDataReader dr = cmdExec.ExecuteReader();	
			while (dr.Read())
			{
				if (dr.GetValue(0).ToString().Trim() == "S")
				{
					dr.Close();	
					myConnection.Close();			
					return(true);
				}
			}
			dr.Close();	
			myConnection.Close();			
			return(false);			
		}			
		
		public static string gConsultarValor(string pstrCmd, string pstrConn, int pintRegiPosi)
		{
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			cmdExec.CommandText = pstrCmd;
			SqlDataReader dr = cmdExec.ExecuteReader();	
			string lstrValor = "";
			while (dr.Read())
			{
				lstrValor = dr.GetValue(pintRegiPosi).ToString().Trim();
			}
			dr.Close();	
			myConnection.Close();			
			return(lstrValor);			
		}			

		public static object gObtenerValorCampo(string pstrConn, string pstrTabla, string pstrParam, string pstrCampo)
		{
			object lobjValor = DBNull.Value;
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			cmdExec.CommandText = pstrTabla + "_consul " + pstrParam;
			SqlDataReader dr = cmdExec.ExecuteReader();

			while (dr.Read())
			{
				lobjValor = dr.GetValue(dr.GetOrdinal(pstrCampo));
			}
			dr.Close();
			myConnection.Close();
			return(lobjValor);
		}

		public static DataSet gParaConsul(string pstrConn)
		{
			SqlDataAdapter cmdExecCommand = new SqlDataAdapter("exec parametros_consul", pstrConn);
			DataSet ds = new DataSet();
			cmdExecCommand.Fill(ds);
			return (ds);            
		}	
		public static string gParaFieldConsul(string pstrConn, ref string pstrParaPageSize)
		{
			string lstrField = "";
			int lOrden;
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			string lstrCmd = "exec parametros_consul ";
			cmdExec.CommandText = lstrCmd;
			SqlDataReader dr = cmdExec.ExecuteReader();	
			while (dr.Read())
			{
				lOrden=dr.GetOrdinal("para_page_size_grilla");
				pstrParaPageSize = dr.GetValue(lOrden).ToString();
			}
			dr.Close();	
			myConnection.Close();
			return(lstrField);
		}	
		public static string gCampoValorConsul(string pstrConn, string pstrsp,string pstrCampo)
		{
			string lstrField = "";
			int lOrden;
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			string lstrCmd = "exec " + pstrsp;
			cmdExec.CommandText = lstrCmd;
			SqlDataReader dr = cmdExec.ExecuteReader();	
			while (dr.Read())
			{
				lOrden=dr.GetOrdinal(pstrCampo);
				lstrField = dr.GetValue(lOrden).ToString();
			}
			dr.Close();	
			myConnection.Close();
			return(lstrField);
		}	
		
		
		public static string gParametroValorConsul(SqlTransaction Trans, string pstrCampo)
		{
			return(gParametroValorConsul(Trans.Connection.ConnectionString ,pstrCampo));
		}

		public static string gParametroValorConsul(string pstrConn, string pstrCampo)
		{
			string lstrField = "";
			int lOrden;
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			string lstrCmd = "exec parametros_consul ";
			cmdExec.CommandText = lstrCmd;
			SqlDataReader dr = cmdExec.ExecuteReader();	
			while (dr.Read())
			{
				lOrden=dr.GetOrdinal(pstrCampo);
				lstrField = dr.GetValue(lOrden).ToString();
			}
			dr.Close();	
			myConnection.Close();
			return(lstrField);
		}	

		
		public static DataSet gExecuteQuery(string pstrConn, string pstrProc)
		{
			try
			{
				if (pstrConn.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrConn");
				}
				if (pstrProc.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrProc");
				}

				SqlDataAdapter cmdExecCommand = new SqlDataAdapter(pstrProc, pstrConn);
				DataSet ds = new DataSet();
				cmdExecCommand.Fill(ds);
			
				return (ds);
			}
			catch (Exception ex)
			{
				clsError.gManejarError(new Exception(ex.Message + " - " + pstrProc, ex));
				throw(ex);
			}
		}
		
		public static DataSet gExecuteQuery(string pstrConn, string pstrProc, int pintTimeOut)
		{
			try
			{
				if (pstrConn.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrConn");
				}
				if (pstrProc.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrProc");
				}

				SqlDataAdapter cmdExecCommand = new SqlDataAdapter(pstrProc, pstrConn);
				if (cmdExecCommand.SelectCommand != null)
				{cmdExecCommand.SelectCommand.CommandTimeout = pintTimeOut;}
				if (cmdExecCommand.DeleteCommand != null)
				{cmdExecCommand.DeleteCommand.CommandTimeout = pintTimeOut;}
				if (cmdExecCommand.UpdateCommand != null)
				{cmdExecCommand.UpdateCommand.CommandTimeout = pintTimeOut;}
				if (cmdExecCommand.InsertCommand != null)
				{cmdExecCommand.InsertCommand.CommandTimeout = pintTimeOut;}
				
				
				DataSet ds = new DataSet();
				cmdExecCommand.Fill(ds);
			
				return (ds);
			}
			catch (Exception ex)
			{
				clsError.gManejarError(new Exception(ex.Message + " - " + pstrProc, ex));
				throw(ex);
			}
		}
		public static DataSet gExecuteQuery(SqlTransaction Trans, string pstrProc)
		{
			if (Trans == null)
			{
				ArgumentNullException Throw = new ArgumentNullException("Trans");
			}

			//SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlDataAdapter cmdExecCommand = new SqlDataAdapter(pstrProc,  Trans.Connection);
			cmdExecCommand.SelectCommand.Transaction = Trans;
			DataSet ds = new DataSet();

			//myConnection.Open();
			cmdExecCommand.Fill(ds);
			//myConnection.Close();
		
			return (ds);
		}

		
		/// <summary>
		/// sobrecarga con timeout
		/// </summary>
		/// <param name="Trans"></param>
		/// <param name="pstrProc"></param>
		/// <returns>DataSet</returns>
		public static DataSet gExecuteQuery(SqlTransaction Trans, string pstrProc, int pintTimeOut)
		{
			if (Trans == null)
			{
				ArgumentNullException Throw = new ArgumentNullException("Trans");
			}
			//SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlDataAdapter cmdExecCommand = new SqlDataAdapter(pstrProc,  Trans.Connection);
			cmdExecCommand.SelectCommand.Transaction = Trans;
			cmdExecCommand.SelectCommand.CommandTimeout = pintTimeOut;
			DataSet ds = new DataSet();

			//myConnection.Open();
			cmdExecCommand.Fill(ds);
			//myConnection.Close();
		
			return (ds);
		}

		
		public static SqlDataReader gExecuteQueryDR(string pstrConn, string pstrProc)
		{
			try
			{
				SqlConnection myConnection = new SqlConnection(pstrConn);
				SqlCommand cmdExec = new SqlCommand();
				myConnection.Open();
				cmdExec.Connection = myConnection;
				cmdExec.CommandType = CommandType.Text;
				cmdExec.CommandText = pstrProc;
				SqlDataReader dr = cmdExec.ExecuteReader(CommandBehavior.CloseConnection);

				return (dr);
			}
			catch (Exception ex)
			{
				clsError.gManejarError(new Exception(ex.Message + " - " + pstrProc, ex));
				throw(ex);
			}
		}

		public static int gExecute(string pstrConn, string pstrProc) 
		{
			SqlTransaction Trans=null;
			try
			{
				if (pstrConn.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrConn");
				}
				if (pstrProc.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrProc");
				}
				
				SqlConnection myConnection = new SqlConnection(pstrConn);
				SqlCommand cmdExecCommand = new SqlCommand();
				cmdExecCommand.Connection = myConnection;
				cmdExecCommand.CommandText = pstrProc;
				cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
				myConnection.Open();
				Trans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted,"NixorTransac"); 			
				cmdExecCommand.Transaction = Trans;
				int lintExec = cmdExecCommand.ExecuteNonQuery();
				Trans.Commit();
				myConnection.Close();
				return(lintExec);
			}
			catch (Exception ex)
			{
				if(Trans!=null) Trans.Rollback();
				clsError.gManejarError(new Exception(ex.Message + " - " + pstrProc, ex));
				throw(ex);
			}
		}


		public static int gExecuteNONQuery(SqlTransaction Trans, string pstrProc)
		{
			try
			{

				SqlDataAdapter cmdAdapter = new SqlDataAdapter(pstrProc,Trans.Connection );

				cmdAdapter.SelectCommand.Transaction = Trans;
				DataSet ds = new DataSet();
				cmdAdapter.Fill(ds);

				int lintExec = 0;

				if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count >0 )
				{
                      lintExec=  int.Parse(ds.Tables[0].Rows[0][0].ToString());

				}
		
				return(lintExec);
			}
			catch(Exception ex)
			{
				clsError.gManejarError(new Exception( ex.Message + " - " + pstrProc , ex));
				throw(ex);
			}
		}

		public static int gExecuteNONQuery(string pstrConn, string pstrProc) 
		{
			 SqlTransaction Trans= null;
			try
			{
				if (pstrConn.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrConn");
				}
				if (pstrProc.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrProc");
				}
				SqlConnection myConnection = new SqlConnection(pstrConn);
				SqlCommand cmdExecCommand = new SqlCommand();
				cmdExecCommand.Connection = myConnection;
				cmdExecCommand.CommandText = pstrProc;
				cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
				myConnection.Open();
				Trans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted,"NixorTransac"); 			
				cmdExecCommand.Transaction = Trans;

				SqlDataAdapter cmdAdapter = new SqlDataAdapter(pstrProc,Trans.Connection );

				cmdAdapter.SelectCommand.Transaction = Trans;
				DataSet ds = new DataSet();
				cmdAdapter.Fill(ds);

				int lintExec = 0;

				if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count >0 )
				{
					lintExec=  int.Parse(ds.Tables[0].Rows[0][0].ToString());

				}
				Trans.Commit();
				myConnection.Close();
				return(lintExec);
			}
			catch (Exception ex)
			{
				if(Trans!=null) Trans.Rollback();
				clsError.gManejarError(new Exception(ex.Message + " - " + pstrProc, ex));
				throw(ex);
			}
		}


		public static int gExecuteSinTransac(string pstrConn, string pstrProc)
		{
			try
			{
				if (pstrConn.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrConn");
				}
				if (pstrProc.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrProc");
				}
				
				SqlConnection myConnection = new SqlConnection(pstrConn);
				SqlCommand cmdExecCommand = new SqlCommand();
				cmdExecCommand.Connection = myConnection;
				cmdExecCommand.CommandText = pstrProc;
				cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
				myConnection.Open();
				int lintExec = cmdExecCommand.ExecuteNonQuery();
				myConnection.Close();
				return(lintExec);
			}
			catch (Exception ex)
			{
				clsError.gManejarError(new Exception(ex.Message + " - " + pstrProc, ex));
				throw(ex);
			}
		}
		public static object gExecuteScalar(string pstrConn, string pstrProc)
		{
			if (pstrConn.Length == 0)
			{
				ArgumentNullException Throw = new ArgumentNullException("pstrConn");
			}
			if (pstrProc.Length == 0)
			{
				ArgumentNullException Throw = new ArgumentNullException("pstrProc");
			}
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = myConnection;
			cmdExecCommand.CommandText = pstrProc;
			cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
			myConnection.Open();
			object lobjRet = cmdExecCommand.ExecuteScalar();
			myConnection.Close();
			return(lobjRet);
		}

		public static string gExecuteScalarTrans(string pstrConn, string pstrProc)
		{
			SqlTransaction Trans=null;
			try
			{
				if (pstrConn.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrConn");
				}
				if (pstrProc.Length == 0)
				{
					ArgumentNullException Throw = new ArgumentNullException("pstrProc");
				}
				SqlConnection myConnection = new SqlConnection(pstrConn);
				SqlCommand cmdExecCommand = new SqlCommand();
				cmdExecCommand.Connection = myConnection;
				cmdExecCommand.CommandText = pstrProc;
				cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
				myConnection.Open();
				Trans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted,"NixorTransac"); 			
				cmdExecCommand.Transaction = Trans;
				string lstrAltaId = cmdExecCommand.ExecuteScalar().ToString();
				Trans.Commit();
				myConnection.Close();
				return(lstrAltaId);
			}
			catch (Exception ex)
			{
				if(Trans!=null) Trans.Rollback();
				clsError.gManejarError(new Exception(ex.Message + " - " + pstrProc, ex));
				throw(ex);
			}
		}

		public static string gExecuteScalarTransParam(SqlTransaction pTrans, SqlConnection pstrConn, string pstrProc)
		{
						
			//SqlConnection myConnection = new SqlConnection(pstrConn);
			
			SqlCommand cmdExecCommand = new SqlCommand();
			cmdExecCommand.Connection = pstrConn;
			//cmdExecCommand.CommandType = 
			cmdExecCommand.CommandText = pstrProc;
			cmdExecCommand.CommandTimeout = pstrConn.ConnectionTimeout;
			//pstrConn.Open();
			cmdExecCommand.Transaction = pTrans;
			string lstrAltaId = cmdExecCommand.ExecuteScalar().ToString();
			//pstrConn.Close();
			return(lstrAltaId);
			
		}
		
		public static int gExecute(SqlTransaction Trans, string pstrProc)
		{
			try
			{
				SqlCommand cmdExecCommand = new SqlCommand();
				cmdExecCommand.Connection = Trans.Connection;
				cmdExecCommand.CommandText = pstrProc;
				cmdExecCommand.CommandTimeout = cmdExecCommand.Connection.ConnectionTimeout;
				cmdExecCommand.Transaction = Trans;
				int lintExec = cmdExecCommand.ExecuteNonQuery();
				return(lintExec);
			}
			catch(Exception ex)
			{
				clsError.gManejarError(new Exception( ex.Message + " - " + pstrProc , ex));
				throw(ex);
			}
		}

		public static string gExecuteScalar(SqlTransaction Trans, string pstrProc)
		{
			try
			{
				SqlCommand cmdExecCommand = new SqlCommand();
				cmdExecCommand.Connection = Trans.Connection;
				cmdExecCommand.CommandTimeout = cmdExecCommand.Connection.ConnectionTimeout;
				cmdExecCommand.CommandText = pstrProc;
				cmdExecCommand.Transaction = Trans;
				string lstrAltaId = cmdExecCommand.ExecuteScalar().ToString();
				return(lstrAltaId);
			}
			catch(Exception ex)
			{
				clsError.gManejarError(new Exception( ex.Message + " - " + pstrProc , ex));
				throw(ex);
			}
		}

		public static SqlTransaction gObtenerTransac(string pstrConn)
		{
			SqlTransaction Trans;
			SqlConnection myConnection = new SqlConnection(pstrConn);
		
			myConnection.Open();
			Trans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted,"NixorTransac");
			return(Trans);
		}

		public static void gCommitTransac(SqlTransaction Trans)
		{
			if(Trans!=null && Trans.Connection!=null) 
			{
				Trans.Commit();
			}
		}
		
		/// <summary>
		/// Dario 2013-07-10 Metodo que close transaction
		/// </summary>
		/// <param name="Trans"></param>
		public static void gCloseTransac(SqlTransaction Trans)
		{
			if(Trans!=null && Trans.Connection!=null) 
			{
				Trans.Connection.Close();
				Trans.Dispose();
			}
		}	

		public static void gRollbackTransac(SqlTransaction Trans)
		{
			if(Trans!=null && Trans.Connection!=null) 
			{
				Trans.Rollback();

			}
		}

		
		public static DataSet gObtenerEstruc(string pstrConn, string pstrTabla)
		{
			return (gObtenerEstruc(pstrConn,pstrTabla, ""));
		}			

		public static DataSet gObtenerEstruc(SqlConnection myConnection, string pstrTabla)
		{
			SqlDataAdapter cmdExecCommand;
//			if(pstrId=="")
				cmdExecCommand = new SqlDataAdapter("exec " + pstrTabla + "_consul @formato='F'", myConnection.ConnectionString);
//			else
//				cmdExecCommand = new SqlDataAdapter("exec " + pstrTabla + "_consul " + pstrId, myConnection.ConnectionString);

			DataSet ds = new DataSet();
//			myConnection.Open();
			cmdExecCommand.Fill(ds);
			
//			if(pstrId=="")
				ds.Tables[0].Rows.Add(ds.Tables[0].NewRow() );
			
			return (ds);            

		}
		public static DataSet gObtenerEstruc(string pstrConn, string pstrTabla, string pstrId)
		{
			SqlDataAdapter cmdExecCommand;
			if(pstrId=="" )
				cmdExecCommand = new SqlDataAdapter("exec " + pstrTabla + "_consul @formato='F'", pstrConn);
			else
				cmdExecCommand = new SqlDataAdapter("exec " + pstrTabla + "_consul " + pstrId, pstrConn);

			DataSet ds = new DataSet();
			cmdExecCommand.Fill(ds);
			
			ds.Tables[0].TableName = pstrTabla;

			if(pstrId=="")
				ds.Tables[0].Rows.Add(ds.Tables[0].NewRow() );
			
			return (ds);            
		}			
		

		/// <summary>
		/// Dario 2013-07-10 sobrecarga con transaccion 
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pstrId"></param>
		/// <returns></returns>
		public static DataSet gObtenerEstruc(SqlTransaction Transac, string pstrTabla, string pstrId)
		{
			SqlDataAdapter cmdExecCommand;
			if(pstrId=="" )
				cmdExecCommand = new SqlDataAdapter("exec " + pstrTabla + "_consul @formato='F'", Transac.Connection);
			else
				cmdExecCommand = new SqlDataAdapter("exec " + pstrTabla + "_consul " + pstrId, Transac.Connection);

			DataSet ds = new DataSet();
			cmdExecCommand.Fill(ds);
			
			ds.Tables[0].TableName = pstrTabla;

			if(pstrId=="")
				ds.Tables[0].Rows.Add(ds.Tables[0].NewRow() );
			
			return (ds);            
		}			

		public static DataSet gObtenerEstrucParam(string pstrConn, string pstrTabla, string pstrId, string pstrOpci)
		{
			SqlDataAdapter cmdExecCommand;
			
			if(pstrId=="")
				cmdExecCommand = new SqlDataAdapter("exec " + pstrTabla + "_consul @formato='F'", pstrConn);
			else
				cmdExecCommand = new SqlDataAdapter("exec " + pstrTabla + "_consul " + pstrId + "," + pstrOpci, pstrConn);

			
			DataSet ds = new DataSet();
			cmdExecCommand.Fill(ds,pstrConn);
			
			ds.Tables[0].TableName = pstrTabla;

			if(pstrId=="")
				ds.Tables[0].Rows.Add(ds.Tables[0].NewRow() );
			
			return (ds);            
		}

		public static string[,] gCargarLongitudes(string pstrConn, string pstrTabla)
		{
			string lstrCmd;
			lstrCmd = "exec nix_estructura_consul";
			lstrCmd += " " + gFormatArg(pstrTabla, SqlDbType.VarChar);

			DataSet ds = gExecuteQuery(pstrConn, lstrCmd);
			string[,] larrCamp = new string[ds.Tables[0].Rows.Count, 4];

			int lintIndi;
			lintIndi = 0;

			foreach(DataRow dr in ds.Tables[0].Rows)
			{
				larrCamp[lintIndi,0] = dr[1].ToString();
				larrCamp[lintIndi,1] = dr[2].ToString();
				larrCamp[lintIndi,2] = dr[3].ToString();
				larrCamp[lintIndi,3] = dr[0].ToString();
				lintIndi = lintIndi + 1;
			}

			return(larrCamp);
		}

		public static int gObtenerLongitud(string[,] pstrarrCamp, string pstrCampo)
		{
			for(int i=0;i<=pstrarrCamp.GetUpperBound(0);i++)
			{
				if(pstrarrCamp[i,3]==pstrCampo)
					return(int.Parse(pstrarrCamp[i,1]));
			}
			return(0);
		}
		public static string gFormatArg(string pstrParam, SqlDbType pstrTipo)
		{
			if (pstrParam == null)
			{
				return("null");
			}
			string lstrAux = pstrParam.Trim();
			switch (pstrTipo)
			{
				case SqlDbType.VarChar:
					if(lstrAux.IndexOf("'")==-1)
						lstrAux = "'" + lstrAux + "'";
					else
						lstrAux = "[" + lstrAux + "]";
					return(lstrAux);
				case SqlDbType.SmallDateTime:
					if (lstrAux != "")
					{
						IFormatProvider format = new System.Globalization.CultureInfo("fr-FR", true);
						DateTime datFech = DateTime.Parse(lstrAux, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
						lstrAux = datFech.Month.ToString()+"/"+datFech.Day.ToString()+"/"+datFech.Year.ToString();
						lstrAux = "'" + lstrAux + "'";
					}
					else
					{
						lstrAux = "null";
					}
					return(lstrAux);
				case SqlDbType.Int:
					if (lstrAux == "")
					{
						lstrAux = "0";
					}
					return(lstrAux);
				case SqlDbType.Decimal:
					if (lstrAux == "")
					{
						lstrAux = "0";
					}
					lstrAux = lstrAux.Replace(",",".");
					return(lstrAux);
				default:
					return(lstrAux);
			}
		}
		
		//gBaja

		public static void gBaja(string pstrConn, string pstrUserId, string pstrTabla, string lstrId)
		{
			gBaja(pstrConn, null, pstrUserId, pstrTabla, lstrId);
		}

		public static void gBaja(SqlTransaction Trans, string pstrUserId, string pstrTabla, string lstrId, Boolean Audi)
		{
			gBaja("", Trans, pstrUserId, pstrTabla, lstrId, Audi);
		}

		public static void gBaja(SqlTransaction Trans, string pstrUserId, string pstrTabla, string lstrId)
		{
			gBaja("", Trans, pstrUserId, pstrTabla, lstrId);
		}

		private static void gBaja(string pstrConn, SqlTransaction Trans, string pstrUserId, string pstrTabla, string lstrId)
		{
			string lstrCmd = "";
			DataSet ds;

			try
			{
				if(Trans!=null) pstrConn=Trans.Connection.ConnectionString;

				lstrCmd = "exec nix_estructura_consul";
				lstrCmd += " " + gFormatArg(pstrTabla, SqlDbType.VarChar);				
				
				if(Trans==null)
					ds = gExecuteQuery(pstrConn, lstrCmd);
				else
					ds = gExecuteQuery(Trans, lstrCmd);		

				lstrCmd = "exec " + pstrTabla + "_baja " + lstrId;

				//si existe el campo de auditoria le pasa el usuario
				if(ds.Tables[0].Select("name='" + ds.Tables[0].Rows[0][0].ToString().Substring(0, ds.Tables[0].Rows[0][0].ToString().Length-3) + "_audi_user'").GetUpperBound(0) != -1)
					lstrCmd += "," + pstrUserId;				

			
				if(Trans==null) 
					gExecute(pstrConn, lstrCmd);
				else
					gExecute(Trans, lstrCmd);
			}

			catch(Exception ex)
			{
				clsError.gManejarError(new Exception( ex.Message + " - " + lstrCmd , ex));
				throw(ex);
			}
		}

		//gAlta

		//Ale Ruetalo
		private static void gBaja(string pstrConn, SqlTransaction Trans, string pstrUserId, string pstrTabla, 
			string lstrId, Boolean Audi)
		{			
			string lstrCmd = string.Empty;
			DataSet ds;			

			try
			{
				if(Trans!=null) pstrConn=Trans.Connection.ConnectionString;

				lstrCmd = "exec nix_estructura_consul";
				lstrCmd += " " + gFormatArg(pstrTabla, SqlDbType.VarChar);				
				
				if(Trans==null)
					ds = gExecuteQuery(pstrConn, lstrCmd);
				else
					ds = gExecuteQuery(Trans, lstrCmd);		

				lstrCmd = "exec " + pstrTabla + "_baja " + lstrId;

				//si existe el campo de auditoria le pasa el usuario
				if(ds.Tables[0].Select("name='" + ds.Tables[0].Rows[0][0].ToString().Substring
					(0, ds.Tables[0].Rows[0][0].ToString().Length-3) + "_audi_user'").GetUpperBound(0) != -1)
					lstrCmd += "," + pstrUserId;				
			
				if(Trans==null) 
					gExecute(pstrConn, lstrCmd);
				else
					gExecute(Trans, lstrCmd);
			}

			catch(Exception ex)
			{
				clsError.gManejarError(new Exception( ex.Message + " - " + lstrCmd , ex));
				throw(ex);
			}
		}


		public static string gAlta(string pstrConn, string pstrUserId, string pstrTabla, DataSet pdsDatos)
		{
			return gAlta(pstrConn, pstrUserId, pstrTabla, pdsDatos.Tables[0].Rows[0]);
		}

		public static string gAlta(string pstrConn, string pstrUserId, string pstrTabla, DataRow pdrDatos)
		{
			return(gAlta(pstrConn, null, pstrUserId, pstrTabla, pdrDatos));
		}

		public static Int32 gProximoId(string pstrConn, string pstrTabla, string pstrCampo)
		{
			
			string lstrCmd = "select max(" + pstrCampo + ") as Maximo from " + pstrTabla; 
			Int32 Id=0;
			

			
			SqlConnection myConnection = new SqlConnection(pstrConn);
			SqlCommand cmdExec = new SqlCommand();
			myConnection.Open();
			cmdExec.Connection = myConnection;
			cmdExec.CommandType = CommandType.Text;
			cmdExec.CommandText = lstrCmd;
			SqlDataReader dr = cmdExec.ExecuteReader();
			while (dr.Read())
			{
				if (dr.GetValue(0).ToString() !="")
				{
					Id= dr.GetInt32(0);
				}
				
			}
			dr.Close();
			myConnection.Close();
			return (Id);


			
		}
		
		public static string gAlta(SqlTransaction Trans, string pstrUserId, string pstrTabla, DataRow pdrDatos)
		{
			return(gAlta(null, Trans, pstrUserId, pstrTabla, pdrDatos));
		}

		private static string gAlta(string pstrConn, SqlTransaction Trans, string pstrUserId, 
			 string pstrTabla, DataRow pdrDatos)
		{
			string lstrCmd = "exec " + pstrTabla + "_alta ";
			string lstrSep;

			try
			{
				for(int i=1;i<pdrDatos.Table.Columns.Count;i++)
				{
					if(i==1 && lstrCmd.EndsWith("_alta "))
						lstrSep = ""; 
					else
						lstrSep = ",";

					if(!pdrDatos.Table.Columns[i].ColumnName.StartsWith("_"))
					{
						if(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_user"))
							lstrCmd += lstrSep + "@" + pdrDatos.Table.Columns[i].ColumnName + "=" + pstrUserId;
						else
						{
							if(!(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_fecha")||pdrDatos.Table.Columns[i].ColumnName.EndsWith("_baja_fecha")||(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_stat_id")&& pdrDatos.IsNull(i))||pdrDatos.Table.Columns[i].ColumnName.EndsWith("_tmsp")))
								lstrCmd += lstrSep + gFormatParam(pdrDatos[i],pdrDatos.Table.Columns [i].DataType, pdrDatos.Table.Columns[i].ColumnName);
						}
					}
				}

				string lstrId;
				if(Trans == null)
				{
					clsError.gGenerarMensajes("clsdatabase","debug" ,"sin trans" + lstrCmd);
					lstrId = gExecuteScalarTrans(pstrConn, lstrCmd);
				}

				else
				{
					clsError.gGenerarMensajes("clsdatabase","DEBUG" ,"con transaccion" + lstrCmd);
					lstrId = gExecuteScalar(Trans, lstrCmd);
				}

				return(lstrId);
			}
			catch(Exception ex)
			{
				clsError.gManejarError(new Exception( ex.Message + " - " + lstrCmd , ex));
				throw(ex);
			}
		}


		public static string gAltaExecuteNoQuery(string pstrConn, SqlTransaction Trans, string pstrUserId, 
			string pstrTabla, DataRow pdrDatos)
		{
			string lstrCmd = "exec " + pstrTabla + "_alta ";
			string lstrSep;

			try
			{
				for(int i=1;i<pdrDatos.Table.Columns.Count;i++)
				{
					if(i==1 && lstrCmd.EndsWith("_alta "))
						lstrSep = ""; 
					else
						lstrSep = ",";

					if(!pdrDatos.Table.Columns[i].ColumnName.StartsWith("_"))
					{
						if(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_user"))
							lstrCmd += lstrSep + "@" + pdrDatos.Table.Columns[i].ColumnName + "=" + pstrUserId;
						else
						{
							if(!(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_fecha")||pdrDatos.Table.Columns[i].ColumnName.EndsWith("_baja_fecha")||(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_stat_id")&& pdrDatos.IsNull(i))||pdrDatos.Table.Columns[i].ColumnName.EndsWith("_tmsp")))
								lstrCmd += lstrSep + gFormatParam(pdrDatos[i],pdrDatos.Table.Columns [i].DataType, pdrDatos.Table.Columns[i].ColumnName);
						}
					}
				}

				string lstrId="";

				
				if(Trans == null)
				{
					

					clsError.gGenerarMensajes("clsdatabase","debug" ,"sin trans" + lstrCmd);
					lstrId=  gExecuteNONQuery (pstrConn, lstrCmd).ToString();
					
				}

				else
				{
					clsError.gGenerarMensajes("clsdatabase","DEBUG" ,"con transaccion" + lstrCmd);
				
					lstrId= gExecuteNONQuery(Trans, lstrCmd).ToString();
					
				}

				return(lstrId);
			}
			catch(Exception ex)
			{
				clsError.gManejarError(new Exception( ex.Message + " - " + lstrCmd , ex));
				throw(ex);
			}
		}


		/// <summary>
		/// Recibe del dataset modifica la tabla pasada por parametro solamente,
		/// </summary>
		/// <param name="pstrConn">Conneccion</param>
		/// <param name="pstrUserId">UsuarioId</param>
		/// <param name="pstrTabla">Tabla</param>
		/// <param name="pdsDatos">Dataset</param>
		public static void gModi(string pstrConn, string pstrUserId, string pstrTabla, DataSet pdsDatos)
		{
			gModi(pstrConn, pstrUserId, pstrTabla, pdsDatos.Tables[pstrTabla].Rows[0]);
			//gModi(pstrConn, pstrUserId, pstrTabla, pdsDatos.Tables[0].Rows[0]);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="pstrUserId"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pdrDatos"></param>
		public static void gModi(string pstrConn, string pstrUserId, string pstrTabla, DataRow pdrDatos)
		{
			gModi(pstrConn, null, pstrUserId, pstrTabla, pdrDatos);
		}
	
	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="Trans">Transaccion</param>
		/// <param name="pstrUserId">Usuario Id</param>
		/// <param name="pstrTabla">Tabla a Modificar</param>
		/// <param name="pdrDatos">Datarow </param>
		public static void gModi(SqlTransaction Trans, string pstrUserId, string pstrTabla, DataRow pdrDatos)
		{
			gModi(null, Trans, pstrUserId, pstrTabla, pdrDatos);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="Trans">Transaccion</param>
		/// <param name="pstrUserId">Usuario Id</param>
		/// <param name="pstrTabla">Tabla a Modificar</param>
		/// <param name="pdrDatos">Datarow </param>
		private static void gModi(string pstrConn, SqlTransaction Trans, string pstrUserId, string pstrTabla, DataRow pdrDatos)
		{
			string lstrCmd = "exec " + pstrTabla + "_modi";
			string lstrSep;

			for(int i=0;i<pdrDatos.Table.Columns.Count;i++)
			{
				if(i==0)
					lstrSep = " "; 
				else
					lstrSep = ",";

				if(!pdrDatos.Table.Columns[i].ColumnName.StartsWith("_"))
				{
					if(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_user"))
						lstrCmd += lstrSep + "@" + pdrDatos.Table.Columns[i].ColumnName + "=" + pstrUserId;
					else
					{
						if(!(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_fecha")))
							lstrCmd += lstrSep + gFormatParam(pdrDatos[i],pdrDatos.Table.Columns [i].DataType, pdrDatos.Table.Columns[i].ColumnName);
					}
				}
			}

			
			if(Trans != null)
				gValidarTmsp(Trans, pstrTabla, pdrDatos);

			if(Trans == null)
			{
				gExecute(pstrConn, lstrCmd);
				clsError.gGenerarMensajes("clsdatabase","debug" ,"sin trans" + lstrCmd);
			}
				
			else
			{
				gExecute(Trans, lstrCmd);
				clsError.gGenerarMensajes("clsdatabase","debug" ,"con trans" + lstrCmd);
			}
				
			


		}

		
		/// <summary>
		/// Recibe del dataset modifica la tabla pasada por parametro solamente,
		/// </summary>
		/// <param name="pstrConn">Conneccion</param>
		/// <param name="pstrUserId">UsuarioId</param>
		/// <param name="pstrTabla">Tabla</param>
		/// <param name="pdsDatos">Dataset</param>
		/// <param name="pStoreProc"></param>
	
		public static void gModiSP(string pstrConn, string pstrUserId, string pstrTabla, DataSet pdsDatos,string pStoreProc)
		{
			gModiSP(pstrConn, pstrUserId, pstrTabla, pdsDatos.Tables[pstrTabla].Rows[0],pStoreProc);
			
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="pstrUserId"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pdrDatos"></param>
		/// <param name="pStoreProc"></param>
		public static void gModiSP(string pstrConn, string pstrUserId, string pstrTabla, DataRow pdrDatos,string pStoreProc)
		{
			gModiSP(pstrConn, null, pstrUserId, pstrTabla, pdrDatos,pStoreProc);
		}
	
	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="Trans">Transaccion</param>
		/// <param name="pstrUserId">Usuario Id</param>
		/// <param name="pstrTabla">Tabla a Modificar</param>
		/// <param name="pdrDatos">Datarow </param>
		/// <param name="pStoreProc"></param>
		public static void gModiSP(SqlTransaction Trans, string pstrUserId, string pstrTabla, DataRow pdrDatos,string pStoreProc)
		{
			gModiSP(null, Trans, pstrUserId, pstrTabla, pdrDatos,pStoreProc);
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="Trans"></param>
		/// <param name="pstrUserId"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pdrDatos"></param>
		/// <param name="pStoreProc"></param>
		private static void gModiSP(string pstrConn, SqlTransaction Trans, string pstrUserId, string pstrTabla, DataRow pdrDatos,string pStoreProc)
		{
			string lstrCmd = "exec " + pStoreProc;
			string lstrSep;

			for(int i=0;i<pdrDatos.Table.Columns.Count;i++)
			{
				if(i==0)
					lstrSep = " "; 
				else
					lstrSep = ",";

				if(!pdrDatos.Table.Columns[i].ColumnName.StartsWith("_"))
				{
					if(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_user"))
						lstrCmd += lstrSep + "@" + pdrDatos.Table.Columns[i].ColumnName + "=" + pstrUserId;
					else
					{
						if(!(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_fecha")))
							lstrCmd += lstrSep + gFormatParam(pdrDatos[i],pdrDatos.Table.Columns [i].DataType, pdrDatos.Table.Columns[i].ColumnName);
					}
				}
			}

			
			if(Trans != null)
				gValidarTmsp(Trans, pstrTabla, pdrDatos);

			if(Trans == null)
				gExecute(pstrConn, lstrCmd);
			else
				gExecute(Trans, lstrCmd);
		}


		/////--fin---------




		/// <summary>
		/// 
		/// </summary>
		/// <param name="pstrConn"></param>
		/// <param name="pstrUserId"></param>
		/// <param name="pstrTabla"></param>
		/// <param name="pdrDatos"></param>
		/// <returns></returns>
		public static string gModiRet(string pstrConn, string pstrUserId, string pstrTabla, DataRow pdrDatos)
		{
			string lstrCmd = "exec " + pstrTabla + "_modi";
			string lstrSep;

			for(int i=0;i<pdrDatos.Table.Columns.Count;i++)
			{
				if(i==0)
					lstrSep = " "; 
				else
					lstrSep = ",";

				if(!pdrDatos.Table.Columns[i].ColumnName.StartsWith("_"))
				{
					if(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_user"))
						lstrCmd += lstrSep + "@" + pdrDatos.Table.Columns[i].ColumnName + "=" + pstrUserId;
					else
					{
						if(!(pdrDatos.Table.Columns[i].ColumnName.EndsWith("_audi_fecha")))
							lstrCmd += lstrSep + gFormatParam(pdrDatos[i],pdrDatos.Table.Columns [i].DataType, pdrDatos.Table.Columns[i].ColumnName);
					}
				}
			}
			
			string lstrRet;

			lstrRet = gExecuteScalarTrans(pstrConn, lstrCmd);
			
			return(lstrRet);
		}

		public static string gFormatParam(object pstrParam, Type pstrTipo, string pstrColName)
		{
			string lstrAux;

			if (pstrParam is DBNull)
				lstrAux = "null";
			else
			{
				lstrAux = pstrParam.ToString().Trim();
				switch (pstrTipo.Name.ToLower())
				{
					case "string":
					case "char":
						if(lstrAux.IndexOf("'")==-1)
							lstrAux = "'" + lstrAux + "'";
						else
							lstrAux = "[" + lstrAux + "]";
						break;
					case "datetime":
						if (lstrAux != "")
						{
							DateTime datFech = (DateTime) pstrParam;
							lstrAux = datFech.Month.ToString()+"/"+datFech.Day.ToString()+"/"+datFech.Year.ToString();
							if (datFech.Hour!=0||datFech.Minute!=0)
							{
								lstrAux = lstrAux + " " + datFech.Hour.ToString()+":"+datFech.Minute.ToString();
							}
							lstrAux = "'" + lstrAux + "'";
						}
						else
						{
							lstrAux = "null";
						}
						break;
					case "byte":
					case "int32":
					case "int64":
						if (lstrAux == "")
							lstrAux = "0";
						break;
					case "decimal":
						if (lstrAux == "")
						{
							lstrAux = "0";
						}
						lstrAux = lstrAux.Replace(",",".");
						break;
					case "boolean":
						if (lstrAux == "" ||!bool.Parse(lstrAux))
							lstrAux = "0";
						else
							lstrAux = "1";
						break;
					case "byte[]":
						lstrAux = "null";
						break;

				}
			}
			return("@" + pstrColName + "=" + lstrAux);
		}

		public static Int64 gObtenerIdLong(DataTable pdtDatos, string pstrColId)
		{
		try
		{	
				pdtDatos.DefaultView.RowFilter = "";
				if(pdtDatos.DefaultView.Count == 0)
					return(-1);
				else
				{
					pdtDatos.DefaultView.Sort = pstrColId;
					if ( Int64.Parse(pdtDatos.DefaultView[0][pstrColId].ToString()) <= 0)
						return (-1);
					else
						return(Int64.Parse(pdtDatos.DefaultView[0][pstrColId].ToString()) );
				}
			}
			catch
			{
				return(-1);
			}
		}
		public static int gObtenerId(DataTable pdtDatos, string pstrColId)
		{
			try
			{	
				pdtDatos.DefaultView.RowFilter = "";
				if(pdtDatos.DefaultView.Count == 0)
					return(-1);
				else
				{
					pdtDatos.DefaultView.Sort = pstrColId;
					if ( int.Parse(pdtDatos.DefaultView[0][pstrColId].ToString()) > 0)
						return (-1);
					else
						return(int.Parse(pdtDatos.DefaultView[0][pstrColId].ToString()) - 1);
				}
			}
			catch
			{
				return(-1);
			}
		}

		private static void gValidarTmsp(SqlTransaction Trans, string pstrTabla, DataRow pdrDatos)
		{
			string lstrColTmsp="";
			string lstrCmd;
			for(int i=0;i<pdrDatos.Table.Columns.Count;i++)
			{
				if(!pdrDatos.Table.Columns[i].ColumnName.StartsWith("_")&&pdrDatos.Table.Columns[i].ColumnName.EndsWith("_tmsp"))
				{
					lstrColTmsp = pdrDatos.Table.Columns[i].ColumnName;
				}
			}

			if (lstrColTmsp!="")
			{
				lstrCmd = "exec ATmspValidar '" + pstrTabla + "', " + pdrDatos[0].ToString();
				SqlDataAdapter cmdExecCommand = new SqlDataAdapter(lstrCmd, Trans.Connection);
				cmdExecCommand.SelectCommand.Transaction = Trans;
				DataSet ds = new DataSet();
				cmdExecCommand.Fill(ds);

				if(ds.Tables.Count>0 && ds.Tables[0].Rows.Count>0)
				{
					//compara los 2 timestamp, el error tambi�n podr�a devolver el usuario y la fecha que modific�
					if (!gCompararTmsp((byte[]) ds.Tables[0].Rows[0][lstrColTmsp], (byte[])  pdrDatos[lstrColTmsp]))
						throw new System.Exception("El registro fue modificado por otro usuario!");
				}
			}
		}

		private static bool gCompararTmsp(byte[] tmspOri, byte[] tmspDest)
		{
			for(int i=0;i<=tmspOri.GetUpperBound(0);i++)
			{
				if(tmspOri[i]!=tmspDest[i])
					return(false);
			}
			return(true);
		}

		public static  string[] GetDataRow(DataTable dt)
		{
			string[] array = new string[dt.Rows.Count];
			for (int index = 0; index < dt.Rows.Count; index++)
			{
				array[index] = dt.Rows[index]["Nombre"].ToString();
			}

			return array;
		}


		#endregion

        #region Metodos GM 

        static string dstAppCode = ConfigurationManager.AppSettings["AppCode"];
        static string gstrServ = clsFormatear.gDesencriptar(ConfigurationManager.AppSettings["conServ"], dstAppCode);
        static string gstrBase = clsFormatear.gDesencriptar(ConfigurationManager.AppSettings["conBase"], dstAppCode);
        static string gstrUsua = clsFormatear.gDesencriptar(ConfigurationManager.AppSettings["conUser"], dstAppCode);
        static string gstrPass = clsFormatear.gDesencriptar(ConfigurationManager.AppSettings["conPass"], dstAppCode);
        static string gstrTimeOut = ConfigurationManager.AppSettings["conTimeOut"];
        static string nameSpaceMethodError = "AccesoBD.clsSQLServer.";

        private static string getConnectionString()
        {
                return "server=" + gstrServ + ";database=" + gstrBase + ";"
                    + "uid=" + gstrUsua + ";"
                    + "pwd=" + gstrPass + ";"
                    + "Connection Timeout=" + gstrTimeOut;
        }

        /// <summary>
        /// Regresa un DataSet a partir de un SqlCommand.
        /// </summary>
        /// <param name="Cmd"></param>
        /// <returns>DataSet</returns>
        public static DataSet GetDataSetSqlCommand(SqlCommand Cmd)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlConnection cn = new SqlConnection();
            try
            {
                cn.ConnectionString = getConnectionString();
                cn.Open();
                da.SelectCommand = Cmd;
                da.SelectCommand.Connection = cn;
                da.Fill(ds);
            }
            catch (Exception error)
            {
                clsError.gLoggear( nameSpaceMethodError + "GetDataSetSqlCommand: " +  error.ToString());
            }
            finally
            {
                cn.Close();
            }
            return ds;
        }

        /// <summary>
        /// Ejecuta un comando SQL y retorna un valor.
        /// </summary>
        /// <param name="Cmd">Comando SQL</param>
        /// <returns>Valor devuelto</returns>
        public static string ExecuteCommandScalar(SqlCommand Cmd)
        {
            string res = string.Empty;
            SqlConnection cn = new SqlConnection();
            try
            {
                cn.ConnectionString = getConnectionString();
                cn.Open();
                Cmd.Connection = cn;
                res = Cmd.ExecuteScalar().ToString();
            }
            catch (Exception error)
            {
                clsError.gLoggear(nameSpaceMethodError + "ExecuteCommandScalar: " + error.ToString());
            }
            finally
            {
                cn.Close();
            }
            return res;
        }

        /// <summary>
        /// Ejecuta un comando SQL.
        /// </summary>
        /// <param name="Cmd">Comando SQL</param>
        public static void ExecuteCommandNonQuery(SqlCommand Cmd)
        {
            SqlConnection cn = new SqlConnection();
            try
            {
                cn.ConnectionString = getConnectionString();
                cn.Open();
                Cmd.Connection = cn;
                Cmd.ExecuteNonQuery();
            }
            catch (Exception error)
            {
                clsError.gLoggear(nameSpaceMethodError + "ExecuteCommandNonQuery: " + error.ToString());
            }
            finally
            {
                cn.Close();
            }
        }

        /// <summary>
        /// Ejecuta una sentencia SQL y retorna un Comando.
        /// Se puede implementar cuando se requiere un DataSet con varias tablas.
        /// </summary>
        /// <param name="Qry">Instrucci�n SQL</param>
        /// <returns>SqlCommand</returns>
        public static SqlCommand GetCommand(string Qry)
        {
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cn.ConnectionString = getConnectionString();
                cn.Open();
                cmd = new SqlCommand(Qry, cn);
            }
            catch (Exception error)
            {
                clsError.gLoggear(nameSpaceMethodError + "GetCommand: " + error.ToString());
            }
            finally
            {
                cn.Close();
            }
            return cmd;
        }

        /// <summary>
        /// Regresa un DataSet a partir de un QryString.
        /// </summary>
        /// <param name="Qry">Instrucci�n SQL</param>
        /// <returns>DataSet</returns>
        public static DataSet GetDataSetString(string Qry)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter();
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cn.ConnectionString = getConnectionString();
                cn.Open();
                cmd.CommandText = Qry;
                da.SelectCommand = cmd;
                da.SelectCommand.Connection = cn;
                da.Fill(ds);
            }
            catch (Exception error)
            {
                clsError.gLoggear(nameSpaceMethodError + "GetDataSetString: " + error.ToString());
            }
            finally
            {
                cn.Close();
            }
            return ds;
        }

        /// <summary>
        /// Ejecutar una instrucci�n SQL a partir de un QryString.
        /// </summary>
        /// <param name="Qry">Instrucci�n SQL</param>
        public static void ExecuteNonQuery(string Qry)
        {
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cn.ConnectionString = getConnectionString();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = Qry;
                cmd.ExecuteNonQuery();
            }
            catch (Exception error)
            {
                clsError.gLoggear(nameSpaceMethodError + "ExecuteNonQuery: " + error.ToString());
            }
            finally
            {
                cn.Close();
            }
        }

        /// <summary>
        /// Ejecuta una instrucci�n SQL a partir de un QryString y retorna un valor.
        /// </summary>
        /// <param name="Qry">Instrucci�n SQL</param>
        /// <returns>Valor devuelto</returns>
        public static string ExecuteScalar(string Qry)
        {
            string res = string.Empty;
            SqlConnection cn = new SqlConnection();
            SqlCommand cmd = new SqlCommand();
            try
            {
                cn.ConnectionString = getConnectionString();
                cn.Open();
                cmd.Connection = cn;
                cmd.CommandText = Qry;
                res = cmd.ExecuteScalar().ToString();
            }
            catch (Exception error)
            {
                clsError.gLoggear(nameSpaceMethodError + "ExecuteScalar: " + error.ToString());
            }
            finally
            {
                cn.Close();
            }
            return res;
        }

        #endregion

    }
}
