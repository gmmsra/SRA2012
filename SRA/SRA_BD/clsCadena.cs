using System;
using System.Security.Cryptography;
using System.Globalization;
using System.Text;

namespace AccesoBD
{
	/// <summary>
	/// Summary description for cadena.
	/// </summary>
	public class clsFormatear
	{
		static Byte[] IV =  {0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF};

		public static string gSumarValores(string pstrValor1, string pstrValor2)
		{
			string lstrSuma = "0";
			if (pstrValor1 != "" || pstrValor2 != "")
			{
				if (pstrValor1 != "")
				{
					lstrSuma = pstrValor1;
				}
				if (pstrValor2 != "")
				{
					lstrSuma = (Convert.ToDecimal(lstrSuma) + Convert.ToDecimal(pstrValor2)).ToString();
				}
			}
			return(lstrSuma);
		}
		
		public static string gGenerarPassword(int pintLongi)
		{
			RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
			byte[] buff = new byte[10];
			rng.GetBytes(buff);
			string lstrPass;
			lstrPass = Convert.ToBase64String(buff).ToString().Substring(1,pintLongi);
			lstrPass = lstrPass.Replace("/","a");
			lstrPass = lstrPass.Replace("+","b");
			lstrPass = lstrPass.Replace("-","c");
			lstrPass = lstrPass.Replace("=","d");
			return(lstrPass);
		}	

		public static string gFormatFecha2DB(Object pstrFecha)
		{
            string strFecha;
            strFecha = pstrFecha.ToString();

            
			if (  strFecha == string.Empty)
				return("NULL");
			else
				return gFormatFecha2DB(Convert.ToDateTime(strFecha) ); 
		}

		public static string gFormatFechaDS(object pstrFecha)
		{
            string strFecha;
            strFecha = pstrFecha.ToString();

			if (strFecha == string.Empty)
				return("NULL");
			else
                return gFormatFechaDS(Convert.ToDateTime(strFecha));  
		}
		public static string gFormatFechaDS(DateTime pstrFecha)
		{
			return "#"+ pstrFecha.ToString("yyyy/MM/dd")+ "#"; 
		
		}

		public static string gFormatFecha2DB(DateTime pstrFecha)
		{
			//return "'"+ pstrFecha.ToString("MM/dd/yyyy")+ "'"; 
			return "'"+ pstrFecha.ToString("yyyyMMdd")+ "'"; 
		}
		
		public static string gFormatFecha2String(DateTime pstrFecha)
		{
			return pstrFecha.ToString("dd/MM/yyyy"); 
		}
		
		public static string gFormatFecha(DateTime pstrFecha, string pstrCultureName)
		{
			// pstrCultureName: formato de fecha a mostrar.
			//	 []          04/15/2003 20:30:40
			// 	 [en-US]     4/15/2003 8:30:40 PM
			//   [es-AR]     15/04/2003 08:30:40 p.m.
			// 	 [fr-FR]     15/04/2003 20:30:40
			// 	 [hi-IN]     15-04-2003 20:30:40
			//	 [ja-JP]     2003/04/15 20:30:40
			//   [nl-NL]     15-4-2003 20:30:40
			//   [ru-RU]     15.04.2003 20:30:40
			//   [ur-PK]     15/04/2003 8:30:40 PM
			CultureInfo culture = new CultureInfo(pstrCultureName);
			string lstrFecha = Convert.ToString(pstrFecha, culture);
			lstrFecha = lstrFecha.Substring(0,10);
			return(lstrFecha);
		}
		public static string gFormatMonto(string pstrMonto, string pstrCultureName)
		{
			// pstrCultureName: formato de fecha a mostrar.
			//	 []          04/15/2003 20:30:40
			// 	 [en-US]     4/15/2003 8:30:40 PM
			//   [es-AR]     15/04/2003 08:30:40 p.m.
			// 	 [fr-FR]     15/04/2003 20:30:40
			// 	 [hi-IN]     15-04-2003 20:30:40
			//	 [ja-JP]     2003/04/15 20:30:40
			//   [nl-NL]     15-4-2003 20:30:40
			//   [ru-RU]     15.04.2003 20:30:40
			//   [ur-PK]     15/04/2003 8:30:40 PM
			CultureInfo culture = new CultureInfo(pstrCultureName);
			string lstrMonto = Convert.ToString(pstrMonto, culture);
			//lstrMonto = lstrMonto.Substring(0,10);
			return(lstrMonto);
		}
		public static string gFormatCadena(string pstrCade)
		{
			if (pstrCade == null)
			{
				pstrCade = "";
				return(pstrCade);
			}
			pstrCade = pstrCade.Trim();
			pstrCade = pstrCade.Replace("&nbsp;","");
			return (pstrCade);
		}
		public static string gFormatCadenaEntreParentesis(string pstrCade)
		{
			if (pstrCade == null)
			{
				pstrCade = "";
				return(pstrCade);
			}
			pstrCade = pstrCade.Trim();
			pstrCade = "("+ pstrCade + ")";
			return (pstrCade);
		}

		public static string gFormatFechaString(string pstrFecha, string pstrFormat)
		{
			string lstrAux = pstrFecha;
			switch (pstrFormat)
			{
				case "mm/dd/yyyy":
				{
					System.IFormatProvider format = new System.Globalization.CultureInfo("fr-FR", true);
					DateTime datFech = System.DateTime.Parse(lstrAux, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
					lstrAux = datFech.Month.ToString()+"/"+datFech.Day.ToString()+"/"+datFech.Year.ToString();
					break;
				}
				case "Int32":
				{
					double dFecha;
					System.IFormatProvider format = new System.Globalization.CultureInfo("fr-FR", true);
					DateTime datFech = System.DateTime.Parse(lstrAux, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
					dFecha=datFech.ToOADate();
					lstrAux = dFecha.ToString();
					break;
				}
			}
			return(lstrAux); 
		}

		public static object gFormatFechaDateTime(string pstrFecha)
		{
			if (pstrFecha == "")
			{
				return(DBNull.Value);
			}
			else
			{
				System.IFormatProvider format = new System.Globalization.CultureInfo("ES-AR", true);
				DateTime datFech = System.DateTime.Parse(pstrFecha, format, System.Globalization.DateTimeStyles.NoCurrentDateDefault);
				return(datFech); 
			}
		}
		
		public static string CrLf()
		{
			byte[] Bytes;
			Bytes = new byte[2];
			Bytes[0] = 13;
			Bytes[1] = 10;
			return(System.Text.Encoding.ASCII.GetString(Bytes));
		}

//		public static string gEncriptar(string pstrPass)
//		{
//			MD5CryptoServiceProvider md5Hasher = new MD5CryptoServiceProvider();
//			UTF8Encoding encoder = new UTF8Encoding();
//			return (encoder.GetString(md5Hasher.ComputeHash(encoder.GetBytes(pstrPass))));
//		}

		private static Byte[] mKey(string pstrKey)
		{
			return (Encoding.UTF8.GetBytes(pstrKey));
		}

		public static string gEncriptar(string Valor, string pKey) 
		{
			try 
			{
				DESCryptoServiceProvider des = new DESCryptoServiceProvider();
				Byte[] inputByteArray = Encoding.UTF8.GetBytes(Valor);
				System.IO.MemoryStream ms = new System.IO.MemoryStream();
				CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(mKey(pKey), IV), CryptoStreamMode.Write);
				cs.Write(inputByteArray, 0, inputByteArray.Length);
				cs.FlushFinalBlock();
				return (Convert.ToBase64String(ms.ToArray()));
			} 
			catch (Exception e)
			{
				return (e.Message);
			}

		}
/*
			Dim cs As New CryptoStream(ms, des.CreateEncryptor(pKey, IV), CryptoStreamMode.Write)
			cs.Write(inputByteArray, 0, inputByteArray.Length)
			cs.FlushFinalBlock()
			Return Convert.ToBase64String(ms.ToArray())

 * */
		
		public static string gDesencriptar(string Valor, string pKey) 
		{
			try 
			{
				DESCryptoServiceProvider des = new DESCryptoServiceProvider();
				Byte[] inputByteArray = Convert.FromBase64String(Valor);
				System.IO.MemoryStream ms = new System.IO.MemoryStream();
				CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(mKey(pKey), IV), CryptoStreamMode.Write);
				cs.Write(inputByteArray, 0, inputByteArray.Length);
				cs.FlushFinalBlock();
				Encoding encoding = Encoding.UTF8;
				return (encoding.GetString(ms.ToArray()));
			} 
			catch (Exception e)
			{
				return (e.Message);
			}

		}

		
		/*

	Private Function Encriptar(ByVal Valor As String, ByVal pKey As Byte()) As String
		Try
			Dim des As New DESCryptoServiceProvider
			Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes(Valor)
			Dim ms As New System.IO.MemoryStream
			Dim cs As New CryptoStream(ms, des.CreateEncryptor(pKey, IV), CryptoStreamMode.Write)
			cs.Write(inputByteArray, 0, inputByteArray.Length)
			cs.FlushFinalBlock()
			Return Convert.ToBase64String(ms.ToArray())
		Catch e As Exception
			Return e.Message
		End Try
	End Function
	Private Function Desencriptar(ByVal Valor As String, ByVal pKey As Byte()) As String
		Dim inputByteArray(Valor.Length) As Byte
		Try
			Dim des As New DESCryptoServiceProvider
			inputByteArray = Convert.FromBase64String(Valor)
			Dim ms As New System.IO.MemoryStream
			Dim cs As New CryptoStream(ms, des.CreateDecryptor(pKey, IV), CryptoStreamMode.Write)
			cs.Write(inputByteArray, 0, inputByteArray.Length)
			cs.FlushFinalBlock()
			Dim encoding As Encoding = encoding.UTF8
			Return encoding.GetString(ms.ToArray())
		Catch e As Exception
			Return e.Message
		End Try
	End Function
*/
		
	}



}
