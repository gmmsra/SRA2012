using System;
using System.Configuration;

namespace AccesoBD   ///clsReportingServices
{
	/// <summary>
	/// Descripción breve de clsReportingURL.
	/// </summary>
	public class clsReportingURL
	{
		private static string gReportServer()
		{
			//string lstrRepo = System.Configuration.ConfigurationSettings.AppSettings["conRepo"].ToString();
            string lstrRepo = ConfigurationManager.AppSettings["conRepo"].ToString();
            return (lstrRepo);
		}
		private static string gReportFolder()
		{
			//string lstrRepo = System.Configuration.ConfigurationSettings.AppSettings["conRepoDire"].ToString();
            string lstrRepo = ConfigurationManager.AppSettings["conRepoDire"].ToString();
            return (lstrRepo);
		}
		public static string gIniciarReporte(string pstrRptName)
		{
			string lstrRpt = "";
			
			lstrRpt = "http://" + gReportServer() + "/ReportServer?%2f" + gReportFolder() + "%2f" + pstrRptName;
			lstrRpt += "&rs%3aClearSession=true&rs%3aFormat=HTML4.0&rs%3aCommand=Render";
			return(lstrRpt);
		}
		public static string gSetearOpcionesReporte(string pstrRpt)
		{
			string lstrRpt = pstrRpt;
			lstrRpt += "&rc%3aArea=Toolbar&rc%3aLinkTarget=_top";
			lstrRpt += "&rc%3aToolbar=True&rc%3aReplacementRoot=http%3a%2f%2f";
			lstrRpt += gReportServer() + "%2fReports%2fPages%2fReport.aspx%3fServerUrl%3d";
			return(lstrRpt);
		}
	}
}
