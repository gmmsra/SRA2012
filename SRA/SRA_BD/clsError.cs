using System;
using System.IO;
using System.Data.SqlClient;

namespace AccesoBD
{
	/// <summary>
	/// Descripción breve de clsError.
	/// // Dario 2013-10-10 se completa el log que no usa los parametros pasados
	/// </summary>
	public sealed class clsError
	{
		public static void gManejarError(Exception pExcep)
		{
			gManejarError(null,pExcep);
		}

		public static void gManejarError(System.Web.UI.Page pPagina, Exception pExcep)
		{
//			if (!(pExcep.GetType().Name == "ThreadAbortException" || pExcep.GetType().Name=="clsErrNeg" || (pExcep.GetType().Name=="SqlException" && ((SqlException)pExcep).Number==50000)))
//			{
				string lstrArchLog = System.Configuration.ConfigurationSettings.AppSettings["conArchLog"].ToString();
				if(lstrArchLog != "")
				{
					FileStream fs = new FileStream(lstrArchLog , FileMode.OpenOrCreate, FileAccess.ReadWrite);

					StreamWriter w = new StreamWriter(fs);
					w.BaseStream.Seek(0, SeekOrigin.End);

					w.Write(clsFormatear.CrLf() + "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + clsFormatear.CrLf());
					w.Write(pExcep.Message + clsFormatear.CrLf());
					w.Write(pExcep.StackTrace + clsFormatear.CrLf());
					w.Write(pExcep.StackTrace + clsFormatear.CrLf());
					//w.Write(" ** Usuario:- " + pPagina.Session["sUserId"].ToString() + clsFormatear.CrLf());
					w.Write("------------------------------------------------------------------------" + clsFormatear.CrLf());

					w.Flush();
					w.Close();
				}
			//}

			if(pPagina!=null)
			{
				System.Web.UI.WebControls.TextBox lTxtMens = (System.Web.UI.WebControls.TextBox) pPagina.FindControl("lblMens");
				if(lTxtMens!=null)
				{
					if(!pPagina.IsClientScriptBlockRegistered("lblMens"))
					{
						System.Text.StringBuilder lsbMsg = new System.Text.StringBuilder();
						lsbMsg.Append("<SCRIPT language='javascript' for=window event=onload>");
						lsbMsg.Append("if (document.all['lblMens'].value != '') {");
						lsbMsg.Append("var sTemp = document.all['lblMens'].value;");
						lsbMsg.Append("while (sTemp.indexOf('<BR>') != -1) sTemp = sTemp.replace('<BR>', String.fromCharCode(10,13));"); 
						lsbMsg.Append("alert(sTemp);");
						lsbMsg.Append("document.all['lblMens'].value = '';");
						lsbMsg.Append("}");
						lsbMsg.Append("</SCRIPT>");
						pPagina.RegisterClientScriptBlock("lblMens",lsbMsg.ToString());
					}
					lTxtMens.Text=pExcep.Message.Replace("The transaction ended in the trigger. The batch has been aborted.","");
				}
				else
					throw pExcep;
			}
		}


		public static void gManejarError(string pObject, string pTipo ,Exception pExcep)
		{
			string lstrArchLog = System.Configuration.ConfigurationSettings.AppSettings["conArchLog"].ToString();
			if(lstrArchLog != "")
			{
				FileStream fs = new FileStream(lstrArchLog , FileMode.OpenOrCreate, FileAccess.ReadWrite);

				StreamWriter w = new StreamWriter(fs);
				w.BaseStream.Seek(0, SeekOrigin.End);

				w.Write(clsFormatear.CrLf() + "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + clsFormatear.CrLf());
				// Dario 2013-10-10 se completa el log que no usa los parametros pasados
				w.Write(pTipo + clsFormatear.CrLf());
				w.Write(pObject.ToString() + clsFormatear.CrLf());
				w.Write(pExcep.Message + clsFormatear.CrLf());
				w.Write(pExcep.StackTrace + clsFormatear.CrLf());
				w.Write("------------------------------------------------------------------------" + clsFormatear.CrLf());

				w.Flush();
				w.Close();
			}
		}

		public static void gGenerarMensajes(string pPagina,  string pTipo, string pMensaje)
		{
			
			string lstrArchLog = System.Configuration.ConfigurationSettings.AppSettings["conArchLog"].ToString();
			if(lstrArchLog != "")
			{
				FileStream fs = new FileStream(lstrArchLog , FileMode.OpenOrCreate, FileAccess.ReadWrite);

				StreamWriter w = new StreamWriter(fs);
				w.BaseStream.Seek(0, SeekOrigin.End);

				w.Write(clsFormatear.CrLf() + "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + clsFormatear.CrLf());
				w.Write(pPagina  + clsFormatear.CrLf());
				w.Write(pTipo + clsFormatear.CrLf());
				w.Write(pMensaje + clsFormatear.CrLf());
				w.Write("------------------------------------------------------------------------" + clsFormatear.CrLf());

				w.Flush();
				w.Close();
			} 
		}

		public static void gGenerarMensajes(System.Web.UI.Page pPagina, String pstrMensaje)
		{
			
			System.Web.UI.WebControls.TextBox lTxtMens = (System.Web.UI.WebControls.TextBox) pPagina.FindControl("lblMens");
			if(!pPagina.IsClientScriptBlockRegistered("lblMens"))
			{
				System.Text.StringBuilder lsbMsg = new System.Text.StringBuilder();
				lsbMsg.Append("<SCRIPT language='javascript' for=window event=onload>");
				lsbMsg.Append("if (document.all['lblMens'].value != '') {");
				lsbMsg.Append("var sTemp = document.all['lblMens'].value;");
				lsbMsg.Append("while (sTemp.indexOf('<BR>') != -1) sTemp = sTemp.replace('<BR>', String.fromCharCode(10,13));"); 
				lsbMsg.Append("alert(sTemp);");
				lsbMsg.Append("document.all['lblMens'].value = '';");
				lsbMsg.Append("}");
				lsbMsg.Append("</SCRIPT>");
				pPagina.RegisterClientScriptBlock("lblMens",lsbMsg.ToString());
			}
			lTxtMens.Text=pstrMensaje;
		}


		/// <summary>
		/// Dario 2014-06-17
		/// metodo que registra el error sin mensaje al usuario
		/// </summary>
		/// <param name="pExcep"></param>
		public static void gManejarErrorSinMsgBox(Exception pExcep)
		{
			string lstrArchLog = System.Configuration.ConfigurationSettings.AppSettings["conArchLog"].ToString();
			
			if (!(pExcep.GetType().Name == "ThreadAbortException" || pExcep.GetType().Name == "clsErrNeg" || (pExcep.GetType().Name == "SqlException" && ((SqlException)pExcep).Number == 50000)))
			{
                
				try
				{
					FileStream fs = new FileStream(lstrArchLog, FileMode.OpenOrCreate, FileAccess.ReadWrite);

					StreamWriter w = new StreamWriter(fs);
					w.BaseStream.Seek(0, SeekOrigin.End);

					w.Write(clsFormatear.CrLf() + "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + clsFormatear.CrLf());
					w.Write(pExcep.Message + clsFormatear.CrLf());
					w.Write(pExcep.StackTrace + clsFormatear.CrLf());
					w.Write("------------------------------------------------------------------------" + clsFormatear.CrLf());

					w.Flush();
					w.Close();
				}
				catch(Exception ex)
				{
                   string p = ex.StackTrace; 
				}
			}
		}

		/// <summary>
		/// matodo que logea sin error por pantalla
		/// </summary>
		/// <param name="Msg"></param>
		public static void gLoggear(string pExcep)
		{
			string lstrArchLog = System.Configuration.ConfigurationSettings.AppSettings["conArchLog"].ToString();
			
				try
				{
					FileStream fs = new FileStream(lstrArchLog, FileMode.OpenOrCreate, FileAccess.ReadWrite);

					StreamWriter w = new StreamWriter(fs);
					w.BaseStream.Seek(0, SeekOrigin.End);

					w.Write(clsFormatear.CrLf() + "Fecha: " + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") + clsFormatear.CrLf());
					w.Write(pExcep + clsFormatear.CrLf());
					w.Write("------------------------------------------------------------------------" + clsFormatear.CrLf());

					w.Flush();
					w.Close();
				}
				catch(Exception ex)
				{
					string p = ex.StackTrace; 
				}
			
		}
	}
}
