﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SRA_Common
{
    public class Errores
    {
        public static string ReturnExceptionMessage(string NameSpace, string MethodName, string ExceptionMessage)
        {
            return DateTime.Now.ToShortTimeString() + " - " + NameSpace + "." + MethodName + ", registró el siguiente error: " + ExceptionMessage;
        }
    }
}
