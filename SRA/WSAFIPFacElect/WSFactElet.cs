using System;
using WSAFIPFacElect.WS2010AFIP;
using System.Configuration;


namespace WSAFIPFacElect
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class WSFactElet
	{
		static string UrlWSAfip = ConfigurationSettings.AppSettings["UrlWSAfip"];
		static string UsrWSAfip = ConfigurationSettings.AppSettings["UsrWSAfip"];
		static string PwsWSAfip = ConfigurationSettings.AppSettings["PwsWSAfip"];
		static string SisEmpresa = ConfigurationSettings.AppSettings["SisEmpresa"];

		/// <summary>
		/// metodo que verifica que el servicio 2010 esta en linea
		/// </summary>
		/// <returns></returns>
		public static string InicializarServicio()
		{ 
			string respuesta = string.Empty;
			ServiceAFIP ws2010 = new ServiceAFIP(UrlWSAfip);
			
			ws2010.Credentials = new System.Net.NetworkCredential(UsrWSAfip,PwsWSAfip, "");

			respuesta = ws2010.InicializarServicio();

			return respuesta;
		}
			
		/// <summary>
		/// metodo que invoca el ws en 2010 que se conecta a la afip y 
		/// pide el cae para un comp_id y retorna el mismo con el comp_nume ororgado por afip
		/// </summary>
		/// <param name="Empresa"></param>
		/// <param name="CompID"></param>
		public static string GetCAE(string Empresa, string CompID)
		{ 
			string respuesta = string.Empty;
			string _empresa = (Empresa.Trim().Length == 0)? SisEmpresa : Empresa;

			ServiceAFIP ws2010 = new ServiceAFIP(UrlWSAfip);
            
			ws2010.Credentials = new System.Net.NetworkCredential(UsrWSAfip,PwsWSAfip, "");

			//respuesta = "A|SRA|2888415|150|68090637967907|20180309|210||";

			respuesta = ws2010.PedirCAEFActura(_empresa, CompID);

			return respuesta;
		}

		public static string IntegrarClienteAFinneg(string empresa, string clie_id, string tipoOper)
		{
			string respuesta = string.Empty;
			string _empresa = (empresa.Trim().Length == 0) ? SisEmpresa : empresa;

			ServiceAFIP ws2010 = new ServiceAFIP(UrlWSAfip);

			ws2010.Credentials = new System.Net.NetworkCredential(UsrWSAfip, PwsWSAfip, "");

			respuesta = ws2010.EnviarClienteFinnegans(_empresa, clie_id, tipoOper);

			return respuesta;
		}
	}
}
