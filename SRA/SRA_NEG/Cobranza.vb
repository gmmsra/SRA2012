Imports Business.Facturacion.ActividadesBusiness ' Dario 2013-07-23
Imports WSAFIPFacElect ' Dario 2015-06-24
' Dario 2014-04-01 cambio por email de Torres reportando error de nd con actividad administracion
'                  a la que no se le puede hacer nc por ad106

Public Class Cobranza
    Inherits GenericaRel

    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
    Private mstrTablaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep
    Private mstrTablaCuentasCtes As String = SRA_Neg.Constantes.gTab_CuentasCtes
    Private mstrTablaPagos As String = SRA_Neg.Constantes.gTab_ComprobPagos
    Private mstrTablaSaldosACuenta As String = SRA_Neg.Constantes.gTab_SaldosACuenta
    Private mstrTablaComprobACuenta As String = SRA_Neg.Constantes.gTab_ComprobACuenta
    Private mstrTablaBilletes As String = SRA_Neg.Constantes.gTab_Billetes
    Private mstrTablaVtos As String = SRA_Neg.Constantes.gTab_ComprobVtos
    Private mstrTablaCheques As String = SRA_Neg.Constantes.gTab_Cheques
    Private mstrTablaAcuses As String = SRA_Neg.Constantes.gTab_ComprobAcuses
    Private mstrTablaAutor As String = SRA_Neg.Constantes.gTab_ComprobAutor

    Private Const mstrTablaDeuSoc As String = "cobran_deusoc"
    Private Const mstrTablaAcred As String = "cobran_acred"
    Private Const mstrTablaAsiento As String = "cobranzas_asiento"

    Private mstrAcreId As String = ""
    Private mstrAcuseId As String = ""
    Private mstrAcuseNume As String = ""
    Private mstrReciId As String = ""
    Private mstrReciNume As String = ""
    Private mstrFactId As String = ""
    Private mstrFactNume As String = ""
    Private mstrNDIds As String = ""
    Private mstrNDNumes As String = ""
    Private mstrCompIds As String = ""
    Private GenerarNDSobreTasasVto As Boolean = False
    Private eliminoRowsCtaCte As Boolean = False
    Public TipoCobranza As String = ""


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet)
        MyBase.New(pstrConn, pstrUserId, pstrTabla, pdsDatos)
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String)
        MyBase.New(pstrConn, pstrUserId, pstrTabla)
    End Sub

    Public Overloads Function Alta() As String
        Dim lstrCompId, lstrCovtId, lstrAsieCompId, lstrCompIdWSAFIP As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrCompTmpId As String
        Dim idCompsAfip As New ArrayList

        Try
            GenerarNDSobreTasasVto = False
            eliminoRowsCtaCte = False

            Me.mGenerarNDSobreTasasVto()
            mGenerarNDIntereses()
            mGenerarPagosAcred()
            mGenerarAcreditacion()
            mGenerarAcuse()

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            For Each ldrComp As DataRow In mdsDatos.Tables(mstrTabla).Select("", "comp_dh, comp_id")  'primero el inter�s, despu�s el acuse y al final el recibo
                lstrCompTmpId = ldrComp("comp_id")
                lstrCompId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, ldrComp)
                ' Dario 2015-07-02 para que viaje el comp_id a afip
                If (ldrComp.Item("comp_coti_id") = Constantes.ComprobTipos.ND Or ldrComp.Item("comp_coti_id") = Constantes.ComprobTipos.NC) Then
                    lstrCompIdWSAFIP = lstrCompId
                    idCompsAfip.Add(lstrCompIdWSAFIP)
                End If

                'en el caso de que el comprobante sea cancelado por uno ya existente, actualizo ctac_comp_id
                'For Each ldrCtac As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("ctac_comp_id=" + ldrComp.Item("comp_id").ToString)
                '   ldrCtac.Item("ctac_comp_id") = lstrCompId
                'Next

                lstrCovtId = ActualizarDetas(lTransac, mstrTablaVtos, "covt_comp_id", lstrCompId, "covt_comp_id=" + ldrComp.Item("comp_id").ToString)

                'If lstrCovtId <> "" Then
                '   For Each ldrCtac As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("ctac_covt_id < 0")
                '      ldrCtac.Item("ctac_covt_id") = lstrCovtId
                '   Next
                'End If

                For Each ldrCC As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("")
                    If ldrCC.Item("ctac_comp_id") < 0 And ldrCC.Item("ctac_comp_id") = lstrCompTmpId And Not lstrCompId Is Nothing Then
                        ldrCC.Item("ctac_comp_id") = lstrCompId
                        If lstrCovtId <> "" Then
                            ldrCC.Item("ctac_covt_id") = lstrCovtId
                        End If
                    End If
                    ' Dario 2013-07-22 Si es una sobretasa por vto inserto en la tabla comprob_aran los datos 
                    ' de los aranceles que componen el recargo
                    If (ldrCC.Item("_comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.SobreTasasAlVto_CtaCte _
                        And ldrComp.Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.ND) Then
                        '' Dario 2013-07-19 Se agrega si tiene aranceles desde la tabla de sobretasas aranceles
                        Dim intRespuesta As Int32 = -1
                        ' ejecuto el metodo que crea los aranceles desde la tabla de sobretasas calculadas
                        ' con vto a futuro
                        intRespuesta = DAO.Facturacion.FacturacionDao.CrearArancelesDeSobreTasasEnNuevoVto(lTransac _
                                                                                                         , ldrComp.Item("comp_fecha") _
                                                                                                         , ldrCC.Item("_comp_Id_Cancela") _
                                                                                                         , Me.mstrUserId _
                                                                                                         , lstrCompId _
                                                                                                         )
                        ' ejecuto el metodo que crea los conceptos desde la tabla de sobretasas calculadas
                        ' con vto a futuro
                        intRespuesta = DAO.Facturacion.FacturacionDao.CrearConceptosDeSobreTasasEnNuevoVto(lTransac _
                                                                                                          , ldrComp.Item("comp_fecha") _
                                                                                                          , ldrCC.Item("_comp_Id_Cancela") _
                                                                                                          , Me.mstrUserId _
                                                                                                          , lstrCompId _
                                                                                                          )

                        ' Dario 2013-08-29 si viene de mGenerarAcreditacion y no borro la fila 
                        ' para poder generar bien el detalle de la nd lo borro aca sino se deja la fila de 
                        ' ctacta
                        If (eliminoRowsCtaCte = True) Then
                            ldrCC.Delete()
                        End If

                    End If
                Next

                ActualizarDetas(lTransac, mstrTablaDeta, "code_comp_id", lstrCompId, "code_comp_id=" + ldrComp.Item("comp_id").ToString)
                ActualizarDetas(lTransac, mstrTablaConcep, "coco_comp_id", lstrCompId, "coco_comp_id=" + ldrComp.Item("comp_id").ToString)

                If ldrComp.Item("comp_coti_id") <> Constantes.ComprobTipos.ND Then 'las ND no aplican nada
                    ActualizarDetas(lTransac, mstrTablaCuentasCtes, "ctac_canc_comp_id", lstrCompId, "ctac_canc_comp_id=" + ldrComp.Item("comp_id").ToString & " and _comp_coti_id <> 32 and ctac_impor<>0")

                    'chequear si no inserta 2 veces en ND
                    ActualizarDetas(lTransac, mstrTablaCuentasCtes, "ctac_comp_id", lstrCompId, "ctac_comp_id=" + lstrCompId & " and ctac_impor<>0")
                Else
                    'modifico el campo code_gene_inte en comprob_deta de acuerdo a las "aplicadas" por la ND (la aplicacion no se grava!)
                    For Each ldrCtac As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("ctac_canc_comp_id=" & ldrComp.Item("comp_id").ToString & " AND _comp_coti_id=" & Constantes.ComprobTipos.Interes_CtaCte)
                        clsSQLServer.gExecute(lTransac, String.Format("comprob_deta_gene_inte_modi @code_comp_id={0}, @code_audi_user={1}", ldrCtac("ctac_comp_id"), mstrUserId))
                    Next
                End If

                ActualizarDetas(lTransac, mstrTablaDeuSoc, "cods_comp_id", lstrCompId, "cods_comp_id=" + ldrComp.Item("comp_id").ToString)
                ActualizarDetas(lTransac, mstrTablaSaldosACuenta, "sact_comp_id", lstrCompId, "sact_comp_id=" + ldrComp.Item("comp_id").ToString)
                ActualizarDetas(lTransac, mstrTablaComprobACuenta, "cact_comp_id", lstrCompId, "cact_comp_id=" + ldrComp.Item("comp_id").ToString)
                ActualizarDetas(lTransac, mstrTablaAutor, "coau_comp_id", lstrCompId, "coau_comp_id=" + ldrComp.Item("comp_id").ToString)

                'si no hizo el enganche con comp_id y covt_id asignarlos
                For Each ldrAcuse As DataRow In mdsDatos.Tables(mstrTablaAcuses).Select("")
                    If ldrAcuse.Item("coac_comp_id") < 0 And ldrAcuse.Item("coac_comp_id") = lstrCompTmpId And Not lstrCompId Is Nothing Then
                        ldrAcuse.Item("coac_comp_id") = lstrCompId
                        'End If
                        'If ldrAcuse.Item("coac_covt_id") < 0 And ldrAcuse.Item("coac_covt_id") = lstrCompTmpId And Not lstrCovtId Is Nothing Then
                        ldrAcuse.Item("coac_covt_id") = lstrCovtId
                    End If
                Next

                ActualizarDetas(lTransac, mstrTablaAcuses, "coac_canc_comp_id", lstrCompId, "coac_canc_comp_id=" + ldrComp.Item("comp_id").ToString + " and _comp_coti_id <> 32 ")

                ActualizarPagos(lTransac, mstrTablaPagos, "paco_comp_id", lstrCompId, ldrComp.Item("comp_id").ToString)

                'guardo los id de los comp en la tabla de asientos, y actualizo los ctac que relacionan nd con recibos
                Select Case ldrComp.Item("comp_coti_id")
                    Case Constantes.ComprobTipos.ND
                        mdsDatos.Tables(mstrTablaAsiento).Rows(0).Item("proc_nd_comp_id") = mdsDatos.Tables(mstrTablaAsiento).Rows(0).Item("proc_nd_comp_id").ToString & lstrCompId & ","

                        If NDIds <> "" Then NDIds += ","
                        NDIds += lstrCompId
                        lstrCompId = 0
                    Case Constantes.ComprobTipos.Recibo_Acuse
                        mdsDatos.Tables(mstrTablaAsiento).Rows(0).Item("proc_acuse_comp_id") = lstrCompId
                        AcuseId = lstrCompId
                        lstrCompId = 0
                        lstrAsieCompId = lstrCompId
                    Case Constantes.ComprobTipos.Recibo, Constantes.ComprobTipos.AplicacionCreditos, 13  '28/12/2010 Agregado de Transferencias(13)
                        mdsDatos.Tables(mstrTablaAsiento).Rows(0).Item("proc_comp_id") = lstrCompId
                        ReciId = lstrCompId
                        lstrAsieCompId = lstrCompId
                    Case Constantes.ComprobTipos.CancelacionFC
                        lstrAsieCompId = lstrCompId
                        FactId = mdsDatos.Tables(mstrTablaCuentasCtes).Select("_comp_coti_id=" & Constantes.ComprobTipos.Factura)(0).Item("ctac_comp_id")
                        'FactNume = ldrComp.Item("comp_nume")
                    Case Constantes.ComprobTipos.Recibo_Acreditacion
                        AcreId = lstrCompId
                        'lstrCompId = 0
                        lstrAsieCompId = lstrCompId
                End Select
            Next

            If lstrAsieCompId <> "" Then 'solo graba el asiento si graba recibo o aplicaci�n de cr�ditos o acuses
                ActualizarDetas(lTransac, mstrTablaAsiento, "proc_comp_id", lstrAsieCompId)
            End If

            'Roxi: 08/05/2008 - grabar el enganche del recibo con las nds generadas por interes.
            If NDIds <> "" Then
                If ReciId <> "" Then
                    clsSQLServer.gExecute(lTransac, "exec nota_debito_recibo_modi @nds='" & NDIds & "',@reci_id=" & ReciId)
                Else
                    If AcuseId <> "" Then
                        clsSQLServer.gExecute(lTransac, "exec nota_debito_recibo_modi @nds='" & NDIds & "',@reci_id=" & AcuseId)
                    Else

                        If AcreId <> "" Then
                            clsSQLServer.gExecute(lTransac, "exec nota_debito_recibo_modi @nds='" & NDIds & "',@reci_id=" & AcreId)
                        End If
                    End If
                End If
            End If

            ' ldrComp.Item("comp_coti_id") Constantes.ComprobTipos.ND Constantes.ComprobTipos.Recibo
            ' solo entra aca si es alta Fact Elect AFIP
            If idCompsAfip.Count > 0 Then
                Dim idComp As String
                For Each idComp In idCompsAfip
                    Dim respuesta As String
                    Dim objWsAFIP As WSFactElet
                    ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                    respuesta = objWsAFIP.GetCAE(String.Empty, idComp)
                    '                 0                1                2              3              4            5
                    ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                    '                  6                      7                8 
                    '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;

                    Dim respArray As Array = respuesta.Split("|"c)
                    ' si retorna ok 
                    If (respArray(0) = "A") Then
                        ' controlo que el comp id enviado es igual al recibido
                        Dim compIdresp As String = respArray(2)

                        Dim comp_cemi_nume As String = ""
                        Dim opcional1 As String = ""
                        Dim opcional2 As String = ""

                        Try
                            comp_cemi_nume = respArray(6)
                        Catch ex As Exception

                        End Try

                        Try
                            opcional1 = respArray(7)
                        Catch ex As Exception

                        End Try

                        Try
                            opcional2 = respArray(8)
                        Catch ex As Exception

                        End Try

                        If (comp_cemi_nume.Trim().Length = 0) Then
                            comp_cemi_nume = "null"
                        End If

                        If (opcional1.Trim().Length = 0) Then
                            opcional1 = "null"
                        End If

                        If (opcional2.Trim().Length = 0) Then
                            opcional2 = "null"
                        End If

                        If (compIdresp.Trim() = idComp.Trim()) Then
                            ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                            Dim compNumeresp As String = respArray(3)
                            'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & idComp & " ,@comp_nume=" & compNumeresp
                            Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & idComp & " ,@comp_nume=" & compNumeresp _
                                               & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                            ' hago el update del @comp_nume de la tabla de comprobantes
                            clsError.gLoggear("GetCAE 3:: " + proc)
                            Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                            ' si no realizo el update error
                            If (intUpdateRest = 0) Then
                                Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                            End If
                        Else
                            ' si los com id son diferentes error
                            Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & idComp & " * " & respuesta)
                        End If
                    Else
                        ' cualquier cosa fuera de ok error 
                        Throw New Exception(respuesta)
                    End If
                Next
            End If

            clsSQLServer.gCommitTransac(lTransac)

            'pongo el n�mero de los comprobantes generados
            If FactId <> "" Then
                FactNume = clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantesX", FactId, "numero").ToString
            End If

            If AcuseId <> "" Then
                AcuseNume = clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantesX", AcuseId, "numero").ToString
            End If
            If ReciId <> "" Then
                ReciNume = clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantesX", ReciId, "numero").ToString
            End If
            If NDIds <> "" Then
                Dim lvstr() As String = NDIds.ToString.Split(",")
                For i As Integer = 0 To lvstr.GetUpperBound(0)
                    If NDNumes <> "" Then NDNumes += ","
                    NDNumes += clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantesX", lvstr(i), "numero").ToString
                Next
            End If
            Return (lstrCompId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Private Sub mGenerarAcuse()
        Dim dTotalPost, dTotalPostTmp, dImpoAplic As Decimal
        Dim ldrComp As DataRow
        Dim ldrCompDeta As DataRow
        Dim ldrCompDetaOri As DataRow
        Dim ldrCoac As DataRow
        Dim ldrPagosPD() As DataRow

        'busco si hay postdatados
        ldrPagosPD = mdsDatos.Tables(mstrTablaPagos).Select("cheq_chti_id=" & Constantes.ChequesTipos.Postdatado)

        For Each ldrPago As DataRow In ldrPagosPD
            dTotalPost += ldrPago.Item("paco_impo")
        Next

        If dTotalPost > 0 Then
            ldrComp = mdsDatos.Tables(mstrTabla).NewRow
            'agrego en comprobantes
            With ldrComp
                .ItemArray = MiRow.ItemArray
                .Item("comp_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTabla), "comp_id")
                .Item("comp_dh") = 1       '1 = haber
                .Item("comp_cance") = 1    '1 = cancelado
                .Item("comp_cs") = 1       '1 = cta cte
                .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Recibo_Acuse
                .Item("comp_neto") = dTotalPost
                .Table.Rows.Add(ldrComp)
            End With

            ldrCompDeta = mdsDatos.Tables(mstrTablaDeta).NewRow

            ldrCompDetaOri = mdsDatos.Tables(mstrTablaDeta).Rows(0)

            'agrego en comprob_deta
            With ldrCompDeta
                .ItemArray = ldrCompDetaOri.ItemArray
                .Item("code_id") = clsSQLServer.gObtenerId(mdsDatos.Tables(mstrTablaDeta), "code_id")
                .Item("code_comp_id") = ldrComp.Item("comp_id")

                .Table.Rows.Add(ldrCompDeta)
            End With

            'actualizo comp_id en los cheques
            For Each ldrPago As DataRow In ldrPagosPD
                ldrPago.Item("paco_comp_id") = ldrComp.Item("comp_id")
            Next

            dTotalPostTmp = dTotalPost

            'actualizo las cuentas_ctes   (solo las que no sean intereses)
            ' For Each ldrAplic As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("_comp_coti_id<>" & Constantes.ComprobTipos.Interes_CtaCte & " AND _comp_cpti_id = " & SRA_Neg.Constantes.ComprobPagosTipos.CtaCte, "ctac_comp_id,_comp_coti_id")
            For Each ldrAplic As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select(" _comp_cpti_id = " & SRA_Neg.Constantes.ComprobPagosTipos.CtaCte, "ctac_comp_id,_comp_coti_id")
                'For Each ldrAplic As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("ctac_canc_comp_id=" & ldrComp.Item("comp_id") & " and ctac_impor<>0")

                dImpoAplic = ldrAplic.Item("ctac_impor")

                ldrCoac = mdsDatos.Tables(mstrTablaAcuses).NewRow
                With ldrCoac
                    .Item("coac_id") = clsSQLServer.gObtenerId(.Table, "coac_id")
                    .Item("coac_comp_id") = ldrAplic.Item("ctac_comp_id")
                    .Item("coac_covt_id") = ldrAplic.Item("ctac_covt_id")
                    .Item("_comp_coti_id") = ldrAplic.Item("_comp_coti_id")
                    .Item("coac_canc_comp_id") = ldrComp.Item("comp_id")

                    .Table.Rows.Add(ldrCoac)
                End With

                If dImpoAplic <= dTotalPostTmp Then 'si el importe de la aplicaci�n est� completamente incluido en el postdatado
                    ldrCoac.Item("coac_impor") = dImpoAplic
                    ldrAplic.Delete()

                    'If ldrCoac.Item("_comp_coti_id") <> SRA_Neg.Constantes.ComprobTipos.Interes_CtaCte Then
                    dTotalPostTmp = dTotalPostTmp - dImpoAplic
                    'End If

                    If dTotalPostTmp = 0 Then
                        Exit For
                    End If
                Else        'dTotalAplic > dTotalPostTmp
                    'si hace falta, parto la cta_Cte (agrego una nueva y modifico el importe de la original)
                    ldrCoac.Item("coac_impor") = dTotalPostTmp

                    ldrAplic.Item("ctac_impor") = ldrAplic.Item("ctac_impor") - dTotalPostTmp
                    dTotalPostTmp = 0
                    Exit For
                End If
            Next

            If dTotalPostTmp <> 0 Then 'si todav�a falta para completar el acuse
                'actualizo las cuotas sociales (solo las que no sean intereses)
                For Each ldrAplic As DataRow In mdsDatos.Tables(mstrTablaDeuSoc).Select("", "cods_anio,cods_perio")
                    dImpoAplic = ldrAplic.Item("cods_impo")

                    If dImpoAplic <= dTotalPostTmp Then 'si el importe de la aplicaci�n est� completamente incluido en el postdatado
                        ldrAplic.Item("cods_comp_id") = ldrComp.Item("comp_id")

                        dTotalPostTmp = dTotalPostTmp - dImpoAplic
                        If dTotalPostTmp = 0 Then  'si justo coincide con el total, salgo
                            Exit For
                        End If
                    Else        'dTotalAplic > dTotalPostTmp
                        'si hace falta, parto la cuota social (agrego una nueva y modifico el importe de la original)

                        ldrCoac = mdsDatos.Tables(mstrTablaDeuSoc).NewRow
                        With ldrCoac
                            .Item("cods_id") = clsSQLServer.gObtenerId(.Table, "cods_id")
                            .Item("cods_comp_id") = ldrComp.Item("comp_id")
                            .Item("cods_soci_id") = ldrAplic.Item("cods_soci_id")
                            .Item("cods_clie_id") = ldrAplic.Item("cods_clie_id")

                            .Item("cods_anio") = ldrAplic.Item("cods_anio")
                            .Item("cods_perio") = ldrAplic.Item("cods_perio")
                            .Item("cods_peti_id") = ldrAplic.Item("cods_peti_id")
                            .Item("cods_inst_id") = ldrAplic.Item("cods_inst_id")

                            .Item("cods_coti_id") = ldrAplic.Item("cods_coti_id")
                            .Item("cods_impo") = dTotalPostTmp

                            .Table.Rows.Add(ldrCoac)
                        End With

                        ldrAplic.Item("cods_impo") = ldrAplic.Item("cods_impo") - dTotalPostTmp
                        dTotalPostTmp = 0
                        Exit For
                    End If
                Next
            End If

            If dTotalPostTmp <> 0 Then 'si todav�a falta para completar el acuse
                'actualizo los saldos a cta.
                For Each ldrAplic As DataRow In mdsDatos.Tables(mstrTablaSaldosACuenta).Select()
                    dImpoAplic = ldrAplic.Item("sact_impo")

                    If dImpoAplic <= dTotalPostTmp Then 'si el importe de la aplicaci�n est� completamente incluido en el postdatado
                        ldrAplic.Item("sact_comp_id") = ldrComp.Item("comp_id")

                        dTotalPostTmp = dTotalPostTmp - dImpoAplic
                        If dTotalPostTmp = 0 Then  'si justo coincide con el total, salgo
                            Exit For
                        End If
                    Else        'dTotalAplic > dTotalPostTmp
                        'si hace falta, parto el saldo a cta. (agrego una nueva y modifico el importe de la original)

                        ldrCoac = mdsDatos.Tables(mstrTablaSaldosACuenta).NewRow
                        With ldrCoac
                            .Item("sact_id") = clsSQLServer.gObtenerId(.Table, "sact_id")
                            .Item("sact_comp_id") = ldrComp.Item("comp_id")
                            .Item("sact_acti_id") = ldrAplic.Item("sact_acti_id")
                            .Item("sact_clie_id") = ldrAplic.Item("sact_clie_id")
                            .Item("sact_cance") = ldrAplic.Item("sact_cance")
                            .Item("sact_impo") = dTotalPostTmp

                            .Table.Rows.Add(ldrCoac)
                        End With

                        ldrAplic.Item("sact_impo") = ldrAplic.Item("sact_impo") - dTotalPostTmp
                        dTotalPostTmp = 0
                        Exit For
                    End If
                Next
            End If

            If dTotalPostTmp <> 0 Then
                Throw New AccesoBD.clsErrNeg("El importe de los valores postdatados supera al de los items que permiten ser pagados con cheques postdatados.")
            End If

            'resto el importe del acuse
            MiRow.Item("comp_neto") = MiRow.Item("comp_neto") - dTotalPost

            If MiRow.Item("comp_neto") = 0 Then
                MiRow.Delete()
            End If
        End If
    End Sub

    ' Dario 2013-07-22 genera la linea en cta ctes para la sobretasa por vtos retrasado
    Private Sub mGenerarNDSobreTasasVto()
        Dim ldSTvtos As Double
        Dim ldrComp As DataRow
        Dim ldrCompDeta As DataRow
        Dim ldrCompDetaOri As DataRow
        Dim ldrCompVtos As DataRow
        Dim ldrCtac As DataRow
        Dim ldrSobreTasasAlVto() As DataRow
        Dim ldrComprobante() As DataRow
        Dim lintCcosAnt As Integer
        Dim lintComp As Integer
        Dim lintActi As Integer

        'busco si hay intereses
        ldrSobreTasasAlVto = mdsDatos.Tables(mstrTablaCuentasCtes).Select("_comp_coti_id=" & Constantes.ComprobTipos.SobreTasasAlVto_CtaCte, "_acti_ccos_id")

        ldrCompDetaOri = mdsDatos.Tables(mstrTablaDeta).Rows(0)

        For Each ldrSTVtos As DataRow In ldrSobreTasasAlVto
            ' seteo la variable GenerarNDSobreTasasVto en true para no borrar la fila que corresponde de la cuenta corriente
            GenerarNDSobreTasasVto = True
            'ldInte = ldrInte.Item("ctac_impor")
            ldSTvtos = ldrSTVtos.Item("_comp_impo")

            ' Dario 2013-08-30 declaro la variabe que usaremos
            ' para dejar la nd como cancelada o no segun desde donde se ingresa 
            Dim comp_cance As Boolean = False

            ' Dario 2013-08-30
            ' asigno a la variable segun el tipocobranza el valor a 
            ' poner en comp_cance de la nota de debito
            Select Case TipoCobranza
                Case "AB"
                    comp_cance = False ' "Acred. Bancarias - Ingreso"
                Case "AC"
                    comp_cance = True ' "Aplicaci�n de Saldos"
                Case "TR"
                    comp_cance = True ' "Transferencias"
                Case "AS"
                    comp_cance = True ' "Aplicaci�n de Cr�ditos"
                Case Else
                    comp_cance = True ' "Cobranzas"
            End Select

            ldrComp = mdsDatos.Tables(mstrTabla).NewRow
            'agrego en comprobantes
            With ldrComp
                .ItemArray = MiRow.ItemArray
                .Item("comp_id") = clsSQLServer.gObtenerId(.Table, "comp_id")
                .Item("comp_dh") = 0       '0 = debe
                .Item("comp_cance") = comp_cance   '1 = pagado 0 = no pagado
                .Item("comp_cs") = 1       '1 = cta cte
                .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.ND
                .Item("comp_cpti_id") = SRA_Neg.Constantes.ComprobPagosTipos.CtaCte
                .Item("comp_neto") = 0
                .Item("comp_clie_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantes", ldrSTVtos.Item(1).ToString, "comp_clie_id")
                .Item("comp_letra") = clsSQLServer.gObtenerValorCampo(mstrConn, "clientes", .Item("comp_clie_id").ToString, "_ivap_letra")
                .Item("comp_acti_id") = ldrSTVtos.Item("_acti_Id_Cancela") ' ponemos el id de actividad del comprobante que cancela
                .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.SobreTasa_PagoFuera_termino
                .Table.Rows.Add(ldrComp)
            End With

            ldrCompDeta = mdsDatos.Tables(mstrTablaDeta).NewRow

            'agrego en comprob_deta
            With ldrCompDeta
                .ItemArray = ldrCompDetaOri.ItemArray
                .Item("code_id") = clsSQLServer.gObtenerId(.Table, "code_id")
                .Item("code_comp_id") = ldrComp.Item("comp_id")
                .Item("code_asoc_comp_id") = ldrSTVtos.Item("ctac_comp_id") '  
                .Table.Rows.Add(ldrCompDeta)
            End With

            ldrCompVtos = mdsDatos.Tables(mstrTablaVtos).NewRow

            ' Dario 2013-07-10 Cambio por calculo de sobretasas con vto a futuro
            ' crea Objeto para obtener el dato de actividad
            Dim _ActividadesBusiness As New Business.Facturacion.ActividadesBusiness
            Dim intCantidadDiasND As Int32 = _ActividadesBusiness.GetCantidadDiasVtoNotaDebito(ldrSTVtos.Item("_acti_Id_Cancela"))

            'agrego en comprob_vto
            With ldrCompVtos
                .Item("covt_id") = clsSQLServer.gObtenerId(.Table, "covt_id")
                .Item("covt_fecha") = DateAdd(DateInterval.Day, intCantidadDiasND, CDate(MiRow.Item("comp_ingr_fecha")))
                .Item("covt_porc") = 100
                .Item("covt_impo") = 0
                .Item("covt_comp_id") = ldrComp.Item("comp_id")

                .Table.Rows.Add(ldrCompVtos)
            End With

            'Dario 2013-07-22 actualizo canc_comp_id en la sobretasa
            ldrSTVtos.Item("ctac_comp_id") = ldrComp.Item("comp_id")
            ldrComp.Item("comp_neto") = ldSTvtos
            ldrCompVtos.Item("covt_impo") = ldSTvtos
        Next
    End Sub

    Private Sub mGenerarNDIntereses()
        Dim ldInte, ldTotalInte, ldTotalIntePaga As Double
        Dim ldrComp As DataRow
        Dim ldrCompDeta As DataRow
        Dim ldrCompDetaOri As DataRow
        Dim ldrCompVtos As DataRow
        Dim ldrCtac As DataRow
        Dim ldrCompConc As DataRow
        Dim lvdrIntereses() As DataRow
        Dim ldrIntePagaTotal As DataRow
        Dim lintCcosAnt As Integer
        Dim lintComp As Integer
        Dim lintActi As Integer

        'busco si hay intereses
        lvdrIntereses = mdsDatos.Tables(mstrTablaCuentasCtes).Select("_comp_coti_id=" & Constantes.ComprobTipos.Interes_CtaCte, "_acti_ccos_id")

        ldrCompDetaOri = mdsDatos.Tables(mstrTablaDeta).Rows(0)

        For Each ldrInte As DataRow In lvdrIntereses
            'ldInte = ldrInte.Item("ctac_impor")
            ldInte = ldrInte.Item("_comp_impo")

            'If ldrComp Is Nothing Then --> descomentar para generar una unica ND por todos los intereses.
            ldrComp = mdsDatos.Tables(mstrTabla).NewRow
            'agrego en comprobantes
            With ldrComp
                .ItemArray = MiRow.ItemArray
                .Item("comp_id") = clsSQLServer.gObtenerId(.Table, "comp_id")
                .Item("comp_dh") = 0       '0 = debe
                .Item("comp_cance") = 0    '0 = pendiente (despu�s puede cambiar)
                .Item("comp_cs") = 1       '1 = cta cte
                .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.ND
                .Item("comp_cpti_id") = SRA_Neg.Constantes.ComprobPagosTipos.CtaCte
                .Item("comp_neto") = 0
                .Item("comp_clie_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantes", ldrInte.Item(1).ToString, "comp_clie_id")
                .Item("comp_letra") = clsSQLServer.gObtenerValorCampo(mstrConn, "clientes", .Item("comp_clie_id").ToString, "_ivap_letra")
                '.Item("comp_acti_id") = Constantes.Actividades.Administracion ' Dario 2014-04-01 cambio por error
                .Item("comp_acti_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantes", ldrInte.Item(1).ToString, "comp_acti_id")
                .Table.Rows.Add(ldrComp)
            End With

            ldrCompDeta = mdsDatos.Tables(mstrTablaDeta).NewRow

            'agrego en comprob_deta
            With ldrCompDeta
                .ItemArray = ldrCompDetaOri.ItemArray
                .Item("code_id") = clsSQLServer.gObtenerId(.Table, "code_id")
                .Item("code_comp_id") = ldrComp.Item("comp_id")

                .Table.Rows.Add(ldrCompDeta)
            End With

            ldrCompVtos = mdsDatos.Tables(mstrTablaVtos).NewRow

            'agrego en comprob_vto
            With ldrCompVtos
                .Item("covt_id") = clsSQLServer.gObtenerId(.Table, "covt_id")
                .Item("covt_fecha") = DateAdd(DateInterval.Day, CInt(clsSQLServer.gParametroValorConsul(mstrConn, "para_fact_vto_dias")), CDate(MiRow.Item("comp_ingr_fecha")))
                ' Dario 2015-07-06 se cambia para que ponga el calculo con el ingre fecha
                '.Item("covt_fecha") = DateAdd(DateInterval.Day, CInt(clsSQLServer.gParametroValorConsul(mstrConn, "para_fact_vto_dias")), CDate(MiRow.Item("comp_fecha")))
                .Item("covt_porc") = 100
                .Item("covt_impo") = 0
                .Item("covt_comp_id") = ldrComp.Item("comp_id")

                .Table.Rows.Add(ldrCompVtos)
            End With

            'resto los intereses, pero agrego los que se pagaron
            'MiRow.Item("comp_neto") = MiRow.Item("comp_neto") - ldTotalInte + ldTotalIntePaga
            'End If

            'COMPROB_CONCEP
            'If ldrInte.Item("_acti_ccos_id") <> lintCcosAnt Then
            ldrCompConc = mdsDatos.Tables(mstrTablaConcep).NewRow

            'agrego en comprob_concep si cambi� el centro de costo
            With ldrCompConc
                If Not ldrInte.IsNull(1) Then
                    lintComp = ldrInte.Item(1)
                    lintActi = clsSQLServer.gObtenerValorCampo(mstrConn, "comprobantes", "@comp_id=" & lintComp.ToString(), "comp_acti_id")
                    .Item("coco_ccos_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "actividades", "@acti_id=" & lintActi.ToString(), "acti_mora_ccos_id")
                Else
                    .Item("coco_ccos_id") = DBNull.Value
                End If
                .Item("coco_id") = clsSQLServer.gObtenerId(.Table, "coco_id")
                .Item("coco_conc_id") = clsSQLServer.gObtenerValorCampo(mstrConn, "conceptos_formulas", "@conc_form_id=" & Constantes.Formulas.Recargo_por_Mora & ",@acti_id=" & lintActi, "conc_id")
                .Item("coco_impo") = 0
                .Item("coco_comp_id") = ldrComp.Item("comp_id")

                '.Item("coco_desc_ampl") = "PAGO DEUDA VENCIDA: "
                .Table.Rows.Add(ldrCompConc)
            End With
            'End If

            With ldrCompConc  'acumulo por centro de costo
                '.Item("coco_impo") += ldInte
                .Item("coco_impo") = ldInte
                .Item("coco_impo_ivai") = .Item("coco_impo")

                'If .Item("coco_impo") <> ldInte Then
                '.Item("coco_desc_ampl") = .Item("coco_desc_ampl") & " - "
                'End If
                .Item("coco_desc_ampl") = "PAGO DEUDA VENCIDA: "
                .Item("coco_desc_ampl") = .Item("coco_desc_ampl") & ldrInte.Item("_desc") & "(" & ldInte.ToString("######0.00") & ")"
            End With

            'actualizo canc_comp_id en los intereses
            ldrInte.Item("ctac_comp_id") = ldrComp.Item("comp_id")
            'ldrComp.Item("comp_neto") += ldInte
            'ldrCompVtos.Item("covt_impo") += ldInte
            ldrComp.Item("comp_neto") = ldInte
            ldrCompVtos.Item("covt_impo") = ldInte

            'busco si se pagaron los intereses
            For Each ldrIntePaga As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("ctac_comp_id=" & ldrInte.Item("ctac_comp_id").ToString & " AND ctac_covt_id=" & ldrInte.Item("ctac_covt_id").ToString & " AND _comp_coti_id=" & Constantes.ComprobTipos.ND)
                ldrIntePaga.Item("ctac_comp_id") = ldrComp.Item("comp_id")      'se cancela la ND con el comprobante nuevo
                ldrIntePaga.Item("ctac_covt_id") = ldrCompVtos.Item("covt_id")

                ldTotalIntePaga += ldrIntePaga.Item("ctac_impor")

                If ldrIntePagaTotal Is Nothing Then
                    ldrIntePagaTotal = ldrIntePaga
                Else
                    ldrIntePagaTotal.Item("ctac_impor") = ldTotalIntePaga
                    ldrIntePaga.Delete()
                End If
            Next

            lintCcosAnt = ldrInte.Item("_acti_ccos_id")

            ldTotalInte += ldInte
        Next

        If Not ldrComp Is Nothing Then
            If ldTotalInte = ldTotalIntePaga Then
                ldrComp.Item("comp_cance") = 1   'si se pagaron los intereses generados, la ND est� cancelada
            End If
        End If

        'grabar code_gene_inte desde cuentas_ctes_alta
    End Sub

    Private Sub mGenerarPagosAcred()
        Dim ldrDatos As DataRow

        'genera la aplicacion a un saldo a cuento o a un comprobante (NC), seg�n corresponda
        For Each ldrAcred As DataRow In mdsDatos.Tables(mstrTablaAcred).Select
            If Not ldrAcred.IsNull("acre_sact_id") Then
                ldrDatos = mdsDatos.Tables(mstrTablaComprobACuenta).NewRow
                With ldrDatos
                    .Item("cact_id") = clsSQLServer.gObtenerId(.Table, "cact_id")
                    .Item("cact_comp_id") = ldrAcred.Item("acre_comp_id")
                    .Item("cact_sact_id") = ldrAcred.Item("acre_sact_id")
                    .Item("cact_impo") = ldrAcred.Item("acre_impo")

                    .Table.Rows.Add(ldrDatos)
                End With

                If Not ldrAcred.IsNull("acre_auto_usua_id") Then   'si requiere autorizaci�n, graba 1 solo registro por recibo
                    If mdsDatos.Tables(mstrTablaAutor).Select("coau_comp_id=" + ldrAcred.Item("acre_comp_id").ToString).GetUpperBound(0) = -1 Then
                        ldrDatos = mdsDatos.Tables(mstrTablaAutor).NewRow
                        With ldrDatos
                            .Item("coau_id") = clsSQLServer.gObtenerId(.Table, "coau_id")
                            .Item("coau_comp_id") = ldrAcred.Item("acre_comp_id")
                            .Item("coau_auti_id") = ldrAcred.Item("acre_auto_auti_id")
                            .Item("coau_user") = ldrAcred.Item("acre_auto_usua_id")
                            .Item("coau_auto") = True

                            .Table.Rows.Add(ldrDatos)
                        End With
                    End If
                End If

            Else
                ldrDatos = mdsDatos.Tables(mstrTablaCuentasCtes).NewRow
                With ldrDatos
                    .Item("ctac_id") = clsSQLServer.gObtenerId(.Table, "ctac_id")
                    .Item("ctac_comp_id") = ldrAcred.Item("acre_comp_id")
                    .Item("ctac_canc_comp_id") = ldrAcred.Item("acre_apli_comp_id")
                    .Item("ctac_impor") = ldrAcred.Item("acre_impo")

                    .Table.Rows.Add(ldrDatos)
                End With
            End If
        Next
    End Sub

    Private Sub mGenerarAcreditacion()
        Dim ldrCoac As DataRow

        For Each ldrComp As DataRow In mdsDatos.Tables(mstrTabla).Select("comp_coti_id=" & Constantes.ComprobTipos.Recibo_Acreditacion)
            'borro las cuentas_ctes de la acreditacion y grabo como acuses (las cuotas sociales en cobran_deusoc_alta)
            For Each ldrAplic As DataRow In mdsDatos.Tables(mstrTablaCuentasCtes).Select("ctac_canc_comp_id=" & ldrComp.Item("comp_id") & " and ctac_impor<>0")
                'If ldrAplic.Item("_comp_coti_id") <> 41 Then
                ldrCoac = mdsDatos.Tables(mstrTablaAcuses).NewRow
                With ldrCoac
                    .Item("coac_id") = clsSQLServer.gObtenerId(.Table, "coac_id")
                    .Item("coac_comp_id") = ldrAplic.Item("ctac_comp_id")
                    .Item("coac_covt_id") = ldrAplic.Item("ctac_covt_id")
                    .Item("coac_impor") = ldrAplic.Item("ctac_impor")
                    .Item("_comp_coti_id") = ldrAplic.Item("_comp_coti_id")
                    .Item("coac_canc_comp_id") = ldrComp.Item("comp_id")

                    .Table.Rows.Add(ldrCoac)
                End With
                If ((GenerarNDSobreTasasVto = True And ldrAplic.Item("_comp_coti_id") <> SRA_Neg.Constantes.ComprobTipos.SobreTasasAlVto_CtaCte) _
                     Or GenerarNDSobreTasasVto = False) Then
                    ldrAplic.Delete()
                Else
                    eliminoRowsCtaCte = True
                End If
                'End If
            Next
        Next
    End Sub

    Private Sub ActualizarPagos(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTablaDet As String, ByVal pstrCampoPK As String, ByVal lintCabId As Integer, ByVal lstrCompIdOri As String)
        Dim lstrDetaId As String

        For Each odrDeta As DataRow In mdsDatos.Tables(pstrTablaDet).Select("paco_comp_id=" + lstrCompIdOri)
            If odrDeta.Item(0) <= 0 Then
                odrDeta.Item(pstrCampoPK) = lintCabId
                'ALTA 
                lstrDetaId = clsSQLServer.gAlta(lTransac, mstrUserId, pstrTablaDet, odrDeta)
            Else
                'MODIFICACION 
                clsSQLServer.gModi(lTransac, mstrUserId, pstrTablaDet, odrDeta)

                lstrDetaId = odrDeta.Item(0)
            End If

            ActualizarDetas(lTransac, mstrTablaBilletes, "bill_paco_id", lstrDetaId, "bill_paco_id=" + odrDeta.Item("paco_id").ToString)
        Next
    End Sub

    Public Sub ModiImpreso(ByVal pstrId As String, ByVal pbooImpreso As Boolean)
        mdsDatos = clsSQLServer.gObtenerEstruc(mstrConn, mstrTabla, pstrId)

        With mdsDatos.Tables(0)
            .TableName = mstrTabla
            If UCase(mstrTabla) = "PROFORMAS" Then
                .Rows(0).Item("prfr_impre") = pbooImpreso
            Else
                .Rows(0).Item("comp_impre") = pbooImpreso
            End If
        End With

        While mdsDatos.Tables.Count > 1
            mdsDatos.Tables.Remove(mdsDatos.Tables(1))
        End While

        Modi()
    End Sub

    Public Property AcuseId() As String
        Get
            Return (mstrAcuseId)
        End Get
        Set(ByVal Value As String)
            mstrAcuseId = Value
        End Set
    End Property

    Public Property AcuseNume() As String
        Get
            Return (mstrAcuseNume)
        End Get
        Set(ByVal Value As String)
            mstrAcuseNume = Value
        End Set
    End Property

    Public Property AcreId() As String
        Get
            Return (mstrAcreId)
        End Get
        Set(ByVal Value As String)
            mstrAcreId = Value
        End Set
    End Property

    Public Property ReciId() As String
        Get
            Return (mstrReciId)
        End Get
        Set(ByVal Value As String)
            mstrReciId = Value
        End Set
    End Property

    Public Property ReciNume() As String
        Get
            Return (mstrReciNume)
        End Get
        Set(ByVal Value As String)
            mstrReciNume = Value
        End Set
    End Property

    Public Property FactId() As String
        Get
            Return (mstrFactId)
        End Get
        Set(ByVal Value As String)
            mstrFactId = Value
        End Set
    End Property

    Public Property FactNume() As String
        Get
            Return (mstrFactNume)
        End Get
        Set(ByVal Value As String)
            mstrFactNume = Value
        End Set
    End Property

    Public Property NDIds() As String
        Get
            Return (mstrNDIds)
        End Get
        Set(ByVal Value As String)
            mstrNDIds = Value
        End Set
    End Property

    Public Property NDNumes() As String
        Get
            Return (mstrNDNumes)
        End Get
        Set(ByVal Value As String)
            mstrNDNumes = Value
        End Set
    End Property

    Public Property CompIds() As String
        Get
            Return (mstrCompIds)
        End Get
        Set(ByVal Value As String)
            mstrCompIds = Value
        End Set
    End Property

    Public Shared Function GuardarAutorizaciones(ByVal pstrConn As String, ByVal pintUserId As Integer, ByVal pintSactId As Integer, ByVal pintApro As Integer, ByVal pintAutoUserId As Integer, ByVal pintOpcion As Integer, ByVal pdsDatos As DataSet) As Integer
        Dim ldrDatos As DataRow
        Dim lintRet As Integer

        If CBool(pintApro) Then
            lintRet = Comprobantes.gObtenerAutorizaci�n(pstrConn, pintUserId, Constantes.AutorizaTipos.Saldo_a_cuenta_pertenece_a_otra_actividad, pintOpcion, pintAutoUserId)
            pintApro = lintRet
        End If


        With pdsDatos.Tables(mstrTablaAcred).Select("acre_sact_id=" & pintSactId)
            If .GetUpperBound(0) <> -1 Then
                ldrDatos = .GetValue(0)
            Else
                ldrDatos = pdsDatos.Tables(mstrTablaAcred).NewRow
                With ldrDatos
                    .Item("acre_sact_id") = pintSactId
                    .Table.Rows.Add(ldrDatos)
                End With
            End If
        End With

        With ldrDatos
            If CBool(pintApro) Then
                .Item("acre_auto_usua_id") = IIf(lintRet = 1, pintUserId, pintAutoUserId)
                .Item("acre_auto_auti_id") = Constantes.AutorizaTipos.Saldo_a_cuenta_pertenece_a_otra_actividad
            Else
                .Item("acre_auto_usua_id") = DBNull.Value
                .Item("acre_auto_auti_id") = DBNull.Value
            End If
        End With

        Return (lintRet)
    End Function
End Class