Imports System.Net.Mail
Imports System.Net
Imports System
Imports System.Security.Cryptography.X509Certificates

Public Class clsMail
    Public Shared Sub gEnviarMensaje(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTemplate As String, ByVal pstrIdsUsers As String, ByVal pdsDatos As DataSet, Optional ByVal pstrAdjuPath As String = "")
        Dim lvstrUsers() As String
        Dim lstrParaLoc As String
        Dim lstrMail As String
        Dim lstrUsuarioActual As String
        Dim lstrLnk As String

        If System.Configuration.ConfigurationSettings.AppSettings("conEnviarMails").ToString() = "1" Then
            lstrUsuarioActual = clsSQLServer.gObtenerValorCampo(pstrConn, Constantes.gTab_Usuarios, pstrUserId, "usua_apel").ToString

            lvstrUsers = Split(pstrIdsUsers, ",")
            For i As Integer = 0 To lvstrUsers.GetUpperBound(0)
                lstrMail = ""
                If lvstrUsers(i) <> "" Then
                    If IsNumeric(lvstrUsers(i)) Then 'si es numerico, es el usuario
                        lstrMail = clsSQLServer.gObtenerValorCampo(pstrConn, Constantes.gTab_Usuarios, lvstrUsers(i), "usua_email").ToString
                    Else
                        lstrMail = lvstrUsers(i)
                    End If
                    lstrParaLoc += ";" + lstrMail
                End If
            Next
            lstrLnk = System.Configuration.ConfigurationSettings.AppSettings("conLnkSistema").ToString()
            gEnviarMensajeDir(pstrConn, pstrTemplate, lstrParaLoc, pdsDatos, lstrLnk, lstrUsuarioActual, pstrAdjuPath)
        End If
    End Sub

    Public Shared Sub gEnviarMensajeDir(ByVal pstrConn As String, ByVal pstrTemplate As String, ByVal pstrMails As String, ByVal pdsDatos As DataSet, ByVal pstrLink As String, ByVal pstrUsuarioActual As String, Optional ByVal pstrAdjuPath As String = "")
        Dim lstrMsg As String = mLeerTemplate(pstrTemplate + ".htm")
        Dim lstrAsunto As String = mLeerTemplate(pstrTemplate + "_asunto.txt")
        Dim lstrText As String
        Dim lstrMsgRet As String
        Dim lintPosiIni As Integer
        Dim lintPosiFin As Integer
        Dim lstrTempHead As String
        Dim lstrTempRepe As String
        Dim lstrTempFoot As String
        Dim lstrTemp As String

        If System.Configuration.ConfigurationSettings.AppSettings("conEnviarMails").ToString() = "1" Then
            If pstrMails = "" Then
                Return
            End If

            lintPosiIni = lstrMsg.IndexOf("<!-- inicio_repeticion -->")
            lintPosiFin = lstrMsg.IndexOf("<!-- fin_repeticion -->")

            If (lintPosiIni <> -1) Then
                If (lintPosiIni <> 0) Then
                    lstrTempHead = lstrMsg.Substring(0, lintPosiIni - 1)
                End If
                lstrTempRepe = lstrMsg.Substring(lintPosiIni + 26, (lintPosiFin - lintPosiIni - 26))
                lstrTempFoot = lstrMsg.Substring(lintPosiFin + 23)
            Else
                lstrTempRepe = lstrMsg
            End If

            lstrMsgRet = lstrTempHead
            Dim ldrRow As DataRow

            With pdsDatos.Tables(0)
                For i As Integer = 0 To .DefaultView.Count - 1
                    lstrTemp = lstrTempRepe
                    ldrRow = .DefaultView.Item(i).Row
                    For Each ldcCol As DataColumn In .Columns
                        If Not ldrRow.IsNull(ldcCol.ColumnName) Then
                            If ldcCol.DataType.Name = "DateTime" Then
                                lstrText = CDate(ldrRow.Item(ldcCol.ColumnName)).ToString("dd/MM/yyyy HH:mm")
                            Else
                                lstrText = ldrRow.Item(ldcCol.ColumnName).ToString
                            End If
                        Else
                            lstrText = ""
                        End If

                        lstrText = lstrText.Replace(Chr(10) + Chr(13), "<br>")

                        lstrTemp = lstrTemp.Replace("[" + ldcCol.ColumnName + "]", lstrText)
                        lstrAsunto = lstrAsunto.Replace("[" + ldcCol.ColumnName + "]", lstrText)

                    Next
                    lstrMsgRet += lstrTemp
                Next
            End With

            lstrMsgRet += lstrTempFoot

            lstrMsgRet = lstrMsgRet.Replace("[getdate]", Date.Now.ToString("dd/MM/yyyy HH:mm").Replace("lnksist", pstrLink))
            lstrMsgRet = lstrMsgRet.Replace("lnksist", pstrLink)
            lstrAsunto = lstrAsunto.Replace("[getdate]", Date.Now.ToString("dd/MM/yyyy HH:mm"))

            lstrMsgRet = lstrMsgRet.Replace("[_usua_actual]", pstrUsuarioActual)
            lstrAsunto = lstrAsunto.Replace("[_usua_actual]", pstrUsuarioActual)

            gEnviarMail(pstrMails, lstrAsunto, lstrMsgRet, pstrAdjuPath)
        End If
    End Sub

    Public Shared Function mLeerTemplate(ByVal pstrFile As String) As String
        Dim sr As New System.IO.StreamReader(System.Configuration.ConfigurationSettings.AppSettings("conMailTemplates").ToString() + pstrFile, System.Text.Encoding.UTF7)
        Dim lstrFile As String = sr.ReadToEnd()
        sr.Close()
        Return (lstrFile)
    End Function

    Public Shared Sub gEnviarMail(ByVal pstrPara As String, ByVal pstrAsunto As String, ByVal pstrMensaje As String, Optional ByVal pstrAdjuPath As String = "", Optional ByVal pbooSoloTexto As Boolean = False)
        Dim lstrMailServ As String = System.Configuration.ConfigurationSettings.AppSettings("conMailServ").ToString()
        Dim lstrMailFrom As String = System.Configuration.ConfigurationSettings.AppSettings("conMailFrom").ToString()
        Dim varError As String = ""

        If pstrPara.Replace("&nbsp;", "") = "" Then
            Return
        End If

        Try
            ' Paso 1 
            varError = "Paso 1 crea cliente smpt"
            Dim cliente = New SmtpClient
            varError = "Paso 1.1 carga purto y el host smtp"
            cliente.Port = System.Configuration.ConfigurationSettings.AppSettings("smtpPuerto").ToString()
            cliente.Host = System.Configuration.ConfigurationSettings.AppSettings("conMailServ").ToString()
            ' Se descartan las credenciales por defecto
            varError = "Paso 1.2 UseDefaultCredentials=false"
            cliente.UseDefaultCredentials = False
            ' Credenciales de autenticaci�n
            varError = "Paso 1.3 crea credenciales"
            cliente.Credentials = New Net.NetworkCredential(System.Configuration.ConfigurationSettings.AppSettings("sendusername").ToString(), System.Configuration.ConfigurationSettings.AppSettings("sendpassword").ToString())
            ' Los mensajes de correo electr�nico se env�an a un servidor SMTP a trav�s de la red
            varError = "Paso 1.5 setea el DeliveryMethod SmtpDeliveryMethod.Network"
            cliente.DeliveryMethod = SmtpDeliveryMethod.Network
            varError = "Paso 1.6 EnableSsl"
            cliente.EnableSsl = System.Configuration.ConfigurationSettings.AppSettings("EnableSsl").ToString()
            varError = "Paso 1.7 crea MailMessage"
            Dim lmsgMail As New MailMessage
            varError = "Paso 1.8 direccionOrigen = New MailAddress"
            Dim direccionOrigen = New MailAddress(lstrMailFrom, "Sociedad Rural Argentina - Envios")
            varError = "Paso 1.9 lmsgMail.From = direccionOrigen"
            lmsgMail.From = direccionOrigen
            varError = "Paso 1.9.1 Dim direccionDestino = New MailAddress(pstrPara); pstrPara=" + pstrPara
            If (pstrPara.StartsWith(";")) Then
                pstrPara = pstrPara.Substring(1, (pstrPara.Length - 1))
            End If
            varError = varError + "pstrPara.Substring(1, pstrPara.Length) =" + pstrPara
            Dim direccionDestino = New MailAddress(pstrPara)
            varError = "Paso 1.9.2 lmsgMail.To.Add(direccionDestino)"
            lmsgMail.To.Add(direccionDestino)
            varError = "Paso 1.9.3 lmsgMail.Subject = pstrAsunto"
            lmsgMail.Subject = pstrAsunto
            ' Paso 2
            varError = "Paso 2 Body BodyEncoding"
            lmsgMail.BodyEncoding = Encoding.UTF8
            varError = "Paso 2.1 Body IsBodyHtml"
            If pbooSoloTexto = True Then
                lmsgMail.IsBodyHtml = False
            Else
                lmsgMail.IsBodyHtml = True
            End If
            ' Paso 3
            varError = "Paso 3 Body"
            lmsgMail.Body = pstrMensaje
            ' Paso 4
            varError = "Paso 4 Body Priority"
            lmsgMail.Priority = MailPriority.Normal

            ' Paso 5
            varError = "Paso 5 pstrAdjuPath"
            If pstrAdjuPath <> "" Then
                Dim lobjAttach = New Web.Mail.MailAttachment(pstrAdjuPath, Web.Mail.MailEncoding.Base64)
                lmsgMail.Attachments.Add(lobjAttach)
            End If
            ' Paso 6
            varError = "Paso 6 ServicePointManager.ServerCertificateValidationCallback"
            ServicePointManager.ServerCertificateValidationCallback = _
                    Function(se As Object, _
                    cert As System.Security.Cryptography.X509Certificates.X509Certificate, _
                    chain As System.Security.Cryptography.X509Certificates.X509Chain, _
                    sslerror As System.Net.Security.SslPolicyErrors) True

            ' Env�o del mensaje
            varError = "Paso 7 cliente.Send"
            cliente.Send(lmsgMail)
            ' Paso 8
            varError = "Paso 8"

            'With lmsgMail
            '    .From = lstrMailFrom
            '    .To = pstrPara
            '    .CC = ""
            '    .Bcc = ""
            '    .Subject = pstrAsunto

            '    ' Paso 2
            '    varError = "Paso 2"

            '    If pbooSoloTexto Then
            '        .BodyFormat = MailFormat.Text
            '    Else
            '        .BodyFormat = MailFormat.Html
            '    End If

            '    ' Paso 3
            '    varError = "Paso 3"

            '    .Body = pstrMensaje
            '    .Priority = MailPriority.Normal

            '    ' Paso 4
            '    varError = "Paso 4"

            '    If System.Configuration.ConfigurationSettings.AppSettings("smtpauthenticate").ToString() <> "" Then
            '        .Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserver", System.Configuration.ConfigurationSettings.AppSettings("conMailServ").ToString())
            '        .Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", System.Configuration.ConfigurationSettings.AppSettings("smtpauthenticate").ToString())
            '        .Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", System.Configuration.ConfigurationSettings.AppSettings("sendusername").ToString())
            '        .Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", System.Configuration.ConfigurationSettings.AppSettings("sendpassword").ToString())
            '        .Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", System.Configuration.ConfigurationSettings.AppSettings("smtpPuerto").ToString())
            '        .Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", "2")
            '        .Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", System.Configuration.ConfigurationSettings.AppSettings("EnableSsl").ToString())
            '    End If

            '    ' Paso 5
            '    varError = "Paso 5"

            '    If pstrAdjuPath <> "" Then
            '        Dim lobjAttach = New MailAttachment(pstrAdjuPath, MailEncoding.Base64)
            '        .Attachments.Add(lobjAttach)
            '    End If
            'End With
            '' Paso 6
            'varError = "Paso 6"
            ''SmtpMail.SmtpServer.Insert(0, lstrMailServ)
            '' Paso 7
            'varError = "Paso 7"
            ''SmtpMail.Send(lmsgMail)
            '.Mail.SmtpMail.Send(lmsgMail)
            ' Paso 8
            ' varError = "Paso 8"
        Catch ex As Exception
            clsError.gManejarError(New Exception(ex.Message + " - " + varError, ex))
            Throw New Exception("Error al conectar al servidor de correo", ex)
        End Try
    End Sub

    Public Shared Sub gEnviarMailErrores(ByVal pstrConn As String, ByVal pdtCriadores As DataTable, ByVal pdtDatos As DataTable, ByVal pintProceso As Integer)
        Dim lstrTemplate As String = mLeerTemplate("informe_errores_template.htm")
        Dim lstrAsunto As String = mLeerTemplate("informe_errores_asunto.txt")
        Dim lstrLogo As String = mLeerTemplate("logo_sra.txt")
        Dim lstrMensaje As String
        Dim lstrPrdtID As String
        Dim lstrMail As String
        Dim lstrCabeObserva As String
        Dim lstrObserva As String = ""

        lstrAsunto = lstrAsunto.Replace("[PROCESO]", mProcesoDesc(pintProceso))

        For Each ldrCabeRow As DataRow In pdtCriadores.Select("", "cria_id asc")
            With ldrCabeRow
                lstrMail = clsSQLServer.gObtenerValorCampo(pstrConn, "criadores_mail", ldrCabeRow(1).ToString(), "cria_email").ToString     '"panta51@hotmail.com"			'
                lstrMensaje = lstrTemplate
                lstrMensaje = lstrMensaje.Replace("[LOGO]", lstrLogo)
                lstrMensaje = lstrMensaje.Replace("[FECHA]", mFecha(Date.Today.ToShortDateString()))
                lstrMensaje = lstrMensaje.Replace("[RAZA]", .Item(0).ToString())
                lstrMensaje = lstrMensaje.Replace("[CRIADOR]", .Item(3).ToString())
                lstrMensaje = lstrMensaje.Replace("[NROCRIADOR]", .Item(2).ToString())
                lstrMensaje = lstrMensaje.Replace("[PROCESO]", mProcesoDesc(pintProceso))

                For Each ldrRow As DataRow In pdtDatos.Select("cria_id=" & ldrCabeRow(1).ToString())
                    If lstrPrdtID <> ldrRow.Item(4).ToString() Then
                        lstrPrdtID = ldrRow.Item(4).ToString()
                        If lstrObserva <> "" Then lstrMensaje = lstrMensaje.Replace("[REPETICION]", lstrObserva)
                        lstrObserva = ""
                        lstrCabeObserva = mCabeObservaHTML(pintProceso)
                        lstrCabeObserva = lstrCabeObserva.Replace("[PRODUCTO]", lstrPrdtID)
                        lstrCabeObserva = lstrCabeObserva.Replace("[PADRE]", ldrRow.Item(6).ToString())
                        lstrCabeObserva = lstrCabeObserva.Replace("[MADRE]", ldrRow.Item(7).ToString())
                        lstrCabeObserva = lstrCabeObserva.Replace("[SERVICIO]", ldrRow.Item(8).ToString())
                        lstrMensaje = lstrMensaje.Replace("<!--OBSERVACIONES-->", lstrCabeObserva)
                    Else
                        lstrObserva = lstrObserva & ObservaHTML(ldrRow)
                    End If
                Next
                If lstrObserva <> "" Then lstrMensaje = lstrMensaje.Replace("[REPETICION]", lstrObserva)
                gEnviarMail(lstrMail, lstrAsunto, lstrMensaje)

            End With
        Next

    End Sub

    Private Shared Function mFecha(ByVal laFecha As String) As String
        'Ejemplo: #25-11-2008# devuelve "25 de Noviembre de 2008"
        Dim mes As Integer
        mes = Month(laFecha)
        Select Case mes
            Case 1 : Return (CStr(Day(laFecha)) & " de Enero de " & CStr(Year(laFecha)))
            Case 2 : Return (CStr(Day(laFecha)) & " de Febrero de " & CStr(Year(laFecha)))
            Case 3 : Return (CStr(Day(laFecha)) & " de Marzo de " & CStr(Year(laFecha)))
            Case 4 : Return (CStr(Day(laFecha)) & " de Abril de " & CStr(Year(laFecha)))
            Case 5 : Return (CStr(Day(laFecha)) & " de Mayo de " & CStr(Year(laFecha)))
            Case 6 : Return (CStr(Day(laFecha)) & " de Junio de " & CStr(Year(laFecha)))
            Case 7 : Return (CStr(Day(laFecha)) & " de Julio de " & CStr(Year(laFecha)))
            Case 8 : Return (CStr(Day(laFecha)) & " de Agosto de " & CStr(Year(laFecha)))
            Case 9 : Return (CStr(Day(laFecha)) & " de Septiembre de " & CStr(Year(laFecha)))
            Case 10 : Return (CStr(Day(laFecha)) & " de Octubre de " & CStr(Year(laFecha)))
            Case 11 : Return (CStr(Day(laFecha)) & " de Noviembre de " & CStr(Year(laFecha)))
            Case 12 : Return (CStr(Day(laFecha)) & " de Diciembre de " & CStr(Year(laFecha)))
        End Select

    End Function

    Private Shared Function mProcesoDesc(ByVal pintProceso As Integer) As String
        Select Case pintProceso
            Case 1 : Return ("Denuncias de Nacimiento")
            Case 2 : Return ("Denuncias de Servicio")
            Case 5 : Return ("Transferencias de Productos")
            Case 6 : Return ("Implante y Recuperacion de Embriones")
            Case 7 : Return ("Declaracion de Nacimientos de Porcinos")
            Case 8 : Return ("Movimiento de Plantel de Porcinos")
            Case 9 : Return ("Importacion de Productos")
            Case 10 : Return ("Exportacion de Productos")
            Case 11 : Return ("Exportacion de Semen")
            Case 12 : Return ("Exportacion de Embriones")
            Case 13 : Return ("Importacion de Embriones")
            Case 14 : Return ("Transferencia de Embriones")
            Case 15 : Return ("Importacion de Semen")
            Case 16 : Return ("Transferencia de Semen")
        End Select
    End Function

    Private Shared Function mCabeObservaHTML(ByVal pintProceso As Integer) As String
        Dim strObse As String

        Select Case pintProceso
            Case 1, 8   ' Nacimiento

                strObse = "<TABLE CELLSPACING='0' CELLPADDING='0' style='WIDTH:100%;'>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD COLSPAN='2' class='r1'>[PRODUCTO]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD class='a8' COLSPAN='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>PADRE:</B> [PADRE]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD class='a8' COLSPAN='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>MADRE:</B> [MADRE]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD class='a8' COLSPAN='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>Servicio:</B> [SERVICIO]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:3.00mm'><TD COLSPAN='2'>&nbsp;</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD COLSPAN='2' class='a8'>*Observaciones:</TD></TR>"
                strObse = strObse & "<TR><TD COLSPAN='2'><TABLE CELLSPACING='0' CELLPADDING='0' BORDER='0' style='WIDTH:100%;'>[REPETICION]</TABLE></TD></TR></TABLE>"
                strObse = strObse & "<BR><!--OBSERVACIONES-->"
                Return (strObse)
            Case 2   ' Servicio
                strObse = "<TABLE CELLSPACING='0' CELLPADDING='0' style='WIDTH:100%;'>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD COLSPAN='2' class='r1'><b>Servicio:</B> [SERVICIO]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD class='a8' COLSPAN='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>PADRE:</B> [PADRE]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD class='a8' COLSPAN='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>MADRE:</B> [MADRE]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:3.00mm'><TD COLSPAN='2'>&nbsp;</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD COLSPAN='2' class='a8'>*Observaciones:</TD></TR>"
                strObse = strObse & "<TR><TD COLSPAN='2'><TABLE CELLSPACING='0' CELLPADDING='0' BORDER='0' style='WIDTH:100%;'>[REPETICION]</TABLE></TD></TR></TABLE>"
                strObse = strObse & "<BR><!--OBSERVACIONES-->"

                Return (strObse)
            Case 5, 6, 7, 9, 10, 11, 15, 16   ' tramites
                strObse = "<TABLE CELLSPACING='0' CELLPADDING='0' style='WIDTH:100%;'>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD COLSPAN='2' class='r1'>[PRODUCTO]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD class='a8' COLSPAN='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>PADRE:</B> [PADRE]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD class='a8' COLSPAN='2'>&nbsp;&nbsp;&nbsp;&nbsp;<b>MADRE:</B> [MADRE]</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:3.00mm'><TD COLSPAN='2'>&nbsp;</TD></TR>"
                strObse = strObse & "<TR style='HEIGHT:6.35mm'><TD COLSPAN='2' class='a8'>*Observaciones:</TD></TR>"
                strObse = strObse & "<TR><TD COLSPAN='2'><TABLE CELLSPACING='0' CELLPADDING='0' BORDER='0' style='WIDTH:100%;'>[REPETICION]</TABLE></TD></TR></TABLE>"
                strObse = strObse & "<BR><!--OBSERVACIONES-->"
                Return (strObse)
                'Case 12, 13, 14  
        End Select
    End Function

    Private Shared Function ObservaHTML(ByVal pdrRow As DataRow) As String
        Dim strObse As String
        strObse = "<TR><TD style='WIDTH:20%;' class='a7'>" & pdrRow.Item(5).ToString() & "&nbsp;</TD><TD class='a71'> - " & pdrRow.Item(10).ToString() & "</TD></TR>"
        Return (strObse)

    End Function

End Class



Public Class clsSSL
    Public Function AcceptAllCertifications(ByVal sender As Object, ByVal certification As System.Security.Cryptography.X509Certificates.X509Certificate, ByVal chain As System.Security.Cryptography.X509Certificates.X509Chain, ByVal sslPolicyErrors As System.Net.Security.SslPolicyErrors) As Boolean
        Return True
    End Function
End Class
