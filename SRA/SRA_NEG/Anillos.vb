Imports System.Data.SqlClient

Public Class Anillos
   Inherits Generica

   Protected mstrTablaAni As String
   Protected mstrTablaRaza As String
   Protected server As System.Web.HttpServerUtility

	Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaAni As String, ByVal pstrTablaRaza As String, ByVal pdsDatos As DataSet, ByVal server As System.Web.HttpServerUtility)
		Me.mstrConn = pstrConn
		Me.mstrUserId = pstrUserId
		Me.mstrTabla = pstrTabla
		Me.mstrTablaAni = pstrTablaAni
		Me.mstrTablaRaza = pstrTablaRaza
		Me.mdsDatos = pdsDatos
		Me.server = server
	End Sub

	Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaAni As String, ByVal pstrTablaRaza As String)
		Me.mstrConn = pstrConn
		Me.mstrUserId = pstrUserId
		Me.mstrTabla = pstrTabla
		Me.mstrTablaAni = pstrTablaAni
		Me.mstrTablaRaza = pstrTablaRaza
	End Sub

	Public Overloads Function Alta() As String
		Return (mActualizar())
	End Function

	Public Overloads Sub Modi()
		mActualizar()
	End Sub

	Private Function mActualizar() As String
		Dim lstrSoliId As String
		Dim lTransac As SqlClient.SqlTransaction

		Try

			lTransac = clsSQLServer.gObtenerTransac(mstrConn)

			'Cabecera de la solicitud.
			If CInt(MiRow.Item("amca_id")) <= 0 Then
				'ALTA TABLA
				lstrSoliId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
			Else
				'MODIFICACION TABLA
				clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
				lstrSoliId = MiRow.Item("amca_id").ToString
			End If

			'Detalle de la solicitud.
			For Each odrDetaAni As DataRow In mdsDatos.Tables(mstrTablaAni).Select()
				If odrDetaAni.Item("amde_amca_id") Is DBNull.Value Then
					odrDetaAni.Item("amde_amca_id") = lstrSoliId
					'ALTA 
					clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaAni, odrDetaAni)
				Else
					'MODIFICACION 
					clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaAni, odrDetaAni)
				End If
			Next

			If mstrTablaRaza <> "" Then
				'Nuevas razas del criadero.
				For Each odrDetaRaza As DataRow In mdsDatos.Tables(mstrTablaRaza).Select()
					If odrDetaRaza.Item("crrz_amca_id") Is DBNull.Value Then
						'ALTA 
						odrDetaRaza.Item("crrz_amca_id") = lstrSoliId
						clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaRaza, odrDetaRaza)
					Else
						'MODIFICACION 
						clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaRaza, odrDetaRaza)
					End If
				Next
			End If

			clsSQLServer.gCommitTransac(lTransac)

			Return (lstrSoliId)

		Catch ex As Exception
			clsSQLServer.gRollbackTransac(lTransac)
			clsError.gManejarError(ex)
			Throw (ex)
		End Try
	End Function
End Class