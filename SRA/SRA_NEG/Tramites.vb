Imports System.IO
Imports System.XML
Imports ReglasValida.Validaciones
Imports SRA_Entidad
Imports AccesoBD



Public Class Tramites
    Inherits GenericaRel

    Private mdsDatosProd As DataSet
    Private mdsDatosMadre As DataSet
    Private mdsDatosRelaciones As DataSet
    Private mdsDatosPadre As DataSet
    Private mstrCmd As String
    Private mbooEmbriones As Boolean
    Private mintTtraId As Integer

    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_Tramites_Deta
    Private mstrTablaDocu As String = SRA_Neg.Constantes.gTab_Tramites_Docum
    Private mstrTablaObse As String = SRA_Neg.Constantes.gTab_Tramites_Obse
    Private mstrTablaPersonas As String = SRA_Neg.Constantes.gTab_Tramites_Personas
    Private mstrTablaProductos As String = SRA_Neg.Constantes.gTab_Productos
    Private mstrTablaEmbriones As String = SRA_Neg.Constantes.gTab_EmbrionesStock
    Private mstrTablaTramitesProductos As String = SRA_Neg.Constantes.gTab_Tramites_Productos
    Private mstrTablaDenuncia As String = SRA_Neg.Constantes.gTab_TeDenun
    Private mstrTablaSemen As String = SRA_Neg.Constantes.gTab_SemenStock
    Private mstrTablaAnalisis As String = SRA_Neg.Constantes.gTab_ProductosAnalisis

    ' Dario 2014-06-12 - sobrecarga del constructor base que acepta transaccion
    Public Sub New(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrUserId As String, ByVal pstrTabla As String, _
    ByVal pdsDatos As DataSet, ByVal pdsDatosProd As DataSet, _
    ByVal pContext As System.Web.HttpContext, ByVal pbooEmbriones As Boolean, _
    ByVal pintTtraId As Integer, ByVal pdsDatosMadre As DataSet, _
    ByVal pdsDatosPadre As DataSet)
        Me.mstTransac = lTransac
        mstrUserId = pstrUserId
        mstrTabla = pstrTabla
        mdsDatos = pdsDatos
        lContext = pContext
        mdsDatosProd = pdsDatosProd
        mbooEmbriones = pbooEmbriones
        mintTtraId = pintTtraId
        mdsDatosMadre = pdsDatosMadre
        mdsDatosPadre = pdsDatosPadre
        Me.mstrConn = ""
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, _
    ByVal pdsDatos As DataSet, ByVal pdsDatosProd As DataSet, _
    ByVal pContext As System.Web.HttpContext, ByVal pbooEmbriones As Boolean, _
    ByVal pintTtraId As Integer, ByVal pdsDatosMadre As DataSet, _
    ByVal pdsDatosPadre As DataSet)
        mstrConn = pstrConn
        mstrUserId = pstrUserId
        mstrTabla = pstrTabla
        mdsDatos = pdsDatos
        lContext = pContext
        mdsDatosProd = pdsDatosProd
        mbooEmbriones = pbooEmbriones
        mintTtraId = pintTtraId
        mdsDatosMadre = pdsDatosMadre
        mdsDatosPadre = pdsDatosPadre
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, _
    ByVal pdsDatos As DataSet, ByVal pdsDatosProd As DataSet, _
    ByVal pContext As System.Web.HttpContext, ByVal pbooEmbriones As Boolean, _
    ByVal pintTtraId As Integer, ByVal pdsDatosMadre As DataSet, _
    ByVal pdsDatosPadre As DataSet, ByVal pdsDatosRelaciones As DataSet)
        mstrConn = pstrConn
        mstrUserId = pstrUserId
        mstrTabla = pstrTabla
        mdsDatos = pdsDatos
        lContext = pContext
        mdsDatosProd = pdsDatosProd
        mbooEmbriones = pbooEmbriones
        mintTtraId = pintTtraId
        mdsDatosMadre = pdsDatosMadre
        mdsDatosPadre = pdsDatosPadre
        mdsDatosRelaciones = pdsDatosRelaciones

    End Sub

    ' Dario 2014-06-12 - sobrecarga del constructor base que acepta transaccion
    Public Sub New(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrUserId As String)
        Me.mstTransac = lTransac
        Me.mstrUserId = pstrUserId
        Me.mstrConn = ""
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub

    Private Property lstrColPK As String

    Public Overloads Function Alta() As String
        Select Case mintTtraId
            Case SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones
                Return mActualImportacionEmbriones("A")

            Case SRA_Neg.Constantes.TramitesTipos.ImportacionProductos
                Return mActualizarAnimalEnPie("A")

            Case SRA_Neg.Constantes.TramitesTipos.ImportacionSemen
                Return mActualizarImportacionSemen("A")

            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen
                Return mActualizarTransferenciaSemen("A")

            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones
                Return mActualizarTransferenciaEmbriones("A")

            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos
                Return mActualTransferenciaAnimalEnPie("A")

            Case SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones
                Return mActualExportacionEmbriones("A")

            Case SRA_Neg.Constantes.TramitesTipos.ExportacionSemen
                Return mActualExportacionSemen("A")

            Case SRA_Neg.Constantes.TramitesTipos.ExportacionProductos
                Return mActualExportacionAnimalEnPie("A")
            Case Else
                Return mActualizar()
        End Select

    End Function

    Public Overloads Sub Modi()

        Select Case mintTtraId
            Case SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones
                mActualImportacionEmbriones("M")

            Case SRA_Neg.Constantes.TramitesTipos.ImportacionSemen
                mActualizarImportacionSemen("M")

            Case SRA_Neg.Constantes.TramitesTipos.ImportacionProductos
                mActualizarAnimalEnPie("M")

            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen
                mActualizarTransferenciaSemen("M")

            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones
                mActualizarTransferenciaEmbriones("M")

            Case SRA_Neg.Constantes.TramitesTipos.TransferenciaProductos
                mActualTransferenciaAnimalEnPie("M")

            Case SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones
                mActualExportacionEmbriones("M")

            Case SRA_Neg.Constantes.TramitesTipos.ExportacionSemen
                mActualExportacionSemen("M")

            Case SRA_Neg.Constantes.TramitesTipos.ExportacionProductos
                mActualExportacionAnimalEnPie("M")

            Case Else
                mActualizar()
        End Select
    End Sub

    Public Sub BajaLogica(ByVal TramiteId As Object)
        If Not mdsDatos Is Nothing Then
            For Each odrBaja As DataRow In mdsDatos.Tables(Constantes.gTab_Tramites).Select("tram_id=" + TramiteId)
                odrBaja.Item("tram_esta_id") = SRA_Neg.Constantes.Estados.Tramites_Baja
                ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
                If (mstrConn Is Nothing Or mstrConn = "") Then
                    clsSQLServer.gModi(Me.mstTransac, mstrUserId, Constantes.gTab_Tramites, odrBaja)
                Else
                    clsSQLServer.gModi(mstrConn, mstrUserId, Constantes.gTab_Tramites, odrBaja)
                End If
            Next
        End If
    End Sub
    Public Sub GrabarEstadoRetenidaEnTramites(ByVal TramiteId As Object)
        GrabarEstado(TramiteId, SRA_Neg.Constantes.Estados.Tramites_Retenida)
    End Sub

    Private Sub GrabarEstado(ByVal pTramiteId As Object, ByVal pEstadoId As Object)
        Dim mstrCmd As String

        mstrCmd = "exec SetEstadoDeTramite "
        mstrCmd = mstrCmd + " @TramiteId = " + clsSQLServer.gFormatArg(pTramiteId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @EstadoId = " + clsSQLServer.gFormatArg(pEstadoId, SqlDbType.VarChar)
        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            clsSQLServer.gExecuteNONQuery(Me.mstTransac, mstrCmd)
        Else
            clsSQLServer.gExecuteNONQuery(mstrConn, mstrCmd)
        End If
    End Sub

    Public Sub GrabarEstadoVigenteEnTramites(ByVal TramiteId As String)
        GrabarEstado(TramiteId, SRA_Neg.Constantes.Estados.Tramites_Vigente)

    End Sub
    Public Sub GrabarEstadoEnTramites(ByVal TramiteId As String, ByVal EstadoId As Int16)
        GrabarEstado(TramiteId, EstadoId)
    End Sub
    Public Sub GrabarEstadoBajaEnTramites(ByVal TramiteId As String)

        If Not mdsDatos Is Nothing Then
            For Each odrEstado As DataRow _
            In mdsDatos.Tables(Constantes.gTab_Tramites).Select("tram_id=" + TramiteId)
                odrEstado.Item("tram_esta_id") = SRA_Neg.Constantes.Estados.Tramites_Baja
                odrEstado.Item("tram_fina_fecha") = Today
                odrEstado.Item("tram_baja_fecha") = Today
                odrEstado.Item("tram_fina_user") = mstrUserId

                mdsDatos.AcceptChanges()
                ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
                If (mstrConn Is Nothing Or mstrConn = "") Then
                    clsSQLServer.gModi(Me.mstTransac, mstrUserId, Constantes.gTab_Tramites, odrEstado)
                Else
                    clsSQLServer.gModi(mstrConn, mstrUserId, Constantes.gTab_Tramites, odrEstado)
                End If
            Next
        End If

    End Sub

    Private Overloads Function mActualizar() As String
        'Dim lstrTramId, lstrProdId, lstrId As String
        Dim lstrProdId, lstrId As String
        lstrId = Nothing
        Dim lstrProdIdOri As String = Nothing
        Dim lstrSemenId As String
        Dim lstrEmbrionId As String
        Dim lstrDenunciaId As String
        Dim lbooProdAlta As Boolean = False
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String
        'Dim lstrProdMadre As String
        'Dim lstrProdPadre As String

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If Not mdsDatosProd Is Nothing Then
                BorrarDetas(lTransac, Constantes.gTab_ProductosRelaciones, mdsDatosProd)
                BorrarDetas(lTransac, Constantes.gTab_ProductosNumeros, mdsDatosProd)

                If Not IsNothing(mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis)) Then
                    BorrarDetas(lTransac, Constantes.gTab_ProductosAnalisis, mdsDatosProd)
                End If



                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    If Not odrProd.IsNull(0) Then




                        If CInt(odrProd.Item(0)) <= 0 Then
                            'ALTA TABLA
                            lstrProdId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                            odrProd(0) = lstrProdId

                            'ACTUALIZO CON EL ID DEL PRODUCTO NUEVO
                            For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                odrRela.Item("ptre_prdt_id") = lstrProdId
                            Next

                            For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_supe_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                odrRela.Item("ptre_supe_prdt_id") = lstrProdId
                            Next

                            For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosNumeros).Select("ptnu_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                odrRela.Item("ptnu_prdt_id") = lstrProdId
                            Next

                            If mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then
                                For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                    odrRela.Item("trpr_prdt_id") = lstrProdId
                                Next
                            End If

                            For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                odrRela.Item("trpr_prdt_id") = lstrProdId
                            Next

                            If Not IsNothing(mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis)) Then
                                For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis).Select("prta_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                    odrRela.Item("prta_prdt_id") = lstrProdId
                                Next
                            End If

                            For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosNumeros).Select("ptnu_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                odrRela.Item("ptnu_prdt_id") = lstrProdId
                            Next

                            odrProd.Item("prdt_id") = lstrProdId
                        Else
                            'MODIFICACION TABLA
                            If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Or _
                            mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionProductos Or _
                            mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Then
                                clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                            End If
                            lstrProdId = odrProd.Item(0).ToString
                        End If

                        'GUARDO EL ID DEL PRINCIPAL PARA ASOCIARLO AL TR�MITE

                        If mintTtraId <> SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then


                            lstrProdIdOri = lstrProdId
                            lbooProdAlta = True
                        End If


                    End If
                Next



                'PRODUCTOS_RELACIONES
                If lstrProdIdOri = Nothing Then
                    lstrProdIdOri = lstrId
                End If

                ActualizarDetas(lTransac, Constantes.gTab_ProductosRelaciones, "ptre_prdt_id", lstrProdIdOri, , mdsDatosProd)
                'PRODUCTOS_ANALISIS
                If Not IsNothing(mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis)) Then
                    ActualizarDetas(lTransac, Constantes.gTab_ProductosAnalisis, "prta_prdt_id", lstrProdIdOri, , mdsDatosProd)
                End If
            End If



            'TABLA TRAMITES Y SUS DETALLES
            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                Next
            End If

            'TRAMITES PRODUCTOS
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select()
                odrDeta.Item("trpr_tram_id") = lstrId
                If lbooProdAlta Then
                    odrDeta.Item("trpr_prdt_id") = IIf(lstrProdIdOri Is Nothing, DBNull.Value, lstrProdIdOri)
                End If
            Next

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrId
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)

                    'ACTUALIZO CON EL ID DETALLE NUEVO
                    For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                        odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                    Next
                End If
            Next

            lstrDenunciaId = 0
            'GUARDO TABLA DE DENUNCIAS
            '''''Importacion de Embriones, si no existen los padres se dan de alta.
            If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then
                For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
                    If IsDBNull(odrDeta.Item(0)) Then
                        odrDeta.Item(0) = 0
                    End If

                    If CInt(odrDeta.Item(0)) <= 0 Then
                        'ALTA DENUNCIAS
                        odrDeta.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDeta)
                        lstrDenunciaId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDeta)
                    Else
                        'MODIFICACION TABLA
                        clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_TeDenun, odrDeta)

                    End If
                Next

            End If


            'Importacion de Embriones, si no existen los padres se dan de alta.
            If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then
                '    'MADRE

                For Each odrProd As DataRow In mdsDatos.Tables(Constantes.gTab_EmbrionesStock).Select("")
                    '            
                    If CInt(odrProd.Item("tesk_id")) <= 0 Then
                        '                'ALTA TABLA
                        odrProd.Item("tesk_tede_id") = lstrDenunciaId
                        odrProd.Item("tesk_tram_id") = lstrId
                        lstrEmbrionId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_EmbrionesStock, odrProd)

                    Else
                        '                'MODIFICACION TABLA
                        clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_EmbrionesStock, odrProd)

                    End If
                Next


            End If


            'Importacion de Importacion Semen
            If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Then

                For Each odrProd As DataRow In mdsDatos.Tables(Constantes.gTab_SemenStock).Select("")
                    '            
                    If CInt(odrProd.Item("sest_id")) <= 0 Then
                        '                'ALTA TABLA
                        ' '''  odrProd.Item("test_tede_id") = lstrDenunciaId
                        odrProd.Item("sest_tram_id") = lstrId
                        lstrSemenId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_SemenStock, odrProd)

                    Else
                        '                'MODIFICACION TABLA
                        clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_SemenStock, odrProd)

                    End If
                Next


            End If


            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName



                If mdsDatos.Tables(i).TableName <> "te_denun" Then

                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)

                End If
            Next





            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Private Overloads Function mActualizarAnimalEnPie(ByVal pTipo As String) As String
        Dim lstrTramId, lstrProdId, lstrId As String

        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrProdMadre As String = Nothing
        Dim lstrProdPadre As String = Nothing
        Dim ContItem As Integer = Nothing
        Dim ProdIdPadre As String = Nothing
        Dim ProdIdMadre As String = Nothing
        Dim ProdIdImportado As String = Nothing

        Try
            lstrTramId = "0"

            'GUARDO EL ID DEL PRINCIPAL PARA ASOCIARLO AL TR�MITE

            If Not mdsDatos Is Nothing Then
                For Each odrTramite As DataRow In _
                 mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                    If pTipo = "M" Then
                        lstrTramId = odrTramite.Item("tram_id")
                    End If
                Next
            End If

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            BorrarDetas(lTransac, Constantes.gTab_ProductosRelaciones, mdsDatosRelaciones)
            If Not IsNothing(mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis)) Then
                BorrarDetas(lTransac, Constantes.gTab_ProductosAnalisis, mdsDatosProd)
            End If

            ContItem = 1

            Dim dtPadre As New DataTable
            Dim dtMadre As New DataTable

            If Not mdsDatosProd Is Nothing Then
                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    lstrProdId = mdsDatosProd.Tables(Constantes.gTab_Productos).Rows(0).Item("prdt_id")
                    'ACTUALIZO CON EL ID DEL PRODUCTO NUEVO
                    For Each odrRela As DataRow In mdsDatosRelaciones.Tables(Constantes.gTab_ProductosRelaciones).Select()
                        odrRela.Item("ptre_prdt_id") = lstrProdId
                    Next

                    For Each odrRela As DataRow In mdsDatosRelaciones.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_supe_prdt_id=" + odrProd.Item("prdt_id").ToString)
                        If pTipo = "A" Then
                            odrRela.Item("ptre_supe_prdt_id") = lstrProdId
                        End If
                    Next

                    For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                        odrRela.Item("trpr_prdt_id") = lstrProdId
                    Next

                    If Not IsNothing(mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis)) Then
                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis).Select("prta_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("prta_prdt_id") = lstrProdId
                        Next
                    End If
                Next
            End If


            'TABLA TRAMITES Y SUS DETALLES
            If pTipo = "A" Then
                If CInt(MiRow.Item(0)) <= 0 Then

                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                End If
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrTramId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                Next
            End If

            ContItem = 1

            Dim mstrUltIdRelacion As Integer = Nothing

            '''''relaciones
            For Each odrRelac As DataRow In mdsDatosRelaciones.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_reti_id = 1 or ptre_reti_id = 2")
                With odrRelac
                    ' Dario  2014-11-05 se comenta se maneja todo como modif si esta -1 el id inserta y controla el 
                    ' sp que no ingresen duplicados
                    'If pTipo = "A" Then
                    '    'ALTA 
                    '    mstrUltIdRelacion = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_ProductosRelaciones, odrRelac)
                    'Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_ProductosRelaciones, odrRelac)
                    lstrId = .Item(0)
                    'End If
                End With
            Next

            ''PRODUCTOS_ANALISIS
            If Not IsNothing(mdsDatos.Tables(Constantes.gTab_ProductosAnalisis)) Then
                ActualizarDetas(lTransac, Constantes.gTab_ProductosAnalisis, "prta_prdt_id", lstrProdId, , mdsDatosProd)
            End If

            'TRAMITES PRODUCTOS
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select()
                odrDeta.Item("trpr_tram_id") = lstrTramId
                If pTipo = "A" Then
                    odrDeta.Item("trpr_prdt_id") = lstrProdId
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
                End If
            Next

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)

                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                        Next
                    End If
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                End If
            Next

            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)

                    'ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrObse.Item("_trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrObse.Item("_trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrObse.Item("trao_id")
                        Next
                    End If

                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next



            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                End If
            Next

            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                If mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "productos" Then
                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)
                End If
            Next


            'Roxi: 03/08/2007 chequear y aprobar luego de aplicar las reglas.
            'Si se cumplieron todos los requisitos se finaliza el tramite.
            'mstrCmd = "exec tramites_aprobar " & lstrId & ", " & mstrUserId
            'clsSQLServer.gExecute(lTransac, mstrCmd)

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Private Overloads Function mActualizarEmbriones(ByVal pTipo As Object) As String

        Dim lstrTramId, lstrProdId, lstrId As String
        lstrTramId = Nothing
        lstrProdId = Nothing
        lstrId = Nothing
        Dim lstrProdIdOri As String = Nothing
        Dim lstrSemenId As String = Nothing
        Dim lstrEmbrionId As String = Nothing
        Dim lstrDenunciaId As String = Nothing
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String = Nothing
        Dim lstrProdMadre As String = Nothing
        Dim lstrProdPadre As String = Nothing
        Dim dtDenuncia As New DataTable
        Dim dtEmbrionesStock As New DataTable

        Try
            Dim oDenuncia As New SRA_Neg.Denuncias(mstrConn, mstrUserId)

            'PRODUCTOS
            lstrTramId = "0"

            If Not mdsDatosProd Is Nothing Then
                BorrarDetas(lTransac, Constantes.gTab_ProductosRelaciones, mdsDatosProd)

                If pTipo = "M" Then
                    For Each odrProd As DataRow In _
                            mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")
                        If CInt(odrProd.Item("prdt_id")) <= 0 Then
                            If UCase(odrProd.Item("prdt_sexo")) = "TRUE" Then
                                odrProd.Item("prdt_id") = GetPadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            Else
                                odrProd.Item("prdt_id") = GetMadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            End If
                        End If
                    Next
                End If

                lTransac = clsSQLServer.gObtenerTransac(mstrConn)

                If Not mdsDatos Is Nothing Then
                    For Each odrTramite As DataRow In _
                     mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                        lstrTramId = odrTramite.Item("tram_id")

                        If pTipo = "M" Then
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Tramites, odrTramite)
                        End If
                    Next
                End If

                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    If CInt(odrProd.Item(0)) <= 0 Then
                        'ALTA TABLA
                        If pTipo = "A" Then
                            lstrProdId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                            odrProd(0) = lstrProdId
                        Else
                            lstrProdId = odrProd.Item("prdt_id").ToString()
                        End If

                        'ACTUALIZO CON EL ID DEL PRODUCTO NUEVO
                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_supe_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_supe_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        If Not IsNothing(mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis)) Then
                            For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis).Select("prta_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                odrRela.Item("prta_prdt_id") = lstrProdId
                            Next
                        End If

                        odrProd.Item("prdt_id") = lstrProdId
                        odrProd.AcceptChanges()
                    Else
                        'MODIFICACION TABLA
                        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                        End If
                        lstrProdId = odrProd.Item(0).ToString
                    End If
                Next

                'PRODUCTOS_RELACIONES
                If lstrProdIdOri = Nothing Then
                    lstrProdIdOri = lstrId
                End If
                ActualizarDetas(lTransac, Constantes.gTab_ProductosRelaciones, "ptre_prdt_id", lstrProdIdOri, , mdsDatosProd)
            End If

            'TABLA TRAMITES Y SUS DETALLES
            If CInt(MiRow.Item(0)) <= 0 Then
                If pTipo = "A" Then
                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                Else
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                    lstrTramId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                End If

            End If

            'TRAMITES PRODUCTOS SON EMBRIONES NO HAY IMPORTACION DE UN PRODUCTO 
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select()
                odrDeta.Item("trpr_tram_id") = lstrTramId
                If pTipo = "A" Then
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
                End If
                Exit For
            Next

            lstrDenunciaId = 0
            'GUARDO TABLA DE DENUNCIAS

            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
                If IsDBNull(odrDeta.Item(0)) Then
                    odrDeta.Item(0) = 0
                End If

                If CInt(odrDeta.Item(0)) <= 0 Then
                    If pTipo = "A" Then
                        'ALTA DENUNCIAS
                        odrDeta.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDeta)
                        lstrDenunciaId = odrDeta.Item("tede_id")
                        odrDeta.AcceptChanges()
                        Exit For
                    Else
                        dtDenuncia = oDenuncia.GetDenunciaEmbrionesByTramiteId(lstrTramId, "")

                        If dtDenuncia.Rows.Count > 0 Then
                            odrDeta.Item("tede_id") = dtDenuncia.Rows(0).Item("tede_id")
                            odrDeta.Item("tede_nume") = dtDenuncia.Rows(0).Item("tede_nume")
                            odrDeta.AcceptChanges()
                            'MODIFICACION TABLA
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_TeDenun, odrDeta)
                            Exit For
                        End If
                    End If
                End If
            Next

            Dim dtEmbrionStock As DataTable
            Dim oEmbrion As New SRA_Neg.EmbrionStock(mstrConn, mstrUserId)

            For Each odrProd As DataRow In mdsDatos.Tables(Constantes.gTab_EmbrionesStock).Select("")
                If IsDBNull(odrProd.Item(0)) Then
                    odrProd.Item(0) = 0
                End If

                If CInt(odrProd.Item("tesk_id")) <= 0 Then
                    If pTipo = "A" Then
                        '                'ALTA TABLA
                        odrProd.Item("tesk_tede_id") = lstrDenunciaId
                        odrProd.Item("tesk_tram_id") = lstrTramId
                        odrProd.AcceptChanges()

                        lstrEmbrionId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_EmbrionesStock, odrProd)
                        Exit For
                    Else
                        dtEmbrionStock = oEmbrion.GetEmbrionesStockByTramiteId(lstrTramId, "")

                        If dtEmbrionStock.Rows.Count > 0 Then
                            odrProd.Item("tesk_id") = dtEmbrionStock.Rows(0).Item("tesk_id")
                            odrProd.Item("tesk_tede_id") = dtEmbrionStock.Rows(0).Item("tesk_tede_id")
                            odrProd.AcceptChanges()
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_EmbrionesStock, odrProd)
                            Exit For
                        End If
                    End If
                End If
            Next

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.AcceptChanges()
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                    odrDeta.AcceptChanges()
                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                            odrRela.AcceptChanges()
                        Next
                    End If
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                End If
            Next

            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.AcceptChanges()
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)
                    odrObse.AcceptChanges()
                    'ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrObse.Item("_trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrObse.Item("_trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrObse.Item("trao_id")
                        Next
                    End If
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next

            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                    odrDocu.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                End If
            Next

            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                If mdsDatos.Tables(i).TableName.ToLower <> "te_denun" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "embriones_stock" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "semen_stock" Then
                    '    mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos"  ' Dario 2014-10-03
                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)
                End If
            Next

            'Roxi: 03/08/2007 chequear y aprobar luego de aplicar las reglas.
            'Si se cumplieron todos los requisitos se finaliza el tramite.
            'mstrCmd = "exec tramites_aprobar " & lstrId & ", " & mstrUserId
            'clsSQLServer.gExecute(lTransac, mstrCmd)

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function

    Private Overloads Function mActualizarSemen(ByVal pTipo As String) As String

        Dim lstrTramId, lstrProdId, lstrId As String
        lstrTramId = Nothing
        lstrProdId = Nothing
        lstrId = Nothing

        Dim lstrProdIdOri As String = Nothing
        Dim lstrSemenId As String = Nothing
        Dim lstrDenunciaId As String = Nothing
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String = Nothing
        Dim lstrProdMadre As String = Nothing
        Dim lstrProdPadre As String = Nothing
        Dim ProdIdPadre As Integer

        Try


            'PRODUCTOS
            lstrTramId = "0"

            ProdIdPadre = mdsDatosPadre.Tables("Productos").Rows(0).Item("prdt_id").ToString()



            If Not mdsDatosProd Is Nothing Then
                BorrarDetas(lTransac, Constantes.gTab_ProductosRelaciones, mdsDatosProd)

                If pTipo = "M" Then


                    For Each odrProd As DataRow In _
                 mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")
                        If CLng(odrProd.Item("prdt_id")) <= 0 Then
                            odrProd.Item("prdt_id") = ProdIdPadre
                        End If

                    Next

                End If


                lTransac = clsSQLServer.gObtenerTransac(mstrConn)


                If Not mdsDatos Is Nothing Then
                    For Each odrTramite As DataRow In _
                     mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                        lstrTramId = odrTramite.Item("tram_id")


                        If pTipo = "M" Then

                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Tramites, odrTramite)

                        End If
                    Next



                End If

                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    If CLng(odrProd.Item(0)) <= 0 Then
                        'ALTA TABLA

                        If pTipo = "A" Then
                            lstrProdId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                            odrProd(0) = lstrProdId
                        Else
                            lstrProdId = odrProd.Item("prdt_id").ToString()
                        End If


                        'ACTUALIZO CON EL ID DEL PRODUCTO NUEVO
                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_supe_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_supe_prdt_id") = lstrProdId
                        Next


                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        If Not IsNothing(mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis)) Then
                            For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis).Select("prta_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                odrRela.Item("prta_prdt_id") = lstrProdId
                            Next
                        End If



                        odrProd.Item("prdt_id") = lstrProdId
                    Else
                        'MODIFICACION TABLA
                        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Then
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                        End If
                        lstrProdId = odrProd.Item(0).ToString
                    End If



                Next


                'PRODUCTOS_RELACIONES
                If lstrProdIdOri = Nothing Then
                    lstrProdIdOri = lstrId
                End If

                ActualizarDetas(lTransac, Constantes.gTab_ProductosRelaciones, "ptre_prdt_id", lstrProdIdOri, , mdsDatosProd)

            End If



            'TABLA TRAMITES Y SUS DETALLES
            If CLng(MiRow.Item(0)) <= 0 Then
                If pTipo = "A" Then
                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                Else
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                    lstrTramId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                End If

            End If

            'TRAMITES PRODUCTOS
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select()

                odrDeta.Item("trpr_tram_id") = lstrTramId
                If pTipo = "A" Then

                    odrDeta.Item("trpr_prdt_id") = IIf(lstrProdIdOri Is Nothing, DBNull.Value, lstrProdIdOri)
                End If
            Next


            lstrDenunciaId = 0
            'GUARDO TABLA DE DENUNCIAS
            Dim dtDenuncia As DataTable
            Dim dtSemenStock As DataTable
            Dim oDenuncia As New SRA_Neg.Denuncias(mstrConn, mstrUserId)

            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
                If IsDBNull(odrDeta.Item(0)) Then
                    odrDeta.Item(0) = 0
                End If

                If CInt(odrDeta.Item(0)) <= 0 Then

                    If pTipo = "A" Then
                        'ALTA DENUNCIAS
                        odrDeta.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDeta)
                        lstrDenunciaId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDeta)
                        odrDeta.AcceptChanges()
                        Exit For
                    Else

                        dtDenuncia = oDenuncia.GetDenunciaSemenByTramiteId(lstrTramId, "")
                        If dtDenuncia.Rows.Count > 0 Then
                            odrDeta.Item("tede_id") = dtDenuncia.Rows(0).Item("tede_id")
                            odrDeta.Item("tede_nume") = dtDenuncia.Rows(0).Item("tede_nume")
                            'MODIFICACION TABLA
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_TeDenun, odrDeta)
                            odrDeta.AcceptChanges()
                            Exit For
                        End If
                    End If
                End If
            Next





            For Each odrProd As DataRow In mdsDatos.Tables(Constantes.gTab_SemenStock).Select()
                If IsDBNull(odrProd.Item(0)) Then
                    odrProd.Item(0) = 0
                End If

                If CInt(odrProd.Item("sest_id")) <= 0 Then
                    If pTipo = "A" Then
                        odrProd.Item("sest_tede_id") = lstrDenunciaId
                        odrProd.Item("sest_tram_id") = lstrTramId
                        odrProd.Item("sest_prdt_id") = lstrProdId
                        odrProd.Item("sest_usopropio") = odrProd.Item("sest_usopropio")
                        odrProd.AcceptChanges()
                        lstrSemenId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_SemenStock, odrProd)

                        Exit For
                    Else

                        dtSemenStock = GetSemenStockByTramiteId(lstrTramId, "")
                        If dtSemenStock.Rows.Count > 0 Then
                            odrProd.Item("sest_id") = dtSemenStock.Rows(0).Item("sest_id")
                            odrProd.Item("sest_tede_id") = dtSemenStock.Rows(0).Item("sest_tede_id")
                            odrProd.Item("sest_usopropio") = odrProd.Item("sest_usopropio")
                            odrProd.AcceptChanges()

                            '                'MODIFICACION TABLA
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_SemenStock, odrProd)

                        End If
                    End If

                End If
            Next

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                    odrDeta.AcceptChanges()

                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                            odrRela.AcceptChanges()
                        Next
                    End If
                    odrDeta.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)

                End If

            Next



            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.AcceptChanges()
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)
                    odrObse.AcceptChanges()
                    ''''ACTUALIZO CON EL ID DETALLE NUEVO
                    '''If odrObse.Item("_trad_requ_id").ToString <> "" Then
                    '''    For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrObse.Item("_trad_requ_id").ToString)
                    '''        odrRela.Item("trao_trad_id") = odrObse.Item("trao_id")
                    '''    Next
                    '''End If

                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next





            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.AcceptChanges()
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                    odrDocu.AcceptChanges()


                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)

                End If

            Next





            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName


                If mdsDatos.Tables(i).TableName.ToLower <> "te_denun" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "embriones_stock" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "semen_stock" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "tramites_deta" And _
                     mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" Then


                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)

                End If
            Next

            'Roxi: 03/08/2007 chequear y aprobar luego de aplicar las reglas.
            'Si se cumplieron todos los requisitos se finaliza el tramite.
            'mstrCmd = "exec tramites_aprobar " & lstrId & ", " & mstrUserId
            'clsSQLServer.gExecute(lTransac, mstrCmd)

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function

    Private Overloads Function mActualizarImportacionSemen(ByVal pTipo) As String

        Dim lstrTramId, lstrProdId, lstrId As String
        Dim lstrProdIdOri As String
        Dim lstrSemenId As String
        Dim lstrDenunciaId As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String
        Dim lstrProdMadre As String
        Dim lstrProdPadre As String
        Dim ProdIdPadre As Integer

        Try
            Dim oDenuncia As New SRA_Neg.Denuncias(mstrConn, mstrUserId)
            'PRODUCTOS
            lstrTramId = "0"
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If Not mdsDatos Is Nothing Then
                For Each odrTramite As DataRow In _
                 mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                    lstrTramId = odrTramite.Item("tram_id")
                    If pTipo = "M" Then
                        clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Tramites, odrTramite)
                    End If
                Next
            End If

            'PRODUCTOS
            If Not mdsDatosProd Is Nothing Then
                BorrarDetas(lTransac, Constantes.gTab_ProductosRelaciones, mdsDatosProd)
                BorrarDetas(lTransac, Constantes.gTab_ProductosNumeros, mdsDatosProd)
                If pTipo = "M" Then
                    For Each odrProd As DataRow In _
                            mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")
                        If CLng(odrProd.Item("prdt_id")) <= 0 Then
                            If UCase(ODRPROD.Item("prdt_sexo")) = "TRUE" Then
                                odrProd.Item("prdt_id") = GetPadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            Else
                                odrProd.Item("prdt_id") = GetMadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            End If
                        End If
                    Next
                End If

                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    If CLng(odrProd.Item(0)) <= 0 Then
                        'ALTA TABLA
                        If pTipo = "A" Then
                            lstrProdId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                            odrProd(0) = lstrProdId
                        Else
                            lstrProdId = odrProd.Item("prdt_id").ToString()
                        End If

                        'ACTUALIZO CON EL ID DEL PRODUCTO NUEVO
                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_supe_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_supe_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis).Select("prta_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("prta_prdt_id") = lstrProdId
                        Next

                        odrProd.Item("prdt_id") = lstrProdId
                        odrProd.AcceptChanges()
                    Else
                        'MODIFICACION TABLA
                        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionSemen Then
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                        End If
                        lstrProdId = odrProd.Item(0).ToString
                    End If
                Next

                'PRODUCTOS_RELACIONES
                If lstrProdIdOri = Nothing Then
                    lstrProdIdOri = lstrId
                End If

                ActualizarDetas(lTransac, Constantes.gTab_ProductosRelaciones, "ptre_prdt_id", lstrProdIdOri, , mdsDatosProd)
            End If

            'TABLA TRAMITES Y SUS DETALLES
            If CInt(MiRow.Item(0)) <= 0 Then
                If pTipo = "A" Then
                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                Else
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                    lstrTramId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                End If
            End If

            lstrDenunciaId = 0
            mdsdatos.AcceptChanges()

            'GUARDO TABLA DE DENUNCIAS
            Dim dtDenuncia As DataTable

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                    odrDeta.AcceptChanges()

                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                            odrRela.AcceptChanges()
                        Next
                    End If
                    odrDeta.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                End If
            Next

            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.AcceptChanges()
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)
                    odrObse.AcceptChanges()
                    ''''ACTUALIZO CON EL ID DETALLE NUEVO
                    '''If odrObse.Item("_trad_requ_id").ToString <> "" Then
                    '''    For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrObse.Item("_trad_requ_id").ToString)
                    '''        odrRela.Item("trao_trad_id") = odrObse.Item("trao_id")
                    '''    Next
                    '''End If
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next

            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.AcceptChanges()
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                    odrDocu.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                End If
            Next

            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                If pTipo = "A" Then
                    ' If mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _ Dario 2014-10-03
                    If mdsDatos.Tables(i).TableName.ToLower <> "te_denun" And _
                        mdsDatos.Tables(i).TableName.ToLower <> "semen_stock" And _
                        mdsDatos.Tables(i).TableName.ToLower <> "tramites" Then
                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)
                    End If
                Else
                    If mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _
                        mdsDatos.Tables(i).TableName.ToLower <> "te_denun" And _
                        mdsDatos.Tables(i).TableName.ToLower <> "semen_stock" And _
                        mdsDatos.Tables(i).TableName.ToLower <> "productos" And _
                        mdsDatos.Tables(i).TableName.ToLower <> "tramites" Then
                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)
                    End If
                End If
            Next

            For Each odrDenun As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
                If odrDenun.Item(0) Is System.DBNull.Value Then
                    odrDenun.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)
                Else
                    'clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)
                End If
            Next

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function

    Private Overloads Function mActualizarTransferenciaSemen(ByVal pTipo) As String
        Dim lstrTramId, lstrProdId, lstrId As String
        Dim lstrProdIdOri As String
        Dim lstrSemenId As String
        Dim lstrDenunciaId As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String
        Dim lstrProdMadre As String
        Dim lstrProdPadre As String
        Try
            'PRODUCTOS
            lstrTramId = "0"

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If Not mdsDatosProd Is Nothing Then
                BorrarDetas(lTransac, Constantes.gTab_ProductosRelaciones, mdsDatosProd)
                BorrarDetas(lTransac, Constantes.gTab_ProductosNumeros, mdsDatosProd)
                If pTipo = "M" Then
                    For Each odrProd As DataRow In _
                            mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")
                        If CLng(odrProd.Item("prdt_id")) <= 0 Then
                            If UCase(ODRPROD.Item("prdt_sexo")) = "TRUE" Then
                                odrProd.Item("prdt_id") = GetPadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            End If
                        End If
                    Next
                End If

                If Not mdsDatos Is Nothing Then
                    For Each odrTramite As DataRow In _
                     mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                        lstrTramId = odrTramite.Item("tram_id")

                        If pTipo = "M" Then
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Tramites, odrTramite)
                        End If
                    Next

                End If

                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    If CLng(odrProd.Item(0)) <= 0 Then
                        'ALTA TABLA
                        If pTipo = "A" Then
                            'lstrProdId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                            odrProd(0) = lstrProdId
                        Else
                            lstrProdId = odrProd.Item("prdt_id").ToString()
                        End If

                        'ACTUALIZO CON EL ID DEL PRODUCTO NUEVO
                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_prdt_id") = lstrProdId
                            odrRela.Item("ptre_audi_user") = mstrUserId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_supe_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_supe_prdt_id") = lstrProdId
                            odrRela.Item("ptre_audi_user") = mstrUserId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosNumeros).Select("ptnu_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptnu_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        If Not IsNothing(mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis)) Then
                            For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis).Select("prta_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                odrRela.Item("prta_prdt_id") = lstrProdId
                            Next
                        End If

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosNumeros).Select("ptnu_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptnu_prdt_id") = lstrProdId
                        Next

                        odrProd.Item("prdt_id") = lstrProdId
                    Else
                        'MODIFICACION TABLA
                        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaSemen Then
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                        End If
                        lstrProdId = odrProd.Item(0).ToString
                    End If
                Next

                'PRODUCTOS_RELACIONES
                If lstrProdIdOri = Nothing Then
                    lstrProdIdOri = lstrProdId
                End If

                Dim mstrUltIdRelacion As Integer

                For Each odrRelac As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select
                    With odrRelac
                        If pTipo = "A" Then
                            If .Item("ptre_id") <= 0 Then
                                If .Item("ptre_reti_id") = 1 Then
                                    .Item("ptre_supe_prdt_id") = lstrProdId
                                Else
                                    .Item("ptre_supe_prdt_id") = System.DBNull.Value
                                End If
                                .Item("ptre_prdt_id") = lstrProdId

                            End If
                            .Item("ptre_audi_user") = mstrUserId
                            odrRelac.AcceptChanges()
                            'ALTA 
                            '               mstrUltIdRelacion = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_ProductosRelaciones, odrRelac)
                        Else
                            'MODIFICACION 
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_ProductosRelaciones, odrRelac)
                            lstrId = .Item(0)
                        End If

                    End With
                Next

                ActualizarDetas(lTransac, Constantes.gTab_ProductosRelaciones, "ptre_prdt_id", lstrProdIdOri, , mdsDatosProd)

            End If

            ' ActualizarDetas(lTransac, Constantes.gTab_ProductosAnalisis, "prta_prdt_id", lstrProdIdOri, , mdsDatosProd)
            If lstrProdId Is Nothing Then
                For Each odrProd As DataRow In mdsDatos.Tables(Constantes.gTab_Tramites).Select
                    lstrProdId = odrProd.Item("tram_embr_padre_id")
                Next
            End If

            'TABLA TRAMITES Y SUS DETALLES
            If CInt(MiRow.Item(0)) <= 0 Then
                If pTipo = "A" Then
                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                End If
            Else
                If pTipo = "M" Then
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                    lstrTramId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                End If
            End If
            ' Dario 2014-07-22 se descomenta hay que insertarlo
            'TRAMITES PRODUCTOS
            'For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select()
            '    odrDeta.Item("trpr_tram_id") = lstrTramId
            '    If pTipo = "A" Then
            '        odrDeta.Item("trpr_prdt_id") = IIf(lstrProdIdOri Is Nothing, DBNull.Value, lstrProdIdOri)
            '    End If
            'Next

            'TRAMITES PRODUCTOS
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select()
                odrDeta.Item("trpr_tram_id") = lstrTramId
                If pTipo = "A" Then
                    odrDeta.Item("trpr_prdt_id") = lstrProdId
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
                End If
            Next

            lstrDenunciaId = 0
            'GUARDO TABLA DE DENUNCIAS
            Dim dtDenuncia As DataTable
            Dim dtSemenStock As DataTable
            Dim oDenuncia As New SRA_Neg.Denuncias(mstrConn, mstrUserId)
            Dim oSemen As New SRA_Neg.SemenStock(mstrConn, mstrUserId)

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                    odrDeta.AcceptChanges()

                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                            odrRela.AcceptChanges()
                        Next
                    End If
                    odrDeta.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)

                End If
            Next

            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.AcceptChanges()
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)
                    odrObse.AcceptChanges()
                    ''''ACTUALIZO CON EL ID DETALLE NUEVO
                    '''If odrObse.Item("_trad_requ_id").ToString <> "" Then
                    '''    For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrObse.Item("_trad_requ_id").ToString)
                    '''        odrRela.Item("trao_trad_id") = odrObse.Item("trao_id")
                    '''    Next
                    '''End If

                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next

            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.AcceptChanges()
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                    odrDocu.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)

                End If
            Next

            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                If mdsDatos.Tables(i).TableName.ToLower <> "te_denun" And _
                     mdsDatos.Tables(i).TableName.ToLower <> "semen_stock" And _
                     mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "productos_analisis" Then

                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)
                End If
            Next

            For Each odrDenun As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
                If odrDenun.Item(0) Is System.DBNull.Value Then
                    dtSemenStock = oSemen.GetSemenStockByTramiteId(lstrTramId, "")

                    If dtSemenStock.Rows.Count <= 0 Then
                        ' recien puedo dar de alta  uns dennuncia cuando agrego en semen_stock
                        odrDenun.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)

                        lstrDenunciaId = odrDenun.Item("tede_id")
                    Else
                        ' la denuncia no sepuede  modificar 
                        'clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)
                    End If
                End If
            Next

            ' Dario 2014-07-22 se sacar no corresponde
            'For Each odrProd As DataRow In mdsDatos.Tables(Constantes.gTab_SemenStock).Select()
            '    If IsDBNull(odrProd.Item(0)) Then
            '        odrProd.Item(0) = 0
            '    End If

            '    If CInt(odrProd.Item("sest_id")) <= 0 Then
            '        odrProd.Item("sest_tede_id") = lstrDenunciaId
            '        odrProd.Item("sest_tram_id") = lstrTramId
            '        odrProd.Item("sest_prdt_id") = lstrProdId
            '        odrProd.Item("sest_cant") = 0
            '        odrProd.Item("sest_cria_cant") = 0
            '        odrProd.Item("sest_auto") = 0   ' manual
            '        odrProd.Item("sest_tipo_movi") = SRA_Neg.Constantes.TipoMovimientos.DenunciaRecuperacionSemen
            '        odrProd.AcceptChanges()
            '        lstrSemenId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_SemenStock, odrProd)
            '        Exit For
            '    End If
            'Next

            'Roxi: 03/08/2007 chequear y aprobar luego de aplicar las reglas.
            'Si se cumplieron todos los requisitos se finaliza el tramite.
            'mstrCmd = "exec tramites_aprobar " & lstrId & ", " & mstrUserId
            'clsSQLServer.gExecute(lTransac, mstrCmd)

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function

    Private Overloads Function mActualizarTransferenciaEmbriones(ByVal pTipo) As String

        Dim lstrTramId, lstrProdId, lstrProdIdPadre, lstrProdIdMadre, lstrId As String
        Dim lstrProdIdOri As String
        Dim lstrEmbrionId As String
        Dim lstrDenunciaId As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String
        Dim lstrProdMadre As String
        Dim lstrProdPadre As String

        Try
            'PRODUCTOS
            lstrTramId = "0"

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'If Not mdsDatos Is Nothing Then
            '    For Each odrTramite As DataRow In _
            '        mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
            '        lstrTramId = odrTramite.Item("tram_id")
            '        If pTipo = "M" Then
            '            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Tramites, odrTramite)
            '        End If
            '    Next
            'End If

            If Not mdsDatosProd Is Nothing Then
                BorrarDetas(lTransac, Constantes.gTab_ProductosRelaciones, mdsDatosProd)
                BorrarDetas(lTransac, Constantes.gTab_ProductosNumeros, mdsDatosProd)
                If pTipo = "M" Then
                    For Each odrProd As DataRow In _
                            mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")
                        If CLng(odrProd.Item("prdt_id")) <= 0 Then
                            If UCase(ODRPROD.Item("prdt_sexo")) = "TRUE" Then
                                odrProd.Item("prdt_id") = GetPadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            End If
                        End If
                    Next
                End If

                'If Not mdsDatos Is Nothing Then
                '    For Each odrTramite As DataRow In _
                '     mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                '        lstrTramId = odrTramite.Item("tram_id")
                '        If pTipo = "M" Then
                '            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Tramites, odrTramite)
                '        End If
                '    Next
                'End If

                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")
                    If CInt(odrProd.Item(0)) <= 0 Then
                        'ALTA TABLA
                        lstrProdIdPadre = ""
                        lstrProdIdMadre = ""
                        If pTipo = "A" Then
                            If UCase(ODRPROD.Item("prdt_sexo")) = "TRUE" Then
                                odrProd.Item("prdt_id") = GetPadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                                lstrProdIdPadre = odrProd.Item("prdt_id")
                            Else
                                odrProd.Item("prdt_id") = GetMadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                                lstrProdIdMadre = odrProd.Item("prdt_id")
                            End If
                        Else
                            If lstrProdIdMadre <> "" Then
                                lstrProdId = lstrProdIdMadre
                            End If
                        End If

                        ''ACTUALIZO CON EL ID DEL PRODUCTO NUEVO
                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_prdt_id") = lstrProdId
                            odrRela.Item("ptre_audi_user") = mstrUserId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_supe_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_supe_prdt_id") = lstrProdIdMadre
                            odrRela.Item("ptre_audi_user") = mstrUserId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        If Not IsNothing(mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis)) Then
                            For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis).Select("prta_prdt_id=" + odrProd.Item("prdt_id").ToString)
                                odrRela.Item("prta_prdt_id") = lstrProdId
                            Next
                        End If

                        odrProd.Item("prdt_id") = lstrProdId
                    Else
                        'MODIFICACION TABLA
                        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.TransferenciaEmbriones Then
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                        End If
                        lstrProdId = odrProd.Item(0).ToString
                    End If
                Next

                'PRODUCTOS_RELACIONES
                If lstrProdIdOri = Nothing Then
                    lstrProdIdOri = lstrProdIdMadre
                End If

                Dim mstrUltIdRelacion As Integer

                For Each odrRelac As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select
                    With odrRelac
                        If pTipo = "A" Then
                            If .Item("ptre_id") <= 0 Then
                                If .Item("ptre_reti_id") = 1 Then
                                    .Item("ptre_supe_prdt_id") = lstrProdIdMadre
                                Else
                                    .Item("ptre_supe_prdt_id") = lstrProdIdPadre
                                End If
                                .Item("ptre_prdt_id") = lstrProdId
                            End If
                            .Item("ptre_audi_user") = mstrUserId
                            odrRelac.AcceptChanges()
                            'ALTA 
                            ' mstrUltIdRelacion = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_ProductosRelaciones, odrRelac)
                        Else
                            'MODIFICACION 
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_ProductosRelaciones, odrRelac)
                            lstrId = .Item(0)
                        End If
                    End With
                Next
                ActualizarDetas(lTransac, Constantes.gTab_ProductosRelaciones, "ptre_prdt_id", lstrProdIdOri, , mdsDatosProd)
            End If

            'TABLA TRAMITES Y SUS DETALLES
            If CInt(MiRow.Item(0)) <= 0 Then
                If pTipo = "A" Then
                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                End If
            Else
                If pTipo = "M" Then
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                    lstrTramId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                End If
            End If

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                    odrDeta.AcceptChanges()

                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                            odrRela.AcceptChanges()
                        Next
                    End If
                    odrDeta.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                End If
            Next

            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.AcceptChanges()
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)
                    odrObse.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next

            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.AcceptChanges()
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                    odrDocu.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                End If
            Next

            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                If mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _
                   mdsDatos.Tables(i).TableName.ToLower <> "embriones_stock" And _
                   mdsDatos.Tables(i).TableName.ToLower <> "te_denun" And _
                   mdsDatos.Tables(i).TableName.ToLower <> "productos_analisis" Then
                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)
                End If
            Next

            'For Each odrDenun As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
            '    If odrDenun.Item(0) Is System.DBNull.Value Then
            '        odrDenun.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)
            '    Else
            '        clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)
            '    End If
            'Next

            'Roxi: 03/08/2007 chequear y aprobar luego de aplicar las reglas.
            'Si se cumplieron todos los requisitos se finaliza el tramite.
            'mstrCmd = "exec tramites_aprobar " & lstrId & ", " & mstrUserId
            'clsSQLServer.gExecute(lTransac, mstrCmd)

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function

    Private Overloads Function mActualTransferenciaAnimalEnPie(ByVal pTipo As String) As String
        Dim lstrTramId, lstrProdId, lstrId As String
        Dim lstrTramProdId As String
        Dim lstrProdIdOri As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String
        Dim lstrProdMadre As String
        Dim lstrProdPadre As String
        Dim ProdIdPadre As String
        Dim ProdIdMadre As String
        Dim ProdIdImportado As String
        Dim dtPadre As DataTable
        Dim dtMadre As DataTable

        Try




            lstrTramId = "0"

            'GUARDO EL ID DEL PRINCIPAL PARA ASOCIARLO AL TR�MITE



            If Not mdsDatos Is Nothing Then
                For Each odrTramite As DataRow In _
                 mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                    If pTipo = "M" Then
                        lstrTramId = odrTramite.Item("tram_id")
                    End If
                Next
            End If


            lTransac = clsSQLServer.gObtenerTransac(mstrConn)



            If Not mdsDatosProd Is Nothing Then
                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    If Not odrProd.IsNull(0) Then
                        lstrProdId = odrProd.ItemArray(0).ToString()
                    End If

                    For Each odrRela As DataRow In _
                    mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + _
                    odrProd.Item("prdt_id").ToString)
                        odrRela.Item("trpr_prdt_id") = lstrProdId
                    Next
                    lstrProdIdOri = lstrProdId
                    Exit For
                Next
            End If



            'TABLA TRAMITES Y SUS DETALLES
            If pTipo = "A" Then
                If CLng(MiRow.Item(0)) <= 0 Then

                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                    mdsDatos.Tables("Tramites").Rows(0).Item("tram_id") = lstrTramId
                End If
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrTramId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                Next
            End If

            'TRAMITES PRODUCTOS
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select()
                odrDeta.Item("trpr_tram_id") = lstrTramId
                If pTipo = "A" Then
                    odrDeta.Item("trpr_prdt_id") = lstrProdId
                    mdsDatos.AcceptChanges()
                    mdsDatosProd.AcceptChanges()

                    lstrTramProdId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
                    odrDeta.Item("trpr_id") = lstrTramProdId
                    mdsDatos.AcceptChanges()
                Else
                    mdsDatos.AcceptChanges()
                    mdsDatosProd.AcceptChanges()

                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)

                End If
                Exit For
            Next



            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)


                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In _
                        mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + _
                        odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                        Next
                    End If

                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)

                End If

            Next



            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)

                    'ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrObse.Item("_trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In _
                        mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + _
                        odrObse.Item("_trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrObse.Item("trao_id")
                        Next
                    End If

                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next

            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                End If

            Next


            'DETALLE

            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName


                If mdsDatos.Tables(i).TableName.ToLower <> "productos" And _
                   mdsDatos.Tables(i).TableName.ToLower <> "tramites" And _
                   mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "productos_analisis" Then

                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)
                End If

            Next



            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Private Overloads Function mActualExportacionAnimalEnPie(ByVal pTipo As String) As String
        Dim lstrTramId, lstrProdId, lstrId As String
        Dim lstrTramProdId As String
        Dim lstrProdIdOri As String
        Dim lstrColPK As String
        Dim lstrProdMadre As String
        Dim lstrProdPadre As String
        Dim ContItem As Integer
        Dim ProdIdPadre As String
        Dim ProdIdMadre As String
        Dim dtPadre As DataTable
        Dim dtMadre As DataTable

        Try
            lstrTramId = "0"

            'GUARDO EL ID DEL PRINCIPAL PARA ASOCIARLO AL TR�MITE
            If Not mdsDatos Is Nothing Then
                For Each odrTramite As DataRow In _
                 mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                    If pTipo = "M" Then
                        lstrTramId = odrTramite.Item("tram_id")
                    End If
                Next
            End If

            ContItem = 1

            If Not mdsDatosProd Is Nothing Then
                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    If Not odrProd.IsNull(0) Then
                        If pTipo = "M" Then
                            clsSQLServer.gModi(mstTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                            ContItem = ContItem + 1
                        End If
                        lstrProdId = odrProd.ItemArray(0).ToString()
                    End If

                    For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                        odrRela.Item("trpr_prdt_id") = lstrProdId
                    Next

                    lstrProdIdOri = lstrProdId
                    Exit For
                Next
            End If

            'TABLA TRAMITES Y SUS DETALLES
            If pTipo = "A" Then
                If CLng(MiRow.Item(0)) <= 0 Then

                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(mstTransac, mstrUserId, mstrTabla, MiRow)
                    mdsDatos.Tables("Tramites").Rows(0).Item("tram_id") = lstrTramId
                End If
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(mstTransac, mstrUserId, mstrTabla, MiRow)
                lstrTramId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(mstTransac, mdsDatos.Tables(i).TableName)
                Next
            End If

            'TRAMITES PRODUCTOS
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select()
                odrDeta.Item("trpr_tram_id") = lstrTramId
                If pTipo = "A" Then
                    odrDeta.Item("trpr_prdt_id") = lstrProdId
                    mdsDatos.AcceptChanges()
                    mdsDatosProd.AcceptChanges()
                    lstrTramProdId = clsSQLServer.gAlta(mstTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
                    odrDeta.Item("trpr_id") = lstrTramProdId
                    mdsDatos.AcceptChanges()

                Else
                    mdsDatos.AcceptChanges()
                    mdsDatosProd.AcceptChanges()
                    clsSQLServer.gModi(mstTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
                End If
            Next

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(mstTransac, mstrUserId, mstrTablaDeta, odrDeta)


                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                        Next
                    End If
                Else
                    clsSQLServer.gModi(mstTransac, mstrUserId, mstrTablaDeta, odrDeta)
                End If
                Exit For
            Next

            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(mstTransac, mstrUserId, mstrTablaObse, odrObse)

                    'ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrObse.Item("_trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrObse.Item("_trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrObse.Item("trao_id")
                        Next
                    End If

                Else
                    clsSQLServer.gModi(mstTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next

            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(mstTransac, mstrUserId, mstrTablaDocu, odrDocu)
                Else
                    clsSQLServer.gModi(mstTransac, mstrUserId, mstrTablaDocu, odrDocu)
                End If
            Next

            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                If mdsDatos.Tables(i).TableName.ToLower <> "productos" And _
                  mdsDatos.Tables(i).TableName.ToLower <> "tramites" And _
                  mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _
                   mdsDatos.Tables(i).TableName.ToLower <> "productos_analisis" Then
                    ActualizarDetas(mstTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)
                End If
            Next

            Return (lstrTramId)

        Catch ex As Exception
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Private Overloads Function mActualExportacionEmbriones(ByVal pTipo) As String
        Dim lstrTramId, lstrProdId, lstrId As String
        Dim lstrProdIdOri As String
        Dim lstrSemenId As String
        Dim lstrEmbrionId As String
        Dim lstrDenunciaId As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String
        Dim lstrProdMadre As String
        Dim lstrProdPadre As String
        Dim dtDenuncia As DataTable
        Dim dtEmbrionStock As DataTable

        Try
            Dim oDenuncia As New SRA_Neg.Denuncias(mstrConn, mstrUserId)

            lstrTramId = "0"

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If Not mdsDatos Is Nothing Then
                For Each odrTramite As DataRow In _
                 mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                    lstrTramId = odrTramite.Item("tram_id")

                    If pTipo = "M" Then
                        clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Tramites, odrTramite)
                    End If
                Next
            End If

            'PRODUCTOS
            If Not mdsDatosProd Is Nothing Then
                BorrarDetas(lTransac, Constantes.gTab_ProductosRelaciones, mdsDatosProd)
                BorrarDetas(lTransac, Constantes.gTab_ProductosNumeros, mdsDatosProd)
                If pTipo = "M" Then
                    For Each odrProd As DataRow In _
                            mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")
                        If CLng(odrProd.Item("prdt_id")) <= 0 Then
                            If UCase(ODRPROD.Item("prdt_sexo")) = "TRUE" Then
                                odrProd.Item("prdt_id") = GetPadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            Else
                                odrProd.Item("prdt_id") = GetMadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            End If
                        End If
                    Next
                End If


                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    If CLng(odrProd.Item(0)) <= 0 Then
                        'ALTA TABLA
                        If pTipo = "A" Then
                            lstrProdId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                            odrProd(0) = lstrProdId
                        Else
                            lstrProdId = odrProd.Item("prdt_id").ToString()
                        End If

                        'ACTUALIZO CON EL ID DEL PRODUCTO NUEVO
                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_supe_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_supe_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis).Select("prta_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("prta_prdt_id") = lstrProdId
                        Next

                        odrProd.Item("prdt_id") = lstrProdId
                        odrProd.AcceptChanges()
                    Else
                        'MODIFICACION TABLA
                        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ExportacionEmbriones Then
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                        End If
                        lstrProdId = odrProd.Item(0).ToString
                    End If
                Next

                'PRODUCTOS_RELACIONES
                If lstrProdIdOri = Nothing Then
                    lstrProdIdOri = lstrId
                End If
                ActualizarDetas(lTransac, Constantes.gTab_ProductosRelaciones, "ptre_prdt_id", lstrProdIdOri, , mdsDatosProd)
            End If

            'TABLA TRAMITES Y SUS DETALLES
            If CInt(MiRow.Item(0)) <= 0 Then
                If pTipo = "A" Then
                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                End If
            Else
                If pTipo = "M" Then
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                    lstrTramId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                End If

            End If

            ''''''TRAMITES PRODUCTOS SON EMBRIONES NO HAY IMPORTACION DE UN PRODUCTO 
            '''For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select()
            '''    odrDeta.Item("trpr_tram_id") = lstrTramId
            '''    If pTipo = "A" Then
            '''        clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
            '''    Else
            '''        clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaTramitesProductos, odrDeta)
            '''    End If
            '''    Exit For
            '''Next

            lstrDenunciaId = 0
            'GUARDO TABLA DE DENUNCIAS

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.AcceptChanges()
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                    odrDeta.AcceptChanges()
                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                            odrRela.AcceptChanges()
                        Next
                    End If
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                End If
            Next

            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.AcceptChanges()
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)
                    odrObse.AcceptChanges()
                    '''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrObse.Item("_trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrObse.Item("_trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrObse.Item("trao_id")
                        Next
                    End If
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next

            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                    odrDocu.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                End If
            Next

            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName



                If mdsDatos.Tables(i).TableName.ToLower <> "te_denun" And _
                   mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "embriones_stock" Then

                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)

                End If
            Next

            For Each odrDenun As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
                If odrDenun.Item(0) Is System.DBNull.Value Then

                    odrDenun.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)

                Else
                    '  clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)
                End If
            Next

            'Roxi: 03/08/2007 chequear y aprobar luego de aplicar las reglas.
            'Si se cumplieron todos los requisitos se finaliza el tramite.
            'mstrCmd = "exec tramites_aprobar " & lstrId & ", " & mstrUserId
            'clsSQLServer.gExecute(lTransac, mstrCmd)

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function

    Private Overloads Function mActualImportacionEmbriones(ByVal pTipo) As String

        Dim lstrTramId, lstrProdId, lstrId As String
        Dim lstrProdIdOri As String
        Dim lstrSemenId As String
        Dim lstrEmbrionId As String
        Dim lstrDenunciaId As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String
        Dim lstrProdMadre As String
        Dim lstrProdPadre As String
        Dim dtDenuncia As DataTable

        Dim dtEmbrionStock As DataTable

        Try
            Dim oDenuncia As New SRA_Neg.Denuncias(mstrConn, mstrUserId)
            lstrTramId = "0"

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If Not mdsDatos Is Nothing Then
                For Each odrTramite As DataRow In _
                 mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                    lstrTramId = odrTramite.Item("tram_id")
                    If pTipo = "M" Then
                        clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Tramites, odrTramite)
                    End If
                Next
            End If

            'PRODUCTOS
            If Not mdsDatosProd Is Nothing Then
                BorrarDetas(lTransac, Constantes.gTab_ProductosRelaciones, mdsDatosProd)
                BorrarDetas(lTransac, Constantes.gTab_ProductosNumeros, mdsDatosProd)
                If pTipo = "M" Then
                    For Each odrProd As DataRow In _
                            mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")
                        If CLng(odrProd.Item("prdt_id")) <= 0 Then
                            If UCase(ODRPROD.Item("prdt_sexo")) = "TRUE" Then
                                odrProd.Item("prdt_id") = GetPadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            Else
                                odrProd.Item("prdt_id") = GetMadreByTramiteId(lstrTramId, "").Rows(0).Item("prdt_id")
                            End If
                        End If
                    Next
                End If

                For Each odrProd As DataRow In _
                   mdsDatosProd.Tables(Constantes.gTab_Productos).Select("", "prdt_id asc")

                    If CLng(odrProd.Item(0)) <= 0 Then
                        'ALTA TABLA

                        If pTipo = "A" Then
                            lstrProdId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                            odrProd(0) = lstrProdId
                        Else
                            lstrProdId = odrProd.Item("prdt_id").ToString()
                        End If

                        'ACTUALIZO CON EL ID DEL PRODUCTO NUEVO
                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosRelaciones).Select("ptre_supe_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("ptre_supe_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaTramitesProductos).Select("trpr_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("trpr_prdt_id") = lstrProdId
                        Next

                        For Each odrRela As DataRow In mdsDatosProd.Tables(Constantes.gTab_ProductosAnalisis).Select("prta_prdt_id=" + odrProd.Item("prdt_id").ToString)
                            odrRela.Item("prta_prdt_id") = lstrProdId
                        Next

                        odrProd.Item("prdt_id") = lstrProdId
                        odrProd.AcceptChanges()
                    Else
                        'MODIFICACION TABLA
                        If mintTtraId = SRA_Neg.Constantes.TramitesTipos.ImportacionEmbriones Then
                            clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Productos, odrProd)
                        End If
                        lstrProdId = odrProd.Item(0).ToString
                    End If
                Next

                'PRODUCTOS_RELACIONES
                If lstrProdIdOri = Nothing Then
                    lstrProdIdOri = lstrId
                End If

                ActualizarDetas(lTransac, Constantes.gTab_ProductosRelaciones, "ptre_prdt_id", lstrProdIdOri, , mdsDatosProd)
            End If

            'TABLA TRAMITES Y SUS DETALLES
            If CInt(MiRow.Item(0)) <= 0 Then
                If pTipo = "A" Then
                    'ALTA TABLA
                    lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, "tramites", MiRow)
                End If
            Else
                If pTipo = "M" Then
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, "tramites", MiRow)
                    lstrTramId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                End If
            End If

            lstrDenunciaId = 0
            'GUARDO TABLA DE DENUNCIAS
            mdsdatos.AcceptChanges()

            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
                If IsDBNull(odrDeta.Item(0)) Then
                    odrDeta.Item(0) = 0
                End If

                If CInt(odrDeta.Item(0)) <= 0 Then
                    If pTipo = "A" Then
                        'ALTA DENUNCIAS
                        odrDeta.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDeta)
                        lstrDenunciaId = odrDeta.Item("tede_id")
                        odrDeta.AcceptChanges()
                        Exit For
                    Else
                        dtDenuncia = oDenuncia.GetDenunciaEmbrionesByTramiteId(lstrTramId, "")

                        If dtDenuncia.Rows.Count > 0 Then
                            odrDeta.Item("tede_id") = dtDenuncia.Rows(0).Item("tede_id")
                            odrDeta.Item("tede_nume") = dtDenuncia.Rows(0).Item("tede_nume")
                            odrDeta.AcceptChanges()
                            'MODIFICACION TABLA
                            ' clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_TeDenun, odrDeta)
                            odrDeta.AcceptChanges()
                            Exit For
                        End If
                    End If
                End If
            Next

            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.AcceptChanges()
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                    odrDeta.AcceptChanges()
                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                            odrRela.AcceptChanges()
                        Next
                    End If
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                End If
            Next

            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.AcceptChanges()
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)
                    odrObse.AcceptChanges()
                    '''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrObse.Item("_trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrObse.Item("_trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrObse.Item("trao_id")
                        Next
                    End If
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next

            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                    odrDocu.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                End If
            Next

            'DETALLE
            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                'If mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _ Dario 2014-10-03
                If mdsDatos.Tables(i).TableName.ToLower <> "embriones_stock" And _
                    mdsDatos.Tables(i).TableName.ToLower <> "te_denun" Then

                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)

                End If
            Next

            For Each odrDenun As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
                If odrDenun.Item(0) Is System.DBNull.Value Then

                    odrDenun.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)

                Else
                    ' clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)
                End If
            Next

            'Roxi: 03/08/2007 chequear y aprobar luego de aplicar las reglas.
            'Si se cumplieron todos los requisitos se finaliza el tramite.
            'mstrCmd = "exec tramites_aprobar " & lstrId & ", " & mstrUserId
            'clsSQLServer.gExecute(lTransac, mstrCmd)

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function

    Private Overloads Function mActualExportacionSemen(ByVal pTipo) As String

        Dim lstrTramId, lstrProdId, lstrId As String
        Dim lstrSemenId As String
        Dim lstrDenunciaId As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String

        Dim lstrProdPadre As String

        Try

            'PRODUCTOS
            lstrTramId = "0"
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)




            If Not mdsDatosProd Is Nothing Then

                For Each odrProd As DataRow In _
             mdsDatos.Tables(Constantes.gTab_Tramites).Select()

                    If pTipo = "M" Then
                        lstrProdPadre = odrProd("tram_embr_padre_id").ToString()

                        lstrTramId = odrProd.Item("tram_id").ToString()

                    Else
                        lstrProdPadre = odrProd("tram_embr_padre_id").ToString()
                    End If

                    Exit For
                Next


                lTransac = clsSQLServer.gObtenerTransac(mstrConn)


            End If



            If Not mdsDatos Is Nothing Then
                For Each odrTramite As DataRow In _
                 mdsDatos.Tables(Constantes.gTab_Tramites).Select("")
                    lstrTramId = odrTramite.Item("tram_id")


                    If pTipo = "M" Then
                        clsSQLServer.gModi(lTransac, mstrUserId, Constantes.gTab_Tramites, odrTramite)
                        For i As Integer = 1 To mdsDatos.Tables.Count - 1
                            BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                        Next
                    End If
                    If pTipo = "A" Then
                        If CLng(odrTramite.Item(0)) <= 0 Then
                            lstrTramId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, odrTramite)
                            mdsDatos.Tables("Tramites").Rows(0).Item("tram_id") = lstrTramId
                        End If
                    End If

                Next
            End If

            lstrDenunciaId = 0
            'GUARDO TABLA DE DENUNCIAS
            Dim dtDenuncia As DataTable
            Dim dtSemenStock As DataTable
            Dim oDenuncia As New SRA_Neg.Denuncias(mstrConn, mstrUserId)
            Dim oSemen As New SRA_Neg.SemenStock(mstrConn, mstrUserId)
            Dim compCriaId As Integer
            Dim compClienteId As Integer
            Dim cantDosis As Integer




            'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
            For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                If CInt(odrDeta.Item(0)) <= 0 Then
                    'ALTA DETALLE
                    odrDeta.Item("trad_tram_id") = lstrTramId
                    odrDeta.Item("trad_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDeta)
                    odrDeta.AcceptChanges()

                    ''ACTUALIZO CON EL ID DETALLE NUEVO
                    If odrDeta.Item("trad_requ_id").ToString <> "" Then
                        For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaObse).Select("_trad_requ_id=" + odrDeta.Item("trad_requ_id").ToString)
                            odrRela.Item("trao_trad_id") = odrDeta.Item("trad_id")
                            odrRela.AcceptChanges()
                        Next
                    End If
                    odrDeta.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDeta)

                End If

            Next



            For Each odrObse As DataRow In mdsDatos.Tables(mstrTablaObse).Select()
                If CInt(odrObse.Item(0)) <= 0 Then
                    'ALTA OBSERVACION
                    odrObse.Item("trao_tram_id") = lstrTramId
                    odrObse.AcceptChanges()
                    odrObse.Item("trao_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaObse, odrObse)
                    odrObse.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaObse, odrObse)
                End If
            Next


            For Each odrDocu As DataRow In mdsDatos.Tables(mstrTablaDocu).Select()
                If CInt(odrDocu.Item(0)) <= 0 Then
                    'ALTA documentos
                    odrDocu.Item("trdo_tram_id") = lstrTramId
                    odrDocu.AcceptChanges()
                    odrDocu.Item("trdo_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocu, odrDocu)
                    odrDocu.AcceptChanges()
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDocu, odrDocu)

                End If

            Next

            If lstrProdId Is Nothing Then
                For Each odrProd As DataRow In mdsDatos.Tables(Constantes.gTab_Tramites).Select
                    lstrProdId = odrProd.Item("tram_embr_padre_id")
                    compCriaId = odrProd.Item("tram_comp_cria_id")
                    compClienteId = odrProd.Item("tram_comp_clie_id")

                    cantDosis = odrProd.Item("tram_dosi_cant")
                Next
            End If


            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName


                If mdsDatos.Tables(i).TableName.ToLower <> "tramites_productos" And _
                mdsDatos.Tables(i).TableName.ToLower <> "te_denun" And _
                mdsDatos.Tables(i).TableName.ToLower <> "semen_stock" And _
                 mdsDatos.Tables(i).TableName.ToLower <> "productos" Then


                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrTramId)

                End If
            Next


            For Each odrDenun As DataRow In mdsDatos.Tables(mstrTablaDenuncia).Select()
                If odrDenun.Item(0) Is System.DBNull.Value Then


                    dtSemenStock = oSemen.GetSemenStockByTramiteId(lstrTramId, "")

                    If dtSemenStock.Rows.Count <= 0 Then
                        ' recien puedo dar de alta  uns dennuncia cuando agrego en semen_stock
                        odrDenun.Item("tede_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)

                        lstrDenunciaId = odrDenun.Item("tede_id")
                    Else
                        ' la denuncia no sepuede  modificar 
                        'clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDenuncia, odrDenun)
                    End If
                End If
            Next



            For Each odrProd As DataRow In mdsDatos.Tables(Constantes.gTab_SemenStock).Select()
                If IsDBNull(odrProd.Item(0)) Then
                    odrProd.Item(0) = 0
                End If
                'con transaccionexec @sest_cria_id=null,@sest_clie_id=null,@sest_prdt_id=4696671,@sest_usopropio=null,@sest_fecha_tran=null,@sest_cant=0,@sest_cria_cant=0,@sest_recu_fecha=null,@sest_pres_fecha=null,@sest_auto=0,@sest_tipo_movi=2,@sest_tram_id=363940,@sest_audi_user=1

                If CInt(odrProd.Item("sest_id")) <= 0 Then

                    odrProd.Item("sest_tede_id") = lstrDenunciaId
                    odrProd.Item("sest_tram_id") = lstrTramId
                    odrProd.Item("sest_prdt_id") = lstrProdId

                    odrProd.Item("sest_cant") = cantDosis
                    odrProd.Item("sest_cria_cant") = 0
                    odrProd.Item("sest_auto") = 0   ' manual
                    odrProd.Item("sest_tipo_movi") = SRA_Neg.Constantes.TipoMovimientos.DenunciaRecuperacionSemen
                    odrprod.Item("sest_cria_id") = compCriaId
                    odrprod.Item("sest_clie_id") = compClienteId

                    odrProd.AcceptChanges()
                    lstrSemenId = clsSQLServer.gAlta(lTransac, mstrUserId, Constantes.gTab_SemenStock, odrProd)

                    Exit For


                End If
            Next

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrTramId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function

    Public Sub AplicarReglasTramite(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTramId As String, ByVal pintProce As Integer, _
                                    ByVal pTabla As String, ByVal pSexo As String, ByVal pRaza As String, _
                                    ByRef eError As ErrorEntity)
        Try
            Dim lstrId As String = ""
            Dim lstrRaza As String = ""
            Dim dsVali As DataSet
            Dim lngProdId As Long = 0
            Dim intTramProdId As Long = 0
            Dim dtEmbrionStock As DataTable
            Dim oTramite As New SRA_Neg.Tramites(lTransac, mstrUserId)
            'ConvertDsToXML(dsVali, "dsvali 1")

            If pintProce.ToString() = ReglasValida.Validaciones.Procesos.ImportacionProductos Or _
               pintProce.ToString() = ReglasValida.Validaciones.Procesos.ExportacionProductos Or _
               pintProce.ToString() = ReglasValida.Validaciones.Procesos.TransferenciaProductos Then

                lngProdId = oTramite.GetProductoIdByTramiteId(pstrTramId, "")
                intTramProdId = oTramite.GetTramProdIdByPK(pstrTramId, lngProdId)
            End If

            ReglasValida.Validaciones.gLimpiarErrores(lTransac, pTabla, pstrTramId)

            eError.errorDescripcion = ReglasValida.Validaciones.gValidarReglas(lTransac, pTabla, lngProdId, pRaza, pSexo, mstrUserId, True, _
                                                                               pintProce, pstrTramId, intTramProdId)

            If eError.errorDescripcion <> "" Then
                AccesoBD.clsError.gGenerarMensajes("tramites.AplicarReglas", "Error", eError.errorDescripcion)
            Else
                mstrCmd = "exec tramites_aprobar @tram_id = " & pstrTramId & _
                ", @audi_user=" & mstrUserId

                Try
                    clsSQLServer.gExecute(lTransac, mstrCmd)
                Catch ex As Exception
                    clsError.gManejarError("tramites.AplicarReglas", "ERROR", ex)
                    eError.errorDescripcion = ex.Message.ToString()
                End Try
            End If

        Catch ex As Exception
            clsError.gManejarError("tramites.AplicarReglas", "ERROR", ex)
            eError.errorDescripcion = ex.Message.ToString()
        End Try
    End Sub

    Public Sub AplicarReglasTramite(ByVal pstrTramId As String, ByVal pintProce As Integer, _
        ByVal pTabla As String, ByVal pSexo As String, ByVal pRaza As String, _
        ByRef eError As ErrorEntity)
        Try
            Dim lstrId As String = ""
            Dim lstrRaza As String = ""
            Dim dsVali As DataSet
            Dim lTransac As SqlClient.SqlTransaction
            Dim lngProdId As Long = 0
            Dim intTramProdId As Long = 0
            Dim dtEmbrionStock As DataTable
            Dim oTramite As New SRA_Neg.Tramites(mstrConn, mstrUserId)

            'ConvertDsToXML(dsVali, "dsvali 1")

            If pintProce.ToString() = ReglasValida.Validaciones.Procesos.ImportacionProductos Or _
               pintProce.ToString() = ReglasValida.Validaciones.Procesos.ExportacionProductos Or _
               pintProce.ToString() = ReglasValida.Validaciones.Procesos.TransferenciaProductos Then

                lngProdId = oTramite.GetProductoIdByTramiteId(pstrTramId, "")
                intTramProdId = oTramite.GetTramProdIdByPK(pstrTramId, lngProdId)
            End If

            ReglasValida.Validaciones.gLimpiarErrores(mstrConn, pTabla, pstrTramId)

            eError.errorDescripcion = ReglasValida.Validaciones.gValidarReglas(mstrConn, pTabla, lngProdId, pRaza, pSexo, mstrUserId, True, pintProce, pstrTramId, intTramProdId)

            If eError.errorDescripcion <> "" Then
                AccesoBD.clsError.gGenerarMensajes("tramites.AplicarReglas", "Error", eError.errorDescripcion)
            Else
                mstrCmd = "exec tramites_aprobar @tram_id = " & pstrTramId & ", @audi_user=" & mstrUserId

                Try
                    lTransac = clsSQLServer.gObtenerTransac(mstrConn)
                    clsSQLServer.gExecute(lTransac, mstrCmd)
                    clsSQLServer.gCommitTransac(lTransac)
                    clsSQLServer.gCloseTransac(lTransac)
                Catch ex As Exception
                    clsSQLServer.gRollbackTransac(lTransac)
                    clsError.gManejarError("tramites.AplicarReglas", "ERROR", ex)
                    eError.errorDescripcion = ex.Message.ToString()
                End Try
            End If

        Catch ex As Exception
            clsError.gManejarError("tramites.AplicarReglas", "ERROR", ex)
            eError.errorDescripcion = ex.Message.ToString()
        End Try
    End Sub

    Public Sub AplicarReglasTramite(ByVal pstrTramId As String, ByVal pintProce As Integer, _
          ByVal pTabla As String, ByVal pSexo As String, ByVal pRaza As String, _
          ByVal pFechaInicioRegla As Date, ByVal pFechaFinRegla As Date, _
          ByRef eError As ErrorEntity)
        Try
            Dim lstrId As String = ""
            Dim lstrRaza As String = ""
            Dim dsVali As DataSet
            Dim lTransac As SqlClient.SqlTransaction


            dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, pintProce.ToString(), "W")

            ConvertDsToXML(dsVali, "dsvali 2")

            lstrId = pstrTramId

            ReglasValida.Validaciones.gLimpiarErrores(mstrConn, pTabla, lstrId)

            eError.errorDescripcion = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, _
            pTabla, lstrId, lstrRaza, _
            pSexo, mstrUserId, True, pintProce, pstrTramId, 0)


            If eError.errorDescripcion <> "" Then
                AccesoBD.clsError.gGenerarMensajes("tramites.AplicarReglas", "Error", eError.errorDescripcion)
            Else
                Try
                    lTransac = clsSQLServer.gObtenerTransac(mstrConn)
                    clsSQLServer.gExecute(lTransac, mstrCmd)
                    clsSQLServer.gCommitTransac(lTransac)
                    clsSQLServer.gCloseTransac(lTransac)


                Catch ex As Exception
                    clsSQLServer.gRollbackTransac(lTransac)

                End Try

            End If

        Catch ex As Exception
            clsError.gManejarError("tramites.AplicarReglas", "ERROR", ex)
            eError.errorDescripcion = ex.Message.ToString()
            ReglasValida.Validaciones.mGrabarError(mstrConn, pTabla, pstrTramId, eError.errorId, mstrUserId)
        End Try
    End Sub

    Public Sub AplicarReglasTramite(ByVal pstrTramId As String, ByVal pintProce As Integer, _
            ByVal pTabla As String, ByVal pSexo As String, ByVal pRaza As String, _
            ByVal pFechaInicioRegla As Date, ByVal pFechaFinRegla As Date, _
            ByRef eError As ErrorEntity, ByVal pObject As Object)
        Try
            Dim lstrId As String = ""
            Dim lstrRaza As String = ""
            Dim dsVali As DataSet
            Dim lTransac As SqlClient.SqlTransaction


            dsVali = ReglasValida.Validaciones.gDeterminarReglas(mstrConn, pintProce.ToString(), "W")

            ConvertDsToXML(dsVali, "dsvali 3")

            lstrId = pstrTramId

            ReglasValida.Validaciones.gLimpiarErrores(mstrConn, pTabla, lstrId)

            eError.errorDescripcion = ReglasValida.Validaciones.gValidarReglas(mstrConn, dsVali, _
            pTabla, lstrId, lstrRaza, _
            pSexo, mstrUserId, True, pintProce, pstrTramId, 0)


            If eError.errorDescripcion <> "" Then
                AccesoBD.clsError.gGenerarMensajes("tramites.AplicarReglas", "Error", eError.errorDescripcion)
            Else
                Try
                    lTransac = clsSQLServer.gObtenerTransac(mstrConn)
                    clsSQLServer.gExecute(lTransac, mstrCmd)
                    clsSQLServer.gCommitTransac(lTransac)
                    clsSQLServer.gCloseTransac(lTransac)
                Catch ex As Exception
                    clsSQLServer.gRollbackTransac(lTransac)
                End Try
            End If

        Catch ex As Exception
            clsError.gManejarError("tramites.AplicarReglas", "ERROR", ex)
            eError.errorDescripcion = ex.Message.ToString()
            ReglasValida.Validaciones.mGrabarError(mstrConn, pTabla, pstrTramId, eError.errorId, mstrUserId)
        End Try
    End Sub

    Private Overloads Sub BorrarDetas(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTablaDet As String, Optional ByVal pdsDatos As DataSet = Nothing)
        Dim ldsDatos As DataSet = pdsDatos
        If ldsDatos Is Nothing Then
            ldsDatos = mdsDatos
        End If

        Dim lstrColumnaPath As String = ""
        For Each odrDeta As DataRow In ldsDatos.Tables(pstrTablaDet).Select("", "", DataViewRowState.Deleted)
            With odrDeta
                .RejectChanges()
                If Not .IsNull(0) Then
                    'BORRA ARCHIVOS ADJUNTOS
                    mBorrarArchivo(lTransac, odrDeta, pstrTablaDet)
                    'BORRA EL REGISTRO DEL DETALLE
                    Select Case pstrTablaDet.ToLower
                        Case "tramites_personas_compradores", "tramites_personas_vendedores"
                            mstrCmd = "exec tramites_personas_baja " & .Item(0).ToString & "," & mstrUserId
                        Case Else
                            mstrCmd = "exec " & pstrTablaDet & "_baja " & .Item(0).ToString & "," & mstrUserId
                    End Select
                    clsSQLServer.gExecute(lTransac, mstrCmd)
                End If
                odrDeta.Table.Rows.Remove(odrDeta)
            End With
        Next
    End Sub

    Public Function GetPadreByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable

        'embriones
        Dim dt As New DataTable
        dt.TableName = pTableName

        mstrCmd = "exec GetPadreByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        Return dt

    End Function

    Public Function GetMadreByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        'embriones
        Dim dt As New DataTable
        dt.TableName = pTableName
        mstrCmd = "exec GetMadreByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If
        Return dt

    End Function

    Public Function GetProdAnimalPiePadreByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName

        mstrCmd = "exec GetProdAnimalPiePadreByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        Return dt

    End Function

    Public Function GetProdAnimalPieMadreByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName
        mstrCmd = "exec GetProdAnimalPieMadreByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        Return dt

    End Function

    Public Function GetSemenStockByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName

        mstrCmd = "exec GetSemenStockByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        Return dt

    End Function

    Public Function GetProductoByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetProductoByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        Return dt

    End Function

    Public Function GetFechaTransByTramiteId(ByVal TramiteId As String) As String
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim pTableName As String
        Dim vReturn As String = ""

        pTableName = "tablaProd"

        mstrCmd = "exec GetProductoByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        dt = GetProductoByTramiteId(TramiteId, pTableName)

        If dt.Rows.Count > 0 Then
            If Not dt.Rows(0).Item("prdt_tran_fecha") Is DBNull.Value Then
                vReturn = dt.Rows(0).Item("prdt_tran_fecha").ToString()
            End If

        End If
        Return vReturn

    End Function

    Public Function GetProductoIdByTramiteId(ByVal pTramiteId As String, ByVal pTableName As String) As Integer
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim intProductoId As Long = 0
        dt.TableName = pTableName

        dt = GetProductoByTramiteId(pTramiteId, pTableName)
       
        If dt.Rows.Count > 0 Then
            intProductoId = ValidarNulos(dt.Rows(0).Item("prdt_id"), True)
        End If

        Return intProductoId

    End Function

    Public Function GetTramProdIdByPK(ByVal TramiteId As String, ByVal ProductoId As String) As Integer
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim intTramProdId As Integer = 0

        mstrCmd = "exec GetTramProdIdByPK "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @prdt_id = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        If dt.Rows.Count() > 0 Then
            intTramProdId = dt.Rows(0).Item("trpr_id")
        End If

        Return intTramProdId

    End Function

    Public Function GetTramiteById(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetTramiteById "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        Return dt

    End Function

    Public Function GetTramiteByDenunTEId(ByVal strDenunTEId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName

        Dim mstrCmd As String

        mstrCmd = "exec GetTramiteByDenunTEId "
        mstrCmd = mstrCmd + " @tede_id = " + clsSQLServer.gFormatArg(strDenunTEId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        Return dt

    End Function


    Public Function GetProductosAnimalPieByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetProductosByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        Return dt

    End Function

    Public Function Tramites_updateByTramiteId(ByVal TramiteId As String, ByVal NumeroTramite As String) As Boolean
        Dim mstrCmd As String
        Dim rows As Int16

        mstrCmd = "exec Tramites_updNumeroTramiteByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @tram_nume = " + clsSQLServer.gFormatArg(NumeroTramite, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            rows = clsSQLServer.gExecute(Me.mstTransac, mstrCmd)
        Else
            rows = clsSQLServer.gExecute(mstrConn, mstrCmd)
        End If

        Return rows
    End Function

    Public Function GetCantFacturadoByNroControlRazaCriador(ByVal pNroControl As String, _
    ByVal pRaza As String, ByVal pCriador As String) As DataRow

        Dim mstrCmd As String
        Dim dtCantFact As New DataTable
        Dim drFact As DataRow

        mstrCmd = "exec GetFacturacionByNroControl "
        mstrCmd = mstrCmd + " @pNroControl = " + clsSQLServer.gFormatArg(pNroControl, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @pRaza = " + clsSQLServer.gFormatArg(pRaza, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @pCriador = " + clsSQLServer.gFormatArg(pCriador, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dtCantFact = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dtCantFact = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        If dtCantFact.Rows.Count > 0 Then
            drFact = dtCantFact.Rows(0)
        Else
            mstrCmd = "exec GetCantProformaByNroControl "
            mstrCmd = mstrCmd + " @pNroControl = " + clsSQLServer.gFormatArg(pNroControl, SqlDbType.VarChar)
            mstrCmd = mstrCmd + " ,@pRaza = " + clsSQLServer.gFormatArg(pRaza, SqlDbType.VarChar)
            mstrCmd = mstrCmd + " ,@pCriador = " + clsSQLServer.gFormatArg(pCriador, SqlDbType.VarChar)

            ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
            If (mstrConn Is Nothing Or mstrConn = "") Then
                dtCantFact = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
            Else
                dtCantFact = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
            End If

            If dtCantFact.Rows.Count > 0 Then
                drFact = dtCantFact.Rows(0)
            End If

        End If

        Return drFact

    End Function

    Public Function mObtenerTramite(ByVal pstrId) As String
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim lstrId As String = ""

        mstrCmd = "exec tramites_consul @tram_id =" & clsSQLServer.gFormatArg(pstrId, SqlDbType.VarChar)

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            dt = clsSQLServer.gExecuteQuery(Me.mstTransac, mstrCmd).Tables(0)
        Else
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If

        If dt.Rows.Count > 0 Then
            lstrId = Convert.ToInt32(Utiles.mValidarNulos(dt.Rows(0).Item("tram_nume"), False))
        End If

        Return (lstrId)
    End Function

    Public Function ValidarPaisesTransferencia(ByVal pClienteVendedor As Int32, _
    ByVal pClienteComprador As Int32) As Boolean

        Dim mstrCmd As String
        Dim mstrConntmp As String
        Dim intClientePaisComprador As Int16 = 0
        Dim intClientePaisVendedor As Int16 = 0

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            mstrConntmp = Me.mstTransac.Connection.ConnectionString
        Else
            mstrConntmp = mstrConn
        End If

        Dim oCliente As New SRA_Neg.Clientes(mstrConntmp, mstrUserId)

        intClientePaisVendedor = oCliente.GetPaisByClienteId(pClienteVendedor)
        intClientePaisComprador = oCliente.GetPaisByClienteId(pClienteComprador)

        If intClientePaisVendedor = 350 And intClientePaisComprador = 350 Then
            Return True

        End If
        Return False

    End Function


    Public Function ValidarPaisesTransferenciaByRazaCriador(ByVal pRazaIdVendedor As Integer, _
       ByVal pCriaIdVendedor As Integer, _
       ByVal pRazaIdComprador As Integer, _
       ByVal pCriaIdComprador As Integer) As Boolean

        Dim mstrCmd As String
        Dim mstrConntmp As String
        Dim intClientePaisComprador As Integer = 0
        Dim intClientePaisVendedor As Integer = 0
        Dim intClienteComprador As Integer = 0
        Dim intClienteVendedor As Integer = 0

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            mstrConntmp = Me.mstTransac.Connection.ConnectionString
        Else
            mstrConntmp = mstrConn
        End If
        Dim oCliente As New SRA_Neg.Clientes(mstrConntmp, mstrUserId)
        intClienteVendedor = oCliente.GetClienteIdByRazaCriador(pRazaIdVendedor, pCriaIdVendedor)
        intClienteComprador = oCliente.GetClienteIdByRazaCriador(pRazaIdComprador, pCriaIdComprador)


        intClientePaisVendedor = oCliente.GetPaisByClienteId(intClienteVendedor)
        intClientePaisComprador = oCliente.GetPaisByClienteId(intClienteComprador)

        If intClientePaisVendedor = 350 And intClientePaisComprador = 350 Then
            Return True

        End If
        Return False

    End Function

    Public Function ValidarPaisesExportacion(ByVal pClienteVendedor As Integer, _
    ByVal pClienteComprador As Integer) As Boolean
        Dim mstrCmd As String
        Dim mstrConntmp As String
        Dim intClientePaisComprador As Integer = 0
        Dim intClientePaisVendedor As Integer = 0

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            mstrConntmp = Me.mstTransac.Connection.ConnectionString
        Else
            mstrConntmp = mstrConn
        End If
        Dim oCliente As New SRA_Neg.Clientes(mstrConntmp, mstrUserId)

        intClientePaisVendedor = oCliente.GetPaisByClienteId(pClienteVendedor)
        intClientePaisComprador = oCliente.GetPaisByClienteId(pClienteComprador)

        If intClientePaisVendedor = 350 And intClientePaisComprador <> 350 Then
            Return True

        End If
        Return False

    End Function


    Public Function ValidarPaisesExportacionByRazaCriador( _
        ByVal pRazaIdVendedor As Integer, ByVal pCriaIdVendedor As Integer, _
        ByVal pRazaIdComprador As Integer, ByVal pCriaIdComprador As Integer) As Boolean
        Dim mstrCmd As String
        Dim mstrConntmp As String
        Dim intClientePaisComprador As Integer = 0
        Dim intClientePaisVendedor As Integer = 0
        Dim intClienteComprador As Integer = 0
        Dim intClienteVendedor As Integer = 0

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            mstrConntmp = Me.mstTransac.Connection.ConnectionString
        Else
            mstrConntmp = mstrConn
        End If
        Dim oCliente As New SRA_Neg.Clientes(mstrConntmp, mstrUserId)

        intClienteVendedor = oCliente.GetClienteIdByRazaCriador(pRazaIdVendedor, pCriaIdVendedor)
        intClienteComprador = oCliente.GetClienteIdByRazaCriador(pRazaIdComprador, pCriaIdComprador)


        intClientePaisVendedor = oCliente.GetPaisByClienteId(intClienteVendedor)
        intClientePaisComprador = oCliente.GetPaisByClienteId(intClienteComprador)

        If intClientePaisVendedor = 350 And intClientePaisComprador <> 350 Then
            Return True

        End If
        Return False
    End Function

    Public Function ValidarPaisesImportacion(ByVal pClienteVendedor As Int32, ByVal pClienteComprador As Int32) As Boolean
        Dim mstrCmd As String
        Dim mstrConntmp As String
        Dim intClientePaisComprador As Int16 = 0
        Dim intClientePaisVendedor As Int16 = 0

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            mstrConntmp = Me.mstTransac.Connection.ConnectionString
        Else
            mstrConntmp = mstrConn
        End If
        Dim oCliente As New SRA_Neg.Clientes(mstrConntmp, mstrUserId)

        intClientePaisVendedor = oCliente.GetPaisByClienteId(pClienteVendedor)
        intClientePaisComprador = oCliente.GetPaisByClienteId(pClienteComprador)

        If intClientePaisVendedor <> 350 And intClientePaisComprador = 350 Then
            Return True

        End If
        Return False

    End Function

    Public Function ValidarPaisesImportacionByRazaCriador(ByVal pRazaIdVendedor As Integer, ByVal pCriaIdVendedor As Integer, _
        ByVal pRazaIdComprador As Integer, ByVal pCriaIdComprador As Integer) As Boolean
        Dim mstrConntmp As String
        Dim intClientePaisComprador As Integer = 0
        Dim intClientePaisVendedor As Integer = 0
        Dim intClienteComprador As Integer = 0
        Dim intClienteVendedor As Integer = 0

        ' Dario 2014-06-12 controlo que tenga transaccion o solo coneccion
        If (mstrConn Is Nothing Or mstrConn = "") Then
            mstrConntmp = Me.mstTransac.Connection.ConnectionString
        Else
            mstrConntmp = mstrConn
        End If
        Dim oCliente As New SRA_Neg.Clientes(mstrConntmp, mstrUserId)
        intClienteVendedor = oCliente.GetClienteIdByRazaCriador(pRazaIdVendedor, pCriaIdVendedor)
        intClienteComprador = oCliente.GetClienteIdByRazaCriador(pRazaIdComprador, pCriaIdComprador)

        intClientePaisVendedor = oCliente.GetPaisByClienteId(intClienteVendedor)
        intClientePaisComprador = oCliente.GetPaisByClienteId(intClienteComprador)

        If intClientePaisVendedor <> 350 And intClientePaisComprador = 350 Then
            Return True

        End If
        Return False

    End Function
End Class