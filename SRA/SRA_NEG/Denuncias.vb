Imports System.Data.SqlClient

Public Class Denuncias
   Inherits Generica

   Protected mstrTablaDeta As String
   Protected mstrTablaPadres As String
   Protected server As System.Web.HttpServerUtility

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaDeta As String, ByVal pstrTablaPadres As String, ByVal pdsDatos As DataSet, ByVal server As System.Web.HttpServerUtility)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaDeta = pstrTablaDeta
      Me.mstrTablaPadres = pstrTablaPadres
      Me.mdsDatos = pdsDatos
      Me.server = server
   End Sub

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaDeta As String, ByVal pstrTablaPadres As String)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaDeta = pstrTablaDeta
      Me.mstrTablaPadres = pstrTablaPadres
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub

    Public Function GetDenunciaSemenByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetDenunciaSemenByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function
    Public Function GetDenunciaEmbrionesByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String

        dt.TableName = pTableName



        mstrCmd = "exec GetDenunciaEmbrionesByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function
    Public Function GetDenunciaBySemenId(ByVal SemenId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetDenunciaBySemenId "
        mstrCmd = mstrCmd + " @sest_id = " + clsSQLServer.gFormatArg(SemenId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetDenuBajaDetalleByProdId(ByVal ProductoId As String) As String
        'GSZ 28/04/2015 SE agrego para que muestre el motivo de baja
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim strObservacion As String = ""
        dt.TableName = "tblDenuncia"

        mstrCmd = "exec GetDenuBajaDetalleByProdId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            strObservacion = dt.Rows(0).Item("bjdd_observacion")
        End If
        Return strObservacion

    End Function


    Public Overloads Function Alta() As String
        Return (mActualizar())
    End Function

    Public Overloads Sub Modi()
        mActualizar()
    End Sub

    Private Function mActualizar() As String
        Dim lstrDenuId As String
        Dim lstrDenuDetaId As String
        Dim lstrDenuPadre As String
        Dim lTransac As SqlClient.SqlTransaction

        Try

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'Datos de la denuncia.
            If CInt(MiRow.Item("sede_id")) <= 0 Then
                'ALTA TABLA
                lstrDenuId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrDenuId = MiRow.Item("sede_id").ToString
            End If

            'Eliminar padres dados de baja.
            For Each odrPadre As DataRow In mdsDatos.Tables(mstrTablaPadres).Select("", "", DataViewRowState.Deleted)
                With odrPadre
                    .RejectChanges()
                    If Not .IsNull(0) Then
                        clsSQLServer.gBaja(lTransac, mstrUserId, mstrTablaPadres, .Item(0).ToString)
                    End If
                    odrPadre.Table.Rows.Remove(odrPadre)
                End With
            Next

            'Servicios de la denuncia.
            For Each odrDetaDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()
                lstrDenuPadre = odrDetaDeta.Item("sdde_id")
                If lstrDenuPadre < 0 Then
                    odrDetaDeta.Item("sdde_sede_id") = lstrDenuId
                    'ALTA 
                    lstrDenuDetaId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDetaDeta)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDetaDeta)
                    lstrDenuDetaId = ""
                End If

                '--- Padres Siembra Multiple ---
                'Alta/Modificacion de padres asociados.
                For Each odrPadre As DataRow In mdsDatos.Tables(mstrTablaPadres).Select("sdpa_sdde_id=" & lstrDenuPadre)
                    If lstrDenuDetaId <> "" Then
                        odrPadre.Item("sdpa_sdde_id") = lstrDenuDetaId
                    End If
                    If odrPadre.Item("sdpa_id") < 0 Then
                        'ALTA 
                        clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaPadres, odrPadre)
                    Else
                        'MODIFICACION 
                        clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaPadres, odrPadre)
                    End If
                Next
            Next

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrDenuId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

End Class
