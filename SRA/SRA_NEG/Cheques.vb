Imports WSAFIPFacElect ' Dario 2015-06-24
Public Class Cheques
    Inherits GenericaRel

    Private mstrTablaCheques As String = SRA_Neg.Constantes.gTab_Cheques
    Private mstrTablaChequesMovim As String = SRA_Neg.Constantes.gTab_ChequesMovim
    Private mstrTablaChequesProceso As String = "pagos_pasaje_proceso"

    Private mstrTablaComp As String = SRA_Neg.Constantes.gTab_Comprobantes
    Private mstrTablaDeta As String = SRA_Neg.Constantes.gTab_ComprobDeta
    Private mstrTablaConcep As String = SRA_Neg.Constantes.gTab_ComprobConcep
    Private mstrTablaCuentasCtes As String = SRA_Neg.Constantes.gTab_CuentasCtes
    Private mstrTablaPagos As String = SRA_Neg.Constantes.gTab_ComprobPagos
    Private mstrTablaSaldosACuenta As String = SRA_Neg.Constantes.gTab_SaldosACuenta
    Private mstrTablaComprobACuenta As String = SRA_Neg.Constantes.gTab_ComprobACuenta
    Private mstrTablaBilletes As String = SRA_Neg.Constantes.gTab_Billetes
    Private mstrTablaVtos As String = SRA_Neg.Constantes.gTab_ComprobVtos
    Private mstrTablaND As String = "nota_debito"
    Private mstrTablaAsientoRechazo As String = "rechazo_cheques_asiento"
    Private mstrTablaAsientoRechazoBaja As String = "rechazo_cheques_asiento_baja"
    Private mstrTablaAsientoRechazoPagos As String = "rechazo_pagos_asiento"
    Private mstrTablaAsientoRechazoPagosBaja As String = "rechazo_pagos_asiento_baja"


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet)
        MyBase.New(pstrConn, pstrUserId, pstrTabla, pdsDatos)
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String)
        MyBase.New(pstrConn, pstrUserId, pstrTabla)
    End Sub

    Public Sub PasarChequesPost()
        Dim lstrPacoIds As String
        Dim lTransac As SqlClient.SqlTransaction

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'modifico los cheques
            For Each ldrCheq As DataRow In mdsDatos.Tables(mstrTablaCheques).Select()
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaCheques, ldrCheq)

                'If lstrPacoIds <> "" Then lstrPacoIds += ","
                'lstrPacoIds += ldrCheq.Item("_paco_id").ToString

                With mdsDatos.Tables(mstrTablaChequesProceso).Rows(0)
                    .Item("proc_paco_ids") = ldrCheq.Item("_paco_id").ToString
                    .Item("proc_modu_id") = Constantes.Modulos.ChequesPostdatados
                End With
                clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaChequesProceso, mdsDatos.Tables(mstrTablaChequesProceso).Rows(0))

            Next

            'With mdsDatos.Tables(mstrTablaChequesProceso).Rows(0)
            '    .Item("proc_paco_ids") = lstrPacoIds
            '    .Item("proc_modu_id") = Constantes.Modulos.ChequesPostdatados
            'End With

            'ejecuto el proceso de pasaje
            'clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaChequesProceso, mdsDatos.Tables(mstrTablaChequesProceso).Rows(0))

            clsSQLServer.gCommitTransac(lTransac)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Sub

    Public Function RechazarCheques(ByVal pbooEsAlta As Boolean) As String
        Dim lstrCompId As String = ""
        Dim lstrND As String
        Dim lTransac As SqlClient.SqlTransaction

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If Not pbooEsAlta Then
                'anula la ND
                With mdsDatos.Tables(mstrTablaCheques).Rows(0)
                    If .IsNull("cheq_nd_comp_id") Then
                        lstrND = ""
                    Else
                        lstrND = .Item("cheq_nd_comp_id")
                    End If
                End With
            End If

            If mdsDatos.Tables(mstrTablaComp).Rows.Count > 0 Then 'por si era un cheque de recibos varios o es un des-rechazo
                lstrCompId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaComp, mdsDatos.Tables(mstrTablaComp).Rows(0))
                If Not pbooEsAlta Then
                    mdsDatos.Tables(mstrTablaDeta).Rows(0).Item("code_asoc_comp_id") = lstrND
                End If
                ActualizarDetas(lTransac, mstrTablaDeta, "code_comp_id", lstrCompId)
                ActualizarDetas(lTransac, mstrTablaConcep, "coco_comp_id", lstrCompId)
                ActualizarDetas(lTransac, mstrTablaVtos, "covt_comp_id", lstrCompId)

                mdsDatos.Tables(IIf(pbooEsAlta, mstrTablaAsientoRechazo, mstrTablaAsientoRechazoBaja)).Rows(0).item("proc_comp_id") = lstrCompId
            End If

            If pbooEsAlta Then
                'graba el enganche Cheque/ND
                With mdsDatos.Tables(mstrTablaCheques).Rows(0)
                    .Item("cheq_nd_comp_id") = IIf(lstrCompId <> "", lstrCompId, DBNull.Value)
                End With
            Else
                If lstrND <> "" Then
                    ActualizarDetas(lTransac, mstrTablaND, "comp_id", lstrND)
                End If
            End If

            clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaCheques, mdsDatos.Tables(mstrTablaCheques).Rows(0))
            clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaChequesMovim, mdsDatos.Tables(mstrTablaChequesMovim).Rows(0))
            clsSQLServer.gAlta(lTransac, mstrUserId, IIf(pbooEsAlta, mstrTablaAsientoRechazo, mstrTablaAsientoRechazoBaja), mdsDatos.Tables(IIf(pbooEsAlta, mstrTablaAsientoRechazo, mstrTablaAsientoRechazoBaja)).Rows(0))

            If pbooEsAlta Then
                If (mdsDatos.Tables(mstrTablaComp).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.ND _
                                Or mdsDatos.Tables(mstrTablaComp).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.NC) Then
                    Dim respuesta As String
                    Dim objWsAFIP As WSFactElet
                    ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                    respuesta = objWsAFIP.GetCAE(String.Empty, lstrCompId)
                    '                 0                1                2              3              4            5
                    ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                    '                  6                      7                8 
                    '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;

                    Dim respArray As Array = respuesta.Split("|"c)
                    ' si retorna ok 
                    If (respArray(0) = "A") Then
                        ' controlo que el comp id enviado es igual al recibido
                        Dim compIdresp As String = respArray(2)

                        Dim comp_cemi_nume As String = ""
                        Dim opcional1 As String = ""
                        Dim opcional2 As String = ""

                        Try
                            comp_cemi_nume = respArray(6)
                        Catch ex As Exception

                        End Try

                        Try
                            opcional1 = respArray(7)
                        Catch ex As Exception

                        End Try

                        Try
                            opcional2 = respArray(8)
                        Catch ex As Exception

                        End Try

                        If (comp_cemi_nume.Trim().Length = 0) Then
                            comp_cemi_nume = "null"
                        End If

                        If (opcional1.Trim().Length = 0) Then
                            opcional1 = "null"
                        End If

                        If (opcional2.Trim().Length = 0) Then
                            opcional2 = "null"
                        End If


                        If (compIdresp.Trim() = lstrCompId.Trim()) Then
                            ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                            Dim compNumeresp As String = respArray(3)
                            Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrCompId & " ,@comp_nume=" & compNumeresp _
                                               & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                            ' hago el update del @comp_nume de la tabla de comprobantes
                            clsError.gLoggear("GetCAE 1:: " + proc)
                            Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                            ' si no realizo el update error
                            If (intUpdateRest = 0) Then
                                Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                            End If
                        Else
                            ' si los com id son diferentes error
                            Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrCompId & " * " & respuesta)
                        End If
                    Else
                        ' cualquier cosa fuera de ok error 
                        Throw New Exception(respuesta)
                    End If
                End If
            End If

            clsSQLServer.gCommitTransac(lTransac)
            Return (lstrCompId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Function RechazarCupones(ByVal pbooEsAlta As Boolean) As String
        Dim lstrCompId As String
        Dim lstrND As String = ""
        Dim lTransac As SqlClient.SqlTransaction

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If Not pbooEsAlta Then
                'anula la ND
                With mdsDatos.Tables(mstrTablaPagos).Rows(0)
                    If .IsNull("paco_nd_comp_id") Then
                        lstrND = ""
                    Else
                        lstrND = .Item("paco_nd_comp_id")
                    End If
                End With
            End If

            If mdsDatos.Tables(mstrTablaComp).Rows.Count > 0 Then 'por si es un des-rechazo
                lstrCompId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaComp, mdsDatos.Tables(mstrTablaComp).Rows(0))
                If Not pbooEsAlta Then
                    mdsDatos.Tables(mstrTablaDeta).Rows(0).Item("code_asoc_comp_id") = lstrND
                End If
                ActualizarDetas(lTransac, mstrTablaDeta, "code_comp_id", lstrCompId)
                ActualizarDetas(lTransac, mstrTablaConcep, "coco_comp_id", lstrCompId)
                ActualizarDetas(lTransac, mstrTablaVtos, "covt_comp_id", lstrCompId)

                mdsDatos.Tables(IIf(pbooEsAlta, mstrTablaAsientoRechazoPagos, mstrTablaAsientoRechazoPagosBaja)).Rows(0).item("proc_comp_id") = lstrCompId
            End If

            If pbooEsAlta Then
                'graba el enganche Cupon/ND
                With mdsDatos.Tables(mstrTablaPagos).Rows(0)
                    .Item("paco_nd_comp_id") = IIf(lstrCompId <> "", lstrCompId, DBNull.Value)
                End With
            Else
                If lstrND <> "" Then
                    ActualizarDetas(lTransac, mstrTablaND, "comp_id", lstrND)
                End If
            End If

            clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaPagos, mdsDatos.Tables(mstrTablaPagos).Rows(0))
            clsSQLServer.gAlta(lTransac, mstrUserId, IIf(pbooEsAlta, mstrTablaAsientoRechazoPagos, mstrTablaAsientoRechazoPagosBaja), mdsDatos.Tables(IIf(pbooEsAlta, mstrTablaAsientoRechazoPagos, mstrTablaAsientoRechazoPagosBaja)).Rows(0))

            If Not (mdsDatos.Tables(mstrTablaPagos + "_rechazo") Is Nothing) Then 'si es acreditación bancaria
                With mdsDatos.Tables(mstrTablaPagos + "_rechazo")
                    clsSQLServer.gAlta(lTransac, mstrUserId, .TableName, .Rows(0))
                End With
            End If

            If pbooEsAlta Then
                If (mdsDatos.Tables(mstrTablaComp).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.ND _
                    Or mdsDatos.Tables(mstrTablaComp).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.NC) Then
                    Dim respuesta As String
                    Dim objWsAFIP As WSFactElet
                    ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                    respuesta = objWsAFIP.GetCAE(String.Empty, lstrCompId)
                    '                 0                1                2              3              4            5
                    ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                    '                  6                      7                8 
                    '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                    Dim respArray As Array = respuesta.Split("|"c)
                    ' si retorna ok 
                    If (respArray(0) = "A") Then
                        ' controlo que el comp id enviado es igual al recibido
                        Dim compIdresp As String = respArray(2)

                        Dim comp_cemi_nume As String = ""
                        Dim opcional1 As String = ""
                        Dim opcional2 As String = ""

                        Try
                            comp_cemi_nume = respArray(6)
                        Catch ex As Exception

                        End Try

                        Try
                            opcional1 = respArray(7)
                        Catch ex As Exception

                        End Try

                        Try
                            opcional2 = respArray(8)
                        Catch ex As Exception

                        End Try

                        If (comp_cemi_nume.Trim().Length = 0) Then
                            comp_cemi_nume = "null"
                        End If

                        If (opcional1.Trim().Length = 0) Then
                            opcional1 = "null"
                        End If

                        If (opcional2.Trim().Length = 0) Then
                            opcional2 = "null"
                        End If

                        If (compIdresp.Trim() = lstrCompId.Trim()) Then
                            ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                            Dim compNumeresp As String = respArray(3)
                            'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrCompId & " ,@comp_nume=" & compNumeresp
                            Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrCompId & " ,@comp_nume=" & compNumeresp _
                                               & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                            ' hago el update del @comp_nume de la tabla de comprobantes
                            clsError.gLoggear("GetCAE 2:: " + proc)
                            Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                            ' si no realizo el update error
                            If (intUpdateRest = 0) Then
                                Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                            End If
                        Else
                            ' si los com id son diferentes error
                            Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrCompId & " * " & respuesta)
                        End If
                    Else
                        ' cualquier cosa fuera de ok error 
                        Throw New Exception(respuesta)
                    End If
                End If
            End If

            clsSQLServer.gCommitTransac(lTransac)
            Return (lstrCompId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function
End Class