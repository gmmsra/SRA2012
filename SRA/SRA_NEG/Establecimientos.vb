Imports System.Data.SqlClient

Public Class Establecimientos
   Inherits Generica

   Protected mstrTablaTele As String
   Protected mstrTablaDire As String
   Protected mstrTablaMail As String
   Protected mstrTablaRaza As String
   Protected mstrTablaCria As String 
   Protected server As System.Web.HttpServerUtility

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaTele As String, ByVal pstrTablaDire As String, ByVal pstrTablaMail As String, ByVal pstrTablaRaza As String, ByVal pstrTablaCria As String, ByVal pdsDatos As DataSet, ByVal server As System.Web.HttpServerUtility)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaTele = pstrTablaTele
      Me.mstrTablaDire = pstrTablaDire
      Me.mstrTablaMail = pstrTablaMail
      Me.mstrTablaRaza = pstrTablaRaza
      Me.mstrTablaCria = pstrTablaCria
      Me.mdsDatos = pdsDatos
      Me.server = server
   End Sub

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaTele As String, ByVal pstrTablaDire As String, ByVal pstrTablaMail As String, ByVal pstrTablaRaza As String, ByVal pstrTablaCria As String)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaTele = pstrTablaTele
      Me.mstrTablaDire = pstrTablaDire
      Me.mstrTablaMail = pstrTablaMail
      Me.mstrTablaRaza = pstrTablaRaza
      Me.mstrTablaCria = pstrTablaCria
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub


    Public Overloads Function Alta() As String
        Return (mActualizar())
    End Function

    Public Overloads Sub Modi()
        mActualizar()
    End Sub

    Private Function mActualizar() As String
        Dim lstrTABLAId As String
        Dim lstrEstaId As String
        Dim lstrClieId As String
        Dim lTransac As SqlClient.SqlTransaction

        Try

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'Datos del establecimiento.
            If CInt(MiRow.Item("esbl_id")) <= 0 Then
                'ALTA TABLA
                lstrEstaId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrEstaId = MiRow.Item("esbl_id").ToString
                'Eliminar los detalles anteriores.
                mBorrarDetas(lTransac, mstrTablaTele, lstrEstaId, "T")
                'mBorrarDetas(lTransac, mstrTablaDire, lstrEstaId, "D")
                mBorrarDetas(lTransac, mstrTablaMail, lstrEstaId, "M")
            End If

            lstrClieId = MiRow.Item("esbl_clie_id").ToString()

            'Tel�fonos del cliente para el establecimiento.
            For Each odrDetaTele As DataRow In mdsDatos.Tables(mstrTablaTele).Select()
                If odrDetaTele.Item("acti_esbl_id") Is DBNull.Value Then
                    odrDetaTele.Item("tecl_clie_id") = lstrClieId
                    odrDetaTele.Item("acti_esbl_id") = lstrEstaId
                    'ALTA 
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaTele, odrDetaTele)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaTele, odrDetaTele)
                End If
            Next

            'Direcciones del cliente para el establecimiento.
            For Each odrDetaDire As DataRow In mdsDatos.Tables(mstrTablaDire).Select()
                If odrDetaDire.Item("acti_esbl_id") Is DBNull.Value Then
                    odrDetaDire.Item("dicl_clie_id") = lstrClieId
                    odrDetaDire.Item("acti_esbl_id") = lstrEstaId
                    'ALTA 
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDire, odrDetaDire)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDire, odrDetaDire)
                End If
            Next

            'Mails del cliente para el establecimiento.
            For Each odrDetaMail As DataRow In mdsDatos.Tables(mstrTablaMail).Select()
                If odrDetaMail.Item("acti_esbl_id") Is DBNull.Value Then
                    odrDetaMail.Item("macl_clie_id") = lstrClieId
                    odrDetaMail.Item("acti_esbl_id") = lstrEstaId
                    'ALTA 
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaMail, odrDetaMail)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaMail, odrDetaMail)
                End If
            Next

            'Razas del establecimiento.
            For Each odrDetaRaza As DataRow In mdsDatos.Tables(mstrTablaRaza).Select()
                If odrDetaRaza.Item("esra_esbl_id") Is DBNull.Value Then
                    'ALTA 
                    odrDetaRaza.Item("esra_esbl_id") = lstrEstaId
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaRaza, odrDetaRaza)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaRaza, odrDetaRaza)
                End If
            Next

            'Criadores asociados al establecimiento.
            For Each odrDetaCria As DataRow In mdsDatos.Tables(mstrTablaCria).Select()
                If odrDetaCria.Item("escr_esbl_id") Is DBNull.Value Then
                    'ALTA 
                    odrDetaCria.Item("escr_esbl_id") = lstrEstaId
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaCria, odrDetaCria)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaCria, odrDetaCria)
                End If
            Next

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrEstaId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Private Sub mBorrarDetas(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTablaDet As String, ByVal pstrEstaId As String, ByVal pstrTipo As String)
        Dim lstrCmd As String
        For Each odrDeta As DataRow In mdsDatos.Tables(pstrTablaDet).Select("", "", DataViewRowState.Deleted)
            With odrDeta
                .RejectChanges()
                If Not .IsNull(0) Then
                    Select Case pstrTipo
                        Case "T"
                            lstrCmd = "tele_clientes_baja " & odrDeta(0) & "," & mstrUserId
                        Case "M"
                            lstrCmd = "mails_clientes_baja " & odrDeta(0) & "," & mstrUserId
                    End Select
                    clsSQLServer.gExecute(lTransac, lstrCmd)
                End If
                odrDeta.Table.Rows.Remove(odrDeta)
            End With
        Next
    End Sub


    Public Function GetEstablecimientobyClienteId(ByVal ClienteId As String) As String
        Dim dtEstablecimiento As New DataTable
        Dim mstrCmd As String
        Dim EstablecimientoDescripcion As String
        EstablecimientoDescripcion = ""


        mstrCmd = "exec GetEstablecimientobyClienteId "
        mstrCmd = mstrCmd + " @ClienteId = " + clsSQLServer.gFormatArg(ClienteId, SqlDbType.VarChar)
        dtEstablecimiento = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtEstablecimiento.Rows.Count > 0 Then
            EstablecimientoDescripcion = dtEstablecimiento.Rows(0).Item("esbl_desc")
        End If


        Return EstablecimientoDescripcion



    End Function


    Public Function GetEstablecimientoByRazaIdCriadorId(ByVal RazaId As String, ByVal CriadorId As String) As String
        Dim dtEstablecimiento As New DataTable
        Dim mstrCmd As String
        Dim EstablecimientoDescripcion As String
        EstablecimientoDescripcion = ""


        mstrCmd = "exec GetEstablecimientoByRazaIdCriadorId "
        mstrCmd = mstrCmd + " @RazaId = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + " ,@CriadorId = " + clsSQLServer.gFormatArg(CriadorId, SqlDbType.VarChar)
        dtEstablecimiento = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtEstablecimiento.Rows.Count > 0 Then
            EstablecimientoDescripcion = dtEstablecimiento.Rows(0).Item("esbl_desc")
        End If


        Return EstablecimientoDescripcion



    End Function


    Public Function GetEstablecCriadorIDByCriaIdEstablecId(ByVal CriadorId As String, ByVal EstablecimientoId As String) As String
        Dim dtEstablecimiento As New DataTable
        Dim mstrCmd As String
        Dim strEstablecCriadorID As String = ""



        mstrCmd = "exec GetEstablecCriadorIDByCriaIdEstablecId "
        mstrCmd = mstrCmd + " @Cria_Id = " + clsSQLServer.gFormatArg(CriadorId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @esbl_id = " + clsSQLServer.gFormatArg(EstablecimientoId, SqlDbType.VarChar)
        dtEstablecimiento = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtEstablecimiento.Rows.Count > 0 Then
            strEstablecCriadorID = dtEstablecimiento.Rows(0).Item("escr_id")
        End If


        Return strEstablecCriadorID



    End Function


End Class