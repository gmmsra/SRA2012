Imports ReglasValida.Validaciones


Public Class Estado

    Inherits Generica


    Protected server As System.Web.HttpServerUtility


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub

    Public Function GetEstadoById(ByVal EstadoId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName

        Dim mstrCmd As String

        mstrCmd = "exec GetEstadoById "
        mstrCmd = mstrCmd + " @EstadoId = " + clsSQLServer.gFormatArg(EstadoId, SqlDbType.Int)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetEstadoDescripById(ByVal EstadoId As Integer) As String



        Dim dtEstado As DataTable
        Dim sReturn As String = ""


        dtEstado = Me.GetEstadoById(EstadoId.ToString(), "")
        If dtEstado.Rows.Count > 0 Then
            sReturn = dtEstado.Rows(0).Item("ESTA_DESC").ToString()
        End If

        Return sReturn
    End Function


End Class