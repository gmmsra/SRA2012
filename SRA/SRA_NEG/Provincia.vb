
Imports ReglasValida.Validaciones
Public Class Provincia
    Inherits Generica



    Protected server As System.Web.HttpServerUtility

   
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId

    End Sub



    Public Function GetProvinciaDescripcionById(ByVal provinciaId As String) As String
        Dim dtProvincia As New DataTable
        Dim mstrCmd As String
        Dim ProvinciaDescripcion As String = ""



        mstrCmd = "exec GetProvinciaById "
        mstrCmd = mstrCmd + " @pcia_id = " + clsSQLServer.gFormatArg(provinciaId, SqlDbType.VarChar)
        dtProvincia = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtProvincia.Rows.Count > 0 Then
            ProvinciaDescripcion = dtProvincia.Rows(0).Item("pcia_desc")
        End If


        Return ProvinciaDescripcion.Trim()



    End Function






End Class