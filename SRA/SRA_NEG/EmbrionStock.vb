Public Class EmbrionStock


    Inherits Generica


    Protected server As System.Web.HttpServerUtility


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub

    Public Function GetEmbrionesStockByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName

        Dim mstrCmd As String

        mstrCmd = "exec GetEmbrionesStockByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function


    Public Function GetDescripcionTE(ByVal pstrId As String)
        Dim lsrtFiltro As String = " @tede_id =" + clsSQLServer.gFormatArg(pstrId, SqlDbType.Int)
        Dim dt As New DataTable
        Dim ds As DataSet
        Dim mstrCmd As String


        mstrCmd = "exec dbo.te_denun_tramite_busq " & lsrtFiltro

        'For Each ldr As DataRow In ds.Tables(0).Select
        '    With ldr
        '        txtTeDesc.Text = "Denuncia TE Nro.: " + Convert.ToString(.Item("tede_nume"))
        '        txtCantEmbrSemen.Text = .Item("tede_embr")
        '        usrProd.usrPadreExt.Valor = .Item("tede_pad1_prdt_id")

        '    End With
        'Next

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        Return dt


    End Function

    Public Function GetSaldoEmbrionesStockByCriaIdPadres(ByVal pCria_Id As Long, _
ByVal ppadr_prdt_id As Long, ByVal pmadr_prdt_id As Long) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String

        mstrCmd = "exec GetSaldoEmbrionesStockByCriaIdPadres "
        mstrCmd = mstrCmd + " @Cria_Id = " + clsSQLServer.gFormatArg(pCria_Id, SqlDbType.VarChar)

        mstrCmd = mstrCmd + ", @padr_prdt_id = " + clsSQLServer.gFormatArg(ppadr_prdt_id, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @madr_prdt_id = " + clsSQLServer.gFormatArg(pmadr_prdt_id, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function


    Public Function GetSaldoEmbrionesStockByClieIdPadres(ByVal pClie_Id As Long, _
ByVal ppadr_prdt_id As Long, ByVal pmadr_prdt_id As Long) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String

        mstrCmd = "exec GetSaldoEmbrionesStockByClieIdPadres "
        mstrCmd = mstrCmd + " @Clie_Id = " + clsSQLServer.gFormatArg(pClie_Id, SqlDbType.VarChar)

        mstrCmd = mstrCmd + ", @padr_prdt_id = " + clsSQLServer.gFormatArg(ppadr_prdt_id, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @madr_prdt_id = " + clsSQLServer.gFormatArg(pmadr_prdt_id, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetSaldosComprador(ByVal pCriaId As Long, ByVal pPadreId As Long, ByVal pMadreId As Long) As DataTable

        Dim dtEmbrionStockComp As DataTable

        Dim oEmbrion As New SRA_Neg.EmbrionStock(mstrConn, Me.mstrUserId)
        dtEmbrionStockComp = oEmbrion.GetSaldoEmbrionesStockByCriaIdPadres(pCriaId, pPadreId, pMadreId)


        Return dtEmbrionStockComp


    End Function

    Public Function GetSaldosVendedor(ByVal pCriaId As Long, ByVal pPadreId As Long, ByVal pMadreId As Long) As DataTable


        Dim dtEmbrionStockVend As DataTable
        Dim oEmbrion As New SRA_Neg.EmbrionStock(mstrConn, Me.mstrUserId)
        dtEmbrionStockVend = oEmbrion.GetSaldoEmbrionesStockByCriaIdPadres(pCriaId, pPadreId, pMadreId)

        Return dtEmbrionStockVend

    End Function



End Class

