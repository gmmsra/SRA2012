
Imports ReglasValida.Validaciones
Public Class Enfermedades
    Inherits Generica

    Protected server As System.Web.HttpServerUtility

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId

    End Sub

    Public Function GetEnfermedadDescripcionById(ByVal enfe_Id As String) As String
        Dim dtEnfermedad As New DataTable
        Dim mstrCmd As String
        Dim EnfermedadDescripcion As String = ""



        mstrCmd = "exec GetEnfermedadDescripcionById "
        mstrCmd = mstrCmd + " @enfe_Id = " + clsSQLServer.gFormatArg(enfe_Id, SqlDbType.VarChar)
        dtEnfermedad = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtEnfermedad.Rows.Count > 0 Then
            EnfermedadDescripcion = dtEnfermedad.Rows(0).Item("enfe_deno")
        End If


        Return EnfermedadDescripcion.Trim()



    End Function

    Public Function GetEnfermedadDiasVigenciaById(ByVal enfe_Id As String) As Integer
        Dim dtEnfermedad As New DataTable
        Dim mstrCmd As String
        Dim EnfermedadDiasVigencia As String = ""



        mstrCmd = "exec GetEnfermedadDiasVigenciaById "
        mstrCmd = mstrCmd + " @enfe_Id = " + clsSQLServer.gFormatArg(enfe_Id, SqlDbType.VarChar)
        dtEnfermedad = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtEnfermedad.Rows.Count > 0 Then
            EnfermedadDiasVigencia = dtEnfermedad.Rows(0).Item("enfe_dias_vigencia")
        End If


        Return EnfermedadDiasVigencia



    End Function
End Class