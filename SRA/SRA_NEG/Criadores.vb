Imports System.Data.SqlClient

Public Class Criadores
   Inherits Generica

   Protected server As System.Web.HttpServerUtility

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet, ByVal server As System.Web.HttpServerUtility)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mdsDatos = pdsDatos
      Me.server = server
   End Sub

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId

    End Sub

    Public Sub BajaMoti(ByVal pstrId As String, ByVal pstrBajaMoti As String, ByVal pstrUser As String)
        mBaja(pstrId, pstrBajaMoti, pstrUser)
    End Sub

    Public Overloads Function Alta() As String
        Return (mActualizar())
    End Function

    Public Overloads Sub Modi(ByVal pbooAgru As Boolean)
        mActualizar()
    End Sub

    Private Function mActualizar() As String
        Dim lstrTABLAId As String
        Dim lstrCriaId As String
        Dim lTransac As SqlClient.SqlTransaction

        Try

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'Datos del criador.
            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                lstrCriaId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrCriaId = MiRow.Item(0).ToString
            End If

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrCriaId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Private Sub mBaja(ByVal pstrId As String, ByVal pstrBajaMoti As String, ByVal pstrUser As String)

        Dim lstrCmd As String
        Dim lstrCriaId As String
        Dim lTransac As SqlClient.SqlTransaction

        Try

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            lstrCmd = mstrTabla & "_baja " & pstrId
            lstrCmd = lstrCmd & ",'" & pstrBajaMoti & "'"
            lstrCmd = lstrCmd & ", " & pstrUser

            clsSQLServer.gExecute(lTransac, lstrCmd)

            clsSQLServer.gCommitTransac(lTransac)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Sub

    Public Function GetCriadorIdByClienteId(ByVal ClienteId As String) As Integer
        Dim dtCriador As New DataTable
        Dim idCriador As Int32

        Dim mstrCmd As String
        dtCriador.TableName = "dtCriador"

        idCriador = 0

        mstrCmd = "exec GetCriadorIdByClienteId "
        mstrCmd = mstrCmd + " @ClienteId = " + clsSQLServer.gFormatArg(ClienteId, SqlDbType.VarChar)
        dtCriador = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dtCriador.Rows.Count > 0 Then

            idCriador = IIf(dtCriador.Rows(0).Item("cria_id") Is System.DBNull.Value, 0, dtCriador.Rows(0).Item("cria_id"))
        End If

        Return idCriador

    End Function

    Public Function GetCriadorIdByProductoId(ByVal ProductoId As String) As Integer
        Dim dtCriador As New DataTable
        Dim idCriador As Int32

        Dim mstrCmd As String
        dtCriador.TableName = "dtCriador"

        idCriador = 0

        mstrCmd = "exec GetCriadorIdByProductoId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dtCriador = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dtCriador.Rows.Count > 0 Then
            idCriador = IIf(dtCriador.Rows(0).Item("prdt_cria_id") Is System.DBNull.Value, 0, dtCriador.Rows(0).Item("prdt_cria_id"))
        End If

        Return idCriador

    End Function
    Public Function GetCriaNumeByCriadorId(ByVal CriadorId As String) As Integer
        Dim dtCriador As New DataTable
        Dim CriadorNume As Int32

        Dim mstrCmd As String
        dtCriador.TableName = "dtCriador"

        CriadorNume = 0

        mstrCmd = "exec GetCriadorById "
        mstrCmd = mstrCmd + " @CriadorId = " + clsSQLServer.gFormatArg(CriadorId, SqlDbType.VarChar)
        dtCriador = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dtCriador.Rows.Count > 0 Then
            CriadorNume = IIf(dtCriador.Rows(0).Item("cria_nume") Is System.DBNull.Value, 0, dtCriador.Rows(0).Item("cria_nume"))
        End If

        Return CriadorNume

    End Function

    Public Function GetCriadorIdByRazaIdCriaNume(ByVal RazaId As String, ByVal CriaNume As String) As Integer
        Dim dtCriador As New DataTable
        Dim idCriador As Int32

        Dim mstrCmd As String
        dtCriador.TableName = "dtCriador"

        idCriador = 0

        mstrCmd = "exec GetCriadorIdByRazaIdCriaNume "
        mstrCmd = mstrCmd + " @RazaId = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar) + ","
        mstrCmd = mstrCmd + " @CriaNume = " + clsSQLServer.gFormatArg(CriaNume, SqlDbType.VarChar)
        dtCriador = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dtCriador.Rows.Count > 0 Then
            idCriador = IIf(dtCriador.Rows(0).Item("cria_id") Is System.DBNull.Value, 0, dtCriador.Rows(0).Item("cria_id"))
        End If

        Return idCriador

    End Function


    Public Function GetCriadorByRazaIdCriaNume(ByVal RazaId As String, ByVal CriaNume As String) As DataTable
        Dim dtCriador As New DataTable
        Dim idCriador As Int32

        idCriador = 0

        Dim mstrCmd As String
        dtCriador.TableName = "dtCriador"


        mstrCmd = "exec GetCriadorIdByRazaIdCriaNume "
        mstrCmd = mstrCmd + " @RazaId = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar) + ","
        mstrCmd = mstrCmd + " @CriaNume = " + clsSQLServer.gFormatArg(CriaNume, SqlDbType.VarChar)
        dtCriador = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dtCriador.Rows.Count > 0 Then
            dtCriador = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        End If



        Return dtCriador

    End Function


End Class
