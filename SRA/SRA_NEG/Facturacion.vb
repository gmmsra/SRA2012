Imports System.Net
Imports System.Object
Imports System
Imports System.Web
Imports System.Web.UI.Page
Imports System.Web.UI.Control
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebControl
Imports System.Web.UI.WebControls.TableRow
Imports System.Web.UI.WebControls.DataGridItem
Imports ReglasValida.Validaciones

Public Class Facturacion
    Inherits Generica

#Region "Variables"
    Private mboolPagoCtaCte As Boolean
    Private mboolAutorizaciones As Boolean
    Private mboolTarjetas As Boolean
    Private mboolTurnos As Boolean
    Private mboolRRGG As Boolean
    Private mstrEstadoSaldos As String
    Private mstrEspeciales As String
    Private marrEstadoSaldos As System.Collections.ArrayList
    Private marrEspeciales As System.Collections.ArrayList
    Private mdoubCotizaDolar As Double
    Private mdsDs As DataSet
    Const mintConcAdic As Integer = 298 'ADICIONAL POR SUSPENSION DE SERVICI



    Private mRequest As System.Web.HttpRequest

#Region "Enumeradores"
    Private Enum TipoComprobGene As Integer
        Factura = 0
        Proforma = 1
        ProformaAuto = 2
        ProformaExcede = 3
    End Enum
    Private Enum TipoItems As Integer
        descuento = 1
        sobretasa = 2
        concepTurno = 3
    End Enum
    Private Enum CamposTurnos
        ID = 0
        TURN_OTO = 1
        TURN_REC = 2
        FECHA_REC = 3
        FECHA_COSE = 4
        FECHA_FISE = 5
        SOB_PFF = 6
        REC_EFF = 7
        DTO_OPP = 8
        DTO_OPC = 9
        REC_PTU = 10
        REGICONT = 11
        clcu_id = 12
        cant = 13
    End Enum
    Private Enum CamposTurnosEnDeta
        SOB_PFF = 0  'sobretasa palermo
        REC_EFF = 1  'recargo turno
        REC_PTU = 2 'recargo tram. urg.
        DTO_OPP = 3  'desc. promocion
        DTO_OPC = 4  'desc. cantidad 
    End Enum
    Private Enum Columnas As Integer

        id = 1
        tipo = 2
        codi_aran_concep = 3
        desc_aran_concep = 4
        cant = 5
        cant_sin_cargo = 6
        anim_no_fact = 7
        impo = 8
        fecha = 9
        inic_rp = 10
        fina_rp = 11
        tram = 12
        acta = 13
        ccos_codi = 14
        exen = 15
        sin_carg = 16
        aran_concep_id = 17
        ccos_id = 18
        impo_bruto = 19
        tipo_IVA = 20
        tasa_iva = 21
        impo_ivai = 22
        autom = 23
    End Enum
    Private Enum OtrosDatos As Integer
        cuit = 0
        condIVA = 1
        categoria = 2
        legIsea = 3
        LegCeida = 4
        estado = 5
        NroSocio = 6
        legEgea = 7
    End Enum
    Private Enum tablasDs As Integer
        mstrTabla = 0
        mstrTablaDeta = 1
        mstrTablaDetaAran = 2
        mstrTablaDetaConcep = 3
        mstrTablaVtos = 4
        mstrTablaAutor = 5
        mstrTablaPagos = 6 ' Dario 2013-07-19 
        mstrTablaSobreTasaVtos = 7  ' Dario 2013-07-19 
        mstrTablaTempDeta = 8
    End Enum
    Private Enum autori As Integer
        'tabla autoriza_tipos
        Convenio_con_Saldo_Inferior = 1
        Descuentos_no_Automaticos = 2
        Eliminar_Descuentos_Automaticos = 3
        Facturar_con_Saldos_Vencidos = 4
        Facturar_sin_Cargo = 5
        Sobretasas_eliminadas = 6
    End Enum
    Private Enum Tablas As Integer
        inturfac_bov = 0
        inturfac_equ = 1
        estados = 2
        turnos = 3
    End Enum
    Public Enum TipoMoneda As Integer
        dolar = 2
        euro = 13
    End Enum
    Public Enum TipoPeriodo As Integer
        mes = 1
        dia = 2
        diaMes = 3 ' para las sobretasas con formula 51 y periodo "dia" (cant*precio*dif)
        mesMes = 4 ' para las sobretasas con formula 51 y periodo "mes"(cant*precio*dif)
        mesa�o = 5 ' para la mayoria de las rutinas, toma el mes (sin dias) para el calculo
    End Enum
    Public Enum CamposListaPreciosAran
        aran_id = 0
        impo_soci = 1
        impo_noso = 2
        impo_adhe = 3
        mone_id = 4
        reca_adhe = 5
        reca_noso = 6
        reca_ctac = 7
        reca_tarj = 8
        aplicAdhe = 15
    End Enum
    Public Enum CamposEspeciales As Integer
        exento = 0
        iva = 1
        soci = 2
        precio_socio = 3
        miem = 4
        color = 5
        desc = 6
        socio_falle_precio_socio = 7
        soli_tram_precio_socio = 8
        obser = 9
        precio_no_socio = 10 ' fallec, renun en tram, baja, susp
        socio_adhe = 11
        cate_id = 12
        desc_tari = 13
        socio_adicional = 14
        categoIIBB = 15
        nroInsc = 16
        agrupCantMiembrosSocios = 17
        agrupCantMiembrosNoSocios = 18
        agrupCantMiembrosSociosAdherentes = 19
        agrupCantMiembrosMorosos = 20
    End Enum
    Public Enum CamposEstado As Integer
        saldo_cta_cte = 0
        saldo_social = 1
        saldo_total = 2
        imp_a_cta_cta_cte = 3
        imp_a_cta_cta_social = 4
        lim_saldo = 5
        color = 6
        Saldo_excedido = 7
        saldo_cta_cte_venc = 8
        saldo_social_venc = 9
        TotalProformas = 10
        OtrasCtas = 11
        OtrasCtasVenc = 12

    End Enum
    Public Enum CamposPreciosLista As Integer 'precios_lista_aran_consul
        peli_id = 0
        peli_acti_id = 1
        peli_reca_adhe = 2
        peli_vige_fecha = 3
        peli_reca_noso = 4
        peli_aprob_fecha = 5
        peli_reca_ctac = 6
        peli_reca_tarj = 7
        prar_id = 8
        prar_aran_id = 9
        prar_impo_soci = 10
        prar_impo_adhe = 11
        prar_mone_id = 12
        prar_impo_noso = 13

    End Enum
    Public Enum CamposAranceles As Integer ' aranceles_consul
        aran_id = 0
        aran_desc = 1
        aran_acti_id = 2
        aran_ccos_id = 3
        aran_cuct_id = 4
        aran_grav = 5
        aran_exen = 6
        aran_grava_tasa = 7
        aran_codi = 8
        aran_form_id = 9
        aran_cant_anim = 10
        aran_rrgg_desc_fech = 11
        aran_rrgg_tser_id = 12
        aran_rrgg_vmax = 13
        aran_rrgg_vmin = 14
        aran_rrgg_vper = 15
        aran_rrgg_cmax = 16
        aran_rrgg_cmin = 17
        aran_rrgg_cper = 18
        aran_rrgg_aran_id = 19
        aran_rrgg_mmyy = 26
        aran_rrgg_verror = 29
        aran_val_fecha = 38
    End Enum
    Public Enum TiposDePagos As Integer
        Contado = 1
        CtaCte = 2
        Tarjeta = 3
        PagoDiferido = 4
    End Enum
    Public Enum Actividades As Integer
        Varios = 1 ' administracion por ejemplo, pero en relidad son todos los demas
        RRGG = 3
        Laboratorio = 4
        AlumnosISEA = 6
        AlumnosCEIDA = 10
        Exposiciones = 7
    End Enum
    Public Enum Formulas
        AjustePorConvenio = 1
        DescFactSinCargo = 3
        SobretasaPalermo = 9
        RecargoFueraDeFecha = 10
        DtoXPromoLaboratorio = 11
        DescuentoXCantidad = 12
        RecargoXTramiteUrgente = 17
        PrecioXCant = 50
        PrecioXCantXDif = 51
        DescuentoFacturacionExenta = 14
        AdicionalSuspensionServicios = 26


    End Enum
    Public Enum TiposIVA As Integer
        tasa = 1
        reduc = 2
        sobre = 3

    End Enum
    Public Enum TiposComprobantes As Integer
        Factura = 29
        Proforma = 28


    End Enum

#End Region
#End Region

#Region "Constructores"
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet, ByVal pSession As System.Web.SessionState.HttpSessionState)
        mstrConn = pstrConn
        mstrUserId = pstrUserId
        mstrTabla = pstrTabla
        mdsDatos = pdsDatos
        mSetearDsEstados(pstrConn, pstrUserId, pSession)
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pSession As System.Web.SessionState.HttpSessionState)
        mSetearDsEstados(pstrConn, pstrUserId, pSession)
    End Sub
    
    Private Sub mSetearDsEstados(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pSession As System.Web.SessionState.HttpSessionState)
        mstrConn = pstrConn

        If Not pSession Is Nothing Then
            mdsDs = pSession("ds")
        End If

        If mdsDs Is Nothing Then
            mdsDs = CrearDataSet(pstrConn, pstrUserId)

            If Not pSession Is Nothing Then
                pSession("ds") = mdsDs
            End If
        End If
    End Sub

    Public Sub New(ByVal pPagina As System.Web.UI.Page, ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.New(pstrConn, pstrUserId, pPagina.Session)
        mRequest = pPagina.Request
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub
    Private Function CrearDataSet(ByVal pstrConn As String, ByVal pstrUserId As String) As DataSet
        Dim ldsEsta As New DataSet

        If pstrConn <> "" Then     'JOSE: para cuando no es facturaci�n
            ldsEsta = clsSQLServer.gObtenerEstruc(pstrConn, "inturfac_bov", "")

            ldsEsta.Tables(0).TableName = "inturfac_bov"
            ldsEsta.Tables(1).TableName = "inturfac_equ"
        Else
            ldsEsta.Tables.Add("inturfac_bov")
            ldsEsta.Tables.Add("inturfac_equ")
        End If

        ldsEsta.Tables.Add("estados")
        With ldsEsta.Tables(Tablas.estados)
            .Columns.Add("mboolPagoCtaCte", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolAutorizaciones", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolTarjetas", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolRRGG", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolLabora", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolExpo", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolVarios", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolAlum", System.Type.GetType("System.Boolean"))
            .Columns.Add("mintActiv", System.Type.GetType("System.Int32"))
            .Columns.Add("mstrEstadoSaldos", System.Type.GetType("System.String"))
            .Columns.Add("mstrEspeciales", System.Type.GetType("System.String"))
            .Columns.Add("marrEstadoSaldos", System.Type.GetType("System.Collections.ArrayList"))
            .Columns.Add("marrEspeciales", System.Type.GetType("System.Collections.ArrayList"))
            .Columns.Add("mdoubCotizaDolar", System.Type.GetType("System.Double"))
            .Columns.Add("mdecTotalBrutoGral", System.Type.GetType("System.Decimal"))
            .Columns.Add("mdecTotalIVAGral", System.Type.GetType("System.Decimal"))
            .Columns.Add("mdecTotalSobreTGral", System.Type.GetType("System.Decimal"))
            .Columns.Add("mdecTotalesIVA", System.Type.GetType("System.Decimal"))
            .Columns.Add("mdecTotalReduGral", System.Type.GetType("System.Decimal"))
            .Columns.Add("mdecTotalNetoGral", System.Type.GetType("System.Decimal"))
            .Columns.Add("mdecCuotaImporte", System.Type.GetType("System.Decimal"))
            .Columns.Add("mdecCuotaPorcentaje", System.Type.GetType("System.Decimal"))
            .Columns.Add("mstrLetraComprob", System.Type.GetType("System.String"))
            .Columns.Add("mintCentroEmisorId", System.Type.GetType("System.Int32"))
            .Columns.Add("mintCentroEmisorNro", System.Type.GetType("System.Int32"))
            .Columns.Add("mdecIVATasa", System.Type.GetType("System.Decimal"))
            .Columns.Add("mdecIVATasaRedu", System.Type.GetType("System.Decimal"))
            .Columns.Add("mdecIVATasaSobre", System.Type.GetType("System.Decimal"))
            .Columns.Add("mboolConvSaldo", System.Type.GetType("System.Boolean"))
            .Columns.Add("mstrHost", System.Type.GetType("System.String"))
            '.Columns.Add("mintDiasVtoCuota", System.Type.GetType("System.Int32"))
            .Columns.Add("mintItems", System.Type.GetType("System.Int32")) ' items del comprobante
            .Columns.Add("mboolAranRRGGRepresAnim", System.Type.GetType("System.Boolean"))
            .Columns.Add("mstrAranRRGGDescFecha", System.Type.GetType("System.String"))
            .Columns.Add("mboolAranRRGGActa", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolAranRRGGError", System.Type.GetType("System.Boolean"))
            .Columns.Add("mintAranRRGGSobretasa", System.Type.GetType("System.Int32"))
            .Columns.Add("mboolAranRRGGconSobretasaNormal", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolAranRRGGconSobretasaRango", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolAranRRGGconSobretasa", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolProforma", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolProformaAutoExcSaldo", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolProformaAuto", System.Type.GetType("System.Boolean"))
            .Columns.Add("mintTipoComprobGenera", System.Type.GetType("System.Int32"))
            .Columns.Add("mboolProformaCmbComprobCargado", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolPrecioSocio_falle", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolTurnosDTSEqui", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolTurnosDTSBov", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolRecalculaValoresProforma", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolProformaRecalcularaValores", System.Type.GetType("System.Boolean"))
            .Columns.Add("mbooPrecioNosocio", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolProformaPend", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolTurnos", System.Type.GetType("System.Boolean"))
            .Columns.Add("mboolPrecioSocio_trami", System.Type.GetType("System.Boolean"))

            .Rows.Add(.NewRow)

            .Rows(0).Item("mboolPagoCtaCte") = False
            .Rows(0).Item("mboolAutorizaciones") = False
            .Rows(0).Item("mboolTarjetas") = False
            .Rows(0).Item("mboolRRGG") = False
            .Rows(0).Item("mboolAlum") = False
            .Rows(0).Item("mboolVarios") = True
            .Rows(0).Item("mboolLabora") = False
            .Rows(0).Item("mboolExpo") = False
            .Rows(0).Item("mboolProformaPend") = False
            .Rows(0).Item("mintActiv") = 0
            .Rows(0).Item("mstrEstadoSaldos") = ""
            .Rows(0).Item("mstrEspeciales") = ""
            .Rows(0).Item("mdoubCotizaDolar") = 0
            .Rows(0).Item("mdecTotalBrutoGral") = 0
            .Rows(0).Item("mdecTotalIVAGral") = 0
            .Rows(0).Item("mdecTotalesIVA") = 0
            .Rows(0).Item("mdecTotalSobreTGral") = 0
            .Rows(0).Item("mdecTotalReduGral") = 0
            .Rows(0).Item("mdecTotalNetoGral") = 0
            .Rows(0).Item("mdecCuotaImporte") = 0
            .Rows(0).Item("mdecCuotaPorcentaje") = 0
            .Rows(0).Item("mstrLetraComprob") = ""
            .Rows(0).Item("mintCentroEmisorId") = 0
            .Rows(0).Item("mintCentroEmisorNro") = 0
            .Rows(0).Item("mdecIVATasa") = 0
            .Rows(0).Item("mdecIVATasaRedu") = 0
            .Rows(0).Item("mdecIVATasaSobre") = 0
            .Rows(0).Item("mboolConvSaldo") = False
            .Rows(0).Item("mstrHost") = ""
            '.Rows(0).Item("mintDiasVtoCuota") = 0
            .Rows(0).Item("mboolAranRRGGRepresAnim") = False
            .Rows(0).Item("mstrAranRRGGDescFecha") = ""
            .Rows(0).Item("mboolAranRRGGActa") = False
            .Rows(0).Item("mboolAranRRGGError") = False
            .Rows(0).Item("mintAranRRGGSobretasa") = 0
            .Rows(0).Item("mboolAranRRGGconSobretasa") = False
            .Rows(0).Item("mboolAranRRGGconSobretasaNormal") = False
            .Rows(0).Item("mboolAranRRGGconSobretasaRango") = False
            .Rows(0).Item("mboolProforma") = False
            .Rows(0).Item("mboolProformaAutoExcSaldo") = False
            .Rows(0).Item("mboolProformaAuto") = False
            .Rows(0).Item("mintTipoComprobGenera") = 0
            .Rows(0).Item("mboolProformaCmbComprobCargado") = False
            .Rows(0).Item("mboolPrecioSocio_falle") = False
            .Rows(0).Item("mboolTurnosDTSEqui") = False
            .Rows(0).Item("mboolTurnosDTSBov") = False
            .Rows(0).Item("mboolRecalculaValoresProforma") = False
            .Rows(0).Item("mboolProformaRecalcularaValores") = False
            .Rows(0).Item("mbooPrecioNosocio") = False
            .Rows(0).Item("mboolTurnos") = False
            .Rows(0).Item("mboolPrecioSocio_trami") = False

        End With

        Return ldsEsta
    End Function
#End Region

#Region "Propiedades"
    Public Property pLetraComprob() As String
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrLetraComprob")

        End Get
        Set(ByVal Value As String)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrLetraComprob") = Value
        End Set
    End Property
    Public Property pRecalculaValoresProforma() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolRecalculaValoresProforma")

        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolRecalculaValoresProforma") = Value
        End Set
    End Property
    Public Property pProformaRecalcularaValores() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaRecalcularaValores")

        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaRecalcularaValores") = Value
        End Set
    End Property

    Public Property pPrecioNosocio() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mbooPrecioNosocio")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mbooPrecioNosocio") = Value
        End Set
    End Property

    Public Property pIVATasa() As Double
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecIVATasa")
        End Get
        Set(ByVal Value As Double)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecIVATasa") = Value
        End Set
    End Property
    Public Property pIVATasaRedu() As Double
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecIVATasaRedu")
        End Get
        Set(ByVal Value As Double)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecIVATasaRedu") = Value
        End Set
    End Property
    Public Property pIVATasaSobre() As Double
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecIVATasaSobre")
        End Get
        Set(ByVal Value As Double)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecIVATasaSobre") = Value
        End Set
    End Property
    Public Property pProformaPend() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaPend")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaPend") = Value
        End Set
    End Property
    Public Property pHost() As String
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrHost")
        End Get
        Set(ByVal Value As String)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrHost") = Value
        End Set
    End Property
    Public Property pTurnosDTSBov() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolTurnosDTSBov")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolTurnosDTSBov") = Value
        End Set
    End Property
    Public Property pTurnosDTSEqui() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolTurnosDTSEqui")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolTurnosDTSEqui") = Value
        End Set
    End Property
    Public Property pCentroEmisorNro() As Integer
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mintCentroEmisorNro")
        End Get
        Set(ByVal Value As Integer)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mintCentroEmisorNro") = Value
        End Set
    End Property
    Public Property pCentroEmisorId() As Integer
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mintCentroEmisorId")
        End Get
        Set(ByVal Value As Integer)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mintCentroEmisorId") = Value
        End Set
    End Property
    Public Property pTurnos() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolTurnos")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolTurnos") = Value
        End Set
    End Property
    Public Property pTarjetas() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolTarjetas")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolTarjetas") = Value
        End Set
    End Property
    Public Property pConvSaldo() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolConvSaldo")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolConvSaldo") = Value
        End Set
    End Property
    Public Property pTotalBrutoGral() As Decimal
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalBrutoGral")
        End Get
        Set(ByVal Value As Decimal)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalBrutoGral") = Value
        End Set
    End Property

    Public Property pPrecioSocio_falle() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolPrecioSocio_falle")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolPrecioSocio_falle") = Value
        End Set
    End Property

    Public Property pPrecioSocio_trami() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolPrecioSocio_trami")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolPrecioSocio_trami") = Value
        End Set
    End Property

    Public Property pTotalNetoGral() As Decimal
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalNetoGral")
        End Get
        Set(ByVal Value As Decimal)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalNetoGral") = Value
        End Set
    End Property
    Public Property pTotalIVAGral() As Decimal
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalIVAGral")
        End Get
        Set(ByVal Value As Decimal)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalIVAGral") = Value
        End Set
    End Property
    Public Property pCuotaImporte() As Decimal
        Get

            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecCuotaImporte")
        End Get
        Set(ByVal Value As Decimal)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecCuotaImporte") = Value
        End Set
    End Property
    Public Property pCuotaPorcentaje() As Decimal
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecCuotaPorcentaje")
        End Get
        Set(ByVal Value As Decimal)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecCuotaPorcentaje") = Value
        End Set
    End Property
    Public Property pTotalSobreTGral() As Decimal
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalSobreTGral")
        End Get
        Set(ByVal Value As Decimal)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalSobreTGral") = Value
        End Set
    End Property
    Public Property pTotalesIVA() As Decimal
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalesIVA")
        End Get
        Set(ByVal Value As Decimal)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalesIVA") = Value
        End Set
    End Property
    Public Property pTotalReduGral() As Decimal
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalReduGral")
        End Get
        Set(ByVal Value As Decimal)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdecTotalReduGral") = Value
        End Set
    End Property
    Public Property pRRGG() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolRRGG")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolRRGG") = Value
        End Set
    End Property
    Public Property pProforma() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProforma")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProforma") = Value
        End Set
    End Property


    Public Property pAranRRGGconSobretasa() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGconSobretasa")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGconSobretasa") = Value
        End Set
    End Property
    Public Property pAranRRGGconSobretasaNormal() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGconSobretasaNormal")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGconSobretasaNormal") = Value
        End Set
    End Property
    Public Property pAranRRGGconSobretasaRango() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGconSobretasaRango")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGconSobretasaRango") = Value
        End Set
    End Property

    Public Property pAranRRGGActa() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGActa")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGActa") = Value
        End Set
    End Property

    Public Property pAranRRGGError() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGError")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGError") = Value
        End Set
    End Property
    Public Property pProformaCmbComprobCargado() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaCmbComprobCargado")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaCmbComprobCargado") = Value
        End Set
    End Property
    Public Property pAranRRGGDescFecha() As String
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrAranRRGGDescFecha")
        End Get
        Set(ByVal Value As String)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrAranRRGGDescFecha") = Value
        End Set
    End Property


    Public Property pAranRRGGSobretasa() As Integer
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mintAranRRGGSobretasa")

        End Get
        Set(ByVal Value As Integer)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mintAranRRGGSobretasa") = Value
        End Set
    End Property
    Public Property pAranRRGGRepresAnim() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGRepresAnim")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAranRRGGRepresAnim") = Value
        End Set
    End Property
    Public Property pTipo() As Integer
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mintActiv")
        End Get
        Set(ByVal Value As Integer)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mintActiv") = Value
        End Set
    End Property
    Public Property pActiv() As Integer
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mintActiv")
        End Get
        Set(ByVal Value As Integer)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mintActiv") = Value
        End Set
    End Property
    Public Property pAlum() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAlum")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAlum") = Value
        End Set
    End Property
    Public Property pLabora() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolLabora")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolLabora") = Value
        End Set
    End Property
    Public Property pExpo() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolExpo")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolExpo") = Value
        End Set
    End Property
    Public Property pVarios() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolVarios")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolVarios") = Value
        End Set
    End Property


    Public Property pPagoCtaCte() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolPagoCtaCte")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolPagoCtaCte") = Value
        End Set
    End Property


    Public Property pTipoComprobGenera() As Integer
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mintTipoComprobGenera")
        End Get
        Set(ByVal Value As Integer)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mintTipoComprobGenera") = Value
        End Set
    End Property
    Public Property pProformaAuto() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaAuto")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaAuto") = Value
        End Set
    End Property

    Public Property pProformaAutoExcSaldo() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaAutoExcSaldo")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolProformaAutoExcSaldo") = Value
        End Set
    End Property


    Public Property pAutorizaciones() As Boolean
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAutorizaciones")
        End Get
        Set(ByVal Value As Boolean)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mboolAutorizaciones") = Value
        End Set
    End Property
    Public Property pEstadoSaldos() As String

        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrEstadoSaldos")
        End Get
        Set(ByVal Value As String)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrEstadoSaldos") = Value
        End Set
    End Property
    Public Property pEspeciales() As String

        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrEspeciales")
        End Get
        Set(ByVal Value As String)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mstrEspeciales") = Value
        End Set
    End Property

    Public Property pEspecialesTodo() As System.Collections.ArrayList
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("marrEspeciales")
        End Get
        Set(ByVal Value As System.Collections.ArrayList)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("marrEspeciales") = Value
        End Set
    End Property
    Public Property pEstadoSaldosTodo() As System.Collections.ArrayList
        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("marrEstadoSaldos")
        End Get
        Set(ByVal Value As System.Collections.ArrayList)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("marrEstadoSaldos") = Value
        End Set
    End Property


    Public Property pCotizaDolar() As Double

        Get
            Return mdsDs.Tables(Tablas.estados).Rows(0).Item("mdoubCotizaDolar")
        End Get
        Set(ByVal Value As Double)
            mdsDs.Tables(Tablas.estados).Rows(0).Item("mdoubCotizaDolar") = Value
        End Set

    End Property


#End Region

#Region "Sub/Funciones Privadas"
    Private Function mDSProformas(ByVal pstrConn As String, ByVal pstrIds As String) As DataSet
        Dim ldsDatosProfo As DataSet = clsSQLServer.gObtenerEstruc(pstrConn, SRA_Neg.Constantes.gTab_Proformas, pstrIds)

        ldsDatosProfo.Tables(0).TableName = SRA_Neg.Constantes.gTab_Proformas
        ldsDatosProfo.Tables(1).TableName = SRA_Neg.Constantes.gTab_ProformaDeta
        ldsDatosProfo.Tables(2).TableName = SRA_Neg.Constantes.gTab_ProformaAranc
        ldsDatosProfo.Tables(3).TableName = SRA_Neg.Constantes.gTab_ProformaConcep
        ldsDatosProfo.Tables(4).TableName = SRA_Neg.Constantes.gTab_ProformaAutor

        Utiles.gAgregarTabla(pstrConn, ldsDatosProfo, SRA_Neg.Constantes.gTab_ProformaArancDeta)

        Return (ldsDatosProfo)
    End Function

    Private Function mFactSinCargo(ByVal pstrConn, ByVal pdsDatos) As Boolean
        'solo las filas no eliminadas 
        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            If ldr.Item("temp_tipo") = "A" Then
                If ldr.Item("temp_sin_carg") = "Si" Then
                    Return True
                End If
            End If
        Next
    End Function
    Private Function mDeudaVencida(ByVal pstrConn, ByVal pdsDatos) As Boolean
        ' filas no eliminadas 
        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            'solo aranceles
            If ldr.Item("temp_tipo") = "A" Then
                If ldr.Item("temp_sin_carg") = "Si" Then
                    Return True
                End If
            End If
        Next
    End Function
    Private Function mDescNoAuto(ByVal pstrConn, ByVal pdsDatos) As Boolean
        'solo las filas no eliminadas 
        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            'de aplica un descuento manualamente
            If ldr.Item("temp_auto") Is DBNull.Value And ldr.Item("temp_impo") < 0 Then
                If ldr.Item("temp_auto") = 1 Then Return True
            End If
        Next
    End Function

    Private Function mSobretasaAgregada(ByVal pstrConn, ByVal pdsDatos, ByVal pintOrigen) As Boolean

        For Each ldr As DataRow In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Select
            If ldr.IsNull("temp_borra") And ldr.IsNull("temp_sobre_auto") And ldr.IsNull("temp_rela_id") Then
                If ldr.Item("temp_tipo").ToString <> "C" Then
                    If (clsSQLServer.gCampoValorConsul(pstrConn, "aranceles_consul @aran_id=" & ldr.Item(21), "aran_rrgg_aran_id")).ToString > "0" Then
                        'ldr.Item("temp_desc_aran_concep_cortada").ToString.Substring(0, 1) = "S" Then
                        Return True
                    End If
                End If
            End If
        Next
    End Function

    Private Function mFilaEliminada(ByVal pstrConn, ByVal pdsDatos, ByVal pintOrigen) As Boolean
        For Each ldr As DataRow In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Select
            'elimina un desc autom�tico
            Select Case pintOrigen
                Case 1 ' descuentos
                    If Not (ldr.IsNull("temp_borra")) And Not (ldr.IsNull("temp_auto")) And (ldr.IsNull("temp_turnos")) Then
                        If ldr.Item("temp_auto") = 1 Then Return True
                    End If
                Case 2 ' sobretasa
                    If Not (ldr.IsNull("temp_borra")) And Not (ldr.IsNull("temp_rela_id")) And (ldr.Item("temp_tipo") = "S") Then
                        Return True
                    End If
                Case 3 ' concep. automaticos de turnos (laboratorio)
                    If Not (ldr.IsNull("temp_borra")) And Not (ldr.IsNull("temp_auto")) And Not (ldr.IsNull("temp_turnos")) Then
                        If ldr.Item("temp_auto") = 1 Then Return True
                    End If
            End Select
        Next
    End Function
    Private Sub mExistenciaDeSobretasas()
        If pAranRRGGconSobretasaNormal And pAranRRGGconSobretasaRango Then
            pAranRRGGconSobretasa = True
        Else
            pAranRRGGconSobretasa = False
        End If
    End Sub
    Private Function mMesDeUnaFecha(ByVal pstrFecha As Date) As Integer
        Dim ldateRef As Date = pstrFecha 'clsFormatear.gFormatFechaDateTime(pstrFecha)
        Return ldateRef.Month
    End Function
    Private Function mCrearDatasetConSobretasas(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintAranId As Integer, ByVal pintActivId As Integer, ByVal pstrFechaRef As Date, ByVal pstrFechaIva As Date) As DataSet
        Dim ds, dsR As DataSet
        Dim lintMes As Integer
        Dim lDtN, lDtR As DataTable
        Dim lDrN As DataRow


        'sobretasas con rango de fecha (el mes se toma de la fecha de referencia)
        lintMes = mMesDeUnaFecha(pstrFechaRef)
        dsR = SobretasasDelArancelNormalconRango(pstrConn, pintAranId, pintActivId, pstrFechaIva, lintMes)


        'sobretasas normales
        ds = SobretasasDelArancelNormal(pstrConn, pintAranId, pintActivId, pstrFechaIva)
        lDtN = ds.Tables(0)

        'agrego 2 columnas
        With ds.Tables(0)
            Dim lintCol As DataColumn
            .Columns.Add("raad_tope_fecha") ', Type.GetType("System.DateTime"))
            .Columns.Add("raad_sobre_fecha") ', Type.GetType("System.DateTime"))
        End With

        'se agregan las sobretasas que tiene rango de fechas 
        For Each ldr As DataRow In dsR.Tables(0).Select
            lDrN = lDtN.NewRow
            With lDrN
                .Item("aran_id") = ldr.Item("aran_id")
                .Item("aran_codi") = ldr.Item("aran_codi")
                .Item("aran_desc") = ldr.Item("aran_desc")
                .Item("valor_tasa") = ldr.Item("valor_tasa")
                .Item("tipo_tasa") = ldr.Item("tipo_tasa")
                .Item("aran_rrgg_desc_fech") = ldr.Item("aran_rrgg_desc_fech")
                .Item("codi_cc") = ldr.Item("codi_cc")
                .Item("ccos_id") = ldr.Item("ccos_id")
                .Item("raad_tope_fecha") = ldr.Item("raad_tope_fecha")
                .Item("raad_sobre_fecha") = ldr.Item("raad_sobre_fecha")
            End With
            lDtN.Rows.Add(lDrN)
        Next
        Return ds
    End Function

    Private Function mCrearDatasetConConceptos(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintFormula As Integer)
        Return mCrearDatasetConConceptos(pdsDatos, pstrConn, pintFormula, 0)
    End Function
    Private Function mCrearDatasetConConceptos(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintFormula As Integer, ByVal pintactiv As Integer) As DataSet
        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.conceptos_formulas_consul " & clsSQLServer.gFormatArg(pintFormula, SqlDbType.Int) & ", " & clsSQLServer.gFormatArg(pintactiv, SqlDbType.Int))
        Return ds
    End Function
    Public Function ConceptosFactExterna(ByVal pstrConn As String, ByVal pstrID01 As Integer, ByVal pstrID02 As Integer) As DataSet
        ' facturacion externa (conceptos)
        Return mDsFacExterna(pstrConn, pstrID01, pstrID02, "dbo.FACA802_consul")
    End Function
    Public Function VtosFactExterna(ByVal pstrConn As String, ByVal pstrID01 As Integer, ByVal pstrID02 As Integer) As DataSet
        ' facturacion externa (Vtos)
        Return mDsFacExterna(pstrConn, pstrID01, pstrID02, "dbo.FACA803_consul")
    End Function
    Public Function CabeFactExterna(ByVal pstrConn As String, ByVal pstrID01 As Integer, ByVal pstrID02 As Integer) As DataSet
        ' facturacion externa (Vtos)
        Return mDsFacExterna(pstrConn, pstrID01, pstrID02, "dbo.FACA801_consul")
    End Function
    Private Function mDsFacExterna(ByVal pstrConn As String, ByVal pstrID01 As Integer, ByVal pstrID02 As Integer, ByVal pstrSP As String) As DataSet
        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec " & pstrSP & " " & clsSQLServer.gFormatArg(pstrID01, SqlDbType.Int) & "," & clsSQLServer.gFormatArg(pstrID02, SqlDbType.Int))
        Return ds
    End Function
    Private Function mCrearDatasetConConceptos(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pstrIDS As String) As DataSet
        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.conceptos_formulasX_consul  @conc_form_ids ='" & pstrIDS & "'")
        Return ds
    End Function
    Private Function mSoloMesDia(ByVal dfecha As Date) As Integer
        Dim strRes As String
        strRes = CType(dfecha.Month, String)
        strRes = strRes.Insert(strRes.Length(), CType(dfecha.Day, String))

        Return CType(strRes, Integer)
    End Function

    Private Sub mArmarFechasRangoNacimiento(ByVal pdateFechaValor As Date, ByRef pdateFechaTope As Date, ByRef pdateFechaSobre As Date, ByVal pdateFechaNaci As Date)

        Dim lintAnio As Integer = 0

        If pdateFechaValor.Month < pdateFechaTope.Month Then
            pdateFechaTope = New Date(pdateFechaValor.Year - 1, pdateFechaTope.Month, pdateFechaTope.Day)
        Else
            pdateFechaTope = New Date(pdateFechaValor.Year, pdateFechaTope.Month, pdateFechaTope.Day)
        End If
        'calcular fecha de sobretasa: 6 meses a partir del final del mes del nacimiento.
        pdateFechaSobre = DateAdd("m", 6, pdateFechaNaci)
        pdateFechaSobre = New Date(pdateFechaSobre.Year, pdateFechaSobre.Month, mDiasDelMes(pdateFechaSobre))

    End Sub

    Public Function mDiasDelMes(ByVal mstrFecha As String) As Integer
        Dim lbooBisiesto As Boolean = Day(DateSerial(Year(mstrFecha), 3, 0)) = 29
        Select Case Month(mstrFecha)
            Case 2 : mDiasDelMes = IIf(lbooBisiesto, 29, 28)
            Case 1, 3, 5, 7, 8, 10, 12 : mDiasDelMes = 31
            Case 4, 6, 9, 11 : mDiasDelMes = 30
        End Select
    End Function

    Private Sub mArmarFechasRango(ByVal pdateFechaValor As Date, ByRef pdateFechaTope As Date, ByRef pdateFechaSobre As Date)

        Dim lintAnio As Integer = 0

        If pdateFechaValor.Month < pdateFechaTope.Month Then
            pdateFechaTope = New Date(pdateFechaValor.Year - 1, pdateFechaTope.Month, pdateFechaTope.Day)
        Else
            pdateFechaTope = New Date(pdateFechaValor.Year, pdateFechaTope.Month, pdateFechaTope.Day)
        End If

        If pdateFechaTope.Month > pdateFechaSobre.Month Then
            pdateFechaSobre = New Date(pdateFechaTope.Year + 1, pdateFechaSobre.Month, pdateFechaSobre.Day)
        Else
            pdateFechaSobre = New Date(pdateFechaTope.Year, pdateFechaSobre.Month, pdateFechaSobre.Day)
        End If
    End Sub

    Private Function maplicaSobretasaNormal(ByVal pintMin As Integer, ByVal pintMax As Integer, ByVal pintDif As Integer) As Boolean
        Dim lboolAgregarSobretasa As Boolean = False

        'si ninguno de los topes son nulos, y son de igual valor
        If pintMax <> 0 And pintMin <> 0 And pintMax = pintMin Then
            If pintDif = pintMax Then
                lboolAgregarSobretasa = True

            End If
        End If
        ' si ninguno de los topes son nulos, la sobretasa se aplica si la diferencia esta dentro del rango
        ' los valores no son iguales
        If pintMax <> 0 And pintMin <> 0 And pintMax <> pintMin Then
            If pintDif < pintMax And pintDif >= pintMin Then
                lboolAgregarSobretasa = True
            End If
        End If
        ' si solo esta el tope minino, la sobretasa se aplica si 
        ' la diferencia es mayor al valor minimo
        If pintMax = 0 And pintMin <> 0 Then
            If pintDif >= pintMin Then lboolAgregarSobretasa = True
        End If

        ' si solo esta el tope maximo, la sobretasa se aplica si
        ' la diferencia es menor al valor maximo
        If pintMin = 0 And pintMax <> 0 Then
            If pintDif < pintMax Then lboolAgregarSobretasa = True
        End If
        Return lboolAgregarSobretasa
    End Function
    Public Function DiferenciaEntreFechas(ByVal pstrFechaA As Date, ByVal pstrFechaB As Date, ByVal pintPerio As Integer) As Integer
        Return mDiferenciaEntreFechas(pstrFechaA, pstrFechaB, pintPerio)
    End Function
    Private Function mDiferenciaEntreFechas(ByVal pstrFechaRef As Date, ByVal pstrFechaValor As Date, ByVal pintPerio As Integer, Optional ByVal pintTiempo As Integer = 0) As Integer
        Dim lintDif As Long
        Dim ldateRef As Date = pstrFechaRef 'clsFormatear.gFormatFechaDateTime(pstrFechaRef)
        Dim ldateValor As Date = pstrFechaValor 'clsFormatear.gFormatFechaDateTime(pstrFechaValor)
        Select Case pintPerio
            Case TipoPeriodo.dia
                lintDif = DateDiff(DateInterval.Day, ldateRef, ldateValor)
            Case TipoPeriodo.mes
                'se toma un mes mas si el dia de de la fecha de referencia es menor a la de valor
                'ej: 01/01/06 - 03/02/06  = 2 meses 
                lintDif = DateDiff(DateInterval.Month, ldateRef, ldateValor)
                If ldateRef.Day < ldateValor.Day Then
                    lintDif = lintDif + 1
                End If
            Case TipoPeriodo.mesa�o
                'se toma para el calculo el mes sin importar la fecha 
                lintDif = DateDiff(DateInterval.Month, ldateRef, ldateValor)
                If ldateRef.Day < ldateValor.Day Then ' Dario 2023-02-15 1029 corrige error de mal control de fechas
                    lintDif = lintDif + 1
                End If
            Case TipoPeriodo.diaMes
                'suma x dias a la fecha de referencia y luego calcula la dif en meses estre la nueva fecha de ref y la de valor
                'Se calcula la nueva fecha de ref (fecha ref + x dias)
                ldateRef = DateAdd(DateInterval.Day, pintTiempo, ldateRef)
                'se calcula la dif en meses
                lintDif = DateDiff(DateInterval.Month, ldateRef, ldateValor)
                'se redondea hacia arriba (ej: 5 meses 3 dias = 6 meses)
                If ldateRef.Day < ldateValor.Day Then lintDif = lintDif + 1
            Case TipoPeriodo.mesMes
                'suma x meses a la fecha de referencia y luego calcula la dif en meses estre la nueva fecha de ref y la de valor
                'Se calcula la nueva fecha de ref (fecha ref + x meses)
                ldateRef = DateAdd(DateInterval.Month, pintTiempo, ldateRef)
                'se calcula la dif en meses
                lintDif = DateDiff(DateInterval.Month, ldateRef, ldateValor)
                'se redondea hacia arriba (ej: 5 meses 3 dias = 6 meses)
                If ldateRef.Day < ldateValor.Day Then lintDif = lintDif + 1
        End Select
        Return lintDif
    End Function
    Private Function mFormula(ByVal pstrConn As String, ByVal pintAranId As Integer) As Integer
        Return ObtenerValorCampo(pstrConn, "aranceles", "aran_form_id", pintAranId)
    End Function



#End Region

#Region "M�todos"
    Public Function CentroCostoDistDesc(ByVal pintCCos As Integer) As Boolean
        'devuelve verdadero si el cod del centro de costo empieza con '06'
        Dim lstrCosto As String = ObtenerValorCampo(mstrConn, "centrosc", "ccos_codi", pintCCos)
        lstrCosto = lstrCosto.Trim.Substring(0, 2)
        If lstrCosto = "06" Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Function ExistenItemsEnLaFactura(ByVal pdsDatos As DataSet) As Boolean
        Dim lboolRet As Boolean = False
        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            lboolRet = True
        Next
        Return lboolRet
    End Function
    Public Function ClienteTipoIva(ByVal pstrConn As String, ByVal pintValor As Integer) As Integer
        Dim lintIvaId As Integer = ObtenerValorCampo(pstrConn, "clientes", "clie_ivap_id", pintValor)
        Return lintIvaId
    End Function
    Public Function ClienteDiscrimIVA(ByVal pstrConn As String, ByVal pintValor As Integer)
        Dim lboolIvaId As Boolean = ObtenerValorCampo(pstrConn, "clientes_posic_iva", "ivap_discri", pintValor)
        Return lboolIvaId
    End Function

    Public Function SeleccionComprobantes() As String
        Dim lstrIDS As String = TiposComprobantes.Factura & "," & TiposComprobantes.Proforma
        Return lstrIDS
    End Function
    Public Function CantidadItems(ByVal pdsDatos As DataSet) As String

        Dim pdecCoti, pintMoneId, ldecCantTmp, ldecPrecUniTmp As Decimal
        Dim lstrCadena As String
        ' incompleto!!!!!!!
        'recorre la tabla temporal
        For Each ldr As DataRow In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Select
            If ldr.Item("temp_tipo") = "C" Then ' concepto
                If Not ldr.IsNull("temp_desc_ampl") Then
                End If
            End If

        Next
    End Function

    Public Function ExistenArancRRGGRepresAnim(ByVal pstrConn As String, ByVal pdsDatos As DataSet) As Boolean
        'Verifica si existe algun arancel (RRGG) que represente animales
        Dim lintRAnim As Boolean = False

        For Each ldr As DataRow In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Select("temp_tipo = 'A'")
            If ObtenerValorCampo(pstrConn, "aranceles", "aran_cant_anim", ldr.Item("temp_aran_concep_id")) Then lintRAnim = True
        Next
        Return lintRAnim
    End Function
    Public Function ExistenAutorizacionesSinConf(ByVal pdsDatos As DataSet) As Integer
        '31/08/06
        '1-devuelve que se conf todas 
        '0-quedan sin conf y no es la 7
        '2-conf todas menos "no tiene cta. cte." 
        '3-conf todas e incluye "no tiene cta. cte."
        '4-no hay autorizaciones
        Dim lintConf, lintSinConf, lintSinConfCtaCte, lintCtaCte As Boolean
        Dim lintAut As Integer = 1 ' si hay autorizaciones, devuelve como si se hubiesen conf
        'si no hay autorizaciones  
        If pdsDatos.Tables(tablasDs.mstrTablaAutor).Rows.Count = 0 Then
            Return 4
        End If
        For Each ldr As DataRow In pdsDatos.Tables(tablasDs.mstrTablaAutor).Select
            If ldr.Item("coau_auti_id") = 7 Then lintCtaCte = True
            If ldr.Item("coau_auto") = 0 Then
                If ldr.Item("coau_auti_id") = 7 Then
                    lintSinConfCtaCte = True
                Else
                    lintSinConf = True
                End If
            Else
                lintConf = True
            End If
        Next
        'existen sin confirmar 
        If lintSinConf Then
            lintAut = 0
        End If
        'conf todas menos "no tiene cta. cte."
        If lintSinConf = False And lintSinConfCtaCte Then
            lintAut = 2
        End If
        'conf todas 
        If lintConf And lintSinConfCtaCte = False And lintSinConf = False Then
            lintAut = 1
        End If
        'conf todas incluye "no tiene cta. cte."
        If lintConf And lintCtaCte And lintSinConf = False And lintSinConfCtaCte = False And lintAut <> 1 Then
            lintAut = 3
        End If
        Return lintAut
    End Function
    Public Function ExistenTurnosSinFacturar(ByVal pdsDatos As DataSet) As Boolean
        Dim lstrBov, lstrEqu, lstrTempBov, lstrTempEqu As Boolean

        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_turnos is not null and temp_borra is null"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            If ldr.Item("temp_turnos") = "B" Then lstrBov = True
            If ldr.Item("temp_turnos") = "E" Then lstrEqu = True
        Next

        'bovinos (verifica si hay turnos de bovinos sin facturar)
        If lstrBov Then
            lstrTempBov = mTurnoNoFactuardo(Tablas.inturfac_bov)
        End If

        'equinos (verifica si hay turnos de equinos sin facturar)
        If lstrEqu Then
            lstrTempEqu = mTurnoNoFactuardo(Tablas.inturfac_equ)
        End If

        ' existen turnos sin facturar
        If (lstrTempBov And lstrBov) Or (lstrTempEqu And lstrEqu) Then Return True

    End Function

    Private Function mTurnoNoFactuardo(ByVal pintTablaTurnos As Integer) As Boolean
        mdsDs.Tables(pintTablaTurnos).DefaultView.RowFilter = "cant <> 0"
        For Each ldr As DataRowView In mdsDs.Tables(pintTablaTurnos).DefaultView
            Return True
        Next
    End Function
    Public Function FilaDSTurnos(ByVal pdsDatos As DataSet, ByVal pintID As Integer) As System.Collections.ArrayList
        Dim lstrCampos As New System.Collections.ArrayList
        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_id=" & pintID
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            With ldr
                lstrCampos.Add(IIf(.Item("temp_turno_SP") Is DBNull.Value, 0, .Item("temp_turno_SP")))
                lstrCampos.Add(IIf(.Item("temp_turno_RT") Is DBNull.Value, 0, .Item("temp_turno_RT")))
                lstrCampos.Add(IIf(.Item("temp_turno_RT") Is DBNull.Value, 0, .Item("temp_turno_RTU")))
                lstrCampos.Add(IIf(.Item("temp_turno_RT") Is DBNull.Value, 0, .Item("temp_turno_DP")))
                lstrCampos.Add(IIf(.Item("temp_turno_RT") Is DBNull.Value, 0, .Item("temp_turno_DC")))
            End With
        Next
        Return lstrCampos
    End Function

    Public Function FilaDSTurnos(ByVal pintTablaTurnos As Integer, ByVal pintID As Integer) As System.Collections.ArrayList
        Dim lstrCampos As New System.Collections.ArrayList
        mdsDs.Tables(pintTablaTurnos).DefaultView.RowFilter = "id=" & pintID
        For Each ldr As DataRowView In mdsDs.Tables(pintTablaTurnos).DefaultView
            With ldr
                lstrCampos.Add(.Item("ID"))
                lstrCampos.Add(.Item("TURN_OTO"))
                lstrCampos.Add(.Item("TURN_REC"))
                lstrCampos.Add(.Item("FECHA_REC"))
                lstrCampos.Add(.Item("FECHA_COSE"))
                lstrCampos.Add(.Item("FECHA_FISE"))
                lstrCampos.Add(.Item("SOB_PFF"))
                lstrCampos.Add(.Item("REC_EFF"))
                lstrCampos.Add(.Item("DTO_OPP"))
                lstrCampos.Add(.Item("DTO_OPC"))
                lstrCampos.Add(.Item("REC_PTU"))
                lstrCampos.Add(.Item("REGICONT"))
                lstrCampos.Add(.Item("clcu_id"))
                lstrCampos.Add(.Item("cant"))
            End With
        Next
        Return lstrCampos
    End Function

    Public Function FilaDSTurnos(ByVal pstrConn As String, ByVal pintID As Integer) As System.Collections.ArrayList
        Dim lstrCampos As New System.Collections.ArrayList
        Dim lds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.proforma_aranceles_deta_consul @prad_pran_id= " & clsSQLServer.gFormatArg(pintID, SqlDbType.Int))
        For Each ldr As DataRow In lds.Tables(0).select
            With ldr
                lstrCampos.Add(.Item("prad_id"))
                lstrCampos.Add(.Item("prad_turn_otor"))
                lstrCampos.Add(.Item("prad_turn_reci"))
                lstrCampos.Add(.Item("prad_rece_fecha"))
                lstrCampos.Add(.Item("prad_cose_fecha"))
                lstrCampos.Add(.Item("prad_fise_fecha"))
                lstrCampos.Add(.Item("prad_sobr_paff"))
                lstrCampos.Add(.Item("prad_reca_turn"))
                lstrCampos.Add(.Item("prad_dcto_prom"))
                lstrCampos.Add(.Item("prad_dcto_cant"))
                lstrCampos.Add(.Item("prad_tram_urge"))
                lstrCampos.Add(.Item("prad_codi"))
                lstrCampos.Add(.Item("prad_cunica"))
                lstrCampos.Add(0) '.Item("cant")
            End With
        Next
        Return lstrCampos
    End Function
    Public Function CalcularTotales(ByVal pdsDatos As DataSet, ByVal pboolDiscrimIVA As Boolean, ByVal pboolSobretasaIVA As Boolean) As String
        Dim ldSubTotal, ldSubTotalXPil, ldTotal, ldTotalIVA, ldTotalIVAR, ldTotalIVAS, ldCant As Decimal
        Dim pdecCoti, pintMoneId, ldecCantTmp, ldecPrecUniTmp As Decimal

        'recorre la tabla temporal (solo las filas no borradas)
        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            'total de iva si discrimina y tiene iva el item, se acumula en esta variable
            Dim ldouTotalIVA As Double = 0
            If pboolDiscrimIVA Then
                ' If ldr.Item("temp_tasa_IVA") > 0 Then
                ldouTotalIVA = Math.Abs(CType(ldr.Item("temp_impo"), Double)) * (CType(ldr.Item("temp_tasa_IVA"), Double) / 100)
                ' para que reste el iva del arancel sin cargo
                If ldr.Item("temp_impo") < 0 Then ldouTotalIVA = ldouTotalIVA * -1
                'End If
                Select Case ldr.Item("temp_tipo_IVA")
                    Case TiposIVA.tasa
                        'importe iva
                        ldTotalIVA = ldTotalIVA + ldouTotalIVA
                    Case TiposIVA.reduc
                        ldTotalIVAR = ldTotalIVAR + ldouTotalIVA
                        'Case TiposIVA.sobre
                        '  ldTotalIVAS = ldTotalIVAS + ldouTotalIVA
                End Select
            End If
            ldTotal = ldTotal + ldr.Item("temp_impo")
        Next
        'total neto

        ' totales
        If pboolDiscrimIVA Then
            pTotalBrutoGral = SRA_Neg.Utiles.gRound2(ldTotal)
            pTotalIVAGral = SRA_Neg.Utiles.gRound2(ldTotalIVA)
            pTotalReduGral = SRA_Neg.Utiles.gRound2(ldTotalIVAR)
            If pboolSobretasaIVA Then 'el cliente es por ej incripto No responsable
                pTotalSobreTGral = SRA_Neg.Utiles.gRound2((pTotalIVAGral + pTotalReduGral) / 2)
            End If
            'si el iva es negativo 
            If pTotalIVAGral < 0 Then pTotalIVAGral = 0
            If pTotalReduGral < 0 Then pTotalReduGral = 0
            If pTotalSobreTGral < 0 Then pTotalSobreTGral = 0
            'total de IVA (iva o iva reducido) 
            'siempre alguno de los 2 es cero, se los suma solo para programar una sola linea  
            pTotalesIVA = SRA_Neg.Utiles.gRound2(pTotalIVAGral + pTotalReduGral)
            If pTotalesIVA < 0 Then pTotalesIVA = 0
        Else  ' si el cliente no discrimina IVA entonces el valor bruto es igual al total
            pTotalBrutoGral = SRA_Neg.Utiles.gRound2(ldTotal)
        End If
        pTotalNetoGral = SRA_Neg.Utiles.gRound2(pTotalBrutoGral + pTotalSobreTGral + pTotalReduGral + pTotalIVAGral)

    End Function
    Public Function SobretasasDelArancelNormal(ByVal pstrConn As String, ByVal pintAranId As Integer, ByVal pintActivId As Integer, ByVal pstrFechaIva As Date) As DataSet
        Dim ds As DataSet
        ' ds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.aranceles_sobretasas_consul " & clsSQLServer.gFormatArg(pintAranId, SqlDbType.Int) + "," + clsSQLServer.gFormatArg(pintActivId, SqlDbType.Int) + "," + clsSQLServer.gFormatArg(pstrFechaIva, SqlDbType.SmallDateTime))
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.aranceles_sobretasas_consul " & clsSQLServer.gFormatArg(pintAranId, SqlDbType.Int) + "," + clsSQLServer.gFormatArg(pintActivId, SqlDbType.Int) + "," + clsFormatear.gFormatFecha2DB(pstrFechaIva))

        pAranRRGGconSobretasaNormal = IIf(ds.Tables(0).Rows.Count > 0, True, False)
        mExistenciaDeSobretasas()
        Return ds
    End Function
    Public Function SobretasasDelArancelNormalconRango(ByVal pstrConn As String, ByVal pintAranId As Integer, ByVal pintActivId As Integer, ByVal pstrFechaIva As Date, ByVal pintMes As Integer) As DataSet
        ' sobretasas con rangos de fechas 
        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.aranceles_sobretasas_rango_consul " & clsSQLServer.gFormatArg(pintAranId, SqlDbType.Int) + "," + clsSQLServer.gFormatArg(pintActivId, SqlDbType.Int) + "," + clsSQLServer.gFormatArg(pintMes, SqlDbType.Int) + "," + clsFormatear.gFormatFecha2DB(pstrFechaIva))
        pAranRRGGconSobretasaRango = IIf(ds.Tables(0).Rows.Count > 0, True, False)
        mExistenciaDeSobretasas()
        Return ds
    End Function
    Public Function CortarCadena(ByVal pstrCadena As String, ByVal pintLargo As Integer)
        If pstrCadena.Length > 30 Then pstrCadena = pstrCadena.Substring(0, pintLargo)
        Return pstrCadena
    End Function
    Private Function mFormulaAplicaturnos(ByVal parrValores As System.Collections.ArrayList) As String
        Dim lstrIDS As String
        If parrValores.Count = 0 Then Return ""
        'sobretasa Palermo
        If parrValores.Item(CamposTurnosEnDeta.SOB_PFF) <> 0 Then
            lstrIDS = "9"
        End If
        'recargo
        If parrValores.Item(CamposTurnosEnDeta.REC_EFF) <> 0 Then
            If lstrIDS = "" Then
                lstrIDS = "10"
            Else
                lstrIDS = lstrIDS + ",10"
            End If
        End If
        'descuento promocion
        If parrValores.Item(CamposTurnosEnDeta.DTO_OPP) <> 0 Then
            If lstrIDS = "" Then
                lstrIDS = "11"
            Else
                lstrIDS = lstrIDS + ",11"
            End If
        End If
        'descuento cantidad
        If parrValores.Item(CamposTurnosEnDeta.DTO_OPC) <> 0 Then
            If lstrIDS = "" Then
                lstrIDS = "12"
            Else
                lstrIDS = lstrIDS + ",12"
            End If
        End If
        'tramite urgente
        If parrValores.Item(CamposTurnosEnDeta.REC_PTU) <> 0 Then
            If lstrIDS = "" Then
                lstrIDS = "17"
            Else
                lstrIDS = lstrIDS + ",17"
            End If
        End If
        Return lstrIDS
    End Function
    Public Function EsConceptoAutom(ByVal ldr As DataRow) As Boolean
        If ldr.Item("temp_tipo") = "C" And ldr.Item("temp_auto") = "1" Then
            Return True
        End If
    End Function
    ' Dario 2013-04-26 se agrega nuevo bool lboolPrecioSocio para el caso que dos socios uno es moroso
    '                  tiene que calcular con el precio adherente
    Public Function ConceptosAutoma(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pboolDiscrimIVA As Boolean, _
                                    ByVal pintActiId As Integer, ByVal lboolPrecioSocio As Boolean) As DataTable
        'concep automaticos por diferencia de precios (socio/No socio)
        Dim ds As DataSet
        Dim lDtAux As DataTable
        Dim ldouTotal, ldouTotalPSocio, ldouPSoci, ldouPNoSoci As Double
        Dim lstrConcepto As String

        'verifico que el cliente deber�a pagar a precio No Socio
        'If pEspecialesTodo(CamposEspeciales.precio_no_socio) = "No" Then Exit Function   29/12/2010

        'sumo los importes de los aranceles que deber�an cobrar a precio de no socio 
        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null and temp_tipo ='A'"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            If pboolDiscrimIVA Then
                'importe sin iva
                ldouPSoci = ldr.Item("temp_impo")
            Else
                'importe con iva
                ldouPSoci = ldr.Item("temp_impo_ivai")
            End If
            'solo acumula los precios donde hay un adicional (p No socio > p socio)
            Dim tmpPrecioValor As Double = 0

            ' Dario 2013-04-26 toma el valor para el calculo segun el precio declarado a aplicar
            'If (pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios) = pEspecialesTodo(CamposEspeciales.agrupCantMiembrosMorosos)) Then
            '    tmpPrecioValor = CType(ldr.Item("temp_impo_Nosocio_aran"), Double)
            'Else
            '    tmpPrecioValor = CType(ldr.Item("temp_impo_Adhe_aran"), Double) * ldr.Item("temp_cant")
            'End If

            Dim cantidadTotalMiembros As Integer = 0
            cantidadTotalMiembros = Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios)) _
                                  + Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosNoSocios)) _
                                  + Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSociosAdherentes))

            'Dim cantidadTotalMiembrosSociosyAdherentes As Integer = 0
            'cantidadTotalMiembrosSociosyAdherentes = Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios)) _
            '                                       + Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSociosAdherentes))

            'If (cantidadTotalMiembros > 1 And _
            '    Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios)) > 0 And _
            '    cantidadTotalMiembros <> Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios))) Then
            '    tmpPrecioValor = CType(ldr.Item("temp_impo_Adhe_aran"), Double) * ldr.Item("temp_cant")
            'End If

            If (cantidadTotalMiembros = 1) Then ' cliente solo
                If (Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosNoSocios)) = 1) Then ' cliente no socio precio no socio
                    tmpPrecioValor = CType(ldr.Item("temp_impo_Nosocio_aran"), Double)
                ElseIf (Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosMorosos)) = 1) Then ' cliente moroso precio no socio
                    tmpPrecioValor = CType(ldr.Item("temp_impo_Nosocio_aran"), Double)
                ElseIf (Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios)) = 1) Then ' cliente socio precio no socio
                    tmpPrecioValor = CType(ldr.Item("temp_impo_Nosocio_aran"), Double)
                ElseIf (Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSociosAdherentes)) = 1) Then ' cliente adherente precio no socio
                    tmpPrecioValor = CType(ldr.Item("temp_impo_Nosocio_aran"), Double)
                End If
            Else ' agrupacion
                If (cantidadTotalMiembros = Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios))) Then ' agrupo miembros todos socios
                    If (Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosMorosos)) = 0) Then ' y mosotos en 0 precio socio
                        tmpPrecioValor = CType(ldr.Item("temp_impo_Nosocio_aran"), Double)
                    Else ' con morosos igual a candidad de socios precio no socio 
                        If (Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios)) _
                           = Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosMorosos))) Then
                            tmpPrecioValor = CType(ldr.Item("temp_impo_Nosocio_aran"), Double)
                        Else ' socio o adherente diferente de cantidad de morosos mix
                            tmpPrecioValor = CType(ldr.Item("temp_impo_Adhe_aran"), Double) * ldr.Item("temp_cant")
                        End If
                    End If
                Else ' hay o no socios o adherentes precio no socio
                    tmpPrecioValor = CType(ldr.Item("temp_impo_Nosocio_aran"), Double)
                End If
            End If

            If ldouPSoci < tmpPrecioValor Then
                ldouTotal = ldouTotal + ldouPSoci
                ldouPNoSoci = tmpPrecioValor
                ' importe No socio
                ldouTotalPSocio = ldouTotalPSocio + ldouPNoSoci
            End If
        Next

        'total del concepto 
        ldouTotal = SRA_Neg.Utiles.gRound2(ldouTotalPSocio - ldouTotal)

        'cargo el concepto
        If ldouTotal > 0 Then
            lDtAux = mCargaDsConConcepAuto(pdsDatos, pstrConn, Formulas.AdicionalSuspensionServicios, ldouTotal, pintActiId)
        End If

        Return lDtAux

    End Function

    'Public Function ConceptosAutoma(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pboolDiscrimIVA As Boolean, ByVal pintActiId As Integer) As DataTable
    '   'concep automatico por deuda social vencida

    '   'verifico que tenga deuda social vencida
    '   If pEstadoSaldosTodo.Item(CamposEstado.saldo_social_venc) = 0 Then Exit Function

    '   Dim lDtAux As DataTable
    '   Dim ds As DataSet
    '   Dim ldouConv, ldouImpo As Double

    '   'sumo los importes de los aranceles  
    '   pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null and temp_tipo ='A'"
    '   For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
    '      If pboolDiscrimIVA Then
    '         'importe sin iva
    '         ldouImpo = ldouImpo + ldr.Item("temp_impo")
    '      Else
    '         'importe con iva
    '         ldouImpo = ldouImpo + ldr.Item("temp_impo_ivai")
    '      End If
    '   Next
    '   'total acumulado 
    '   ldouImpo = Math.Round(ldouImpo, 2)
    '   'coeficiente del convenio
    '   ldouConv = Math.Round(ldouImpo * ldoucoef, 2)
    '   'valor del concepto
    '   ldouImpo = Math.Round(ldouConv - ldouImpo, 2)

    '   'cargo el concepto
    '   If ldouImpo <> 0 Then
    '      lDtAux = mCargaDsConConcepAuto(pdsDatos, pstrConn, Formulas.AjustePorConvenio, ldouImpo, pintActiId)
    '   End If
    '   Return lDtAux

    'End Function
    Public Function ConceptosAutoma(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pboolDiscrimIVA As Boolean, ByVal pintActiId As Integer, ByVal pintConvId As Integer) As DataTable
        'concep automaticos por convenio

        'verifico que se haya seleccionado un convenio
        If pintConvId = 0 Then Exit Function

        Dim lDtAux As DataTable
        Dim ds As DataSet
        Dim ldouConv, ldouImpo As Double

        Dim ldoucoef As Double = ObtenerValorCampo(pstrConn, "convenios", "conv_coef", pintConvId)

        'sumo los importes de los aranceles  
        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null and temp_tipo ='A'"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            If pboolDiscrimIVA Then
                'importe sin iva
                ldouImpo = ldouImpo + ldr.Item("temp_impo")
            Else
                'importe con iva
                ldouImpo = ldouImpo + ldr.Item("temp_impo_ivai")
            End If
        Next
        'total acumulado 
        ldouImpo = SRA_Neg.Utiles.gRound2(ldouImpo)
        'coeficiente del convenio
        ldouConv = SRA_Neg.Utiles.gRound2(ldouImpo * ldoucoef)
        'valor del concepto
        ldouImpo = SRA_Neg.Utiles.gRound2(ldouConv - ldouImpo)

        'cargo el concepto
        If ldouImpo <> 0 Then
            lDtAux = mCargaDsConConcepAuto(pdsDatos, pstrConn, Formulas.AjustePorConvenio, ldouImpo, pintActiId)
        End If
        Return lDtAux

    End Function

    Private Function mCargaDsConConcepAuto(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintFormula As Integer, ByVal pdouImporte As Double, ByVal pintActiId As Integer) As DataTable
        Dim lDtAux As DataTable
        Dim lDrAux As DataRow
        Dim ds As DataSet
        Dim lstrConcepto As String

        'copia la estructura de la tabla de detalle 
        lDtAux = pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Clone
        'cargo un ds con el concepto  
        ds = mCrearDatasetConConceptos(pdsDatos, pstrConn, pintFormula, pintActiId)
        For Each ldr As DataRow In ds.Tables(0).Select
            lDrAux = lDtAux.NewRow
            With lDrAux
                lstrConcepto = ldr.Item("conc_desc")
                .Item("temp_aran_concep_id") = ldr.Item("conc_id")
                .Item("temp_codi_aran_concep") = ldr.Item("conc_codi")
                .Item("temp_desc_aran_concep") = ldr.Item("conc_desc")
                lstrConcepto = CortarCadena(lstrConcepto, 30)
                If pRRGG Then
                    lstrConcepto = "C" + "-" + Trim(ldr.Item("conc_codi")) + "-" + lstrConcepto
                    'lstrConcepto = "C" + "-" + lstrConcepto
                End If
                .Item("temp_desc_aran_concep_cortada") = lstrConcepto
                .Item("temp_tipo_IVA") = 0
                .Item("temp_impo") = pdouImporte
                .Item("temp_impo_ivai") = pdouImporte
                .Item("temp_tasa_iva") = 0
                .Item("temp_ccos_codi") = ObtenerValorCampo(pstrConn, "centrosc", "ccos_codi", ObtenerValorCampo(pstrConn, "actividades", "acti_ccos_id", pintActiId))
                .Item("temp_ccos_id") = ObtenerValorCampo(pstrConn, "actividades", "acti_ccos_id", pintActiId)
                .Item("temp_desc_ampl") = ""
                .Item("temp_auto") = 1 ' el concep, es automatico 
                .Item("temp_rela_id") = 0 'pintId 'relaciona el concepto con el arancel que lo genera (posicion en la grilla) 
            End With
            lDtAux.Rows.Add(lDrAux)
        Next
        Return lDtAux
    End Function

    Public Function ConceptosAutoma(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintId As Integer, ByVal pdoubValor As Double, ByVal pintCCosto As Integer, ByVal pintClieId As Integer) As DataTable
        'turnos ----concep automaticos
        Dim ds As DataSet
        Dim lDtAux As DataTable
        Dim lDrAux As DataRow
        Dim larrCampos As System.Collections.ArrayList
        Dim ldouImpoSIva, ldouImporteIva As Double
        Dim lstrFormulas As String
        Dim lbooDesc As Boolean
        Dim lintCCostoNeg As Integer
        Dim lstrConcepto As String
        Dim ldouImpoDescuentos As Double
        Dim lstrArancel As String

        'copia la estructura de la tabla de detalle 
        lDtAux = pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Clone
        For Each ldr As DataRow In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Select("")
            lstrArancel = ldr.Item("temp_aran_concep_id")
        Next

        'datos de la fila del arancel
        ' If pintTablaTurnos = 99 Then
        'cuando se recalcula los conceptos (proforma que se factura) 
        ' larrCampos = FilaDSTurnos(pstrConn, pintTurnosID)
        ' Else
        '    larrCampos = FilaDSTurnos(pintTablaTurnos, pintTurnosID)
        ' End If

        larrCampos = FilaDSTurnos(pdsDatos, pintId)


        'tomo la formula que debo aplicar
        lstrFormulas = mFormulaAplicaturnos(larrCampos)

        'cargo un ds con los conceptos  
        ds = mCrearDatasetConConceptos(pdsDatos, pstrConn, lstrFormulas)


        ' Modificacion Roxi: 05/06/2008
        ' primero los descuentos y despues los recargos porque los recargos son sobre
        ' el importe con los descuentos aplicados, no sobre el importe original.

        ' 1- DESCUENTOS
        ldouImpoDescuentos = 0
        For Each ldr As DataRow In ds.Tables(0).Select
            lbooDesc = FormulaDescuento(ldr.Item("conc_form_id").ToString)
            If lbooDesc Then
                lDrAux = lDtAux.NewRow
                With lDrAux
                    lstrConcepto = ldr.Item("conc_desc")
                    .Item("temp_aran_concep_id") = ldr.Item("conc_id")
                    .Item("temp_codi_aran_concep") = ldr.Item("conc_codi")
                    .Item("temp_desc_aran_concep") = ldr.Item("conc_desc")
                    lstrConcepto = CortarCadena(lstrConcepto, 30)
                    If pRRGG Then
                        lstrConcepto = "C" + "-" + Trim(ldr.Item("conc_codi")) + "-" + lstrConcepto
                        'lstrConcepto = "C" + "-" + lstrConcepto
                    End If
                    .Item("temp_desc_aran_concep_cortada") = lstrConcepto
                    .Item("temp_tipo_IVA") = ObtenerValorCampo(mstrConn, "conceptosX", "tipo_tasa", ldr.Item("conc_id").ToString)
                    ' importe sin iva
                    ldouImpoSIva = AplicarFormula(pdoubValor, ldr.Item("conc_form_id"), larrCampos)
                    ldouImporteIva = ldouImpoSIva * (CType(.Item("temp_tipo_IVA"), Double) / 100)
                    .Item("temp_impo") = ldouImpoSIva
                    'importe con iva
                    If ClienteDiscrimIVA(pstrConn, pintClieId) Then
                        .Item("temp_impo_ivai") = ldouImpoSIva + ldouImporteIva
                    Else
                        .Item("temp_impo_ivai") = ldouImpoSIva
                    End If
                    'importe tasa iva
                    .Item("temp_tasa_iva") = ldouImporteIva

                    If lstrArancel <> "" Then
                        lintCCostoNeg = ObtenerValorCampo(pstrConn, "arancelesX_nega", "aran_nega_ccos_id", "@aran_id=" & lstrArancel)
                    End If

                    .Item("temp_ccos_codi") = ObtenerValorCampo(pstrConn, "centrosc", "ccos_codi", lintCCostoNeg)
                    .Item("temp_ccos_id") = lintCCostoNeg
                    .Item("temp_desc_ampl") = ""
                    .Item("temp_auto") = 1 ' el concep, es automatico 
                    .Item("temp_rela_id") = pintId 'relaciona el concepto con el arancel que lo genera (posicion en la grilla) 
                End With
                lDtAux.Rows.Add(lDrAux)
                ldouImpoDescuentos = ldouImpoDescuentos + (ldouImpoSIva * -1)
            End If
        Next

        ' 2- RECARGOS
        For Each ldr As DataRow In ds.Tables(0).Select
            lbooDesc = FormulaDescuento(ldr.Item("conc_form_id").ToString)
            If Not lbooDesc Then
                lDrAux = lDtAux.NewRow
                With lDrAux
                    lstrConcepto = ldr.Item("conc_desc")
                    .Item("temp_aran_concep_id") = ldr.Item("conc_id")
                    .Item("temp_codi_aran_concep") = ldr.Item("conc_codi")
                    .Item("temp_desc_aran_concep") = ldr.Item("conc_desc")
                    lstrConcepto = CortarCadena(lstrConcepto, 30)
                    If pRRGG Then
                        lstrConcepto = "C" + "-" + Trim(ldr.Item("conc_codi")) + "-" + lstrConcepto
                        'lstrConcepto = "C" + "-" + lstrConcepto
                    End If
                    .Item("temp_desc_aran_concep_cortada") = lstrConcepto
                    .Item("temp_tipo_IVA") = ObtenerValorCampo(mstrConn, "conceptosX", "tipo_tasa", ldr.Item("conc_id").ToString)
                    ' importe sin iva
                    'ldouImpoSIva = AplicarFormula(pdoubValor, ldr.Item("conc_form_id"), larrCampos)
                    ldouImpoSIva = AplicarFormula(pdoubValor - ldouImpoDescuentos, ldr.Item("conc_form_id"), larrCampos)
                    ldouImporteIva = ldouImpoSIva * (CType(.Item("temp_tipo_IVA"), Double) / 100)
                    .Item("temp_impo") = ldouImpoSIva
                    'importe con iva
                    If ClienteDiscrimIVA(pstrConn, pintClieId) Then
                        .Item("temp_impo_ivai") = ldouImpoSIva + ldouImporteIva
                    Else
                        .Item("temp_impo_ivai") = ldouImpoSIva
                    End If
                    'importe tasa iva
                    .Item("temp_tasa_iva") = ldouImporteIva
                    .Item("temp_ccos_codi") = ObtenerValorCampo(pstrConn, "centrosc", "ccos_codi", pintCCosto)
                    .Item("temp_ccos_id") = pintCCosto
                    .Item("temp_desc_ampl") = ""
                    .Item("temp_auto") = 1 ' el concep, es automatico 
                    .Item("temp_rela_id") = pintId 'relaciona el concepto con el arancel que lo genera (posicion en la grilla) 
                End With
                lDtAux.Rows.Add(lDrAux)
            End If
        Next

        Return lDtAux

    End Function


    Public Function ConceptosAutoma(ByVal pstrConn As String, ByVal pdsDatos As DataSet, ByVal pintId As Integer, ByVal pdoubValor As Double, ByVal pintCCosto As Integer, ByVal pintFormula As Integer, ByVal pintClieId As Integer, ByVal pstrFechaIva As Date, ByVal pintAct As Integer) As DataTable
        'conceptos automaticos S/cargo
        Dim ds As DataSet
        Dim lDtAux As DataTable
        Dim lDrAux As DataRow
        Dim lstrConcepto As String
        Dim ldouImpoSIva, ldouImporteIva, ldouTasa As Double

        'copia la estructura de la tabla de detalle 
        lDtAux = pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Clone

        'cargo un ds con los conceptos  
        ds = mCrearDatasetConConceptos(pdsDatos, pstrConn, pintFormula, pintAct)

        'toma la fila con los datos del arancel
        Dim ldrAran As DataRow
        ldrAran = pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Select("temp_id=" & pintId)(0)
        '      With ldrAran
        '      txtCuotFecha.Fecha = .Item("covt_fecha")
        '      End With

        For Each ldr As DataRow In ds.Tables(0).Select
            lDrAux = lDtAux.NewRow
            With lDrAux
                lstrConcepto = ldr.Item("conc_desc")
                .Item("temp_aran_concep_id") = ldr.Item("conc_id")
                .Item("temp_codi_aran_concep") = ldr.Item("conc_codi")
                .Item("temp_desc_aran_concep") = ldr.Item("conc_desc")
                lstrConcepto = CortarCadena(lstrConcepto, 30)
                If pRRGG Then
                    lstrConcepto = "C" + "-" + Trim(ldr.Item("conc_codi")) + "-" + lstrConcepto
                    'lstrConcepto = "C" + "-" + lstrConcepto

                End If

                .Item("temp_desc_aran_concep_cortada") = lstrConcepto

                'los calculos de iva (tipo y valor tasa)son del arancel no del concepto (08/02/07)

                'iva Tasa
                .Item("temp_tipo_IVA") = ldrAran.Item("temp_tipo_IVA")
                ' importe sin iva
                ldouImpoSIva = AplicarFormula(pdoubValor)

                ldouImporteIva = 0
                If ldrAran.Item("temp_tasa_iva") > 0 Then
                    ldouImporteIva = SRA_Neg.Utiles.gRound2(ldouImpoSIva * (ldouTasa / 100))
                End If

                'importe tasa iva
                .Item("temp_tasa_iva") = ldrAran.Item("temp_tasa_iva")
                .Item("temp_impo") = ldouImpoSIva ' sin cargo
                'importe con iva
                If ClienteDiscrimIVA(pstrConn, pintClieId) Then
                    .Item("temp_impo_ivai") = ldouImpoSIva + ldouImporteIva
                Else
                    .Item("temp_impo_ivai") = ldouImpoSIva
                End If
                .Item("temp_ccos_codi") = ObtenerValorCampo(pstrConn, "centrosc", "ccos_codi", pintCCosto)
                .Item("temp_ccos_id") = pintCCosto
                .Item("temp_desc_ampl") = ""
                .Item("temp_auto") = 1 ' el desc, es automatico 
                .Item("temp_rela_id") = pintId 'relaciona el concepto con el arancel que lo genera (posicion en la grilla) 
            End With
            lDtAux.Rows.Add(lDrAux)
        Next
        Return lDtAux

    End Function


    Public Function Sobretasas(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintId As Integer, ByVal pintAranId As Integer, ByVal pintCant As Integer, ByVal pintActivId As Integer, ByVal pintFormaPagoId As Integer, ByVal pboolDiscrimIVA As Boolean, ByVal pstrFechaRef As Date, ByVal pstrFechaValor As Date, ByVal pstrFechaIva As Date, ByVal pstrListaId As String, ByVal pstrFechaNaci As String) As DataTable

        Dim ds As DataSet
        Dim lDtAux As DataTable
        Dim lDrAux As DataRow
        Dim lbooldif As Boolean = False
        Dim lintDif, lintMax, lintMinF, lintMaxF, lintPeri As Integer
        Dim ldouImpoIVA, ldouTasaIVA, ldouImporte As Double
        Dim lstrDesc As String
        Dim lboolAgregarSobretasa As Boolean
        Dim lintArancelSobretasaRango As Integer
        Dim lintAgregarSobretasaRango As Integer
        Dim lstrCmd As String = ""
        Dim lintFilaRango As Integer = 0

        'copia la estructura de la tabla de detalle 
        lDtAux = pdsDatos.Tables(tablasDs.mstrTablaTempDeta).Clone

        'cargo un ds con las sobretasas (normales y con rango de fechas) 
        ds = mCrearDatasetConSobretasas(pdsDatos, pstrConn, pintAranId, pintActivId, pstrFechaRef, pstrFechaIva)

        For Each ldr As DataRow In ds.Tables(0).Select()
            lboolAgregarSobretasa = False
            lintFilaRango = lintFilaRango + 1
            If ldr.IsNull("raad_tope_fecha") Then 'sobretasa normal
                '**********************************
                'obtiene la diferencia entre fechas
                '**********************************
                If ldr.IsNull("aran_rrgg_mmyy") OrElse ldr.Item("aran_rrgg_mmyy") = 0 Then
                    lintDif = mDiferenciaEntreFechas(pstrFechaRef, pstrFechaValor, ldr.Item("aran_rrgg_cper"))
                Else
                    ' calcula la diferencia de fechas tomando solo mes (no dias)
                    lintDif = mDiferenciaEntreFechas(pstrFechaRef, pstrFechaValor, TipoPeriodo.mesa�o)
                End If
                '*************************************************************************** 
                ' Si la diferencia se encuentra en el rango de calculo de la sobretasa
                ' entonces lo ingresa en el ds (sobretasas normales)
                '***************************************************************************

                lboolAgregarSobretasa = maplicaSobretasaNormal(IIf(ldr.IsNull("aran_rrgg_cmin"), 0, ldr.Item("aran_rrgg_cmin")), IIf(ldr.IsNull("aran_rrgg_cmax"), 0, ldr.Item("aran_rrgg_cmax")), lintDif)
                lintAgregarSobretasaRango = 0

            Else 'sobretasa rango
                lboolAgregarSobretasa = False

                Dim ldateValor As Date = pstrFechaValor 'clsFormatear.gFormatFechaDateTime(pstrFechaValor)
                Dim ldateFTope As Date = ldr.Item("raad_tope_fecha")
                Dim ldateFSobre As Date
                If Not ldr.IsNull("raad_sobre_fecha") Then
                    ldateFSobre = ldr.Item("raad_sobre_fecha")
                End If

                'toma dia y mes para el calculo

                If pstrFechaNaci = "" Then
                    pstrFechaNaci = pstrFechaRef
                End If

                'If Not ldr.IsNull("raad_sobre_fecha") Then
                '    mArmarFechasRango(ldateValor, ldateFTope, ldateFSobre)
                'Else
                '    mArmarFechasRangoNacimiento(ldateValor, ldateFTope, ldateFSobre, pstrFechaNaci)
                'End If

                'If pstrFechaNaci < ldateFTope And ldateValor < ldateFSobre And ldateValor < ldateFTope Then
                '   lintArancelSobretasaRango = ldr.Item("aran_id")
                'Else
                '   lintArancelSobretasaRango = 0
                'End If

                'If mSoloMesDia(ldateValor) > mSoloMesDia(ldateFTope) And mSoloMesDia(ldateValor) <= mSoloMesDia(ldateFSobre) Then
                'lboolAgregarSobretasa = True
                'End If

                lintArancelSobretasaRango = 0
                lintAgregarSobretasaRango = 0
                lstrCmd = "rangos_aran_fecha_consul "
                lstrCmd = lstrCmd & " @fecha_refe=" & clsSQLServer.gFormatArg(pstrFechaNaci, SqlDbType.SmallDateTime)
                lstrCmd = lstrCmd & ",@fecha_valor=" & clsSQLServer.gFormatArg(ldateValor, SqlDbType.SmallDateTime)
                lintArancelSobretasaRango = clsSQLServer.gCampoValorConsul(mstrconn, lstrCmd, "mes_mora")
            End If
            If lintArancelSobretasaRango <> 0 Then
                'If ds.Tables(0).Select("aran_id > " + lintArancelSobretasaRango.ToString()).Length = 0 Then
                '    ldr = ds.Tables(0).Select("aran_id=" + lintArancelSobretasaRango.ToString())(0)
                '    lintAgregarSobretasaRango = lintArancelSobretasaRango
                'End If
                If lintArancelSobretasaRango = lintFilaRango And lDtAux.Select("temp_aran_concep_id=" + ldr.Item("aran_id").ToString()).Length = 0 Then
                    lintAgregarSobretasaRango = ldr.Item("aran_id")
                End If
            End If
            If lboolAgregarSobretasa Or lintAgregarSobretasaRango <> 0 Then
                lDrAux = lDtAux.NewRow
                With lDrAux
                    lstrDesc = ldr.Item("aran_desc")
                    .Item("temp_tipo") = "S"
                    .Item("temp_aran_concep_id") = ldr.Item("aran_id")
                    .Item("temp_codi_aran_concep") = ldr.Item("aran_codi")
                    .Item("temp_desc_aran_concep") = lstrDesc
                    lstrDesc = "S" + "-" + Trim(.Item("temp_codi_aran_concep")) + "-" + CortarCadena(lstrDesc, 30)
                    'lstrDesc = "S" + "-" + CortarCadena(lstrDesc, 30)
                    .Item("temp_desc_aran_concep_cortada") = lstrDesc
                    .Item("temp_cant_sin_cargo") = 0
                    '****************************
                    'calcula el valor del arancel
                    '****************************
                    ldouImporte = ValorArancel(pstrConn, ldr.Item("aran_id"), pintActivId, pCotizaDolar, pintFormaPagoId, pstrListaId)
                    .Item("temp_unit_impo") = ldouImporte
                    '************************
                    ' aplica la formula
                    '************************
                    '*******************************************************************************************
                    'si la formula que utiliza es Formulas.PrecioXCantXDif, se pasa el valor  para el calculo
                    'de la diferencia entre fechas y TipoPeriodo.diaMes
                    If mFormula(pstrConn, ldr.Item("aran_id")) = Formulas.PrecioXCantXDif Then
                        If ldr.IsNull("aran_rrgg_cmin") Then
                            lintMax = 0
                        Else
                            lintMax = CType(ldr.Item("aran_rrgg_cmin"), Integer)
                            'esta linea es porque para saber si corresponde la sobretasa
                            'se verifica si la dif no es >= aran_rrgg_cmin
                            'pero para el calculo es necesario tomar aran_rrgg_cmin - 1
                            If lintMax > 1 Then lintMax = lintMax - 1
                        End If

                        If ldr.Item("aran_rrgg_cper") = TipoPeriodo.dia Then lintPeri = TipoPeriodo.diaMes
                        If ldr.Item("aran_rrgg_cper") = TipoPeriodo.mes Then lintPeri = TipoPeriodo.mesMes
                        lintDif = mDiferenciaEntreFechas(pstrFechaRef, pstrFechaValor, lintPeri, lintMax)
                        'para que no de negativa la sobretasa (sirve para cuando en el calculo de la fecha de ref supera a la de valor, al sumarle meses o dias )
                        If lintDif < 0 Then lintDif = 0
                        lbooldif = True
                    End If
                    '*******************************************************************************************

                    ldouImporte = AplicarFormula(pstrConn, ldr.Item("aran_id"), ldouImporte, pintCant, lintDif)
                    If pboolDiscrimIVA Then
                        ldouTasaIVA = ldr.Item("valor_tasa")
                        ldouImporte = SRA_Neg.Utiles.gRound2(ldouImporte)
                        ldouImpoIVA = (ldouImporte * (ldouTasaIVA / 100))
                        ldouImpoIVA = SRA_Neg.Utiles.gRound2(ldouImpoIVA)
                        ldouImporte = SRA_Neg.Utiles.gRound2(ldouImporte + ldouImpoIVA)
                    End If
                    .Item("temp_cant") = IIf(lbooldif = False, pintCant, pintCant * lintDif)
                    .Item("temp_impo") = (ldouImporte - ldouImpoIVA)
                    .Item("temp_tasa_iva") = ldouTasaIVA
                    .Item("temp_impo_ivai") = ldouImporte
                    .Item("temp_tipo_IVA") = ldr.Item("tipo_tasa")
                    .Item("temp_anim_no_fact") = 0
                    .Item("temp_fecha") = pstrFechaRef 'clsFormatear.gFormatFechaDateTime(pstrFechaRef)
                    .Item("temp_desc_fecha") = IIf(ldr.Item("aran_rrgg_desc_fech") Is DBNull.Value, "Fecha", ldr.Item("aran_rrgg_desc_fech"))
                    .Item("temp_inic_rp") = 0
                    .Item("temp_fina_rp") = 0
                    .Item("temp_tram") = 0
                    .Item("temp_acta") = 0
                    .Item("temp_ccos_codi") = ldr.Item("codi_cc")
                    .Item("temp_ccos_id") = ldr.Item("ccos_id")
                    .Item("temp_exen") = 0
                    .Item("temp_sin_carg") = 0
                    .Item("temp_rela_id") = pintId 'clsSQLServer.gFormatArg(pintAranId, SqlDbType.Int)
                End With
                lDtAux.Rows.Add(lDrAux)
            End If

        Next
        Return lDtAux
    End Function
    Public Function ConsultarProforma(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintProformaid As Integer) As DataSet
        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.proformas_consul " & clsSQLServer.gFormatArg(pintProformaid, SqlDbType.Int))
        ds.Tables(0).TableName = SRA_Neg.Constantes.gTab_Proformas
        ds.Tables(1).TableName = SRA_Neg.Constantes.gTab_ProformaDeta
        ds.Tables(2).TableName = SRA_Neg.Constantes.gTab_ProformaAranc
        ds.Tables(3).TableName = SRA_Neg.Constantes.gTab_ProformaConcep
        ds.Tables(4).TableName = SRA_Neg.Constantes.gTab_ProformaAutor

        Return ds
    End Function

    Public Function GuardarAutorizaciones(ByVal pintAuti_id As Integer, ByVal pintApro As Integer, ByVal pintAutoUserId As Integer, ByVal pintOpcion As Integer, ByVal pstrComen As String) As Integer
        Dim ldrDatos As DataRow
        Dim lintRet As Integer

        If CBool(pintApro) Then
            lintRet = Comprobantes.gObtenerAutorizaci�n(mstrConn, mstrUserId, pintAuti_id, pintOpcion, pintAutoUserId)
            pintApro = lintRet
        End If

        ldrDatos = mdsDatos.Tables(tablasDs.mstrTablaAutor).Select("coau_auti_id=" & pintAuti_id)(0)
        With ldrDatos
            .Item("coau_auto") = CBool(pintApro)
            If .Item("coau_auto") Then
                .Item("coau_user") = IIf(lintRet = 1, mstrUserId, pintAutoUserId)
            End If
            .Item("coau_obse") = IIf(pstrComen <> "", pstrComen, DBNull.Value)
        End With

        Return (lintRet)
    End Function

    Public Sub GuardarAutorizaciones(ByVal pdsDatos As DataSet, ByVal pgrdAutoriza As System.Web.UI.WebControls.DataGrid)
        Dim ldrDatos As DataRow
        Dim ldsDatosTmp As DataSet
        Dim lboolApro As Boolean
        Dim ltextObser As NixorControls.TextBoxTab

        For Each oDataItem As DataGridItem In pgrdAutoriza.Items
            If DirectCast(oDataItem.FindControl("chkApro"), CheckBox).Checked Then
                lboolApro = True
            Else
                lboolApro = False
            End If
            ltextObser = oDataItem.FindControl("txtComen")
            ldrDatos = pdsDatos.Tables(tablasDs.mstrTablaAutor).Select("coau_auti_id=" & oDataItem.Cells(1).Text)(0)
            With ldrDatos
                .Item("coau_auto") = lboolApro
                .Item("coau_obse") = ltextObser.Valor
            End With
        Next
    End Sub

    Public Function SeleccionaComprobante(ByVal pstrComprobante As String, ByVal pstrComprobSelec As String) As String
        Return SeleccionaComprobante(pstrComprobante, pstrComprobSelec, mdsdatos)
    End Function
    Public Function SeleccionaComprobante(ByVal pstrComprobante As String, ByVal pstrComprobSelec As String, ByVal pdsDatos As DataSet) As String
        Dim lintTipoComprob As Integer
        'si el total da cero, se genera una factura 
        ' Se comento el bloque IF porque no corresponde (16/05/2011)
        'If pTotalNetoGral = 0 Then
        '   ' If pstrComprobante = "Factura" Then  ' factura
        '   ' lintTipoComprob = TipoComprobGene.Factura
        '   'Else
        '   '  lintTipoComprob = TipoComprobGene.Proforma
        '   'End If
        '   pTipoComprobGenera = TipoComprobGene.Factura 'lintTipoComprob
        '   Return pTipoComprobGenera
        '   Exit Function
        'End If

        'el usuario selecciona manualmente el tipo de comprobante
        '061006 --se pueden generar proformas al contado
        If pstrComprobSelec <> "" Then
            If pstrComprobSelec = TiposComprobantes.Factura Then   ' factura
                lintTipoComprob = TipoComprobGene.Factura
            Else
                lintTipoComprob = TipoComprobGene.Proforma
            End If
            pTipoComprobGenera = lintTipoComprob
            Return pTipoComprobGenera
            Exit Function
        End If

        If pProforma And Not pProformaAutoExcSaldo And Not pProformaAuto Then
            If pstrComprobante = "Proforma" Then
                lintTipoComprob = TipoComprobGene.Proforma
            Else
                lintTipoComprob = TipoComprobGene.Factura
            End If
            pTipoComprobGenera = lintTipoComprob
            Return pTipoComprobGenera
            Exit Function
        End If

        If pProforma Then
            Dim ldouValor As Double
            'ver si excede el saldo sumandole el importe de la factura 
            pProformaAutoExcSaldo = False
            If (pEstadoSaldosTodo(CamposEstado.lim_saldo) <> "" And pEstadoSaldosTodo(CamposEstado.lim_saldo) <> "0") And (pEstadoSaldosTodo(CamposEstado.Saldo_excedido) <> "1") Then
                ldouValor = CType(pEstadoSaldosTodo(CamposEstado.lim_saldo), Double) - (pTotalNetoGral + CType(pEstadoSaldosTodo(CamposEstado.saldo_cta_cte), Double))
                If ldouValor < 0 Then
                    pProformaAutoExcSaldo = True
                End If
            End If
            If pProformaAutoExcSaldo Then
                lintTipoComprob = TipoComprobGene.ProformaExcede
            End If
            If pProformaAuto And Not pProformaAutoExcSaldo Then ' And Not TipoComprobGene.ProformaExcede Then
                lintTipoComprob = TipoComprobGene.ProformaAuto
                'conf todas menos "no tiene cta. cte."
                ' If mobj.ExistenAutorizacionesSinConf(mdsDatos) = 2 Then
                ' lintTipoComprob = TipoComprobGene.ProformaAuto
                'Else
                'el cliente no tiene cta cte pero esta conf la autorizacion
                If ExistenAutorizacionesSinConf(pdsDatos) = 3 Or ExistenAutorizacionesSinConf(pdsDatos) = 1 Then
                    lintTipoComprob = TipoComprobGene.Factura
                End If
                ' End If
                'el cliente no tiene cta cte pero esta conf la autorizacion
                ' If mobj.ExistenAutorizacionesSinConf(mdsDatos) = 3 Then
                ' lintTipoComprob = TipoComprobGene.ProformaAuto
                'Else
                '  lintTipoComprob = TipoComprobGene.Factura
                ' End If
            End If
            If Not pProformaAuto And Not pProformaAutoExcSaldo Then
                ' If pintComp = 0 Or pintComp = TiposComprobantes.Factura Then    ' factura
                ' lintTipoComprob = TipoComprobGene.Factura
                'Else
                lintTipoComprob = TipoComprobGene.Proforma
                'End If
            End If
        Else
            lintTipoComprob = TipoComprobGene.Factura
        End If

        pTipoComprobGenera = lintTipoComprob
        Return pTipoComprobGenera
    End Function

    Public Function ArancelesDeProforma(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintProformaid As Integer) As DataSet
        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.proforma_aranc_consul " & clsSQLServer.gFormatArg(pintProformaid, SqlDbType.Int))
        Return ds
    End Function

    Public Function TurnosFacturadosDT(ByVal pstrConn As String, ByVal pdsDatos As DataSet, ByVal pstrTabla As String) As DataSet
        Dim ldsDatos As New DataSet
        Dim ldrDatos As DataRow
        Dim larrTurnos As System.Collections.ArrayList
        Dim lintTablas As Integer
        Dim lstrPref As String = "coad"
        Dim lstrPrefCab As String = "coan"

        'cambia el prefijo segun si es proforma o comprobante
        If pstrTabla = SRA_Neg.Constantes.gTab_ProformaArancDeta Then
            lstrPref = "prad"
            lstrPrefCab = "pran"
        End If
        ldsDatos = clsSQLServer.gObtenerEstruc(pstrConn, pstrTabla, "")

        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            With ldr
                If .Item("temp_tipo") = "A" Then
                    ldrDatos = ldsDatos.Tables(0).NewRow()
                    If Not .Item("temp_turnos") Is DBNull.Value Then
                        If .Item("temp_turnos") = "B" Then
                            lintTablas = Tablas.inturfac_bov
                        Else
                            lintTablas = Tablas.inturfac_equ
                        End If
                        larrTurnos = FilaDSTurnos(lintTablas, .Item("temp_rela_turno_id"))
                        ldrDatos.Item(lstrPref + "_turn_otor") = larrTurnos.Item(CamposTurnos.TURN_OTO)
                        ldrDatos.Item(lstrPref + "_turn_reci") = .Item("temp_cant")

                        ldrDatos.Item(lstrPref + "_rece_fecha") = larrTurnos.Item(CamposTurnos.FECHA_REC)
                        ldrDatos.Item(lstrPref + "_cose_fecha") = larrTurnos.Item(CamposTurnos.FECHA_COSE)
                        ldrDatos.Item(lstrPref + "_fise_fecha") = larrTurnos.Item(CamposTurnos.FECHA_FISE)

                        ldrDatos.Item(lstrPref + "_sobr_paff") = larrTurnos.Item(CamposTurnos.SOB_PFF)

                        ldrDatos.Item(lstrPref + "_reca_turn") = larrTurnos.Item(CamposTurnos.REC_EFF)
                        ldrDatos.Item(lstrPref + "_dcto_prom") = larrTurnos.Item(CamposTurnos.DTO_OPP)

                        ldrDatos.Item(lstrPref + "_dcto_cant") = larrTurnos.Item(CamposTurnos.DTO_OPC)
                        ldrDatos.Item(lstrPref + "_tram_urge") = larrTurnos.Item(CamposTurnos.REC_PTU)
                        ldrDatos.Item(lstrPref + "_codi") = larrTurnos.Item(CamposTurnos.REGICONT)
                        ldrDatos.Item(lstrPref + "_cunica") = larrTurnos.Item(CamposTurnos.clcu_id)
                        ldrDatos.Item(lstrPref + "_tipo") = .Item("temp_turnos")

                    End If

                    ldrDatos.Item(lstrPref + "_id") = -1
                    ldrDatos.Item(lstrPref + "_" + lstrPrefCab + "_id") = -1 * IIf(.Item("temp_rela_turno_id") Is DBNull.Value, 1, .Item("temp_rela_turno_id"))

                    ldrDatos.Item(lstrPref + "_usua_sobr_paff") = .Item("temp_turno_SP")
                    ldrDatos.Item(lstrPref + "_usua_reca_turn") = .Item("temp_turno_RT")
                    ldrDatos.Item(lstrPref + "_usua_dcto_prom") = .Item("temp_turno_DP")
                    ldrDatos.Item(lstrPref + "_usua_dcto_cant") = .Item("temp_turno_DC")
                    ldrDatos.Item(lstrPref + "_usua_tram_urge") = .Item("temp_turno_RTU")
                    If Not (.Item("temp_turno_SP").ToString() = "" And .Item("temp_turno_RT").ToString() = "" _
                      And .Item("temp_turno_DP").ToString() = "" And .Item("temp_turno_DC").ToString() = "" _
                      And .Item("temp_turno_RTU").ToString() = "" And .Item("temp_turnos") Is DBNull.Value) Then
                        ldrDatos.Table.Rows.Add(ldrDatos)
                    End If
                End If
            End With
        Next

        ldsDatos.Tables(0).Rows.Remove(ldsDatos.Tables(0).Select("")(0))

        Return ldsDatos
    End Function

    ' Dario 2013-07-10 sobrecarga con transaccion 
    Public Function TurnosFacturadosDT(ByVal lTransac As SqlClient.SqlTransaction, ByVal pdsDatos As DataSet, ByVal pstrTabla As String) As DataSet
        Dim ldsDatos As New DataSet
        Dim ldrDatos As DataRow
        Dim larrTurnos As System.Collections.ArrayList
        Dim lintTablas As Integer
        Dim lstrPref As String = "coad"
        Dim lstrPrefCab As String = "coan"

        'cambia el prefijo segun si es proforma o comprobante
        If pstrTabla = SRA_Neg.Constantes.gTab_ProformaArancDeta Then
            lstrPref = "prad"
            lstrPrefCab = "pran"
        End If
        ldsDatos = clsSQLServer.gObtenerEstruc(lTransac.Connection, pstrTabla)

        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            With ldr
                If .Item("temp_tipo") = "A" Then
                    ldrDatos = ldsDatos.Tables(0).NewRow()
                    If Not .Item("temp_turnos") Is DBNull.Value Then
                        If .Item("temp_turnos") = "B" Then
                            lintTablas = Tablas.inturfac_bov
                        Else
                            lintTablas = Tablas.inturfac_equ
                        End If
                        larrTurnos = FilaDSTurnos(lintTablas, .Item("temp_rela_turno_id"))
                        ldrDatos.Item(lstrPref + "_turn_otor") = larrTurnos.Item(CamposTurnos.TURN_OTO)
                        ldrDatos.Item(lstrPref + "_turn_reci") = .Item("temp_cant")

                        ldrDatos.Item(lstrPref + "_rece_fecha") = larrTurnos.Item(CamposTurnos.FECHA_REC)
                        ldrDatos.Item(lstrPref + "_cose_fecha") = larrTurnos.Item(CamposTurnos.FECHA_COSE)
                        ldrDatos.Item(lstrPref + "_fise_fecha") = larrTurnos.Item(CamposTurnos.FECHA_FISE)

                        ldrDatos.Item(lstrPref + "_sobr_paff") = larrTurnos.Item(CamposTurnos.SOB_PFF)

                        ldrDatos.Item(lstrPref + "_reca_turn") = larrTurnos.Item(CamposTurnos.REC_EFF)
                        ldrDatos.Item(lstrPref + "_dcto_prom") = larrTurnos.Item(CamposTurnos.DTO_OPP)

                        ldrDatos.Item(lstrPref + "_dcto_cant") = larrTurnos.Item(CamposTurnos.DTO_OPC)
                        ldrDatos.Item(lstrPref + "_tram_urge") = larrTurnos.Item(CamposTurnos.REC_PTU)
                        ldrDatos.Item(lstrPref + "_codi") = larrTurnos.Item(CamposTurnos.REGICONT)
                        ldrDatos.Item(lstrPref + "_cunica") = larrTurnos.Item(CamposTurnos.clcu_id)
                        ldrDatos.Item(lstrPref + "_tipo") = .Item("temp_turnos")

                    End If

                    ldrDatos.Item(lstrPref + "_id") = -1
                    ldrDatos.Item(lstrPref + "_" + lstrPrefCab + "_id") = -1 * IIf(.Item("temp_rela_turno_id") Is DBNull.Value, 1, .Item("temp_rela_turno_id"))

                    ldrDatos.Item(lstrPref + "_usua_sobr_paff") = .Item("temp_turno_SP")
                    ldrDatos.Item(lstrPref + "_usua_reca_turn") = .Item("temp_turno_RT")
                    ldrDatos.Item(lstrPref + "_usua_dcto_prom") = .Item("temp_turno_DP")
                    ldrDatos.Item(lstrPref + "_usua_dcto_cant") = .Item("temp_turno_DC")
                    ldrDatos.Item(lstrPref + "_usua_tram_urge") = .Item("temp_turno_RTU")
                    If Not (.Item("temp_turno_SP").ToString() = "" And .Item("temp_turno_RT").ToString() = "" _
                      And .Item("temp_turno_DP").ToString() = "" And .Item("temp_turno_DC").ToString() = "" _
                      And .Item("temp_turno_RTU").ToString() = "" And .Item("temp_turnos") Is DBNull.Value) Then
                        ldrDatos.Table.Rows.Add(ldrDatos)
                    End If
                End If
            End With
        Next

        ldsDatos.Tables(0).Rows.Remove(ldsDatos.Tables(0).Select("")(0))

        Return ldsDatos
    End Function

    Public Function TurnosDTS(ByVal pstrConn As String, ByVal pstrOrigen As String) As Integer
        'pstrorigen = E (equino), B (bovino)

        Dim lstrCmd As String = "turnos_labo_pendientes_consul " & clsSQLServer.gFormatArg(pstrOrigen, SqlDbType.Text)
        Dim res As Integer = clsSQLServer.gExecuteScalarTrans(pstrConn, lstrCmd)

        If res = 1 Then ' operacion exitosa
            If pstrOrigen = "E" Then
                pTurnosDTSEqui = True
            Else
                pTurnosDTSBov = True
            End If
        End If
        Return res

    End Function
    Public Function FacExposicionesDTS(ByVal pstrConn As String) As Integer
        Dim lstrCmd As String = "facturas_exposicion_pendientes_consul"
        Dim res As Integer = clsSQLServer.gExecuteScalarTrans(pstrConn, lstrCmd)
        Return res
    End Function
    Public Function FacExposicionesPendientes(ByVal pstrConn As String, ByVal pintClieId As Integer, ByVal pintActivId As Integer) As DataTable
        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec facturacion_externa_pend_consul " & pintClieId & "," & pintActivId)
        Return ds.Tables(0)
    End Function
    Public Function TurnosPendientes(ByVal pintTablaTurnos As Integer) As DataTable
        Return mdsDs.Tables(pintTablaTurnos)
    End Function
    Public Function CantTotalTurnosFact(ByVal pdsDatos As DataSet, ByVal pintID As Integer, ByVal pstrOrigen As String) As Integer
        Return CantTotalTurnosFact(pdsDatos, pintID, pstrOrigen, False)
    End Function
    Public Function EjecutarTurnosDTS(ByVal pstrConn As String)
        If pTurnosDTSBov Then EjecutarTurnosDTS(pstrConn, "B")
        If pTurnosDTSEqui Then EjecutarTurnosDTS(pstrConn, "E")
    End Function
    Public Function EjecutarTurnosDTS(ByVal pstrConn As String, ByVal pstrOrigen As String)
        If TurnosDTS(mstrConn, pstrOrigen) <> 1 Then
            Throw New AccesoBD.clsErrNeg("Se produjo un error al intentar obtener los turnos de bovinos desde el archivo.")
        End If
    End Function
    'Public Function TurnosGrillaDS(ByVal pstrConn As String, ByVal pstrClieId As String, ByVal pstrOrigen As String) As DataTable
    '   Dim ldt As DataTable
    '   Dim lintinturfac_bov As Byte = 0
    '   Dim lintinturfac_equ As Byte = 1
    '   Select Case pstrOrigen
    '      Case "B"
    '       
    '         If Not pTurnosDTSBov Then
    '            'solo se ejecuta una vez
    '            Dim res As Integer = TurnosDTS(mstrConn, "B")
    '            If res <> 1 Then
    '               Throw New AccesoBD.clsErrNeg("Se produjo un error al intentar obtener los turnos de bovinos desde el archivo.")
    '            End If
    '            ldt = TurnosPendientes(mstrConn, pstrClieId, pstrOrigen)
    '         Else
    '            ldt = TurnosPendientes(lintinturfac_bov)
    '         End If

    '      Case "E"
    '         If Not pTurnosDTSEqui Then
    '            'solo se ejecuta una vez
    '            Dim res As Integer = TurnosDTS(mstrConn, "E")
    '            If res <> 1 Then
    '               Throw New AccesoBD.clsErrNeg("Se produjo un error al intentar obtener los turnos de equinos desde el archivo.")
    '            End If
    '            ldt = TurnosPendientes(pstrConn, pstrClieId, pstrOrigen)
    '         Else
    '            ldt = TurnosPendientes(lintinturfac_equ)
    '         End If
    '   End Select
    '   ldt.DefaultView.RowFilter = ""
    '   Return ldt
    'End Function
    Public Function TurnosGrillaDS(ByVal pstrConn As String, ByVal pstrClieId As String, ByVal pstrProfId As String, ByVal pstrOrigen As String) As DataTable
        Dim ldt As DataTable
        'bov = 0, equ = 1
        Return mTurnosGrillaDS(pstrConn, pstrClieId, pstrProfId, pstrOrigen, IIf(pstrOrigen = "B", 0, 1))
    End Function
    Private Function mTurnosGrillaDS(ByVal pstrConn As String, ByVal pstrClieId As String, ByVal pstrProfId As String, ByVal pstrOrigen As String, ByVal pbyteTablaTurnos As Byte) As DataTable
        Dim ldt As DataTable
        If Not pTurnosDTSBov Then
            EjecutarTurnosDTS(mstrConn, pstrOrigen)
            ldt = TurnosPendientes(mstrConn, pstrClieId, pstrProfId, pstrOrigen)
        Else
            ldt = TurnosPendientes(pbyteTablaTurnos)
        End If
        ldt.DefaultView.RowFilter = ""
        Return ldt
    End Function
    Public Function CantTotalTurnosFact(ByVal pdsDatos As DataSet, ByVal pintID As Integer, ByVal pstrOrigen As String, ByVal pboolModif As Boolean) As Integer
        'sumar la cantidad de turnos del mismo 'turno' facturados
        'recorre la tabla temporal (solo las filas no borradas)
        Dim ldouCantTotal As Double
        Dim lboolCalcular As Boolean = False

        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = "temp_borra is null"
        For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            With ldr
                If .Item("temp_tipo") = "A" And Not .Item("temp_turnos") Is DBNull.Value And Not .Item("temp_rela_turno_id") Is DBNull.Value AndAlso .Item("temp_rela_turno_id") = pintID Then
                    ldouCantTotal = ldouCantTotal + .Item("temp_cant")
                    lboolCalcular = True
                End If
            End With
        Next

        'modifica la cant restante del turno a facturar
        If Not lboolCalcular Then ldouCantTotal = 0

        Return TurnosPendientesModif(IIf(pstrOrigen = "B", Tablas.inturfac_bov, Tablas.inturfac_equ), ldouCantTotal, pintID)
    End Function
    Public Function TurnosCantFacturados(ByVal pdsDatos As DataSet, ByVal pintCantFact As Integer, ByVal pstrIdTemp As String, ByVal pintID As Integer, ByVal pstrOrigen As String) As Integer
        ' pintIdTemp ->  id del dt detalle (TablaTempDeta), para descartarlo en la suma
        ' pintCantFact -> la cant que se va a ingresar
        Dim lstrFiltro As String
        Dim lintCantTurno, lintCant As Integer
        Dim lintTablaTurnos As Integer = Tablas.inturfac_bov

        'si el item se esta editando, no se toma en la suma
        If pstrIdTemp = "" Then
            lstrFiltro = "temp_rela_id= " & pintID & " and temp_turnos= '" & pstrOrigen & "' and temp_borra is null"
        Else
            lstrFiltro = "temp_rela_id= " & pintID & " and temp_turnos= '" & pstrOrigen & "' and temp_id<> " & pstrIdTemp & " and temp_borra is null"
        End If
        If pstrOrigen = "E" Then lintTablaTurnos = Tablas.inturfac_equ
        ' acumula los turnos que fueron ingresados en la grilla, menos el que se esta editando
        pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView.RowFilter = lstrFiltro
        For Each ldrDeta As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
            lintCant = lintCant + IIf(ldrDeta.Item("temp_cant") Is DBNull.Value, 0, ldrDeta.Item("temp_cant"))
        Next
        'cantidad total a facturar del turno
        Dim ldr As DataRow = mdsDs.Tables(lintTablaTurnos).Select("id = " & pintID)(0)
        lintCantTurno = ldr.Item("TURN_REC")
        Return lintCantTurno - lintCant - pintCantFact

    End Function
    Public Function TurnosPendientesModif(ByVal pintTablaTurnos As Integer, ByVal pintCant As Integer, ByVal pintID As Integer) As Integer
        Dim ldr As DataRow
        Dim lintCant As Integer
        ldr = mdsDs.Tables(pintTablaTurnos).Select("id = " & pintID)(0)
        lintCant = ldr.Item("TURN_REC") - pintCant
        ldr.Item("cant") = lintCant
        Return lintCant
    End Function
    Public Function TurnosPendientes(ByVal pstrConn As String, ByVal pstrClieId As String, ByVal pstrProfId As String, ByVal pstrOrigen As String) As DataTable
        Dim ds As DataSet
        Dim lstrSP As String
        Dim ldrDatos As DataRow
        Dim lintTabla As Integer
        If pstrOrigen = "B" Then
            lstrSP = "exec dbo.inturfac_bov_consul "
            lintTabla = Tablas.inturfac_bov

        Else
            lstrSP = "exec dbo.inturfac_equ_consul "
            lintTabla = Tablas.inturfac_equ

        End If

        ds = clsSQLServer.gExecuteQuery(pstrConn, lstrSP & clsSQLServer.gFormatArg(pstrClieId, SqlDbType.Int) & ", " & clsSQLServer.gFormatArg(pstrProfId, SqlDbType.Int))

        For Each dr As DataRow In ds.Tables(0).Select
            With dr
                ldrDatos = mdsDs.Tables(lintTabla).NewRow()

                ldrDatos.Item("ID") = .Item("ID")
                ldrDatos.Item("TURN_REC") = .Item("TURN_REC")
                ldrDatos.Item("TURN_OTO") = .Item("TURN_OTO")
                ldrDatos.Item("FECHA_REC") = .Item("FECHA_REC")
                ldrDatos.Item("FECHA_COSE") = .Item("FECHA_COSE")
                ldrDatos.Item("FECHA_FISE") = .Item("FECHA_FISE")
                ldrDatos.Item("SOB_PFF") = .Item("SOB_PFF")
                ldrDatos.Item("REC_EFF") = .Item("REC_EFF")
                ldrDatos.Item("DTO_OPP") = .Item("DTO_OPP")
                ldrDatos.Item("DTO_OPC") = .Item("DTO_OPC")
                ldrDatos.Item("REC_PTU") = .Item("REC_PTU")
                ldrDatos.Item("REGICONT") = .Item("REGICONT")
                ldrDatos.Item("clcu_id") = .Item("clcu_id")
                ldrDatos.Item("cant") = .Item("cant")
                ldrDatos.Table.Rows.Add(ldrDatos)
            End With
        Next

        Return mdsDs.Tables(lintTabla)
    End Function
    Public Function ConceptosDeProforma(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintProformaid As Integer) As DataSet
        Dim ds As DataSet
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.proforma_concep_consul " & clsSQLServer.gFormatArg(pintProformaid, SqlDbType.Int))
        Return ds
    End Function
    Public Sub CargarAutorizaciones(ByVal pstrConn As String, ByVal pintClie As Integer, ByVal pstrFechaValor As String, ByVal pstrConv As String, ByVal pintListaPrecios As Integer, ByVal pintActiv As Integer)
        CargarAutorizaciones(mdsDatos, pstrConn, pintClie, pstrFechaValor, pstrConv, pintListaPrecios, pintActiv)
    End Sub
    Public Sub CargarAutorizaciones(ByVal pdsDatos As DataSet, ByVal pstrConn As String, ByVal pintClie As Integer, ByVal pstrFechaValor As String, ByVal pstrConv As String, ByVal pintListaPrecios As Integer, ByVal pintActiv As Integer)

        Dim filtro As String
        Dim ldrDatos As DataRow
        Dim ldsDatosAuto As DataSet
        Dim tabla As Integer

        Especiales(pstrConn, pintClie)
        EstadoSaldos(pstrConn, pintClie, clsFormatear.gFormatFechaDateTime(pstrFechaValor))
        If pstrConv = "" Then pstrConv = 0
        ConvSaldo(mstrConn, pstrConv)
        ldsDatosAuto = Autorizaciones(pstrConn, pdsDatos, pintClie, clsFormatear.gFormatFechaDateTime(pstrFechaValor), pintListaPrecios, pintActiv)

        For Each ldr As DataRow In ldsDatosAuto.Tables(0).Select
            With ldr
                'verifica que no este la ingresada la autorizacion
                filtro = "coau_auti_id = " + ldr.Item(0).ToString()
                If (pdsDatos.Tables(tablasDs.mstrTablaAutor).Select(filtro).GetLength(0) > 0) Then
                    ' If (mEstaEnElDataSet(mdsDatos.Tables(mstrTablaAutor), ldr, "coau_auti_id", "coau_auti_id")) Then
                Else
                    ' agrega la autotizacion 
                    ldrDatos = pdsDatos.Tables(tablasDs.mstrTablaAutor).NewRow
                    ldrDatos.Item("coau_auti_id") = .Item("id")
                    ldrDatos.Item("coau_auto") = 0
                    ldrDatos.Item("_auti_desc") = .Item("descr")
                    ldrDatos.Table.Rows.Add(ldrDatos)

                End If
            End With
        Next

        ' elimina las autorizaciones que no estan en el dr 

        For Each ldr As DataRow In pdsDatos.Tables(tablasDs.mstrTablaAutor).Select
            With ldr
                'verifica que este la autorizacion 
                filtro = "id = " + ldr.Item(2).ToString()
                If (ldsDatosAuto.Tables(0).Select(filtro).GetLength(0) > 0) Then
                Else
                    ldr.Delete()
                End If
            End With
        Next
    End Sub
    Public Sub Autorizaciones(ByVal pdsDatos As DataSet)
        pAutorizaciones = False
        For Each ldr As DataRow In pdsDatos.Tables(tablasDs.mstrTablaAutor).Select
            pAutorizaciones = True
        Next
    End Sub
    Public Function Autorizaciones(ByVal pstrConn As String, ByVal pdsDatos As DataSet, ByVal pClieId As Integer) As DataSet
        Return Autorizaciones(pstrConn, pdsDatos, pClieId, Today, 0, 0)
    End Function
    Public Function Autorizaciones(ByVal pstrConn As String, ByVal pdsDatos As DataSet, ByVal pClieId As Integer, ByVal pDateFechaValor As Date, ByVal pintListaPrecios As Integer, ByVal pintActiv As Integer) As DataSet
        Dim lstrIDS As String
        Dim ds As DataSet


        'creo una cadena con los ID de las autorizaciones

        If pConvSaldo Then
            If pEstadoSaldosTodo(CamposEstado.imp_a_cta_cta_cte) < pTotalNetoGral Then
                lstrIDS = "1"
            End If
        End If

        If mDescNoAuto(pstrConn, pdsDatos) Then
            If lstrIDS = "" Then
                lstrIDS = "2"
            Else
                lstrIDS = lstrIDS + ",2"
            End If

        End If

        If mFilaEliminada(pstrConn, pdsDatos, TipoItems.descuento) Then
            If lstrIDS = "" Then
                lstrIDS = "3"
            Else
                lstrIDS = lstrIDS.ToString + ",3"
            End If

        End If

        If DeudaVencida(pstrConn, pClieId) > 0 Then
            If lstrIDS = "" Then
                lstrIDS = "4"
            Else
                lstrIDS = lstrIDS.ToString + ",4"
            End If

        End If

        If mFactSinCargo(pstrConn, pdsDatos) Then
            If lstrIDS = "" Then
                lstrIDS = "5"
            Else
                lstrIDS = lstrIDS.ToString + ",5"
            End If

        End If

        If mFilaEliminada(pstrConn, pdsDatos, TipoItems.sobretasa) Then
            If lstrIDS = "" Then
                lstrIDS = "6"
            Else
                lstrIDS = lstrIDS.ToString + ",6"
            End If

        End If
        If pEstadoSaldosTodo(CamposEstado.lim_saldo) = "" And pPagoCtaCte Then
            ' el cliente no opera con cta.cte. y la act es rrgg o labo (pProforma=true)
            'If pProforma And pEstadoSaldosTodo(CamposEstado.lim_saldo) = "" Then

            If lstrIDS = "" Then
                lstrIDS = "7"
            Else
                lstrIDS = lstrIDS.ToString + ",7"
            End If
        End If

        If mFilaEliminada(pstrConn, pdsDatos, TipoItems.concepTurno) Then
            If lstrIDS = "" Then
                lstrIDS = "8"
            Else
                lstrIDS = lstrIDS.ToString + ",8"
            End If

        End If
        'proformas para facturar (paso los 30 dias entre la fecha de la factura y la proforma)
        If pProformaRecalcularaValores Then
            If pRecalculaValoresProforma = False Then
                If lstrIDS = "" Then
                    lstrIDS = "9"
                Else
                    lstrIDS = lstrIDS.ToString + ",9"
                End If
            End If
        End If

        'la lista de precios no coincide con la de la fecha 
        If pintListaPrecios <> 0 Then
            If mDistintaListaDePrecios(pstrConn, pDateFechaValor, pintListaPrecios, pintActiv) Then
                If lstrIDS = "" Then
                    lstrIDS = "12"
                Else
                    lstrIDS = lstrIDS.ToString + ",12"
                End If
            End If
        End If

        'sobretasas agregadas manualemente
        If mSobretasaAgregada(pstrConn, pdsDatos, TipoItems.sobretasa) Then
            If lstrIDS = "" Then
                lstrIDS = "14"
            Else
                lstrIDS = lstrIDS.ToString + ",14"
            End If

        End If

        If lstrIDS <> "" Then
            pAutorizaciones = True
        Else
            pAutorizaciones = False
        End If


        'ejecuto la consulta y cargo un DS para luego llenar la grilla
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec autoriza_tiposX_consul @auti_ids ='" & lstrIDS & "'")


        Return ds
    End Function

    Private Function mDistintaListaDePrecios(ByVal pstrConn As String, ByVal pDateFechaValor As Date, ByVal pintListaPrecios As Integer, ByVal pintActiv As Integer) As Boolean

        Dim lstrFiltro As String = clsFormatear.gFormatFecha2DB(pDateFechaValor) + "," + clsSQLServer.gFormatArg(pintActiv, SqlDbType.Int)
        Dim lstrLista As String = ObtenerValorCampo(pstrConn, "precios_lista_fact", "lista_id", lstrFiltro)
        Dim lintIdLista As Integer = 0
        If lstrLista <> "" Then
            lintIdLista = Convert.ToInt32(lstrLista)
        End If

        If lintIdLista <> pintListaPrecios Then
            Return True
        Else
            Return False
        End If

    End Function
    Public Function AutorizacionesNDNC(ByVal pstrConn As String, ByVal pdsDatos As DataSet, ByVal pdateFechaValor As Date, ByVal pintComprobanteAsoc As Integer, ByVal pintListaPrecios As Integer, ByVal pintActiv As Integer) As DataSet
        'autorizaciones para NC/ND
        'solo se pide autorizacion cuando no se referencia a un comprobante original
        Dim lstrIDS As String
        Dim ds As DataSet

        'creo una cadena con los ID de las autorizaciones
        If pintComprobanteAsoc = 0 Then
            lstrIDS = "10"
        End If

        If pintListaPrecios <> 0 Then
            If mDistintaListaDePrecios(pstrConn, pdateFechaValor, pintListaPrecios, pintActiv) Then
                If lstrIDS = "" Then
                    lstrIDS = "12"
                Else
                    lstrIDS = lstrIDS.ToString + ",12"
                End If
            End If
        End If


        If lstrIDS <> "" Then
            pAutorizaciones = True
        Else
            pAutorizaciones = False
        End If


        'ejecuto la consulta y cargo un DS para luego llenar la grilla
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec autoriza_tiposX_consul @auti_ids ='" & lstrIDS & "'")


        Return ds
    End Function

    Public Sub PagoCtaCte(ByVal pintTipoPago As Integer)
        pPagoCtaCte = False
        If pintTipoPago = TiposDePagos.CtaCte Then
            pPagoCtaCte = True
        End If
    End Sub
    Public Sub Proforma()
        'puede generar proforma si el tipo de pago es cta. cte.
        'la actividad es RRGG o Lab y no se trata de una proforma pendiente
        'que se selecciono para facturarla 
        '14/11/06 se agrego exposiciones
        pProforma = False
        If pPagoCtaCte And (pRRGG Or pLabora Or pExpo) And Not pProformaPend Then
            pProforma = True
        End If
    End Sub
    ' Dario 2013-08-08 Sobrecarga se le pasa el id de actividad
    Public Sub Proforma(ByVal pintActiv As Integer)
        'puede generar proforma si el tipo de pago es cta. cte.
        'la actividad es RRGG o Lab y no se trata de una proforma pendiente
        'que se selecciono para facturarla 
        '14/11/06 se agrego exposiciones
        pProforma = False

        ' Dario 2013-08-28 crea Objeto para obtener el dato de actividad permite proforma
        Dim _ActividadesBusiness As New Business.Facturacion.ActividadesBusiness
        Dim bitAdmiteProforma As Boolean = _ActividadesBusiness.GetAdmiteProforma(pintActiv)

        If pPagoCtaCte And bitAdmiteProforma And Not pProformaPend Then
            pProforma = True
        End If
    End Sub

    Public Function CambioActividad(ByVal pintActiv As Integer) As Boolean
        Dim lintActivAnterior As Integer = pActiv
        Actividad(pintActiv)
        If lintActivAnterior <> pActiv Then
            ' cambio la actividad
            Return True
        Else
            Return False
        End If

    End Function

    Public Sub RecalculaValoresProforma(ByVal pintValor As Integer)
        If pintValor = 1 Then
            pRecalculaValoresProforma = True
        Else
            pRecalculaValoresProforma = False
        End If
    End Sub
    Public Sub Actividad(ByVal pintActiv As Integer)
        pRRGG = False
        pAlum = False
        pVarios = False
        pLabora = False
        pExpo = False


        Select Case pintActiv
            Case Actividades.RRGG
                pRRGG = True
                pActiv = Actividades.RRGG
            Case Actividades.Laboratorio
                pLabora = True
                pActiv = Actividades.Laboratorio
            Case Actividades.Exposiciones
                pExpo = True
                pActiv = Actividades.Exposiciones
            Case Actividades.AlumnosCEIDA Or Actividades.AlumnosISEA
                pAlum = True
                pActiv = Actividades.AlumnosCEIDA ' alumnos, no importa si es CEIDA o ISEA
            Case Else
                pVarios = True
                pActiv = Actividades.Varios ' en realidad son todos los demas.. 
        End Select

    End Sub
    Public Function DistintoIva(ByVal pdsDatos As DataSet, ByVal pintiva As Integer) As Boolean
        'verifica que se este facturando con el mismo iva
        Dim lboolDif As Boolean = False
        Dim lintIva As Integer = 0
        If pintiva <> 0 Then
            For Each ldr As DataRowView In pdsDatos.Tables(tablasDs.mstrTablaTempDeta).DefaultView
                If Not ldr.Item("temp_tipo_IVA") Is DBNull.Value Then
                    If lintIva = 0 Then lintIva = ldr.Item("temp_tipo_IVA")
                End If
            Next
            If lintIva <> 0 And lintIva <> pintiva Then lboolDif = True
        End If
        Return lboolDif

    End Function

    Public Function Tarjetas(ByVal pstrCmd As String, ByVal pstrConn As String, ByVal pintRegiPosi As Integer) As Boolean
        Dim lintActiDefa
        lintActiDefa = clsSQLServer.gConsultarValor(pstrCmd, pstrConn, 0)
        pTarjetas = False
        If lintActiDefa <> "" Then
            pTarjetas = True
        End If
        Return pTarjetas
    End Function

    Public Function EstadoSaldos(ByVal pstrConn As String, ByVal pintClie As Integer, ByVal pFecha As Date) As String

        Dim lstrCampos As New System.Collections.ArrayList

        Dim lstrCmd = "exec clientes_estado_de_saldos_consul "
        lstrCmd += " " + clsSQLServer.gFormatArg(pintClie, SqlDbType.Int)
        lstrCmd += "," + clsFormatear.gFormatFecha2DB(pFecha)

        Dim myConnection As New System.Data.SqlClient.SqlConnection(pstrConn)
        Dim cmdExec = New System.Data.SqlClient.SqlCommand
        myConnection.Open()
        cmdExec.Connection = myConnection
        cmdExec.CommandType = CommandType.Text
        cmdExec.CommandText = lstrCmd
        Dim dr As System.Data.SqlClient.SqlDataReader = cmdExec.ExecuteReader()
        While (dr.Read())
            lstrCampos.Add(dr.GetValue(CamposEstado.saldo_cta_cte).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.saldo_social).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.saldo_total).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.imp_a_cta_cta_cte).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.imp_a_cta_cta_social).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.lim_saldo).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.color).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.Saldo_excedido).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.saldo_cta_cte_venc).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.saldo_social_venc).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.TotalProformas).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.OtrasCtas).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEstado.OtrasCtasVenc).ToString().Trim())

            pEstadoSaldos = dr.GetValue(CamposEstado.color).ToString().Trim()

        End While

        dr.Close()
        myConnection.Close()
        pEstadoSaldosTodo = lstrCampos
        Return pEstadoSaldos

    End Function
    Public Function OtrosDatosDelCliente(ByVal pstrConn As String, ByVal pintClie As Integer)

        Dim lstrCampos As New System.Collections.ArrayList

        Dim lstrCmd = "exec clientes_otros_datos_consul "
        lstrCmd = lstrCmd + " " + clsSQLServer.gFormatArg(pintClie, SqlDbType.Int)

        Dim myConnection As New System.Data.SqlClient.SqlConnection(pstrConn)
        Dim cmdExec = New System.Data.SqlClient.SqlCommand
        myConnection.Open()
        cmdExec.Connection = myConnection
        cmdExec.CommandType = CommandType.Text
        cmdExec.CommandText = lstrCmd
        Dim dr As System.Data.SqlClient.SqlDataReader = cmdExec.ExecuteReader()
        While (dr.Read())
            lstrCampos.Add(dr.GetValue(OtrosDatos.cuit).ToString().Trim())
            lstrCampos.Add(dr.GetValue(OtrosDatos.condIVA).ToString().Trim())
            lstrCampos.Add(dr.GetValue(OtrosDatos.categoria).ToString().Trim())
            lstrCampos.Add(dr.GetValue(OtrosDatos.legIsea).ToString().Trim())
            lstrCampos.Add(dr.GetValue(OtrosDatos.LegCeida).ToString().Trim())
            lstrCampos.Add(dr.GetValue(OtrosDatos.estado).ToString().Trim())
            lstrCampos.Add(dr.GetValue(OtrosDatos.NroSocio).ToString().Trim())
            lstrCampos.Add(dr.GetValue(OtrosDatos.legEgea).ToString().Trim())
        End While
        dr.Close()
        myConnection.Close()

        Return lstrCampos



    End Function
    Public Function CotizaDolar(ByVal pstrConn As String, ByVal pFecha As Object)
        pCotizaDolar = SRA_Neg.Comprobantes.gCotizaMoneda(pstrConn, pFecha)
    End Function
    Public Function PrecioSocio_Falle(ByVal pstrConn As String, ByVal pintClie As Integer, ByVal pstrfechaValor As Date)
        Dim lstrFiltro As String = clsSQLServer.gFormatArg(pintClie, SqlDbType.Int) + "," + clsFormatear.gFormatFecha2DB(pstrfechaValor)
        pPrecioSocio_falle = ObtenerValorCampo(pstrConn, "clientes_alertas_especiales", "falle_soci", lstrFiltro)
    End Function
    Public Function PrecioSocio_Trami(ByVal pstrConn As String, ByVal pintClie As Integer, ByVal pstrfechaValor As Date)
        Dim lstrFiltro As String = clsSQLServer.gFormatArg(pintClie, SqlDbType.Int) + "," + clsFormatear.gFormatFecha2DB(pstrfechaValor)
        pPrecioSocio_trami = ObtenerValorCampo(pstrConn, "clientes_alertas_especiales", "soli_tram", lstrFiltro)
    End Function

    Public Function Especiales(ByVal pstrConn As String, ByVal pintClie As Integer)
        Dim lstrCampos As New System.Collections.ArrayList

        Dim lstrCmd = "exec clientes_alertas_especiales_consul "
        lstrCmd = lstrCmd + " " + clsSQLServer.gFormatArg(pintClie, SqlDbType.Int)

        Dim myConnection As New System.Data.SqlClient.SqlConnection(pstrConn)
        Dim cmdExec = New System.Data.SqlClient.SqlCommand
        myConnection.Open()
        cmdExec.Connection = myConnection
        cmdExec.CommandType = CommandType.Text
        cmdExec.CommandText = lstrCmd
        Dim dr As System.Data.SqlClient.SqlDataReader = cmdExec.ExecuteReader()
        While (dr.Read())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.exento).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.iva).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.soci).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.precio_socio).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.miem).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.color).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.desc).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.socio_falle_precio_socio).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.soli_tram_precio_socio).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.obser).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.precio_no_socio).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.socio_adhe).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.cate_id).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.desc_tari).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.socio_adicional).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.categoIIBB).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.nroInsc).ToString().Trim())
            'Dario 2013-04-15 (agredado por precio unitario descriminado por la composicion de la agrup si corresponde
            lstrCampos.Add(dr.GetValue(CamposEspeciales.agrupCantMiembrosSocios).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.agrupCantMiembrosNoSocios).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.agrupCantMiembrosSociosAdherentes).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposEspeciales.agrupCantMiembrosMorosos).ToString().Trim())

            pEspeciales = dr.GetValue(CamposEspeciales.color).ToString().Trim()
        End While
        dr.Close()
        myConnection.Close()
        pEspecialesTodo = lstrCampos
        Return pEspeciales
    End Function

    Public Function ObtenerValorCampo(ByVal pstrConn As String, ByVal pstrTabla As String, ByVal pstrCampo As String, Optional ByVal pstrFiltro As String = "") As String

        Dim lDsDatos As DataSet
        If pstrFiltro = "" Then
            lDsDatos = clsSQLServer.gObtenerEstruc(pstrConn, pstrTabla)
        Else
            lDsDatos = clsSQLServer.gObtenerEstruc(pstrConn, pstrTabla, pstrFiltro)
        End If
        Dim lstrValor As String
        lstrValor = "0"
        If lDsDatos.Tables(0).Rows.Count > 0 Then
            lstrValor = IIf(lDsDatos.Tables(0).Rows(0).Item(pstrCampo) Is DBNull.Value, "0", lDsDatos.Tables(0).Rows(0).Item(pstrCampo))

        End If

        Return lstrValor
    End Function
    ' Dario 2013-07-10 sobrecarga con transaccion 
    Public Function ObtenerValorCampo(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTabla As String, ByVal pstrCampo As String, Optional ByVal pstrFiltro As String = "") As String

        Dim lDsDatos As DataSet
        If pstrFiltro = "" Then
            lDsDatos = clsSQLServer.gObtenerEstruc(lTransac.Connection, pstrTabla)
        Else
            lDsDatos = clsSQLServer.gObtenerEstruc(lTransac, pstrTabla, pstrFiltro)
        End If
        Dim lstrValor As String
        lstrValor = ""
        If lDsDatos.Tables(0).Rows.Count > 0 Then
            lstrValor = IIf(lDsDatos.Tables(0).Rows(0).Item(pstrCampo) Is DBNull.Value, "", lDsDatos.Tables(0).Rows(0).Item(pstrCampo))

        End If

        Return lstrValor
    End Function

    Public Function AranRRGG(ByVal pstrConn As String, ByVal pintAranId As Integer)
        Return AranRRGG(pstrConn, pintAranId, "")
    End Function
    Public Function AranRRGG(ByVal pstrConn As String, ByVal pintAranId As Integer, ByVal pintSobreAuto As String)
        Dim lstrCmd = "exec dbo.aranceles_consul "
        lstrCmd = lstrCmd + " " + clsSQLServer.gFormatArg(pintAranId, SqlDbType.Int)

        Dim myConnection As New System.Data.SqlClient.SqlConnection(pstrConn)
        Dim cmdExec = New System.Data.SqlClient.SqlCommand
        myConnection.Open()
        cmdExec.Connection = myConnection
        cmdExec.CommandType = CommandType.Text
        cmdExec.CommandText = lstrCmd
        Dim dr As System.Data.SqlClient.SqlDataReader = cmdExec.ExecuteReader()
        While (dr.Read())
            pAranRRGGDescFecha = dr.GetValue(CamposAranceles.aran_rrgg_desc_fech).ToString().Trim()
            pAranRRGGRepresAnim = dr.GetValue(CamposAranceles.aran_cant_anim).ToString().Trim()
            If pintSobreAuto = "" Or pintSobreAuto = "0" Then
                'la sobretasa No es automatica, por lo tanto funciona como un arancel normal (17/10/06)  
                pAranRRGGSobretasa = 0
            Else
                pAranRRGGSobretasa = IIf(dr.GetValue(CamposAranceles.aran_rrgg_aran_id).ToString().Trim() = "", 0, dr.GetValue(CamposAranceles.aran_rrgg_aran_id).ToString().Trim())
            End If
        End While
        dr.Close()
        myConnection.Close()
    End Function
    Public Function AranRRGGValidacionFecha(ByVal pstrConn As String, ByVal pintAranId As String, ByVal pFechaRef As String, ByVal pFechaValor As String)
        Return AranRRGGValidacionFecha(pstrConn, pintAranId, clsFormatear.gFormatFechaDateTime(pFechaRef), clsFormatear.gFormatFechaDateTime(pFechaValor))
        'Return AranRRGGValidacionFecha(pstrConn, pintAranId, pFechaRef, pFechaValor)
    End Function
    Public Function AranRRGGValidacionFecha(ByVal pstrConn As String, ByVal pintAranId As String, ByVal pFechaRef As Date, ByVal pFechaValor As Date)
        Dim lboolActa_Error As Boolean = False
        Dim lstrCampos As New System.Collections.ArrayList
        Dim ldifentreFechas, lintMax, lintMin As Integer
        Dim lstrCmd = "exec dbo.aranceles_consul "
        lstrCmd = lstrCmd + " " + clsSQLServer.gFormatArg(pintAranId, SqlDbType.Int)

        Dim myConnection As New System.Data.SqlClient.SqlConnection(pstrConn)
        Dim cmdExec = New System.Data.SqlClient.SqlCommand
        myConnection.Open()
        cmdExec.Connection = myConnection
        cmdExec.CommandType = CommandType.Text
        cmdExec.CommandText = lstrCmd
        Dim dr As System.Data.SqlClient.SqlDataReader = cmdExec.ExecuteReader()
        While (dr.Read())
            lstrCampos.Add(dr.GetValue(CamposAranceles.aran_rrgg_vmin).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposAranceles.aran_rrgg_vmax).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposAranceles.aran_rrgg_vper).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposAranceles.aran_rrgg_mmyy).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposAranceles.aran_rrgg_verror).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposAranceles.aran_val_fecha).ToString().Trim())
        End While
        dr.Close()
        myConnection.Close()
        pAranRRGGActa = False
        pAranRRGGError = False

        'si el periodo es nulo,toma mes
        If lstrCampos(2) = "" Then lstrCampos(2) = TipoPeriodo.mes

        If lstrCampos(3) = False Then
            'ldifentreFechas = mDiferenciaEntreFechas(clsFormatear.gFormatFechaDateTime(pFechaRef), clsFormatear.gFormatFechaDateTime(pFechaValor), lstrCampos(2))
            ldifentreFechas = mDiferenciaEntreFechas(pFechaRef, pFechaValor, lstrCampos(2))
        Else
            ' calcula la diferencia de fechas tomando solo mes y a�o (no dias)
            ldifentreFechas = mDiferenciaEntreFechas(pFechaRef, pFechaValor, TipoPeriodo.mesa�o)
        End If

        'minimo
        If lstrCampos(0) = "" Then
            lintMin = 0
        Else
            lintMin = CType(lstrCampos(0), Integer)
        End If
        'maximo
        If lstrCampos(1) = "" Then
            lintMax = 0
        Else
            lintMax = CType(lstrCampos(1), Integer)
        End If

        'si los extremos son iguales
        If lintMax <> 0 And lintMin <> 0 And lintMax = lintMin Then
            If ldifentreFechas <> lintMax Then
                lboolActa_Error = True
            End If
        End If

        'fuera de rango 
        If lintMax <> 0 And lintMin <> 0 And lintMax <> lintMin Then
            If ldifentreFechas > lintMax Or ldifentreFechas < lintMin Then
                lboolActa_Error = True
            End If
        End If

        'fuera de un maximo 
        If lintMax <> 0 And lintMin = 0 Then
            If ldifentreFechas > lintMax Then
                lboolActa_Error = True
            End If
        End If

        'fuera de un minimo 
        If lintMax = 0 And lintMin <> 0 Then
            If ldifentreFechas < lintMin Then
                lboolActa_Error = True
            End If
        End If

        'error o acta 
        If lstrCampos(4) Then
            pAranRRGGError = lboolActa_Error
        Else
            pAranRRGGActa = lboolActa_Error
        End If

        ''No validar fecha de acta
        Select Case lstrCampos(5)
            Case "1"    'fecha valor supera 4 meses
                If mDiferenciaEntreFechas(pFechaRef, pFechaValor, TipoPeriodo.mesa�o) > 4 Then
                    pAranRRGGActa = True
                End If
            Case "2"    '11 meses
                If mDiferenciaEntreFechas(pFechaRef, pFechaValor, TipoPeriodo.mesMes) > 11 Then pAranRRGGActa = True
            Case "3"    '90 dias
                If mDiferenciaEntreFechas(pFechaRef, pFechaValor, TipoPeriodo.dia) > 90 Then pAranRRGGActa = True

            Case "4"    '12 meses
                If mDiferenciaEntreFechas(pFechaRef, pFechaValor, TipoPeriodo.mesMes) > 12 Then
                    If pFechaValor >= pFechaRef Then
                        pAranRRGGActa = True
                    End If
                End If
            Case "5"    '10 meses
                If mDiferenciaEntreFechas(pFechaRef, pFechaValor, TipoPeriodo.mesMes) > 10 Then
                    If pFechaValor >= pFechaRef Then
                        pAranRRGGActa = True
                    End If
                End If
            Case "6"
                If mDiferenciaEntreFechas(pFechaRef, pFechaValor, TipoPeriodo.mesMes) > 12 Then pAranRRGGActa = True

            Case "8"    '18 meses
                If mDiferenciaEntreFechas(pFechaRef, pFechaValor, TipoPeriodo.mesMes) > 18 Then
                    If pFechaValor >= pFechaRef Then
                        pAranRRGGActa = True
                    End If
                End If
            Case "0", "10", "11", "12"
                pAranRRGGActa = False
            Case Else
                pAranRRGGActa = False
        End Select


        'If lstrCampos(5) = "10" Or lstrCampos(5) = "11" Or lstrCampos(5) = "12" Then
        '    pAranRRGGActa = False
        'End If



    End Function



    Public Function AplicarFormula(ByVal pdoubImporte As Double) As Double
        ' PARA CONCEPTOS AUTOMATICOS POR ARANCEL SIN CARGO
        Return (pdoubImporte * -1)

    End Function
    Public Function FormulaDescuento(ByVal pintFormulaId As Integer) As Boolean
        ' DETERMINA SI LA FORMULA ES DESCUENTO O RECARGO
        Select Case pintFormulaId
            Case Formulas.SobretasaPalermo
                FormulaDescuento = False
            Case Formulas.RecargoFueraDeFecha
                FormulaDescuento = False
            Case Formulas.DtoXPromoLaboratorio
                FormulaDescuento = True
            Case Formulas.DescuentoXCantidad
                FormulaDescuento = True
            Case Formulas.RecargoXTramiteUrgente
                FormulaDescuento = False
        End Select

    End Function
    Public Function AplicarFormula(ByVal pdoubTotal As Double, ByVal pintFormulaId As Integer, ByVal parrFila As System.Collections.ArrayList) As Double
        ' PARA CONCEPTOS AUTOMATICOS POR ARANCEL DE TURNOS (laboratorio)
        Dim ldouCoef As Double

        Select Case pintFormulaId
            Case Formulas.SobretasaPalermo
                ldouCoef = parrFila.Item(CamposTurnosEnDeta.SOB_PFF)
            Case Formulas.RecargoFueraDeFecha
                ldouCoef = parrFila.Item(CamposTurnosEnDeta.REC_EFF)
            Case Formulas.DtoXPromoLaboratorio
                ldouCoef = parrFila.Item(CamposTurnosEnDeta.DTO_OPP)
            Case Formulas.DescuentoXCantidad
                ldouCoef = parrFila.Item(CamposTurnosEnDeta.DTO_OPC)
            Case Formulas.RecargoXTramiteUrgente
                ldouCoef = parrFila.Item(CamposTurnosEnDeta.REC_PTU)
        End Select
        'el 2 se toma como 100%
        If ldouCoef = 2 Or ldouCoef = 1 Then
            ' calcula el 100%
            Return pdoubTotal
        Else
            If ldouCoef < 1 Then
                Return (pdoubTotal * ldouCoef) * -1
            Else
                Return (pdoubTotal * ldouCoef) - pdoubTotal
            End If
        End If

    End Function

    Public Function AplicarFormula(ByVal pstrConn As String, ByVal pintAranId As Integer, ByVal pdoubImporte As Double, ByVal pintCant As Integer, Optional ByVal pintDif As Integer = 0) As Double
        Dim lintFormula As Integer = mFormula(pstrConn, pintAranId)
        Dim ldoubImporte As Double
        Select Case lintFormula
            Case Formulas.PrecioXCant
                ldoubImporte = (pdoubImporte * pintCant)
            Case Formulas.PrecioXCantXDif
                ldoubImporte = (pdoubImporte * pintCant) * pintDif
        End Select
        Return ldoubImporte
    End Function
    '-------------------------------------------------------------------------------------------------------
    ' Dario 2013-04-16 (funcion que retorna el valor de importe calculado prorateado por la integracion 
    '                   de la agrupacion)
    '-------------------------------------------------------------------------------------------------------
    Private Function ValorArencelDiscriminado(ByVal lstrCampos As System.Collections.ArrayList, ByVal boolAdhe As Boolean _
                                              , ByVal doubCantidadSocio As Double, ByVal doubCantidadNoSocio As Double _
                                              , ByVal doubCantidadSocioAdherente As Double) As Double
        ' variables
        Dim respuesta As Double = 0
        Dim valorSocio As Double = 0
        Dim valorNoSocio As Double = 0
        Dim valorSocioAdherente As Double = 0
        Dim cantMiembros As Double = doubCantidadSocio + doubCantidadNoSocio + doubCantidadSocioAdherente

        ' calculo prorateado
        ' % valor socio
        valorSocio = (lstrCampos(CamposListaPreciosAran.impo_soci) / cantMiembros) * doubCantidadSocio

        ' si es el bool adherente true calculo no socios y adherentes por separado
        ' si es false calculo el valor sumados los grupos no socios y adherentes
        If (boolAdhe = False) Then
            ' % valor no socio + adherente
            valorNoSocio = (lstrCampos(CamposListaPreciosAran.impo_noso) / cantMiembros) * (doubCantidadNoSocio + doubCantidadSocioAdherente)
        Else
            ' % valor no socio solo 
            valorNoSocio = (lstrCampos(CamposListaPreciosAran.impo_noso) / cantMiembros) * doubCantidadNoSocio
            ' % valor socio adherente
            valorSocioAdherente = (lstrCampos(CamposListaPreciosAran.impo_adhe) / cantMiembros) * doubCantidadSocioAdherente
        End If

        ' sumo todas las partes y retorno el valor compuesto 
        respuesta = valorSocio + valorNoSocio + valorSocioAdherente

        Return respuesta
    End Function


    Public Function ValorArancel(ByVal pstrConn As String, ByVal pintAranId As Integer, ByVal pintActiId As Integer, ByVal pdouCotiDolar As Double, ByVal pintTipoPago As Integer, ByVal pstrPeliId As Integer)
        Dim lstrCampos As New System.Collections.ArrayList
        Dim ldoubValor As Double
        Dim bitAplicoCalculoDiscriminado As Boolean = False
        ' Dario 2013-05-17 
        ' nueva variable para aplicar precio adherente cuando el arancel es del grupo 8 varios 
        ' aves, canarios, gatos, conejos etc.
        Dim bitAplicAdherente As Boolean = False
        ' fin 2013-05-17

        Dim lstrCmd = "exec dbo.precios_aranX_consul "
        lstrCmd = lstrCmd + " @aran_id=" + clsSQLServer.gFormatArg(pintAranId, SqlDbType.Int)
        lstrCmd = lstrCmd + ", @acti_id=" + clsSQLServer.gFormatArg(pintActiId, SqlDbType.Int)
        lstrCmd = lstrCmd + ", @peli_id=" + clsSQLServer.gFormatArg(pstrPeliId, SqlDbType.Int)

        'lstrCmd = lstrCmd + ", @peli_vige_fecha=" + clsFormatear.gFormatFecha2DB(pstrFechaVige)
        'clsSQLServer.gFormatArg(pstrFechaVige, SqlDbType.SmallDateTime)

        Dim myConnection As New System.Data.SqlClient.SqlConnection(pstrConn)
        Dim cmdExec = New System.Data.SqlClient.SqlCommand
        myConnection.Open()
        cmdExec.Connection = myConnection
        cmdExec.CommandType = CommandType.Text
        cmdExec.CommandText = lstrCmd
        Dim dr As System.Data.SqlClient.SqlDataReader = cmdExec.ExecuteReader()

        While (dr.Read())
            lstrCampos.Add(dr.GetValue(CamposListaPreciosAran.aran_id).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposListaPreciosAran.impo_soci).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposListaPreciosAran.impo_noso).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposListaPreciosAran.impo_adhe).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposListaPreciosAran.mone_id).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposListaPreciosAran.reca_adhe).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposListaPreciosAran.reca_noso).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposListaPreciosAran.reca_ctac).ToString().Trim())
            lstrCampos.Add(dr.GetValue(CamposListaPreciosAran.reca_tarj).ToString().Trim())
            bitAplicAdherente = Convert.ToBoolean(dr.GetValue(CamposListaPreciosAran.aplicAdhe))
        End While

        dr.Close()
        myConnection.Close()

        'estaba renombrado, y tiraba error, 13/12/2021
        If lstrCampos.Count = 0 Then
            Return ldoubValor
        End If

        'verifica si la actividad permite precio adherente
        Dim lboolAdhe As Boolean = ObtenerValorCampo(pstrConn, "actividades", "acti_adhe", pintActiId)

        ' Dario - 2013-04-15 (cambio por calculo discriminado de impote de acuerdo a la composicion de la agrupacion)
        ' verifica si la actividad tiene que hacer el calculo discriminado del importe 
        ' de acuerdo a las partes que componen la agrupacion si es ese el caso
        Dim lboolDiscrpreciounit As Boolean = ObtenerValorCampo(pstrConn, "actividades", "acti_discriminapreciounitario", pintActiId)

        'el valor puede ser pasado por parametro, por lo tanto no se calcula su origen
        'ej para concep autom por precio de No socio
        If pPrecioNosocio Then
            'precio no socio
            ldoubValor = lstrCampos(CamposListaPreciosAran.impo_noso)
            bitAplicoCalculoDiscriminado = True
        Else
            ''-----------------------------------------------
            '' 1) tomar el importe del arancel segun corresponda
            ''----------------------------------------------
            'If pEspecialesTodo(CamposEspeciales.soci) = "Si" Then
            '    If pEspecialesTodo(CamposEspeciales.precio_no_socio) = "Si" Then
            '        If pEspecialesTodo(CamposEspeciales.socio_adhe) = "Si" And lboolAdhe Then
            '            ldoubValor = lstrCampos(CamposListaPreciosAran.impo_adhe)
            '        Else
            '            ldoubValor = lstrCampos(CamposListaPreciosAran.impo_noso)
            '            bitAplicoCalculoDiscriminado = True
            '        End If
            '    Else
            '        ldoubValor = lstrCampos(CamposListaPreciosAran.impo_soci)
            '    End If
            'Else
            '    If pEspecialesTodo(CamposEspeciales.precio_no_socio) = "Si" Then
            '        If pEspecialesTodo(CamposEspeciales.soli_tram_precio_socio) Then
            '            ldoubValor = lstrCampos(CamposListaPreciosAran.impo_soci)
            '        Else
            '            If pEspecialesTodo(CamposEspeciales.precio_socio) = "Si" Then
            '                ldoubValor = lstrCampos(CamposListaPreciosAran.impo_soci)
            '            Else
            '                If pEspecialesTodo(CamposEspeciales.socio_adhe) = "Si" And lboolAdhe Then
            '                    ldoubValor = lstrCampos(CamposListaPreciosAran.impo_adhe)
            '                Else
            '                    ldoubValor = lstrCampos(CamposListaPreciosAran.impo_noso)
            '                    bitAplicoCalculoDiscriminado = True
            '                End If
            '            End If
            '        End If
            '    Else
            '        ldoubValor = lstrCampos(CamposListaPreciosAran.impo_soci)
            '    End If
            'End If
            '-----------------------------------------------
            ' 1) tomar el importe del arancel segun corresponda
            '----------------------------------------------
            If pEspecialesTodo(CamposEspeciales.soci) = "Si" Then
                If pEspecialesTodo(CamposEspeciales.precio_no_socio) = "Si" Then
                    If pEspecialesTodo(CamposEspeciales.socio_adhe) = "Si" And lboolAdhe And bitAplicAdherente Then
                        ldoubValor = lstrCampos(CamposListaPreciosAran.impo_adhe)
                    Else
                        ldoubValor = lstrCampos(CamposListaPreciosAran.impo_noso)
                        bitAplicoCalculoDiscriminado = True
                    End If
                Else
                    ldoubValor = lstrCampos(CamposListaPreciosAran.impo_soci)
                End If
            Else
                If pEspecialesTodo(CamposEspeciales.precio_no_socio) = "Si" Then
                    If pEspecialesTodo(CamposEspeciales.soli_tram_precio_socio) Then
                        ldoubValor = lstrCampos(CamposListaPreciosAran.impo_soci)
                    Else
                        If pEspecialesTodo(CamposEspeciales.precio_socio) = "Si" Then
                            ldoubValor = lstrCampos(CamposListaPreciosAran.impo_soci)
                        Else
                            If pEspecialesTodo(CamposEspeciales.socio_adhe) = "Si" And lboolAdhe And bitAplicAdherente Then
                                ldoubValor = lstrCampos(CamposListaPreciosAran.impo_adhe)
                            Else
                                ldoubValor = lstrCampos(CamposListaPreciosAran.impo_noso)
                                bitAplicoCalculoDiscriminado = True
                            End If
                        End If
                    End If
                Else
                    ldoubValor = lstrCampos(CamposListaPreciosAran.impo_soci)
                End If
            End If
        End If

        '-------------------------------------------------------------------------------------------------------
        ' Dario 2013-04-25 (si es agrupacion y la cantidad total de miembros es distinta de la cantidad de socios
        '                   ponemos precio adherente)
        '-------------------------------------------------------------------------------------------------------
        Dim cantidadTotalMiembros As Integer = 0
        cantidadTotalMiembros = Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios)) _
                              + Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosNoSocios)) _
                              + Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSociosAdherentes))

        If (pPrecioNosocio = False And cantidadTotalMiembros > 1 And _
            Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios)) > 0 And _
            cantidadTotalMiembros <> Convert.ToInt32(pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios))) Then
            ldoubValor = lstrCampos(CamposListaPreciosAran.impo_adhe)
        End If

        '-------------------------------------------------------------------------------------------------------
        ' Dario 2013-04-16 (cambio para calculo discriminado de acuerdo a los integrantes de la agrup)
        '-------------------------------------------------------------------------------------------------------
        If (pPrecioNosocio = False And bitAplicoCalculoDiscriminado And lboolDiscrpreciounit) Then
            ldoubValor = Me.ValorArencelDiscriminado(lstrCampos, lboolAdhe _
                                                    , pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSocios) _
                                                    , pEspecialesTodo(CamposEspeciales.agrupCantMiembrosNoSocios) _
                                                    , pEspecialesTodo(CamposEspeciales.agrupCantMiembrosSociosAdherentes) _
                                                    )
        End If

        '--------------
        ' lo redondea
        '------------
        ldoubValor = SRA_Neg.Utiles.gRound2(ldoubValor)

        '-------------------------------------------------
        ' 2) Convertir el importe a $ si estuviese en u$s
        '------------------------------------------------
        If lstrCampos(CamposListaPreciosAran.mone_id) = TipoMoneda.dolar Then
            ldoubValor = (ldoubValor * pdouCotiDolar)
        End If

        ' Dario2021-03-05 
        '---------------------------------------------------
        ' 3) Convertir el importe a $ si estuviese en Euros
        '---------------------------------------------------
        If lstrCampos(CamposListaPreciosAran.mone_id) = TipoMoneda.euro Then
            Dim mone_id As Int16 = lstrCampos(CamposListaPreciosAran.mone_id)
            ' exec cotiza_monedas_consul @como_mone_id=13, @fecha_desde='20210305', @fecha_hasta='20210305', @formato =null
            Dim lstrCmd1 = "exec cotiza_monedas_consul @formato =null "
            lstrCmd1 = lstrCmd1 + ", @como_mone_id= " + Convert.ToString(mone_id)
            'lstrCmd1 = lstrCmd1 + ", @fecha_desde=" + clsFormatear.gFormatFecha2DB(System.DateTime.Now)
            'lstrCmd1 = lstrCmd1 + ", @fecha_hasta=" + clsFormatear.gFormatFecha2DB(System.DateTime.Now)

            Dim myConnection1 As New System.Data.SqlClient.SqlConnection(pstrConn)
            Dim cmdExec1 = New System.Data.SqlClient.SqlCommand
            myConnection1.Open()
            cmdExec1.Connection = myConnection1
            cmdExec1.CommandType = CommandType.Text
            cmdExec1.CommandText = lstrCmd1

            Dim dr1 As System.Data.SqlClient.SqlDataReader = cmdExec1.ExecuteReader()

            Dim importeE As Double

            While (dr1.Read())
                importeE = Convert.ToDouble(dr1.GetValue(3))
                Exit While
            End While

            dr1.Close()
            myConnection1.Close()

            ldoubValor = (ldoubValor * importeE)
        End If

        '-----------------------------
        ' 4) Recargo por forma de pago
        '-----------------------------
        Dim ldoubVal As Double = 0
        Select Case pintTipoPago
            Case TiposDePagos.CtaCte, TiposDePagos.PagoDiferido
                ' recargo por ctacte
                If lstrCampos(CamposListaPreciosAran.reca_ctac) <> 0 Then
                    ldoubVal = lstrCampos(CamposListaPreciosAran.reca_ctac)
                End If
            Case TiposDePagos.Tarjeta
                ' recargo por tarjeta
                If lstrCampos(CamposListaPreciosAran.reca_tarj) <> 0 Then
                    ldoubVal = lstrCampos(CamposListaPreciosAran.reca_tarj)
                End If
        End Select
        ldoubValor = ldoubValor * (1 + (ldoubVal / 100))
        '-----------------------
        ' lo redondea nuevamente
        '-----------------------
        ldoubValor = SRA_Neg.Utiles.gRound2(ldoubValor)

        Return ldoubValor
    End Function
    Public Function NroComprobanteGenerado(ByVal pstrConn As String, ByVal pintComprob As Integer) As String
        Dim lstrTabla As String
        If pTipoComprobGenera = TipoComprobGene.Factura Then
            lstrTabla = "facturas_rpt"
        Else
            lstrTabla = "proformas_rpt"
        End If
        Return ObtenerValorCampo(pstrConn, lstrTabla, "numero", pintComprob)
    End Function
    ' Dario 2013-07-10 - sobrecarga del metodo base que acepta transaccion 
    Public Function NroComprobanteGenerado(ByVal lTransac As SqlClient.SqlTransaction, ByVal pintComprob As Integer) As String
        Dim lstrTabla As String
        If pTipoComprobGenera = TipoComprobGene.Factura Then
            lstrTabla = "facturas_rpt"
        Else
            lstrTabla = "proformas_rpt"
        End If
        Return ObtenerValorCampo(lTransac, lstrTabla, "numero", pintComprob)
    End Function

    'Public Function FechaUltVtoComprob(ByVal pstrConn As String, ByVal pintCompId As Integer) As String
    '   Return ObtenerValorCampo(pstrConn, "dbo.comprob_vtos_ultimo", pintCompId)
    'End Function
    Public Function DiasVtoFactura(ByVal pstrConn As String) As Integer
        Return ObtenerValorCampo(pstrConn, "parametros", "para_fact_vto_dias")
    End Function
    Public Function LetraComprob(ByVal pstrConn As String, ByVal pintTipoIVAId As Integer)
        pLetraComprob = ObtenerValorCampo(pstrConn, "iva_posic", "ivap_letra", pintTipoIVAId)
    End Function
    Public Function CentroEmisorNro(Optional ByVal pstrConn As String = "")
        Dim pContext As HttpContext = HttpContext.Current
        If pContext.Session("sCentroEmisorId") Is Nothing Or pContext.Session("sCentroEmisorId") Is DBNull.Value Then
            ' Dario 2013-09-19 cambio para que muestre nuevo mensaje
            'Throw New AccesoBD.clsErrNeg(String.Format("La m�quina {0} no tiene un centro de emisi�n asociado", pContext.Session("sHost")))
            Throw New AccesoBD.clsErrNeg(String.Format("El Usuario {0} no tiene un centro de emisi�n asociado", pContext.Session("sUserName")))
        Else
            pCentroEmisorId = pContext.Session("sCentroEmisorId")
            pCentroEmisorNro = pContext.Session("sCentroEmisorNro")
            pHost = pContext.Session("sHost")
        End If
    End Function
    Public Function CajaCerrada(ByVal pstrConn As String, ByVal pintFormaPago As Integer, ByVal pintEmisor As Integer) As Boolean
        'devuelve si la caja del dia esta cerrada o no.
        Dim lbooResu As Boolean = False
        'si el tipo de pago es con tarjeta o efectivo, de lo contrario no valida y devuelve false 
        If pintFormaPago = TiposDePagos.Contado Or pintFormaPago = TiposDePagos.Tarjeta Or pintFormaPago = TiposDePagos.PagoDiferido Then
            If ObtenerValorCampo(pstrConn, "estado_caja", "cerrada", clsSQLServer.gFormatArg(pintEmisor, SqlDbType.Int)) = "S" Then
                lbooResu = True
            End If
        End If
        Return lbooResu
    End Function

    Public Function ConvSaldo(ByVal pstrConn As String, ByVal pintConvenio As Integer)
        If pintConvenio = 0 Then
            pConvSaldo = False
        Else
            pConvSaldo = ObtenerValorCampo(pstrConn, "convenios", "conv_cont_sldo", pintConvenio)
        End If
    End Function
    Public Function IVA(ByVal pstrConn As String, ByVal pFecha As String)
        'ver con la bd
        Dim lDsDatos As DataSet = clsSQLServer.gObtenerEstruc(pstrConn, "iva_tasasX", clsSQLServer.gFormatArg(pFecha, SqlDbType.SmallDateTime))
        Dim lstrValor As String
        lstrValor = ""
        If lDsDatos.Tables(0).Rows.Count > 0 Then
            pIVATasa = lDsDatos.Tables(0).Rows(0).Item("valor_tasa")
            pIVATasaRedu = lDsDatos.Tables(0).Rows(0).Item("valor_tasa_redu")
            pIVATasaSobre = lDsDatos.Tables(0).Rows(0).Item("valor_tasa_sobre")
        End If
        Return lstrValor
    End Function
    Public Function CalcularSaldoCuotas(ByVal pdsDatos As DataSet, ByVal pdouTotal As Double) As String
        Dim ldouAcum As Decimal
        ldouAcum = 0
        'recorre la tabla de comprob_vtos
        For Each ldr As DataRow In pdsDatos.Tables(tablasDs.mstrTablaVtos).Select
            If ldr.Item(0) Is DBNull.Value OrElse ldr.Item(0) <= 0 Then
                ldouAcum = ldouAcum + ldr.Item("covt_impo")
            End If
        Next
        Return Format((pdouTotal - ldouAcum), "###0.00")
    End Function
    Public Function CalcularValoresCuota(ByVal pdouTotal As Double, ByVal pdouPorcentaje As Double, ByVal pdouImporte As Double)

        ' se toma el valor del porcentaje, no se tiene en cuenta el importe
        If pdouPorcentaje <> 0 Then
            pCuotaImporte = (pdouTotal * pdouPorcentaje) / 100
            pCuotaPorcentaje = pdouPorcentaje
        Else
            pCuotaImporte = pdouImporte
            pCuotaPorcentaje = 0
        End If

    End Function
    Public Function DeudaVencida(ByVal pstrConn As String, ByVal pintClieId As Integer)
        Return ObtenerValorCampo(pstrConn, "clientes_deuda_vencida", "valor_deuda", pintClieId)
    End Function

    Public Function ErrProformaFact(ByVal pstrConn As String, ByVal pstrIds As String)
        'Descripci�n de porque no pueden facturarse la/s proforma/s.
        Dim ds As DataSet

        'ejecuto la consulta y cargo un DS para luego llenar la grilla
        ds = clsSQLServer.gExecuteQuery(pstrConn, "exec dbo.proformas_fact_masiva_valida_consul @prfr_ids ='" & pstrIds & "'")
        Return ds
    End Function
    Public Function ProformaFact(ByVal pstrConn As String, ByVal lstrIDS As String)
        Dim lintClieIdAnt, lintEmctIdAnt, lintActiIdAnt, lintCptiIdAnt As Integer
        Dim lintFechaAnt As DateTime
        Dim ldrComp, ldrCode, ldrCoco, ldrCopc, ldrCovt, ldrCoan, ldrCoad, ldrRela As DataRow
        Dim ldsProf, ldsComp As DataSet
        Dim lstrFiltro As String

        'trae las proformas que se van a facturar
        ldsProf = mDSProformas(pstrConn, "@prfr_ids='" & lstrIDS & "'")

        'agrupa por centro emisor/actividad/cliente/fecha valor/tipo pago

        '1 factura puede tener varias proformas (deben tener los mismos datos en los sig. campos:
        '   centro emisor/actividad/cliente/fecha valor/tipo pago)
        ' puede que por superar saldo no puedan facturarse todas las proformas, esas
        ' deben sumarse en la grilla de los errores  
        '       

        ldsComp = mCrearDsComp(pstrConn)

        For Each ldrPrfr As DataRow In ldsProf.Tables(Constantes.gTab_Proformas).Select("", "prfr_clie_id, prfr_emct_id, prfr_acti_id, prfr_fecha, prfr_cpti_id,prfr_neto desc")
            If True Then   'antes cortaba por estas condiciones, ahora es una proforma, una factura
                'lintClieIdAnt <> ldrPrfr.Item("prfr_clie_id") _
                '            Or lintEmctIdAnt <> ldrPrfr.Item("prfr_emct_id") _
                '            Or lintActiIdAnt <> ldrPrfr.Item("prfr_acti_id") _
                '            Or lintCptiIdAnt <> ldrPrfr.Item("prfr_cpti_id") _
                '            Or lintFechaAnt <> ldrPrfr.Item("prfr_fecha") Then
                ldrComp = ldsComp.Tables(SRA_Neg.Constantes.gTab_Comprobantes).NewRow

                'COMPROBANTES
                With ldrComp
                    .Item("comp_id") = clsSQLServer.gObtenerId(.Table, "comp_id")
                    .Item("comp_fecha") = ldrPrfr.Item("prfr_fecha")
                    .Item("comp_ingr_fecha") = ldrPrfr.Item("prfr_ingr_fecha")
                    .Item("comp_impre") = 0
                    .Item("comp_clie_id") = ldrPrfr.Item("prfr_clie_id")
                    .Item("comp_dh") = ldrPrfr.Item("prfr_dh")
                    .Item("comp_cance") = 0 ' 0 = pendiente
                    .Item("comp_cs") = ldrPrfr.Item("prfr_cs")
                    .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Factura
                    .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.Facturaci�n_Proformas
                    .Item("comp_mone_id") = ldrPrfr.Item("prfr_mone_id")
                    .Item("comp_neto") = ldrPrfr.Item("prfr_neto")
                    .Item("comp_letra") = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", .Item("comp_clie_id").ToString).Tables(0).Rows(0).Item("_ivap_letra")
                    .Item("comp_cpti_id") = ldrPrfr.Item("prfr_cpti_id")
                    .Item("comp_acti_id") = ldrPrfr.Item("prfr_acti_id")
                    .Item("comp_cemi_nume") = ldrPrfr.Item("prfr_cemi_nume")
                    .Item("comp_emct_id") = ldrPrfr.Item("prfr_emct_id")
                    .Item("comp_host") = ldrPrfr.Item("prfr_host")

                    .Item("_comp_desc") = clsSQLServer.gObtenerEstruc(mstrConn, "clientesX", .Item("comp_clie_id").ToString).Tables(0).Rows(0).Item("clie_apel")
                    .Table.Rows.Add(ldrComp)
                End With

                For Each ldrPrde As DataRow In ldsProf.Tables(Constantes.gTab_ProformaDeta).Select("prde_prfr_id=" + ldrPrfr.Item("prfr_id").ToString)
                    'COMPROB_DETA
                    ldrCode = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobDeta).NewRow

                    With ldrCode
                        .Item("code_id") = clsSQLServer.gObtenerId(.Table, "code_id")
                        .Item("code_comp_id") = ldrComp.Item("comp_id")
                        .Item("code_coti") = ldrPrde.Item("prde_coti")
                        .Item("code_obse") = ldrPrde.Item("prde_obse")
                        .Item("code_cria_id") = ldrPrde.Item("prde_cria_id")
                        .Item("code_raza_id") = ldrPrde.Item("prde_raza_id")
                        .Item("code_pers") = ldrPrde.Item("prde_pers")
                        .Item("code_impo_ivat_tasa") = ldrPrde.Item("prde_impo_ivat_tasa")
                        .Item("code_impo_ivat_tasa_redu") = ldrPrde.Item("prde_impo_ivat_tasa_redu")
                        .Item("code_impo_ivat_tasa_sobre") = ldrPrde.Item("prde_impo_ivat_tasa_sobre")
                        .Item("code_impo_ivat_perc") = ldrPrde.Item("prde_impo_ivat_perc")
                        .Item("code_expo_id") = ldrPrde.Item("prde_expo_id")    '21/06/2011
                        .Item("code_conv_id") = ldrPrde.Item("prde_conv_id")    '21/06/2011
                        .Table.Rows.Add(ldrCode)
                    End With
                Next

                'COMPROB_VTOS
                ldrCovt = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobVtos).NewRow
                With ldrCovt
                    .Item("covt_id") = clsSQLServer.gObtenerId(.Table, "covt_id")
                    .Item("covt_comp_id") = ldrComp.Item("comp_id")
                    .Item("covt_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, ldrComp.Item("comp_acti_id"), ldrComp.Item("comp_fecha"))

                    .Item("covt_porc") = 100
                    .Item("covt_impo") = ldrComp.Item("comp_neto")
                    .Item("covt_cance") = 0

                    .Table.Rows.Add(ldrCovt)
                End With
            Else
                ldrComp.Item("comp_neto") += ldrPrfr.Item("prfr_neto")
                ldrCovt.Item("covt_impo") = ldrComp.Item("comp_neto")
            End If

            'RELACION ENTRE EL COMPROBANTE Y LA PROFORMA
            ldrRela = ldsComp.Tables(SRA_Neg.Constantes.gTab_Comprobantes & "_rela").NewRow
            With ldrRela
                .Item("comp_id") = ldrComp.Item("comp_id")
                .Item("prfr_id") = ldrPrfr.Item("prfr_id")
                .Table.Rows.Add(ldrRela)
            End With

            lintClieIdAnt = ldrPrfr.Item("prfr_clie_id")
            lintEmctIdAnt = ldrPrfr.Item("prfr_emct_id")
            lintActiIdAnt = ldrPrfr.Item("prfr_acti_id")
            lintCptiIdAnt = ldrPrfr.Item("prfr_cpti_id")
            lintFechaAnt = ldrPrfr.Item("prfr_fecha")

            'COMPROB_CONCEPTOS
            For Each ldrPrco As DataRow In ldsProf.Tables(SRA_Neg.Constantes.gTab_ProformaConcep).Select("prco_prfr_id=" & ldrPrfr.Item("prfr_id").ToString)
                With ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobConcep).Select("coco_comp_id=" + ldrComp.Item("comp_id").ToString + " AND coco_conc_id=" + ldrPrco.Item("prco_conc_id").ToString + " AND coco_ccos_id " + IIf(ldrPrco.IsNull("prco_ccos_id"), "IS NULL", "=" & ldrPrco.Item("prco_ccos_id").ToString))
                    If .GetUpperBound(0) = -1 Then
                        ldrCoco = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobConcep).NewRow
                        With ldrCoco
                            .Item("coco_id") = clsSQLServer.gObtenerId(.Table, "coco_id")
                            .Item("coco_comp_id") = ldrComp.Item("comp_id")
                            .Item("coco_conc_id") = ldrPrco.Item("prco_conc_id")
                            .Item("coco_ccos_id") = ldrPrco.Item("prco_ccos_id")
                            .Item("coco_impo") = 0
                            .Table.Rows.Add(ldrCoco)
                        End With
                    Else
                        ldrCoco = .GetValue(0)
                    End If
                End With

                With ldrCoco
                    .Item("coco_impo") += ldrPrco.Item("prco_impo")
                    .Item("coco_impo_ivai") = .Item("coco_impo")
                End With
            Next

            'COMPROB_ARANC
            For Each ldrPran As DataRow In ldsProf.Tables(SRA_Neg.Constantes.gTab_ProformaAranc).Select("pran_prfr_id=" & ldrPrfr.Item("prfr_id").ToString)
                lstrFiltro = "coan_comp_id=" + ldrComp.Item("comp_id").ToString + " AND coan_aran_id=" + ldrPran.Item("pran_aran_id").ToString + " AND coan_rrgg_aran_id " + IIf(ldrPran.IsNull("pran_rrgg_aran_id"), "IS NULL", "=" & ldrPran.Item("pran_rrgg_aran_id").ToString)
                If ldrPran.IsNull("pran_fecha") Then
                    lstrFiltro += " AND coan_fecha IS NULL"
                Else
                    lstrFiltro += " AND coan_fecha=" + clsFormatear.gFormatFechaDS(ldrPran.Item("pran_fecha"))
                End If

                With ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobAranc).Select(lstrFiltro)
                    If .GetUpperBound(0) = -1 Then
                        ldrCoan = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobAranc).NewRow
                        With ldrCoan
                            .Item("coan_id") = clsSQLServer.gObtenerId(.Table, "coan_id")
                            .Item("coan_comp_id") = ldrComp.Item("comp_id")

                            .Item("coan_aran_id") = ldrPran.Item("pran_aran_id")
                            .Item("coan_ccos_id") = ldrPran.Item("pran_ccos_id")
                            .Item("coan_unit_impo") = ldrPran.Item("pran_unit_impo")
                            .Item("coan_tasa_iva") = ldrPran.Item("pran_tasa_iva")
                            .Item("coan_fecha") = ldrPran.Item("pran_fecha")
                            .Item("coan_inic_rp") = ldrPran.Item("pran_inic_rp")
                            .Item("coan_fina_rp") = ldrPran.Item("pran_fina_rp")
                            .Item("coan_tram") = ldrPran.Item("pran_tram")
                            .Item("coan_acta") = ldrPran.Item("pran_acta")
                            .Item("coan_anim_no_fact") = ldrPran.Item("pran_anim_no_fact")
                            .Item("coan_auto") = ldrPran.Item("pran_auto")
                            .Item("coan_desc_ampl") = ldrPran.Item("pran_desc_ampl")
                            .Item("coan_rrgg_aran_id") = ldrPran.Item("pran_rrgg_aran_id")
                            .Item("coan_exen") = ldrPran.Item("pran_exen")
                            .Item("coan_sin_carg") = ldrPran.Item("pran_sin_carg")

                            .Item("coan_impo") = 0
                            .Item("coan_impo_ivai") = 0
                            .Item("coan_cant") = 0

                            .Table.Rows.Add(ldrCoan)
                        End With
                    Else
                        ldrCoan = .GetValue(0)
                    End If
                End With

                With ldrCoan
                    .Item("coan_cant") += ldrPran.Item("pran_cant")
                    .Item("coan_impo") += ldrPran.Item("pran_impo")
                    .Item("coan_impo_ivai") += ldrPran.Item("pran_impo_ivai")
                End With

                'COMPROB_ARANC_DETA
                For Each ldrPrad As DataRow In clsSQLServer.gObtenerEstruc(pstrConn, Constantes.gTab_ProformaArancDeta, "@prad_pran_id=" & ldrPran.Item("pran_id").ToString).Tables(0).Select()
                    ldrCoad = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobArancDeta).NewRow
                    With ldrCoad
                        .Item("coad_id") = clsSQLServer.gObtenerId(.Table, "coad_id")
                        .Item("coad_coan_id") = ldrCoan.Item("coan_id")
                        .Item("coad_turn_otor") = ldrPrad.Item("prad_turn_otor")
                        .Item("coad_turn_reci") = ldrPrad.Item("prad_turn_reci")

                        .Item("coad_rece_fecha") = ldrPrad.Item("prad_rece_fecha")
                        .Item("coad_cose_fecha") = ldrPrad.Item("prad_cose_fecha")
                        .Item("coad_fise_fecha") = ldrPrad.Item("prad_fise_fecha")

                        .Item("coad_sobr_paff") = ldrPrad.Item("prad_sobr_paff")

                        .Item("coad_reca_turn") = ldrPrad.Item("prad_reca_turn")
                        .Item("coad_dcto_prom") = ldrPrad.Item("prad_dcto_prom")

                        .Item("coad_dcto_cant") = ldrPrad.Item("prad_dcto_cant")
                        .Item("coad_tram_urge") = ldrPrad.Item("prad_tram_urge")
                        .Item("coad_codi") = ldrPrad.Item("prad_codi")
                        .Item("coad_cunica") = ldrPrad.Item("prad_cunica")
                        .Item("coad_tipo") = ldrPrad.Item("prad_tipo")

                        .Table.Rows.Add(ldrCoad)
                    End With
                Next
            Next
        Next
        Return (ldsComp)
    End Function

    ''' Sobrecarga: Se le pasa el ID Centro Emisor y el Numero Centro Emisor. Usada: FactMasivaProf.aspx.vb
    Public Function ProformaFact(ByVal pstrConn As String, ByVal lstrIDS As String, ByVal iCentroEmisorId As Integer, ByVal iCentroEmisorNro As Integer)
        Dim lintClieIdAnt, lintEmctIdAnt, lintActiIdAnt, lintCptiIdAnt As Integer
        Dim lintFechaAnt As DateTime
        Dim ldrComp, ldrCode, ldrCoco, ldrCopc, ldrCovt, ldrCoan, ldrCoad, ldrRela As DataRow
        Dim ldsProf, ldsComp As DataSet
        Dim lstrFiltro As String
        Dim blnCtaCte As Boolean = False

        'trae las proformas que se van a facturar
        ldsProf = mDSProformas(pstrConn, "@prfr_ids='" & lstrIDS & "'")

        'agrupa por centro emisor/actividad/cliente/fecha valor/tipo pago

        '1 factura puede tener varias proformas (deben tener los mismos datos en los sig. campos:
        '   centro emisor/actividad/cliente/fecha valor/tipo pago)
        ' puede que por superar saldo no puedan facturarse todas las proformas, esas
        ' deben sumarse en la grilla de los errores  
        '       

        ldsComp = mCrearDsComp(pstrConn)

        For Each ldrPrfr As DataRow In ldsProf.Tables(Constantes.gTab_Proformas).Select("", "prfr_clie_id, prfr_emct_id, prfr_acti_id, prfr_fecha, prfr_cpti_id,prfr_neto desc")
            If True Then   'antes cortaba por estas condiciones, ahora es una proforma, una factura
                'lintClieIdAnt <> ldrPrfr.Item("prfr_clie_id") _
                '            Or lintEmctIdAnt <> ldrPrfr.Item("prfr_emct_id") _
                '            Or lintActiIdAnt <> ldrPrfr.Item("prfr_acti_id") _
                '            Or lintCptiIdAnt <> ldrPrfr.Item("prfr_cpti_id") _
                '            Or lintFechaAnt <> ldrPrfr.Item("prfr_fecha") Then
                ldrComp = ldsComp.Tables(SRA_Neg.Constantes.gTab_Comprobantes).NewRow

                'COMPROBANTES
                With ldrComp
                    .Item("comp_id") = clsSQLServer.gObtenerId(.Table, "comp_id")
                    .Item("comp_fecha") = ldrPrfr.Item("prfr_fecha")
                    .Item("comp_ingr_fecha") = ldrPrfr.Item("prfr_ingr_fecha")
                    .Item("comp_impre") = 0
                    .Item("comp_clie_id") = ldrPrfr.Item("prfr_clie_id")
                    .Item("comp_dh") = ldrPrfr.Item("prfr_dh")
                    .Item("comp_cance") = 0 ' 0 = pendiente
                    .Item("comp_cs") = ldrPrfr.Item("prfr_cs")
                    .Item("comp_coti_id") = SRA_Neg.Constantes.ComprobTipos.Factura
                    .Item("comp_modu_id") = SRA_Neg.Constantes.Modulos.Facturaci�n_Proformas
                    .Item("comp_mone_id") = ldrPrfr.Item("prfr_mone_id")
                    .Item("comp_neto") = ldrPrfr.Item("prfr_neto")
                    .Item("comp_letra") = clsSQLServer.gObtenerEstruc(mstrConn, "clientes", .Item("comp_clie_id").ToString).Tables(0).Rows(0).Item("_ivap_letra")
                    .Item("comp_cpti_id") = ldrPrfr.Item("prfr_cpti_id")
                    'Si es 2, es CtaCte.
                    If .Item("comp_cpti_id") = 2 Then blnCtaCte = True Else blnCtaCte = False
                    .Item("comp_acti_id") = ldrPrfr.Item("prfr_acti_id")
                    .Item("comp_cemi_nume") = iCentroEmisorNro 'ldrPrfr.Item("prfr_cemi_nume")
                    .Item("comp_emct_id") = iCentroEmisorId 'ldrPrfr.Item("prfr_emct_id")
                    .Item("comp_host") = ldrPrfr.Item("prfr_host")

                    .Item("_comp_desc") = clsSQLServer.gObtenerEstruc(mstrConn, "clientesX", .Item("comp_clie_id").ToString).Tables(0).Rows(0).Item("clie_apel")
                    .Table.Rows.Add(ldrComp)
                End With

                For Each ldrPrde As DataRow In ldsProf.Tables(Constantes.gTab_ProformaDeta).Select("prde_prfr_id=" + ldrPrfr.Item("prfr_id").ToString)
                    'COMPROB_DETA
                    ldrCode = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobDeta).NewRow

                    With ldrCode
                        .Item("code_id") = clsSQLServer.gObtenerId(.Table, "code_id")
                        .Item("code_comp_id") = ldrComp.Item("comp_id")
                        .Item("code_coti") = ldrPrde.Item("prde_coti")
                        .Item("code_obse") = ldrPrde.Item("prde_obse")
                        .Item("code_cria_id") = ldrPrde.Item("prde_cria_id")
                        .Item("code_raza_id") = ldrPrde.Item("prde_raza_id")
                        .Item("code_pers") = ldrPrde.Item("prde_pers")
                        .Item("code_impo_ivat_tasa") = ldrPrde.Item("prde_impo_ivat_tasa")
                        .Item("code_impo_ivat_tasa_redu") = ldrPrde.Item("prde_impo_ivat_tasa_redu")
                        .Item("code_impo_ivat_tasa_sobre") = ldrPrde.Item("prde_impo_ivat_tasa_sobre")
                        .Item("code_impo_ivat_perc") = ldrPrde.Item("prde_impo_ivat_perc")
                        .Item("code_expo_id") = ldrPrde.Item("prde_expo_id")    '21/06/2011
                        .Item("code_conv_id") = ldrPrde.Item("prde_conv_id")    '21/06/2011
                        .Table.Rows.Add(ldrCode)
                    End With
                Next

                'COMPROB_VTOS
                ldrCovt = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobVtos).NewRow
                With ldrCovt
                    .Item("covt_id") = clsSQLServer.gObtenerId(.Table, "covt_id")
                    .Item("covt_comp_id") = ldrComp.Item("comp_id")

                    If Not blnCtaCte Then
                        .Item("covt_fecha") = Today
                    Else
                        .Item("covt_fecha") = SRA_Neg.Comprobantes.FechaVtoFactura(mstrConn, ldrComp.Item("comp_acti_id"), Today)
                    End If

                    .Item("covt_porc") = 100
                    .Item("covt_impo") = ldrComp.Item("comp_neto")
                    .Item("covt_cance") = 0

                    .Table.Rows.Add(ldrCovt)
                End With
            Else
                ldrComp.Item("comp_neto") += ldrPrfr.Item("prfr_neto")
                ldrCovt.Item("covt_impo") = ldrComp.Item("comp_neto")
            End If

            'RELACION ENTRE EL COMPROBANTE Y LA PROFORMA
            ldrRela = ldsComp.Tables(SRA_Neg.Constantes.gTab_Comprobantes & "_rela").NewRow
            With ldrRela
                .Item("comp_id") = ldrComp.Item("comp_id")
                .Item("prfr_id") = ldrPrfr.Item("prfr_id")
                .Table.Rows.Add(ldrRela)
            End With

            lintClieIdAnt = ldrPrfr.Item("prfr_clie_id")
            lintEmctIdAnt = ldrPrfr.Item("prfr_emct_id")
            lintActiIdAnt = ldrPrfr.Item("prfr_acti_id")
            lintCptiIdAnt = ldrPrfr.Item("prfr_cpti_id")
            lintFechaAnt = ldrPrfr.Item("prfr_fecha")

            'COMPROB_CONCEPTOS
            For Each ldrPrco As DataRow In ldsProf.Tables(SRA_Neg.Constantes.gTab_ProformaConcep).Select("prco_prfr_id=" & ldrPrfr.Item("prfr_id").ToString)
                With ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobConcep).Select("coco_comp_id=" + ldrComp.Item("comp_id").ToString + " AND coco_conc_id=" + ldrPrco.Item("prco_conc_id").ToString + " AND coco_ccos_id " + IIf(ldrPrco.IsNull("prco_ccos_id"), "IS NULL", "=" & ldrPrco.Item("prco_ccos_id").ToString))
                    If .GetUpperBound(0) = -1 Then
                        ldrCoco = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobConcep).NewRow
                        With ldrCoco
                            .Item("coco_id") = clsSQLServer.gObtenerId(.Table, "coco_id")
                            .Item("coco_comp_id") = ldrComp.Item("comp_id")
                            .Item("coco_conc_id") = ldrPrco.Item("prco_conc_id")
                            .Item("coco_ccos_id") = ldrPrco.Item("prco_ccos_id")
                            .Item("coco_impo") = 0
                            .Table.Rows.Add(ldrCoco)
                        End With
                    Else
                        ldrCoco = .GetValue(0)
                    End If
                End With

                With ldrCoco
                    .Item("coco_impo") += ldrPrco.Item("prco_impo")
                    .Item("coco_impo_ivai") = .Item("coco_impo")
                End With
            Next

            'COMPROB_ARANC
            For Each ldrPran As DataRow In ldsProf.Tables(SRA_Neg.Constantes.gTab_ProformaAranc).Select("pran_prfr_id=" & ldrPrfr.Item("prfr_id").ToString)
                lstrFiltro = "coan_comp_id=" + ldrComp.Item("comp_id").ToString + " AND coan_aran_id=" + ldrPran.Item("pran_aran_id").ToString + " AND coan_rrgg_aran_id " + IIf(ldrPran.IsNull("pran_rrgg_aran_id"), "IS NULL", "=" & ldrPran.Item("pran_rrgg_aran_id").ToString)
                If ldrPran.IsNull("pran_fecha") Then
                    lstrFiltro += " AND coan_fecha IS NULL"
                Else
                    lstrFiltro += " AND coan_fecha=" + clsFormatear.gFormatFechaDS(ldrPran.Item("pran_fecha"))
                End If

                With ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobAranc).Select(lstrFiltro)
                    If .GetUpperBound(0) = -1 Then
                        ldrCoan = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobAranc).NewRow
                        With ldrCoan
                            .Item("coan_id") = clsSQLServer.gObtenerId(.Table, "coan_id")
                            .Item("coan_comp_id") = ldrComp.Item("comp_id")

                            .Item("coan_aran_id") = ldrPran.Item("pran_aran_id")
                            .Item("coan_ccos_id") = ldrPran.Item("pran_ccos_id")
                            .Item("coan_unit_impo") = ldrPran.Item("pran_unit_impo")
                            .Item("coan_tasa_iva") = ldrPran.Item("pran_tasa_iva")
                            .Item("coan_fecha") = ldrPran.Item("pran_fecha")
                            .Item("coan_inic_rp") = ldrPran.Item("pran_inic_rp")
                            .Item("coan_fina_rp") = ldrPran.Item("pran_fina_rp")
                            .Item("coan_tram") = ldrPran.Item("pran_tram")
                            .Item("coan_acta") = ldrPran.Item("pran_acta")
                            .Item("coan_anim_no_fact") = ldrPran.Item("pran_anim_no_fact")
                            .Item("coan_auto") = ldrPran.Item("pran_auto")
                            .Item("coan_desc_ampl") = ldrPran.Item("pran_desc_ampl")
                            .Item("coan_rrgg_aran_id") = ldrPran.Item("pran_rrgg_aran_id")
                            .Item("coan_exen") = ldrPran.Item("pran_exen")
                            .Item("coan_sin_carg") = ldrPran.Item("pran_sin_carg")

                            .Item("coan_impo") = 0
                            .Item("coan_impo_ivai") = 0
                            .Item("coan_cant") = 0

                            .Table.Rows.Add(ldrCoan)
                        End With
                    Else
                        ldrCoan = .GetValue(0)
                    End If
                End With

                With ldrCoan
                    .Item("coan_cant") += ldrPran.Item("pran_cant")
                    .Item("coan_impo") += ldrPran.Item("pran_impo")
                    .Item("coan_impo_ivai") += ldrPran.Item("pran_impo_ivai")
                End With

                'COMPROB_ARANC_DETA
                For Each ldrPrad As DataRow In clsSQLServer.gObtenerEstruc(pstrConn, Constantes.gTab_ProformaArancDeta, "@prad_pran_id=" & ldrPran.Item("pran_id").ToString).Tables(0).Select()
                    ldrCoad = ldsComp.Tables(SRA_Neg.Constantes.gTab_ComprobArancDeta).NewRow
                    With ldrCoad
                        .Item("coad_id") = clsSQLServer.gObtenerId(.Table, "coad_id")
                        .Item("coad_coan_id") = ldrCoan.Item("coan_id")
                        .Item("coad_turn_otor") = ldrPrad.Item("prad_turn_otor")
                        .Item("coad_turn_reci") = ldrPrad.Item("prad_turn_reci")

                        .Item("coad_rece_fecha") = ldrPrad.Item("prad_rece_fecha")
                        .Item("coad_cose_fecha") = ldrPrad.Item("prad_cose_fecha")
                        .Item("coad_fise_fecha") = ldrPrad.Item("prad_fise_fecha")

                        .Item("coad_sobr_paff") = ldrPrad.Item("prad_sobr_paff")

                        .Item("coad_reca_turn") = ldrPrad.Item("prad_reca_turn")
                        .Item("coad_dcto_prom") = ldrPrad.Item("prad_dcto_prom")

                        .Item("coad_dcto_cant") = ldrPrad.Item("prad_dcto_cant")
                        .Item("coad_tram_urge") = ldrPrad.Item("prad_tram_urge")
                        .Item("coad_codi") = ldrPrad.Item("prad_codi")
                        .Item("coad_cunica") = ldrPrad.Item("prad_cunica")
                        .Item("coad_tipo") = ldrPrad.Item("prad_tipo")

                        .Table.Rows.Add(ldrCoad)
                    End With
                Next
            Next
        Next
        Return (ldsComp)
    End Function

    Public Function GetCantidadItemsPagosProforma(ByVal pstrId) As Integer

        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim lstrId As String = ""


        mstrCmd = "exec tramites_consul @tram_id =" & clsSQLServer.gFormatArg(pstrId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            lstrId = Convert.ToInt32(ValidarNulos(dt.Rows(0).Item("tram_id"), False))
        End If


        Return (lstrId)

    End Function

    Private Function mCrearDsComp(ByVal pstrConn As String) As DataSet
        Dim ldsDatos As New DataSet

        ldsDatos = SRA_Neg.Comprobantes.gCrearDataSetComp(mstrConn, "")
        ldsDatos.Tables(SRA_Neg.Constantes.gTab_Comprobantes).Rows(0).Delete()
        Utiles.gAgregarTabla(pstrConn, ldsDatos, SRA_Neg.Constantes.gTab_ComprobArancDeta)

        With ldsDatos.Tables.Add(Constantes.gTab_Comprobantes & "_rela")
            .Columns.Add("prfr_id", System.Type.GetType("System.Int32"))
            .Columns.Add("comp_id", System.Type.GetType("System.Int32"))
        End With
        Return ldsDatos
    End Function

    Public Sub Limpiar(Optional ByVal pboolTodo As Boolean = True)
        'el limpiar es necesario cuando se agrega un nuevo comprobante y no 
        'se cierra la pagina
        ' If mdsDs Is Nothing Then
        '  For Each ldr As DataRow In mdsDs.Tables(Tablas.inturfac_bov).Select
        '  ldr.Delete()
        '  Next
        mdsDs.Tables(Tablas.inturfac_bov).Clear()
        mdsDs.Tables(Tablas.inturfac_equ).Clear()
        pPagoCtaCte = False
        pAutorizaciones = False
        pTarjetas = False
        pRRGG = False
        pAlum = False
        pVarios = True
        pLabora = False
        pExpo = False
        pProformaPend = False
        pActiv = 0
        pEstadoSaldos = ""
        pEspeciales = ""
        pTotalBrutoGral = 0
        pTotalIVAGral = 0
        pTotalSobreTGral = 0
        pTotalReduGral = 0
        pTotalNetoGral = 0
        pTotalesIVA = 0
        pCuotaImporte = 0
        pCuotaPorcentaje = 0
        pLetraComprob = ""
        pConvSaldo = False
        pAranRRGGRepresAnim = False
        pAranRRGGDescFecha = ""
        pAranRRGGActa = False
        pAranRRGGSobretasa = 0
        pAranRRGGconSobretasa = False
        pAranRRGGconSobretasaNormal = False
        pAranRRGGconSobretasaRango = False
        pProforma = False
        pProformaAutoExcSaldo = False
        pProformaAuto = False
        pTipoComprobGenera = 0
        pProformaCmbComprobCargado = False
        pPrecioSocio_falle = False
        pPrecioSocio_trami = False
        pTurnosDTSBov = False
        pTurnosDTSEqui = False
        pProformaRecalcularaValores = False
        pPrecioNosocio = False
        If pboolTodo Then pRecalculaValoresProforma = False
        '  End If

    End Sub
    Public Sub Liberar(ByVal pPagina As System.Web.UI.Page)
        pPagina.Session("ds") = Nothing
    End Sub
#End Region

#Region "ABM"
    Public Overloads Function Alta() As String
        Return (mActualizar())
    End Function
    Private Function mActualizar() As String
        Dim lstrTABLAId As String
        Dim lstrId As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                Next
            End If

            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)
            Next

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function
#End Region
End Class
