Public Class ComisionesRazas
   Inherits GenericaRel

   Private mstrTablaAgenda As String = SRA_Neg.Constantes.gTab_ComisionesAgenda
   Private mstrTablaCriadores As String = SRA_Neg.Constantes.gTab_ComisionesCriadores
   Private mstrTablaDocum As String = SRA_Neg.Constantes.gTab_ComisionesDocum
   Private mstrTablaActas As String = SRA_Neg.Constantes.gTab_ComisionesActas

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet, ByVal pContext As System.Web.HttpContext)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mdsDatos = pdsDatos
      Me.lContext = pContext
   End Sub

   Public Overloads Function Alta() As String
      Return (mActualizar())
   End Function

   Public Overloads Sub Modi()
      mActualizar()
   End Sub

   Private Overloads Function mActualizar() As String
      Dim lstrId, lstrDocId As String
      Dim lTransac As SqlClient.SqlTransaction
      Dim lstrColPK As String

      Try
         lTransac = clsSQLServer.gObtenerTransac(mstrConn)

         'TABLA COMISIONES
         If CInt(MiRow.Item(0)) <= 0 Then
            'ALTA TABLA
            lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
         Else
            'MODIFICACION TABLA
            clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
            lstrId = MiRow.Item(0).ToString

            For i As Integer = 1 To mdsDatos.Tables.Count - 1
               BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
            Next
         End If

         Dim lstrIdAnt As String

         'GUARDO TABLA DE DETALLES DE TRAMITE PARA USARLO EN OBSERVACIONES
         For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDocum).Select()
            lstrIdAnt = odrDeta.Item("cdoc_id")

            If CInt(odrDeta.Item(0)) <= 0 Then
               'ALTA DETALLE
               odrDeta.Item("cdoc_craz_id") = lstrId
               odrDeta.Item("cdoc_id") = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDocum, odrDeta)
            End If

            lstrDocId = odrDeta.Item("cdoc_id")
            'ACTUALIZO CON EL ID DETALLE NUEVO
            For Each odrRela As DataRow In mdsDatos.Tables(mstrTablaActas).Select("crac_cdoc_id=" + lstrIdAnt)
               odrRela.Item("crac_cdoc_id") = lstrDocId
               If odrRela.Item("crac_id") <= 0 Then
                  clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaActas, odrRela)
               Else
                  clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaActas, odrRela)
               End If
            Next
         Next

         'COMISIONES ACTAS
         mdsDatos.Tables.Remove(mdsDatos.Tables(mstrTablaActas))

         'DETALLE
         For i As Integer = 1 To mdsDatos.Tables.Count - 1
            lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
            lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
            lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

            ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)
         Next

         clsSQLServer.gCommitTransac(lTransac)

         Return (lstrId)
      Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(ex)
         Throw (ex)
      End Try
   End Function

End Class
