Imports System.IO
Imports System.Web
Imports System.Web.UI.WebControls

Public Class Utiles

    Public Function AddCol(ByVal ds As DataSet, ByVal dt As DataTable, ByVal colName As String, _
    ByVal type As Type, ByVal location As Integer) As DataTable

        ' Dim dt As New DataTable
        Dim colIndex As Integer = 0
        Dim dc2 As DataColumn = Nothing
        For Each item As DataColumn In ds.Tables(0).Columns

            If colIndex = location Then

                dc2 = New DataColumn
                dc2.ColumnName = colName
                dc2.DataType = type
                dt.Columns.Add(dc2)

            End If

            dc2 = New DataColumn
            dc2.ColumnName = item.ColumnName
            dc2.DataType = item.DataType
            dt.Columns.Add(dc2)
            colIndex += 1

        Next

        For Each dr As DataRow In ds.Tables(0).Rows

            dt.ImportRow(dr)

        Next

        Return dt

    End Function


    Public Shared Function ParametrosEtiquetas(ByVal pstrTipo As String, Optional ByVal pstrRpt As String = "0", Optional ByVal pstrConDire As String = "True", Optional ByVal pstrAsamId As String = "0", Optional ByVal pstrGrupo As String = "0", Optional ByVal pstrDistDesde As String = "0", Optional ByVal pstrDistHasta As String = "0", Optional ByVal pstrDevuelve As String = "E", Optional ByVal pstrIncob As String = "0", Optional ByVal pstrMail As String = "", Optional ByVal pstrCorreo As String = "", Optional ByVal pstrFechaDesde As String = "0", Optional ByVal pstrFechaHasta As String = "0", Optional ByVal pstrCate As String = "0", Optional ByVal pstrEsta As String = "0", Optional ByVal pstrSociId As String = "0", Optional ByVal pstrOrden As String = "0", Optional ByVal pstrTeco As String = "0", Optional ByVal pstrSociDesde As String = "0", Optional ByVal pstrSociHasta As String = "0", Optional ByVal pstrCpDesde As String = "", Optional ByVal pstrCpHasta As String = "", Optional ByVal pstrDecaId As String = "", Optional ByVal pstrEstaPFP As String = "0", Optional ByVal pstrDecaEstaId As String = "0", Optional ByVal pstrTarjId As String = "", Optional ByVal lstrAnio As String = "", Optional ByVal lstrPerio As String = "", Optional ByVal pstrSR As String = "", Optional ByVal pstrSociLista As String = "") As String

        Dim lstrRpt As String = ""

        lstrRpt = "&tipo=" & pstrTipo
        lstrRpt = lstrRpt & "&rptf_id=" & pstrRpt
        lstrRpt = lstrRpt & "&ConDireccion=" & pstrConDire
        lstrRpt = lstrRpt & "&AsamId=" & pstrAsamId
        lstrRpt = lstrRpt & "&Grupo=" & pstrGrupo
        lstrRpt = lstrRpt & "&DistDesde=" & pstrDistDesde
        lstrRpt = lstrRpt & "&DistHasta=" & pstrDistHasta
        lstrRpt = lstrRpt & "&Devuelve=" & pstrDevuelve
        lstrRpt = lstrRpt & "&Incob=" & pstrIncob
        If pstrMail <> "" Then
            lstrRpt = lstrRpt & "&Mail=" & pstrMail
        End If
        If pstrCorreo <> "" Then
            lstrRpt = lstrRpt & "&Correo=" & pstrCorreo
        End If
        lstrRpt += "&fechadesde=" + pstrFechaDesde
        lstrRpt += "&fechaAl=" + pstrFechaHasta
        lstrRpt += "&soci_cate_id=" + pstrCate
        lstrRpt += "&PFP_esta=" + pstrEstaPFP
        lstrRpt += "&soci_esta_id=" + pstrEsta
        If pstrSociId <> "" Then
            lstrRpt += "&soci_id=" + pstrSociId
        End If
        lstrRpt += "&orden=" + pstrOrden
        If pstrTeco <> "0" And pstrTeco <> "" Then
            lstrRpt += "&teco_id=" + pstrTeco
        End If
        If pstrSociDesde <> "0" Then
            lstrRpt += "&soci_desde=" + pstrSociDesde
        End If
        If pstrSociHasta <> "0" Then
            lstrRpt += "&soci_hasta=" + pstrSociHasta
        End If
        If pstrCpDesde <> "" Then
            lstrRpt += "&cp_desde=" + pstrCpDesde
        End If
        If pstrCpHasta <> "" Then
            lstrRpt += "&cp_hasta=" + pstrCpHasta
        End If
        If pstrDecaId <> "" Then
            lstrRpt += "&deca_id=" + pstrDecaId
        End If
        If pstrDecaEstaId <> "" Then
            lstrRpt += "&esta_id=" + pstrDecaEstaId
        End If
        If pstrTarjId <> "" Then
            lstrRpt += "&tarj_id=" + pstrTarjId
        End If
        If lstrAnio <> "" Then
            lstrRpt += "&deca_anio=" + lstrAnio
        End If
        If lstrPerio <> "" Then
            lstrRpt += "&deca_perio=" + lstrPerio
        End If
        If pstrSociLista <> "" Then
            lstrRpt += "&soci_lista=" + pstrSociLista
        Else
            lstrRpt += "&soci_lista=;"
        End If
        If pstrSR <> "" Then
            lstrRpt += "&sr=" + pstrSR
        Else
            lstrRpt += "&sr=R"
        End If

        Return lstrRpt

    End Function

    Public Shared Function ObtenerValorCampo(ByVal pstrConn As String, ByVal pstrTabla As String, ByVal pstrCampo As String, Optional ByVal pstrFiltro As String = "") As String

        Dim lDsDatos As DataSet
        If pstrFiltro = "" Then
            lDsDatos = clsSQLServer.gObtenerEstruc(pstrConn, pstrTabla)
        Else
            lDsDatos = clsSQLServer.gObtenerEstruc(pstrConn, pstrTabla, pstrFiltro)
        End If
        Dim lstrValor As String
        lstrValor = ""
        If lDsDatos.Tables(0).Rows.Count > 0 Then
            lstrValor = IIf(lDsDatos.Tables(0).Rows(0).Item(pstrCampo) Is DBNull.Value, "", lDsDatos.Tables(0).Rows(0).Item(pstrCampo))

        End If

        Return lstrValor
    End Function

    Public Shared Function ObtenerDescCiclo(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lstrRet As New System.Text.StringBuilder

        If pstrArgs = "" Then
            Return ("")
        Else
            lstrCmd.Append("exec ciclos_consul ")
            lstrCmd.Append(pstrArgs)
            ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

            If ldsDatos.Tables(0).Rows.Count = 0 Then
                Return ("")
            Else
                With ldsDatos.Tables(0).Rows(0)
                    lstrRet.Append("A�o: ")
                    lstrRet.Append(.Item("cicl_anio").ToString)
                    lstrRet.Append(" - Ciclo: ")
                    lstrRet.Append(.Item("cicl_ciclo").ToString)
                    lstrRet.Append(" - Carrera: ")
                    lstrRet.Append(.Item("_carr_desc").ToString)
                    lstrRet.Append(" - Turno: ")
                    lstrRet.Append(.Item("_tucu_desc").ToString.ToUpper)
                    lstrRet.Append(vbCrLf)
                    lstrRet.Append("Inic.Cursada: ")
                    lstrRet.Append(CDate(.Item("cicl_curs_inic_fecha")).ToString("dd/MM/yyyy"))
                    lstrRet.Append(" - Fin Cursada: ")
                    lstrRet.Append(CDate(.Item("cicl_curs_fina_fecha")).ToString("dd/MM/yyyy"))
                End With

                Return (lstrRet.ToString)
            End If
        End If
    End Function

    Public Shared Function BuscarCriadorDeriv(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lstrCmdCopropiedad As String
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'valor;tabla;campo_busc;InseId;CateTitu;CatePode;EstaId
        Dim lstrRet As New System.Text.StringBuilder
        Dim lstrCriaId As String
        Dim ldsDatosCopropiedad As DataSet
        Dim lboolDeshabilitaCoPropiedad As Boolean

        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            lstrCmd.Append("exec " + lvstrArgs(1) + "_consul ")

            If lvstrArgs(2) <> "id" Then    'si es una consulta directa por id no le pasa el nombre del campo
                lstrCmd.Append("@" + lvstrArgs(2) + "=")
            End If

            lstrCmd.Append(lvstrArgs(0))

            Select Case lvstrArgs(1).ToLower
                Case "clientes"
                    If lvstrArgs(7) = 1 Then
                        lstrCmd.Append(", @busq=null")
                    Else
                        lstrCmd.Append(", @busq=1")
                    End If

                Case "alumnos"
                    lstrCmd.Append(", @alum_inse_id=" + lvstrArgs(3))

                Case "socios"
                    If lvstrArgs(4) <> "" Then lstrCmd.Append(", @cate_titu=" + lvstrArgs(4))
                    If lvstrArgs(5) <> "" Then lstrCmd.Append(", @cate_pode=" + lvstrArgs(5))
                    If lvstrArgs(6) <> "" Then lstrCmd.Append(", @esta_id=" + lvstrArgs(6))

                Case "criadores"
                    Try
                        If lvstrArgs(8) <> "" Then
                            lstrCmd.Append(", @cria_raza_id=" + lvstrArgs(8))
                            '   lstrCmd.Append(", @cria_raza_id=" + lvstrArgs(8))
                        End If
                    Catch ex As Exception

                    End Try

                    lstrCmd.Append(", @busq=1")

            End Select

            ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

            If ldsDatos.Tables(0).Rows.Count = 0 Then
                Return ("")
            Else
                With ldsDatos.Tables(0).Rows(0)
                    Select Case lvstrArgs(1).ToLower
                        Case "clientes"
                            lstrRet.Append(.Item("clie_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("clie_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("clie_apel").ToString.TrimEnd)
                            If .Item("clie_nomb").ToString.Trim.TrimEnd <> "" Then
                                lstrRet.Append(", ")
                                lstrRet.Append(.Item("clie_nomb").ToString.Trim.TrimEnd)
                            End If
                            lstrRet.Append("|")
                            lstrRet.Append("Cliente:")
                            lstrRet.Append(.Item("clie_apel").ToString.Trim.TrimEnd)
                            If .Item("clie_nomb").ToString.Trim.TrimEnd <> "" Then
                                lstrRet.Append(", ")
                                lstrRet.Append(.Item("clie_nomb").ToString.Trim.TrimEnd)
                            End If
                            lstrRet.Append(" - Fantasia:")
                            lstrRet.Append(.Item("clie_fanta").ToString.Trim)

                            If Not .IsNull("_soci_nume") Then
                                lstrRet.Append(" - Socio:")
                                lstrRet.Append(.Item("_soci_nume").ToString.Trim)
                            End If

                            If Not .IsNull("clie_cuit") AndAlso .Item("clie_cuit") <> 0 Then
                                lstrRet.Append(" - CUIT:")
                                lstrRet.Append(.Item("_cuit").ToString.Trim)
                            End If

                        Case "alumnos"
                            lstrRet.Append(.Item("alum_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("alum_lega").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_clie_apel").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append("Alumno:")
                            lstrRet.Append(.Item("_clie_apel").ToString.Trim)

                            lstrRet.Append(" - Legajo:")
                            lstrRet.Append(.Item("alum_lega").ToString.Trim)
                            lstrRet.Append(" - Nro.Doc.:")
                            lstrRet.Append(.Item("_clie_docu_nume").ToString.Trim)

                        Case "socios"
                            lstrRet.Append(.Item("soci_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("soci_nume").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_clie_apel").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append("Socio:")
                            lstrRet.Append(.Item("_clie_apel").ToString.Trim)
                            lstrRet.Append(" - Nro.Socio:")
                            lstrRet.Append(.Item("soci_nume").ToString.Trim)
                            lstrRet.Append(" - Nro.Doc.:")
                            lstrRet.Append(.Item("_clie_docu_nume").ToString.Trim)
                            lstrRet.Append(" - Categor�a:")
                            lstrRet.Append(.Item("_cate_desc").ToString.Trim)

                        Case "criadores"
                            lstrCriaId = .Item("cria_id").ToString.Trim
                            lstrRet.Append(.Item("cria_id").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("cria_nume").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_clie_apel").ToString.Trim)
                            lstrRet.Append("| Raza: ")
                            lstrRet.Append(.Item("_raza_desc").ToString)
                            lstrRet.Append(" - ")
                            lstrRet.Append("Cliente: ")
                            lstrRet.Append(.Item("cria_clie_id").ToString.Trim)
                            lstrRet.Append(" ")
                            lstrRet.Append(.Item("_clie_apel").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("cria_raza_id").ToString)
                    End Select
                End With

                lstrCmdCopropiedad = " exec criadores_por_cliente_raza_busq @cria_id = " & lstrCriaId
                ldsDatosCopropiedad = clsSQLServer.gExecuteQuery(pstrConn, lstrCmdCopropiedad)
                lboolDeshabilitaCoPropiedad = False
                If ldsDatosCopropiedad.Tables(0).Rows.Count > 0 Then
                    If ldsDatosCopropiedad.Tables(0).Rows(0).Item(0) = 0 Then
                        lboolDeshabilitaCoPropiedad = True
                    End If
                End If


                lstrRet.Append("|")
                lstrRet.Append(lboolDeshabilitaCoPropiedad.ToString)

                Return (lstrRet.ToString)
            End If
        End If
    End Function

    Public Shared Function BuscarClieDeriv(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'valor;tabla;campo_busc;InseId;CateTitu;CatePode;EstaId
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            lstrCmd.Append("exec " + lvstrArgs(1) + "_consul ")

            If lvstrArgs(2) <> "id" Then    'si es una consulta directa por id no le pasa el nombre del campo
                lstrCmd.Append("@" + lvstrArgs(2) + "=")
            End If

            lstrCmd.Append(lvstrArgs(0))

            Select Case lvstrArgs(1).ToLower
                Case "clientes"
                    If lvstrArgs(7) = 1 Then
                        lstrCmd.Append(", @busq=null")
                    Else
                        lstrCmd.Append(", @busq=1")
                    End If

                Case "alumnos"
                    lstrCmd.Append(", @alum_inse_id=" + lvstrArgs(3))

                Case "socios"
                    If lvstrArgs(4) <> "" Then lstrCmd.Append(", @cate_titu=" + lvstrArgs(4))
                    If lvstrArgs(5) <> "" Then lstrCmd.Append(", @cate_pode=" + lvstrArgs(5))
                    If lvstrArgs(6) <> "" Then lstrCmd.Append(", @esta_id=" + lvstrArgs(6))

                Case "criadores"
                    Try
                        If (lvstrArgs.Length - 1) = 8 Then
                            If lvstrArgs(8) <> "" Then
                                lstrCmd.Append(", @cria_raza_id=" + lvstrArgs(8))

                            End If
                        End If

                    Catch ex As Exception

                    End Try

                    lstrCmd.Append(", @busq=1")

            End Select

            ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

            If ldsDatos.Tables(0).Rows.Count = 0 Then
                Return ("")
            Else
                With ldsDatos.Tables(0).Rows(0)
                    Select Case lvstrArgs(1).ToLower
                        Case "clientes"
                            lstrRet.Append(.Item("clie_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("clie_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("clie_apel").ToString.TrimEnd)
                            If .Item("clie_nomb").ToString.Trim.TrimEnd <> "" Then
                                lstrRet.Append(", ")
                                lstrRet.Append(.Item("clie_nomb").ToString.Trim.TrimEnd)
                            End If
                            lstrRet.Append("|")
                            lstrRet.Append("Cliente:")
                            lstrRet.Append(.Item("clie_apel").ToString.Trim.TrimEnd)
                            If .Item("clie_nomb").ToString.Trim.TrimEnd <> "" Then
                                lstrRet.Append(", ")
                                lstrRet.Append(.Item("clie_nomb").ToString.Trim.TrimEnd)
                            End If
                            lstrRet.Append(" - Fantasia:")
                            lstrRet.Append(.Item("clie_fanta").ToString.Trim)

                            If Not .IsNull("_soci_nume") Then
                                lstrRet.Append(" - Socio:")
                                lstrRet.Append(.Item("_soci_nume").ToString.Trim)
                            End If

                            If Not .IsNull("clie_cuit") AndAlso .Item("clie_cuit") <> 0 Then
                                lstrRet.Append(" - CUIT:")
                                lstrRet.Append(.Item("_cuit").ToString.Trim)
                            End If

                        Case "alumnos"
                            lstrRet.Append(.Item("alum_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("alum_lega").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_clie_apel").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append("Alumno:")
                            lstrRet.Append(.Item("_clie_apel").ToString.Trim)

                            lstrRet.Append(" - Legajo:")
                            lstrRet.Append(.Item("alum_lega").ToString.Trim)
                            lstrRet.Append(" - Nro.Doc.:")
                            lstrRet.Append(.Item("_clie_docu_nume").ToString.Trim)

                        Case "socios"
                            lstrRet.Append(.Item("soci_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("soci_nume").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_clie_apel").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append("Socio:")
                            lstrRet.Append(.Item("_clie_apel").ToString.Trim)
                            lstrRet.Append(" - Nro.Socio:")
                            lstrRet.Append(.Item("soci_nume").ToString.Trim)
                            lstrRet.Append(" - Nro.Doc.:")
                            lstrRet.Append(.Item("_clie_docu_nume").ToString.Trim)
                            lstrRet.Append(" - Categor�a:")
                            lstrRet.Append(.Item("_cate_desc").ToString.Trim)

                        Case "criadores"
                            lstrRet.Append(.Item("cria_id").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("cria_nume").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_clie_apel").ToString.Trim)
                            lstrRet.Append("| Raza: ")
                            lstrRet.Append(.Item("_raza_desc").ToString)
                            lstrRet.Append(" - ")
                            lstrRet.Append("Cliente: ")
                            lstrRet.Append(.Item("cria_clie_id").ToString.Trim)
                            lstrRet.Append(" ")
                            lstrRet.Append(.Item("_clie_apel").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("cria_raza_id").ToString)

                    End Select
                End With

                Return (lstrRet.ToString)
            End If
        End If
    End Function

    Public Shared Function BuscarCriaDeriv(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'valor;tabla;campo_busc;InseId;CateTitu;CatePode;EstaId
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            lstrCmd.Append("exec " + lvstrArgs(1) + "_consul ")

            If lvstrArgs(2) <> "id" Then    'si es una consulta directa por id no le pasa el nombre del campo
                lstrCmd.Append("@" + lvstrArgs(2) + "=")
            End If

            lstrCmd.Append(lvstrArgs(0))

            Select Case lvstrArgs(1).ToLower
                Case "criaderos"
                    lstrCmd.Append(", @busq=1")
            End Select

            ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

            If ldsDatos.Tables(0).Rows.Count = 0 Then
                Return ("")
            Else
                With ldsDatos.Tables(0).Rows(0)
                    Select Case lvstrArgs(1).ToLower
                        Case "criaderos"
                            lstrRet.Append(.Item("crdr_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("crdr_nume").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("crdr_desc").ToString.TrimEnd)
                            lstrRet.Append("|")
                            lstrRet.Append("Cliente:")
                            lstrRet.Append(.Item("crdr_clie_id").ToString.Trim.TrimEnd)
                            lstrRet.Append(" - ")
                            lstrRet.Append(.Item("_cliente").ToString.Trim.TrimEnd)
                            If Not .IsNull("_cuit") AndAlso .Item("_cuit") <> "" Then
                                lstrRet.Append(" - CUIT:")
                                lstrRet.Append(.Item("_cuit").ToString.Trim)
                            End If
                    End Select
                End With

                Return (lstrRet.ToString)
            End If
        End If
    End Function

    Public Shared Function BuscarProdDeriv(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'valor;tabla;campo_busc;CateTitu;CatePode;EstaId
        Dim lstrRet As New System.Text.StringBuilder

        Try
            'Armo el Query.
            lstrCmd.Append("exec " + lvstrArgs(1) + "_consul ")

            'Si tiene el ID de Producto, busco directamente por el prdt_id.
            'De lo contrario, busco por los filtros que tengo.
            If lvstrArgs(2) = "prdt_id" Then
                lstrCmd.Append("@" + lvstrArgs(2) + "=")
                lstrCmd.Append(lvstrArgs(3))
            Else

                If lvstrArgs(2) <> "id" Then  'si es una consulta directa por id no le pasa el nombre del campo
                    lstrCmd.Append("@" + lvstrArgs(2) + "=")
                End If

                'HBA.
                If lvstrArgs(0) <> "" Then
                    lstrCmd.Append(lvstrArgs(0))
                Else
                    lstrCmd.Append("0")
                End If

                Select Case lvstrArgs(1).ToLower
                    Case "productos"
                        'Busqueda.
                        If lvstrArgs(6) = "1" Then
                            lstrCmd.Append(", @busq=null")
                        Else
                            lstrCmd.Append(", @busq=1")
                        End If
                        If lvstrArgs(2) <> "id" Then
                            'Raza.
                            lstrCmd.Append(", @prdt_raza_id=" + lvstrArgs(7))
                            'Sexo.
                            lstrCmd.Append(", @prdt_sexo=" + lvstrArgs(8))
                            'Nro. Extranjero.
                            If lvstrArgs(9) <> "" Then
                                lstrCmd.Append(", @prdt_asoc_nume=" + lvstrArgs(9))
                            Else
                                lstrCmd.Append(", @prdt_asoc_nume=0")
                            End If
                            'Asociaci�n.
                            If lvstrArgs(10) <> "" Then
                                lstrCmd.Append(", @prdt_asoc_id=" + lvstrArgs(10))
                            Else
                                lstrCmd.Append(", @prdt_asoc_id=0")
                            End If
                            'RP.
                            If lvstrArgs.Length.ToString > 11 Then
                                If lvstrArgs(11) <> "" Then
                                    lstrCmd.Append(", @prdt_rp='" + lvstrArgs(11) + "'")
                                End If
                            End If
                            'Criador.
                            If lvstrArgs.Length.ToString > 12 Then
                                If lvstrArgs(12) <> "" Then
                                    lstrCmd.Append(", @cria_id=" + lvstrArgs(12))
                                End If
                            End If
                            'RP Extranjero.
                            If lvstrArgs.Length.ToString >= 13 Then
                                If lvstrArgs(13) <> "" Then
                                    lstrCmd.Append(", @prdt_rp_extr='" + lvstrArgs(13) + "'")
                                End If
                            End If
                        End If
                    Case "productoxsranume"
                        lstrCmd.Append(", @trpr_tram_id=" & lvstrArgs(3))
                    Case "productos_control"
                        'Busqueda.
                        If lvstrArgs(6) = "1" Then
                            lstrCmd.Append(", @busq=null")
                        Else
                            lstrCmd.Append(", @busq=1")
                        End If
                        If lvstrArgs(2) <> "id" Then
                            'Raza.
                            lstrCmd.Append(", @prdt_raza_id=" + lvstrArgs(7))
                            'Sexo.
                            lstrCmd.Append(", @prdt_sexo=" + lvstrArgs(8))
                            'Nro. Extranjero.
                            If lvstrArgs(9) <> "" Then
                                lstrCmd.Append(", @prdt_asoc_nume=" + lvstrArgs(9))
                            Else
                                lstrCmd.Append(", @prdt_asoc_nume=0")
                            End If
                            'Asociaci�n.
                            If lvstrArgs(10) <> "" Then
                                lstrCmd.Append(", @prdt_asoc_id=" + lvstrArgs(10))
                            Else
                                lstrCmd.Append(", @prdt_asoc_id=0")
                            End If
                            'RP.
                            If lvstrArgs.Length.ToString > 11 Then
                                If lvstrArgs(11) <> "" Then
                                    lstrCmd.Append(", @prdt_rp='" + lvstrArgs(11) + "'")
                                End If
                            End If
                            'Criador.
                            If lvstrArgs.Length.ToString > 12 Then
                                If lvstrArgs(12) <> "" Then
                                    lstrCmd.Append(", @cria_id=" + lvstrArgs(12))
                                End If
                            End If
                            'RP Extranjero.
                            If lvstrArgs.Length.ToString >= 13 Then
                                If lvstrArgs(13) <> "" Then
                                    lstrCmd.Append(", @prdt_rp_extr='" + lvstrArgs(13) + "'")
                                End If
                            End If
                        End If
                End Select
            End If

            'Ejecuto el Query.
            ' Dario 2014-11-17 se agraga timeout
            ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString, 9999)

            'Si NO devuelve ning�n registro, devuelvo vac�o.
            If ldsDatos.Tables(0).Rows.Count = 0 Then
                Return ("")

                'Si devuelve m�s de un registro, abro la pantalla de Productos.aspx a modo de consulta.
            ElseIf ldsDatos.Tables(0).Rows.Count > 1 Then
                Return ("AbrirPopupPaginaProductos")

                'Si devuelve un �nico registro, devuelvo los datos separados por "|".
            Else
                With ldsDatos.Tables(0).Rows(0)
                    Select Case lvstrArgs(1).ToLower
                        Case "productos"
                            lstrRet.Append(.Item("prdt_id").ToString) '0
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_raza_id").ToString.Trim) '1
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_nume").ToString.Trim) '2
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_nomb").ToString) '3
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_sexo").ToString) '4
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_desc").ToString) '5
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_rp").ToString) '6
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_ori_asoc_id").ToString) '7
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_sexo").ToString) '8
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_sra_nume").ToString) '9
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_regt_id").ToString) '10
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_rp_extr").ToString) '11
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_rp_extr").ToString) '12
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_condicional").ToString) '13
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_apodo").ToString) '14
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_vari_id").ToString) '15
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_prin_bitTrim").ToString) '16
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_naci_fecha").ToString) '17
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_px").ToString) '18
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_cria_nume").ToString) '19
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_criador").ToString) '20
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_a_pela_id").ToString) '21
                        Case "productoxsranume"
                            lstrRet.Append(.Item("prdt_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_raza_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_sexo").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_nomb").ToString.Trim)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_px").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_rp_nume").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_rp_extr").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("padre_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("madre_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_cria_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prta_nume").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prta_ares_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_ori_asoc_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_numero").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_prop_clie_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("padre_asoc_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("NumeroPadre").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("madre_asoc_id").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("NumeroMadre").ToString)
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_regt_id").ToString)
                        Case "productos_control"
                            lstrRet.Append(.Item("prdt_id").ToString) '0
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_raza_id").ToString.Trim) '1
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_nume").ToString.Trim) '2
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_nomb").ToString) '3
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_sexo").ToString) '4
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_desc").ToString) '5
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_rp").ToString) '6
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_ori_asoc_id").ToString) '7
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_sexo").ToString) '8
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_sra_nume").ToString) '9
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_regt_id").ToString) '10
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_rp_extr").ToString) '11
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_rp_extr").ToString) '12
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_condicional").ToString) '13
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_apodo").ToString) '14
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_vari_id").ToString) '15
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_prin_bitTrim").ToString) '16
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_naci_fecha").ToString) '17
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_px").ToString) '18
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_cria_nume").ToString) '19
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("_criador").ToString) '20
                            lstrRet.Append("|")
                            lstrRet.Append(.Item("prdt_a_pela_id").ToString) '21
                    End Select
                End With

                'Devuelvo los datos obtenidos separados por "|".
                Return (lstrRet.ToString)
            End If

        Catch ex As Exception
            clsError.gManejarError(ex)
        End Try
    End Function

    Public Shared Function ObtenerFechaVigencia(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim lintVigeDias As Integer = clsSQLServer.gParaConsul(pstrConn).Tables(0).Rows(0).Item("para_soin_vige_dias")
        Dim ldatFecha As DateTime

        If pstrArgs = "" Then
            Return ("")
        Else
            ldatFecha = clsFormatear.gFormatFechaDateTime(pstrArgs)
            ldatFecha = ldatFecha.AddDays(lintVigeDias)
            Return (ldatFecha.ToString("dd/MM/yyyy"))
        End If
    End Function

    Public Shared Function ObtenerIdAlumXCliente(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id;inse_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then  'clie_id
            Return ("")
        Else
            lstrCmd.Append("exec alumnos_consul")
            lstrCmd.Append(" @alum_clie_id=")
            lstrCmd.Append(lvstrArgs(0))
            lstrCmd.Append(", @alum_inse_id=")
            lstrCmd.Append(lvstrArgs(1))
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        If ldsDatos.Tables(0).Rows.Count = 0 Then
            Return ("")
        Else
            Return (ldsDatos.Tables(0).Rows(0).Item("alum_id"))
        End If
    End Function

    Public Shared Function Tarjetas(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim ldouresp As Double
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)


        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            ldouresp = mobj.Tarjetas("exec tarjetas_clientes_busq " & lvstrArgs(0), pstrConn, 0)

        End If


        If ldouresp = 0 Then
            Return ("")
        Else
            Return (ldouresp)
        End If
    End Function

    Public Shared Function Turnos(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim ldouresp As Double
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)

        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            ldouresp = mobj.Tarjetas("exec lab_turnos_ctrol_busq " & lvstrArgs(0), pstrConn, 0)
        End If

        If ldouresp = 0 Then
            Return ("")
        Else
            Return (ldouresp)
        End If
    End Function

    Public Shared Function EstadoSaldos(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lstrresp As String
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)

        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            lstrresp = mobj.EstadoSaldos(pstrConn, lvstrArgs(0), clsFormatear.gFormatFechaDateTime(lvstrArgs(1)))
        End If

        Return lstrresp
    End Function
    Public Shared Function QuitarLetras(ByVal Texto As String) As String
        For Each _caracter As Char In Texto
            If IsNumeric(_caracter) Then
                QuitarLetras = QuitarLetras & _caracter
            End If
        Next
        Return QuitarLetras
    End Function

    Public Shared Function ValidarFechaMayor(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim lstrresp As String
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        lstrresp = ""
        If lvstrArgs(0) = "" Then   ' la fecha  rrgg es nula
            Return ("")
        Else

            Dim ldateRRGG As Date = clsFormatear.gFormatFechaDateTime(lvstrArgs(0))
            Dim ldateValor As Date = clsFormatear.gFormatFechaDateTime(lvstrArgs(1))

            If ldateRRGG > ldateValor Then
                lstrresp = True
            End If


        End If

        Return lstrresp

    End Function

    Public Shared Function AranRRGGSobretasa(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lstrresp As String
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)


        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            'primero debe llamarse a la funcion AranRRGG 
            lstrresp = mobj.pAranRRGGSobretasa
        End If

        Return lstrresp

    End Function

    Public Shared Function AranRRGGDescFecha(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lstrresp As String
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)


        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            'primeto debe llamarse a la funcion AranRRGG 
            lstrresp = mobj.pAranRRGGDescFecha
        End If

        Return lstrresp

    End Function

    Public Shared Function LetraComprob(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lstrresp As String
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder
        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)
        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            Dim lstrTipoIva As String = mobj.ObtenerValorCampo(pstrConn, "clientes", "clie_ivap_id", lvstrArgs(0))
            mobj.LetraComprob(pstrConn, lstrTipoIva)
            lstrresp = mobj.pLetraComprob
        End If

        Return lstrresp

    End Function

    Public Shared Function AranRRGG(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lstrresp As String
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)


        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            mobj.AranRRGG(pstrConn, lvstrArgs(0))
            lstrresp = mobj.pAranRRGGRepresAnim
        End If

        Return lstrresp

    End Function

    Public Shared Function AranRRGGValidacionFecha(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lstrresp As String
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)


        If lvstrArgs(0) = "" Then
            Return ("")
        Else

            If mobj.pAranRRGGDescFecha <> "" And lvstrArgs(0) <> "" Then
                lstrresp = mobj.AranRRGGValidacionFecha(pstrConn, lvstrArgs(0), lvstrArgs(1), lvstrArgs(2))
                lstrresp = mobj.pAranRRGGActa
            Else
                lstrresp = ""
            End If


        End If

        Return lstrresp

    End Function

    Public Shared Function Especiales(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lstrresp As String
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)


        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            lstrresp = mobj.Especiales(pstrConn, lvstrArgs(0))
        End If

        Return lstrresp

    End Function

    Public Shared Function CalculoConcepto(ByVal pPagina As System.Web.UI.Page, ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrArgs As String) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim ldouImporte, ldouTasa, ldouImporteTasaIVA, ldouSobreTasa, ldouSobreTasaImporte As Double
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pPagina, pstrConn, pstrUserId)
        ' ldouImporteiva = document.all("txtImporteConcep").value - document.all("txtImporteConcep").value / (1 + ldouTasaIVA / 100)
        'sFiltro = document.all("cmbConcep").value + ";" + document.all("hdnFechaIva").value;

        'lvstrArgs(0)  document.all("cmbConcep").value 
        'lvstrArgs(1)  document.all("hdnFechaIva").value
        'lvstrArgs(2)  document.all("txtImporteConcep").value


        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            'iva Tasa
            ldouTasa = mobj.ObtenerValorCampo(pstrConn, "conceptosX", "valor_tasa", lvstrArgs(0) + "," + clsSQLServer.gFormatArg(lvstrArgs(1), SqlDbType.SmallDateTime))
            lstrRet.Append(ldouTasa)
            lstrRet.Append(";")
            'importe del iva
            ldouImporteTasaIVA = 0
            If ldouTasa > 0 Then
                ldouImporteTasaIVA = Format(lvstrArgs(2) * (ldouTasa / 100), "###0.00")
                'ldouImporte = Format(lvstrArgs(2) - (lvstrArgs(2) / (1 + ldouTasa / 100)), "###0.00")
            End If
            lstrRet.Append(ldouImporteTasaIVA)
            lstrRet.Append(";")
            'importe sin iva
            lstrRet.Append(lvstrArgs(2))
            lstrRet.Append(";")
            'importe neto
            ldouImporte = (lvstrArgs(2) + ldouImporteTasaIVA)
            lstrRet.Append(ldouImporte)
            lstrRet.Append(";")
            'importe sobretasa
            ldouSobreTasa = clsSQLServer.gCampoValorConsul(pstrConn, "sobre_tasa_porcen_consul", "sobre_tasa")
            ldouSobreTasa = Format(ldouSobreTasa, "###0.00")
            ldouSobreTasaImporte = 0
            If ldouTasa > 0 And ldouSobreTasa > 0 Then
                ldouSobreTasaImporte = Format(lvstrArgs(2) * (ldouSobreTasa / 100), "###0.00")
            Else
                ldouSobreTasa = 0
            End If
            lstrRet.Append(ldouSobreTasaImporte)
            lstrRet.Append(";")
            'tasa sobretasa
            lstrRet.Append(ldouSobreTasa)
        End If

        'vsRet[0];//TASA IVA
        'vsRet[1];//IMPORTE DEL IVA
        'vsRet[2];//IMPORTE DEL CONCEPTO SIN IVA
        'vsRet[3];//IMPORTE DEL CONCEPTO CON IVA
        'vsRet[4];//IMPORTE DE LA SOBRETASA
        'vsRet[5];//TASA SOBRETASA

        Return lstrRet.ToString

    End Function

    Public Shared Function CalculoConcepto(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim ldouImporte, ldouTasa, ldouImporteTasaIVA, ldouSobreTasa, ldouSobreTasaImporte As Double
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)
        ' ldouImporteiva = document.all("txtImporteConcep").value - document.all("txtImporteConcep").value / (1 + ldouTasaIVA / 100)
        'sFiltro = document.all("cmbConcep").value + ";" + document.all("hdnFechaIva").value;

        'lvstrArgs(0)  document.all("cmbConcep").value 
        'lvstrArgs(1)  document.all("hdnFechaIva").value
        'lvstrArgs(2)  document.all("txtImporteConcep").value


        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            'iva Tasa
            ldouTasa = mobj.ObtenerValorCampo(pstrConn, "conceptosX", "valor_tasa", lvstrArgs(0) + "," + clsSQLServer.gFormatArg(lvstrArgs(1), SqlDbType.SmallDateTime))
            lstrRet.Append(ldouTasa)
            lstrRet.Append(";")
            'importe del iva
            ldouImporteTasaIVA = 0
            If ldouTasa > 0 Then
                'importe iva
                ldouImporteTasaIVA = Format(lvstrArgs(2) * (ldouTasa / 100), "###0.00")
                'ldouImporte = Format(lvstrArgs(2) - (lvstrArgs(2) / (1 + ldouTasa / 100)), "###0.00")
            End If
            lstrRet.Append(ldouImporteTasaIVA)
            lstrRet.Append(";")
            'importe sin iva
            lstrRet.Append(lvstrArgs(2))
            lstrRet.Append(";")
            'importe neto
            ldouImporte = (lvstrArgs(2) + ldouImporteTasaIVA)
            lstrRet.Append(ldouImporte)
            lstrRet.Append(";")
            'importe sobretasa
            ldouSobreTasa = clsSQLServer.gCampoValorConsul(pstrConn, "sobre_tasa_porcen_consul", "sobre_tasa")
            ldouSobreTasa = Format(ldouSobreTasa, "###0.00")
            ldouSobreTasaImporte = 0
            If ldouTasa > 0 And ldouSobreTasa > 0 Then
                ldouSobreTasaImporte = Format(lvstrArgs(2) * (ldouSobreTasa / 100), "###0.00")
            Else
                ldouSobreTasa = 0
            End If
            lstrRet.Append(ldouSobreTasaImporte)
            lstrRet.Append(";")
            'tasa sobretasa
            lstrRet.Append(ldouSobreTasa)
        End If

        'vsRet[0];//TASA IVA
        'vsRet[1];//IMPORTE DEL IVA
        'vsRet[2];//IMPORTE DEL CONCEPTO SIN IVA
        'vsRet[3];//IMPORTE DEL CONCEPTO CON IVA
        'vsRet[4];//IMPORTE DE LA SOBRETASA
        'vsRet[5];//TASA SOBRETASA

        Return lstrRet.ToString

    End Function

    Public Shared Function CargarAutorizaciones(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        '0 pintClie 
        '1 pstrFechaValor
        '2 pintConv
        '3 pintListaPrecios
        '4 pintActiv

        Dim mobj As SRA_Neg.Facturacion
        Dim lvstrArgs() As String = pstrArgs.Split(";")
        Dim lstrRet As New System.Text.StringBuilder
        mobj = New SRA_Neg.Facturacion(pstrConn, "", "", pSession(Constantes.gTab_Comprobantes), pSession)
        ' Dario 2022-10-12 0751 
        '                                           38443;                11/10/2022        ;               ;           ;6          ;pstrOpc=
        mobj.CargarAutorizaciones(pstrConn, CType(lvstrArgs(0), Integer), lvstrArgs(1), lvstrArgs(2), IIf(lvstrArgs(3).Trim().Length = 0, "0", lvstrArgs(3)), lvstrArgs(4))
        Return ("")
    End Function

    Public Shared Function GuardarAutorizaciones(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState, ByVal pintOpcion As Integer) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'auti_id;auto;usua_id;comentarios
        Dim lstrRet As New System.Text.StringBuilder
        Dim lintRet As Integer

        mobj = New SRA_Neg.Facturacion(pstrConn, pstrUserId, "", pSession(Constantes.gTab_Comprobantes), pSession)
        lintRet = mobj.GuardarAutorizaciones(CType(lvstrArgs(0), Integer), CType(lvstrArgs(1), Integer), CType(lvstrArgs(2), Integer), pintOpcion, lvstrArgs(3))
        Return (lintRet)
    End Function

    Public Shared Function SeleccionaComprobante(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lvstrArgs() As String = pstrArgs.Split(";")
        Dim lstrRet As String

        mobj = New SRA_Neg.Facturacion(pstrConn, "", "", pSession(Constantes.gTab_Comprobantes), pSession)
        lstrRet = mobj.SeleccionaComprobante(lvstrArgs(0), lvstrArgs(1))
        Return (lstrRet.ToString)
    End Function

    Public Shared Function CambioTipoPago(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim lvstrArgs() As String = pstrArgs.Split(";")
        Dim lstrRet As String

        mobj = New SRA_Neg.Facturacion(pstrConn, "", "", pSession(Constantes.gTab_Comprobantes), pSession)
        mobj.PagoCtaCte(lvstrArgs(0))
        mobj.Proforma()
        Return ("")
    End Function

    Public Shared Function CalcularImporteCuota(ByVal pstrArgs As String) As String

        Dim lvstrArgs() As String = pstrArgs.Split(";")
        Dim lstrRet As New System.Text.StringBuilder
        Dim ldouRecaCtaCte, ldouRecaTarj As Double

        If lvstrArgs(0) = "" Then
            Return ("")
        End If

        lstrRet.Append(Format(CType(lvstrArgs(0), Double) * CType(lvstrArgs(1), Double) / 100, "###0.00"))

        Return lstrRet.ToString

    End Function

    Public Shared Function Recargos(ByVal pstrArgs As String) As String

        Dim lvstrArgs() As String = pstrArgs.Split(";")
        Dim lstrRet As New System.Text.StringBuilder
        Dim ldouRecaCtaCte, ldouRecaTarj As Double

        'lvstrArgs(0)  document.all("txtImpoSoci").value
        'lvstrArgs(1)  document.all("txtImpoNoSoci").value
        'lvstrArgs(2)  document.all("txtImpoAdhe").value
        'lvstrArgs(3)  document.all("hdnRecaCtaCte").value
        'lvstrArgs(4)  document.all("hdnRecaTarj").value



        If lvstrArgs(3) = "" Or lvstrArgs(3) = "0" Then
            ldouRecaCtaCte = 1
        Else
            ldouRecaCtaCte = (1 + (Math.Round(CType(lvstrArgs(3), Double), 2) / 100))
        End If
        If lvstrArgs(4) = "" Or lvstrArgs(4) = "0" Then
            ldouRecaTarj = 1
        Else
            ldouRecaTarj = (1 + (Math.Round(CType(lvstrArgs(4), Double), 2) / 100))
        End If
        'ImpoNoSoci
        If lvstrArgs(1) <> "" Then
            lstrRet.Append(Format(Math.Round(CType(lvstrArgs(1), Double), 2) * ldouRecaCtaCte, "###0.00"))
            lstrRet.Append(";")
            lstrRet.Append(Format(Math.Round(CType(lvstrArgs(1), Double), 2) * ldouRecaTarj, "###0.00"))
            lstrRet.Append(";")
        Else
            lstrRet.Append("")
            lstrRet.Append(";")
            lstrRet.Append("")
            lstrRet.Append(";")
        End If
        'ImpoSoci
        If lvstrArgs(0) <> "" Then
            lstrRet.Append(Format(Math.Round(CType(lvstrArgs(0), Double), 2) * ldouRecaCtaCte, "###0.00"))
            lstrRet.Append(";")
            lstrRet.Append(Format(Math.Round(CType(lvstrArgs(0), Double), 2) * ldouRecaTarj, "###0.00"))
            lstrRet.Append(";")
        Else
            lstrRet.Append("")
            lstrRet.Append(";")
            lstrRet.Append("")
            lstrRet.Append(";")
        End If
        'ImpoAdhe
        If lvstrArgs(2) <> "" Then
            lstrRet.Append(Format(Math.Round(CType(lvstrArgs(2), Double), 2) * ldouRecaCtaCte, "###0.00"))
            lstrRet.Append(";")
            lstrRet.Append(Format(Math.Round(CType(lvstrArgs(2), Double), 2) * ldouRecaTarj, "###0.00"))
            lstrRet.Append(";")
        Else
            lstrRet.Append("")
            lstrRet.Append(";")
            lstrRet.Append("")
            lstrRet.Append(";")
        End If

        ' vsRet[0]; //txtNoSociRecCtaCte
        ' vsRet[1]; //txtNoSociRecTarj
        ' vsRet[2]; //txtsociRecCtaCte
        ' vsRet[3]; //txtsociRecTarj
        ' vsRet[4]; //txtAdheRecCtaCte
        ' vsRet[5]; //txtAdheRecTarj
        Return lstrRet.ToString





    End Function

    Public Shared Function CalculoArancel(ByVal pPagina As System.Web.UI.Page, ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrArgs As String) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim ldouImporte, ldouTasa, ldouImporteTasaIVA, pdouUnitSIVA, pdouPrecioAdherente As Double
        Dim ldouSobreTasa, ldouSobreTasaImporte As Double
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'AranId,Importe,Cant
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pPagina, pstrConn, pstrUserId)
        'lvstrArgs(0)  document.all("cmbAran").value 
        'lvstrArgs(1)  document.all("txtCantAran").value 
        'lvstrArgs(2)  document.all("hdnActiId").value 
        'lvstrArgs(3)  document.all("hdnFechaIva").value 
        'lvstrArgs(4)  document.all("hdnCotDolar").value 
        'lvstrArgs(5)  document.all("hdnFormaPago").value 
        'lvstrArgs(6)  document.all("cmbListaPrecios").value    'lvstrArgs(6)  document.all("hdnFechaValor").value 
        'lvstrArgs(7)  document.all("txtPunitAran").value  solo tiene valor si se ingresa manualmente el valor del arancel, es sin iva



        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            If lvstrArgs(1) = "0" Then lvstrArgs(1) = ""


            If lvstrArgs(4).Length = 0 Then lvstrArgs(4) = 0
            If lvstrArgs(5).Length = 0 Then lvstrArgs(5) = 0
            If lvstrArgs(7) = "" Then   'calcula normalmente el precio unitario
                'If lvstrArgs(6) = "" Then 'importe unitario sin IVA
                'pdouUnitSIVA = Format(mobj.ValorArancel(pstrConn, CType(lvstrArgs(0), Integer), CType(lvstrArgs(2), Integer), CType(lvstrArgs(4), Double), CType(lvstrArgs(5), Integer)), "###0.00")
                'Else
                pdouUnitSIVA = Format(mobj.ValorArancel(pstrConn, CType(lvstrArgs(0), Integer), CType(lvstrArgs(2), Integer), CType(lvstrArgs(4), Double), CType(lvstrArgs(5), Integer), lvstrArgs(6)), "###0.00")

                'End If
            Else
                pdouUnitSIVA = (CType(lvstrArgs(7), Double))   'el precio lo ingresa el usuario (ND/NC)
            End If
            lstrRet.Append(pdouUnitSIVA)
            lstrRet.Append(";")

            'importe aplicada la f�rmula
            ldouImporte = Format(mobj.AplicarFormula(pstrConn, lvstrArgs(0), pdouUnitSIVA, IIf(lvstrArgs(1) = "", 1, lvstrArgs(1)), 1), "###0.00")
            lstrRet.Append(ldouImporte)
            lstrRet.Append(";")
            'iva Tasa
            ldouTasa = Format(CType(mobj.ObtenerValorCampo(pstrConn, "precios_aranX", "valor_tasa", lvstrArgs(0) + "," + lvstrArgs(2) + "," + lvstrArgs(6) + "," + clsSQLServer.gFormatArg(lvstrArgs(3), SqlDbType.SmallDateTime)), Double), "###0.00")
            lstrRet.Append(ldouTasa)
            lstrRet.Append(";")
            'importe iva
            ldouImporteTasaIVA = Format(ldouImporte * (ldouTasa / 100), "###0.00")
            lstrRet.Append(ldouImporteTasaIVA)
            lstrRet.Append(";")
            'importe neto
            ldouImporte = (ldouImporte + ldouImporteTasaIVA)
            lstrRet.Append(ldouImporte)
            lstrRet.Append(";")
            'precio unitario C/IVA
            ldouImporte = Format((pdouUnitSIVA * (1 + (ldouTasa / 100))), "###0.00")
            lstrRet.Append(ldouImporte)
            lstrRet.Append(";")
            'importe unitario sobretasa
            ''ldouSobreTasa = Format(ldouTasa / 2, "###0.00")
            ldouSobreTasa = clsSQLServer.gCampoValorConsul(pstrConn, "sobre_tasa_porcen_consul", "sobre_tasa")
            ldouSobreTasa = Format(ldouSobreTasa, "###0.00")
            ldouImporte = pdouUnitSIVA * IIf(lvstrArgs(1) = "", 1, lvstrArgs(1))
            ldouSobreTasaImporte = Format(ldouImporte * (ldouSobreTasa / 100), "###0.00")
            lstrRet.Append(ldouSobreTasaImporte)
            lstrRet.Append(";")
            'tasa sobretasa
            lstrRet.Append(ldouSobreTasa)
            ' Dario 2013-04-26 precio adherente
            pdouPrecioAdherente = Format(CType(mobj.ObtenerValorCampo(pstrConn, "precios_aranX", "prar_impo_adhe", lvstrArgs(0) + "," + lvstrArgs(2) + "," + lvstrArgs(6) + "," + clsSQLServer.gFormatArg(lvstrArgs(3), SqlDbType.SmallDateTime)), Double), "###0.00")
            lstrRet.Append(";")
            lstrRet.Append(pdouPrecioAdherente)
            End If

            ' [0];//PRECIO UNITARIO
            ' [1];//IMPORTE APLICADA LA FORMULA 
            ' [2];//TASA IVA
            ' [3];//IMPORTE IVA
            ' [4];//IMPORTE NETO TOTAL
            ' [5];//IMPORTE UNITARIO CON IVA 
            ' [6];//IMPORTE UNITARIO SOBRETASA
            ' [7];//TASA SOBRETASA
            ' [8];//importe adherente

            Return lstrRet.ToString

    End Function

    Public Shared Function CalculoArancel(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim ldouImporte, ldouTasa, ldouImporteTasaIVA, pdouUnitSIVA, pdouPrecioAdherente As Double
        Dim ldouSobreTasa, ldouSobreTasaImporte As Double
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'AranId,Importe,Cant
        Dim lstrRet As New System.Text.StringBuilder
        Dim lbooAranGrav As Boolean

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)

        'lvstrArgs(0)  document.all("cmbAran").value 
        'lvstrArgs(1)  document.all("txtCantAran").value 
        'lvstrArgs(2)  document.all("hdnActiId").value 
        'lvstrArgs(3)  document.all("hdnFechaIva").value 
        'lvstrArgs(4)  document.all("hdnCotDolar").value 
        'lvstrArgs(5)  document.all("hdnFormaPago").value
        'lvstrArgs(6)  document.all("cmbListaPrecios").value ' 'lvstrArgs(6)  document.all("hdnFechaValor").value
        'lvstrArgs(7)  document.all("txtPunitAran").value  solo tiene valor si se ingresa manualmente el valor del arancel

        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            lbooAranGrav = (clsSQLServer.gCampoValorConsul(pstrConn, "aranceles_consul @aran_id=" & lvstrArgs(0), "aran_grava_tasa") = "1")
            If lvstrArgs(1) = "0" Then lvstrArgs(1) = ""
            If lvstrArgs(7) = "" Then   'calcula normalmente el precio unitario
                'If lvstrArgs(6) = "" Then 'importe unitario sin IVA
                'pdouUnitSIVA = Format(mobj.ValorArancel(pstrConn, CType(lvstrArgs(0), Integer), CType(lvstrArgs(2), Integer), CType(lvstrArgs(4), Double), CType(lvstrArgs(5), Integer)), "###0.00")
                'Else
                pdouUnitSIVA = Format(mobj.ValorArancel(pstrConn, CType(lvstrArgs(0), Integer), CType(lvstrArgs(2), Integer), CType(lvstrArgs(4), Double), CType(lvstrArgs(5), Integer), lvstrArgs(6)), "###0.00")
                'End If
            Else   'el valor fue ingresado manuelmete por el usuario(NC/ND)
                pdouUnitSIVA = Format(CType(lvstrArgs(7), Double), "###0.00")
            End If
            lstrRet.Append(pdouUnitSIVA)
            lstrRet.Append(";")
            'importe aplicada la f�rmula
            ldouImporte = Format(mobj.AplicarFormula(pstrConn, lvstrArgs(0), pdouUnitSIVA, IIf(lvstrArgs(1) = "", 1, lvstrArgs(1)), 1), "###0.00")
            lstrRet.Append(ldouImporte)
            lstrRet.Append(";")
            'iva Tasa
            ldouTasa = Format(CType(mobj.ObtenerValorCampo(pstrConn, "precios_aranX", "valor_tasa", lvstrArgs(0) + "," + lvstrArgs(2) + "," + lvstrArgs(6) + "," + clsSQLServer.gFormatArg(lvstrArgs(3), SqlDbType.SmallDateTime)), Double), "###0.00")
            lstrRet.Append(ldouTasa)
            lstrRet.Append(";")
            'importe iva
            ldouImporteTasaIVA = Format(ldouImporte * (ldouTasa / 100), "###0.00")
            lstrRet.Append(ldouImporteTasaIVA)
            lstrRet.Append(";")
            'importe neto
            ldouImporte = (ldouImporte + ldouImporteTasaIVA)
            lstrRet.Append(ldouImporte)
            lstrRet.Append(";")
            'precio unitario C/IVA
            ldouImporte = Format((pdouUnitSIVA * (1 + (ldouTasa / 100))), "###0.00")
            lstrRet.Append(ldouImporte)
            lstrRet.Append(";")
            'importe unitario sobretasa
            ''ldouSobreTasa = Format(ldouTasa / 2, "###0.00")
            ldouSobreTasa = clsSQLServer.gCampoValorConsul(pstrConn, "sobre_tasa_porcen_consul", "sobre_tasa")
            ldouSobreTasa = Format(ldouSobreTasa, "###0.00")
            ldouImporte = pdouUnitSIVA * IIf(lvstrArgs(1) = "", 1, lvstrArgs(1))
            ldouSobreTasaImporte = Format(ldouImporte * (ldouSobreTasa / 100), "###0.00")
            If lbooAranGrav Then
                lstrRet.Append(ldouSobreTasaImporte)
            Else
                lstrRet.Append("0")
            End If
            lstrRet.Append(";")
            'tasa sobretasa
            If lbooAranGrav Then
                lstrRet.Append(ldouSobreTasa)
            Else
                lstrRet.Append("0")
            End If
            ' Dario 2013-04-26 precio adherente
            pdouPrecioAdherente = Format(CType(mobj.ObtenerValorCampo(pstrConn, "precios_aranX", "prar_impo_adhe", lvstrArgs(0) + "," + lvstrArgs(2) + "," + lvstrArgs(6) + "," + clsSQLServer.gFormatArg(lvstrArgs(3), SqlDbType.SmallDateTime)), Double), "###0.00")
            lstrRet.Append(";")
            lstrRet.Append(pdouPrecioAdherente)
        End If

        ' [0]; //PRECIO UNITARIO
        ' [1]; //IMPORTE APLICADA LA FORMULA 
        ' [2];//TASA IVA
        ' [3];//IMPORTE IVA
        ' [4];//IMPORTE NETO TOTAL
        ' [5];//IMPORTE UNITARIO CON IVA 
        ' [6];//IMPORTE UNITARIO SOBRETASA
        ' [7];//TASA SOBRETASA
        Return lstrRet.ToString

    End Function

    Public Shared Function ImporteIva(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim ldouresp As Double
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'importe/iva
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return (0)
        Else
            ldouresp = (lvstrArgs(0) * (lvstrArgs(1) / 100))

        End If


        If ldouresp = 0 Then
            Return (0)
        Else
            Return ldouresp
        End If
    End Function

    Public Shared Function AplicarFormula(ByVal pstrConn As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState) As String
        Dim mobj As SRA_Neg.Facturacion
        Dim ldouresp As Double
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'AranId,Importe,Cant
        Dim lstrRet As New System.Text.StringBuilder

        mobj = New SRA_Neg.Facturacion(pstrConn, "", pSession)


        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            ldouresp = mobj.AplicarFormula(pstrConn, lvstrArgs(0), lvstrArgs(1), lvstrArgs(2))
        End If

        If ldouresp = 0 Then
            Return ("")
        Else
            Return (ldouresp)
        End If
    End Function

    Public Shared Function ClienteGenerico(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then  'clie_id
            Return ("")
        Else
            lstrCmd.Append("exec clientes_consul")
            lstrCmd.Append(" @clie_id=")
            lstrCmd.Append(lvstrArgs(0))
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        If ldsDatos.Tables(0).Rows.Count = 0 Then
            Return ("")
        Else
            Return (IIf(ldsDatos.Tables(0).Rows(0).Item("clie_gene") Is DBNull.Value, "", ldsDatos.Tables(0).Rows(0).Item("clie_gene")))
        End If
    End Function

    Public Shared Function RptFiltrosLimpiar(ByVal pstrConn As String, ByVal pstrArgs As String) As String

        Dim lstrCmd As String
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'clie_id;inse_id

        If lvstrArgs(0) <> "" Then
            lstrCmd = "exec rpt_filtros_limpiar " + lvstrArgs(0)
            clsSQLServer.gExecute(pstrConn, lstrCmd)
        End If

        Return ("")

    End Function

    Public Shared Function DatosEvento(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'even_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return (";")
        Else
            lstrCmd.Append("exec eventos_consul")
            lstrCmd.Append(" @even_id=")
            lstrCmd.Append(lvstrArgs(0))
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return (";")
            Else
                If Not .Rows(0).IsNull("even_evtr1_id") Then
                    lstrRet.Append(.Rows(0).Item("even_evtr1_id"))
                End If
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("even_evtr2_id") Then
                    lstrRet.Append(.Rows(0).Item("even_evtr2_id"))
                End If
                lstrRet.Append(";")
                If Not .Rows(0).Item("even_cont1_nume") Then
                    lstrRet.Append("N")
                Else
                    lstrRet.Append("S")
                End If
                lstrRet.Append(";")
                If Not .Rows(0).Item("even_cont2_nume") Then
                    lstrRet.Append("N")
                Else
                    lstrRet.Append("S")
                End If
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("even_evtr1_desc") Then
                    lstrRet.Append(.Rows(0).Item("even_evtr1_desc"))
                End If
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("even_evtr2_desc") Then
                    lstrRet.Append(.Rows(0).Item("even_evtr2_desc"))
                End If
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("even_evtr3_id") Then
                    lstrRet.Append(.Rows(0).Item("even_evtr3_id"))
                End If
                lstrRet.Append(";")
                If Not .Rows(0).Item("even_cont3_nume") Then
                    lstrRet.Append("N")
                Else
                    lstrRet.Append("S")
                End If
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("even_evtr3_desc") Then
                    lstrRet.Append(.Rows(0).Item("even_evtr3_desc"))
                End If

                Return (lstrRet.ToString)
            End If
        End With
    End Function

    'REALIZA: Calcula la deuda de Cuotas del Socio para la Fecha Actual.  //Created by: Hern�n.
    Public Shared Function CalcularDeudaSocio(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'soci_id;inte

        If lvstrArgs(0) = "" Then
            Return (";")
        Else
            'Armo el lstrCmd para luego ejecutarlo.
            lstrCmd.Append("exec socios_deuda_cuota_fecha_consul")
            lstrCmd.Append(" @soci_id=")
            lstrCmd.Append(lvstrArgs(0))
            lstrCmd.Append(",@inte=")
            lstrCmd.Append(lvstrArgs(1))
        End If

        'Ejecuto el SP y devuelvo en un DataReader.
        Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(pstrConn, lstrCmd.ToString())
        Dim lstrDeuda As String = ""

        'Recorro el DataReader.
        While (dr.Read())
            'Verifico si el campo deuda_actual � deuda_venc es mayor que 0.
            If dr.GetValue(dr.GetOrdinal("deuda_actual")) > 0 Or dr.GetValue(dr.GetOrdinal("deuda_venc")) > 0 Then
                'Obtengo el campo deuda.
                lstrDeuda = dr.GetValue(dr.GetOrdinal("deuda"))
                'Verifico si el campo inte_fecha contiene un valor.
                If dr.GetValue(dr.GetOrdinal("inte_fecha")) <> "" Then
                    'Armo el string de Deuda a mostrar.
                    lstrDeuda = " al " + dr.GetValue(dr.GetOrdinal("inte_fecha")) + " " + lstrDeuda
                End If
            End If
        End While
        'Cierro la conexi�n del DataReader.
        dr.Close()

        'Verifico si lstrDeuda contiene un valor.
        If lstrDeuda = "" Then
            'Si no hay deuda, muestro "Socio sin Deuda".
            lstrDeuda = " Socio sin Deuda "
        End If

        'Retorno el string de Deuda a mostrar.
        Return lstrDeuda
    End Function

    Public Shared Function DatosEventoCate(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'soci_id;even_id
        Dim lstrRet As New System.Text.StringBuilder
        Dim lstrInst As String = " - Com�n"

        If lvstrArgs(0) = "" Then
            Return (";")
        Else
            'Armo el lstrCmd para luego ejecutarlo.
            lstrCmd.Append("exec eventos_catego_socio_consul")
            lstrCmd.Append(" @soci_id=")
            lstrCmd.Append(lvstrArgs(0))
            lstrCmd.Append(",@even_id=")
            lstrCmd.Append(lvstrArgs(1))
        End If

        'Ejecuto el SP y devuelvo en un DataSet.
        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        'Recorro el DataSet.
        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return (";")
            Else
                'Verifico si el campo _institucion contiene un valor.
                If Not .Rows(0).IsNull("_institucion") Then
                    If .Rows(0).Item("_institucion") <> "" Then
                        'Obtengo el valor del campo _institucion para luego concatenarlo a la Categor�a.
                        lstrInst = " - " & .Rows(0).Item("_institucion")
                    End If
                End If
                If Not .Rows(0).IsNull("evca_ent1_cant") Then
                    'Inserto el valor en lstrRet de la Cantidad 1.
                    lstrRet.Append(.Rows(0).Item("evca_ent1_cant"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("evca_ent2_cant") Then
                    'Inserto el valor en lstrRet de la Cantidad 2.
                    lstrRet.Append(.Rows(0).Item("evca_ent2_cant"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("cate_desc") Then
                    'Inserto el valor en lstrRet de la descripci�n de Categor�a y concateno la Institucion.
                    lstrRet.Append(.Rows(0).Item("cate_desc") & lstrInst)
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("esta_desc") Then
                    'Inserto el valor en lstrRet de la descripci�n de Estado.
                    lstrRet.Append(.Rows(0).Item("esta_desc"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                'Inserto el valor en lstrRet de la deuda del Socio.
                lstrRet.Append(CalcularDeudaSocio(pstrConn, lvstrArgs(0) & ";" & 1))
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_entr1_fecha") Then
                    'Fecha 1.
                    lstrRet.Append(.Rows(0).Item("soev_entr1_fecha"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_evlu1_id") Then
                    'Lugar 1.
                    lstrRet.Append(.Rows(0).Item("soev_evlu1_id"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_entr2_fecha") Then
                    'Fecha 2.
                    lstrRet.Append(.Rows(0).Item("soev_entr2_fecha"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_evlu2_id") Then
                    'Lugar 2.
                    lstrRet.Append(.Rows(0).Item("soev_evlu2_id"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_obse") Then
                    'Observaci�n.
                    lstrRet.Append(.Rows(0).Item("soev_obse"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("evca_ent3_cant") Then
                    'Inserto el valor en lstrRet de la Cantidad 2.
                    lstrRet.Append(.Rows(0).Item("evca_ent3_cant"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_entr3_fecha") Then
                    'Fecha 3.
                    lstrRet.Append(.Rows(0).Item("soev_entr3_fecha"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_evlu3_id") Then
                    'Lugar 3.
                    lstrRet.Append(.Rows(0).Item("soev_evlu3_id"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_ent1_desde_nume") Then
                    'Entrada_Nume_Desde 1.
                    lstrRet.Append(.Rows(0).Item("soev_ent1_desde_nume"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_ent1_hasta_nume") Then
                    'Entrada_Nume_Hasta 1.
                    lstrRet.Append(.Rows(0).Item("soev_ent1_hasta_nume"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_ent2_desde_nume") Then
                    'Entrada_Nume_Desde 2.
                    lstrRet.Append(.Rows(0).Item("soev_ent2_desde_nume"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_ent2_hasta_nume") Then
                    'Entrada_Nume_Hasta 2.
                    lstrRet.Append(.Rows(0).Item("soev_ent2_hasta_nume"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_ent3_desde_nume") Then
                    'Entrada_Nume_Desde 3.
                    lstrRet.Append(.Rows(0).Item("soev_ent3_desde_nume"))
                End If
                'Inserto un ";" para hacer el split.
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("soev_ent3_hasta_nume") Then
                    'Entrada_Nume_Hasta 3.
                    lstrRet.Append(.Rows(0).Item("soev_ent3_hasta_nume"))
                End If
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("soev_id"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("cargo_desc"))
                'Retorno el string de Deuda a mostrar.
                Return (lstrRet.ToString)
            End If
        End With
    End Function

    Public Shared Function DatosIngreSocio(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'soci_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return (";")
        Else
            lstrCmd.Append("exec socios_datos_consul")
            lstrCmd.Append(" @clie_id=")
            lstrCmd.Append(lvstrArgs(0))
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return (";")
            Else
                If Not .Rows(0).Item("clie_naci_fecha") Is DBNull.Value Then
                    lstrRet.Append(.Rows(0).Item("clie_naci_fecha"))
                End If
                lstrRet.Append(";")
                If Not .Rows(0).Item("soci_eman_fecha") Is DBNull.Value Then
                    lstrRet.Append(.Rows(0).Item("soci_eman_fecha"))
                End If
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("clie_peti"))
                Return (lstrRet.ToString)
            End If
        End With
    End Function

    Public Shared Function DatosCateSocio(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'soci_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return (";")
        Else
            lstrCmd.Append("exec socio_cate_esta_consul")
            lstrCmd.Append(" @soci_id=")
            lstrCmd.Append(lvstrArgs(0))
            lstrCmd.Append(",@tipo='C'")
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return (";")
            Else
                lstrRet.Append(.Rows(0).Item("cate_fecha"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("categoria"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("soci_dist_id"))
                Return (lstrRet.ToString)
            End If
        End With
    End Function

    Public Shared Function DatosIIBB(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'ibbc_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return (";")
        Else
            lstrCmd.Append("exec iibb_catego_cargar")
            lstrCmd.Append(" @ibbc_id=")
            lstrCmd.Append(lvstrArgs(0))
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return (";")
            Else
                lstrRet.Append(.Rows(0).Item("id"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("descrip"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("requiere"))
                Return (lstrRet.ToString)
            End If
        End With
    End Function

    Public Shared Function DatosEstaSocio(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'soci_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return (";")
        Else
            lstrCmd.Append("exec socio_cate_esta_consul")
            lstrCmd.Append(" @soci_id=")
            lstrCmd.Append(lvstrArgs(0))
            lstrCmd.Append(",@tipo='E'")
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return (";")
            Else
                lstrRet.Append(.Rows(0).Item("esta_fecha"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("estado"))
                Return (lstrRet.ToString)
            End If
        End With
    End Function

    Public Shared Function DatosRegistroPRDT(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'prdt_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return (";")
        Else
            lstrCmd.Append("exec rg_registro_producto_consul")
            lstrCmd.Append(" @prdt_id=")
            lstrCmd.Append(lvstrArgs(0))
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return (";")
            Else
                lstrRet.Append(.Rows(0).Item("regt_id"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("regt_desc"))
                Return (lstrRet.ToString)
            End If
        End With
    End Function

    Public Shared Function DatosEstaMoviProduc(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'mopr_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return (";")
        Else
            lstrCmd.Append("exec productos_movim_esta_consul")
            lstrCmd.Append(" @mopr_id=")
            lstrCmd.Append(lvstrArgs(0))
            'lstrCmd.Append(",@tipo='E'")
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return (";")
            Else
                lstrRet.Append(.Rows(0).Item("esta_fecha"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("estado"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("bamo_desc"))
                lstrRet.Append(";")
                lstrRet.Append(.Rows(0).Item("mopr_bamo_fecha"))
                Return (lstrRet.ToString)
            End If
        End With
    End Function

    Public Shared Function FechasCicloMate(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'cicl_id;mate_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Or lvstrArgs(1) = "" Then
            Return (";")
        Else
            lstrCmd.Append("exec ciclos_materias_consul")
            lstrCmd.Append(" @cima_cicl_id=")
            lstrCmd.Append(lvstrArgs(0))
            lstrCmd.Append(", @cima_mate_id=")
            lstrCmd.Append(lvstrArgs(1))
        End If

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return (";")
            Else
                If Not .Rows(0).IsNull("cima_inic_fecha") Then
                    lstrRet.Append(CDate(.Rows(0).Item("cima_inic_fecha")).ToString("dd/MM/yyyy"))
                End If
                lstrRet.Append(";")
                If Not .Rows(0).IsNull("cima_fina_fecha") Then
                    lstrRet.Append(CDate(.Rows(0).Item("cima_fina_fecha")).ToString("dd/MM/yyyy"))
                End If
                Return (lstrRet.ToString)
            End If
        End With
    End Function

    Public Shared Function ObtenerInstituEntidades(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'inen_inst_id;inen_enti_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Or lvstrArgs(1) = "" Then
            Return ("")
        End If

        lstrCmd.Append("exec ")
        lstrCmd.Append(Constantes.gTab_InstituEntidades)
        lstrCmd.Append("_consul")
        lstrCmd.Append(" @inen_enti_id=")
        lstrCmd.Append(lvstrArgs(1))

        'lstrCmd.Append(" @inen_inst_id=")
        'lstrCmd.Append(lvstrArgs(0))

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                Return ("")
            Else
                With .Select("inen_baja_fecha is null AND inen_inst_id<>" + lvstrArgs(0))
                    If .GetUpperBound(0) > -1 Then
                        lstrRet.Append(DirectCast(.GetValue(0), DataRow).Item("_inst_desc"))
                    End If
                End With
                Return (lstrRet.ToString)
            End If
        End With
    End Function

    Public Shared Function DetallesTarjetaCliente(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrRet As New System.Text.StringBuilder
        Dim lstraEjecutar As String
        Dim lstrResult As String

        lstraEjecutar = "exec detalle_tarjeta_cliente @" + pstrArgs

        ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstraEjecutar.ToString)
        With ldsDatos.Tables(0)
            If .Rows.Count = 0 Then
                lstrResult = ""
            Else
                lstrResult = .Rows(0).Item("_detalle").ToString()
            End If
        End With
        Return lstrResult
    End Function

    Public Shared Sub verDT(ByVal oDataTable As DataTable)
        Dim sLinea As New System.Text.StringBuilder
        Dim sDato As String
        For Each ocol As DataColumn In oDataTable.Columns
            sLinea.Append(ocol.ColumnName.ToString)
            sLinea.Append("   ")
        Next
        System.Diagnostics.Debug.WriteLine(sLinea.ToString)
        System.Diagnostics.Debug.WriteLine(New String(Char.Parse("-"), sLinea.Length))

        For Each orow As DataRow In oDataTable.Rows
            sLinea.Length = 0
            If orow.RowState = DataRowState.Detached Or orow.RowState = DataRowState.Deleted Then
                sLinea.Append("---- Fila Eliminada ----")
            Else
                For Each ocol As DataColumn In oDataTable.Columns
                    sDato = orow.Item(ocol.ColumnName).ToString

                    If sDato.Length <= ocol.ColumnName.Length Then
                        sLinea.Append(sDato)
                        sLinea.Append(New String(Char.Parse(" "), ocol.ColumnName.Length + 3 - sDato.Length))
                    Else
                        sLinea.Append(sDato.Substring(0, ocol.ColumnName.Length) & "   ")
                    End If
                Next
            End If
            System.Diagnostics.Debug.WriteLine(sLinea.ToString)
        Next
    End Sub

    Public Shared Function gAgregarTabla(ByVal pstrConn As String, ByVal pdsDatos As DataSet, ByVal pstrTabla As String, Optional ByVal pstrId As String = "") As DataRow
        Dim ldsAdic As DataSet
        Dim ldtAdic As DataTable

        ldsAdic = clsSQLServer.gObtenerEstruc(pstrConn, pstrTabla, pstrId)
        ldtAdic = ldsAdic.Tables(0)
        If ldtAdic.Rows.Count > 0 And pstrId = "" Then
            ldtAdic.Rows.Remove(ldtAdic.Rows(0))
        End If
        ldtAdic.TableName = pstrTabla
        ldsAdic.Tables.Remove(ldtAdic)
        pdsDatos.Tables.Add(ldtAdic)

        If pstrId <> "" And ldtAdic.Rows.Count > 0 Then
            Return (ldtAdic.Rows(0))
        End If
    End Function

    Public Shared Function gAgregarRegistro(ByVal pstrConn As String, ByVal pdtDatos As DataTable, ByVal pstrId As String) As DataRow
        Dim lDrTemp As DataRow

        lDrTemp = clsSQLServer.gObtenerEstruc(pstrConn, pdtDatos.TableName, pstrId).Tables(0).Rows(0)
        pdtDatos.ImportRow(lDrTemp)
        Return (pdtDatos.Rows(pdtDatos.Rows.Count - 1))
    End Function

    Public Shared Function PlanDeudaConsulTotales(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim lvstrArgs() As String = pstrArgs.Split(",")   'soci_id;fecha;anio,perio,incluido
        Dim lstrCmd As New StringBuilder
        Dim ds, lDsDescu As DataSet
        Dim lstrFiltro As String
        Dim ldecTotal, ldecSel, ldecCuotas, ldecInte, ldecDesc As Decimal
        Dim lbooInclu As Boolean
        Dim lintAnio, lintPerio As Integer
        Dim ldatFecha As DateTime

        lbooInclu = CBool(CInt(lvstrArgs(4)))
        lintAnio = CInt(lvstrArgs(2))
        lintPerio = CInt(lvstrArgs(3))
        ldatFecha = clsFormatear.gFormatFechaDateTime(lvstrArgs(1))

        lstrCmd.Append("exec rpt_calculo_deuda_consul")
        lstrCmd.Append(" @soci_id=")
        lstrCmd.Append(lvstrArgs(0))
        lstrCmd.Append(", @fecha=")
        lstrCmd.Append(clsFormatear.gFormatFecha2DB(ldatFecha))
        lstrCmd.Append(", @consulta=1")

        ds = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

        If ds.Tables(0).Rows.Count > 0 Then
            With ds.Tables(0).Select("", "anio desc,perio desc")(0)
                lDsDescu = SRA_Neg.Comprobantes.gObtenerDescuentoSocioPagoAdel(pstrConn, lvstrArgs(0), .Item("anio"), .Item("perio"), .Item("peti_id"), "0" & .Item("inst_id").ToString, ldatFecha)

                If lDsDescu.Tables(0).Rows.Count > 0 Then
                    .Item("descuento") = lDsDescu.Tables(0).Rows(0).Item("cucd_valor")
                    .Item("total") += .Item("descuento")
                End If
            End With
        End If

        For Each lDr As DataRow In ds.Tables(0).Select("", "anio,perio,interes")
            ldecTotal += lDr.Item("total")
            If lDr.Item("anio") < lintAnio Or (lDr.Item("anio") = lintAnio And (lDr.Item("perio") < lintPerio Or (lintPerio = lDr.Item("perio") And lbooInclu))) Then
                ldecSel += lDr.Item("total")
                ldecCuotas += lDr.Item("cuota")
                ldecDesc += lDr.Item("descuento")
                If Not lDr.IsNull("interes") Then ldecInte += lDr.Item("interes")
            End If
        Next

        Return ldecSel.ToString("######0.00") & ";" & (ldecTotal - ldecSel).ToString("######0.00") & ";" & ldecCuotas.ToString("######0.00") & ";" & ldecInte.ToString("######0.00") & ";" & ldecDesc.ToString("######0.00")
    End Function

    Public Shared Sub gSetearRaza(ByVal pcmbRaza As NixorControls.ComboBox)
        If Not HttpContext.Current.Session("sRazaId") Is Nothing Then
            If Not IsNothing(pcmbRaza) Then
                If pcmbRaza.Valor Is DBNull.Value OrElse pcmbRaza.Valor.Trim.Length = 0 Then
                    pcmbRaza.Valor = HttpContext.Current.Session("sRazaId").ToString
                End If
            End If
        End If
    End Sub
    ' sobrecarga que toma un combo no nixor
    Public Shared Sub gSetearRaza(ByVal pcmbRaza As ListControl, ByVal RazaCodi As TextBox, ByVal mstrConn As String)
        If Not HttpContext.Current.Session("sRazaId") Is Nothing Then
            If Not IsNothing(pcmbRaza) Then
                If pcmbRaza.SelectedValue Is DBNull.Value OrElse pcmbRaza.SelectedValue = "0" OrElse pcmbRaza.SelectedValue = "" Then
                    pcmbRaza.SelectedValue = HttpContext.Current.Session("sRazaId").ToString
                    RazaCodi.Text = SRA_Neg.Utiles.ObtenerValorCampo(mstrConn, "RAZAS", "raza_codi", " @raza_id=" & HttpContext.Current.Session("sRazaId").ToString)
                End If
            End If
        End If
    End Sub

    Public Shared Sub gSetearEspecie(ByVal pcmbEspecie As NixorControls.ComboBox)
        If Not HttpContext.Current.Session("sEspeId") Is Nothing Then
            If Not IsNothing(pcmbEspecie) Then
                If pcmbEspecie.Valor Is DBNull.Value OrElse pcmbEspecie.Valor = 0 Then
                    pcmbEspecie.Valor = HttpContext.Current.Session("sEspeId").ToString
                End If
            End If

        End If
    End Sub

    Public Shared Sub ModiImpreso(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrArgs As String)
        Dim lobj As New SRA_Neg.Cobranza(pstrConn, pstrUserId, Constantes.gTab_Comprobantes)
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'comp_id;comp_impr

        lobj.ModiImpreso(lvstrArgs(0), CBool(CInt(lvstrArgs(1))))
    End Sub

    Public Shared Function AutorizarSaldoCtaActi(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrArgs As String, ByVal pSession As System.Web.SessionState.HttpSessionState, ByVal pintOpcion As Integer) As String
        Dim lvstrArgs() As String = pstrArgs.Split(";")   'sact_id;auto;usua_id;sess
        Dim lstrRet As New System.Text.StringBuilder
        Dim lintRet As Integer

        lintRet = SRA_Neg.Cobranza.GuardarAutorizaciones(pstrConn, pstrUserId, CType(lvstrArgs(0), Integer), CType(lvstrArgs(1), Integer), CType(lvstrArgs(2), Integer), pintOpcion, pSession(lvstrArgs(3)))
        Return (lintRet)
    End Function

    Public Shared Function gRound2(ByVal pdouValor As Double) As Decimal

        pdouValor = CDec(Format(pdouValor, "###0.00"))
        Return (pdouValor)

    End Function

    Public Shared Function mGuardarArchivos(ByVal pstrPath As String, ByVal pstrNombre As String, ByVal pContext As HttpContext, Optional ByVal pstrId As String = "") As String
        Dim lstrOrigen As String = pstrNombre
        pstrNombre = pstrNombre.Substring(pstrNombre.LastIndexOf("\") + 1)

        If pstrId <> "" Then
            pstrNombre = pstrId & "_" & pstrNombre
        End If

        If File.Exists(lstrOrigen) Then
            If Not System.IO.Directory.Exists(pContext.Server.MapPath("") + "\" + pstrPath) Then
                System.IO.Directory.CreateDirectory(pContext.Server.MapPath("") + "\" + pstrPath)
            End If

            File.Copy(lstrOrigen, pContext.Server.MapPath("") + "\" + pstrPath + "\" + pstrNombre, True)
        End If

        Return pstrNombre
    End Function

    Public Shared Sub mGuardarArchivosTemp(ByVal pTransac As SqlClient.SqlTransaction, ByVal pstrNombre As String, ByVal pContext As HttpContext, ByVal pctrArchivo As System.Web.UI.HtmlControls.HtmlInputFile)
        Dim lstrTemp As String = clsSQLServer.gParametroValorConsul(pTransac, "para_temp_path")

        If Not (pctrArchivo.PostedFile Is Nothing) Then
            If Not System.IO.Directory.Exists(pContext.Server.MapPath("") + "\" + lstrTemp) Then
                System.IO.Directory.CreateDirectory(pContext.Server.MapPath("") + "\" + lstrTemp)
            End If
            pctrArchivo.PostedFile.SaveAs(pContext.Server.MapPath("") + "\" + lstrTemp + "\" + pstrNombre)
        End If
    End Sub
    Public Shared Sub mGuardarArchivosTemp(ByVal pstrConn As String, ByVal pstrNombre As String, ByVal pContext As HttpContext, ByVal pctrArchivo As System.Web.UI.HtmlControls.HtmlInputFile)
        Dim lTransac As SqlClient.SqlTransaction
        lTransac = clsSQLServer.gObtenerTransac(pstrConn)
        mGuardarArchivosTemp(lTransac, pstrNombre, pContext, pctrArchivo)
    End Sub

    Public Shared Sub mGuardarArchivosFisico(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTabla As String, ByVal pstrNombre As String, ByVal pstrId As String, ByVal pContext As HttpContext, Optional ByVal pstrNombreAnt As String = "")
        Dim lstrTemp As String = clsSQLServer.gParametroValorConsul(lTransac, "para_temp_path")
        Dim lstrCarpeta As String = pstrNombre.Substring(0, pstrNombre.IndexOf("_"))
        Dim lstrNombreNuevo As String

        If File.Exists(pContext.Server.MapPath("") + "\" + lstrTemp + "\" + pstrNombre) Then
            If Not System.IO.Directory.Exists(pContext.Server.MapPath("") + "\" + lstrCarpeta) Then
                System.IO.Directory.CreateDirectory(pContext.Server.MapPath("") + "\" + lstrCarpeta)
            End If

            lstrNombreNuevo = pstrNombre.Substring(pstrNombre.IndexOf("__") + 2)
            lstrNombreNuevo = pstrId + "_" + pstrTabla + "_" + lstrNombreNuevo

            File.Copy(pContext.Server.MapPath("") + "\" + lstrTemp + "\" + pstrNombre, pContext.Server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombreNuevo, True)
            File.Delete(pContext.Server.MapPath("") + "\" + lstrTemp + "\" + pstrNombre)
            If pstrNombreAnt <> "" And pstrId + "_" + pstrTabla + "_" + pstrNombreAnt <> lstrNombreNuevo Then
                File.Delete(pContext.Server.MapPath("") + "\" + lstrCarpeta + "\" + pstrId + "_" + pstrTabla + "_" + pstrNombreAnt)
            End If
        End If
    End Sub
    Public Shared Sub mGuardarArchivosFisico(ByVal pstrConn As String, ByVal pstrTabla As String, ByVal pstrNombre As String, ByVal pstrId As String, ByVal pContext As HttpContext)
        Dim lTransac As SqlClient.SqlTransaction
        lTransac = clsSQLServer.gObtenerTransac(pstrConn)
        mGuardarArchivosFisico(lTransac, pstrTabla, pstrNombre, pstrId, pContext)
    End Sub

    Public Shared Sub mBorrarArchivos(ByVal pstrPath As String, ByVal pstrTabla As String, ByVal pstrNombre As String, ByVal pContext As HttpContext, ByVal pstrId As String)
        'borra un archivo
        If pstrNombre <> "" And Not pContext Is Nothing Then
            pstrNombre = pstrId & "_" & pstrTabla & "_" & pstrNombre
            If File.Exists(pContext.Server.MapPath("") + "\" + pstrPath + "\" + pstrNombre) Then
                File.Delete(pContext.Server.MapPath("") + "\" + pstrPath + "\" + pstrNombre)
            End If
        End If
    End Sub
    Public Shared Function mValidarNulos(ByVal pstrCampo As String, ByVal pboolnull As Boolean) As String
        Dim lstrRt As String = pstrCampo
        If pstrCampo Is DBNull.Value Then If pboolnull Then lstrRt = "" Else lstrRt = "0"
        Return lstrRt
    End Function

#Region "Denuncias de nacimiento"
    Private Enum Tipo
        EspeBovinosNOHolando = 0
        EspeBovinos = 3
        EspePeliferos = 10
        EspeCaprinos = 9
        EspeOvinos = 12
        RazaHolando = 2
        RazaCriolla = 45
    End Enum

    Private Shared Function mOvinos(ByVal pstRaza As Integer, ByVal pstrConn As String) As Boolean
        Dim lstrValor As String = SRA_Neg.Utiles.ObtenerValorCampo(pstrConn, "razas", "raza_espe_id", pstRaza)
        If lstrValor = Tipo.EspeOvinos Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Shared Function mCaprinos(ByVal pstRaza As Integer, ByVal pstrConn As String) As Boolean
        Dim lstrValor As String = SRA_Neg.Utiles.ObtenerValorCampo(pstrConn, "razas", "raza_espe_id", pstRaza)
        If lstrValor = Tipo.EspeCaprinos Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Shared Function mBovinos(ByVal pstRaza As Integer, ByVal pstrConn As String) As Boolean
        If SRA_Neg.Utiles.ObtenerValorCampo(pstrConn, "razas", "raza_espe_id", pstRaza) = Tipo.EspeBovinos Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Shared Function mPeliferos(ByVal pstRaza As Integer, ByVal pstrConn As String) As Boolean
        If SRA_Neg.Utiles.ObtenerValorCampo(pstrConn, "razas", "raza_espe_id", pstRaza) = Tipo.EspePeliferos Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Shared Function mBovinosNoHolando(ByVal pstRaza As Integer, ByVal pstrConn As String) As Boolean
        If mBovinos(pstRaza, pstrConn) And pstRaza <> Tipo.RazaHolando Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Shared Function EspeRaza(ByVal pRaza As String, ByVal pstrConn As String) As Integer
        If mPeliferos(pRaza, pstrConn) Then Return Tipo.EspePeliferos
        If mBovinosNoHolando(pRaza, pstrConn) Then Return Tipo.EspeBovinosNOHolando
        If mCaprinos(pRaza, pstrConn) Then Return Tipo.EspeCaprinos
        'If mBovinos(pRaza, pstrConn) Then Return Tipo.EspeBovinos
        If mOvinos(pRaza, pstrConn) Then Return Tipo.EspeOvinos
        If pRaza = Tipo.RazaCriolla Then Return Tipo.RazaCriolla
        If pRaza = Tipo.RazaHolando Then Return Tipo.RazaHolando
    End Function


    Public Shared Function EspeRazaJs(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim lvstrArgs() As String = pstrArgs.Split(";")
        Dim lstrRet As String
        Return EspeRaza(lvstrArgs(0), pstrConn).ToString
    End Function
#End Region

    ' metodo que genera la fila en la tabla novnuev de la integracion
    ' pasarle el tipo operacion y el id del elemento
    Public Shared Sub NovNuevClientesNovedades(ByVal pstrConn As String, ByVal NovOperacion As String, ByVal Id As Int64)
        ' exec novnuev_clientes_novedades @nov_operacion='INGR_CTE',@pClienteId=@ReturnClie_id  

        Try
            Dim mstrCmd As String
            mstrCmd = "exec novnuev_clientes_novedades "
            mstrCmd = mstrCmd + " @nov_operacion='" + NovOperacion + "', "
            mstrCmd = mstrCmd + " @pClienteId = " + clsSQLServer.gFormatArg(Id, SqlDbType.Int)
            clsSQLServer.gExecuteQuery(pstrConn, mstrCmd)
        Catch ex As Exception
            clsError.gManejarError("Utiles.NovNuevClientesNovedades", "ERROR", ex)
        End Try
    End Sub

    Public Shared Function LimpiarListbox(ByVal pstrArgs As String) As String

        Dim MyPos As Integer
        MyPos = InStr(1, pstrArgs, "-")   ' Returns 0.

        If MyPos > 0 Then
            LimpiarListbox = Left(pstrArgs, MyPos - 1)
        End If

        Return LimpiarListbox

    End Function

End Class