Imports ReglasValida.Validaciones


Public Class CentroImplante

    Inherits Generica


    Protected server As System.Web.HttpServerUtility


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub

   

   
   

    Public Function GetCentroImplanteById(ByVal CentroImplanteId As String) As DataTable
        Dim strFechaInicialEmbargo As String = ""
        Dim mstrCmd As String
        Dim dt As DataTable


        mstrCmd = "exec  GetCentroImplanteById "
        mstrCmd = mstrCmd + " @Id = " + clsSQLServer.gFormatArg(CentroImplanteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        Return dt

    End Function

End Class