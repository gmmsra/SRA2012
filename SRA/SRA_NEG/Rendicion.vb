Public Class Rendicion
   Inherits GenericaRel

   Private mstrTablaDepo As String = SRA_Neg.Constantes.gTab_RendicionesDepo
   Private mstrTablaPagos As String = SRA_Neg.Constantes.gTab_RendicionesPagos
   Private mstrTablaFaltantes As String = SRA_Neg.Constantes.gTab_RendicionesFaltantes
   Private mstrTablaCheques As String = SRA_Neg.Constantes.gTab_Cheques
   Private mstrTablaChequesMovim As String = SRA_Neg.Constantes.gTab_ChequesMovim


   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet)
      MyBase.New(pstrConn, pstrUserId, pstrTabla, pdsDatos)
   End Sub

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String)
      MyBase.New(pstrConn, pstrUserId, pstrTabla)
   End Sub

   Public Overloads Sub Modi()
      Dim lstrRendId As String
      Dim lTransac As SqlClient.SqlTransaction

      Try
         lTransac = clsSQLServer.gObtenerTransac(mstrConn)

         lstrRendId = MiRow.Item("rend_id")

         clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)

         BorrarDetas(lTransac, mstrTablaDepo)
         BorrarDetas(lTransac, mstrTablaPagos)
         BorrarDetas(lTransac, mstrTablaFaltantes)

         ActualizarDetas(lTransac, mstrTablaDepo, "redo_rend_id", lstrRendId)
         ActualizarDetas(lTransac, mstrTablaPagos, "repa_rend_id", lstrRendId)
         ActualizarDetas(lTransac, mstrTablaFaltantes, "refa_rend_id", lstrRendId)

         If Not mdsDatos.Tables(mstrTablaCheques) Is Nothing Then
            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaCheques).Select
               clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaCheques, lDr)
            Next

            For Each lDr As DataRow In mdsDatos.Tables(mstrTablaChequesMovim).Select
               clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaChequesMovim, lDr)
            Next
         End If

         clsSQLServer.gCommitTransac(lTransac)

      Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(ex)
         Throw (ex)
      End Try
   End Sub
End Class