Imports System.Data.SqlClient

Public Class Socios
    Inherits Generica


    Protected server As System.Web.HttpServerUtility


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub

    

    Public Function GetMailDefaultBySocioId(ByVal pSocioId As Int32) As DataRow

        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim mstrCmd As String


            mstrCmd = "exec GetMailDefaultBySocioId "
            mstrCmd = mstrCmd + " @soci_id = " + clsSQLServer.gFormatArg(pSocioId, SqlDbType.Int)
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

            If dt.Rows.Count > 0 Then
                For Each drMailDefault As DataRow In dt.Rows
                    Return drMailDefault
                Next
            End If
            Return dr
        Catch ex As Exception
            clsError.gManejarError("socios.GetMailDefaultBySocioId", "ERROR", ex)
        End Try

    End Function

    Public Function SetDireDefaultByDiclId(ByVal pDiclId As Int32) As Boolean

        Try

            Dim mstrCmd As String
            Dim booResp As Boolean = False


            mstrCmd = "exec SetDireDefaultByMaclId "
            mstrCmd = mstrCmd + " @dicl_id = " + clsSQLServer.gFormatArg(pDiclId, SqlDbType.Int)
            clsSQLServer.gExecute(mstrConn, mstrCmd)


            Return booResp
        Catch ex As Exception
            clsError.gManejarError("socios.SetDireDefaultByDiclId", "ERROR", ex)
        End Try

    End Function

    Public Function SetMailDefaultByMaclId(ByVal pMaclId As Int32) As Boolean

        Try

            Dim mstrCmd As String
            Dim booResp As Boolean = False


            mstrCmd = "exec SetMailDefaultByMaclId "
            mstrCmd = mstrCmd + " @macl_id = " + clsSQLServer.gFormatArg(pMaclId, SqlDbType.Int)
            clsSQLServer.gExecute(mstrConn, mstrCmd)


            Return booResp
        Catch ex As Exception
            clsError.gManejarError("socios.SetMailDefaultByMaclId", "ERROR", ex)
        End Try

    End Function


    Public Function SetTeleDefaultByMaclId(ByVal pMaclId As Int32) As Boolean

        Try

            Dim mstrCmd As String
            Dim booResp As Boolean = False


            mstrCmd = "exec SetTeleDefaultByMaclId "
            mstrCmd = mstrCmd + " @tecl_id = " + clsSQLServer.gFormatArg(pMaclId, SqlDbType.Int)
            clsSQLServer.gExecute(mstrConn, mstrCmd)


            Return booResp
        Catch ex As Exception
            clsError.gManejarError("socios.SetTeleDefaultByMaclId", "ERROR", ex)
        End Try

    End Function



    Public Function GetMailAdicionalBySocioId(ByVal pSocioId As Int32) As DataRow

        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim mstrCmd As String


            mstrCmd = "exec GetMailTopAdicionalBySocioId "
            mstrCmd = mstrCmd + " @soci_id = " + clsSQLServer.gFormatArg(pSocioId, SqlDbType.Int)
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

            If dt.Rows.Count > 0 Then
                For Each drMailDefault As DataRow In dt.Rows
                    Return drMailDefault
                Next
            End If
            Return dr
        Catch ex As Exception
            clsError.gManejarError("socios.GetMailDefaultBySocioId", "ERROR", ex)
        End Try

    End Function

    Public Function SetBajaLogicaTelefonoDefaultById(ByVal pTelefonoId As Int32) As Boolean

        Try
            Dim booResp As Boolean = False
            Dim mstrCmd As String


            mstrCmd = "exec SetBajaLogicaTelefonoDefaultById "
            mstrCmd = mstrCmd + " @id = " + clsSQLServer.gFormatArg(pTelefonoId, SqlDbType.Int)
            clsSQLServer.gExecute(mstrConn, mstrCmd)
            Return booResp
        Catch ex As Exception
            clsError.gManejarError("socios.SetBajaLogicaTelefonoDefaultById", "ERROR", ex)
        End Try

    End Function




End Class
