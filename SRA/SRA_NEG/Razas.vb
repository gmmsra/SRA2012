Imports ReglasValida.Validaciones
Public Class Razas
    Inherits Generica


    Protected mstrTablaAna As String
    Protected server As System.Web.HttpServerUtility

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaAna As String, ByVal pdsDatos As DataSet)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
        Me.mstrTabla = pstrTabla
        Me.mstrTablaAna = pstrTablaAna
        Me.mdsDatos = pdsDatos
        Me.server = server
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId

    End Sub



    Public Function GetRazaDescripcionById(ByVal RazaId As String) As String
        Dim dtRaza As New DataTable
        Dim mstrCmd As String
        Dim RazaDescripcion As String
        RazaDescripcion = ""


        mstrCmd = "exec GetRazaById "
        mstrCmd = mstrCmd + " @RazaId = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar)
        dtRaza = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtRaza.Rows.Count > 0 Then
            RazaDescripcion = dtRaza.Rows(0).Item("raza_desc")
        End If


        Return RazaDescripcion



    End Function
    Public Function GetRazaByCriaId(ByVal CriaId As String) As DataTable
        Dim dtRaza As New DataTable
        Dim mstrCmd As String

        mstrCmd = "exec GetRazaByCriaId "
        mstrCmd = mstrCmd + " @CriaId = " + clsSQLServer.gFormatArg(CriaId, SqlDbType.VarChar)
        dtRaza = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dtRaza

    End Function

    Public Function GetRazaCodiById(ByVal RazaId As String) As Integer
        Dim dtRaza As New DataTable
        Dim mstrCmd As String
        Dim RazaCodi As Integer



        mstrCmd = "exec GetRazaById "
        mstrCmd = mstrCmd + " @RazaId = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar)
        dtRaza = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtRaza.Rows.Count > 0 Then
            RazaCodi = dtRaza.Rows(0).Item("raza_codi")
        End If


        Return RazaCodi



    End Function

    Public Function GetRazaPermiteDescorne(ByVal RazaId As String) As Boolean
        ' Modificado : GSZ 23/04/2015
        Dim dtRaza As New DataTable
        Dim mstrCmd As String
        Dim boolDescorne As Boolean = True

        mstrCmd = "exec GetRazaPermiteDescorne "
        mstrCmd = mstrCmd + " @RazaId = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar)
        dtRaza = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dtRaza.Rows.Count > 0 Then
            boolDescorne = dtRaza.Rows(0).Item("raza_descorne")
        End If

        Return boolDescorne

    End Function


    Public Function GetRazaByRazaCodigo(ByVal RazaCodi As String) As String
        Dim dtRaza As New DataTable
        Dim mstrCmd As String
        Dim RazaDesc As String


        mstrCmd = "exec GetRazaIdByRazaCodigo "
        mstrCmd = mstrCmd + " @pRazaCodigo = " + clsSQLServer.gFormatArg(RazaCodi, SqlDbType.VarChar)
        dtRaza = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dtRaza.Rows.Count > 0 Then
            RazaDesc = dtRaza.Rows(0).Item("raza_desc")
        End If


        Return RazaDesc



    End Function


End Class
