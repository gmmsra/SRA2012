Imports System.Web
Public Class Comprobantes

    Public Shared mchrSepa As String = ";"c

	Public Shared Function gCrearDataSetComp(ByVal pstrConn As String, ByVal pstrId As String) As DataSet
		Dim lDs As DataSet

		lDs = clsSQLServer.gObtenerEstruc(pstrConn, Constantes.gTab_Comprobantes, pstrId)

		With lDs
			.Tables(0).TableName = Constantes.gTab_Comprobantes
			.Tables(1).TableName = Constantes.gTab_ComprobDeta
			.Tables(2).TableName = Constantes.gTab_ComprobAranc
			.Tables(3).TableName = Constantes.gTab_ComprobConcep
			.Tables(4).TableName = Constantes.gTab_ComprobVtos
			.Tables(5).TableName = Constantes.gTab_ComprobAutor
            .Tables(6).TableName = Constantes.gTab_ComprobPagos
            .Tables(7).TableName = Constantes.gTab_ComprobSobreTasaVtos
		End With

		Return (lDs)
	End Function

	Public Shared Function gObtenerDatosDeudaSocio(ByVal pstrConn As String, ByVal pstrSociId As String, ByVal pstrAnio As String, ByVal pstrPetiId As String, ByVal pstrPerio As String, ByVal pintPromNormal As Integer) As DataSet
		Dim lstrCmd As New StringBuilder
		Dim lDs As DataSet

		lstrCmd.Append("exec socio_deuda_datos_consul")
		lstrCmd.Append(" @anio=" + pstrAnio)
		lstrCmd.Append(",@peti_id=" + pstrPetiId)
		lstrCmd.Append(",@perio=" + pstrPerio)
		lstrCmd.Append(",@soci_id=" + pstrSociId)
		lstrCmd.Append(",@es_debito=" + pintPromNormal.ToString)

		lDs = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)
		If lDs.Tables.Count > 1 AndAlso lDs.Tables(1).Rows.Count = 0 Then
			lDs.Tables(1).Rows.Add(lDs.Tables(1).NewRow)
		End If

		Return lDs
	End Function

	Public Shared Function gCentroEmisorId(ByVal pstrConn As String, ByVal pRequest As Web.HttpRequest, Optional ByVal pbooIgnorarError As Boolean = False) As Integer
		Dim pContext As HttpContext = HttpContext.Current
		If pContext.Session("sCentroEmisorId") Is Nothing Or pContext.Session("sCentroEmisorId") Is DBNull.Value Then
			If pbooIgnorarError Then
				Return (0)
            Else
                ' Dario 2013-09-19 cambio para que muestre nuevo mensaje
                'Throw New AccesoBD.clsErrNeg(String.Format("La m�quina {0} no tiene un centro de emisi�n asociado", pContext.Session("sHost")))
                Throw New AccesoBD.clsErrNeg(String.Format("El Usuario {0} no tiene un centro de emisi�n asociado", pContext.Session("sUserName")))
            End If
        Else
			Return (pContext.Session("sCentroEmisorId"))
		End If
	End Function

	Public Shared Function FechaVtoFactura(ByVal pstrConn As String, ByVal pintActiId As Integer, ByVal pdatFecha As DateTime, Optional ByVal pintPlanCuotaId As Integer = 0) As DateTime
		Dim lds As DataSet
		If pintPlanCuotaId <> 0 Then
			'toma la fecha de la cuota
			lds = clsSQLServer.gObtenerEstruc(pstrConn, "pagos_cuotas", pintPlanCuotaId)
			With lds.Tables(0).Rows(0)
				Return .Item("plco_fecha")
			End With
		Else
			lds = clsSQLServer.gObtenerEstruc(pstrConn, "actividades", pintActiId)
			With lds.Tables(0).Rows(0)
				If Not .IsNull("acti_vcto_cant_dias") AndAlso .Item("acti_vcto_cant_dias") > 0 Then
					Return (pdatFecha.AddDays(.Item("acti_vcto_cant_dias")))
				End If

				If Not .IsNull("acti_vcto_fijo_dia") AndAlso .Item("acti_vcto_fijo_dia") > 0 Then
					Return (New Date(IIf(pdatFecha.Month + 1 = 13, pdatFecha.Year + 1, pdatFecha.Year), IIf(pdatFecha.Month + 1 = 13, 1, pdatFecha.Month + 1), .Item("acti_vcto_fijo_dia")))
				End If
			End With
		End If

		Return (pdatFecha)

	End Function

	Public Shared Function gMonedaPesos(ByVal pstrConn As String) As Integer
		Return (clsSQLServer.gExecuteScalar(pstrConn, "parametros_mone_pesos_consul"))
	End Function

	Public Shared Function gMonedaDolar(ByVal pstrConn As String) As Integer
		Return (clsSQLServer.gExecuteScalar(pstrConn, "parametros_mone_dolar_consul"))
	End Function

    Public Shared Function gCotizaMoneda(ByVal pstrConn As String, ByVal pFecha As Object, Optional ByVal pMoneId As Integer = 0) As Object
        Dim lstrCmd As String
        Dim lRet As Object

        lstrCmd = "exec cotiza_monedas_busq"
        lstrCmd += " @como_fecha_cotiza =" + clsFormatear.gFormatFecha2DB(pFecha)
        If pMoneId <> 0 Then
            lstrCmd += ",@como_mone_id=" + pMoneId.ToString
        End If
        lRet = clsSQLServer.gExecuteScalar(pstrConn, lstrCmd)
        If lRet Is Nothing Then
            lRet = 1
        End If
        Return (lRet)
    End Function

	Public Shared Function gObtenerTarjCliente(ByVal pstrConn As String, ByVal pstrClieId As String, ByVal pstrTarjId As String, ByVal pstrTarjNume As String) As Object
		Dim lstrCmd As String

		lstrCmd = "exec tarjetas_clientes_consul"
		lstrCmd += " @tacl_clie_id=" + pstrClieId
		lstrCmd += ",@tacl_tarj_id=" + pstrTarjId
		lstrCmd += ",@tacl_nume=" + clsSQLServer.gFormatArg(pstrTarjNume, SqlDbType.VarChar)

		Dim lDsDatos As DataSet
		lDsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)
		If lDsDatos.Tables(0).Rows.Count = 0 Then
			Return (DBNull.Value)
		Else
			Return (lDsDatos.Tables(0).Rows(0).Item("tacl_id"))
		End If
	End Function

	Public Shared Function gObtenerDescuentoSocioPagoAdel(ByVal pstrConn As String, ByVal pintSociId As Integer, ByVal pintAnio As Integer, ByVal pintPerio As Integer, ByVal pintPetiId As Integer, ByVal pintInstId As Integer, ByVal pdatFecha As DateTime) As DataSet
		Dim lstrCmd As String

		lstrCmd = "exec socios_descuen_consul"
		lstrCmd += " @soci_id=" + pintSociId.ToString
		lstrCmd += ",@anio=" + pintAnio.ToString
		lstrCmd += ",@perio=" + pintPerio.ToString
		lstrCmd += ",@peti_id=" + pintPetiId.ToString
		lstrCmd += ",@inst_id=" + pintInstId.ToString
		lstrCmd += ",@fecha=" + clsFormatear.gFormatFecha2DB(pdatFecha)

		Return (clsSQLServer.gExecuteQuery(pstrConn, lstrCmd))
	End Function

 Public Shared Sub gBorrarCuotasSociSobrantes(ByVal pdtTabla As DataTable, ByVal pstrSociId As String, ByVal pstrCampoPref As String, ByVal pvrDatos As String(), Optional ByVal pstrSepara As String = "")
  Dim lbooExiste As Boolean
  Dim lvrDato() As String

  For Each ldrDatos As DataRow In pdtTabla.Select(pstrCampoPref + "_soci_id=" + pstrSociId)
   lbooExiste = False
   With ldrDatos
    If Not pvrDatos Is Nothing Then
     For i As Integer = 0 To pvrDatos.GetUpperBound(0)
        If pstrSepara = "" Then
            lvrDato = pvrDatos(i).Split(mchrSepa)      'CompId(6)Anio(6)Perio(6)PetiId(6)InstId(6)ImpoCuota(6)ImpoInteres(6)SociId(6)Fecha(5)...
        Else
            lvrDato = pvrDatos(i).Split(Chr(6))        'CompId(6)Anio(6)Perio(6)PetiId(6)InstId(6)ImpoCuota(6)ImpoInteres(6)SociId(6)Fecha(5)...
        End If
      lbooExiste = .Item(pstrCampoPref + "_anio") = lvrDato(1) And .Item(pstrCampoPref + "_perio") = lvrDato(2)
      If lbooExiste Then Exit For
     Next
    End If

    If Not lbooExiste Then
     .Delete()
    End If
   End With
  Next
 End Sub

 Public Shared Function gObtenerAutorizaci�n(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pintAuti_id As Integer, ByVal pintOpcion As Integer, ByVal pintAutoUserId As Integer) As Integer
  Dim lintRet As Integer
  Dim lstrFiltro As New StringBuilder

  lstrFiltro.Append("@usua_id=")
  lstrFiltro.Append(pstrUserId)
  lstrFiltro.Append(",@auto_usua_id=")
  lstrFiltro.Append(pintAutoUserId)
  lstrFiltro.Append(",@auti_id=")
  lstrFiltro.Append(pintAuti_id)
  lstrFiltro.Append(",@opcion=")
  lstrFiltro.Append(pintOpcion)

  lintRet = clsSQLServer.gObtenerValorCampo(pstrConn, "usuarios_autoriza_tipos_valida", lstrFiltro.ToString, "salida")

  Return (lintRet)
 End Function
End Class