Imports System.Data.SqlClient

Public Class ClienteDocu
   Inherits Generica

   Protected filDocu As System.Web.UI.HtmlControls.HtmlInputFile
   Protected server As System.Web.HttpServerUtility

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet, ByVal filDocu As System.Web.UI.HtmlControls.HtmlInputFile, ByVal server As System.Web.HttpServerUtility)
      mstrConn = pstrConn
      mstrUserId = pstrUserId
      mstrTabla = pstrTabla
      mdsDatos = pdsDatos
      Me.filDocu = filDocu
      Me.server = server
   End Sub

   Public Overloads Function Alta() As String
      mActualizar()
   End Function

   Public Overloads Sub Modi()
      mActualizar()
   End Sub

   Private Sub mActualizar()

      Dim lstrTABLAId As String
      Dim lstrClieId As String
      Dim lTransac As SqlClient.SqlTransaction

      Try

         mGuardarArchivo(MiRow.Item("cldo_clie_id"))

         lTransac = clsSQLServer.gObtenerTransac(mstrConn)

         'Datos del cliente.
         If CInt(MiRow.Item(0)) <= 0 Then
            'ALTA TABLA
            lstrClieId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
         Else
            'MODIFICACION TABLA
            clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
            lstrClieId = MiRow.Item(0).ToString

         End If

         clsSQLServer.gCommitTransac(lTransac)

      Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(ex)
         Throw (ex)
      End Try

   End Sub

   Private Sub mGuardarArchivo(pstrClieId as String )
      Dim lstrNombre As String
      Dim lstrCarpeta As String

      If Not (filDocu.PostedFile Is Nothing) AndAlso filDocu.PostedFile.FileName <> "" Then
         lstrNombre = filDocu.PostedFile.FileName
         lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)

         lstrCarpeta = clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_docu_path")

         lstrCarpeta = server.MapPath("") + "\" + lstrCarpeta + "\" + pstrClieId

         If Not System.IO.Directory.Exists(lstrCarpeta) Then
            System.IO.Directory.CreateDirectory(lstrCarpeta)
         End If

         filDocu.PostedFile.SaveAs(lstrCarpeta + "\" + lstrNombre)
      End If
   End Sub

   Private Sub mEscribir(ByVal pDatos As String)
      Dim lstrArchLog As String = System.Configuration.ConfigurationSettings.AppSettings("conArchLog").ToString
      Dim fs As New System.IO.FileStream(lstrArchLog, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite)

      Dim w = New System.IO.StreamWriter(fs)
      w.BaseStream.Seek(0, System.IO.SeekOrigin.End)

      w.Write(clsFormatear.CrLf() & "Fecha: " & DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") & clsFormatear.CrLf())
      w.Write(pDatos & clsFormatear.CrLf())
      w.Write("------------------------------------------------------------------------" & clsFormatear.CrLf())

      w.Flush()
      w.Close()
   End Sub
End Class