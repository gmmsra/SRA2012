Imports System.Data.SqlClient
Imports System.Web
Imports System.IO
Imports System.XML
Imports System.Web.UI.WebControls
Imports System.Collections

Public Class Generica

   Protected mstrTabla As String
   Protected mstrConn As String
   Protected mstrUserId As String
    Protected mstrStoreProc As String
    Protected mdsDatos As DataSet
   Protected mstrUltId As String

   Public ReadOnly Property gstrTabla() As String
      Get
         Return mstrTabla
      End Get
   End Property

   Public ReadOnly Property MiRow() As DataRow
      Get
         If mdsDatos.Tables(0).Rows.Count > 0 Then
            Return (mdsDatos.Tables(0).Rows(0))
         Else
            Return Nothing
         End If

      End Get
    End Property

    Public ReadOnly Property MiRowDeta() As DataRow
        Get
            If mdsDatos.Tables(1).Rows.Count > 0 Then
                Return (mdsDatos.Tables(1).Rows(0))
            Else
                Return Nothing
            End If

        End Get
    End Property

    Public Property MiDataSet() As DataSet
        Get
            Return (mdsDatos)
        End Get
        Set(ByVal Value As DataSet)
            mdsDatos = Value
        End Set
    End Property

    Public ReadOnly Property gstrUltId() As String
        Get
            Return mstrUltId
        End Get
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet)
        mstrConn = pstrConn
        mstrUserId = pstrUserId
        mstrTabla = pstrTabla
        mdsDatos = pdsDatos
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet, ByVal pdsStoreProc As String)
        mstrConn = pstrConn
        mstrUserId = pstrUserId
        mstrTabla = pstrTabla
        mdsDatos = pdsDatos
        mstrStoreProc = pdsStoreProc
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String)
        mstrConn = pstrConn
        mstrUserId = pstrUserId
        mstrTabla = pstrTabla
    End Sub

    Public Function Alta(ByVal Trans As SqlTransaction) As String
        Return clsSQLServer.gAlta(Trans, mstrUserId, mstrTabla, mdsDatos.Tables(0).Rows(0))
    End Function
    Public Function Alta() As String
        Return clsSQLServer.gAlta(mstrConn, mstrUserId, mstrTabla, mdsDatos)
    End Function

    Public Function ActualizaMul() As String
        Dim lTransac As SqlClient.SqlTransaction
        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            mdsDatos.Tables(0).TableName = mstrTabla
            BorrarDetas(lTransac, mstrTabla)

            For Each ldr As DataRow In mdsDatos.Tables(0).Select
                If ldr.Item(0) Is DBNull.Value OrElse ldr.Item(0) <= 0 Then
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, ldr)
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, ldr)
                End If
            Next

            clsSQLServer.gCommitTransac(lTransac)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Sub Modi()
        clsSQLServer.gModi(mstrConn, mstrUserId, mstrTabla, mdsDatos)
    End Sub
    Public Sub Modi(ByVal Trans As SqlTransaction)
        clsSQLServer.gModi(Trans, Me.mstrUserId, Me.mstrTabla, Me.mdsDatos.Tables.Item(0).Rows.Item(0))
    End Sub

    Public Sub ModiSP()
        clsSQLServer.gModiSP(mstrConn, mstrUserId, mstrTabla, mdsDatos, mstrStoreProc)
    End Sub
    Public Sub ModiSP(ByVal Trans As SqlTransaction)
        clsSQLServer.gModiSP(Trans, Me.mstrUserId, Me.mstrTabla, Me.mdsDatos.Tables.Item(0).Rows.Item(0), mstrStoreProc)
    End Sub



    Public Function ModiRet() As String
        Return (clsSQLServer.gModiRet(mstrConn, mstrUserId, mstrTabla, mdsDatos.Tables(0).Rows(0)))
    End Function
    Public Sub Baja(ByVal pstrId As String)
        clsSQLServer.gBaja(mstrConn, mstrUserId, mstrTabla, pstrId)
    End Sub
    'Public Sub Baja(ByVal pstrId As String, ByVal Trans As SqlTransaction)
    '   clsSQLServer.gBaja(Trans, mstrUserId, mstrTabla, pstrId)
    'End Sub

    Public Sub BorrarArchivo(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTablaDet As String, ByVal odrDeta As DataRow, ByVal pContext As HttpContext)
        Dim lstrColumna As String = ""
        Dim lstrColumnaPath As String = ""
        Dim lstrPath As String
        Dim lstrParamPath As String

        With odrDeta
            For Each dCol As DataColumn In .Table.Columns
                If dCol.ColumnName.EndsWith("_parampath") Then
                    lstrColumnaPath = dCol.ColumnName
                End If
                If dCol.ColumnName.EndsWith("_path") Then
                    lstrColumna = dCol.ColumnName
                    lstrPath = .Item(lstrColumna).ToString
                End If
            Next

            If lstrColumnaPath <> "" Then
                lstrParamPath = clsSQLServer.gParametroValorConsul(lTransac.Connection.ConnectionString, .Item(lstrColumnaPath))

                If lstrParamPath.ToString <> "" Then
                    SRA_Neg.Utiles.mBorrarArchivos(lstrParamPath, pstrTablaDet, lstrPath, pContext, .Item(0).ToString)
                End If
            End If
        End With
    End Sub

    Protected Sub BorrarDetas(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTablaDet As String, Optional ByVal pContext As HttpContext = Nothing)
        Dim lstrColumnaPath As String = ""
        For Each odrDeta As DataRow In mdsDatos.Tables(pstrTablaDet).Select("", "", DataViewRowState.Deleted)
            With odrDeta
                .RejectChanges()
                If Not .IsNull(0) Then
                    For Each dCol As DataColumn In .Table.Columns
                        If dCol.ColumnName.EndsWith("_parampath") Then
                            lstrColumnaPath = dCol.ColumnName
                        End If
                    Next
                    If lstrColumnaPath <> "" Then
                        If clsSQLServer.gExecuteScalar(lTransac, pstrTablaDet + "_baja_accion " + .Item(0).ToString) = "0" Then
                            BorrarArchivo(lTransac, pstrTablaDet, odrDeta, pContext)
                        End If
                    End If

                    clsSQLServer.gBaja(lTransac, mstrUserId, pstrTablaDet, .Item(0).ToString)
                End If
                odrDeta.Table.Rows.Remove(odrDeta)
            End With
        Next
    End Sub

    Protected Function ActualizarDetas(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTablaDet As String, ByVal pstrCampoPK As String, ByVal lintCabId As Integer, Optional ByVal pstrFiltro As String = "", Optional ByVal pContext As HttpContext = Nothing) As String
        Dim lstrColumna As String = ""
        Dim lstrColumnaPath As String = ""
        Dim lstrPath As String = ""
        Dim lstrParamPath As String = ""
        Dim lstrId As Integer

        For Each odrDeta As DataRow In mdsDatos.Tables(pstrTablaDet).Select(pstrFiltro)
            With odrDeta

                For Each dCol As DataColumn In .Table.Columns
                    If dCol.ColumnName.EndsWith("_parampath") Then
                        lstrColumnaPath = dCol.ColumnName
                    End If
                    If dCol.ColumnName.EndsWith("_path") Then
                        lstrColumna = dCol.ColumnName
                        lstrPath = .Item(lstrColumna).ToString
                        .Item(lstrColumna) = .Item(lstrColumna).ToString.Substring(.Item(lstrColumna).ToString.LastIndexOf("\") + 1)
                        .Item(lstrColumna) = IIf(.Item(lstrColumna) = "", DBNull.Value, .Item(lstrColumna))
                    End If
                Next

                If .Item(0) <= 0 Then
                    .Item(pstrCampoPK) = lintCabId
                    'ALTA 
                    mstrUltId = clsSQLServer.gAlta(lTransac, mstrUserId, pstrTablaDet, odrDeta)
                    lstrId = mstrUltId
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, pstrTablaDet, odrDeta)
                    lstrId = .Item(0)
                End If

                If lstrColumnaPath <> "" Then
                    lstrParamPath = clsSQLServer.gParametroValorConsul(lTransac.Connection.ConnectionString, .Item(lstrColumnaPath))

                    If lstrParamPath.ToString <> "" Then
                        SRA_Neg.Utiles.mGuardarArchivos(lstrParamPath, lstrPath, pContext, lstrId)
                    End If
                End If

            End With
        Next

        Return mstrUltId
    End Function
    Public Shared Function ConvertDsToXML(ByVal pDs As DataSet, ByVal pLogFile As String)
        Dim strFileName As String
        strFileName = "C:\TEMP\" + pLogFile + ".XML"
        If (File.Exists(strFileName)) Then
            File.Delete(strFileName)
        End If



        Dim fs As New FileStream(strFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
        Dim writer As New StreamWriter(fs, Encoding.UTF8)
        If Not IsNothing(pDs) Then
            pDs.WriteXml(writer, XmlWriteMode.WriteSchema)
        End If

        fs.Close()


    End Function
    Public Shared Function ConvertDsToXML(ByVal pDs As DataSet)
        Dim strFileName As String
        strFileName = "C:\TEMP\DIF.XML"
        If (File.Exists(strFileName)) Then
            File.Delete(strFileName)
        End If
        Dim fs As New FileStream(strFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
        Dim writer As New StreamWriter(fs, Encoding.UTF8)
        If Not IsNothing(pDs) Then
            pDs.WriteXml(writer, XmlWriteMode.WriteSchema)
        End If

        fs.Close()


    End Function
End Class

