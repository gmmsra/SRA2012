Imports System.Data.SqlClient

Public Class Aranceles
   Inherits Generica

   Protected mstrTablaDeta As String

   Protected server As System.Web.HttpServerUtility

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaDeta As String, ByVal pdsDatos As DataSet, ByVal server As System.Web.HttpServerUtility)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaDeta = pstrTablaDeta
      Me.mdsDatos = pdsDatos
      Me.server = server
   End Sub

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaDeta As String)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaDeta = pstrTablaDeta
   End Sub


   Public Overloads Function Alta() As String
      Return (mActualizar())
   End Function

   Public Overloads Sub Modi()
      mActualizar()
   End Sub

   Public Overloads Sub Baja(ByVal pstrId As String)
      clsSQLServer.gBaja(mstrConn, mstrUserId, mstrTabla, pstrId)
   End Sub

   Private Function mActualizar() As String
      Dim lstrId As String
      Dim lTransac As SqlClient.SqlTransaction

      Try

         lTransac = clsSQLServer.gObtenerTransac(mstrConn)

         'Datos del arancel.
         If CInt(MiRow.Item(0)) <= 0 Then
            'ALTA TABLA
            lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)

         Else
            'MODIFICACION TABLA
            clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
            lstrId = MiRow.Item(0).ToString
         End If



         'Rangos de RRGG.

         Dim lstrdetId As Integer
         
         For Each odrDetaDeta As DataRow In mdsDatos.Tables(mstrTablaDeta).Select()

            If CInt(odrDetaDeta.Item(0)) <= 0 Then
               If odrDetaDeta.Item("arra_aran_id") Is DBNull.Value or odrDetaDeta.Item("arra_aran_id")= 0 Then
                  odrDetaDeta.Item("arra_aran_id") = lstrId
               End If
               'ALTA TABLA
               lstrdetId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDeta, odrDetaDeta)

            Else
               'MODIFICACION TABLA
               clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDeta, odrDetaDeta)
               lstrdetId = MiRow.Item(0).ToString
            End If


         Next

         clsSQLServer.gCommitTransac(lTransac)

         Return (lstrId)

      Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(ex)
         Throw (ex)
      End Try
   End Function

End Class
