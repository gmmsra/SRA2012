Imports ReglasValida.Validaciones


Public Class CambioTatuaje

    Inherits Generica


    Protected server As System.Web.HttpServerUtility


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub



    Public Function GetUltimoCambioTatuajeIdByProductoId(ByVal ProductoId As String) As Integer
        Dim intUltimoCambioTatuajeId As Long = 0
        Dim mstrCmd As String
        Dim dt As DataTable


        mstrCmd = "exec  GetUltimoCambioTatuajeIdByProductoId "
        mstrCmd = mstrCmd + " @rpcm_prdt_id = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then
            intUltimoCambioTatuajeId = ValidarNulos(dt.Rows(0).Item("rpcm_id"), True)
        End If


        Return intUltimoCambioTatuajeId

    End Function

End Class