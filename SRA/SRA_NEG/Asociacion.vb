
Imports System.Data.SqlClient
Imports ReglasValida.Validaciones



Public Class Asociacion
    Inherits GenericaRel

    Private Sub GetAsociacionByAsocById()
      

    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId

    End Sub

    Public Function GetAsociacionMadreByTramiteId(ByVal TramiteId As String) As DataSet
        Dim ds As New DataSet

        Dim mstrCmd As String


        mstrCmd = "exec GetAsociacionMadreByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

        Return ds

    End Function
    Public Function GetAsociacionPadreByTramiteId(ByVal TramiteId As String) As DataSet
        Dim ds As New DataSet

        Dim mstrCmd As String


        mstrCmd = "exec GetAsociacionPadreByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

        Return ds

    End Function


    Public Function GetAsociacionByAsocId(ByVal AsocId As String) As DataSet
        Dim ds As New DataSet
        Dim mstrCmd As String

        mstrCmd = "exec GetAsociacionByAsocId "
        mstrCmd = mstrCmd + " @asoc_id = " + clsSQLServer.gFormatArg(AsocId, SqlDbType.VarChar)

        ds = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)

        Return ds

    End Function


End Class
