Imports System.Data.SqlClient


Public Class Clientes
   Inherits Generica

   Protected mstrTablaTele As String
   Protected mstrTablaDire As String
   Protected mstrTablaMail As String
   Protected mstrTablaInte As String
   Protected mstrPath As String
   Protected filFoto As System.Web.UI.HtmlControls.HtmlInputFile
   Protected filFirma As System.Web.UI.HtmlControls.HtmlInputFile
   Protected server As System.Web.HttpServerUtility
   Protected WithEvents txtFoto As NixorControls.TextBoxTab
   Protected WithEvents txtFirma As NixorControls.TextBoxTab

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaTele As String, ByVal pstrTablaDire As String, ByVal pstrTablaMail As String, ByVal pstrTablaInte As String, ByVal pdsDatos As DataSet, ByVal pfilFoto As System.Web.UI.HtmlControls.HtmlInputFile, ByVal pfilFirma As System.Web.UI.HtmlControls.HtmlInputFile, ByVal server As System.Web.HttpServerUtility, ByRef ptxtFoto As NixorControls.TextBoxTab, ByRef ptxtFirma As NixorControls.TextBoxTab)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaTele = pstrTablaTele
      Me.mstrTablaDire = pstrTablaDire
      Me.mstrTablaMail = pstrTablaMail
      Me.mstrTablaInte = pstrTablaInte
      Me.mdsDatos = pdsDatos
      Me.txtFoto = ptxtFoto
      Me.txtFirma = ptxtFirma
      Me.filFoto = pfilFoto
      Me.filFirma = pfilFirma
      Me.server = server
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaTele As String, ByVal pstrTablaDire As String, ByVal pstrTablaMail As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
        Me.mstrTabla = pstrTabla
        Me.mstrTablaTele = pstrTablaTele
        Me.mstrTablaDire = pstrTablaDire
        Me.mstrTablaMail = pstrTablaMail
    End Sub

    Public Overloads Function Alta(ByVal pbooAgru As Boolean) As String
        Return (mActualizar(pbooAgru))
    End Function

    Public Overloads Sub Modi(ByVal pbooAgru As Boolean)
        mActualizar(pbooAgru)
    End Sub

    Private Function mActualizar(ByVal pbooAgru As Boolean) As String
        Dim lstrTABLAId As String
        Dim lstrClieId As String
        Dim lTransac As SqlClient.SqlTransaction

        Try

            mstrPath = clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_foto_path")

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'Datos del cliente.
            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                '  lstrClieId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrClieId = clsSQLServer.gAltaExecuteNoQuery("", lTransac, mstrUserId, mstrTabla, MiRow)

            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrClieId = MiRow.Item(0).ToString
                'Eliminar los detalles anteriores.
                mBorrarDetas(lTransac, mstrTablaTele, lstrClieId)
                'mBorrarDetas(lTransac, mstrTablaDire, lstrClieId)
                mBorrarDetas(lTransac, mstrTablaMail, lstrClieId)
                mBorrarDetas(lTransac, mstrTablaInte, lstrClieId)
            End If

            'Tel�fonos del cliente



            For Each odrDetaTele As DataRow In mdsDatos.Tables(mstrTablaTele).Select()
                If odrDetaTele.Item("tecl_clie_id") Is DBNull.Value OrElse odrDetaTele.Item("tecl_clie_id") <= 0 Then
                    odrDetaTele.Item("tecl_clie_id") = lstrClieId
                    'ALTA 
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaTele, odrDetaTele)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaTele, odrDetaTele)
                End If
            Next

            'Direcciones del cliente
            For Each odrDetaDire As DataRow In mdsDatos.Tables(mstrTablaDire).Select()
                If odrDetaDire.Item("dicl_clie_id") Is DBNull.Value OrElse odrDetaDire.Item("dicl_clie_id") <= 0 Then
                    odrDetaDire.Item("dicl_clie_id") = lstrClieId
                    'ALTA 
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDire, odrDetaDire)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDire, odrDetaDire)
                End If
            Next

            'Mails del cliente
            For Each odrDetaMail As DataRow In mdsDatos.Tables(mstrTablaMail).Select()
                If odrDetaMail.Item("macl_clie_id") Is DBNull.Value OrElse odrDetaMail.Item("macl_clie_id") <= 0 Then
                    odrDetaMail.Item("macl_clie_id") = lstrClieId
                    'ALTA 
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaMail, odrDetaMail)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaMail, odrDetaMail)
                End If
            Next

            'Integrantes de la persona juridica
            For Each odrDetaInte As DataRow In mdsDatos.Tables(mstrTablaInte).Select("_peti_id = 2 and _clie_agru = 0")
                If odrDetaInte.Item("clag_clie_id") Is DBNull.Value OrElse odrDetaInte.Item("clag_clie_id") <= 0 Then
                    odrDetaInte.Item("clag_clie_id") = lstrClieId
                    'ALTA 
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaInte, odrDetaInte)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaInte, odrDetaInte)
                End If
            Next

            If pbooAgru Then
                'Integrantes de la agrupacion
                For Each odrDetaInte As DataRow In mdsDatos.Tables(mstrTablaInte).Select("_peti_id <> 2")
                    If odrDetaInte.Item("clag_clie_id") Is DBNull.Value OrElse odrDetaInte.Item("clag_clie_id") <= 0 Then
                        odrDetaInte.Item("clag_clie_id") = lstrClieId
                        'ALTA 
                        clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaInte, odrDetaInte)
                    Else
                        'MODIFICACION 
                        clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaInte, odrDetaInte)
                    End If
                Next
            End If

            mGuardarArchivos(lstrClieId, mstrPath)

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrClieId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Private Sub mGuardarArchivos(ByVal lstrClieId As String, ByVal lstrPath As String)
        Dim lstrNombre As String
        Dim lstrCarpeta As String

        lstrCarpeta = lstrPath
        If Not (filFoto.PostedFile Is Nothing) AndAlso filFoto.PostedFile.FileName <> "" Then
            lstrNombre = filFoto.PostedFile.FileName
            lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)
            lstrNombre = lstrClieId & "_" & lstrNombre
            filFoto.PostedFile.SaveAs(server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre)
            txtFoto.Text = lstrNombre
        Else
            If txtFoto.Text = "" And Not mdsDatos.Tables(0).Rows(0).IsNull("clie_foto") Then
                mdsDatos.Tables(0).Rows(0).Item("clie_foto") = DBNull.Value
            End If
        End If

        If Not (filFirma.PostedFile Is Nothing) AndAlso filFirma.PostedFile.FileName <> "" Then
            lstrNombre = filFirma.PostedFile.FileName
            lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)
            lstrNombre = lstrClieId & "_" & lstrNombre
            filFirma.PostedFile.SaveAs(server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre)
            txtFirma.Text = lstrNombre
        Else
            If txtFirma.Text = "" And Not mdsDatos.Tables(0).Rows(0).IsNull("clie_firma") Then
                mdsDatos.Tables(0).Rows(0).Item("clie_firma") = DBNull.Value
            End If
        End If

    End Sub

    Private Sub mBorrarDetas(ByVal lTransac As SqlClient.SqlTransaction, ByVal mstrTablaDet As String, ByVal pstrClieId As String)
        For Each odrDeta As DataRow In mdsDatos.Tables(mstrTablaDet).Select("", "", DataViewRowState.Deleted)
            odrDeta.RejectChanges()
            clsSQLServer.gBaja(lTransac, mstrUserId, mstrTablaDet, odrDeta.Item(0).ToString)
            odrDeta.Table.Rows.Remove(odrDeta)
        Next
    End Sub

    Public Shared Function BuscarClie(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(",") 'clie_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            lstrCmd.Append("exec clientes_consul ")
            lstrCmd.Append(lvstrArgs(0))
            ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

            If ldsDatos.Tables(0).Rows.Count = 0 Then
                Return ("")
            Else
                With ldsDatos.Tables(0).Rows(0)
                    lstrRet.Append(.Item("clie_apel").ToString)
                    lstrRet.Append("|")
                    lstrRet.Append("Apellido:")
                    lstrRet.Append(.Item("clie_apel").ToString.Trim)
                    lstrRet.Append(" - Nombre:")
                    lstrRet.Append(.Item("clie_nomb").ToString.Trim)
                    lstrRet.Append(" - Fantasia:")
                    lstrRet.Append(.Item("clie_fanta").ToString.Trim)
                    lstrRet.Append(" - CUIT:")
                    lstrRet.Append(.Item("clie_cuit").ToString.Trim)
                End With

                Return (lstrRet.ToString)
            End If
        End If
    End Function

    Public Shared Function ObtenerDatosIncobCliente(ByVal pstrConn As String, ByVal pstrArgs As String) As String
        Dim ldsDatos As DataSet
        Dim lstrCmd As New System.Text.StringBuilder
        Dim lvstrArgs() As String = pstrArgs.Split(",") 'clie_id,soci_id
        Dim lstrRet As New System.Text.StringBuilder

        If lvstrArgs(0) = "" Then
            Return ("")
        Else
            lstrCmd.Append("exec clientes_datos_incob_consul @clie_id=")
            lstrCmd.Append(lvstrArgs(0))
            lstrCmd.Append(", @soci_id=")
            lstrCmd.Append(lvstrArgs(1))
            ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, lstrCmd.ToString)

            If ldsDatos.Tables(0).Rows.Count = 0 Then
                Return ("")
            Else
                With ldsDatos.Tables(0).Rows(0)
                    lstrRet.Append(.Item("deuda").ToString)
                    lstrRet.Append("|")
                    lstrRet.Append(.Item("soci_id").ToString.Trim)
                    lstrRet.Append("|")
                    lstrRet.Append(.Item("soci_nume").ToString.Trim)
                    lstrRet.Append("|")
                    lstrRet.Append(.Item("deuda_social").ToString.Trim)
                    lstrRet.Append("|")
                    lstrRet.Append(.Item("deuda_ctac").ToString.Trim)
                End With

                Return (lstrRet.ToString)
            End If
        End If
    End Function

    Public Function GetPaisByClienteId(ByVal pClienteId As Integer) As Integer
        Dim mstrCmd As String
        Dim dt As DataTable
        Dim PaisId As Integer

        PaisId = 0

        mstrCmd = "exec GetPaisByClienteId "
        mstrCmd = mstrCmd + " @Clie_Id = " + clsSQLServer.gFormatArg(pClienteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then
            PaisId = Convert.ToInt16(dt.Rows(0).Item("pcia_pais_id").ToString())
        
        End If

        Return PaisId
    End Function

    Public Function GetClienteIdByRazaCriador(ByVal RazaId As String, ByVal CriadorId As String) As Integer
        Dim dtCliente As New DataTable
        Dim idCliente As Int32

        Dim mstrCmd As String
        dtCliente.TableName = "dtCliente"

        idCliente = 0

        mstrCmd = "exec GetClienteIdByRazaCriador "
        mstrCmd = mstrCmd + " @RazaId = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar) + ","
        mstrCmd = mstrCmd + " @CriadorId = " + clsSQLServer.gFormatArg(CriadorId, SqlDbType.VarChar)

        dtCliente = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dtCliente.Rows.Count > 0 Then
            idCliente = dtCliente.Rows(0).Item("clie_id")
        End If

        Return idCliente

    End Function

    Public Function GetTelefonosActivosClientes(ByVal pClienteId As Int32) As DataRow

        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim mstrCmd As String


            mstrCmd = "exec GetTelefonosActivosClientes "
            mstrCmd = mstrCmd + " @clie_id = " + clsSQLServer.gFormatArg(pClienteId, SqlDbType.Int)
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

            If dt.Rows.Count > 0 Then
                For Each drCliente As DataRow In dt.Rows
                    Return drCliente
                Next
            End If
            Return dr
        Catch ex As Exception
            clsError.gManejarError("clientes.GetTelefonosActivosClientes", "ERROR", ex)
        End Try

    End Function
    Public Function GetDireccionActivasClientes(ByVal pClienteId As Int32) As DataRow

        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim mstrCmd As String


            mstrCmd = "exec GetDireccionActivasClientes"
            mstrCmd = mstrCmd + " @clie_id = " + clsSQLServer.gFormatArg(pClienteId, SqlDbType.Int)
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

            If dt.Rows.Count > 0 Then
                For Each drCliente As DataRow In dt.Rows
                    Return drCliente
                Next
            End If
            Return dr
        Catch ex As Exception
            clsError.gManejarError("clientes.GetDireccionActivasClientes", "ERROR", ex)
        End Try

    End Function
    Public Function GetMailsActivosClientes(ByVal pClienteId As Int32, ByVal pUserId As Int32) As DataRow

        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim mstrCmd As String


            mstrCmd = "exec GetMailsActivosClientes "
            mstrCmd = mstrCmd + " @clie_id = " + clsSQLServer.gFormatArg(pClienteId, SqlDbType.Int)
            mstrCmd = mstrCmd + ", @audi_user = " + clsSQLServer.gFormatArg(pUserId, SqlDbType.Int)

            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

            If dt.Rows.Count > 0 Then
                For Each drCliente As DataRow In dt.Rows
                    Return drCliente
                Next
            End If
            Return dr
        Catch ex As Exception
            clsError.gManejarError("clientes.GetMailsActivosClientes", "ERROR", ex)
        End Try

    End Function

    Public Function GetMailAdicionalByClienteId(ByVal pClienteId As Int32) As DataRow

        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim mstrCmd As String


            mstrCmd = "exec GetMailTopAdicionalByClienteId "
            mstrCmd = mstrCmd + " @clie_id = " + clsSQLServer.gFormatArg(pClienteId, SqlDbType.Int)
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

            If dt.Rows.Count > 0 Then
                For Each drMailDefault As DataRow In dt.Rows
                    Return drMailDefault
                Next
            End If
            Return dr
        Catch ex As Exception
            clsError.gManejarError("clientes.GetMailTopAdicionalByClienteId", "ERROR", ex)
        End Try

    End Function
    Public Function GetDireAdicionalByClienteId(ByVal pClienteId As Int32) As DataRow

        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim mstrCmd As String


            mstrCmd = "exec GetDireTopAdicionalByClienteId "
            mstrCmd = mstrCmd + " @clie_id = " + clsSQLServer.gFormatArg(pClienteId, SqlDbType.Int)
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

            If dt.Rows.Count > 0 Then
                For Each drDireDefault As DataRow In dt.Rows
                    Return drDireDefault
                Next
            End If
            Return dr
        Catch ex As Exception
            clsError.gManejarError("clientes.GetDireTopAdicionalByClienteId", "ERROR", ex)
        End Try

    End Function
    Public Function GetTeleAdicionalByClienteId(ByVal pClienteId As Int32) As DataRow

        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim mstrCmd As String


            mstrCmd = "exec GetTeleTopAdicionalByClienteId "
            mstrCmd = mstrCmd + " @clie_id = " + clsSQLServer.gFormatArg(pClienteId, SqlDbType.Int)
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

            If dt.Rows.Count > 0 Then
                For Each drMailDefault As DataRow In dt.Rows
                    Return drMailDefault
                Next
            End If
            Return dr
        Catch ex As Exception
            clsError.gManejarError("clientes.GetTeleTopAdicionalByClienteId", "ERROR", ex)
        End Try

    End Function

    Public Function GetMailDefaultByClienteId(ByVal pClienteId As Int32) As DataRow

        Try
            Dim dt As New DataTable
            Dim dr As DataRow
            Dim mstrCmd As String


            mstrCmd = "exec GetMailDefaultByClienteId "
            mstrCmd = mstrCmd + " @clie_id = " + clsSQLServer.gFormatArg(pClienteId, SqlDbType.Int)
            dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

            If dt.Rows.Count > 0 Then
                For Each drMailDefault As DataRow In dt.Rows
                    Return drMailDefault
                Next
            End If
            Return dr
        Catch ex As Exception
            clsError.gManejarError("Clientes.GetMailDefaultByClienteId", "ERROR", ex)
        End Try

    End Function
End Class


