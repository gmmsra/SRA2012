Public Class Constantes
    Public Const gTab_ActasCD As String = "actas_cd"
    Public Const gTab_ActasCDSoli As String = "soli_actas_cd"
    Public Const gTab_ActiTele As String = "actividades_tele"
    Public Const gTab_ActiMail As String = "actividades_mails"
    Public Const gTab_ActiDire As String = "actividades_dire"
    Public Const gTab_Actividades As String = "actividades"
    Public Const gTab_Ajustes As String = "ajustes"
    Public Const gTab_AjustesConcep As String = "ajustes_concep"
    Public Const gTab_Alertas As String = "alertas"
    Public Const gTab_AlertasHorarios As String = "alertas_horarios"
    Public Const gTab_AlertasUsuarios As String = "alertas_usuarios"
    Public Const gTab_AlertasLog As String = "alertas_log"
    Public Const gTab_Alumnos As String = "alumnos"
    Public Const gTab_Aranceles As String = "aranceles"
    Public Const gTab_Arqueos As String = "arqueos"
    Public Const gTab_AsientosEnvios As String = "asientos_envios"
    Public Const gTab_AsientosEnviosCabe As String = "asientos_envios_cabe"
    Public Const gTab_AsientosEnviosDeta As String = "asientos_envios_deta"
    Public Const gTab_Billetes As String = "billetes"
    Public Const gTab_Asociaciones As String = "asociaciones"
    Public Const gTab_AutoridAsoc As String = "autorid_asoc"
    Public Const gTab_AsocTele As String = "tele_asoc"
    Public Const gTab_AsocMails As String = "mails_asoc"
    Public Const gTab_AsociacionesRazas As String = "asociaciones_razas"
    Public Const gTab_Calificaciones As String = "calificaciones"
    Public Const gTab_Carnets As String = "carnets"
    Public Const gTab_Cheques As String = "cheques"
    Public Const gTab_ChequesMovim As String = "movim_cheques"
    Public Const gTab_Ciclos As String = "ciclos"
    Public Const gTab_CiclosMaterias As String = "ciclos_materias"
    Public Const gTab_Clientes As String = "clientes"
    Public Const gTab_ClientesEntidades As String = "clientes_entidades"
    Public Const gTab_ClientesAgrup As String = "clientes_agrup"
    Public Const gTab_ClientesIncob As String = "incobrables"
    Public Const gTab_ClientesIncobDeta As String = "incob_deta"
    Public Const gTab_Comisiones As String = "Comisiones"
    Public Const gTab_ComisionesCambios As String = "Comisiones_cambios"
    Public Const gTab_ComisionesDias As String = "comisiones_dias"
    Public Const gTab_ComisionesInscrip As String = "comisiones_inscrip"
    Public Const gTab_ComisionesProfe As String = "comisiones_profe"
    Public Const gTab_ComisionesSemana As String = "comisiones_semana"
    Public Const gTab_Comprobantes As String = "comprobantes"
    Public Const gTab_ComprobACuenta As String = "comprob_a_cuenta"
    Public Const gTab_ComprobAcuses As String = "comprob_acuses"
    Public Const gTab_ComprobDeta As String = "comprob_deta"
    Public Const gTab_ComprobAranc As String = "comprob_aranc"
    Public Const gTab_ComprobArancDeta As String = "comprob_aranceles_deta"
    Public Const gTab_ProformaArancDeta As String = "proforma_aranceles_deta"
    Public Const gTab_ComprobConcep As String = "comprob_concep"
    Public Const gTab_ComprobAutor As String = "comprob_autor"
    Public Const gTab_ComprobCuotas As String = "comprob_cuotas"
    Public Const gTab_ComprobVtos As String = "comprob_vtos"
    Public Const gTab_ComprobPagos As String = "comprob_pagos"
    Public Const gTab_ComprobSobreTasaVtos As String = "COMPROB_Sobre_Tasa_Vtos"
    Public Const gTab_ComprobPagosCuotas As String = "comprob_pagos_cuotas"
    Public Const gTab_Contactos As String = "contactos"
    Public Const gTab_Convenios As String = "convenios"
    Public Const gTab_ClasifiContactos As String = "clasifi_contactos"
    Public Const gTab_CuentasCtes As String = "cuentas_ctes"
    Public Const gTab_ClientesDire As String = "dire_clientes"
    Public Const gTab_DebitosCabe As String = "debitos_cabe"
    Public Const gTab_DebitosDeta As String = "debitos_deta"
    Public Const gTab_Depositos As String = "depositos"
    Public Const gTab_Depositos_Cheques As String = "depo_cheques"
    Public Const gTab_Divisiones As String = "divisiones"
    Public Const gTab_DivisionesInscrip As String = "divisiones_inscrip"
    Public Const gTab_ClientesEstudios As String = "estudios_clientes"
    Public Const gTab_ClientesExplota As String = "explota_clientes"
    Public Const gTab_ClientesFunciones As String = "funciones_clientes"
    Public Const gTab_Embargos As String = "embargos"
    Public Const gTab_EmbargosDeta As String = "embargos_deta"
    Public Const gTab_EmerAgrope As String = "emer_agrope"
    Public Const gTab_Especies As String = "especies"
    Public Const gTab_Fallas As String = "fallas"
    Public Const gTab_Firmantes As String = "firmantes"
    Public Const gTab_ImplantesCentros As String = "Implante_ctros"
    Public Const gTab_ImplantesCentrosDire As String = "dire_implantes"
    Public Const gTab_ImplantesCentrosMails As String = "mails_implantes"
    Public Const gTab_ImplantesCentrosTele As String = "tele_implantes"
    Public Const gTab_ImplantesCentrosAutor As String = "autorizados_implantes"
    Public Const gTab_Inscripciones As String = "inscripciones"
    Public Const gtab_Inspec_Cria_Deta As String = "Inspec_Cria_Deta"
    Public Const gtab_Inspecciones_Criadores As String = "Inspecciones_criadores"
    Public Const gTab_InscripCMate As String = "inscrip_cmate"
    Public Const gTab_InstituEntidades As String = "institu_entidades"
    Public Const gTab_Institutos As String = "institutos"
    Public Const gTab_IvaPosic As String = "iva_posic"
    Public Const gTab_IvaTasas As String = "iva_tasas"
    Public Const gTab_IIBB As String = "iibb_catego"
    Public Const gTab_IIBBTasas As String = "iibb_tasas"
    Public Const gTab_ClientesMails As String = "mails_clientes"
    Public Const gTab_MailsModelos As String = "mails_modelos"
    Public Const gTab_MailsModeActi As String = "mails_mode_activi"
    Public Const gTab_MailsModeCate As String = "mails_mode_catego"
    Public Const gTab_Mesas As String = "mesas_exam"
    Public Const gTab_MesasAlum As String = "mesas_alumnos"
    Public Const gTab_MesasProfe As String = "mesas_profe"
    Public Const gTab_MesasVota As String = "mesas_vota"
    Public Const gTab_MesasVotaCate As String = "mesas_cate"
    Public Const gTab_TextosComunicados As String = "textos_comunicados"
    Public Const gTab_TextosComunicadosCate As String = "textos_comunicados_catego"

    Public Const gTab_Monedas As String = "monedas"
    Public Const gTab_SociosMovim As String = "movim_socios"
    Public Const gTab_OrdenVota As String = "orden_vota"
    Public Const gTab_OrganContactos As String = "organ_contactos"
    Public Const gTab_ClientesOcupaciones As String = "ocupaciones_clientes"
    Public Const gTab_PagosPlan As String = "Pagos_Plan"
    Public Const gTab_PagosCuotas As String = "Pagos_Cuotas"
    Public Const gTab_PlanPagos As String = "plan_pagos"
    Public Const gTab_PlanPagosConcep As String = "plan_pagos_concep"
    Public Const gTab_PlanPagosDeta As String = "plan_pagos_deta"
    Public Const gTab_PlanPagosVtos As String = "plan_pagos_vtos"
    Public Const gTab_Plantel_Base As String = "rg_plantel_base"
    Public Const gTab_Plantel_Base_Detalle As String = "rg_plantel_base_deta"
    Public Const gTab_PreciosLista As String = "precios_lista"
    Public Const gTab_PreciosAran As String = "precios_aran"
    Public Const gTab_Prestamos As String = "Prestamos"
    Public Const gTab_PrestamosDeta As String = "Prestamos_deta"
    Public Const gTab_PromoCategorias As String = "promo_categorias"
    Public Const gTab_PromoClientes As String = "promo_clientes"
    Public Const gTab_Promociones As String = "promociones"

    Public Const gTab_Proformas As String = "proformas"
    Public Const gTab_ProformaDeta As String = "proforma_deta"
    Public Const gTab_ProformaAranc As String = "proforma_aranc"
    Public Const gTab_ProformaConcep As String = "proforma_concep"
    Public Const gTab_ProformaAutor As String = "proforma_autor"

    Public Const gTab_Razas As String = "razas"
    Public Const gTab_RazasDocum As String = "razas_Docum"
    Public Const gTab_RazasPadres As String = "razas_razas"
    Public Const gTab_Rendiciones As String = "rendiciones"
    Public Const gTab_RendicionesDepo As String = "rendiciones_depo"
    Public Const gTab_RendicionesPagos As String = "rendiciones_pagos"
    Public Const gTab_RendicionesFaltantes As String = "rendiciones_faltantes"


    Public Const gTab_SaldosACuenta As String = "saldos_a_cuenta"
    Public Const gTab_Socios As String = "socios"
    Public Const gTab_SoliIngreso As String = "soli_ingreso"
    Public Const gTab_SoliCambios As String = "soli_cambios"
    Public Const gTab_ClientesTele As String = "tele_clientes"
    Public Const gTab_TarjetasClientes As String = "tarjetas_clientes"
    Public Const gTab_TarjetasClientesExten As String = "tarjetas_clientes_exten"
    Public Const gTab_Usuarios As String = "usuarios"
    Public Const gTab_UsuariosAutorizaTipos As String = "usuarios_autoriza_tipos"

    Public Const gTab_TeleContactos As String = "tele_contactos"
    Public Const gTab_MailsContactos As String = "mails_contactos"
    Public Const gTab_EstudiosContactos As String = "estudios_contactos"
    Public Const gTab_ExplotaContactos As String = "explota_contactos"
    Public Const gTab_FuncionesContactos As String = "funciones_contactos"
    Public Const gTab_OcupacionesContactos As String = "ocupaciones_contactos"

    'RRGG
    Public Const gTab_AutorizadosCriadores As String = "autorizados_criadores"
    Public Const gTab_ComisionesRazas As String = "comisiones_razas"
    Public Const gTab_ComisionesAgenda As String = "Comisiones_agenda"
    Public Const gTab_ComisionesDocum As String = "Comisiones_docum"
    Public Const gTab_ComisionesCriadores As String = "Comisiones_criadores"
    Public Const gTab_ComisionesActas As String = "Comisiones_actas"
    Public Const gTab_Criadores As String = "criadores"
    Public Const gTab_Establecimientos As String = "establecimientos"
    Public Const gTab_EstablecRazas As String = "establec_razas"
    Public Const gTab_EstablecCriador As String = "establec_criador"
    Public Const gTab_Requisitos As String = "rg_requisitos"
    Public Const gTab_Prefijos_Cabecera As String = "prefijos_cabe"
    Public Const gTab_Prefijos_Detalle As String = "prefijos"
    Public Const gTab_Productos As String = "productos"
    Public Const gTab_ProductosProp As String = "productos_prop"
    Public Const gTab_ProductosAnalisis As String = "productos_analisis"
    Public Const gTab_Criaderos As String = "criaderos"
    Public Const gTab_Criaderos_Razas As String = "criaderos_razas"
    Public Const gTab_Anillos_Movim_Cabe As String = "rg_anillos_movim_cabe"
    Public Const gTab_Anillos As String = "rg_anillos"
    Public Const gTab_Anillos_Razas As String = "rg_anillos_razas"
    Public Const gTab_Anillos_Movim_Deta As String = "rg_anillos_movim_deta"
    Public Const gTab_ServiDenuncias As String = "rg_servi_denuncias"
    Public Const gTab_ServiDenunciasDeta As String = "rg_servi_denuncias_deta"
    Public Const gTab_ServiDenunciasDetaPadres As String = "rg_servi_denuncias_deta_padres"
    Public Const gTab_ProductosInscrip As String = "rg_productos_inscrip"
    Public Const gTab_ProductosInscripDocum As String = "rg_productos_inscrip_docum"
    Public Const gTab_ProductosDocum As String = "productos_docum"
    Public Const gTab_ProductosNumeros As String = "productos_numeros"
    Public Const gTab_ProductosRelaciones As String = "productos_relaciones"
    Public Const gTab_Tramites As String = "tramites"
    Public Const gTab_Tramites_Deta As String = "tramites_deta"
    Public Const gTab_Tramites_Docum As String = "tramites_docum"
    Public Const gTab_Tramites_Obse As String = "tramites_obse"
    Public Const gTab_Tramites_Personas As String = "tramites_personas"
    Public Const gTab_Tramites_Productos As String = "tramites_productos"
    Public Const gTab_Tramites_Embriones_Frescos As String = "tramites_embriones_frescos"
    Public Const gTab_Tramites_Plantilla As String = "rg_tramites_plan"
    Public Const gTab_Tramites_Plantilla_Deta As String = "rg_tramites_plan_deta"
    Public Const gTab_Tramites_Plantilla_Razas As String = "rg_tramites_plan_razas"
    Public Const gTab_Temporarios As String = "rg_temporarios"
    Public Const gTab_GruposValidacion As String = "rg_grupos_vali"
    Public Const gTab_GruposValidacionRazas As String = "grupos_razas"
    Public Const gTab_GruposValidacionReglas As String = "grupos_reglas"
    Public Const gTab_Productos_Inspec As String = "Productos_Inspec"
    Public Const gTab_LabTurnosCtrol As String = "lab_turnos_ctrol"
    Public Const gTab_Importadores As String = "importadores"
    Public Const gTab_BajaDenun As String = "baja_denun"
    Public Const gTab_BajaDenunDeta As String = "baja_denun_deta"
    Public Const gTab_TeDenun As String = "te_denun"
    Public Const gTab_TeDenunDeta As String = "te_denun_deta"
    Public Const gTab_SemenStock As String = "semen_stock"
    Public Const gTab_RangosAranDeta As String = "rangos_aran_deta"
    Public Const gTab_RangosAranCabe As String = "rangos_aran_cabe"
    Public Const gTab_ArancelesRangos As String = "aranceles_rangos"
    Public Const gTab_EmbrionesStock As String = "embriones_stock"

    Public Const gTab_SociosIncob As String = "incobrables_socios"
    Public Const gTab_SociosIncobDeta As String = "incob_deta_socios"

    Public Const TramiteVigente = "Vigente"
    Public Const TramiteDeBaja = "De Baja"
    Public Const TramiteRetenida = "Retenida"

    'Modulo de Aves
    Public Const gTab_Jaulas As String = "jaulas"

    Public Enum Estados As Integer
        Alertas_vigente = 52
        Alumnos_Vigente = 4
        Alumnos_De_Baja = 7

        Carreras_Vigente = 10
        Carreras_De_Baja = 9
        Clientes_Vigente = 3
        Clientes_De_baja = 6

        CentrosImplanes_Vigente = 69
        CentrosImplanes_De_Baja = 71


        DebitoDeta_Vigente = 41
        DebitoDeta_Rechazado = 43
        DebitoDeta_Aprobado = 45
        DebitoDeta_Devuelto = 46
        DebitoDeta_Incluido = 51


        Materias_Vigente = 5
        Materias_De_Baja = 8


        Prestamos_Ingresado = 107
        Prestamos_Anulado = 108
        Prestamos_Retenido = 109

        Embargos_Ingresado = 113
        Embargos_Anulado = 114
        Embargos_Retenido = 115

        Profesores_Vigente = 11
        Profesores_De_Baja = 12
        Productos_Vigente = 73
        Productos_Baja = 74
        Productos_Retenido = 102
        Productos_Exportado = 96


        Rendiciones_Emitida = 61
        Rendiciones_Recibida = 62
        Rendiciones_Incompleta = 63

        RRGG_Solicitado = 56
        RRGG_Reservado = 100
        RRGG_Vigente = 58
        RRGG_NoVigente = 59
        RRGG_NoUtilizado = 60

        Socios_Vigente = 15
        Socios_Renuncia = 16
        Socios_Eliminado = 17
        Socios_Suspendido = 18
        Socios_Suspendido_Moroso = 19
        Socios_Ex_Menor = 20
        Socios_Fallecido = 21
        Socios_Renuncia_en_Tramite = 22
        Socios_Licencia = 23
        SoliActasCD_Aprobada = 27
        SoliActasCD_Rechazada = 28
        SoliActasCD_Pospuesta = 29
        SoliActasCD_Presentada = 100


       


        Tramites_Vigente = 85
        Tramites_Baja = 86
        'Tramites_Retenida = 102
        Tramites_Retenida = 110

    End Enum

    Public Enum EstaTipos As Integer
        EstaTipo_Clientes = 1
        EstaTipo_Alumnos = 2
        EstaTipo_Socios = 3
        EstaTipo_Carreras = 10
        EstaTipo_Materias = 11
        EstaTipo_Profesores = 12
        EstaTipo_Solicitud_Ingreso = 20
        EstaTipo_SoliActasCD = 21
        EstaTipo_DebitosDeta = 25
        EstaTipo_Alertas = 40
        EstaTipo_CtroImplante = 60
        EstaTipo_Trámites = 90
        EstaTipo_Prestamos = 92
        EstaTipo_Embargos = 93
    End Enum
 
    Public Enum Actividades As Integer
        Administracion = 1
        Socios = 2
        RegistrosGenealógicos = 3
        Laboratorio = 4
        LABORATORIO_SUELOS_PASTOS_AGUA = 5
        ISEA = 6
        EXPOSICION_GANADERA_PALERMO = 7
        SERVICIOS_INSTITUCIONALES = 9
        CEIDA = 10
        SEMINARIOS_SRA = 15
    End Enum

    Public Enum Categorias As Integer
        Menor = 1
        Activo = 2
        Vitalicio = 3
        Honorario = 4
        Honor = 5
        Adherente = 6
        Menor_F = 7
        Adherente_F = 8
    End Enum

    Public Enum PeriodoTipos As Integer
        Hora = 1
        Dia = 2
        Semana = 3
        Mes = 4
        Bimestre = 5
        Trimestre = 6
        Cuatrimestre = 7
        Semestre = 8
        Año = 9
    End Enum

    Public Enum ComprobTipos As Integer
        Cuota_Social = 1
        Interes_CS_Dev = 2
        Ajuste = 7
        Interes_Cuota_Social = 8
        SolicitudIngreso = 10
        CambioCategoria = 11
        Transferencias = 13
        Proforma = 28
        Factura = 29
        Recibo = 30
        NC = 31
        ND = 32
        Recibo_Acuse = 33
        AplicacionCreditos = 34
        Transf_CC_a_CS = 36
        Emer_Agrop = 37
        Deudores_Incobrables = 38
        Interes_CtaCte = 41
        Recibo_Acreditacion = 42
        SobreTasasAlVto_CtaCte = 43 ' Dario 2013-07-22 
        Depositos = 50
        CancelacionFC = 100
        PlanFacilidades = 101
    End Enum

    Public Enum PagosTipos As Integer
        Efectivo = 1
        Cheque = 2
        Tarjeta = 3
        DineroElec = 4
        AcredBancaria = 5
        AplicCredi = 6
        Retenciones = 7
    End Enum

    Public Enum ComprobPagosTipos As Integer
        Contado = 1
        CtaCte = 2
        Tarjeta = 3
        PagoDiferido = 4
    End Enum

    Public Enum Modulos As Integer
        Devengamientodecuotas = 1
        SolicituddeIngreso = 2
        CambiodeCategoría = 3
        EmergenciaAgropecuaria = 4
        DeudoresIncobrables = 5
        DebitoAutomatico = 6
        DebitoAutomatico_Devolucion = 7
        Facturacion = 8
        Cobranza = 9
        ChequesPostdatados = 10
        PlanPagos = 11
        AplicacionCreditos = 12
        AplicacionCreditosSocios = 13
        Alumnos_Inscripción = 14
        Alumnos_FacturacionCuotas = 15
        FacturacionExterna = 16
        ChequesRechazo = 17
        ChequesVenta = 18
        CuponesRechazo = 19
        AcreditacionesConfirmación = 20
        AjusteCtaCte = 21
        Alumnos_FacturacionManual = 22
        Alumnos_Débito_Automático = 23
        Facturación_Proformas = 24
        Alumnos_Examen = 25
        SobreTasa_PagoFuera_termino = 26
    End Enum

    Public Enum LongitudesTipos As Integer
        Entero = 9
    End Enum

    Public Enum ChequesTipos As Integer
        Comun = 1
        Diferido = 2
        Postdatado = 3
    End Enum

    Public Enum ProductosRelacion As Integer
        Padre = 1
        Madre = 2
        Receptora = 3
    End Enum

    Public Enum Formulas As Integer
        Importe_ingresado = 0
        Ajuste_por_convenio = 1
        Recargo_por_Mora = 2
        Descuento_Facturacion_sin_cargo = 3
        Cuota_Curso = 4
        Rechazo_de_cupon_ajuste_cta_cte = 6
        Rechazo_de_cheque_ajuste_cta_cte = 7
        Gasto_administrativo_por_rechazo_de_Chq = 8
        Sobretasa_Palermo_tipificacion_bovinos = 9
        Recargo_fuera_de_Fecha = 10
        Dto_por_Promocion_en_Laboratorio = 11
        Descuento_por_Cantidad = 12
        Matricula_de_Cursos = 13
        Descuento_Facturacion_Exenta = 14
        Reintegro_por_Prestamos_de_Honor = 15
        Descuento_por_Beca = 16
        Recargo_por_Tramite_Urgente = 17
        Cancelacion_Docum_por_Aplicacion_Saldos = 18
        Cuotas_Seminarios = 19
        Matriculas_Seminarios = 20
        Descuento_socios_en_Cursos = 21
        Descuento_por_pago_adelantado_de_cuotas = 22
        Solicitud_de_Ingreso = 23
        Plan_de_Facilidades_de_Pago = 24
        Baja_de_Saldos_incobrables = 25
        Adicional_por_suspension_de_servicios = 26
        Dto_pago_de_cuotas_con_DebAutom_ISEA = 27
        Cantidad = 40
        aranceles_migracion = 199
        cantidad_X_valor = 50
        cantidad_X_valor_dif = 51
    End Enum

    Public Enum Especies As Integer
        Equinos = 2
        Bovinos = 3
        Caprinos = 9
        Peliferos = 10
        Camelidos = 11
        Ovinos = 12
        BovinosNOHolando = 0
        RazaHolando = 2
        RazaCriolla = 45
    End Enum


    Public Enum AlertasTipos As Integer
        PROFORMAS_FACTURABLES_PENDIENTES_POR_SALDO_DEUDOR = 1
        CHEQUES_POSTDATADOS = 2
        AJUSTES_DE_CUENTA_CORRIENTE_PENDIENTES_DE_APROBACION = 3
        LISTA_DE_CLIENTES_INCOBRABLES = 4
        PROFORMAS_EXPOSICIONES_PENDIENTES_DE_FACTURACION = 5
        NUMEROS_DE_CAI_PROXIMOS_A_VENCER = 6
        ACREDITACION_BANCARIA_CONFIRMACION = 7
        ACREDITACION_BANCARIA_INGRESO = 8
        ACREDITACION_BANCARIA_ANULACION = 9
        RECHAZO_DE_CHEQUES = 10
        RECHAZO_DE_CUPONES = 11
    End Enum

    Public Enum TramitesTipos As Integer
        TransferenciaProductos = 1
        ImportacionProductos = 10
        ImportacionSemen = 11
        ImportacionEmbriones = 12
        ExportacionEmbriones = 33
        TransferenciaEmbriones = 32
        ExportacionProductos = 20
        TransferenciaSemen = 30
        ExportacionSemen = 31
    End Enum

    Public Enum Roles As Integer
        Comprador = 1
        Vendedor = 2
    End Enum

    Public Enum TipoNovedad As Integer
        DenunciaNacimientoNatural = 1
        DenunciaServicio = 2
        DenunciaImplante = 3
        DenunciaMochos = 5
        DenunciaNacimientoTE = 7
        DenunciaBajaProducto = 15
        DenunciaRecuperacionEmbriones = 304

    End Enum

    Public Enum TipoServicios As Integer
        SiembraMultiple = 8
    End Enum

    Public Enum TipoMovimientos As Integer
        DenunciaRecuperacionEmbrionaria = 1
        DenunciaRecuperacionSemen = 2
    End Enum

    Public Enum AutorizaTipos As Integer
        Convenio_con_saldo_inferior = 1
        Descuentos_no_automáticos = 2
        Eliminar_descuentos_automáticos = 3
        Facturar_con_saldos_vencidos = 4
        Facturar_sin_cargo = 5
        Sobretasas_eliminadas = 6
        El_cliente_no_tiene_ctacte = 7
        Eliminar_concep_automático_de_laboratorio_turnos = 8
        Mantiene_valores_de_aranceles_proforma_30_días = 9
        Genera_NC_ND_sin_asociar_un_comprobante = 10
        Saldo_a_cuenta_pertenece_a_otra_actividad = 11
        CambioListaPrecios = 12
        ReasignacionClientes = 13
    End Enum
End Class