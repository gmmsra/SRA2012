Public Class Alertas
   Public Shared Sub EnviarMailAlertas(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrAlerId As String, Optional ByVal pstrParam As String = "")
      Try
         Dim ldsAlertas, ldsDatos As DataSet
         Dim lstrError As String
         Dim lstrAlerTipo As String
         Dim lstrAlerId As String
         Dim lbooAlertUser As Boolean = True

         ldsAlertas = clsSQLServer.gExecuteQuery(pstrConn, "alertas_proceso_consul @aler_id=" + pstrAlerId)
         lstrAlerTipo = clsSQLServer.gCampoValorConsul(pstrConn, "alertas_consul @aler_id=" + pstrAlerId, "aler_alti_id")

         mEscribir("Procesos obtenidos:" & ldsAlertas.Tables(0).Rows.Count)
         For Each ldrRow As DataRow In ldsAlertas.Tables(0).Rows
            lstrError = ""
            With ldrRow
               ldsDatos = clsSQLServer.gExecuteQuery(pstrConn, .Item("alti_proceso") + " " + pstrParam)

               mEscribir("Registros obtenidos:" & ldsDatos.Tables(0).Rows.Count)

               'solo env�a si encontr� algo
               If ldsDatos.Tables(0).Rows.Count > 0 Then
                  Try
                     If pstrAlerId <> 0 Then
                        Select Case lstrAlerTipo
                            Case SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_CONFIRMACION, _
                                SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_ANULACION, _
                                SRA_Neg.Constantes.AlertasTipos.RECHAZO_DE_CHEQUES, _
                                SRA_Neg.Constantes.AlertasTipos.RECHAZO_DE_CUPONES
                                lbooAlertUser = False
                            Case Else
                                lbooAlertUser = True
                        End Select
                     Else
                        lbooAlertUser = True
                     End If
                     gEnviarMensajeAlerta(pstrConn, pstrUserId, .Item("alti_template").ToString, .Item("usuarios").ToString, ldsDatos, "", lstrAlerTipo, lbooAlertUser)
                  Catch ex As Exception
                     mEscribir(ex.Message)
                     mEscribir(ex.StackTrace)
                     lstrError = ex.Message & vbCrLf & ex.StackTrace
                  End Try
               End If

               If .IsNull("alho_id") Then
                  mGuardarLog(pstrConn, pstrUserId, 0, lstrError, pstrAlerId)
                  'mEscribir("Receptores:" & .Item("usuarios").ToString)
               Else
                  'se dispara segun los horarios
                  lstrAlerId = clsSQLServer.gCampoValorConsul(pstrConn, "alertas_horarios_consul @alho_id=" & .Item("alho_id").ToString(), "alho_aler_id")
                  mGuardarLog(pstrConn, pstrUserId, .Item("alho_id"), lstrError, lstrAlerId)
                  mEscribir("AlhoId: " & .Item("alho_id") & ". Receptores:" & .Item("usuarios").ToString)
               End If
            End With
         Next

         mEscribir("Fin del envio")

      Catch ex As Exception
         mEscribir(ex.Message)
         mEscribir(ex.StackTrace)
         clsError.gManejarError(ex)
      End Try
   End Sub

   Public Shared Sub gEnviarMensajeAlerta(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTemplate As String, ByVal pstrIdsUsers As String, ByVal pdsDatos As DataSet, ByVal pstrAdjuPath As String, ByVal pstrAlerTipo As String, ByVal pbooAlertUsua As Boolean)
    Dim lvstrUsers() As String
    Dim lstrParaLoc As String
    Dim lstrMail As String
    Dim lstrUsuarioActual As String
    Dim lstrLnk As String

    If System.Configuration.ConfigurationSettings.AppSettings("conEnviarMails").ToString() = "1" Then
        lstrUsuarioActual = clsSQLServer.gObtenerValorCampo(pstrConn, Constantes.gTab_Usuarios, pstrUserId, "usua_apel").ToString

        lvstrUsers = Split(pstrIdsUsers, ",")
        For i As Integer = 0 To lvstrUsers.GetUpperBound(0)
        lstrMail = ""
        If lvstrUsers(i) <> "" Then
            If IsNumeric(lvstrUsers(i)) Then 'si es numerico, es el usuario
                lstrMail = clsSQLServer.gObtenerValorCampo(pstrConn, Constantes.gTab_Usuarios, lvstrUsers(i), "usua_email").ToString
            Else
                lstrMail = lvstrUsers(i)
            End If
            lstrParaLoc += ";" + lstrMail
        End If
        Next
        lstrLnk = System.Configuration.ConfigurationSettings.AppSettings("conLnkSistema").ToString()
        If pstrAlerTipo = SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_CONFIRMACION Then
           gEnviarMensajeDir_ABC(pstrConn, pstrTemplate, lstrParaLoc, pdsDatos, lstrLnk, lstrUsuarioActual, pstrAdjuPath, pstrAlerTipo, pbooAlertUsua)
        Else
          gEnviarMensajeDir(pstrConn, pstrTemplate, lstrParaLoc, pdsDatos, lstrLnk, lstrUsuarioActual, pstrAdjuPath, pstrAlerTipo, pbooAlertUsua)
        End If
    End If
    End Sub

    Public Shared Sub gEnviarMensajeDir(ByVal pstrConn As String, ByVal pstrTemplate As String, ByVal pstrMails As String, ByVal pdsDatos As DataSet, ByVal pstrLink As String, ByVal pstrUsuarioActual As String, ByVal pstrAdjuPath As String, ByVal pstrAlerTipo As Integer, ByVal pbooAlertUsua As Boolean)
        Dim lstrMsg As String = SRA_Neg.clsMail.mLeerTemplate(pstrTemplate + ".htm")
        Dim lstrAsunto As String = SRA_Neg.clsMail.mLeerTemplate(pstrTemplate + "_asunto.txt")
        Dim lstrText As String
        Dim lstrPara As String
        Dim lstrMsgRet As String
        Dim lintPosiIni As Integer
        Dim lintPosiFin As Integer
        Dim lstrTempHead As String
        Dim lstrTempRepe As String
        Dim lstrTempFoot As String
        Dim lstrTemp As String
        Dim lstrParaMulti As String = ""

        If System.Configuration.ConfigurationSettings.AppSettings("conEnviarMails").ToString() = "1" Then
            lintPosiIni = lstrMsg.IndexOf("<!-- inicio_repeticion -->")
            lintPosiFin = lstrMsg.IndexOf("<!-- fin_repeticion -->")
            If (lintPosiIni <> -1) Then
                If (lintPosiIni <> 0) Then
                    lstrTempHead = lstrMsg.Substring(0, lintPosiIni - 1)
                End If
                lstrTempRepe = lstrMsg.Substring(lintPosiIni + 26, (lintPosiFin - lintPosiIni - 26))
                lstrTempFoot = lstrMsg.Substring(lintPosiFin + 23)
            Else
                lstrTempRepe = lstrMsg
            End If

            lstrMsgRet = lstrTempHead
            Dim ldrRow As DataRow

            If pstrAlerTipo = SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_CONFIRMACION Then
                For i As Integer = 0 To pdsDatos.Tables(1).DefaultView.Count - 1
                    ldrRow = pdsDatos.Tables(1).DefaultView.Item(i).Row
                    If lstrParaMulti <> "" Then
                        lstrParaMulti = lstrParaMulti & ";"
                    End If
                    lstrParaMulti = lstrParaMulti & ldrRow.Item("usua_email")
                Next
            End If

            With pdsDatos.Tables(0)
                For i As Integer = 0 To .DefaultView.Count - 1
                    lstrTemp = lstrTempRepe
                    ldrRow = .DefaultView.Item(i).Row
                    For Each ldcCol As DataColumn In .Columns
                        If Not ldrRow.IsNull(ldcCol.ColumnName) Then
                            If ldcCol.DataType.Name = "DateTime" Then
                                lstrText = CDate(ldrRow.Item(ldcCol.ColumnName)).ToString("dd/MM/yyyy HH:mm")
                            Else
                                lstrText = ldrRow.Item(ldcCol.ColumnName).ToString
                            End If
                        Else
                            lstrText = ""
                        End If

                        lstrText = lstrText.Replace(Chr(10) + Chr(13), "<br>")

                        lstrTemp = lstrTemp.Replace("[" + ldcCol.ColumnName + "]", lstrText)
                        lstrAsunto = lstrAsunto.Replace("[" + ldcCol.ColumnName + "]", lstrText)
                        If Not pbooAlertUsua Then
                            lstrPara = ldrRow.Item("usua_email")
                        Else
                            lstrPara = pstrMails
                        End If
                    Next
                    lstrMsgRet += lstrTemp
                Next
            End With

            If lstrPara = "" Then
                Return
            End If

            If pstrAlerTipo = SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_CONFIRMACION Then
                lstrPara = lstrParaMulti
            End If

            lstrMsgRet += lstrTempFoot

            lstrMsgRet = lstrMsgRet.Replace("[getdate]", Date.Now.ToString("dd/MM/yyyy HH:mm").Replace("lnksist", pstrLink))
            lstrMsgRet = lstrMsgRet.Replace("lnksist", pstrLink)
            lstrAsunto = lstrAsunto.Replace("[getdate]", Date.Now.ToString("dd/MM/yyyy HH:mm"))

            lstrMsgRet = lstrMsgRet.Replace("[_usua_actual]", pstrUsuarioActual)
            lstrAsunto = lstrAsunto.Replace("[_usua_actual]", pstrUsuarioActual)

            SRA_Neg.clsMail.gEnviarMail(lstrPara, lstrAsunto, lstrMsgRet, pstrAdjuPath)

        End If

   End Sub
    Public Shared Sub gEnviarMensajeDir_ABC(ByVal pstrConn As String, ByVal pstrTemplate As String, ByVal pstrMails As String, ByVal pdsDatos As DataSet, ByVal pstrLink As String, ByVal pstrUsuarioActual As String, ByVal pstrAdjuPath As String, ByVal pstrAlerTipo As Integer, ByVal pbooAlertUsua As Boolean)
        Dim lstrMsg As String = SRA_Neg.clsMail.mLeerTemplate(pstrTemplate + ".htm")
        Dim lstrAsunto As String = SRA_Neg.clsMail.mLeerTemplate(pstrTemplate + "_asunto.txt")
        Dim lstrText As String
        Dim lstrPara As String
        Dim lstrMsgRet As String
        Dim lintPosiIni As Integer
        Dim lintPosiFin As Integer
        Dim lstrTempHead As String
        Dim lstrTempRepe As String
        Dim lstrTempFoot As String
        Dim lstrTemp As String
        Dim lstrParaMulti As String = ""

        If System.Configuration.ConfigurationSettings.AppSettings("conEnviarMails").ToString() = "1" Then
            lintPosiIni = lstrMsg.IndexOf("<!-- inicio_repeticion -->")
            lintPosiFin = lstrMsg.IndexOf("<!-- fin_repeticion -->")
            If (lintPosiIni <> -1) Then
                If (lintPosiIni <> 0) Then
                    lstrTempHead = lstrMsg.Substring(0, lintPosiIni - 1)
                End If
                lstrTempRepe = lstrMsg.Substring(lintPosiIni + 26, (lintPosiFin - lintPosiIni - 26))
                lstrTempFoot = lstrMsg.Substring(lintPosiFin + 23)
            Else
                lstrTempRepe = lstrMsg
            End If

            lstrMsgRet = lstrTempHead
            Dim ldrRow As DataRow
            With pdsDatos.Tables(0)
                For i As Integer = 0 To .DefaultView.Count - 1
                    lstrTemp = lstrTempRepe
                    ldrRow = .DefaultView.Item(i).Row
                    lstrText = ""
                    lstrMsgRet = lstrTempHead

                'If pstrAlerTipo = SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_CONFIRMACION Then
                '    For i As Integer = 0 To pdsDatos.Tables(1).DefaultView.Count - 1
                '        ldrRow = pdsDatos.Tables(1).DefaultView.Item(i).Row
                '        If lstrParaMulti <> "" Then
                '            lstrParaMulti = lstrParaMulti & ";"
                '        End If
                '        lstrParaMulti = lstrParaMulti & ldrRow.Item("usua_email")
                '    Next
                'End If
            lstrParaMulti = ldrRow.Item("usua_email")

            'With pdsDatos.Tables(0)
            '    For i As Integer = 0 To .DefaultView.Count - 1
            '        lstrTemp = lstrTempRepe
            '        ldrRow = .DefaultView.Item(i).Row
                    For Each ldcCol As DataColumn In .Columns
                        If Not ldrRow.IsNull(ldcCol.ColumnName) Then
                            If ldcCol.DataType.Name = "DateTime" Then
                                lstrText = CDate(ldrRow.Item(ldcCol.ColumnName)).ToString("dd/MM/yyyy HH:mm")
                            Else
                                lstrText = ldrRow.Item(ldcCol.ColumnName).ToString
                            End If
                        Else
                            lstrText = ""
                        End If

                        lstrText = lstrText.Replace(Chr(10) + Chr(13), "<br>")

                        lstrTemp = lstrTemp.Replace("[" + ldcCol.ColumnName + "]", lstrText)
                        lstrAsunto = lstrAsunto.Replace("[" + ldcCol.ColumnName + "]", lstrText)
                        If Not pbooAlertUsua Then
                            lstrPara = ldrRow.Item("usua_email")
                        Else
                            lstrPara = pstrMails
                        End If
                    Next
                    lstrMsgRet += lstrTemp

            If lstrPara = "" Then
                Return
            End If

            If pstrAlerTipo = SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_CONFIRMACION Then
                lstrPara = lstrParaMulti
            End If

            lstrMsgRet += lstrTempFoot

            lstrMsgRet = lstrMsgRet.Replace("[getdate]", Date.Now.ToString("dd/MM/yyyy HH:mm").Replace("lnksist", pstrLink))
            lstrMsgRet = lstrMsgRet.Replace("lnksist", pstrLink)
            lstrAsunto = lstrAsunto.Replace("[getdate]", Date.Now.ToString("dd/MM/yyyy HH:mm"))

            lstrMsgRet = lstrMsgRet.Replace("[_usua_actual]", pstrUsuarioActual)
            lstrAsunto = lstrAsunto.Replace("[_usua_actual]", pstrUsuarioActual)

            SRA_Neg.clsMail.gEnviarMail(lstrPara, lstrAsunto, lstrMsgRet, pstrAdjuPath)


                Next
            End With

            'If lstrPara = "" Then
            '    Return
            'End If

            'If pstrAlerTipo = SRA_Neg.Constantes.AlertasTipos.ACREDITACION_BANCARIA_CONFIRMACION Then
            '    lstrPara = lstrParaMulti
            'End If

            'lstrMsgRet += lstrTempFoot

            'lstrMsgRet = lstrMsgRet.Replace("[getdate]", Date.Now.ToString("dd/MM/yyyy HH:mm").Replace("lnksist", pstrLink))
            'lstrMsgRet = lstrMsgRet.Replace("lnksist", pstrLink)
            'lstrAsunto = lstrAsunto.Replace("[getdate]", Date.Now.ToString("dd/MM/yyyy HH:mm"))

            'lstrMsgRet = lstrMsgRet.Replace("[_usua_actual]", pstrUsuarioActual)
            'lstrAsunto = lstrAsunto.Replace("[_usua_actual]", pstrUsuarioActual)

            'SRA_Neg.clsMail.gEnviarMail(lstrPara, lstrAsunto, lstrMsgRet, pstrAdjuPath)

        End If

   End Sub

   Private Shared Sub mGuardarLog(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pintAlhoId As Integer, ByVal pstrError As String, ByVal pintAlerId As Integer)
      Dim ldrDatos As DataRow

      ldrDatos = clsSQLServer.gObtenerEstruc(pstrConn, Constantes.gTab_AlertasLog).Tables(0).Rows(0)

      With ldrDatos
         If pintAlhoId = 0 Then
            .Item("allo_alho_id") = DBNull.Value
         Else
            .Item("allo_alho_id") = pintAlhoId
         End If
         .Item("allo_aler_id") = pintAlerId
         .Item("allo_resu") = pstrError = ""
         If pstrError <> "" Then
            .Item("allo_error") = pstrError
         End If
      End With

      clsSQLServer.gAlta(pstrConn, pstrUserId, Constantes.gTab_AlertasLog, ldrDatos)
   End Sub

   Private Shared Sub mEscribir(ByVal pDatos As String)
      If System.Configuration.ConfigurationSettings.AppSettings("conGrabarLog").ToString = "1" Then
         Dim lstrArchLog As String = System.Configuration.ConfigurationSettings.AppSettings("conArchLog").ToString.Replace("errores", "aplicacion")
         Dim fs As New IO.FileStream(lstrArchLog, IO.FileMode.OpenOrCreate, IO.FileAccess.ReadWrite)

         Dim w = New IO.StreamWriter(fs)
         w.BaseStream.Seek(0, IO.SeekOrigin.End)

         w.Write(clsFormatear.CrLf() & "Fecha: " & DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss") & clsFormatear.CrLf())
         w.Write(pDatos & clsFormatear.CrLf())
         w.Write("------------------------------------------------------------------------" & clsFormatear.CrLf())

         w.Flush()
         w.Close()
      End If
   End Sub
End Class