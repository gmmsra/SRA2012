Imports System.Data.SqlClient

Public Class Firmantes
   Inherits Generica

   Protected filFoto As System.Web.UI.HtmlControls.HtmlInputFile
   Protected filFirma As System.Web.UI.HtmlControls.HtmlInputFile
   Protected server As System.Web.HttpServerUtility
   Protected WithEvents txtFoto As NixorControls.TextBoxTab
   Protected WithEvents txtFirma As NixorControls.TextBoxTab

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet, ByVal pfilFoto As System.Web.UI.HtmlControls.HtmlInputFile, ByVal pfilFirma As System.Web.UI.HtmlControls.HtmlInputFile, ByVal server As System.Web.HttpServerUtility, ByRef ptxtFoto As NixorControls.TextBoxTab, ByRef ptxtFirma As NixorControls.TextBoxTab)
      mstrConn = pstrConn
      mstrUserId = pstrUserId
      mstrTabla = pstrTabla
      mdsDatos = pdsDatos
      Me.filFoto = pfilFoto
      Me.filFirma = pfilFirma
      Me.server = server
      Me.txtFoto = ptxtFoto
      Me.txtFirma = ptxtFirma
   End Sub

   Public Overloads Function Alta() As String
      mActualizar()
   End Function

   Public Overloads Sub Modi()
      mActualizar()
   End Sub

   Private Sub mActualizar()

      Dim lstrTABLAId As String
      Dim lstrFirmId As String
      Dim lstrArchId As String
      Dim lTransac As SqlClient.SqlTransaction

      Try

         lTransac = clsSQLServer.gObtenerTransac(mstrConn)

         If CInt(MiRow.Item(0)) <= 0 Then
            'ALTA TABLA
            lstrFirmId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
         Else
            'MODIFICACION TABLA
            clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
            lstrFirmId = MiRow.Item(0).ToString
         End If

         'id del firmante + id del cliente firmante
            lstrArchId = lstrFirmId & "_" & MiRow.Item(9).ToString         'Datos del cliente.

         mGuardarArchivos(lstrArchId)
         clsSQLServer.gCommitTransac(lTransac)

      Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(ex)
         Throw (ex)
      End Try

   End Sub

   Private Sub mGuardarArchivos(ByVal lstrArchId As String)
      Dim lstrNombre As String
      Dim lstrCarpeta As String

      If Not (filFoto.PostedFile Is Nothing) AndAlso filFoto.PostedFile.FileName <> "" Then
         lstrNombre = filFoto.PostedFile.FileName
         lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)
         lstrNombre = lstrArchId & "_" & lstrNombre
         lstrCarpeta = clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_foto_path")
         filFoto.PostedFile.SaveAs(server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre)
         txtFoto.Text = lstrNombre
      End If

      If Not (filFirma.PostedFile Is Nothing) AndAlso filFirma.PostedFile.FileName <> "" Then
         lstrNombre = filFirma.PostedFile.FileName
         lstrNombre = lstrNombre.Substring(lstrNombre.LastIndexOf("\") + 1)
         lstrNombre = lstrArchId & "_" & lstrNombre
         lstrCarpeta = clsSQLServer.gParametroValorConsul((mstrConn), "para_clie_foto_path")
         filFirma.PostedFile.SaveAs(server.MapPath("") + "\" + lstrCarpeta + "\" + lstrNombre)
         txtFirma.Text = lstrNombre
      End If

   End Sub

End Class
