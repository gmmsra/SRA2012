Imports ReglasValida.Validaciones

Public Class Productos
   Inherits Generica

   Protected mstrTablaAna As String
    Protected server As System.Web.HttpServerUtility
    Protected mstTransac As SqlClient.SqlTransaction

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaAna As String, ByVal pdsDatos As DataSet)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaAna = pstrTablaAna
      Me.mdsDatos = pdsDatos
      Me.server = server
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub
    ' Dario 2014-06-12 - sobrecarga del constructor base que acepta transaccion
    Public Sub New(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrUserId As String)
        Me.mstTransac = lTransac
        Me.mstrUserId = pstrUserId
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pdsDatos As DataSet)

        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
        Me.mdsDatos = pdsDatos
    End Sub


    Public Overloads Function Alta() As String
        Return (mActualizar())
    End Function

    Public Overloads Sub Modi()
        mActualizar()
    End Sub

    Private Function mActualizar() As String
        Dim lstrProdId As String
        Dim lTransac As SqlClient.SqlTransaction

        Try

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'Datos del producto.
            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                lstrProdId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrProdId = MiRow.Item(0).ToString
            End If

            'Detalle de Analisis
            For Each odrDetaAna As DataRow In mdsDatos.Tables(mstrTablaAna).Select()
                If odrDetaAna.Item("prta_id") Is DBNull.Value OrElse odrDetaAna.Item("prta_id") <= 0 Then
                    odrDetaAna.Item("prta_prdt_id") = lstrProdId
                    'ALTA 
                    clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaAna, odrDetaAna)
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaAna, odrDetaAna)
                End If
            Next

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrProdId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Function GetProductoById(ByVal ProductoId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetProductoById "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetProductoDescripById(ByVal ProductoId As String) As String
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim strRetProd As String = ""

        dt.TableName = "ProdTemp"

        mstrCmd = "exec GetProductoById "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dt.Rows.Count > 0 Then
            If (dt.Rows(0).Item("prdt_nomb") Is System.DBNull.Value) Then
                Return strRetProd
            Else
                strRetProd = dt.Rows(0).Item("prdt_nomb")
            End If
        End If


        Return strRetProd

    End Function


    Public Function GetTienePadreByProdIdHijo(ByVal ProductoId As String) As Boolean
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = "Padres"

        mstrCmd = "exec GetTienePadreByProdIdHijo "
        mstrCmd = mstrCmd + "  @ProdIdHijo= " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If


    End Function

    Public Function GetTieneMadreByProdIdHijo(ByVal ProductoId As String) As Boolean
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = "Madre"

        mstrCmd = "exec GetTieneMadreByProdIdHijo "
        mstrCmd = mstrCmd + "  @ProdIdHijo= " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If


    End Function

    Public Function GetFechaUltTransferenciaById(ByVal ProductoId As String) As DateTime
        Dim dt As New DataTable
        Dim mstrCmd As String


        mstrCmd = "exec GetFechaUltTransferenciaById "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            If (dt.Rows(0).Item("prdp_audi_fecha") Is System.DBNull.Value) Then
                Return DateTime.MinValue
            Else
                Return dt.Rows(0).Item("prdp_audi_fecha")
            End If
        End If
    End Function

    Public Function GetFechaUltTransferenciaByIdYPropId(ByVal ProductoId As String, ByVal PropietarioId As String) As DateTime
        Dim dt As New DataTable
        Dim mstrCmd As String


        mstrCmd = "exec GetFechaUltTransferenciaByIdYPropId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ",@PropietarioId = " + clsSQLServer.gFormatArg(PropietarioId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            If (dt.Rows(0).Item("prdp_audi_fecha") Is System.DBNull.Value) Then
                Return DateTime.MinValue
            Else
                Return dt.Rows(0).Item("prdp_audi_fecha")
            End If
        End If
    End Function

    Public Function GetUltNroTramiteTransfByProdId(ByVal ProductoId As String, ByVal PropietarioId As String) As Integer
        Dim dt As New DataTable
        Dim mstrCmd As String


        mstrCmd = "exec GetUltNroTramiteTransfByProdId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ",@PropietarioId = " + clsSQLServer.gFormatArg(PropietarioId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            If (dt.Rows(0).Item("prdt_tram_nume") Is System.DBNull.Value) Then
                Return -1
            Else
                Return dt.Rows(0).Item("prdt_tram_nume")
            End If
        End If
    End Function

    Public Function GetSexoByProductoId(ByVal ProductoId As String) As Boolean
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim mbooResult As Boolean

        dt.TableName = "dtSexo"

        mstrCmd = "exec GetProductoById "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then
            mbooResult = ValidarNulos(dt.Rows(0).Item("prdt_sexo"), True)

        End If

        Return mbooResult
    End Function

    Public Function GetProductoDescripByProductoId(ByVal ProductoId As String) As String
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim mstrResult As String
        mstrResult = ""
        dt.TableName = "dtProducto"

        mstrCmd = "exec GetProductoById "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then
            mstrResult = Convert.ToString(ValidarNulos(dt.Rows(0).Item("prdt_nomb"), False))

        End If

        Return mstrResult

    End Function

    Public Function GetProductosByClienteId(ByVal ClienteId As Integer, _
        ByVal RazaId As Integer, ByVal pTableName As String) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String


        mstrCmd = "exec GetProductosByClienteId "
        mstrCmd = mstrCmd + " @ClienteId = " + clsSQLServer.gFormatArg(ClienteId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ",@RazaId = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar)


        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        dt.TableName = pTableName
        Return dt

    End Function
    Public Function GetProductosByCriadorId(ByVal CriadorId As Integer, _
       ByVal RazaId As Integer, ByVal pTableName As String) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String


        mstrCmd = "exec GetProductosByCriadorId "
        mstrCmd = mstrCmd + " @CriadorId = " + clsSQLServer.gFormatArg(CriadorId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ",@RazaId = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar)


        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        dt.TableName = pTableName
        Return dt

    End Function

    Public Function GetProductoByHBARazaSexo(ByVal pHBA As Integer, ByVal pRazaId As Integer, _
            ByVal pSexo As String, ByVal pTableName As String) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetProductoByHBARazaSexo "
        mstrCmd = mstrCmd + " @HBA = " + clsSQLServer.gFormatArg(pHBA, SqlDbType.VarChar)
        mstrCmd = mstrCmd + " ,@RazaId = " + clsSQLServer.gFormatArg(pRazaId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + " ,@Sexo = " + clsSQLServer.gFormatArg(pSexo, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetProductoByHBARazaSexoRP(ByVal pHBA As String, ByVal pRazaId As String, _
            ByVal pSexo As String, ByVal pRP As String, ByVal pTableName As String) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        ' GSZ 12/12/2014 Se agrego busqueda Hba Raza Criador RP

        mstrCmd = "exec GetProductoByHBARazaSexoRP "
        mstrCmd = mstrCmd + " @HBA = " + clsSQLServer.gFormatArg(pHBA, SqlDbType.VarChar)
        mstrCmd = mstrCmd + " ,@RazaId = " + clsSQLServer.gFormatArg(pRazaId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + " ,@Sexo = " + clsSQLServer.gFormatArg(pSexo, SqlDbType.VarChar)
        mstrCmd = mstrCmd + " ,@RP = " + clsSQLServer.gFormatArg(pRP, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetProductoPadreEnSemenByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName



        mstrCmd = "exec GetProductoPadreEnSemenByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetProductoPadreEnEmbrionByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetProductoPadreEnEmbrionByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetProductoMadreEnEmbrionByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName



        mstrCmd = "exec GetProductoMadreEnEmbrionByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetPadreByProductoId(ByVal ProductoId As String) As Long

        ' producto
        Dim mstrCmd As String
        Dim dt As New DataTable
        dt.TableName = "tblProductoPadre"
        Dim lngResult As Long = 0



        mstrCmd = "exec GetPadreByProductoId "
        mstrCmd = mstrCmd + " @prdt_id = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then
            lngResult = Convert.ToString(ValidarNulos(dt.Rows(0).Item("ptre_supe_prdt_id"), False))

        End If

        Return lngResult

    End Function

    Public Function GetMadreByProductoId(ByVal ProductoId As String) As Long

        ' producto
        Dim mstrCmd As String
        Dim dt As New DataTable
        dt.TableName = "tblProductoMadre"
        Dim lngResult As Long = 0



        mstrCmd = "exec GetMadreByProductoId "
        mstrCmd = mstrCmd + " @prdt_id = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then
            lngResult = Convert.ToString(ValidarNulos(dt.Rows(0).Item("ptre_supe_prdt_id"), False))

        End If

        Return lngResult

    End Function

    Public Function TraerPropietarioClie(ByVal prod_id As Integer) As Integer
        Dim lstrCmd As String = ""
        Dim lbooResult As Boolean = False
        Dim lstrId As Integer
        Try
            Dim dtProducto As DataTable

            lstrId = 0

            dtProducto = GetProductoById(prod_id, "")
            If dtProducto.Rows.Count > 0 Then
                lstrId = Convert.ToInt32(ValidarNulos(dtProducto.Rows(0).Item("prdt_prop_clie_id"), False))
            End If


            Return lstrId
        Catch ex As Exception
            clsError.gManejarError(ex)
            Return (0)
        End Try

    End Function

    Public Function TraerPropietarioCria(ByVal prod_id As Integer) As Integer
        Dim lstrCmd As String = ""
        Dim lbooResult As Boolean = False
        Dim lstrId As Integer
        Try


            Dim dtProducto As DataTable
            Dim oProducto As New SRA_Neg.Productos(mstrConn, mstrUserId)

            lstrId = 0

            dtProducto = oProducto.GetProductoById(prod_id, "")
            If dtProducto.Rows.Count > 0 Then
                lstrId = Convert.ToInt32(ValidarNulos(dtProducto.Rows(0).Item("prdt_prop_cria_id"), False))
            End If

            Return lstrId
        Catch ex As Exception
            clsError.gManejarError(ex)
            Return (0)
        End Try

    End Function
    Public Function ValidarExportacionByProductoNombre(ByVal ProductoNombre As String) As Boolean
        Dim dt As New DataTable
        Dim mstrCmd As String


        'mstrCmd = "exec ValidarExportacionByProductoNombre "
        'mstrCmd = mstrCmd + " @prdt_nomb= " + clsSQLServer.gFormatArg(ProductoNombre, SqlDbType.VarChar)

        'dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        'If dt.Rows.Count > 0 Then


        '    If dt.Rows.Count > 0 Then

        '        lstrId = ValidarNulos(dt.Rows(0).Item("prdt_ndad"), False)
        '    End If


        '    'ya se exporto anteriormente
        '    Return False
        'Else
        '    'no se exporto anteriormente
        '    Return True
        'End If

        Return True
    End Function


    Public Function ExisteProductoByRazaSexoNombre( _
        ByVal ProductoNombre As String _
        , ByVal Sexo As Boolean _
        , ByVal RazaId As Integer _
        ) As Boolean


        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim boolExiste As Boolean = False


        mstrCmd = "exec GetProductoByRazaSexoNombre "
        mstrCmd = mstrCmd + " @prdt_raza_id= " + clsSQLServer.gFormatArg(RazaId, SqlDbType.Int)
        mstrCmd = mstrCmd + ", @prdt_Sexo= " + clsSQLServer.gFormatArg(IIf(Sexo, "1", "0"), SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @prdt_nomb= " + clsSQLServer.gFormatArg(ProductoNombre, SqlDbType.VarChar)



        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then


            If dt.Rows.Count > 0 Then
                For Each odrDeta As DataRow In dt.Rows
                    If ValidarNulos(dt.Rows(0).Item("prdt_ndad"), False) = "I" Then
                        boolExiste = True
                    End If
                Next

            End If

            Return boolExiste
        Else
            Return boolExiste
        End If


    End Function

    Public Function ValidarExportacionByProductoId(ByVal ProductoId As Int32) As Boolean
        Dim dt As New DataTable
        Dim mstrCmd As String


        mstrCmd = "exec ValidarExportacionByProductoId "
        mstrCmd = mstrCmd + " @prdt_id= " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then
            'ya se exporto anteriormente
            Return False
        Else
            'no se exporto anteriormente
            Return True
        End If


    End Function


    Public Function ValidarTransferenciaByProductoId(ByVal ProductoId As Int32) As Boolean
        Dim dt As New DataTable
        Dim mstrCmd As String


        mstrCmd = "exec ValidarTransferenciaByProductoId "
        mstrCmd = mstrCmd + " @prdt_id= " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then
            'ya se exporto anteriormente
            Return False
        Else
            'no se exporto anteriormente
            Return True
        End If


    End Function
    Public Function ValidarImportacionByRazaSexoRP(ByVal RazaId As String, ByVal Sexo As String, _
    ByVal RP As String) As Boolean

        Dim dt As New DataTable
        Dim mstrCmd As String

        'todo : el rp puede venir nulo


        mstrCmd = "exec ValidarImportacionByRazaSexoRP "
        mstrCmd = mstrCmd + " @RazaId= " + clsSQLServer.gFormatArg(RazaId, SqlDbType.Int)
        mstrCmd = mstrCmd + ", @Sexo= " + clsSQLServer.gFormatArg(Sexo, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @RP= " + clsSQLServer.gFormatArg(RP, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)
        If dt.Rows.Count > 0 Then
            'ya se importo anteriormente
            Return False
        Else
            'no se importo anteriormente
            Return True
        End If
        'no se importo anteriormente
        ' Return True

    End Function



    Public Function existTramiteProducto(ByVal ProductoId As Int32, ByVal TramiteId As Int32) As Boolean
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim lstrNuevo As String



        mstrCmd = "producto_tramite_determinar @prdt_id=" & ProductoId & _
                 ",@tram_id=" & TramiteId



        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            lstrNuevo = dt.Rows(0).Item("resul")
        End If

        If lstrNuevo = "S" Then
            Return (True)
        Else
            Return (False)

        End If


    End Function


    Public Sub GrabarEstadoRetenidaEnProductosInscrip(ByVal ProductoInscripId)
        If Not mdsDatos Is Nothing Then
            For Each odrRetenido As DataRow In mdsDatos.Tables(Constantes.gTab_ProductosInscrip).Select("pdin_id=" + ProductoInscripId)
                odrRetenido.Item("pdin_esta_id") = SRA_Neg.Constantes.Estados.Productos_Retenido
                clsSQLServer.gModi(mstrConn, mstrUserId, Constantes.gTab_ProductosInscrip, odrRetenido)
            Next
        End If




    End Sub


    Public Sub GrabarEstadoEnProductos(ByVal ProductoId As String, ByVal EstadoId As Int16)

        If Not mdsDatos Is Nothing Then
            For Each odrEstado As DataRow _
            In mdsDatos.Tables(Constantes.gTab_Productos).Select("prdt_id=" + ProductoId)
                odrEstado.Item("prdt_esta_id") = EstadoId
                clsSQLServer.gModi(mstrConn, mstrUserId, Constantes.gTab_Productos, odrEstado)
            Next
        End If

    End Sub

    Public Sub GrabarEstadoExportadoEnProductos(ByVal ProductoId As String, ByVal pdsDatos As DataSet)

        If Not pdsDatos Is Nothing Then
            For Each odrEstado As DataRow _
            In pdsDatos.Tables(Constantes.gTab_Productos).Select("prdt_id=" + ProductoId)
                odrEstado.Item("prdt_esta_id") = SRA_Neg.Constantes.Estados.Productos_Exportado
                odrEstado.Item("prdt_baja_fecha") = Today

                pdsDatos.AcceptChanges()
                clsSQLServer.gModi(mstrConn, mstrUserId, Constantes.gTab_Productos, odrEstado)
            Next
        End If

        If Not pdsDatos Is Nothing Then
            For Each odrTramProd As DataRow _
            In pdsDatos.Tables(Constantes.gTab_Tramites_Productos).Select("trpr_prdt_id=" + ProductoId)
                odrTramProd.Item("trpr_apro_fecha") = Today
                odrTramProd.Item("trpr_baja_fecha") = Today

                pdsDatos.AcceptChanges()

                clsSQLServer.gModi(mstrConn, mstrUserId, Constantes.gTab_Tramites_Productos, odrTramProd)
            Next
        End If


    End Sub


    Public Function GetObsProdInscByRazaCriaRPSexo(ByVal pRazaId As String, ByVal pCriadorId As String, ByVal pSexo As String, ByVal pRP As String) As String
        ' GSZ 29/04/2015 se agrego metodo
        Dim dt As New DataTable
        Dim mstrCmd As String
        Dim strObservacion As String = ""
        dt.TableName = "tblProdInscrip"

        mstrCmd = "exec GetObsProdInscByRazaCriaRPSexo "
        mstrCmd = mstrCmd + " @RazaId = " + clsSQLServer.gFormatArg(pRazaId, SqlDbType.Int)
        mstrCmd = mstrCmd + " ,@CriadorId = " + clsSQLServer.gFormatArg(pCriadorId, SqlDbType.Int)
        mstrCmd = mstrCmd + " ,@Sexo=" + clsSQLServer.gFormatArg(pSexo, SqlDbType.Int)
        mstrCmd = mstrCmd + " ,@RP = " + clsSQLServer.gFormatArg(pRP, SqlDbType.VarChar)


        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            strObservacion = dt.Rows(0).Item("pdin_obser")
        End If
        Return strObservacion

    End Function





End Class
