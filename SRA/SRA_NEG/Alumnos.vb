Imports System.Data.SqlClient

Public Class Alumnos
   Inherits Generica

   Protected mstrClieEstu As String = SRA_Neg.Constantes.gTab_ClientesEstudios
   Protected mstrClieOcup As String = SRA_Neg.Constantes.gTab_ClientesOcupaciones
   Protected mstrClieFunc As String = SRA_Neg.Constantes.gTab_ClientesFunciones
   Protected mstrClieExpl As String = SRA_Neg.Constantes.gTab_ClientesExplota


   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mdsDatos = pdsDatos
   End Sub

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
   End Sub

   Public Overloads Function Alta() As String
      Return (mActualizar())
   End Function

   Public Overloads Sub Modi()
      mActualizar()
   End Sub

   Private Function mActualizar() As String
      Dim lstrAlumId As String
      Dim lintClieId As Integer
      Dim lTransac As SqlClient.SqlTransaction

      Try
         lTransac = clsSQLServer.gObtenerTransac(mstrConn)

         If CInt(MiRow.Item(0)) <= 0 Then
            'ALTA ALUMNOS
            lstrAlumId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
         Else
            'MODIFICACION ALUMNOS
            clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
            lstrAlumId = MiRow.Item(0).ToString

            'Eliminar los detalles anteriores.
            BorrarDetas(lTransac, mstrClieEstu)
            BorrarDetas(lTransac, mstrClieOcup)
            BorrarDetas(lTransac, mstrClieFunc)
            BorrarDetas(lTransac, mstrClieExpl)

         End If

         lintClieId = MiRow.Item("alum_clie_id")

         ActualizarDetas(lTransac, mstrClieEstu, "escl_clie_id", lintClieId)
         ActualizarDetas(lTransac, mstrClieOcup, "occl_clie_id", lintClieId)
         ActualizarDetas(lTransac, mstrClieFunc, "fucl_clie_id", lintClieId)
         ActualizarDetas(lTransac, mstrClieExpl, "excl_clie_id", lintClieId)


         clsSQLServer.gCommitTransac(lTransac)

         Return (lstrAlumId)

      Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(ex)
         Throw (ex)
      End Try
   End Function

End Class