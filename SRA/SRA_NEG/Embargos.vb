Imports ReglasValida.Validaciones


Public Class Embargos

    Inherits Generica


    Protected server As System.Web.HttpServerUtility


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, _
    ByVal pdsDatos As DataSet)


        mstrConn = pstrConn
        mstrUserId = pstrUserId
        mstrTabla = pstrTabla
        mdsDatos = pdsDatos


    End Sub

    Public Function GetEmbargosDetallesById(ByVal EmbargoId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName

        Dim mstrCmd As String

        mstrCmd = "exec GetEmbargosDetallesById "
        mstrCmd = mstrCmd + " @EmbargoId = " + clsSQLServer.gFormatArg(EmbargoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetEmbargoDetalleByProdId(ByVal EmbargoId As String, _
        ByVal ProductoId As String, _
        ByVal pTableName As String) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String

        dt.TableName = pTableName



        mstrCmd = "exec GetEmbargoDetalleByProdId "
        mstrCmd = mstrCmd + " @EmbargoId = " + clsSQLServer.gFormatArg(EmbargoId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function
    Public Function GetEstaEmbargoByProdId(ByVal ProductoId As String) As Boolean


        Dim mstrCmd As String
        Dim strEmbargado As Boolean
        Dim boolEmbargado As Boolean
        Dim dt As DataTable

        mstrCmd = "exec sp_ProductoEmbargadoByProdId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.Int)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            strEmbargado = dt.Rows(0).Item("IsEmbargado")
        End If

        If Not strEmbargado Then
            boolEmbargado = False
        Else
            boolEmbargado = True
        End If


        Return boolEmbargado

    End Function


    Public Function GetFechaEmbargoIniByProdId(ByVal ProductoId As String) As String

        Dim strFechaInicialEmbargo As String = ""
        Dim mstrCmd As String
        Dim dt As DataTable


        mstrCmd = "exec GetFechaEmbargoIniByProdId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            strFechaInicialEmbargo = dt.Rows(0).Item("emba_inic_fecha")
        End If

        Return strFechaInicialEmbargo

    End Function

    Public Function GetEmbargoErrorByProdId(ByVal ProductoId As String) As Boolean


        Dim mstrCmd As String
        Dim boolTieneError As Boolean = False
        Dim dt As DataTable

        mstrCmd = "exec GetEmbargoErrorByProdId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.Int)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            boolTieneError = True
        End If


        Return boolTieneError

    End Function
    Public Function GetEmbargoErrorByNroEmbargo(ByVal pNumeroEmbargo As String) As Boolean


        Dim mstrCmd As String
        Dim boolTieneError As Boolean = False
        Dim dt As DataTable

        mstrCmd = "exec GetEmbargoErrorByNroEmbargo "
        mstrCmd = mstrCmd + " @NroEmbargo = " + clsSQLServer.gFormatArg(pNumeroEmbargo, SqlDbType.Int)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            boolTieneError = True
        End If


        Return boolTieneError

    End Function


    Public Function mActualEmbargos(ByVal pTipo) As String

        Dim lstrEmbargoId, lstrId As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String
        Dim dtEmbargo As DataTable


        Try
            Dim oEmbargo As New SRA_Neg.Embargos(mstrConn, mstrUserId)

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'TABLA embargos Y SUS DETALLES
            If CInt(MiRow.Item(0)) <= 0 Then
                If pTipo = "A" Then
                    lstrEmbargoId = clsSQLServer.gAlta(lTransac, mstrUserId, "embargos", MiRow)
                    MiRow(0) = lstrEmbargoId


                    For Each lDr As DataRow In mdsDatos.Tables(mdsDatos.Tables("embargos_deta").TableName).Select()
                        If lDr.Item("emde_emba_id") < 0 Then
                            lDr.Item("emde_emba_id") = lstrEmbargoId

                        End If
                    Next

                    For Each lDr As DataRow In mdsDatos.Tables("Embargos_deta").Select("_estado='Embargo'")
                        clsSQLServer.gAlta(lTransac, mstrUserId, "Embargos_deta", ldr)
                    Next

                End If
            Else
                If pTipo = "M" Then
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, "embargos", MiRow)
                    lstrEmbargoId = MiRow.Item(0).ToString


                    'For Each lDr As DataRow In mdsDatos.Tables("Embargos_deta").Select("_estado='Embargo'")
                    '    clsSQLServer.gModi(lTransac, mstrUserId, "Embargos_deta", ldr)
                    'Next


                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                    'DETALLE
                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                        lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                        lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrEmbargoId)

                    Next
                End If

            End If



            clsSQLServer.gCommitTransac(lTransac)
            mdsDatos.AcceptChanges()
            Return (lstrEmbargoId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function


End Class