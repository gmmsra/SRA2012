Imports ReglasValida.Validaciones


Public Class MotivoBaja

    Inherits Generica


    Protected server As System.Web.HttpServerUtility


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub

    Public Function GetMotivoBajaById(ByVal MotivoBajaId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName

        Dim mstrCmd As String

        mstrCmd = "exec GetMotivoBajaById "
        mstrCmd = mstrCmd + " @MotivoBajaId = " + clsSQLServer.gFormatArg(MotivoBajaId, SqlDbType.Int)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetMotivoBajaDescripById(ByVal MotivoBajaId As Integer) As String



        Dim dtMotivoBaja As DataTable
        Dim sReturn As String = ""


        dtMotivoBaja = Me.GetMotivoBajaById(MotivoBajaId.ToString(), "")
        If dtMotivoBaja.Rows.Count > 0 Then
            sReturn = dtMotivoBaja.Rows(0).Item("bamo_desc").ToString()
        End If

        Return sReturn
    End Function


End Class