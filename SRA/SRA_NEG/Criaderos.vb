Imports System.Data.SqlClient

Public Class Criaderos
   Inherits Generica

   Protected mstrTablaTele As String
   Protected mstrTablaDire As String
   Protected mstrTablaMail As String
   Protected mstrTablaRaza As String
   Protected server As System.Web.HttpServerUtility

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaTele As String, ByVal pstrTablaDire As String, ByVal pstrTablaMail As String, ByVal pstrTablaRaza As String, ByVal pdsDatos As DataSet, ByVal server As System.Web.HttpServerUtility)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaTele = pstrTablaTele
      Me.mstrTablaDire = pstrTablaDire
      Me.mstrTablaMail = pstrTablaMail
      Me.mstrTablaRaza = pstrTablaRaza
      Me.mdsDatos = pdsDatos
      Me.server = server
   End Sub

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaTele As String, ByVal pstrTablaDire As String, ByVal pstrTablaMail As String, ByVal pstrTablaRaza As String)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mstrTablaTele = pstrTablaTele
      Me.mstrTablaDire = pstrTablaDire
      Me.mstrTablaMail = pstrTablaMail
      Me.mstrTablaRaza = pstrTablaRaza
   End Sub

   Public Overloads Function Alta() As String
      Return (mActualizar())
   End Function

   Public Overloads Sub Modi()
      mActualizar()
   End Sub

   Private Function mActualizar() As String
      Dim lstrTABLAId As String
      Dim lstrCriaId As String
      Dim lstrClieId As String
      Dim lTransac As SqlClient.SqlTransaction

      Try

         lTransac = clsSQLServer.gObtenerTransac(mstrConn)

         'Datos del criadero.
         If CInt(MiRow.Item("crdr_id")) <= 0 Then
            'ALTA TABLA
            lstrCriaId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
         Else
            'MODIFICACION TABLA
            clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
            lstrCriaId = MiRow.Item("crdr_id").ToString
            'Eliminar los detalles anteriores.
            mBorrarDetas(lTransac, mstrTablaTele, lstrCriaId, "T")
            'mBorrarDetas(lTransac, mstrTablaDire, lstrCriaId, "D")
            mBorrarDetas(lTransac, mstrTablaMail, lstrCriaId, "M")
         End If
         lstrClieId = MiRow.Item("crdr_clie_id").ToString

         'Tel�fonos del cliente para el criadero.
         For Each odrDetaTele As DataRow In mdsDatos.Tables(mstrTablaTele).Select()
            If odrDetaTele.Item("acti_crdr_id") Is DBNull.Value Then
               odrDetaTele.Item("tecl_clie_id") = lstrClieId
               odrDetaTele.Item("acti_crdr_id") = lstrCriaId
               'ALTA 
               clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaTele, odrDetaTele)
            Else
               'MODIFICACION 
               clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaTele, odrDetaTele)
            End If
         Next

         'Direcciones del cliente para el criadero.
         For Each odrDetaDire As DataRow In mdsDatos.Tables(mstrTablaDire).Select()
            If odrDetaDire.Item("acti_crdr_id") Is DBNull.Value Then
               odrDetaDire.Item("dicl_clie_id") = lstrClieId
               odrDetaDire.Item("acti_crdr_id") = lstrCriaId
               'ALTA 
               clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaDire, odrDetaDire)
            Else
               'MODIFICACION 
               clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaDire, odrDetaDire)
            End If
         Next

         'Mails del cliente para el criadero.
         For Each odrDetaMail As DataRow In mdsDatos.Tables(mstrTablaMail).Select()
            If odrDetaMail.Item("acti_crdr_id") Is DBNull.Value Then
               odrDetaMail.Item("macl_clie_id") = lstrClieId
               odrDetaMail.Item("acti_crdr_id") = lstrCriaId
               'ALTA 
               clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaMail, odrDetaMail)
            Else
               'MODIFICACION 
               clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaMail, odrDetaMail)
            End If
         Next

         'Razas del criadero.
         For Each odrDetaRaza As DataRow In mdsDatos.Tables(mstrTablaRaza).Select()
            If odrDetaRaza.Item("crrz_crdr_id") Is DBNull.Value Then
               'ALTA 
               odrDetaRaza.Item("crrz_crdr_id") = lstrCriaId
               clsSQLServer.gAlta(lTransac, mstrUserId, mstrTablaRaza, odrDetaRaza)
            Else
               'MODIFICACION 
               clsSQLServer.gModi(lTransac, mstrUserId, mstrTablaRaza, odrDetaRaza)
            End If
         Next

         clsSQLServer.gCommitTransac(lTransac)

         Return (lstrCriaId)

      Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(ex)
         Throw (ex)
      End Try
   End Function

   Private Sub mBorrarDetas(ByVal lTransac As SqlClient.SqlTransaction, ByVal mstrTablaDet As String, ByVal pstrCriaId As String, ByVal pstrTipo As String)
      Dim lstrCmd As String
      Select Case pstrTipo
         Case "T"
            lstrCmd = "tele_criaderos_baja " & pstrCriaId
         Case "M"
            lstrCmd = "mails_criaderos_baja " & pstrCriaId
      End Select
      clsSQLServer.gExecute(lTransac, lstrCmd)
   End Sub
End Class