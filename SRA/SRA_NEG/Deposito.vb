Public Class Deposito
   Inherits GenericaRel

   Private mstrCmd As String
   Private mstrTablaDepoCheq As String
   Private mstrChequesViejos As String

   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaDepoCheq As String, ByVal pdsDatos As DataSet, ByVal pstrChequesViejos As String)
      mstrConn = pstrConn
      mstrUserId = pstrUserId
      mstrTabla = pstrTabla
      mstrTablaDepoCheq = pstrTablaDepoCheq
      mdsDatos = pdsDatos
      mstrChequesViejos = pstrChequesViejos
   End Sub

   Public Overloads Function Alta() As String
      Return (mActualizar())
   End Function

   Public Overloads Function Modif() As String
      Return (mActualizar())
   End Function

   Private Overloads Function mActualizar() As String
      Dim lstrDepoId As String
      Dim lstrDepoIds As String = ""
      Dim lintCont As Integer = 0
      Dim ldecImpo As Decimal = 0
      Dim lTransac As SqlClient.SqlTransaction

      Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'DEPOSITO EN EFECTIVO
            With mdsDatos.Tables(mstrTabla).Select()(0)
                'recuperar monto anterior para generar el asiento por la diferencia 
                If .Item("depo_id").ToString > "0" Then
                    ldecImpo = clsSQLServer.gCampoValorConsul(mstrConn, "depositos_consul @DepoId=" & mdsDatos.Tables(mstrTabla).Select()(0).Item("depo_id"), "depo_monto")
                Else
                    ldecImpo = 0
                End If

                If .Item("depo_id").ToString > "0" And .Item("depo_monto").ToString <> "" Then
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, mdsDatos.Tables(mstrTabla).Select()(0))
                    If .Item("depo_monto").ToString <> "" Then
                        'Agrego el asiento
                        mstrCmd = "exec " + mstrTabla + "_efectivo_asientos_modi"
                        mstrCmd = mstrCmd + " @depo_id = " + clsSQLServer.gFormatArg(.Item("depo_id"), SqlDbType.Int)
                        mstrCmd = mstrCmd + ",@importe = " + ldecImpo.ToString()
                        mstrCmd = mstrCmd + ",@asie_audi_user = " + clsSQLServer.gFormatArg(.Item("depo_audi_user"), SqlDbType.Int)
                        clsSQLServer.gExecute(lTransac, mstrCmd)
                    End If
                Else
                    If .Item("depo_monto").ToString <> "" Then
                        lstrDepoId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, mdsDatos.Tables(mstrTabla).Select()(0))
                        'Agrego los asientos
                        mstrCmd = "exec " + mstrTabla + "_asientos_alta"
                        mstrCmd = mstrCmd + " @depo_id = " + clsSQLServer.gFormatArg(lstrDepoId, SqlDbType.Int)
                        mstrCmd = mstrCmd + ",@asie_audi_user = " + clsSQLServer.gFormatArg(.Item("depo_audi_user"), SqlDbType.Int)
                        clsSQLServer.gExecute(lTransac, mstrCmd)
                        .Item("depo_id") = lstrDepoId
                    End If
                End If
            End With

            'DEPOSITOS EN CHEQUES
            Dim lintCheqBoleta As Integer = IIf(clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", mdsDatos.Tables(mstrTabla).Rows(0).Item("depo_cuba_id"), "cuba_cheq_bole").ToString = "", 1, clsSQLServer.gObtenerValorCampo(mstrConn, "cuentas_bancos", mdsDatos.Tables(mstrTabla).Rows(0).Item("depo_cuba_id"), "cuba_cheq_bole"))
            Dim lstrBancoAnt As String = ""
            Dim lstrClearAnt As String = ""
            Dim lstrTipoAnt As String = ""
            Dim lstrBancoGiroPostal As String = clsSQLServer.gParametroValorConsul(mstrConn, "para_giro_banc_id")

            Dim ldr As DataRow
            'SE HACEN LOS CORTES NECESARIOS

            mdsDatos.Tables(mstrTablaDepoCheq).DefaultView.Sort = "_cheq_chti_id, _banc_id, _clea_id"

            If mdsDatos.Tables(mstrTablaDepoCheq).Select("dech_depo_id is not null").GetUpperBound(0) > -1 Then
                With mdsDatos.Tables(mstrTablaDepoCheq).Select("dech_depo_id is not null")(0)
                    lstrTipoAnt = .Item("_cheq_chti_id").ToString
                    lstrBancoAnt = .Item("_banc_id").ToString
                    lstrClearAnt = .Item("_clea_id").ToString

                    lstrDepoId = mdsDatos.Tables(mstrTabla).Select()(0).Item("depo_id")
                End With
            End If

            For Each odrCheq As DataRow In mdsDatos.Tables(mstrTablaDepoCheq).Select()
                With odrCheq
                    'CONDICIONES DE CORTES DE BOLETAS       'lintCont = 0 Or _
                    If (lintCont = 0 And .IsNull("dech_depo_id")) _
                         Or lintCont = lintCheqBoleta _
                         Or lstrTipoAnt <> .Item("_cheq_chti_id").ToString _
                         Or ( _
                                (lstrBancoAnt <> .Item("_banc_id").ToString And lstrBancoGiroPostal = .Item("_banc_id").ToString) _
                                 Or lstrClearAnt <> .Item("_clea_id").ToString _
                             ) Then 'And .Item("_cheq_chti_id").ToString <> "2" Then
                        '
                        'Mdoificacion Roxi: 23/05/2008 
                        'Antes los cheques diferidos iban a la misma boleta siempre
                        'ahora se separan segun banco y clearing como el resto.
                        'If (lintCont = 0 And .IsNull("dech_depo_id")) Or _
                        '   lintCont = lintCheqBoleta Or _
                        '   lstrTipoAnt <> .Item("_cheq_chti_id").ToString Or _
                        '   (((lstrBancoAnt <> .Item("_banc_id").ToString And lstrBancoGiroPostal = .Item("_banc_id").ToString) Or _
                        '   lstrClearAnt <> .Item("_clea_id").ToString) And .Item("_cheq_chti_id").ToString <> "2") Then


                        lstrTipoAnt = .Item("_cheq_chti_id").ToString
                        lstrBancoAnt = .Item("_banc_id").ToString
                        lstrClearAnt = .Item("_clea_id").ToString

                        If mdsDatos.Tables(mstrTabla).Select()(0).Item("depo_id").ToString <= "0" Then
                            lstrDepoId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, mdsDatos.Tables(mstrTabla).Select("depo_id <= 0")(0))
                        Else
                            ldr = mdsDatos.Tables(mstrTabla).NewRow
                            ldr.Item("depo_id") = -1
                            ldr.Item("depo_fecha") = mdsDatos.Tables(mstrTabla).Select()(0).Item("depo_fecha")
                            ldr.Item("depo_cuba_id") = mdsDatos.Tables(mstrTabla).Select()(0).Item("depo_cuba_id")
                            ldr.Item("depo_mone_id") = mdsDatos.Tables(mstrTabla).Select()(0).Item("depo_mone_id")
                            ldr.Item("depo_emct_id") = mdsDatos.Tables(mstrTabla).Select()(0).Item("depo_emct_id")
                            ldr.Item("depo_audi_user") = mdsDatos.Tables(mstrTabla).Select()(0).Item("depo_audi_user")
                            ldr.Item("_nuevo") = True
                            mdsDatos.Tables(mstrTabla).Rows.Add(ldr.ItemArray)
                            lstrDepoId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, mdsDatos.Tables(mstrTabla).Select("depo_id <= 0")(0))
                        End If

                        ldr = mdsDatos.Tables(mstrTabla).Select("depo_id <= 0")(0)
                        ldr.Item("depo_id") = lstrDepoId

                        lintCont = 0
                    End If
                    .Item("dech_depo_id") = lstrDepoId
                    lintCont += 1
                End With
            Next

            'DETALLE DE DEPOSITOS NUEVOS
            For Each ldrCab As DataRow In mdsDatos.Tables(mstrTabla).Select("depo_monto is null")
                ActualizarDetas(lTransac, mdsDatos.Tables(mstrTablaDepoCheq).TableName, "dech_depo_id", ldrCab.Item("depo_id"), "dech_depo_id = " & ldrCab.Item("depo_id"), mdsDatos)
            Next

            'DETALLE DE DEPOSITOS EXISTENTES
            For Each ldrCab As DataRow In mdsDatos.Tables(mstrTabla).Select("depo_monto is null")
                If ldrCab.Item("_nuevo").tostring = "0" Then
                    mstrCmd = "exec " + mstrTabla + "_cheques_asientos_modi"
                    mstrCmd = mstrCmd + " @depo_id = " + clsSQLServer.gFormatArg(ldrCab.Item("depo_id"), SqlDbType.Int)
                    mstrCmd = mstrCmd + ",@ChequesNuevos = " + clsSQLServer.gFormatArg(mIdCheques(ldrCab.Item("depo_id").tostring), SqlDbType.VarChar)
                    mstrCmd = mstrCmd + ",@ChequesViejos = " + clsSQLServer.gFormatArg(mstrChequesViejos, SqlDbType.VarChar)
                    mstrCmd = mstrCmd + ",@asie_audi_user = " + clsSQLServer.gFormatArg(ldrCab.Item("depo_audi_user"), SqlDbType.Int)
                    clsSQLServer.gExecute(lTransac, mstrCmd)
                Else
                    'AGREGO LOS ASIENTOS DE LOS DEPOSITOS
                    mstrCmd = "exec " + mstrTabla + "_asientos_alta"
                    mstrCmd = mstrCmd + " @depo_id = " + clsSQLServer.gFormatArg(ldrCab.Item("depo_id"), SqlDbType.Int)
                    mstrCmd = mstrCmd + ",@asie_audi_user = " + clsSQLServer.gFormatArg(ldrCab.Item("depo_audi_user"), SqlDbType.Int)
                    clsSQLServer.gExecute(lTransac, mstrCmd)
                End If
            Next

            clsSQLServer.gCommitTransac(lTransac)
            For Each oDataitem As DataRow In mdsDatos.Tables(mstrTabla).Select
                If lstrDepoIds.Length > 0 Then lstrDepoIds += ","
                lstrDepoIds += oDataitem.Item("depo_id").ToString
            Next

            Return lstrDepoIds

        Catch ex As Exception
         clsSQLServer.gRollbackTransac(lTransac)
         clsError.gManejarError(ex)
         Throw (ex)
      End Try
   End Function

   Private Function mIdCheques(ByVal pstrDepo As String)
      Dim lstrIds As String = ""
      For Each oDataItem As DataRow In mdsDatos.Tables(mstrTablaDepoCheq).Select("dech_depo_id = " & pstrDepo)
         If lstrIds.Length > 0 Then lstrIds += ","
         lstrIds += oDataItem.Item("_cheq_id").ToString
      Next
      Return lstrIds
   End Function

End Class
