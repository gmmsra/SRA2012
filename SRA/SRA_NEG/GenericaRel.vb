Imports System.Data.SqlClient
Imports System.Web
Imports System.IO
Imports System.XML
Imports System.Web.UI.WebControls
Imports System.Collections
Imports ReglasValida.Validaciones
Imports WSAFIPFacElect


Public Class GenericaRel
    Protected lContext As HttpContext = Nothing
    Protected mstrTabla As String
    Protected mstrConn As String
    Protected mstTransac As SqlClient.SqlTransaction
    Protected mstrUserId As String
    Protected mdsDatos As DataSet
    Protected mstrUltId As String
    Protected mstrMultiplesId As String

#Region "Constructores"
   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet, ByVal pContext As HttpContext)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mdsDatos = pdsDatos
      lContext = pContext
   End Sub
   Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet)
      Me.mstrConn = pstrConn
      Me.mstrUserId = pstrUserId
      Me.mstrTabla = pstrTabla
      Me.mdsDatos = pdsDatos
    End Sub
    ' Dario 2013-07-10 - sobrecarga del constructor base que acepta transaccion
    Public Sub New(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet)
        Me.mstTransac = lTransac
        Me.mstrUserId = pstrUserId
        Me.mstrTabla = pstrTabla
        Me.mdsDatos = pdsDatos
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
        Me.mstrTabla = pstrTabla
    End Sub
    Public Sub New()
    End Sub
#End Region

#Region "Funciones Utiles"
   Public Function Tables(ByVal pTable As String) As DataTable
      Return mdsDatos.Tables(pTable)
   End Function
   Protected Function mObtieneNombreColumna(ByVal odrDeta As DataRow, ByVal pColumna As String) As String
      'OBTIENE EL NOMBRE DE LA COLUMNA
      For Each dCol As DataColumn In odrDeta.Table.Columns
         If dCol.ColumnName.EndsWith(pColumna) Then
            Return (dCol.ColumnName)
         End If
      Next
      Return ("")
   End Function
#End Region

#Region "Propiedades"
   Public ReadOnly Property gstrTabla() As String
      Get
         Return mstrTabla
      End Get
   End Property
   Public ReadOnly Property MiRow() As DataRow
      Get
         If mdsDatos.Tables(0).Rows.Count > 0 Then
            Return (mdsDatos.Tables(0).Rows(0))
         Else
            Return Nothing
         End If

      End Get
   End Property
   Public Property MiDataSet() As DataSet
      Get
         Return (mdsDatos)
      End Get
      Set(ByVal Value As DataSet)
         mdsDatos = Value
      End Set
   End Property
   Public ReadOnly Property gstrUltId() As String
      Get
         Return mstrUltId
      End Get
   End Property

   Public ReadOnly Property gstrMultiplesId() As String
      Get
         Return mstrMultiplesId
      End Get
   End Property
#End Region

#Region "Funciones"
    Private Function mBaja(ByVal pstrId As String, ByVal Trans As SqlTransaction) As String
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Try
            If Trans Is Nothing Then
                lTransac = clsSQLServer.gObtenerTransac(mstrConn)
            Else
                lTransac = Trans
            End If

            If Not mdsDatos Is Nothing Then
                For i As Integer = 0 To mdsDatos.Tables.Count - 1
                    For Each odrDeta As DataRow In mdsDatos.Tables(i).Rows
                        'VALIDA LA EXISTENCIA DEL PROCEDIMIENTO DE ACCION DE BAJA
                        If clsSQLServer.gExecuteScalar(lTransac, "existe_procedimiento_consul '" + mstrTabla + "_baja_accion'") = "1" Then
                            'BORRA ARCHIVOS ADJUNTOS DE LOS DETALLES SOLO CUANDO LA BAJA DE LA CABECERA SERA FISICA
                            If clsSQLServer.gExecuteScalar(lTransac, mstrTabla + "_baja_accion " + pstrId) = "0" Then
                                mBorrarArchivo(lTransac, odrDeta, mdsDatos.Tables(i).TableName, True)
                            End If
                        Else
                            mBorrarArchivo(lTransac, odrDeta, mdsDatos.Tables(i).TableName, True)
                        End If
                    Next
                Next
            End If
            'BORRA CABECERAS Y DETALLES
            clsSQLServer.gBaja(lTransac, mstrUserId, mstrTabla, pstrId)

            clsSQLServer.gCommitTransac(lTransac)

            Return (pstrId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Protected Function mActualizar(ByVal Trans As SqlTransaction) As String
        Dim lstrId As String
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String

        Dim lstrColumnaPath As String = ""
        Dim lstrColumnaPathAnt As String = ""
        Dim lstrArchAnt As String = ""
        Dim lboolAlta As Boolean = False

        Try
            If Trans Is Nothing Then
                lTransac = clsSQLServer.gObtenerTransac(mstrConn)
            Else
                lTransac = Trans
            End If
            With MiRow
                'OBTIENE CABECERA PARA LOS ARCHIVOS ADJUNTOS
                lstrColumnaPath = mObtieneNombreColumna(MiRow, "_parampath")
                lstrColumnaPathAnt = mObtieneNombreColumna(MiRow, "_path_ant")

                If CInt(MiRow.Item(0)) <= 0 Then
                    'ALTA TABLA
                    lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                    lboolAlta = True
                Else
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                    lstrId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                    lboolAlta = False
                End If

                If lstrColumnaPath <> "" Then
                    If lstrColumnaPathAnt <> "" Then
                        If Not (.Item(lstrColumnaPathAnt) Is DBNull.Value) Then
                            lstrArchAnt = .Item(lstrColumnaPathAnt)

                        End If
                    End If
                    SRA_Neg.Utiles.mGuardarArchivosFisico(lTransac, mstrTabla, .Item(lstrColumnaPath), lstrId, lContext, lstrArchAnt)
                End If

                'DETALLE
                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    ' Dario 2013-09-03 se validad que no se intente actualizar la tabla COMPROB_Sobre_Tasa_Vtos
                    If (mdsDatos.Tables(i).TableName().ToUpper() <> "COMPROB_SOBRE_TASA_VTOS" _
                        And mdsDatos.Tables(i).TableName().ToUpper() <> "TABLE7") _
                        And mdsDatos.Tables(i).TableName.ToLower <> "productos_numeros" Then
                        lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                        lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                        lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName
                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)

                        If (lboolAlta And mdsDatos.Tables(i).TableName().ToLower() = "inscripciones_factura") Then
                            Dim hdnCompId As String = clsSQLServer.gObtenerEstruc(mstrConn, "inscripcionesX", lstrId).Tables(0).Rows(0).Item("_comp_id").ToString

                            Dim respuesta As String
                            'Dim objWsAFIP As WSFactElet
                            ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                            respuesta = WSFactElet.GetCAE(String.Empty, hdnCompId)
                            '                 0                1                2              3              4            5
                            ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                            '                  6                      7                8 
                            '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                            Dim respArray As Array = respuesta.Split("|"c)
                            ' si retorna ok 
                            If (respArray(0) = "A") Then
                                ' controlo que el comp id enviado es igual al recibido
                                Dim compIdresp As String = respArray(2)

                                Dim comp_cemi_nume As String = ""
                                Dim opcional1 As String = ""
                                Dim opcional2 As String = ""

                                Try
                                    comp_cemi_nume = respArray(6)
                                Catch ex As Exception

                                End Try

                                Try
                                    opcional1 = respArray(7)
                                Catch ex As Exception

                                End Try

                                Try
                                    opcional2 = respArray(8)
                                Catch ex As Exception

                                End Try

                                If (comp_cemi_nume.Trim().Length = 0) Then
                                    comp_cemi_nume = "null"
                                End If

                                If (opcional1.Trim().Length = 0) Then
                                    opcional1 = "null"
                                End If

                                If (opcional2.Trim().Length = 0) Then
                                    opcional2 = "null"
                                End If

                                If (compIdresp.Trim() = hdnCompId.Trim()) Then
                                    ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                                    Dim compNumeresp As String = respArray(3)
                                    'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & hdnCompId & " ,@comp_nume=" & compNumeresp
                                    Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & hdnCompId & " ,@comp_nume=" & compNumeresp _
                                                       & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                                    ' hago el update del @comp_nume de la tabla de comprobantes
                                    clsError.gLoggear("GetCAE 8:: " + proc)
                                    Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                                    ' si no realizo el update error
                                    If (intUpdateRest = 0) Then
                                        Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                                    End If
                                Else
                                    ' si los com id son diferentes error
                                    Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & hdnCompId & " * " & respuesta)
                                End If
                            Else
                                ' cualquier cosa fuera de ok error 
                                Throw New Exception(respuesta)
                            End If
                        End If
                    End If
                Next

                ' solo entra aca si es alta Fact Elect AFIP
                If (lboolAlta And mstrTabla = SRA_Neg.Constantes.gTab_Comprobantes) Then
                    If (mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.ND _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.NC _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.Factura) Then
                        Dim respuesta As String
                        'Dim objWsAFIP As WSFactElet
                        ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                        respuesta = WSFactElet.GetCAE(String.Empty, lstrId)
                        '                 0                1                2              3              4            5
                        ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                        '                  6                      7                8 
                        '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                        Dim respArray As Array = respuesta.Split("|"c)
                        ' si retorna ok 
                        If (respArray(0) = "A") Then
                            ' controlo que el comp id enviado es igual al recibido
                            Dim compIdresp As String = respArray(2)

                            Dim comp_cemi_nume As String = ""
                            Dim opcional1 As String = ""
                            Dim opcional2 As String = ""

                            Try
                                comp_cemi_nume = respArray(6)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional1 = respArray(7)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional2 = respArray(8)
                            Catch ex As Exception

                            End Try

                            If (comp_cemi_nume.Trim().Length = 0) Then
                                comp_cemi_nume = "null"
                            End If

                            If (opcional1.Trim().Length = 0) Then
                                opcional1 = "null"
                            End If

                            If (opcional2.Trim().Length = 0) Then
                                opcional2 = "null"
                            End If

                            If (compIdresp.Trim() = lstrId.Trim()) Then
                                ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                                Dim compNumeresp As String = respArray(3)
                                'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp
                                Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp _
                                                   & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                                ' hago el update del @comp_nume de la tabla de comprobantes
                                clsError.gLoggear("GetCAE 9:: " + proc)
                                Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                                ' si no realizo el update error
                                If (intUpdateRest = 0) Then
                                    Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                                End If
                            Else
                                ' si los com id son diferentes error
                                Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrId & " * " & respuesta)
                            End If
                        Else
                            ' cualquier cosa fuera de ok error 
                            Throw New Exception(respuesta)
                        End If
                    End If
                End If

            End With
            clsSQLServer.gCommitTransac(lTransac)

            ' consulto si tengo comprobantes del tipo 29,31,32 sin comp_nume
            Dim dsCompAFIP As DataSet = clsSQLServer.gExecuteQuery(mstrConn, "exec comprobantes_a_enviar_afipws ")
            ' si es asi los recorro y pido autorizacion a la afip
            For Each ldrCompAFIP As DataRow In dsCompAFIP.Tables(0).Rows
                If Not ldrCompAFIP.IsNull("comp_id") Then
                    Dim idCompAFIP As String = ldrCompAFIP.Item("comp_id").ToString()

                    Dim respuesta As String
                    'Dim objWsAFIP As WSFactElet
                    ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                    respuesta = WSFactElet.GetCAE(String.Empty, idCompAFIP)
                    '                 0                1                2              3              4            5
                    ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                    '                  6                      7                8 
                    '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                    Dim respArray As Array = respuesta.Split("|"c)
                    ' si retorna ok 
                    If (respArray(0) = "A") Then
                        ' controlo que el comp id enviado es igual al recibido
                        Dim compIdresp As String = respArray(2)

                        Dim comp_cemi_nume As String = ""
                        Dim opcional1 As String = ""
                        Dim opcional2 As String = ""

                        Try
                            comp_cemi_nume = respArray(6)
                        Catch ex As Exception

                        End Try

                        Try
                            opcional1 = respArray(7)
                        Catch ex As Exception

                        End Try

                        Try
                            opcional2 = respArray(8)
                        Catch ex As Exception

                        End Try

                        If (comp_cemi_nume.Trim().Length = 0) Then
                            comp_cemi_nume = "null"
                        End If

                        If (opcional1.Trim().Length = 0) Then
                            opcional1 = "null"
                        End If

                        If (opcional2.Trim().Length = 0) Then
                            opcional2 = "null"
                        End If

                        If (compIdresp.Trim() = idCompAFIP.Trim()) Then
                            ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                            Dim compNumeresp As String = respArray(3)
                            'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & idCompAFIP & " ,@comp_nume=" & compNumeresp
                            Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & idCompAFIP & " ,@comp_nume=" & compNumeresp _
                                               & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                            ' hago el update del @comp_nume de la tabla de comprobantes
                            clsError.gLoggear("GetCAE 10:: " + proc)
                            Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(mstrConn, proc)
                            ' si no realizo el update error
                            If (intUpdateRest = 0) Then
                                Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                            End If
                        Else
                            ' si los com id son diferentes error
                            Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & idCompAFIP & " * " & respuesta)
                        End If
                    Else
                        ' cualquier cosa fuera de ok error 
                        Throw New Exception(respuesta)
                    End If
                End If
            Next

            Return (lstrId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Protected Function mAltaPlanFacilidades() As String
        Dim lstrId As String
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String

        Dim lstrColumnaPath As String = ""
        Dim lstrColumnaPathAnt As String = ""
        Dim lstrArchAnt As String = ""
        Dim lboolAlta As Boolean = False

        Try

            With MiRow
                lTransac = clsSQLServer.gObtenerTransac(mstrConn)
                'OBTIENE CABECERA PARA LOS ARCHIVOS ADJUNTOS
                lstrColumnaPath = mObtieneNombreColumna(MiRow, "_parampath")
                lstrColumnaPathAnt = mObtieneNombreColumna(MiRow, "_path_ant")

                If CInt(MiRow.Item(0)) <= 0 Then
                    'ALTA TABLA
                    lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                    lboolAlta = True
                Else
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                    lstrId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                    lboolAlta = False
                End If

                If lstrColumnaPath <> "" Then
                    If lstrColumnaPathAnt <> "" Then
                        If Not (.Item(lstrColumnaPathAnt) Is DBNull.Value) Then
                            lstrArchAnt = .Item(lstrColumnaPathAnt)

                        End If
                    End If
                    SRA_Neg.Utiles.mGuardarArchivosFisico(lTransac, mstrTabla, .Item(lstrColumnaPath), lstrId, lContext, lstrArchAnt)
                End If

                'DETALLE
                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    ' Dario 2013-09-03 se validad que no se intente actualizar la tabla COMPROB_Sobre_Tasa_Vtos
                    If (mdsDatos.Tables(i).TableName().ToUpper() <> "COMPROB_SOBRE_TASA_VTOS" _
                        And mdsDatos.Tables(i).TableName().ToUpper() <> "TABLE7") _
                        And mdsDatos.Tables(i).TableName.ToLower <> "productos_numeros" Then
                        lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                        lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                        lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName
                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)
                    End If
                Next

                ' solo entra aca si es alta Fact Elect AFIP
                If (lboolAlta And mstrTabla = SRA_Neg.Constantes.gTab_Comprobantes) Then
                    If (mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.ND _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.NC _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.Factura) Then
                        Dim respuesta As String
                        'Dim objWsAFIP As WSFactElet
                        ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                        respuesta = WSFactElet.GetCAE(String.Empty, lstrId)
                        '                 0                1                2              3              4            5
                        ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                        '                  6                      7                8 
                        '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                        Dim respArray As Array = respuesta.Split("|"c)
                        ' si retorna ok 
                        If (respArray(0) = "A") Then
                            ' controlo que el comp id enviado es igual al recibido
                            Dim compIdresp As String = respArray(2)

                            Dim comp_cemi_nume As String = ""
                            Dim opcional1 As String = ""
                            Dim opcional2 As String = ""

                            Try
                                comp_cemi_nume = respArray(6)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional1 = respArray(7)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional2 = respArray(8)
                            Catch ex As Exception

                            End Try

                            If (comp_cemi_nume.Trim().Length = 0) Then
                                comp_cemi_nume = "null"
                            End If

                            If (opcional1.Trim().Length = 0) Then
                                opcional1 = "null"
                            End If

                            If (opcional2.Trim().Length = 0) Then
                                opcional2 = "null"
                            End If

                            If (compIdresp.Trim() = lstrId.Trim()) Then
                                ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                                Dim compNumeresp As String = respArray(3)
                                'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp
                                Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp _
                                                   & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                                ' hago el update del @comp_nume de la tabla de comprobantes
                                clsError.gLoggear("GetCAE 11:: " + proc)
                                Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                                ' si no realizo el update error
                                If (intUpdateRest = 0) Then
                                    Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                                End If
                            Else
                                ' si los com id son diferentes error
                                Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrId & " * " & respuesta)
                            End If
                        Else
                            ' cualquier cosa fuera de ok error 
                            Throw New Exception(respuesta)
                        End If
                    End If
                End If
                clsSQLServer.gCommitTransac(lTransac)
            End With

            Return (lstrId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Function ActualizaMulRelDetaIds(ByVal pstrTablaIds As String) As String
        Dim lstrTABLAId As String = Nothing
        Dim lstrId As String = Nothing
        Dim lstrCabeId As String = Nothing
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String = Nothing
        Dim lboolAlta As Boolean = False

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            For Each ldrCab As DataRow In mdsDatos.Tables(mstrTabla).Select
                If CInt(ldrCab.Item(0)) <= 0 Then
                    'ALTA TABLA
                    lstrCabeId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, ldrCab)
                    lboolAlta = True
                Else
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, ldrCab)
                    lstrCabeId = ldrCab.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                    lboolAlta = False
                End If

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                    lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                    lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName
                    For Each ldr As DataRow In mdsDatos.Tables(i).Select
                        ldr.Item(lstrColPK) = lstrCabeId
                    Next

                    If mdsDatos.Tables(i).TableName = pstrTablaIds Then
                        lstrId = ActualizarDetasMulIds(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrCabeId, lstrColPK + "=" + lstrCabeId)
                    Else
                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrCabeId, lstrColPK + "=" + lstrCabeId)
                    End If
                Next
                ' solo entra aca si es alta Fact Elect AFIP
                If (lboolAlta And mstrTabla = SRA_Neg.Constantes.gTab_Comprobantes) Then
                    If (mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.ND _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.NC _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.Factura) Then
                        Dim respuesta As String
                        'Dim objWsAFIP As WSFactElet
                        ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                        respuesta = WSFactElet.GetCAE(String.Empty, lstrCabeId)
                        '                 0                1                2              3              4            5
                        ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                        '                  6                      7                8 
                        '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                        Dim respArray As Array = respuesta.Split("|"c)
                        ' si retorna ok 
                        If (respArray(0) = "A") Then
                            ' controlo que el comp id enviado es igual al recibido
                            Dim compIdresp As String = respArray(2)

                            Dim comp_cemi_nume As String = ""
                            Dim opcional1 As String = ""
                            Dim opcional2 As String = ""

                            Try
                                comp_cemi_nume = respArray(6)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional1 = respArray(7)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional2 = respArray(8)
                            Catch ex As Exception

                            End Try

                            If (comp_cemi_nume.Trim().Length = 0) Then
                                comp_cemi_nume = "null"
                            End If

                            If (opcional1.Trim().Length = 0) Then
                                opcional1 = "null"
                            End If

                            If (opcional2.Trim().Length = 0) Then
                                opcional2 = "null"
                            End If

                            If (compIdresp.Trim() = lstrCabeId.Trim()) Then
                                ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                                Dim compNumeresp As String = respArray(3)
                                'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrCabeId & " ,@comp_nume=" & compNumeresp
                                Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrCabeId & " ,@comp_nume=" & compNumeresp _
                                                   & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                                ' hago el update del @comp_nume de la tabla de comprobantes
                                clsError.gLoggear("GetCAE 12:: " + proc)
                                Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                                ' si no realizo el update error
                                If (intUpdateRest = 0) Then
                                    Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                                End If
                            Else
                                ' si los com id son diferentes error
                                Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrCabeId & " * " & respuesta)
                            End If
                        Else
                            ' cualquier cosa fuera de ok error 
                            Throw New Exception(respuesta)
                        End If
                    End If
                End If
            Next

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Function ActualizaMulRelConnection() As String
        Dim lstrTABLAId As String = Nothing
        Dim lstrId As String = Nothing
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String = Nothing
        Dim lboolAlta As Boolean = False

        Try
            For Each ldrCab As DataRow In mdsDatos.Tables(mstrTabla).Select
                lTransac = clsSQLServer.gObtenerTransac(mstrConn)
                If CInt(ldrCab.Item(0)) <= 0 Then
                    'ALTA TABLA
                    lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, ldrCab)
                    lboolAlta = True
                Else
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, ldrCab)
                    lstrId = ldrCab.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                    lboolAlta = False
                End If

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    ' Dario 2013-09-03 se validad que no se intente actualizar la tabla COMPROB_Sobre_Tasa_Vtos
                    If (mdsDatos.Tables(i).TableName().ToUpper() <> "COMPROB_SOBRE_TASA_VTOS" _
                        And mdsDatos.Tables(i).TableName().ToUpper() <> "TABLE7") Then
                        lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                        lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                        lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName
                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId, lstrColPK + "=" + ldrCab.Item(0).ToString)
                    End If
                Next

                ' solo entra aca si es alta Fact Elect AFIP
                If (lboolAlta And mstrTabla = SRA_Neg.Constantes.gTab_Comprobantes) Then
                    If (mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.ND _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.NC _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.Factura) Then
                        Dim respuesta As String
                        'Dim objWsAFIP As WSFactElet
                        ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                        respuesta = WSFactElet.GetCAE(String.Empty, lstrId)
                        '                 0                1                2              3              4            5
                        ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                        '                  6                      7                8 
                        '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                        Dim respArray As Array = respuesta.Split("|"c)
                        ' si retorna ok 
                        If (respArray(0) = "A") Then
                            ' controlo que el comp id enviado es igual al recibido
                            Dim compIdresp As String = respArray(2)

                            Dim comp_cemi_nume As String = ""
                            Dim opcional1 As String = ""
                            Dim opcional2 As String = ""

                            Try
                                comp_cemi_nume = respArray(6)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional1 = respArray(7)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional2 = respArray(8)
                            Catch ex As Exception

                            End Try

                            If (comp_cemi_nume.Trim().Length = 0) Then
                                comp_cemi_nume = "null"
                            End If

                            If (opcional1.Trim().Length = 0) Then
                                opcional1 = "null"
                            End If

                            If (opcional2.Trim().Length = 0) Then
                                opcional2 = "null"
                            End If

                            If (compIdresp.Trim() = lstrId.Trim()) Then
                                ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                                Dim compNumeresp As String = respArray(3)
                                'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp
                                Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp _
                                                   & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                                ' hago el update del @comp_nume de la tabla de comprobantes
                                clsError.gLoggear("GetCAE 13:: " + proc)
                                Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                                ' si no realizo el update error
                                If (intUpdateRest = 0) Then
                                    Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                                End If
                            Else
                                ' si los com id son diferentes error
                                Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrId & " * " & respuesta)
                            End If
                        Else
                            ' cualquier cosa fuera de ok error 
                            Throw New Exception(respuesta)
                        End If
                    End If
                End If
                clsSQLServer.gCommitTransac(lTransac)
                mstrMultiplesId += lstrId + ","
            Next

            If mstrMultiplesId <> "" Then
                mstrMultiplesId = mstrMultiplesId.Substring(0, mstrMultiplesId.Length - 1)
            End If

            Return (lstrId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Function ActualizaMulRel() As String
        Dim lstrTABLAId As String = Nothing
        Dim lstrId As String = Nothing
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String = Nothing
        Dim lboolAlta As Boolean = False

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            For Each ldrCab As DataRow In mdsDatos.Tables(mstrTabla).Select
                If CInt(ldrCab.Item(0)) <= 0 Then
                    'ALTA TABLA
                    lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, ldrCab)
                    lboolAlta = True
                Else
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, ldrCab)
                    lstrId = ldrCab.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next
                    lboolAlta = False
                End If

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    ' Dario 2013-09-03 se validad que no se intente actualizar la tabla COMPROB_Sobre_Tasa_Vtos
                    If (mdsDatos.Tables(i).TableName().ToUpper() <> "COMPROB_SOBRE_TASA_VTOS" _
                        And mdsDatos.Tables(i).TableName().ToUpper() <> "TABLE7") Then
                        lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                        lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                        lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName
                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId, lstrColPK + "=" + ldrCab.Item(0).ToString)
                    End If
                Next

                ' solo entra aca si es alta Fact Elect AFIP
                If (lboolAlta And mstrTabla = SRA_Neg.Constantes.gTab_Comprobantes) Then
                    If (mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.ND _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.NC _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.Factura) Then
                        Dim respuesta As String
                        ' Dim objWsAFIP As WSFactElet
                        ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                        respuesta = WSFactElet.GetCAE(String.Empty, lstrId)
                        '                 0                1                2              3              4            5
                        ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                        '                  6                      7                8 
                        '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                        Dim respArray As Array = respuesta.Split("|"c)
                        ' si retorna ok 
                        If (respArray(0) = "A") Then
                            ' controlo que el comp id enviado es igual al recibido
                            Dim compIdresp As String = respArray(2)

                            Dim comp_cemi_nume As String = ""
                            Dim opcional1 As String = ""
                            Dim opcional2 As String = ""

                            Try
                                comp_cemi_nume = respArray(6)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional1 = respArray(7)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional2 = respArray(8)
                            Catch ex As Exception

                            End Try

                            If (comp_cemi_nume.Trim().Length = 0) Then
                                comp_cemi_nume = "null"
                            End If

                            If (opcional1.Trim().Length = 0) Then
                                opcional1 = "null"
                            End If

                            If (opcional2.Trim().Length = 0) Then
                                opcional2 = "null"
                            End If

                            If (compIdresp.Trim() = lstrId.Trim()) Then
                                ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                                Dim compNumeresp As String = respArray(3)
                                'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp
                                Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp _
                                                   & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                                ' hago el update del @comp_nume de la tabla de comprobantes
                                clsError.gLoggear("GetCAE 14:: " + proc)
                                Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                                ' si no realizo el update error
                                If (intUpdateRest = 0) Then
                                    Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                                End If
                            Else
                                ' si los com id son diferentes error
                                Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrId & " * " & respuesta)
                            End If
                        Else
                            ' cualquier cosa fuera de ok error 
                            Throw New Exception(respuesta)
                        End If
                    End If
                End If

                mstrMultiplesId += lstrId + ","
            Next

            clsSQLServer.gCommitTransac(lTransac)

            If mstrMultiplesId <> "" Then
                mstrMultiplesId = mstrMultiplesId.Substring(0, mstrMultiplesId.Length - 1)
            End If

            Return (lstrId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Function ActualizaMul() As String
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrId As String = Nothing
        Dim lboolAlta As Boolean = False
        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            mdsDatos.Tables(0).TableName = mstrTabla
            BorrarDetas(lTransac, mstrTabla)

            For Each ldr As DataRow In mdsDatos.Tables(0).Select
                If ldr.Item(0) Is DBNull.Value OrElse ldr.Item(0) <= 0 Then
                    lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, ldr)
                    lboolAlta = True
                Else
                    clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, ldr)
                    lboolAlta = False
                End If

                ' solo entra aca si es alta Fact Elect AFIP
                If (lboolAlta And mstrTabla = SRA_Neg.Constantes.gTab_Comprobantes) Then
                    If (mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.ND _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.NC _
                    Or mdsDatos.Tables(mstrTabla).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.Factura) Then
                        Dim respuesta As String
                        'Dim objWsAFIP As WSFactElet
                        ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                        respuesta = WSFactElet.GetCAE(String.Empty, lstrId)
                        '                 0                1                2              3              4            5
                        ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                        '                  6                      7                8 
                        '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                        Dim respArray As Array = respuesta.Split("|"c)
                        ' si retorna ok 
                        If (respArray(0) = "A") Then
                            ' controlo que el comp id enviado es igual al recibido
                            Dim compIdresp As String = respArray(2)

                            Dim comp_cemi_nume As String = ""
                            Dim opcional1 As String = ""
                            Dim opcional2 As String = ""

                            Try
                                comp_cemi_nume = respArray(6)
                            Catch ex As Exception
                                Throw New Exception(ex.ToString())
                            End Try

                            Try
                                opcional1 = respArray(7)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional2 = respArray(8)
                            Catch ex As Exception

                            End Try

                            If (comp_cemi_nume.Trim().Length = 0) Then
                                comp_cemi_nume = "null"
                            End If

                            If (opcional1.Trim().Length = 0) Then
                                opcional1 = "null"
                            End If

                            If (opcional2.Trim().Length = 0) Then
                                opcional2 = "null"
                            End If

                            If (compIdresp.Trim() = lstrId.Trim()) Then
                                ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                                Dim compNumeresp As String = respArray(3)
                                'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp
                                Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp _
                                                   & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                                ' hago el update del @comp_nume de la tabla de comprobantes
                                clsError.gLoggear("GetCAE 15:: " + proc)
                                Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                                ' si no realizo el update error
                                If (intUpdateRest = 0) Then
                                    Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                                End If
                            Else
                                ' si los com id son diferentes error
                                Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrId & " * " & respuesta)
                            End If
                        Else
                            ' cualquier cosa fuera de ok error 
                            Throw New Exception(respuesta)
                        End If
                    End If
                End If

            Next

            clsSQLServer.gCommitTransac(lTransac)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function

    Protected Function ActualizarDetasMulIds(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTablaDet As String, ByVal pstrCampoPK As String, ByVal lintCabId As Integer, Optional ByVal pstrFiltro As String = "", Optional ByVal pdsDatos As DataSet = Nothing) As String
        Dim lstrColumnaPath As String = ""
        Dim lstrColumnaPathAnt As String = ""
        Dim lstrArchAnt As String
        Dim lstrId As Integer
        Dim ldsDatos As DataSet = pdsDatos

        If ldsDatos Is Nothing Then
            ldsDatos = mdsDatos
        End If

        For Each odrDeta As DataRow In ldsDatos.Tables(pstrTablaDet).Select(pstrFiltro)
            With odrDeta

                'OBTIENE CABECERA PARA LOS ARCHIVOS ADJUNTOS
                lstrColumnaPath = mObtieneNombreColumna(odrDeta, "_parampath")
                lstrColumnaPathAnt = mObtieneNombreColumna(odrDeta, "_path_ant")

                If .Item(0) <= 0 Then
                    .Item(pstrCampoPK) = lintCabId
                    'ALTA 
                    lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, pstrTablaDet, odrDeta)
                    If lstrId.ToString <> "0" Then
                        mstrMultiplesId = mstrMultiplesId & lstrId.ToString & ","
                    End If
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, pstrTablaDet, odrDeta)
                    lstrId = .Item(0)
                End If

                If lstrColumnaPath <> "" Then
                    '.Item(lstrColumnaPath) = .Item(lstrColumnaPath)
                    If lstrColumnaPathAnt <> "" Then
                        lstrArchAnt = .Item(lstrColumnaPathAnt).ToString
                    End If
                    SRA_Neg.Utiles.mGuardarArchivosFisico(lTransac, pstrTablaDet, .Item(lstrColumnaPath), lstrId, lContext, lstrArchAnt)
                End If

            End With
        Next

        If mstrMultiplesId <> "" Then
            mstrMultiplesId = mstrMultiplesId.Substring(0, mstrMultiplesId.Length - 1)
        End If

        Return mstrMultiplesId
    End Function

    Protected Function ActualizarDetas(ByVal lTransac As SqlClient.SqlTransaction, _
            ByVal pstrTablaDet As String, _
            ByVal pstrCampoPK As String, _
            ByVal lintCabId As Integer, _
            Optional ByVal pstrFiltro As String = "", _
            Optional ByVal pdsDatos As DataSet = Nothing) As String


        Dim lstrColumnaPath As String = ""
        Dim lstrColumnaPathAnt As String = ""
        Dim lstrArchAnt As String
        Dim lstrId As Integer
        Dim ldsDatos As DataSet = pdsDatos

        If ldsDatos Is Nothing Then
            ldsDatos = mdsDatos
        End If
        Dim contador As Integer
        For Each odrDeta As DataRow In ldsDatos.Tables(pstrTablaDet).Select(pstrFiltro)
            ' ConvertDsToXML(ldsDatos, "contador" & contador.ToString)

            With odrDeta

                'OBTIENE CABECERA PARA LOS ARCHIVOS ADJUNTOS
                lstrColumnaPath = mObtieneNombreColumna(odrDeta, "_parampath")
                lstrColumnaPathAnt = mObtieneNombreColumna(odrDeta, "_path_ant")
                If IsDBNull(odrDeta.Item(0)) Then
                    odrDeta.Item(0) = 0
                End If

                If odrDeta.Item(0) <= 0 Then
                    .Item(pstrCampoPK) = lintCabId
                    'ALTA 
                    mstrUltId = clsSQLServer.gAlta(lTransac, mstrUserId, pstrTablaDet, odrDeta)
                    lstrId = mstrUltId
                Else
                    'MODIFICACION 
                    clsSQLServer.gModi(lTransac, mstrUserId, pstrTablaDet, odrDeta)
                    lstrId = .Item(0)
                End If

                If lstrColumnaPath <> "" Then
                    '.Item(lstrColumnaPath) = .Item(lstrColumnaPath)
                    If lstrColumnaPathAnt <> "" Then
                        lstrArchAnt = .Item(lstrColumnaPathAnt).ToString
                    End If
                    SRA_Neg.Utiles.mGuardarArchivosFisico(lTransac, pstrTablaDet, .Item(lstrColumnaPath), lstrId, lContext, lstrArchAnt)
                End If

            End With
        Next

        Return mstrUltId
    End Function

    Protected Sub BorrarDetas(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTablaDet As String, Optional ByVal pdsDatos As DataSet = Nothing)
        Dim ldsDatos As DataSet = pdsDatos
        If ldsDatos Is Nothing Then
            ldsDatos = mdsDatos
        End If

        Dim lstrColumnaPath As String = ""
        For Each odrDeta As DataRow In ldsDatos.Tables(pstrTablaDet).Select("", "", DataViewRowState.Deleted)
            With odrDeta
                .RejectChanges()
                If Not .IsNull(0) Then
                    'BORRA ARCHIVOS ADJUNTOS
                    mBorrarArchivo(lTransac, odrDeta, pstrTablaDet)
                    'BORRA EL REGISTRO DEL DETALLE
                    clsSQLServer.gBaja(lTransac, mstrUserId, pstrTablaDet, .Item(0).ToString, True)
                End If
                odrDeta.Table.Rows.Remove(odrDeta)
            End With
        Next
    End Sub

    Protected Sub mBorrarArchivo(ByVal lTransac As SqlClient.SqlTransaction, ByVal odrDeta As DataRow, ByVal pstrTablaDet As String, Optional ByVal pboolFisica As Boolean = False)
        Dim lstrColumna As String = ""
        Dim lstrColumnaPath As String = ""
        Dim lstrPath As String
        Dim lstrParamPath As String

        With odrDeta
            lstrColumnaPath = mObtieneNombreColumna(odrDeta, "_parampath")
            lstrColumna = mObtieneNombreColumna(odrDeta, "_path")
            If lstrColumna <> "" Then lstrPath = .Item(lstrColumna).ToString

            'SI CONTIENE ARCHIVOS ADJUNTOS
            If lstrColumnaPath <> "" Then
                lstrParamPath = clsSQLServer.gParametroValorConsul(lTransac.Connection.ConnectionString, .Item(lstrColumnaPath))
                'SI ENCUENTRA EL PATH DEL ARCHIVO
                If lstrParamPath.ToString <> "" Then
                    'BORRA SOLO CUANDO ES BAJA FISICA
                    If clsSQLServer.gExecuteScalar(lTransac, "existe_procedimiento_consul '" + mstrTabla + "_baja_accion'") = "1" Then
                        'BORRA ARCHIVOS ADJUNTOS DE LOS DETALLES SOLO CUANDO LA BAJA DE LA CABECERA SERA FISICA
                        If clsSQLServer.gExecuteScalar(lTransac, pstrTablaDet + "_baja_accion " + .Item(0).ToString) = "0" Or pboolFisica Then
                            SRA_Neg.Utiles.mBorrarArchivos(lstrParamPath, pstrTablaDet, lstrPath, lContext, .Item(0).ToString)
                        End If
                    Else
                        SRA_Neg.Utiles.mBorrarArchivos(lstrParamPath, pstrTablaDet, lstrPath, lContext, .Item(0).ToString)
                    End If
                End If
            End If
        End With
    End Sub
#End Region

#Region "Metodos"
    Public Function Alta() As String
        Return (Alta(Nothing))
    End Function

    Public Function AltaPlanFacilidades() As String
        Return (mAltaPlanFacilidades())
    End Function

    Public Function Alta(ByVal Trans As SqlTransaction) As String
        Return (mActualizar(Trans))
    End Function
    Public Sub Baja(ByVal pstrId As String)
        Baja(pstrId, Nothing)
    End Sub
    Public Sub Baja(ByVal pstrId As String, ByVal Trans As SqlTransaction)
        mBaja(pstrId, Trans)
    End Sub
    Public Shared Function ConvertDsToXML(ByVal pDs As DataSet)
        Dim strFileName As String
        strFileName = "C:\TEMP\DIF.XML"
        Dim fs As New FileStream(strFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
        Dim writer As New StreamWriter(fs, Encoding.UTF8)
        If Not IsNothing(pDs) Then
            pDs.WriteXml(writer, XmlWriteMode.WriteSchema)
        End If

        fs.Close()


    End Function
    Public Shared Function ConvertDsToXML(ByVal pDs As DataSet, ByVal pLogFile As String)
        Dim strFileName As String
        strFileName = "C:\TEMP\" + pLogFile + ".XML"
        Dim fs As New FileStream(strFileName, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite)
        Dim writer As New StreamWriter(fs, Encoding.UTF8)
        If Not IsNothing(pDs) Then
            pDs.WriteXml(writer, XmlWriteMode.WriteSchema)
        End If

        fs.Close()


    End Function
    Public Sub Modi()
        Modi(Nothing)
    End Sub
    Public Sub Modi(ByVal Trans As SqlTransaction)
        mActualizar(Trans)
    End Sub
#End Region

End Class
