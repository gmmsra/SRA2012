Imports ReglasValida.Validaciones
Imports System.IO
Imports System.XML

Imports SRA_Entidad
Imports AccesoBD
Public Class Prestamo


    Inherits Generica


    Protected server As System.Web.HttpServerUtility


    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, _
        ByVal pdsDatos As DataSet)


        mstrConn = pstrConn
        mstrUserId = pstrUserId
        mstrTabla = pstrTabla
        mdsDatos = pdsDatos


    End Sub

    Public Function prestamosGetCantidadById(ByVal PrestamoId As String, ByVal pTableName As String) As Integer
        Dim vRetorno As Integer
        Dim dt As New DataTable
        Dim mstrCmd As String

        mstrCmd = "exec prestamosGetCantidadById "
        mstrCmd = mstrCmd + " @pstd_pstm_id = " + clsSQLServer.gFormatArg(PrestamoId, SqlDbType.VarChar)



        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            vRetorno = Convert.ToInt32(ValidarNulos(dt.Rows(0).Item("cantidad"), False))
        End If


        Return vRetorno

    End Function

    Public Function mActualPrestamos(ByVal pTipo) As String

        Dim lstrPrestamoId, lstrId As String
        Dim lTransac As SqlClient.SqlTransaction
        Dim lstrColPK As String
        Dim dtPrestamo As DataTable

        Try
            Dim oPrestamo As New SRA_Neg.Prestamo(mstrConn, mstrUserId)

            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            'TABLA prestamos Y SUS DETALLES
            If CInt(MiRow.Item(0)) <= 0 Then
                If pTipo = "A" Then
                    lstrPrestamoId = clsSQLServer.gAlta(lTransac, mstrUserId, "Prestamos", MiRow)
                    MiRow(0) = lstrPrestamoId


                    For Each lDr As DataRow In mdsDatos.Tables(mdsDatos.Tables("Prestamos_deta").TableName).Select()
                        If ValidarNulos(lDr.Item("pstd_pstm_id"), False) < 0 Then
                            lDr.Item("pstd_pstm_id") = lstrPrestamoId

                        End If
                    Next

                    For Each lDr As DataRow In mdsDatos.Tables("Prestamos_deta").Select("_estado='Prestado'")
                        clsSQLServer.gAlta(lTransac, mstrUserId, "Prestamos_deta", lDr)
                    Next

                End If
            Else
                If pTipo = "M" Then
                    'MODIFICACION TABLA
                    clsSQLServer.gModi(lTransac, mstrUserId, "prestamos", MiRow)
                    lstrPrestamoId = MiRow.Item(0).ToString

                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                    Next

                    'DETALLE
                    For i As Integer = 1 To mdsDatos.Tables.Count - 1
                        lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                        lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                        lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrPrestamoId)

                    Next
                End If

            End If

            If (lstrPrestamoId <> "-1") Then
                'Limpiar errores anteriores.
                ReglasValida.Validaciones.gLimpiarErrores(mstrConn, SRA_Neg.Constantes.gTab_PrestamosDeta, lstrPrestamoId)

                'Aplicar Reglas.
                ''lstrMsgError = 
                gValidarReglas(lTransac, SRA_Neg.Constantes.gTab_Prestamos, lstrPrestamoId, _
                                mstrUserId, True, ReglasValida.Validaciones.Procesos.Prestamos.ToString(), mdsDatos)


            End If

           

            clsSQLServer.gCommitTransac(lTransac)
            mdsDatos.AcceptChanges()
            Return (lstrPrestamoId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try

    End Function



    Public Function GetPrestamosDetallesById(ByVal PrestamoId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        dt.TableName = pTableName

        Dim mstrCmd As String

        mstrCmd = "exec GetPrestamosDetallesById "
        mstrCmd = mstrCmd + " @PrestamoId = " + clsSQLServer.gFormatArg(PrestamoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetEstaPrestadoByProdId(ByVal ProductoId As String) As Boolean


        Dim mstrCmd As String
        Dim strPrestado As String
        Dim boolPrestado As Boolean
        Dim dt As DataTable

        mstrCmd = "exec sp_ProductoPrestadoByProdId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.Int)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            strPrestado = dt.Rows(0).Item("IsPrestado").ToString()
        End If

        If strPrestado = "0" Then
            boolPrestado = False
        Else
            boolPrestado = True
        End If


        Return boolPrestado

    End Function


    Public Function GetPrestamoErrorByProdId(ByVal ProductoId As String) As Boolean


        Dim mstrCmd As String
        Dim boolTieneError As Boolean = False
        Dim dt As DataTable

        mstrCmd = "exec GetPrestamoErrorByProdId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.Int)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            boolTieneError = True
        End If


        Return boolTieneError

    End Function
    Public Function GetPrestamoErrorByNroPrestamo(ByVal pNumeroPrestamo As String) As Boolean


        Dim mstrCmd As String
        Dim boolTieneError As Boolean = False
        Dim dt As DataTable

        mstrCmd = "exec GetPrestamoErrorByNroPrestamo "
        mstrCmd = mstrCmd + " @NroPrestamo = " + clsSQLServer.gFormatArg(pNumeroPrestamo, SqlDbType.Int)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            boolTieneError = True
        End If


        Return boolTieneError

    End Function


    Public Function sp_FinalizaPrestamoByProdId(ByVal ProductoId As String, ByVal FechaFinaliza As Date) As Boolean


        Dim mstrCmd As String
        Dim strPrestado As String
        Dim boolPrestado As Boolean
        Dim dt As DataTable

        mstrCmd = "exec sp_FinalizaPrestamoByProdId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.Int)
        mstrCmd = mstrCmd + " @FechaFin = " + clsSQLServer.gFormatArg(FechaFinaliza, SqlDbType.Int)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            strPrestado = dt.Rows(0).Item("IsPrestado").ToString()
        End If

        If strPrestado = "0" Then
            boolPrestado = False
        Else
            boolPrestado = True
        End If


        Return boolPrestado

    End Function




    Public Sub GrabarBajaEnPrestamo(ByVal PrestamoId As String)
        Dim mstrCmd As String



        mstrCmd = "exec prestamos_baja "
        mstrCmd = mstrCmd + " @pstm_id = " + clsSQLServer.gFormatArg(PrestamoId, SqlDbType.Int)
        mstrCmd = mstrCmd + ", @pstm_audi_user = " + mstrUserId
        mstrCmd = mstrCmd + ", @pstm_esta_id = " + clsSQLServer.gFormatArg(SRA_Neg.Constantes.Estados.Prestamos_Anulado, SqlDbType.Int)

        clsSQLServer.gExecuteQuery(mstrConn, mstrCmd)


    End Sub


    Public Function GetPrestamoDetalleByProdId(ByVal PrestamoId As String, _
       ByVal ProductoId As String, _
       ByVal pTableName As String) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String

        dt.TableName = pTableName



        mstrCmd = "exec GetPrestamoDetalleByProdId "
        mstrCmd = mstrCmd + " @PrestamoId = " + clsSQLServer.gFormatArg(PrestamoId, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function
    Public Function GetFechaPrestamoIniByProdId(ByVal ProductoId As String) As String

        Dim strFechaInicialPrestamo As String = ""
        Dim mstrCmd As String
        Dim dt As DataTable


        mstrCmd = "exec GetFechaPrestamoIniByProdId "
        mstrCmd = mstrCmd + " @ProductoId = " + clsSQLServer.gFormatArg(ProductoId, SqlDbType.VarChar)

        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        If dt.Rows.Count > 0 Then
            strFechaInicialPrestamo = dt.Rows(0).Item("pstm_inic_fecha")
        End If

        Return strFechaInicialPrestamo

    End Function

End Class

