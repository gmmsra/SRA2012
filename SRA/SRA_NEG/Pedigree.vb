Imports ReglasValida.Validaciones
Imports SRA_Neg.Utiles

Public Class Pedigree
    Inherits Generica


    ' Private mstrConn As String
    Private mstrProd, mstrProdId As String
    Private mstrPadreA, mstrPadreAId As String
    Private mstrMadreB, mstrMadreBId As String
    Private mstrMadreA1, mstrMadreA1Id As String
    Private mstrPadreA1, mstrPadreA1Id As String
    Private mstrPadreB1, mstrPadreB1Id As String
    Private mstrMadreB1, mstrMadreB1Id As String
    Private mstrPadreA21, mstrPadreA21Id As String
    Private mstrPadreA22, mstrPadreA22Id As String
    Private mstrMadreA21, mstrMadreA21Id As String
    Private mstrMadreA22, mstrMadreA22Id As String
    Private mstrPadreB11, mstrPadreB11Id As String
    Private mstrMadreB11, mstrMadreB11Id As String
    Private mstrPadreB12, mstrPadreB12Id As String
    Private mstrMadreB12, mstrMadreB12Id As String
    Private mstrPadreA211, mstrPadreA211Id As String
    Private mstrMadreA211, mstrMadreA211Id As String
    Private mstrPadreA212, mstrPadreA212Id As String
    Private mstrMadreA212, mstrMadreA212Id As String
    Private mstrPadreA221, mstrPadreA221Id As String
    Private mstrMadreA221, mstrMadreA221Id As String
    Private mstrPadreA222, mstrPadreA222Id As String
    Private mstrMadreA222, mstrMadreA222Id As String
    Private mstrPadreB111, mstrPadreB111Id As String
    Private mstrPadreB112, mstrPadreB112Id As String
    Private mstrMadreB111, mstrMadreB111Id As String
    Private mstrMadreB112, mstrMadreB112Id As String
    Private mstrPadreB121, mstrPadreB121Id As String
    Private mstrMadreB121, mstrMadreB121Id As String
    Private mstrPadreB122, mstrPadreB122Id As String
    Private mstrMadreB122, mstrMadreB122Id As String



    Public Sub New(ByVal pstrConn As String)
        mstrConn = pstrConn
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId

    End Sub

#Region "Propiedades"
    Public Property pProd() As String
        Get
            Return mstrProd
        End Get
        Set(ByVal Value As String)
            mstrProd = Value
        End Set
    End Property
    Public Property pProdId() As String
        Get
            Return mstrProdId
        End Get
        Set(ByVal Value As String)
            mstrProdId = Value
        End Set
    End Property
    Public Property pPadreA() As String
        Get
            Return mstrPadreA
        End Get
        Set(ByVal Value As String)
            mstrPadreA = Value
        End Set
    End Property
    Public Property pPadreAId() As String
        Get
            Return mstrPadreAId
        End Get
        Set(ByVal Value As String)
            mstrPadreAId = Value
        End Set
    End Property

    Public Property pMadreB() As String
        Get
            Return mstrMadreB
        End Get
        Set(ByVal Value As String)
            mstrMadreB = Value
        End Set
    End Property
    Public Property pMadreBId() As String
        Get
            Return mstrMadreBId
        End Get
        Set(ByVal Value As String)
            mstrMadreBId = Value
        End Set
    End Property

    Public Property pPadreA1() As String
        Get
            Return mstrPadreA1
        End Get
        Set(ByVal Value As String)
            mstrPadreA1 = Value
        End Set
    End Property
    Public Property pPadreA1Id() As String
        Get
            Return mstrPadreA1Id
        End Get
        Set(ByVal Value As String)
            mstrPadreA1Id = Value
        End Set
    End Property

    Public Property pMadreA1() As String
        Get
            Return mstrMadreA1
        End Get
        Set(ByVal Value As String)
            mstrMadreA1 = Value
        End Set
    End Property
    Public Property pMadreA1Id() As String
        Get
            Return mstrMadreA1Id
        End Get
        Set(ByVal Value As String)
            mstrMadreA1Id = Value
        End Set
    End Property

    Public Property pPadreB1() As String
        Get
            Return mstrPadreB1
        End Get
        Set(ByVal Value As String)
            mstrPadreB1 = Value
        End Set
    End Property
    Public Property pPadreB1Id() As String
        Get
            Return mstrPadreB1Id
        End Get
        Set(ByVal Value As String)
            mstrPadreB1Id = Value
        End Set
    End Property

    Public Property pMadreB1() As String
        Get
            Return mstrMadreB1
        End Get
        Set(ByVal Value As String)
            mstrMadreB1 = Value
        End Set
    End Property
    Public Property pMadreB1Id() As String
        Get
            Return mstrMadreB1Id
        End Get
        Set(ByVal Value As String)
            mstrMadreB1Id = Value
        End Set
    End Property

    Public Property pPadreA21() As String
        Get
            Return mstrPadreA21
        End Get
        Set(ByVal Value As String)
            mstrPadreA21 = Value
        End Set
    End Property
    Public Property pPadreA21Id() As String
        Get
            Return mstrPadreA21Id
        End Get
        Set(ByVal Value As String)
            mstrPadreA21Id = Value
        End Set
    End Property

    Public Property pPadreA22() As String
        Get
            Return mstrPadreA22
        End Get
        Set(ByVal Value As String)
            mstrPadreA22 = Value
        End Set
    End Property
    Public Property pPadreA22Id() As String
        Get
            Return mstrPadreA22Id
        End Get
        Set(ByVal Value As String)
            mstrPadreA22Id = Value
        End Set
    End Property

    Public Property pMadreA21() As String
        Get
            Return mstrMadreA21
        End Get
        Set(ByVal Value As String)
            mstrMadreA21 = Value
        End Set
    End Property
    Public Property pMadreA21Id() As String
        Get
            Return mstrMadreA21Id
        End Get
        Set(ByVal Value As String)
            mstrMadreA21Id = Value
        End Set
    End Property

    Public Property pMadreA22() As String
        Get
            Return mstrMadreA22
        End Get
        Set(ByVal Value As String)
            mstrMadreA22 = Value
        End Set
    End Property
    Public Property pMadreA22Id() As String
        Get
            Return mstrMadreA22Id
        End Get
        Set(ByVal Value As String)
            mstrMadreA22Id = Value
        End Set
    End Property

    Public Property pPadreB11() As String
        Get
            Return mstrPadreB11
        End Get
        Set(ByVal Value As String)
            mstrPadreB11 = Value
        End Set
    End Property
    Public Property pPadreB11Id() As String
        Get
            Return mstrPadreB11Id
        End Get
        Set(ByVal Value As String)
            mstrPadreB11Id = Value
        End Set
    End Property

    Public Property pMadreB11() As String
        Get
            Return mstrMadreB11
        End Get
        Set(ByVal Value As String)
            mstrMadreB11 = Value
        End Set
    End Property
    Public Property pMadreB11Id() As String
        Get
            Return mstrMadreB11Id
        End Get
        Set(ByVal Value As String)
            mstrMadreB11Id = Value
        End Set
    End Property

    Public Property pPadreB12() As String
        Get
            Return mstrPadreB12
        End Get
        Set(ByVal Value As String)
            mstrPadreB12 = Value
        End Set
    End Property
    Public Property pPadreB12Id() As String
        Get
            Return mstrPadreB12Id
        End Get
        Set(ByVal Value As String)
            mstrPadreB12Id = Value
        End Set
    End Property

    Public Property pMadreB12() As String
        Get
            Return mstrMadreB12
        End Get
        Set(ByVal Value As String)
            mstrMadreB12 = Value
        End Set
    End Property
    Public Property pMadreB12Id() As String
        Get
            Return mstrMadreB12Id
        End Get
        Set(ByVal Value As String)
            mstrMadreB12Id = Value
        End Set
    End Property

    Public Property pPadreA211() As String
        Get
            Return mstrPadreA211
        End Get
        Set(ByVal Value As String)
            mstrPadreA211 = Value
        End Set
    End Property
    Public Property pPadreA211Id() As String
        Get
            Return mstrPadreA211Id
        End Get
        Set(ByVal Value As String)
            mstrPadreA211Id = Value
        End Set
    End Property

    Public Property pMadreA211() As String
        Get
            Return mstrMadreA211
        End Get
        Set(ByVal Value As String)
            mstrMadreA211 = Value
        End Set
    End Property
    Public Property pMadreA211Id() As String
        Get
            Return mstrMadreA211Id
        End Get
        Set(ByVal Value As String)
            mstrMadreA211Id = Value
        End Set
    End Property

    Public Property pPadreA212() As String
        Get
            Return mstrPadreA212
        End Get
        Set(ByVal Value As String)
            mstrPadreA212 = Value
        End Set
    End Property
    Public Property pPadreA212Id() As String
        Get
            Return mstrPadreA212Id
        End Get
        Set(ByVal Value As String)
            mstrPadreA212Id = Value
        End Set
    End Property

    Public Property pMadreA212() As String
        Get
            Return mstrMadreA212
        End Get
        Set(ByVal Value As String)
            mstrMadreA212 = Value
        End Set
    End Property
    Public Property pMadreA212Id() As String
        Get
            Return mstrMadreA212Id
        End Get
        Set(ByVal Value As String)
            mstrMadreA212Id = Value
        End Set
    End Property

    Public Property pPadreA221() As String
        Get
            Return mstrPadreA221
        End Get
        Set(ByVal Value As String)
            mstrPadreA221 = Value
        End Set
    End Property
    Public Property pPadreA221Id() As String
        Get
            Return mstrPadreA221Id
        End Get
        Set(ByVal Value As String)
            mstrPadreA221Id = Value
        End Set
    End Property

    Public Property pMadreA221() As String
        Get
            Return mstrMadreA221
        End Get
        Set(ByVal Value As String)
            mstrMadreA221 = Value
        End Set
    End Property
    Public Property pMadreA221Id() As String
        Get
            Return mstrMadreA221Id
        End Get
        Set(ByVal Value As String)
            mstrMadreA221Id = Value
        End Set
    End Property

    Public Property pPadreA222() As String
        Get
            Return mstrPadreA222
        End Get
        Set(ByVal Value As String)
            mstrPadreA222 = Value
        End Set
    End Property
    Public Property pPadreA222Id() As String
        Get
            Return mstrPadreA222Id
        End Get
        Set(ByVal Value As String)
            mstrPadreA222Id = Value
        End Set
    End Property

    Public Property pMadreA222() As String
        Get
            Return mstrMadreA222
        End Get
        Set(ByVal Value As String)
            mstrMadreA222 = Value
        End Set
    End Property
    Public Property pMadreA222Id() As String
        Get
            Return mstrMadreA222Id
        End Get
        Set(ByVal Value As String)
            mstrMadreA222Id = Value
        End Set
    End Property

    Public Property pPadreB111() As String
        Get
            Return mstrPadreB111
        End Get
        Set(ByVal Value As String)
            mstrPadreB111 = Value
        End Set
    End Property
    Public Property pPadreB111Id() As String
        Get
            Return mstrPadreB111Id
        End Get
        Set(ByVal Value As String)
            mstrPadreB111Id = Value
        End Set
    End Property

    Public Property pPadreB112() As String
        Get
            Return mstrPadreB112
        End Get
        Set(ByVal Value As String)
            mstrPadreB112 = Value
        End Set
    End Property
    Public Property pPadreB112Id() As String
        Get
            Return mstrPadreB112Id
        End Get
        Set(ByVal Value As String)
            mstrPadreB112Id = Value
        End Set
    End Property

    Public Property pMadreB111() As String
        Get
            Return mstrMadreB111
        End Get
        Set(ByVal Value As String)
            mstrMadreB111 = Value
        End Set
    End Property
    Public Property pMadreB111Id() As String
        Get
            Return mstrMadreB111Id
        End Get
        Set(ByVal Value As String)
            mstrMadreB111Id = Value
        End Set
    End Property

    Public Property pMadreB112() As String
        Get
            Return mstrMadreB112
        End Get
        Set(ByVal Value As String)
            mstrMadreB112 = Value
        End Set
    End Property
    Public Property pMadreB112Id() As String
        Get
            Return mstrMadreB112Id
        End Get
        Set(ByVal Value As String)
            mstrMadreB112Id = Value
        End Set
    End Property

    Public Property pPadreB121() As String
        Get
            Return mstrPadreB121
        End Get
        Set(ByVal Value As String)
            mstrPadreB121 = Value
        End Set
    End Property
    Public Property pPadreB121Id() As String
        Get
            Return mstrPadreB121Id
        End Get
        Set(ByVal Value As String)
            mstrPadreB121Id = Value
        End Set
    End Property

    Public Property pMadreB121() As String
        Get
            Return mstrMadreB121
        End Get
        Set(ByVal Value As String)
            mstrMadreB121 = Value
        End Set
    End Property
    Public Property pMadreB121Id() As String
        Get
            Return mstrMadreB121Id
        End Get
        Set(ByVal Value As String)
            mstrMadreB121Id = Value
        End Set
    End Property

    Public Property pPadreB122() As String
        Get
            Return mstrPadreB122
        End Get
        Set(ByVal Value As String)
            mstrPadreB122 = Value
        End Set
    End Property
    Public Property pPadreB122Id() As String
        Get
            Return mstrPadreB122Id
        End Get
        Set(ByVal Value As String)
            mstrPadreB122Id = Value
        End Set
    End Property

    Public Property pMadreB122() As String
        Get
            Return mstrMadreB122
        End Get
        Set(ByVal Value As String)
            mstrMadreB122 = Value
        End Set
    End Property
    Public Property pMadreB122Id() As String
        Get
            Return mstrMadreB122Id
        End Get
        Set(ByVal Value As String)
            mstrMadreB122Id = Value
        End Set
    End Property






#End Region

#Region "M�todos"
    Public Function ArmarArbolPedigree(ByVal pstrConn As String, ByVal pstrProdId As String)

        Dim mdsDatos As DataSet
        Dim lstrId As String
        Dim lstrOrigen As String
        Dim lstrProd As String
        Dim lstrEspe As String
        Dim lstrSexo As String
        Dim lstrPadre As String
        Dim lstrNivel As String
        Dim lintRegi As Integer
        ' Dim lstrCmd As String = "exec productos_pedigree_busq " & pstrProdId
        Dim lstrCmd As String = "exec productos_pedigree_busqSinRazPad " & pstrProdId

        Dim dr As SqlClient.SqlDataReader = clsSQLServer.gExecuteQueryDR(pstrConn, lstrCmd)
        While (dr.Read())
            lstrId = dr.GetValue(0).ToString().Trim()
            lstrProd = dr.GetValue(1).ToString().Trim()
            lstrPadre = dr.GetValue(2).ToString().Trim()
            lstrSexo = dr.GetValue(3).ToString().Trim()
            lstrEspe = dr.GetValue(4).ToString().Trim()
            lstrNivel = dr.GetValue(5).ToString().Trim()
            Select Case lstrNivel
                Case 0
                    pProd = lstrProd
                    pProdId = lstrId
                Case 1
                    If lstrSexo = 1 Then
                        pPadreA = lstrProd
                        pPadreAId = lstrId
                    Else
                        pMadreB = lstrProd
                        pMadreBId = lstrId
                    End If

                    Dim oProducto As New SRA_Neg.Productos(mstrConn, mstrUserId)
                    Dim oRaza As New SRA_Neg.Razas(mstrConn, mstrUserId)
                    Dim dtProducto As DataTable

                    dtProducto = oProducto.GetProductoById(pProdId, "")

                    If Not pPadreA Is Nothing And Not pPadreAId Is Nothing Then
                        If pPadreA = "" And pPadreAId = "" Then
                            If ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazP"), False) <> "0" Then
                                pPadreA = "Raza:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazP"), False) + "-" + _
                                oRaza.GetRazaByRazaCodigo(ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazP"), False)).Trim + _
                                 " | HBA:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_hbap"), False) + _
                                  " | RP:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RpPd"), False)

                            End If


                        End If
                    End If

                    If Not pMadreB Is Nothing And Not pMadreBId Is Nothing Then
                        If pMadreB = "" And pMadreBId = "" Then
                            If ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazM"), False) <> "0" Then
                                pMadreB = "Raza:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazM"), False) + "-" + _
                                oRaza.GetRazaByRazaCodigo(ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RazM"), False)).Trim() + _
                                " | HBA:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_hbam"), False) + _
                                " | RP:" + ValidarNulos(dtProducto.Rows(0).Item("prdt_compati_RpMd"), False)

                            End If


                        End If
                    End If

                Case 2
                        If pPadreAId = lstrPadre Then
                            If lstrSexo = 1 Then
                                pPadreA1 = lstrProd
                                pPadreA1Id = lstrId
                            Else
                                pMadreA1 = lstrProd
                                pMadreA1Id = lstrId
                            End If
                        Else
                            If lstrSexo = 1 Then
                                pPadreB1 = lstrProd
                                pPadreB1Id = lstrId
                            Else
                                pMadreB1 = lstrProd
                                pMadreB1Id = lstrId
                            End If
                        End If
                Case 3
                        Select Case True
                            Case pPadreA1Id = lstrPadre And (pPadreA21 Is Nothing And pPadreA21Id Is Nothing And lstrSexo = 1 Or pMadreA21 Is Nothing And pMadreA21Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreA21 = lstrProd
                                    pPadreA21Id = lstrId
                                Else
                                    pMadreA21 = lstrProd
                                    pMadreA21Id = lstrId
                                End If
                            Case pMadreA1Id = lstrPadre And (pPadreA22 Is Nothing And pPadreA22Id Is Nothing And lstrSexo = 1 Or pMadreA22 Is Nothing And pMadreA22Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreA22 = lstrProd
                                    pPadreA22Id = lstrId
                                Else
                                    pMadreA22 = lstrProd
                                    pMadreA22Id = lstrId
                                End If
                            Case pPadreB1Id = lstrPadre And (pPadreB11 Is Nothing And pPadreB11Id Is Nothing And lstrSexo = 1 Or pMadreB11 Is Nothing And pMadreB11Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreB11 = lstrProd
                                    pPadreB11Id = lstrId
                                Else
                                    pMadreB11 = lstrProd
                                    pMadreB11Id = lstrId
                                End If
                            Case pMadreB1Id = lstrPadre And (pPadreB12 Is Nothing And pPadreB12Id Is Nothing And lstrSexo = 1 Or pMadreB12 Is Nothing And pMadreB12Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreB12 = lstrProd
                                    pPadreB12Id = lstrId
                                Else
                                    pMadreB12 = lstrProd
                                    pMadreB12Id = lstrId
                                End If
                        End Select
                Case 4
                        Select Case True
                            Case pPadreA21Id = lstrPadre And (pPadreA211 Is Nothing And pPadreA211Id Is Nothing And lstrSexo = 1 Or pMadreA211 Is Nothing And pMadreA211Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreA211 = lstrProd
                                    pPadreA211Id = lstrId
                                Else
                                    pMadreA211 = lstrProd
                                    pMadreA211Id = lstrId
                                End If
                            Case pMadreA21Id = lstrPadre And (pPadreA212 Is Nothing And pPadreA212Id Is Nothing And lstrSexo = 1 Or pMadreA212 Is Nothing And pMadreA212Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreA212 = lstrProd
                                    pPadreA212Id = lstrId
                                Else
                                    pMadreA212 = lstrProd
                                    pMadreA212Id = lstrId
                                End If
                            Case pPadreA22Id = lstrPadre And (pPadreA221 Is Nothing And pPadreA221Id Is Nothing And lstrSexo = 1 Or pMadreA221 Is Nothing And pMadreA221Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreA221 = lstrProd
                                    pPadreA221Id = lstrId
                                Else
                                    pMadreA221 = lstrProd
                                    pMadreA221Id = lstrId
                                End If
                            Case pMadreA22Id = lstrPadre And (pPadreA222 Is Nothing And pPadreA222Id Is Nothing And lstrSexo = 1 Or pMadreA222 Is Nothing And pMadreA222Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreA222 = lstrProd
                                    pPadreA222Id = lstrId
                                Else
                                    pMadreA222 = lstrProd
                                    pMadreA222Id = lstrId
                                End If
                            Case pPadreB11Id = lstrPadre And (pPadreB111 Is Nothing And pPadreB111Id Is Nothing And lstrSexo = 1 Or pMadreB111 Is Nothing And pMadreB111Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreB111 = lstrProd
                                    pPadreB111Id = lstrId
                                Else
                                    pMadreB111 = lstrProd
                                    pMadreB111Id = lstrId
                                End If
                            Case pMadreB11Id = lstrPadre And (pPadreB112 Is Nothing And pPadreB112Id Is Nothing And lstrSexo = 1 Or pMadreB112 Is Nothing And pMadreB112Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreB112 = lstrProd
                                    pPadreB112Id = lstrId
                                Else
                                    pMadreB112 = lstrProd
                                    pMadreB112Id = lstrId
                                End If
                            Case pPadreB12Id = lstrPadre And (pPadreB121 Is Nothing And pPadreB121Id Is Nothing And lstrSexo = 1 Or pMadreB121 Is Nothing And pMadreB121Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreB121 = lstrProd
                                    pPadreB121Id = lstrId
                                Else
                                    pMadreB121 = lstrProd
                                    pMadreB121Id = lstrId
                                End If
                            Case pMadreB12Id = lstrPadre And (pPadreB122 Is Nothing And pPadreB122Id Is Nothing And lstrSexo = 1 Or pMadreB122 Is Nothing And pMadreB122Id Is Nothing And lstrSexo = 0)
                                If lstrSexo = 1 Then
                                    pPadreB122 = lstrProd
                                    pPadreB122Id = lstrId
                                Else
                                    pMadreB122 = lstrProd
                                    pMadreB122Id = lstrId
                                End If
                        End Select
            End Select
        End While
        dr.Close()

    End Function
#End Region


End Class
