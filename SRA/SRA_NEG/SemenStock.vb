Imports System.Data.SqlClient

Public Class SemenStock
    Inherits Generica


    Protected server As System.Web.HttpServerUtility

    
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
    End Sub

    Public Function GetSemenStockByTramiteId(ByVal TramiteId As String, ByVal pTableName As String) As DataTable
        Dim dt As New DataTable
        Dim mstrCmd As String
        dt.TableName = pTableName

        mstrCmd = "exec GetSemenStockByTramiteId "
        mstrCmd = mstrCmd + " @tram_id = " + clsSQLServer.gFormatArg(TramiteId, SqlDbType.VarChar)
        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetSaldoSemenStockByCriaIdPadres(ByVal pCria_Id As Long, _
ByVal ppadr_prdt_id As Long) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String

        mstrCmd = "exec GetSaldoSemenStockByCriaIdPadres "
        mstrCmd = mstrCmd + " @Cria_Id = " + clsSQLServer.gFormatArg(pCria_Id, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @padr_prdt_id = " + clsSQLServer.gFormatArg(ppadr_prdt_id, SqlDbType.VarChar)


        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function


    Public Function GetSaldoSemenStockByClieIdPadres(ByVal pClie_Id As Long, _
ByVal ppadr_prdt_id As Long) As DataTable

        Dim dt As New DataTable
        Dim mstrCmd As String

        mstrCmd = "exec GetSaldoSemenStockByClieIdPadres "
        mstrCmd = mstrCmd + " @Clie_Id = " + clsSQLServer.gFormatArg(pClie_Id, SqlDbType.VarChar)
        mstrCmd = mstrCmd + ", @padr_prdt_id = " + clsSQLServer.gFormatArg(ppadr_prdt_id, SqlDbType.VarChar)


        dt = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)

        Return dt

    End Function

    Public Function GetSaldosComprador(ByVal pCriaId As Long, ByVal pPadreId As Long) As DataTable

        Dim dtSemenStockComp As DataTable

        Dim oSemen As New SRA_Neg.SemenStock(mstrConn, Me.mstrUserId)
        dtSemenStockComp = oSemen.GetSaldoSemenStockByCriaIdPadres(pCriaId, pPadreId)


        Return dtSemenStockComp


    End Function

    Public Function GetSaldosVendedor(ByVal pCriaId As Long, ByVal pPadreId As Long, ByVal pMadreId As Long) As DataTable


        Dim dtEmbrionStockVend As DataTable
        Dim oEmbrion As New SRA_Neg.EmbrionStock(mstrConn, Me.mstrUserId)
        dtEmbrionStockVend = oEmbrion.GetSaldoEmbrionesStockByCriaIdPadres(pCriaId, pPadreId, pMadreId)

        Return dtEmbrionStockVend

    End Function




End Class
