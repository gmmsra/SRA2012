Imports ReglasValida.Validaciones
Public Class Especie
    Inherits Generica


    Protected mstrTablaAna As String
    Protected server As System.Web.HttpServerUtility

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pstrTablaAna As String, ByVal pdsDatos As DataSet)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId
        Me.mstrTabla = pstrTabla
        Me.mstrTablaAna = pstrTablaAna
        Me.mdsDatos = pdsDatos
        Me.server = server
    End Sub
    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String)
        Me.mstrConn = pstrConn
        Me.mstrUserId = pstrUserId

    End Sub



    Public Function GetEspecieDescripcionById(ByVal EspecieId As String) As String
        Dim dtEspecie As New DataTable
        Dim mstrCmd As String
        Dim EspecieDescripcion As String
        EspecieDescripcion = ""


        mstrCmd = "exec GetEspecieById "
        mstrCmd = mstrCmd + " @Espe_Id = " + clsSQLServer.gFormatArg(EspecieId, SqlDbType.VarChar)
        dtEspecie = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtEspecie.Rows.Count > 0 Then
            EspecieDescripcion = dtEspecie.Rows(0).Item("espe_desc")
        End If


        Return EspecieDescripcion


    End Function

    ' GSZ 12/12/2014 Se agrego Descripcion de especicie
    Public Function GetEspecieNomNumeById(ByVal EspecieId As String) As String
        Dim dtEspecie As New DataTable
        Dim mstrCmd As String
        Dim EspecieNombNume As String
        EspecieNombNume = ""


        mstrCmd = "exec GetEspecieById "
        mstrCmd = mstrCmd + " @Espe_id = " + clsSQLServer.gFormatArg(EspecieId, SqlDbType.VarChar)
        dtEspecie = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtEspecie.Rows.Count > 0 Then
            EspecieNombNume = dtEspecie.Rows(0).Item("espe_nomb_nume")
        End If


        Return EspecieNombNume


    End Function


    Public Function GetEspecieTipoHBADescByRazaId(ByVal RazaId As String) As String
        Dim dtEspecie As New DataTable
        Dim mstrCmd As String
        Dim EspecieNombNume As String
        EspecieNombNume = ""


        mstrCmd = "exec GetEspecieTipoHBADescByRazaId "
        mstrCmd = mstrCmd + " @raza_id = " + clsSQLServer.gFormatArg(RazaId, SqlDbType.VarChar)
        dtEspecie = clsSQLServer.gExecuteQuery(mstrConn, mstrCmd).Tables(0)


        If dtEspecie.Rows.Count > 0 Then
            EspecieNombNume = dtEspecie.Rows(0).Item("espe_nomb_nume")
        End If


        Return EspecieNombNume


    End Function









End Class