' Dario 2013-10-25 Sobrecarga con trasaccion
Imports WSAFIPFacElect

Public Class FacturacionABM
    Inherits GenericaRel

    Private Enum tablasDs As Integer
        mstrTablaDetaAran = 2

    End Enum
    ' Dario 2013-07-10 - sobrecarga del constructor base que acepta transaccion
    Public Sub New(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet)
        MyBase.New(lTransac, pstrUserId, pstrTabla, pdsDatos)
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String, ByVal pdsDatos As DataSet)
        MyBase.New(pstrConn, pstrUserId, pstrTabla, pdsDatos)
    End Sub

    Public Sub New(ByVal pstrConn As String, ByVal pstrUserId As String, ByVal pstrTabla As String)
        MyBase.New(pstrConn, pstrUserId, pstrTabla)
    End Sub

    Public Overloads Function Alta(ByVal pstrProformaId As String, ByVal pdsTurnos As DataSet) As String
        Return (mActualizar(pstrProformaId, pdsTurnos, ""))
    End Function
    ' Dario 2013-07-10 - sobrecarga del metodo para que  acepta transaccion 
    Public Overloads Function Alta(ByVal Transac As SqlClient.SqlTransaction, ByVal pstrProformaId As String, ByVal pdsTurnos As DataSet, ByVal pstrTablaTurnos As String) As String
        Return (mActualizar(Transac, pstrProformaId, pdsTurnos, pstrTablaTurnos))
    End Function

    Public Overloads Function Alta(ByVal pstrProformaId As String, ByVal pdsTurnos As DataSet, ByVal pstrTablaTurnos As String) As String
        Return (mActualizar(pstrProformaId, pdsTurnos, pstrTablaTurnos))
    End Function

    Public Overloads Function Baja(ByVal pstrId As String, ByVal pstrBaja As String, ByVal pstrPrfr As String) As String

        Dim lTransac As SqlClient.SqlTransaction = Nothing

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If Not mdsDatos Is Nothing Then
                For i As Integer = 0 To mdsDatos.Tables.Count - 1
                    For Each odrDeta As DataRow In mdsDatos.Tables(i).Rows
                        'VALIDA LA EXISTENCIA DEL PROCEDIMIENTO DE ACCION DE BAJA
                        If clsSQLServer.gExecuteScalar(lTransac, "existe_procedimiento_consul '" + mstrTabla + "_baja_accion'") = "1" Then
                            'BORRA ARCHIVOS ADJUNTOS DE LOS DETALLES SOLO CUANDO LA BAJA DE LA CABECERA SERA FISICA
                            If clsSQLServer.gExecuteScalar(lTransac, mstrTabla + "_baja_accion " + pstrId) = "0" Then
                                mBorrarArchivo(lTransac, odrDeta, mdsDatos.Tables(i).TableName, True)
                            End If
                        Else
                            mBorrarArchivo(lTransac, odrDeta, mdsDatos.Tables(i).TableName, True)
                        End If
                    Next
                Next
            End If
            'BORRA CABECERAS Y DETALLES
            Dim lstrCmd As String = mstrTabla & "_baja "
            lstrCmd = lstrCmd & " @comp_audi_user=" & mstrUserId
            lstrCmd = lstrCmd & ",@comp_id=" & pstrId
            lstrCmd = lstrCmd & ",@prfr_id=" & IIf(pstrPrfr = "", "null", pstrPrfr)
            lstrCmd = lstrCmd & ",@prfr_baja=" & IIf(pstrBaja = "S", 1, 0)
            clsSQLServer.gExecute(lTransac, lstrCmd)

            clsSQLServer.gCommitTransac(lTransac)

            Return (pstrId)
        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Function AltaProfMasi() As String
        ' Dim lstrTABLAId As String
        Dim lstrId, lstrCoanId, lstrCompIds As String
        lstrCompIds = Nothing
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String
        Dim ldrComp As DataRow
        Dim lDt As DataTable
        Dim lstrFiltro, lstrFiltroCoan As String  ', lstrFiltroCoad As String

        ' se usa para Facturaci�n masiva de proformas

        Try
            'lTransac = clsSQLServer.gObtenerTransac(mstrConn) Dario 2015-09-01

            For j As Integer = 0 To mdsDatos.Tables(0).Select().GetUpperBound(0)
                lTransac = clsSQLServer.gObtenerTransac(mstrConn)

                ldrComp = mdsDatos.Tables(0).Select()(j)

                lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, ldrComp)

                If lstrCompIds <> "" Then lstrCompIds += ","
                lstrCompIds += lstrId

                'Actualizo la relaci�n con las proformas
                For Each ldrRela As DataRow In mdsDatos.Tables(Constantes.gTab_Comprobantes + "_rela").Select("comp_id=" + ldrComp.Item("comp_id").ToString)
                    'ldrRela.Item("comp_id") = lstrId Dario 2015-09-01
                    clsSQLServer.gExecuteQuery(lTransac, "proformas_facturadas_masiva_modi  @prfr_id=" & ldrRela.Item("prfr_id").ToString & ", @comp_id=" & lstrId & ", @audi_user=" & mstrUserId)
                Next

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    lDt = mdsDatos.Tables(i)
                    lstrColPK = lDt.Columns(0).ColumnName
                    lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                    lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                    lstrFiltro = lstrColPK + "=" + ldrComp.Item(0).ToString

                    Select Case lDt.TableName
                        Case Constantes.gTab_ComprobAranc
                            For Each ldrCoan As DataRow In lDt.Select(lstrFiltro)
                                lstrFiltroCoan = lDt.Columns(0).ColumnName + "=" + ldrCoan.Item(0).ToString

                                lstrCoanId = ActualizarDetas(lTransac, Constantes.gTab_ComprobAranc, lstrColPK, lstrId, lstrFiltroCoan)

                                lstrFiltroCoan = "coad_coan_id=" + ldrCoan.Item(0).ToString
                                ActualizarDetas(lTransac, Constantes.gTab_ComprobArancDeta, "coad_coan_id", lstrCoanId, lstrFiltroCoan)
                            Next

                        Case Constantes.gTab_ComprobArancDeta, Constantes.gTab_Comprobantes + "_rela"
                            'no hace nada, se graba desde comprob_aran
                        Case SRA_Neg.Constantes.gTab_ComprobSobreTasaVtos
                            ' Dario 2013-07-19
                            ' si es la tabla de sobre tasas vencimientos no hacemos nada
                        Case "Table7"
                            ' Dario 2013-07-19
                            ' cuando desde alguna funcion no se le ponga nombre a la tabla de sobre tasas vtos sera tabla 7.
                            ' ojo con el agregado de nuevas tablas en el comprobantes consul
                            ' si es la tabla de sobre tasas vencimientos no hacemos nada

                        Case Else
                            ActualizarDetas(lTransac, lDt.TableName, lstrColPK, lstrId, lstrFiltro)
                    End Select
                Next

                'asiento( ND / NC)
                clsSQLServer.gExecuteQuery(lTransac, "dbo.facturacion_asientos   @comp_id=" & lstrId & ", @audi_user=" & mstrUserId)

                ' solo entra aca si es alta Fact Elect AFIP
                If (mstrTabla = SRA_Neg.Constantes.gTab_Comprobantes) Then
                    If (mdsDatos.Tables(0).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.ND _
                        Or mdsDatos.Tables(0).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.NC _
                        Or mdsDatos.Tables(0).Rows(0).Item("comp_coti_id") = Constantes.ComprobTipos.Factura) Then

                        Dim respuesta As String
                        Dim objWsAFIP As New WSFactElet
                        ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                        respuesta = WSFactElet.GetCAE(String.Empty, lstrId)
                        '                 0                1                2              3              4            5
                        ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                        '                  6                      7                8 
                        '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;

                        Dim respArray As Array = respuesta.Split("|"c)
                        ' si retorna ok 
                        If (respArray(0) = "A") Then
                            ' controlo que el comp id enviado es igual al recibido
                            Dim compIdresp As String = respArray(2)

                            Dim comp_cemi_nume As String = ""
                            Dim opcional1 As String = ""
                            Dim opcional2 As String = ""

                            Try
                                comp_cemi_nume = respArray(6)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional1 = respArray(7)
                            Catch ex As Exception

                            End Try

                            Try
                                opcional2 = respArray(8)
                            Catch ex As Exception

                            End Try

                            If (comp_cemi_nume.Trim().Length = 0) Then
                                comp_cemi_nume = "null"
                            End If

                            If (opcional1.Trim().Length = 0) Then
                                opcional1 = "null"
                            End If

                            If (opcional2.Trim().Length = 0) Then
                                opcional2 = "null"
                            End If

                            If (compIdresp.Trim() = lstrId.Trim()) Then
                                ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                                Dim compNumeresp As String = respArray(3)
                                'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp
                                Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp _
                                                   & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                                ' hago el update del @comp_nume de la tabla de comprobantes
                                clsError.gLoggear("GetCAE 4:: " + proc)
                                Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                                ' si no realizo el update error
                                If (intUpdateRest = 0) Then
                                    Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                                End If
                            Else
                                ' si los com id son diferentes error
                                Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrId & " * " & respuesta)
                            End If
                        Else
                            ' cualquier cosa fuera de ok error 
                            Throw New Exception(respuesta)
                        End If
                    End If
                End If

                clsSQLServer.gCommitTransac(lTransac)
            Next
            ' Dario 2015-09-01    
            'For Each ldrPr As DataRow In mdsDatos.Tables(Constantes.gTab_Comprobantes + "_rela").Select
            '    'proforma facturada
            '    clsSQLServer.gExecuteQuery(lTransac, "proformas_facturadas_masiva_modi  @prfr_id=" & ldrPr.Item("prfr_id").ToString & ", @comp_id=" & ldrPr.Item("comp_id").ToString & ", @audi_user=" & mstrUserId)
            'Next
            'clsSQLServer.gCommitTransac(lTransac)

            Return (lstrCompIds)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function
    ' Dario solo proformas no fact electronica
    Private Overloads Function mActualizar(ByVal pstrProformaId As String, ByVal pdsTurnos As DataSet, ByVal pstrTablaTurnos As String) As String
        'Dim lstrTABLAId As String
        Dim lstrId As String
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String
        Dim lboolProf As Boolean

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                Next
            End If

            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                Select Case mdsDatos.Tables(i).TableName
                    Case SRA_Neg.Constantes.gTab_ComprobAranc
                        'graba comprob_aranc
                        ActualizardetaAran(lTransac, mdsDatos.Tables(tablasDs.mstrTablaDetaAran).TableName, "coan_comp_id", lstrId, pdsTurnos, pstrTablaTurnos)

                    Case SRA_Neg.Constantes.gTab_ProformaAranc
                        'graba proforma_aranc
                        ActualizardetaAran(lTransac, mdsDatos.Tables(tablasDs.mstrTablaDetaAran).TableName, "pran_prfr_id", lstrId, pdsTurnos, pstrTablaTurnos)
                        lboolProf = True
                    Case SRA_Neg.Constantes.gTab_ComprobSobreTasaVtos
                        ' Dario 2013-07-19
                        ' si es la tabla de sobre tasas vencimientos no hacemos nada
                    Case "Table7"
                        ' Dario 2013-07-19
                        ' cuando desde alguna funcion no se le ponga nombre a la tabla de sobre tasas vtos sera tabla 7.
                        ' ojo con el agregado de nuevas tablas en el comprobantes consul
                        ' si es la tabla de sobre tasas vencimientos no hacemos nada
                    Case Else
                        lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                        lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                        lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName
                        ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)
                End Select
            Next

            'proforma
            If pstrProformaId <> "" Then  'proforma pendiente 
                If lboolProf Then
                    'proforma de proforma (se anula la pendiente)
                    clsSQLServer.gExecuteQuery(lTransac, "dbo.proformas_anuladas_reactivadas_modi  @prfr_anul_ids='" & pstrProformaId & "', @audi_user=" & mstrUserId)
                    'se generan los nros. de tramite
                    clsSQLServer.gExecuteQuery(lTransac, "dbo.rrgg_tramite_num  @id=" & lstrId & ", @Prof=1")
                Else
                    'proforma facturada
                    clsSQLServer.gExecuteQuery(lTransac, "proformas_facturadas_modi  @prfr_id=" & pstrProformaId & ", @comp_id=" & lstrId & ", @audi_user=" & mstrUserId)
                End If
            Else 'proforma
                'se generan los nros. de tramite
                clsSQLServer.gExecuteQuery(lTransac, "dbo.rrgg_tramite_num  @id=" & lstrId & ", @Prof=1")
            End If
            'asiento(Factura / ND / NC)
            If mdsDatos.Tables(0).TableName <> SRA_Neg.Constantes.gTab_Proformas Then
                clsSQLServer.gExecuteQuery(lTransac, "dbo.facturacion_asientos   @comp_id=" & lstrId & ", @audi_user=" & mstrUserId)
            End If

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Overloads Function AltaFacExt(Optional ByVal pbooNC As Boolean = False) As String
        'Dim lstrTABLAId As String
        Dim lstrId As String
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String
        ' se usa para NC/NC/Facturas procedentes de Facturacion externa

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, "comprobantesExternos", MiRow)
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                Next
            End If

            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                'solo para notas de credito
                If pbooNC And mdsDatos.Tables(i).TableName = SRA_Neg.Constantes.gTab_SaldosACuenta Then
                    For Each ldrSaldosACta As DataRow In mdsDatos.Tables(i).Select()
                        ldrSaldosACta.Item("sact_comp_id") = lstrId
                    Next
                End If

                ' Dario 2013-08-23 se validad que no se intente actualizar la tabla COMPROB_Sobre_Tasa_Vtos
                If (mdsDatos.Tables(i).TableName().ToUpper() <> "COMPROB_SOBRE_TASA_VTOS" _
                    And mdsDatos.Tables(i).TableName().ToUpper() <> "TABLE7") Then
                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)
                End If
            Next

            'asiento( ND / NC)
            clsSQLServer.gExecuteQuery(lTransac, "dbo.facturacion_asientos   @comp_id=" & lstrId & ", @audi_user=" & mstrUserId)

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Public Overloads Function Alta(Optional ByVal pbooNC As Boolean = False) As String
        'Dim lstrTABLAId As String
        Dim lstrId As String
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String
        Dim lboolAlta As Boolean = False
        ' se usa para NC/NC/Facturas procedentes de Facturacion externa

        Try
            lTransac = clsSQLServer.gObtenerTransac(mstrConn)

            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, mstrTabla, MiRow)
                lboolAlta = True
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(lTransac, mstrUserId, mstrTabla, MiRow)
                lstrId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(lTransac, mdsDatos.Tables(i).TableName)
                Next
            End If

            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                'solo para notas de credito
                If pbooNC And mdsDatos.Tables(i).TableName = SRA_Neg.Constantes.gTab_SaldosACuenta Then
                    For Each ldrSaldosACta As DataRow In mdsDatos.Tables(i).Select()
                        ldrSaldosACta.Item("sact_comp_id") = lstrId
                    Next
                End If

                ' Dario 2013-08-23 se validad que no se intente actualizar la tabla COMPROB_Sobre_Tasa_Vtos
                If (mdsDatos.Tables(i).TableName().ToUpper() <> "COMPROB_SOBRE_TASA_VTOS" _
                    And mdsDatos.Tables(i).TableName().ToUpper() <> "TABLE7") Then
                    ActualizarDetas(lTransac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)
                End If
            Next

            'asiento( ND / NC)
            clsSQLServer.gExecuteQuery(lTransac, "dbo.facturacion_asientos   @comp_id=" & lstrId & ", @audi_user=" & mstrUserId)

            ' solo entra aca si es alta Fact Elect AFIP
            If (lboolAlta) Then
                Dim respuesta As String
                'Dim objWsAFIP As WSFactElet
                ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                respuesta = WSFactElet.GetCAE(String.Empty, lstrId)
                '                 0                1                2              3              4            5
                ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                '                  6                      7                8 
                '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;

                Dim respArray As Array = respuesta.Split("|"c)
                ' si retorna ok 
                If (respArray(0) = "A") Then
                    ' controlo que el comp id enviado es igual al recibido
                    Dim compIdresp As String = respArray(2)

                    Dim comp_cemi_nume As String = ""
                    Dim opcional1 As String = ""
                    Dim opcional2 As String = ""

                    Try
                        comp_cemi_nume = respArray(6)
                    Catch ex As Exception

                    End Try

                    Try
                        opcional1 = respArray(7)
                    Catch ex As Exception

                    End Try

                    Try
                        opcional2 = respArray(8)
                    Catch ex As Exception

                    End Try

                    If (comp_cemi_nume.Trim().Length = 0) Then
                        comp_cemi_nume = "null"
                    End If

                    If (opcional1.Trim().Length = 0) Then
                        opcional1 = "null"
                    End If

                    If (opcional2.Trim().Length = 0) Then
                        opcional2 = "null"
                    End If

                    If (compIdresp.Trim() = lstrId.Trim()) Then
                        ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                        Dim compNumeresp As String = respArray(3)
                        'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp
                        Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp _
                                           & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                        ' hago el update del @comp_nume de la tabla de comprobantes
                        clsError.gLoggear("GetCAE 5:: " + proc)
                        Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(lTransac, proc)
                        ' si no realizo el update error
                        If (intUpdateRest = 0) Then
                            Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                        End If
                    Else
                        ' si los com id son diferentes error
                        Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrId & " * " & respuesta)
                    End If
                Else
                    ' cualquier cosa fuera de ok error 
                    Throw New Exception(respuesta)
                End If
            End If

            clsSQLServer.gCommitTransac(lTransac)

            Return (lstrId)

        Catch ex As Exception
            clsSQLServer.gRollbackTransac(lTransac)
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function
    ' Dario 2013-10-25 Sobrecarga con trasaccion
    ' para el alta de nota de NC
    Public Overloads Function Alta(ByVal Transac As SqlClient.SqlTransaction, Optional ByVal pbooNC As Boolean = False) As String
        Dim lstrTABLAId As String = Nothing
        Dim lstrId As String
        Dim lTransac As SqlClient.SqlTransaction = Nothing
        Dim lstrColPK As String
        Dim lboolAlta As Boolean = False
        ' se usa para NC/NC/Facturas procedentes de Facturacion externa

        Try
            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                lstrId = clsSQLServer.gAlta(Transac, mstrUserId, mstrTabla, MiRow)
                lboolAlta = True
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(Transac, mstrUserId, mstrTabla, MiRow)
                lstrId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(Transac, mdsDatos.Tables(i).TableName)
                Next
            End If

            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName

                'solo para notas de credito
                If pbooNC And mdsDatos.Tables(i).TableName = SRA_Neg.Constantes.gTab_SaldosACuenta Then
                    For Each ldrSaldosACta As DataRow In mdsDatos.Tables(i).Select()
                        ldrSaldosACta.Item("sact_comp_id") = lstrId
                    Next
                End If

                ' Dario 2013-08-23 se validad que no se intente actualizar la tabla COMPROB_Sobre_Tasa_Vtos
                If (mdsDatos.Tables(i).TableName().ToUpper() <> "COMPROB_SOBRE_TASA_VTOS" _
                    And mdsDatos.Tables(i).TableName().ToUpper() <> "TABLE7") Then
                    ActualizarDetas(Transac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)
                End If
            Next

            'asiento( ND / NC)
            clsSQLServer.gExecuteQuery(Transac, "dbo.facturacion_asientos   @comp_id=" & lstrId & ", @audi_user=" & mstrUserId)

            ' solo entra aca si es alta Fact Elect AFIP
            If (lboolAlta) Then
                Dim respuesta As String
                'Dim objWsAFIP As WSFactElet
                ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                respuesta = WSFactElet.GetCAE(String.Empty, lstrId)
                '                 0                1                2              3              4            5
                ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                '                  6                      7                8 
                '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                Dim respArray As Array = respuesta.Split("|"c)
                ' si retorna ok 
                If (respArray(0) = "A") Then
                    ' controlo que el comp id enviado es igual al recibido
                    Dim compIdresp As String = respArray(2)

                    Dim comp_cemi_nume As String = ""
                    Dim opcional1 As String = ""
                    Dim opcional2 As String = ""

                    Try
                        comp_cemi_nume = respArray(6)
                    Catch ex As Exception

                    End Try

                    Try
                        opcional1 = respArray(7)
                    Catch ex As Exception

                    End Try

                    Try
                        opcional2 = respArray(8)
                    Catch ex As Exception

                    End Try

                    If (comp_cemi_nume.Trim().Length = 0) Then
                        comp_cemi_nume = "null"
                    End If

                    If (opcional1.Trim().Length = 0) Then
                        opcional1 = "null"
                    End If

                    If (opcional2.Trim().Length = 0) Then
                        opcional2 = "null"
                    End If

                    If (compIdresp.Trim() = lstrId.Trim()) Then
                        ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                        Dim compNumeresp As String = respArray(3)
                        'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp
                        Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp _
                                           & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                        ' hago el update del @comp_nume de la tabla de comprobantes
                        clsError.gLoggear("GetCAE 6:: " + proc)
                        Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(Transac, proc)
                        ' si no realizo el update error
                        If (intUpdateRest = 0) Then
                            Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                        End If
                    Else
                        ' si los com id son diferentes error
                        Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrId & " * " & respuesta)
                    End If
                Else
                    ' cualquier cosa fuera de ok error 
                    Throw New Exception(respuesta)
                End If
            End If

            Return (lstrId)

        Catch ex As Exception
            Dim msg As String
            msg = "Error FacturacionABM.Alta(ByVal Transac As SqlClient.SqlTransaction, Optional ByVal pbooNC As Boolean = False)"
            clsError.gManejarError(msg, "", ex)
            Throw (ex)
        End Try
    End Function
    ' Dario 2013-07-10 - sobrecarga del metodo para que  acepta transaccion 
    Private Overloads Function mActualizar(ByVal Transac As SqlClient.SqlTransaction, ByVal pstrProformaId As String, ByVal pdsTurnos As DataSet, ByVal pstrTablaTurnos As String) As String
        Dim lstrTABLAId As String = Nothing
        Dim lstrId As String
        Dim lstrColPK As String
        Dim lboolProf As Boolean
        Dim lboolAlta As Boolean = False

        Try
            If CInt(MiRow.Item(0)) <= 0 Then
                'ALTA TABLA
                lstrId = clsSQLServer.gAlta(Transac, mstrUserId, mstrTabla, MiRow)
                lboolAlta = True
            Else
                'MODIFICACION TABLA
                clsSQLServer.gModi(Transac, mstrUserId, mstrTabla, MiRow)
                lstrId = MiRow.Item(0).ToString

                For i As Integer = 1 To mdsDatos.Tables.Count - 1
                    BorrarDetas(Transac, mdsDatos.Tables(i).TableName)
                Next
            End If

            For i As Integer = 1 To mdsDatos.Tables.Count - 1
                Select Case mdsDatos.Tables(i).TableName
                    Case SRA_Neg.Constantes.gTab_ComprobAranc
                        'graba comprob_aranc
                        ActualizardetaAran(Transac, mdsDatos.Tables(tablasDs.mstrTablaDetaAran).TableName, "coan_comp_id", lstrId, pdsTurnos, pstrTablaTurnos)
                    Case SRA_Neg.Constantes.gTab_ProformaAranc
                        'graba proforma_aranc
                        ActualizardetaAran(Transac, mdsDatos.Tables(tablasDs.mstrTablaDetaAran).TableName, "pran_prfr_id", lstrId, pdsTurnos, pstrTablaTurnos)
                        lboolProf = True
                    Case SRA_Neg.Constantes.gTab_ComprobSobreTasaVtos
                        ' Dario 2013-07-19
                        ' si es la tabla de sobre tasas vencimientos no hacemos nada
                    Case "Table7"
                        ' Dario 2013-07-19
                        ' cuando desde alguna funcion no se le ponga nombre a la tabla de sobre tasas vtos sera tabla 7.
                        ' ojo con el agregado de nuevas tablas en el comprobantes consul
                        ' si es la tabla de sobre tasas vencimientos no hacemos nada
                    Case Else
                        lstrColPK = mdsDatos.Tables(i).Columns(0).ColumnName
                        lstrColPK = Left(lstrColPK, Len(lstrColPK) - 2)
                        lstrColPK += mdsDatos.Tables(mstrTabla).Columns(0).ColumnName
                        ActualizarDetas(Transac, mdsDatos.Tables(i).TableName, lstrColPK, lstrId)
                End Select
            Next

            'proforma
            If pstrProformaId <> "" Then  'proforma pendiente 
                If lboolProf Then
                    'proforma de proforma (se anula la pendiente)
                    clsSQLServer.gExecuteQuery(Transac, "dbo.proformas_anuladas_reactivadas_modi  @prfr_anul_ids='" & pstrProformaId & "', @audi_user=" & mstrUserId)
                    'se generan los nros. de tramite
                    clsSQLServer.gExecuteQuery(Transac, "dbo.rrgg_tramite_num  @id=" & lstrId & ", @Prof=1")
                Else
                    'proforma facturada
                    clsSQLServer.gExecuteQuery(Transac, "proformas_facturadas_modi  @prfr_id=" & pstrProformaId & ", @comp_id=" & lstrId & ", @audi_user=" & mstrUserId)
                End If
            Else 'proforma
                'se generan los nros. de tramite
                clsSQLServer.gExecuteQuery(Transac, "dbo.rrgg_tramite_num  @id=" & lstrId & ", @Prof=1")
            End If
            'asiento(Factura / ND / NC)
            If mdsDatos.Tables(0).TableName <> SRA_Neg.Constantes.gTab_Proformas Then
                clsSQLServer.gExecuteQuery(Transac, "dbo.facturacion_asientos   @comp_id=" & lstrId & ", @audi_user=" & mstrUserId)
            End If

            ' solo entra aca si es alta Fact Elect AFIP
            If (lboolAlta And mstrTabla = SRA_Neg.Constantes.gTab_Comprobantes) Then
                Dim respuesta As String
                ' Dim objWsAFIP As WSFactElet
                ' Solicito el cae y si todo sale ok la afip me retorna el nro de comprobante y el cae
                respuesta = WSFactElet.GetCAE(String.Empty, lstrId)
                '                 0                1                2              3              4            5
                ' respuesta += Resultado + "|" + Empresa + "|" + comp_id + "|" + nrocomp + "|" + CAE + "|" + CAEFchVto + "|" 
                '                  6                      7                8 
                '            + comp_cemi_nume + "|" + opcional1 + "|" + opcional2;
                Dim respArray As Array = respuesta.Split("|"c)
                ' si retorna ok 
                If (respArray(0) = "A") Then
                    ' controlo que el comp id enviado es igual al recibido
                    Dim compIdresp As String = respArray(2)

                    Dim comp_cemi_nume As String = ""
                    Dim opcional1 As String = ""
                    Dim opcional2 As String = ""

                    Try
                        comp_cemi_nume = respArray(6)
                    Catch ex As Exception

                    End Try

                    Try
                        opcional1 = respArray(7)
                    Catch ex As Exception

                    End Try

                    Try
                        opcional2 = respArray(8)
                    Catch ex As Exception

                    End Try

                    If (comp_cemi_nume.Trim().Length = 0) Then
                        comp_cemi_nume = "null"
                    End If

                    If (opcional1.Trim().Length = 0) Then
                        opcional1 = "null"
                    End If

                    If (opcional2.Trim().Length = 0) Then
                        opcional2 = "null"
                    End If

                    If (compIdresp.Trim() = lstrId.Trim()) Then
                        ' obtengo el nro de comprobante que la afip envio para ese puunto de venta y tipo de doc
                        Dim compNumeresp As String = respArray(3)
                        'Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp
                        Dim proc As String = "comprobantes_asigna_comp_nume @comp_id=" & lstrId & " ,@comp_nume=" & compNumeresp _
                                           & " ,@comp_cemi_nume=" & comp_cemi_nume & " ,@opcional1= " & opcional1 & " ,@opcional2= " & opcional2
                        ' hago el update del @comp_nume de la tabla de comprobantes
                        clsError.gLoggear("GetCAE 7:: " + proc)
                        Dim intUpdateRest As Int32 = clsSQLServer.gExecuteNONQuery(Transac, proc)
                        ' si no realizo el update error
                        If (intUpdateRest = 0) Then
                            Throw New Exception("Error: no Update:: " & proc & " * " & respuesta)
                        End If
                    Else
                        ' si los com id son diferentes error
                        Throw New Exception("Error compIdresp != lstrId *" & compIdresp & "!=" & lstrId & " * " & respuesta)
                    End If
                Else
                    ' cualquier cosa fuera de ok error 
                    Throw New Exception(respuesta)
                End If
            End If

            Return (lstrId)

        Catch ex As Exception
            clsError.gManejarError(ex)
            Throw (ex)
        End Try
    End Function

    Private Sub ActualizardetaAran(ByVal lTransac As SqlClient.SqlTransaction, ByVal pstrTablaDet As String, ByVal pstrCampoPK As String, ByVal lintCabId As Integer, ByRef pdsTurnos As DataSet, ByVal pstrTabla As String)
        Dim lstrId As String = Nothing
        Dim odrTurno As DataRow
        Dim lstrPref As String = "coad"
        Dim lstrPrefCab As String = "coan"
        If pstrTablaDet = SRA_Neg.Constantes.gTab_ProformaArancDeta Or pstrTablaDet = SRA_Neg.Constantes.gTab_ProformaAranc Then
            lstrPref = "prad"
            lstrPrefCab = "pran"
        End If
        For Each odrDeta As DataRow In mdsDatos.Tables(pstrTablaDet).Select("")
            If odrDeta.Item(0) <= 0 Then
                odrDeta.Item(pstrCampoPK) = lintCabId
                'ALTA 
                lstrId = clsSQLServer.gAlta(lTransac, mstrUserId, pstrTablaDet, odrDeta)
            Else
                'MODIFICACION 
                clsSQLServer.gModi(lTransac, mstrUserId, pstrTablaDet, odrDeta)
            End If
            'alta de turnos 
            If pdsTurnos.Tables(0).Rows.Count > 0 Then
                odrTurno = pdsTurnos.Tables(0).Select(lstrPref + "_" + lstrPrefCab + "_id=" & odrDeta.Item(lstrPrefCab + "_id"))(0)
                If lstrPref = "coad" Then
                    'factura (detalle de turnos)
                    odrTurno.Item("coad_coan_id") = lstrId
                Else
                    'proforma (detalle de turnos)
                    odrTurno.Item("prad_pran_id") = lstrId
                End If
                'ALTA 
                clsSQLServer.gAlta(lTransac, mstrUserId, pstrTabla, odrTurno)
                pdsTurnos.Tables(0).Rows.Remove(pdsTurnos.Tables(0).Select(lstrPref + "_" + lstrPrefCab + "_id=" & lstrId)(0))
            End If

        Next
    End Sub
End Class