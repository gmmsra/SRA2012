﻿using Data;
using Helper;
using MailHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigradorAsocExpo
{
    class Program
    {
        public static string conStr = ConfigurationManager.ConnectionStrings["SRAConn"].ConnectionString;
        public static string origenPathReader = ConfigurationManager.AppSettings ["path"].ToString();
        public static char splitSeparator = Convert.ToChar(ConfigurationManager.AppSettings["SplitSeparator"]);

        public static int cantidadregistros = 0;

        static void Main(string[] args)
        {
            Console.WriteLine("INICIO ----------------------------------");
            Logger.info(Logger.Log.Info, new string[] { "INICIO", " ------------------------ " });
            Console.WriteLine("BD:- " + conStr);
            Logger.info(Logger.Log.Info, new string[] { "DB:", "- ", conStr });
            Console.WriteLine("origenPathReader:- " + origenPathReader);
            Logger.info(Logger.Log.Info, new string[] { "Path:", "- ", origenPathReader });

            Console.WriteLine("Ingresa el paso en el cual ician el ingreso de animales");
            string paso = Console.ReadLine();
            if (paso.Trim().Length == 0)
                paso = "0";

            LeerArchivo(paso);

            Console.WriteLine("Cant Proc:-");
            Logger.info(Logger.Log.Info, new string[] { "Cant Proc:", "- ", cantidadregistros.ToString() });
            Console.WriteLine("FIN ----------------------------------");
            Logger.info(Logger.Log.Info, new string[] { "FIN", " ------------------------ " });

            Console.ReadLine();
              
            /*
              MailManager mail = new MailManager();
                    mail.VarEmail = "dvasqueznigro@sra.org.ar";
                    mail.varDisplayName = "ERROR integracion Finnegas";
                    mail.EnviaMensaje("ERROR integracion Finnegans se ejecuto con error:-"
                                     , "la integracion Finnegans se ejecuto con error:- " + "status:" + token.status + ",error:" + token.error);
             */
        }

        public static void LeerArchivo(string paso)
        {
            cantidadregistros = 0;
            StreamReader objReader = new StreamReader(origenPathReader);

            var sLineaTitulos = objReader.ReadLine().Split(splitSeparator);

            while (!objReader.EndOfStream ) // && sLineaLeida.Length > 5)
            {
                string[] sLineaLeida = objReader.ReadLine().Split(splitSeparator);
                cantidadregistros++;

                StringBuilder pstrProc = new StringBuilder();
                try
                {
                    pstrProc.Append("insert into migracion_animalesExpo (LC,Raza,nroAsoc,nroIncrip,Registro,RP,Sexo,fhNac,Color,Prefijo,Nombre,Apodo,fhInspec");
                    pstrProc.Append(",inspector,ResuInsp,Analisis,RazaPad,nroIncripPad,nroAsocPad,asociPad,nroImpPad,RPPadre,RazaMad,nroIncripMad,nroAsocMad");
                    pstrProc.Append(",asociMad,nroImpMad,RPMadre,fhServicio,TipoServicio,prdt_id,paso) VALUES (");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.LC)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.LC)]  : "null");
                    pstrProc.Append(",");
                    pstrProc.Append(sLineaLeida[Convert.ToInt16(colcsv.Raza)]);
                    pstrProc.Append(",");
                    pstrProc.Append(sLineaLeida[Convert.ToInt16(colcsv.nroAsoc)]);
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.nroIncrip)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.nroIncrip)] + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.Registro)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.Registro)] + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.RP)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.RP)] + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.Sexo)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.Sexo)] + "'" : "null");
                    pstrProc.Append(",");
                    DateTime? fechaNaci = null;
                    if (sLineaLeida[Convert.ToInt16(colcsv.fhNac)].Trim().Length > 0)
                    {
                        fechaNaci = Convert.ToDateTime(sLineaLeida[Convert.ToInt16(colcsv.fhNac)]);
                        string feNaci = ((DateTime)fechaNaci).ToString("yyyyMMdd");
                        if (feNaci != "00010101")
                            pstrProc.Append("'" + ((DateTime)fechaNaci).ToString("yyyyMMdd") + "'");
                        else
                            pstrProc.Append("null");
                    }
                    else
                    {
                        pstrProc.Append("null");
                    }
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.Color)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.Color)] + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.Prefijo)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.Prefijo)] + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.Nombre)].Trim().Length > 0) ? "'" + (sLineaLeida[Convert.ToInt16(colcsv.Nombre)]).Replace('\'',' ') + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.Apodo)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.Apodo)] + "'" : "null");
                    pstrProc.Append(",");
                    DateTime? fhInspec = null;
                    if (sLineaLeida[Convert.ToInt16(colcsv.fhInspec)].Trim().Length > 0)
                    {
                        fhInspec = Convert.ToDateTime(sLineaLeida[Convert.ToInt16(colcsv.fhInspec)]);
                        string feInspec = ((DateTime)fhInspec).ToString("yyyyMMdd");
                        if (feInspec != "00010101")
                            pstrProc.Append("'" + ((DateTime)fhInspec).ToString("yyyyMMdd") + "'");
                        else
                            pstrProc.Append("null");
                    }
                    else
                    {
                        fechaNaci = null;
                        pstrProc.Append("null");
                    }
                    pstrProc.Append(",");
                    //padre
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.inspector)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.inspector)] + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.ResuInsp)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.ResuInsp)] + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.Analisis)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.Analisis)] + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.RazaPad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.RazaPad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.nroIncripPad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.nroIncripPad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.nroAsocPad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.nroAsocPad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.asociPad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.asociPad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.nroImpPad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.nroImpPad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.RPPadre)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.RPPadre)] + "'" : "null");
                    pstrProc.Append(",");
                    //madre
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.RazaMad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.RazaMad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.nroIncripMad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.nroIncripMad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.nroAsocMad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.nroAsocMad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.asociMad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.asociMad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.nroImpMad)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.nroImpMad)] : "null");
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.RPMadre)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.RPMadre)] + "'" : "null");
                    pstrProc.Append(",");
                    //
                    DateTime? fhServicio = null;
                    if (sLineaLeida[Convert.ToInt16(colcsv.fhServicio)].Trim().Length > 0)
                    {
                        fhServicio = Convert.ToDateTime(sLineaLeida[Convert.ToInt16(colcsv.fhServicio)]);
                        string feServicio = ((DateTime)fhServicio).ToString("yyyyMMdd");
                        if (feServicio != "00010101")
                            pstrProc.Append("'" + ((DateTime)fhServicio).ToString("yyyyMMdd") + "'");
                        else
                            pstrProc.Append("null");
                    }
                    else
                    {
                        fechaNaci = null;
                        pstrProc.Append("null");
                    }
                    pstrProc.Append(",");
                    pstrProc.Append((sLineaLeida[Convert.ToInt16(colcsv.TipoServicio)].Trim().Length > 0) ? "'" + sLineaLeida[Convert.ToInt16(colcsv.TipoServicio)] + "'" : "null");
                    pstrProc.Append(",");
                    pstrProc.Append("null");
                    pstrProc.Append(",");
                    //pstrProc.Append(0);
                    pstrProc.Append(paso);
                    
                    //fin
                    pstrProc.Append(");");


                    //string[] Msg = { "MigradorAsocExpo.LeerArchivo;", pstrProc.ToString() };
                    //Logger.info(Logger.Log.Info, Msg);

                    DataSet ds = DataBase.gExecuteQuery(conStr, pstrProc.ToString(), 9999);
                    //Console.Clear();
                    Console.WriteLine("Cant:-" + cantidadregistros.ToString());
                }
                catch (Exception ex)
                {
                    string[] MsgError = { "MigradorAsocExpo.LeerArchivo:  ", pstrProc.ToString(), " registro el siguiente error: " + ex.Message };
                    Logger.error(Logger.Log.Error, MsgError);
                    Console.WriteLine("Error: linea:-" + cantidadregistros.ToString() + MsgError[2]);
                }
                // lee siguiente linea
                //sLineaLeida = objReader.ReadLine().Split(splitSeparator);
            }
            objReader.Close();
            //origenPathReader

        }


        #region codigo no se usa
        /*
        private static void MarcaEnvioFinnegansEnNixor(string asec_id, string asec_mesage_envio_FN)
        {
            StringBuilder pstrProc = new StringBuilder();

            pstrProc.Append("exec p_CON_Finnegans_Asientos_Resp_Envio ");

            pstrProc.Append("@asec_id=");
            pstrProc.Append(asec_id);
            pstrProc.Append(",@asec_mesage_envio_FN=");
            if (asec_mesage_envio_FN.Trim().Length > 0) { pstrProc.Append(DataBase.darFormatoSQl(asec_mesage_envio_FN)); } else { pstrProc.Append("null"); }

            try
            {
                string[] Msg = { "MigraAsientosAFinnegas.MarcaEnvioFinnegansEnNixor;", pstrProc.ToString() };
                Logger.info(Logger.Log.Info, Msg);

                DataSet ds = DataBase.gExecuteQuery(conStr, pstrProc.ToString(), 9999);
                
                //if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
                //{
                    
                //}
                
            }
            catch (Exception ex)
            {
                string[] MsgError = { "MigraAsientosAFinnegas.MarcaEnvioFinnegansEnNixor:  ", pstrProc.ToString(), " registro el siguiente error: " + ex.Message };
                Logger.error(Logger.Log.Error, MsgError);

                string Message = MsgError[0] + " " + MsgError[1] + " " + MsgError[2];

                (new EmailSenderBusiness()).EnviarEmailATecnologia(TiposEmail.Error, pstrProc.ToString(), Message, "MigraAsientosAFinnegas.MarcaEnvioFinnegansEnNixor");
            }
        }
        */
        #endregion
    }

    public enum colcsv
    {
        LC = 0,
        Raza=1,
        nroAsoc=2,
        nroIncrip=3,
        Registro=4,
        RP=5,
        Sexo=6,
        fhNac=7,
        Color=8,
        Prefijo=9,
        Nombre=10,
        Apodo=11,
        fhInspec=12,
        inspector=13,
        ResuInsp=14,
        Analisis=15,
        RazaPad=16,
        nroIncripPad=17,
        nroAsocPad=18,
        asociPad=19,
        nroImpPad=20,
        RPPadre=21,
        RazaMad=22,
        nroIncripMad=23,
        nroAsocMad=24,
        asociMad=25,
        nroImpMad=26,
        RPMadre=27,
        fhServicio=28,
        TipoServicio=29,
        prdt_id=30,
        paso=31,
        chip=32
    }
}
