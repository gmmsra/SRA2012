﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Data
{
    public class DataBase
    {
        public static DataTable ExecuteQueryUsingDataReader(string pstrConn, string pstrProc, IEnumerable<SqlParameter> sqlParameters, int timeOut)
        {
            if (pstrConn.Length == 0)
            {
                throw new ArgumentNullException("pstrConn");
            }

            if (pstrProc.Length == 0)
            {
                throw new ArgumentNullException("pstrProc");
            }

            using (SqlConnection connection = new SqlConnection(pstrConn))
            {
                using (SqlCommand cmdExecCommand = new SqlCommand(pstrProc, connection))
                {
                    cmdExecCommand.CommandTimeout = timeOut;
                    cmdExecCommand.CommandType = CommandType.StoredProcedure;

                    if (sqlParameters != null)
                    {
                        cmdExecCommand.Parameters.AddRange(sqlParameters.ToArray());
                    }

                    connection.Open();

                    var reader = cmdExecCommand.ExecuteReader();
                    var dataTable = new DataTable();

                    dataTable.Load(reader);

                    return dataTable;
                }
            }
        }

        public static DataSet ExecuteQuery(string pstrConn, string pstrProc, IEnumerable<SqlParameter> sqlParameters, int timeOut, CommandType commandType = CommandType.StoredProcedure)
        {
            if (pstrConn.Length == 0)
            {
                throw new ArgumentNullException("pstrConn");
            }

            if (pstrProc.Length == 0)
            {
                throw new ArgumentNullException("pstrProc");
            }

            using (SqlConnection connection = new SqlConnection(pstrConn))
            {
                using (SqlCommand cmdExecCommand = new SqlCommand(pstrProc, connection))
                {
                    cmdExecCommand.CommandTimeout = timeOut;
                    cmdExecCommand.CommandType = commandType;

                    if (sqlParameters != null)
                    {
                        cmdExecCommand.Parameters.AddRange(sqlParameters.ToArray());
                    }

                    using (SqlDataAdapter dataAdapter = new SqlDataAdapter(cmdExecCommand))
                    {
                        var dataSet = new DataSet();

                        dataAdapter.Fill(dataSet);

                        return dataSet;
                    }
                }
            }
        }

        public static object ExecuteScalar(string pstrConn, string pstrProc, IEnumerable<SqlParameter> sqlParameters, int timeOut, CommandType commandType = CommandType.StoredProcedure)
        {
            if (pstrConn.Length == 0)
            {
                throw new ArgumentNullException("pstrConn");
            }

            if (pstrProc.Length == 0)
            {
                throw new ArgumentNullException("pstrProc");
            }

            using (SqlConnection connection = new SqlConnection(pstrConn))
            {
                using (SqlCommand cmdExecCommand = new SqlCommand(pstrProc, connection))
                {
                    cmdExecCommand.CommandTimeout = timeOut;
                    cmdExecCommand.CommandType = commandType;

                    if (sqlParameters != null)
                    {
                        cmdExecCommand.Parameters.AddRange(sqlParameters.ToArray());
                    }

                    connection.Open();

                    var data = cmdExecCommand.ExecuteScalar();

                    return data;
                }
            }
        }

        public static object ExecuteFunction(string pstrConn, string udfName, IEnumerable<SqlParameter> sqlParameters, int timeOut)
        {
            if (pstrConn.Length == 0)
            {
                throw new ArgumentNullException("pstrConn");
            }

            if (udfName.Length == 0)
            {
                throw new ArgumentNullException("pstrProc");
            }

            using (SqlConnection connection = new SqlConnection(pstrConn))
            {
                var command = "SELECT dbo.{udfName}(";

                foreach (var p in sqlParameters)
                {
                    command += "{p.ParameterName},";
                }

                command = command.Remove(command.LastIndexOf(","), 1) + ")";

                using (SqlCommand cmdExecCommand = new SqlCommand(command, connection))
                {
                    cmdExecCommand.CommandTimeout = timeOut;
                    cmdExecCommand.CommandType = CommandType.Text;

                    if (sqlParameters != null)
                    {
                        cmdExecCommand.Parameters.AddRange(sqlParameters.ToArray());
                    }

                    connection.Open();

                    var data = cmdExecCommand.ExecuteScalar();

                    return data;
                }
            }
        }

        public static int ExecuteNonQuery(string pstrConn, string pstrProc, int timeOut)
        {
            if (pstrConn.Length == 0)
            {
                throw new ArgumentNullException("pstrConn");
            }

            if (pstrProc.Length == 0)
            {
                throw new ArgumentNullException("pstrProc");
            }

            using (SqlConnection connection = new SqlConnection(pstrConn))
            {
                connection.Open();

                using (SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        using (SqlCommand cmdExecCommand = new SqlCommand(pstrProc, connection, transaction))
                        {
                            cmdExecCommand.CommandTimeout = timeOut;
                            cmdExecCommand.CommandType = CommandType.Text;

                            var rowsAffected = cmdExecCommand.ExecuteNonQuery();

                            transaction.Commit();

                            return rowsAffected;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public static int ExecuteNonQuery(string pstrConn, string pstrProc, IEnumerable<SqlParameter> sqlParameters, int timeOut)
        {
            if (pstrConn.Length == 0)
            {
                throw new ArgumentNullException("pstrConn");
            }

            if (pstrProc.Length == 0)
            {
                throw new ArgumentNullException("pstrProc");
            }

            using (SqlConnection connection = new SqlConnection(pstrConn))
            {
                connection.Open();

                using (SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    try
                    {
                        using (SqlCommand cmdExecCommand = new SqlCommand(pstrProc, connection, transaction))
                        {
                            cmdExecCommand.CommandTimeout = timeOut;
                            cmdExecCommand.CommandType = CommandType.StoredProcedure;

                            if (sqlParameters != null)
                            {
                                cmdExecCommand.Parameters.AddRange(sqlParameters.ToArray());
                            }

                            var rowsAffected = cmdExecCommand.ExecuteNonQuery();

                            transaction.Commit();

                            return rowsAffected;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public static DataSet gExecuteQuery(string pstrConn, string pstrProc)
        {
            try
            {
                if (pstrConn.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrConn");
                }
                if (pstrProc.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrProc");
                }

                SqlDataAdapter cmdExecCommand = new SqlDataAdapter(pstrProc, pstrConn);
                DataSet ds = new DataSet();
                cmdExecCommand.Fill(ds);

                return (ds);
            }
            catch (Exception ex)
            {
                //Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }

        public static DataSet gExecuteQuery(string pstrConn, string pstrProc, int pintTimeOut)
        {
            try
            {
                if (pstrConn.Length == 0)
                {
                    throw new ArgumentNullException("pstrConn");
                }

                if (pstrProc.Length == 0)
                {
                    throw new ArgumentNullException("pstrProc");
                }

                var dataSet = new DataSet();

                using (SqlConnection connection = new SqlConnection(pstrConn))
                {
                    using (SqlCommand cmdExecCommand = new SqlCommand(pstrProc, connection))
                    {
                        cmdExecCommand.CommandTimeout = pintTimeOut;
                        cmdExecCommand.CommandType = CommandType.Text;

                        using (SqlDataAdapter dataAdapter = new SqlDataAdapter(cmdExecCommand))
                        {
                            dataAdapter.Fill(dataSet);
                        }
                    }
                }

                return dataSet;
            }
            catch (Exception ex)
            {
                ////Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }

        //public static DataSet gExecuteQuery(string pstrConn, string pstrProc, int pintTimeOut)
        //{
        //    try
        //    {
        //        if (pstrConn.Length == 0)
        //        {
        //            ArgumentNullException Throw = new ArgumentNullException("pstrConn");
        //        }
        //        if (pstrProc.Length == 0)
        //        {
        //            ArgumentNullException Throw = new ArgumentNullException("pstrProc");
        //        }

        //        SqlDataAdapter cmdExecCommand = new SqlDataAdapter(pstrProc, pstrConn);
        //        if (cmdExecCommand.SelectCommand != null)
        //        { cmdExecCommand.SelectCommand.CommandTimeout = pintTimeOut; }
        //        if (cmdExecCommand.DeleteCommand != null)
        //        { cmdExecCommand.DeleteCommand.CommandTimeout = pintTimeOut; }
        //        if (cmdExecCommand.UpdateCommand != null)
        //        { cmdExecCommand.UpdateCommand.CommandTimeout = pintTimeOut; }
        //        if (cmdExecCommand.InsertCommand != null)
        //        { cmdExecCommand.InsertCommand.CommandTimeout = pintTimeOut; }


        //        DataSet ds = new DataSet();
        //        cmdExecCommand.Fill(ds);

        //        return (ds);
        //    }
        //    catch (Exception ex)
        //    {
        //        ////Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
        //        throw (ex);
        //    }
        //}

        public static DataSet gExecuteQuery(SqlTransaction Trans, string pstrProc)
        {
            if (Trans == null)
            {
                ArgumentNullException Throw = new ArgumentNullException("Trans");
            }

            //SqlConnection myConnection = new SqlConnection(pstrConn);
            SqlDataAdapter cmdExecCommand = new SqlDataAdapter(pstrProc, Trans.Connection);
            cmdExecCommand.SelectCommand.Transaction = Trans;
            DataSet ds = new DataSet();

            //myConnection.Open();
            cmdExecCommand.Fill(ds);
            //myConnection.Close();

            return (ds);
        }


        /// <summary>
        /// sobrecarga con timeout
        /// </summary>
        /// <param name="Trans"></param>
        /// <param name="pstrProc"></param>
        /// <returns>DataSet</returns>
        public static DataSet gExecuteQuery(SqlTransaction Trans, string pstrProc, int pintTimeOut)
        {
            if (Trans == null)
            {
                ArgumentNullException Throw = new ArgumentNullException("Trans");
            }
            //SqlConnection myConnection = new SqlConnection(pstrConn);
            SqlDataAdapter cmdExecCommand = new SqlDataAdapter(pstrProc, Trans.Connection);
            cmdExecCommand.SelectCommand.Transaction = Trans;
            cmdExecCommand.SelectCommand.CommandTimeout = pintTimeOut;
            DataSet ds = new DataSet();

            //myConnection.Open();
            cmdExecCommand.Fill(ds);
            //myConnection.Close();

            return (ds);
        }


        public static SqlDataReader gExecuteQueryDR(string pstrConn, string pstrProc)
        {
            try
            {
                SqlConnection myConnection = new SqlConnection(pstrConn);
                SqlCommand cmdExec = new SqlCommand();
                myConnection.Open();
                cmdExec.Connection = myConnection;
                cmdExec.CommandType = CommandType.Text;
                cmdExec.CommandText = pstrProc;
                SqlDataReader dr = cmdExec.ExecuteReader(CommandBehavior.CloseConnection);

                return (dr);
            }
            catch (Exception ex)
            {
                //Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }

        public static int gExecute(string pstrConn, string pstrProc)
        {
            SqlTransaction Trans = null;
            try
            {
                if (pstrConn.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrConn");
                }
                if (pstrProc.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrProc");
                }

                SqlConnection myConnection = new SqlConnection(pstrConn);
                SqlCommand cmdExecCommand = new SqlCommand();
                cmdExecCommand.Connection = myConnection;
                cmdExecCommand.CommandText = pstrProc;
                cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
                myConnection.Open();
                Trans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted, "NixorTransac");
                cmdExecCommand.Transaction = Trans;
                int lintExec = cmdExecCommand.ExecuteNonQuery();
                Trans.Commit();
                myConnection.Close();
                return (lintExec);
            }
            catch (Exception ex)
            {
                if (Trans != null) Trans.Rollback();
                //Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }

        public static int gExecuteNONQuery(SqlTransaction Trans, string pstrProc)
        {
            try
            {

                SqlDataAdapter cmdAdapter = new SqlDataAdapter(pstrProc, Trans.Connection);

                cmdAdapter.SelectCommand.Transaction = Trans;
                DataSet ds = new DataSet();
                cmdAdapter.Fill(ds);

                int lintExec = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    lintExec = int.Parse(ds.Tables[0].Rows[0][0].ToString());

                }

                return (lintExec);
            }
            catch (Exception ex)
            {
                //Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }

        public static int gExecuteNONQuery(string pstrConn, string pstrProc)
        {
            SqlTransaction Trans = null;
            try
            {
                if (pstrConn.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrConn");
                }
                if (pstrProc.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrProc");
                }
                SqlConnection myConnection = new SqlConnection(pstrConn);
                SqlCommand cmdExecCommand = new SqlCommand();
                cmdExecCommand.Connection = myConnection;
                cmdExecCommand.CommandText = pstrProc;
                cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
                myConnection.Open();
                Trans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted, "NixorTransac");
                cmdExecCommand.Transaction = Trans;

                SqlDataAdapter cmdAdapter = new SqlDataAdapter(pstrProc, Trans.Connection);

                cmdAdapter.SelectCommand.Transaction = Trans;
                DataSet ds = new DataSet();
                cmdAdapter.Fill(ds);

                int lintExec = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    lintExec = int.Parse(ds.Tables[0].Rows[0][0].ToString());

                }
                Trans.Commit();
                myConnection.Close();
                return (lintExec);
            }
            catch (Exception ex)
            {
                if (Trans != null) Trans.Rollback();
                //Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }


        public static int gExecuteSinTransac(string pstrConn, string pstrProc)
        {
            try
            {
                if (pstrConn.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrConn");
                }
                if (pstrProc.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrProc");
                }

                SqlConnection myConnection = new SqlConnection(pstrConn);
                SqlCommand cmdExecCommand = new SqlCommand();
                cmdExecCommand.Connection = myConnection;
                cmdExecCommand.CommandText = pstrProc;
                cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
                myConnection.Open();
                int lintExec = cmdExecCommand.ExecuteNonQuery();
                myConnection.Close();
                return (lintExec);
            }
            catch (Exception ex)
            {
                //Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }
        public static object gExecuteScalar(string pstrConn, string pstrProc)
        {
            if (pstrConn.Length == 0)
            {
                ArgumentNullException Throw = new ArgumentNullException("pstrConn");
            }
            if (pstrProc.Length == 0)
            {
                ArgumentNullException Throw = new ArgumentNullException("pstrProc");
            }
            SqlConnection myConnection = new SqlConnection(pstrConn);
            SqlCommand cmdExecCommand = new SqlCommand();
            cmdExecCommand.Connection = myConnection;
            cmdExecCommand.CommandText = pstrProc;
            cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
            myConnection.Open();
            object lobjRet = cmdExecCommand.ExecuteScalar();
            myConnection.Close();
            return (lobjRet);
        }

        public static string gExecuteScalarTrans(string pstrConn, string pstrProc)
        {
            SqlTransaction Trans = null;
            try
            {
                if (pstrConn.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrConn");
                }
                if (pstrProc.Length == 0)
                {
                    ArgumentNullException Throw = new ArgumentNullException("pstrProc");
                }
                SqlConnection myConnection = new SqlConnection(pstrConn);
                SqlCommand cmdExecCommand = new SqlCommand();
                cmdExecCommand.Connection = myConnection;
                cmdExecCommand.CommandText = pstrProc;
                cmdExecCommand.CommandTimeout = myConnection.ConnectionTimeout;
                myConnection.Open();
                Trans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted, "NixorTransac");
                cmdExecCommand.Transaction = Trans;
                string lstrAltaId = cmdExecCommand.ExecuteScalar().ToString();
                Trans.Commit();
                myConnection.Close();
                return (lstrAltaId);
            }
            catch (Exception ex)
            {
                if (Trans != null) Trans.Rollback();
                //Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }

        public static string gExecuteScalarTransParam(SqlTransaction pTrans, SqlConnection pstrConn, string pstrProc)
        {

            //SqlConnection myConnection = new SqlConnection(pstrConn);

            SqlCommand cmdExecCommand = new SqlCommand();
            cmdExecCommand.Connection = pstrConn;
            //cmdExecCommand.CommandType = 
            cmdExecCommand.CommandText = pstrProc;
            cmdExecCommand.CommandTimeout = pstrConn.ConnectionTimeout;
            //pstrConn.Open();
            cmdExecCommand.Transaction = pTrans;
            string lstrAltaId = cmdExecCommand.ExecuteScalar().ToString();
            //pstrConn.Close();
            return (lstrAltaId);

        }

        public static int gExecute(SqlTransaction Trans, string pstrProc)
        {
            try
            {
                SqlCommand cmdExecCommand = new SqlCommand();
                cmdExecCommand.Connection = Trans.Connection;
                cmdExecCommand.CommandText = pstrProc;
                cmdExecCommand.CommandTimeout = cmdExecCommand.Connection.ConnectionTimeout;
                cmdExecCommand.Transaction = Trans;
                int lintExec = cmdExecCommand.ExecuteNonQuery();
                return (lintExec);
            }
            catch (Exception ex)
            {
                //Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }

        public static string gExecuteScalar(SqlTransaction Trans, string pstrProc)
        {
            try
            {
                SqlCommand cmdExecCommand = new SqlCommand();
                cmdExecCommand.Connection = Trans.Connection;
                cmdExecCommand.CommandTimeout = cmdExecCommand.Connection.ConnectionTimeout;
                cmdExecCommand.CommandText = pstrProc;
                cmdExecCommand.Transaction = Trans;
                string lstrAltaId = cmdExecCommand.ExecuteScalar().ToString();
                return (lstrAltaId);
            }
            catch (Exception ex)
            {
                //Error.gManejar//Error(new Exception(ex.Message + " - " + pstrProc, ex));
                throw (ex);
            }
        }

        public static SqlTransaction gObtenerTransac(string pstrConn)
        {
            SqlTransaction Trans;
            SqlConnection myConnection = new SqlConnection(pstrConn);

            myConnection.Open();
            Trans = myConnection.BeginTransaction(IsolationLevel.ReadCommitted, "NixorTransac");
            return (Trans);
        }

        public static void gCommitTransac(SqlTransaction Trans)
        {
            if (Trans != null && Trans.Connection != null)
            {
                Trans.Commit();
            }
        }

        /// <summary>
        /// Dario 2013-07-10 Metodo que close transaction
        /// </summary>
        /// <param name="Trans"></param>
        public static void gCloseTransac(SqlTransaction Trans)
        {
            if (Trans != null && Trans.Connection != null)
            {
                Trans.Connection.Close();
                Trans.Dispose();
            }
        }

        public static void gRollbackTransac(SqlTransaction Trans)
        {
            if (Trans != null && Trans.Connection != null)
            {
                Trans.Rollback();

            }
        }

        /// <summary>
        /// Permite formatear un string que se envia por parametro a SQL Server
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string darFormatoSQl(string valor)
        {
            if (valor != null)
                valor = valor.Replace("'", " "); // elimino ' en los nombres y direcciones

            return "'" + valor + "'";
        }

        /// <summary>
        /// permite poner el formato decimal al valor
        /// </summary>
        /// <param name="valor"></param>
        /// <returns></returns>
        public static string darFormatoDecimal(string valor)
        {
            if (valor != null)
                valor = valor.Replace(',', '.');

            return valor;
        }

        /// <summary>
        /// metodo que retorna el Int 32 dbnull
        /// </summary>
        /// <returns></returns>
        public static Int32 IntDBNull()
        {
            return 0;
        }

        /// <summary>
        /// metodo que retorna el string dbnull
        /// </summary>
        /// <returns></returns>
        public static string StringDBNull()
        {
            return string.Empty;
        }

        /// <summary>
        /// metodo que retorna el decimal dbnull
        /// </summary>
        /// <returns></returns>
        public static Decimal DecimalDBnull()
        {
            return -1;
        }
    }
}
