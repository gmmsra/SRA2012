﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Configuration;
using Newtonsoft.Json;
using Helper;
using MailHelper;
using System.Data;
using Data;
using System.Net;
using System.Net.Security;

namespace IntegracionFinnegans
{
    class Program
    {
        public static string conStr = ConfigurationManager.ConnectionStrings["SRAConn"].ConnectionString;

        static void Main(string[] args)
        {
            Console.WriteLine("INICIO ----------------------------------");
            Logger.info(Logger.Log.Info, new string[] { "INICIO", " ------------------------ " });
            Console.WriteLine("BD:-");
            Logger.info(Logger.Log.Info, new string[] { "DB:", "- ", conStr });

            Token token = GetToken();
            // test mail
            /*
            MailManager mail = new MailManager();
            mail.VarEmail = "dvasqueznigro@sra.org.ar";
            mail.varDisplayName = "ERROR integracion Finnegas";
            mail.EnviaMensaje("ERROR integracion Finnegans se ejecuto con error:-"
                             , "la integracion Finnegans se ejecuto con error:- " + "status:" + token.status + ",error:" + token.error);
            */
            if (token.status.ToUpper() == "tokenOK".ToUpper())
            {
                //Console.WriteLine(token.error);
                Logger.info(Logger.Log.Info, new string[] { "Token: ", token.error });
                //GetCuenta(token.error, "2022-01-01");
                EnviarAsientosFinnegans();
                //GetAsiento(token.error, "SRA - 30526");
            }

            Console.WriteLine("FIN ----------------------------------" );
            Logger.info(Logger.Log.Info, new string[] { "FIN", " --------------------------- "});
            //Console.ReadLine();
        }

        /// <summary>
        /// metodo que recupera los asientos a envia a finnegans
        /// </summary>
        public static void EnviarAsientosFinnegans()
        {
            int cant = 0;
            Logger.info(Logger.Log.Info, new string[] { "Asientos INICIO:-" + System.DateTime.Now.ToString() });
            // consulto los asientos que no fueron enviados
            List<AsientoGenericoEntity> datosAsientos = GetAsientosAEnviarFinnegans();

            if ((datosAsientos.Count > 0))
            {
                Logger.info(Logger.Log.Info, new string[] { "Asientos Encontrados para enviar:-" + datosAsientos.Count.ToString() });
                Token token = GetToken();
                // obtengo el token
                //Token token = GetToken();
                // si token ok migro asientos
                if (token.status.ToUpper() == "tokenOK".ToUpper())
                {
                    //Console.WriteLine(token.error);
                    Logger.info(Logger.Log.Info, new string[] { "Token: ", token.error });

                    foreach (AsientoGenericoEntity item in datosAsientos)
                    {
                        cant++;
                        //AsientoGenericoEntity item1 = GetJsonText();
                        //item.IdentificacionExterna = "SRA - " + item.IdentificacionExterna;
                        item.IdentificacionExterna = item.IdentificacionExterna;
                        item.NumeroDocumento = item.NumeroDocumento;
                        item.Descripcion = "SRA - " + item.NumeroDocumento;
                        MigraAsientosAFinnegas(token.error, item);
                        System.Threading.Thread.Sleep(50);
                    }
                }
                else
                {
                    // si error en la obtencion del token logeo y mando mail
                    Console.WriteLine("status:" + token.status);
                    Console.WriteLine(",error:" + token.error);
                    Logger.info(Logger.Log.Info, new string[] { "status:" + token.status + ",error:" + token.error });

                    MailManager mail = new MailManager();
                    mail.VarEmail = "dvasqueznigro@sra.org.ar";
                    mail.varDisplayName = "ERROR integracion Finnegas";
                    mail.EnviaMensaje("ERROR integracion Finnegans se ejecuto con error:-"
                                     , "la integracion Finnegans se ejecuto con error:- " + "status:" + token.status + ",error:" + token.error);
                }
            }
            else
            {
                Logger.info(Logger.Log.Info, new string[] { "Asientos Encontrados para enviar:" + datosAsientos.Count.ToString() + " NO hay asientos para migrar." });
            }

            if (cant >= 0)
            {
                Logger.info(Logger.Log.Info, new string[] { "Asientos enviados: " + cant.ToString() + "." });
            }
            Logger.info(Logger.Log.Info, new string[] { "Asientos FIN:-" + System.DateTime.Now.ToString() });
        }

        /// <summary>
        /// metodo que envia los asientos a la api de finnegans
        /// </summary>
        /// <param name="ta"></param>
        /// <param name="item"></param>
        /// <returns>Token</returns>
        private static Token MigraAsientosAFinnegas(string ta, AsientoGenericoEntity item)
        {
            Token respuesta = new Token();
            string urlAsientos = ConfigurationManager.AppSettings["urlApiFinneg"].ToString() + "ApiAsientoGenericoSra?ACCESS_TOKEN=";

            var url = urlAsientos + ta;
            Console.WriteLine(url);

            Logger.info(Logger.Log.Info, new string[] { "finnegans MigraAsientosAFinnegas post: ", url.ToString() });
            try
            {
                HttpClient httpClient = new HttpClient();
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //throw new Exception("pepe");
                // descomentar para test de post con finnegans
                //AsientoGenericoEntity item = GetJsonText();  
                Logger.info(Logger.Log.Info, new string[] { "finnegans MigraAsientosAFinnegas JsonConvert: ", JsonConvert.SerializeObject(item) });
                /*
                var json = JsonConvert.SerializeObject(item);
                var data = new StringContent(json, Encoding.UTF8, "application/json");

                httpClient.DefaultRequestHeaders.Add("AsientoGenericoVO", JsonConvert.SerializeObject(item));
                */
                HttpContent httpContent = new StringContent(JsonConvert.SerializeObject(item), Encoding.UTF8, "application/json"); // .Replace("\n","")
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //httpContent.Headers.Add("ACCESS_TOKEN", ta);

                var result = httpClient.PostAsync(url, httpContent).Result;
                var readTask = result.Content.ReadAsStringAsync().ConfigureAwait(false);
                var rawResponse = readTask.GetAwaiter().GetResult();
                //string[] temp = rawResponse.ToString().Split(',');

                string asec_id = item.IdentificacionExterna;
                string resOper = string.Empty;


                if (result.IsSuccessStatusCode)
                {
                    //var jsonR = result.Content.ReadAsStringAsync();
                    Token tres = JsonConvert.DeserializeAnonymousType(rawResponse.ToString(), new Token());
                    //System.Threading.Thread.Sleep(30000);
                    //GetAsiento(ta, tres.id);    
                    Console.WriteLine(JsonConvert.DeserializeObject(rawResponse.ToString()));
                    string[] msg = { "finnegans MigraAsientosAFinnegas POST Result OK:- ", JsonConvert.DeserializeObject(rawResponse.ToString()).ToString() };
                    Logger.info(Logger.Log.Info, msg);
                    // aca poner el codigo que marca el asiento como enviado ok

                }
                else
                {
                    //respuesta.status = "ERROR";
                    //respuesta.error = rawResponse.ToString(); 
                    Token tres = JsonConvert.DeserializeAnonymousType(rawResponse.ToString(), new Token());

                    Console.WriteLine(JsonConvert.DeserializeObject(rawResponse.ToString()));
                    string[] msg = { "finnegans MigraAsientosAFinnegas POST ERROR :- ", JsonConvert.DeserializeObject(rawResponse.ToString()).ToString() };
                    Logger.info(Logger.Log.Info, msg);

                    resOper = tres.error;

                    string error = asec_id + msg[0] + msg[1];

                    (new EmailSenderBusiness()).EnviarEmailATecnologia(TiposEmail.Error, item.IdentificacionExterna, error, "MigraAsientosAFinnegas.MigraAsientosAFinnegas POST ERROR:");
                }
                // marco el envio en finnegans
                //MarcaEnvioFinnegansEnNixor(asec_id.Substring(asec_id.IndexOf('-'), asec_id.Length), resOper);
                MarcaEnvioFinnegansEnNixor(asec_id.Substring((asec_id.IndexOf('-') + 1), asec_id.Length - asec_id.IndexOf('-') - 1), resOper);
            }
            catch (Exception ex)
            {
                string[] msg = { "finnegans MigraAsientosAFinnegas catch POST ERROR:- ", ex.Message };
                Console.WriteLine(ex.Message);
                Logger.info(Logger.Log.Info, msg);

                respuesta.status = "ERROR";
                respuesta.error = msg[0] + " " + msg[1];

                (new EmailSenderBusiness()).EnviarEmailATecnologia(TiposEmail.Error, item.IdentificacionExterna, respuesta.error, " MigraAsientosAFinnegas.MigraAsientosAFinnegas catch POST:");
            }
            finally
            {
                //Console.ReadLine();
            }

            //Logger.info(Logger.Log.Info, new string[] { "finnegans MigraAsientosAFinnegas respuesta: ", JsonConvert.SerializeObject(respuesta) });
            return respuesta;
        }

        #region metodos get
        /// <summary>
        /// metodo que obtiene el token para la integracion de finnegans
        /// </summary>
        /// <returns>token</returns>
        private static Token GetToken()
        {
            Token respuesta = new Token();
            string urlTA = ConfigurationManager.AppSettings["urlTA"].ToString();
            string grant_type = ConfigurationManager.AppSettings["grant_type"].ToString();
            string client_id = ConfigurationManager.AppSettings["client_id"].ToString();
            string client_secret = ConfigurationManager.AppSettings["client_secret"].ToString();

            var url = urlTA + "grant_type=" + grant_type + "&client_id=" + client_id + "&client_secret=" + client_secret;
            Console.WriteLine(url);

            Logger.info(Logger.Log.Info, new string[] { "finnegans getToken_URL: ", url.ToString() });

            try
            {
                HttpClient httpClient = new HttpClient();
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                //var result = httpClient.GetAsync("http://192.168.123.17/wsrest/RazasExpo?expo_id=35&usua_id=82").Result;
                //throw new Exception("pepe");
                var result = httpClient.GetAsync(url).Result;
                var readTask = result.Content.ReadAsStringAsync().ConfigureAwait(false);
                var rawResponse = readTask.GetAwaiter().GetResult();

                string[] temp = rawResponse.ToString().Split(',');

                if (temp.Length == 1)
                {
                    respuesta.status = "TokenOK";
                    respuesta.error = rawResponse.ToString(); ;
                    Console.WriteLine("Token:- " + rawResponse.ToString());
                    return respuesta;
                }
                else
                {
                    try
                    {
                        Console.WriteLine("Token:- " +JsonConvert.DeserializeObject(rawResponse.ToString()));
                        respuesta = JsonConvert.DeserializeAnonymousType(rawResponse, (new Token()));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(rawResponse.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                string[] msg = { "finnegans getToken ERROR:- ", ex.Message };
                Console.WriteLine(ex.Message);
                Logger.info(Logger.Log.Info, msg);

                respuesta.status = "ERROR";
                respuesta.error = msg[0] + msg[1];
            }
            finally
            {
                //Console.ReadLine();
            }

            return respuesta;
        }
        
        public static Token GetCuenta(string ta, string identificacionExterna)
        {
            Token respuesta = new Token();

            //var url = "https://api.teamplace.finneg.com/api/ApiListCuentas/?ACCESS_TOKEN=" + ta;
            //var url = "https://api.teamplace.finneg.com/api/apilistcuentas/list?ACCESS_TOKEN=" + ta;
            var url = "https://api.teamplace.finneg.com/api/ApiListCuentas/list?" + identificacionExterna + "&ACCESS_TOKEN=" + ta;
            // URL:    https://api.teamplace.finneg.com/api/ApiListCuentas/list?[updatedSince]={$yyyy-mm-dd}&ACCESS_TOKEN={$access_token}
            Console.WriteLine(url);

            Logger.info(Logger.Log.Info, new string[] { "finnegans GetCuenta_url: ", url.ToString() });

            try
            {
                HttpClient httpClient = new HttpClient();
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var result = httpClient.GetAsync(url).Result;
                var readTask = result.Content.ReadAsStringAsync().ConfigureAwait(false);
                var rawResponse = readTask.GetAwaiter().GetResult();

                string[] temp = rawResponse.ToString().Split(',');

                if (temp.Length == 1)
                {
                    respuesta.status = "TokenOK";
                    respuesta.error = rawResponse.ToString(); ;
                    return respuesta;
                }
                else
                {
                    try
                    {
                        Console.WriteLine(JsonConvert.DeserializeObject(rawResponse.ToString()));
                        Logger.info(Logger.Log.Info, new string[] { "finnegans GetCuenta: ", JsonConvert.DeserializeObject(rawResponse.ToString()).ToString() });
                        respuesta = JsonConvert.DeserializeAnonymousType(rawResponse, (new Token()));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(rawResponse.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                string[] msg = { "finnegans GetCuenta_url ERROR:- ", ex.Message };
                Console.WriteLine(ex.Message);
                Logger.info(Logger.Log.Info, msg);

                respuesta.status = "ERROR";
                respuesta.error = msg[0] + msg[1];
            }
            finally
            {
                //Console.ReadLine();
            }

            return respuesta;
        }
        /// <summary>
        /// consulta de asiento registrado en finnegans
        /// </summary>
        /// <param name="ta"></param>
        /// <param name="identificacionExterna"></param>
        /// <returns></returns>
        
        public static Token GetAsiento(string ta, string identificacionExterna)
        {
            Token respuesta = new Token();

            var url = ConfigurationManager.AppSettings["urlApiFinneg"].ToString() + "ApiAsientoGenericoSra/" + identificacionExterna + "?ACCESS_TOKEN=" + ta;
            //var url = ConfigurationManager.AppSettings["urlApiFinneg"].ToString() + "apitestDario/list?ACCESS_TOKEN=" + ta;
            Console.WriteLine(url);

            Logger.info(Logger.Log.Info, new string[] { "finnegans GetAsiento_URL: ", url.ToString() });

            try
            {
                HttpClient httpClient = new HttpClient();
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                var result = httpClient.GetAsync(url).Result;
                var readTask = result.Content.ReadAsStringAsync().ConfigureAwait(false);
                var rawResponse = readTask.GetAwaiter().GetResult();

                string[] temp = rawResponse.ToString().Split(',');

                if (temp.Length == 1)
                {
                    respuesta.status = "TokenOK";
                    respuesta.error = rawResponse.ToString(); ;
                    return respuesta;
                }
                else
                {
                    try
                    {
                        Console.WriteLine(JsonConvert.DeserializeObject(rawResponse.ToString()));
                        Logger.info(Logger.Log.Info, new string[] { "finnegans GetAsiento: ", JsonConvert.DeserializeObject(rawResponse.ToString()).ToString() });
                        respuesta = JsonConvert.DeserializeAnonymousType(rawResponse, (new Token()));
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(rawResponse.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                string[] msg = { "finnegans getToken ERROR:- ", ex.Message };
                Console.WriteLine(ex.Message);
                Logger.info(Logger.Log.Info, msg);

                respuesta.status = "ERROR";
                respuesta.error = msg[0] + msg[1];
            }
            finally
            {
                //Console.ReadLine();
            }

            return respuesta;
        }
        #endregion

        #region base de datos
        /// <summary>
        /// metodo que recupera los asientos apra enviar a finnegans
        /// </summary>
        /// <returns>List<AsientoGenericoEntity></returns>
        private static List<AsientoGenericoEntity> GetAsientosAEnviarFinnegans()
        {
            List<AsientoGenericoEntity> respuesta = new List<AsientoGenericoEntity>();
            StringBuilder pstrProc = new StringBuilder();

            string empresaFinneg = ConfigurationManager.AppSettings["empresaFinneg"];

            pstrProc.Append("exec p_CON_Finnegans_Asientos_a_enviar ");
            /*
            pstrProc.Append("@expo_Id=35");
            pstrProc.Append(", @raza_Codi=104");
            */
            try
            {
                string[] Msg = { "MigraAsientosAFinnegas.GetAsientosAEnviarFinnegans;", pstrProc.ToString() };
                Logger.info(Logger.Log.Info, Msg);

                DataSet ds = DataBase.gExecuteQuery(conStr, pstrProc.ToString(), 9999);

                if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
                {
                    AsientoGenericoEntity item = new AsientoGenericoEntity();
                    int asec_id_temp = 0;
                    double CotizacionDOL = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (asec_id_temp != Convert.ToInt32(dr["IdentificacionExterna"]))
                        {
                            if (item.NumeroDocumento != null && item.NumeroDocumento.Trim().Length > 0)
                            {
                                AsientoGenericoCotizacionesEntity itemCotizacionPes = new AsientoGenericoCotizacionesEntity();
                                itemCotizacionPes.Cotizacion = 1;
                                itemCotizacionPes.MonedaID = "PES";
                                item.AsientoGenericoCotizaciones.Add(itemCotizacionPes);

                                AsientoGenericoCotizacionesEntity itemCotizacionDol = new AsientoGenericoCotizacionesEntity();
                                itemCotizacionDol.Cotizacion = CotizacionDOL;
                                itemCotizacionDol.MonedaID = "DOL";
                                item.AsientoGenericoCotizaciones.Add(itemCotizacionDol);

                                respuesta.Add(item);
                            }

                            asec_id_temp = Convert.ToInt32(dr["IdentificacionExterna"]);
                            string ejercicio = dr["ejercicio"].ToString();

                            item = new AsientoGenericoEntity();
                            
                            item.TransaccionSubtipoID = dr["TransaccionSubtipoID"].ToString();
                            item.Descripcion = dr["Descripcion"].ToString();
                            item.EmpresaID = empresaFinneg; // dr["EmpresaID"].ToString();
                            item.TalonarioID = dr["TalonarioID"].ToString();
                            item.MonedaID = dr["MonedaID"].ToString();
                            item.FechaComprobante = dr["FechaComprobante"].ToString();
                            //item.NumeroDocumento = ejercicio + "-" + dr["NumeroDocumento"].ToString();
                            item.NumeroDocumento = dr["NumeroDocumento"].ToString();
                            item.TransaccionTipoID = dr["TransaccionTipoID"].ToString();
                            item.Fecha = dr["Fecha"].ToString();
                            item.IdentificacionExterna = item.TransaccionSubtipoID +"-"+asec_id_temp.ToString();
                            //item.IdentificacionExterna = "TDVN" + item.TransaccionSubtipoID + "-" + asec_id_temp.ToString();

                            CotizacionDOL = Convert.ToDouble(dr["CotizacionDOL"]);
                        }
                        
                        AsientoItemsEntity itemAsiento = new AsientoItemsEntity();
                        itemAsiento.Descripcion = null;
                        itemAsiento.OperacionBancariaID = null;
                        itemAsiento.OrganizacionID = "ORGNODEFINIDA"; // string.Empty;
                        itemAsiento.ProductoID = null;
                        itemAsiento.DebeHaber = Convert.ToInt32(dr["DebeHaber"]);
                        itemAsiento.CuentaID = dr["CuentaID"].ToString();
                        itemAsiento.ImporteMonTransaccion = Convert.ToDouble(dr["ImporteMonTransaccion"]);
                        itemAsiento.MonedaIDTransaccion = dr["MonedaIDTransaccion"].ToString();
                        itemAsiento.FechaVto = null;

                        string codigo = (!String.IsNullOrEmpty(dr["Codigo"].ToString())) ? dr["Codigo"].ToString().Trim() : string.Empty;
                        if (codigo.Length > 0)
                        {
                            DimensionDistribucionEntity itemDimension = new DimensionDistribucionEntity();
                            itemDimension.dimensionCodigo = (!String.IsNullOrEmpty(dr["dimensionCodigo"].ToString())) ? dr["dimensionCodigo"].ToString().Trim() : null;
                            itemDimension.distribucionCodigo = (!String.IsNullOrEmpty(dr["distribucionCodigo"].ToString())) ? dr["distribucionCodigo"].ToString().Trim() : null;

                            distribucionItemsEntity itemDistri = new distribucionItemsEntity();
                            itemDistri.Codigo = (!String.IsNullOrEmpty(dr["Codigo"].ToString())) ? dr["Codigo"].ToString().Trim() : null;
                            itemDistri.porcentaje = (!String.IsNullOrEmpty(dr["porcentaje"].ToString())) ? Convert.ToDouble(dr["porcentaje"]) : 0;
                            itemDimension.distribucionItems.Add(itemDistri);

                            itemAsiento.DimensionDistribucion.Add(itemDimension);
                        }

                        item.AsientoItems.Add(itemAsiento);
                        /*
                        AsientoGenericoCotizacionesEntity itemCotizacion = new AsientoGenericoCotizacionesEntity();
                        itemCotizacion.Cotizacion = 1;
                        itemCotizacion.MonedaID = "PES";
                        item.AsientoGenericoCotizaciones.Add(itemCotizacion);

                        AsientoGenericoCotizacionesEntity itemCotizacion1 = new AsientoGenericoCotizacionesEntity();
                        itemCotizacion1.Cotizacion = 5;
                        itemCotizacion1.MonedaID = "DOL";
                        item.AsientoGenericoCotizaciones.Add(itemCotizacion1);
                        */
                    }

                    if (item.EmpresaID.Trim().Length > 0)
                        respuesta.Add(item);
                }
            }
            catch (Exception ex)
            {
                string[] MsgError = { "MigraAsientosAFinnegas.GetAsientosAEnviarFinnegans:  ", pstrProc.ToString(), " registro el siguiente error: " + ex.Message };
                Logger.error(Logger.Log.Error, MsgError);

                string Message = MsgError[0] + " " + MsgError[1] + " " + MsgError[2];

                (new EmailSenderBusiness()).EnviarEmailATecnologia(TiposEmail.Error, pstrProc.ToString(), Message, "MigraAsientosAFinnegas.GetAsientosAEnviarFinnegans");
            }

            return respuesta;
        }

        /// <summary>
        /// metodo que marca el asicento cabe como enviado y si hay error deja lo que finnegans envio
        /// si esta ok en error no deja nada
        /// </summary>
        /// <param name="asec_id"></param>
        /// <param name="asec_mesage_envio_FN"></param>
        private static void MarcaEnvioFinnegansEnNixor(string asec_id, string asec_mesage_envio_FN)
        {
            StringBuilder pstrProc = new StringBuilder();

            pstrProc.Append("exec p_CON_Finnegans_Asientos_Resp_Envio ");
            
            pstrProc.Append("@asec_id=");
            pstrProc.Append(asec_id);
            pstrProc.Append(",@asec_mesage_envio_FN=");
            if (asec_mesage_envio_FN.Trim().Length > 0) { pstrProc.Append(DataBase.darFormatoSQl(asec_mesage_envio_FN)); } else { pstrProc.Append("null"); }

            try
            {
                string[] Msg = { "MigraAsientosAFinnegas.MarcaEnvioFinnegansEnNixor;", pstrProc.ToString() };
                Logger.info(Logger.Log.Info, Msg);

                DataSet ds = DataBase.gExecuteQuery(conStr, pstrProc.ToString(), 9999);
                /*
                if ((ds != null) && (ds.Tables.Count != 0) && (ds.Tables[0].Rows.Count != 0))
                {
                    
                }
                */
            }
            catch (Exception ex)
            {
                string[] MsgError = { "MigraAsientosAFinnegas.MarcaEnvioFinnegansEnNixor:  ", pstrProc.ToString(), " registro el siguiente error: " + ex.Message };
                Logger.error(Logger.Log.Error, MsgError);

                string Message = MsgError[0] + " " + MsgError[1] + " " + MsgError[2];

                (new EmailSenderBusiness()).EnviarEmailATecnologia(TiposEmail.Error, pstrProc.ToString(), Message, "MigraAsientosAFinnegas.MarcaEnvioFinnegansEnNixor");
            }
        }

        /// <summary>
        /// metodo que arma un json de asiento de testo
        /// </summary>
        /// <returns></returns>
        private static AsientoGenericoEntity GetJsonText()
        {
            string IdentificacionExterna = "SRA - " + System.DateTime.UtcNow.Ticks.ToString();

            AsientoGenericoEntity respuesta = new AsientoGenericoEntity();
            respuesta.TransaccionSubtipoID = "AG";
            respuesta.Descripcion = IdentificacionExterna;
            respuesta.EmpresaID = "PRUEBA39";
            respuesta.TalonarioID = "AG";
            respuesta.MonedaID = "PES";
            respuesta.FechaComprobante = "2022-07-26";
            respuesta.NumeroDocumento = "00038";
            respuesta.TransaccionTipoID = "ASIENTOGENERICO";
            respuesta.Fecha = "2022-07-26";
            respuesta.IdentificacionExterna = IdentificacionExterna;
            /*
            AsientoGenericoCotizacionesEntity itemCotizacion = new AsientoGenericoCotizacionesEntity();
            itemCotizacion.Cotizacion = 1;
            itemCotizacion.MonedaID = "PES";
            respuesta.AsientoGenericoCotizaciones.Add(itemCotizacion);

            AsientoGenericoCotizacionesEntity itemCotizacion1 = new AsientoGenericoCotizacionesEntity();
            itemCotizacion1.Cotizacion = 5;
            itemCotizacion1.MonedaID = "DOL";
            respuesta.AsientoGenericoCotizaciones.Add(itemCotizacion1);
            */
            AsientoItemsEntity itemAsientoTmp1 = new AsientoItemsEntity();
            itemAsientoTmp1.Descripcion = "null";
            itemAsientoTmp1.OperacionBancariaID = null;
            itemAsientoTmp1.OrganizacionID = null;
            itemAsientoTmp1.ProductoID = "null";
            itemAsientoTmp1.DebeHaber = 1;
            itemAsientoTmp1.CuentaID = "CCE";
            itemAsientoTmp1.ImporteMonTransaccion = 500;
            itemAsientoTmp1.MonedaIDTransaccion = "PES";
            itemAsientoTmp1.FechaVto = "2022-07-26";

            itemAsientoTmp1.DimensionDistribucion = new List<DimensionDistribucionEntity>();
            respuesta.AsientoItems.Add(itemAsientoTmp1);

            AsientoItemsEntity itemAsientoTmp = new AsientoItemsEntity();
            itemAsientoTmp.Descripcion = "null";
            itemAsientoTmp.OperacionBancariaID = null;
            itemAsientoTmp.OrganizacionID = null;
            itemAsientoTmp.ProductoID = "null";
            itemAsientoTmp.DebeHaber = -1;
            itemAsientoTmp.CuentaID = "CCE";
            itemAsientoTmp.ImporteMonTransaccion = 500;
            itemAsientoTmp.MonedaIDTransaccion = "PES";
            itemAsientoTmp.FechaVto = "2022-07-26";
            itemAsientoTmp.DimensionDistribucion = new List<DimensionDistribucionEntity>();
            respuesta.AsientoItems.Add(itemAsientoTmp);

            /*
            AsientoItemsEntity itemAsientoTmp = new AsientoItemsEntity();
            itemAsientoTmp.Descripcion = "null";
            itemAsientoTmp.OperacionBancariaID = null;
            itemAsientoTmp.OrganizacionID = null;
            itemAsientoTmp.ProductoID = "null";
            itemAsientoTmp.DebeHaber = -1;
            itemAsientoTmp.CuentaID = "GASBAN";
            itemAsientoTmp.ImporteMonTransaccion = 200;
            itemAsientoTmp.MonedaIDTransaccion = "PES";
            itemAsientoTmp.FechaVto = "2022-07-26";
            DimensionDistribucionEntity itemDimension = new DimensionDistribucionEntity();
            
            itemDimension.dimensionCodigo = "DIMCTC"; 
            itemDimension.distribucionCodigo = "";

            distribucionItemsEntity itemDistri = new distribucionItemsEntity();
            itemDistri.Codigo = "CC1";
            itemDistri.porcentaje = 100.000000;
            itemDimension.distribucionItems.Add(itemDistri);

            itemAsientoTmp.DimensionDistribucion.Add(itemDimension);
            
            respuesta.AsientoItems.Add(itemAsientoTmp);
            */

            return respuesta;
        }
        #endregion
    }
    public class Token
    {
        public string status { get; set; }
        public string error { get; set; }
        public string id { get; set; }
    }
}

