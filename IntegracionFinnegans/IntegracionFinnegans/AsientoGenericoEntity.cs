﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegracionFinnegans
{
    public class AsientoGenericoEntity
    {
        public string TransaccionSubtipoID { set; get; }
        public string Descripcion { set; get; }
        public string EmpresaID { set; get; }
        public string TalonarioID { set; get; }
        public string MonedaID { set; get; }

        public List<AsientoItemsEntity> AsientoItems = new List<AsientoItemsEntity>();

        public List<AsientoItemsAjusteEntity> AsientoItemsAjuste = new List<AsientoItemsAjusteEntity>();
        public string FechaComprobante { set; get; }
        public string NumeroDocumento { set; get; }
        public string TransaccionTipoID { set; get; }
        public string Fecha { set; get; }
        public string IdentificacionExterna { set; get; }
        public List<AsientoGenericoCotizacionesEntity> AsientoGenericoCotizaciones = new List<AsientoGenericoCotizacionesEntity>();
    }

    public class AsientoGenericoCotizacionesEntity
    {
        public double Cotizacion { set; get; }
        public string MonedaID { set; get; }
    }

    public class AsientoItemsEntity
    {
        public string Descripcion { set; get; }
        public string OperacionBancariaID { set; get; }
        public string OrganizacionID { set; get; }
        public string ProductoID { set; get; }
        public int DebeHaber { set; get; }
        public string CuentaID { set; get; }
        public double ImporteMonTransaccion { set; get; }
        public string MonedaIDTransaccion { set; get; }
        public string FechaVto { set; get; }

        public List<DimensionDistribucionEntity> DimensionDistribucion = new List<DimensionDistribucionEntity>();
    }

    public class AsientoItemsAjusteEntity
    {
        //public string Descripcion { set; get; }
        //public string OperacionBancariaID { set; get; }
        //public string OrganizacionID { set; get; }
        //public string ProductoID { set; get; }
        public int DebeHaber { set; get; }
        public string CuentaID { set; get; }
        public double ImporteMonTransaccion { set; get; }
        public string MonedaIDTransaccion { set; get; }
        //public string FechaVto { set; get; }

        public List<DimensionDistribucionEntity> DimensionDistribucion = new List<DimensionDistribucionEntity>();
    }

    public class DimensionDistribucionEntity
    {
        public string dimensionCodigo { set; get; }
        public string distribucionCodigo { set; get; }
        public List<distribucionItemsEntity> distribucionItems = new List<distribucionItemsEntity>();
    }

    public class distribucionItemsEntity
    {
        public string Codigo { set; get; }
        public double porcentaje { set; get; }
    }
}
