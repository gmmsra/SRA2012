﻿using Data;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]
[assembly: log4net.Config.Repository()]
namespace Helper
{
    public class Logger
    {
        private static string conStr = ConfigurationManager.ConnectionStrings["SRAConn"].ConnectionString;


        private enum LogLevel
        {
            Info,
            Debug,
            Warning,
            Error,
            Fatal
        }

        public enum Log
        {
            Error,
            Info
        }

        private const String cLogger = "LogSRA";

        public Logger()
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        #region Log Level Info

        public static void info(Log archivo, params string[] message)
        {
            Logger.info(archivo, null, message);
        }

        public static void info(Log archivo, string methodName, string httpMethod, object request)
        {
            var messages = new List<string>();

            var baseProperties = request.GetType().BaseType.GetProperties();
            var derivedProperties = request.GetType().GetProperties();
            var modelProperties = baseProperties.Concat(derivedProperties.Where(p => baseProperties.Select(b => b.Name).Contains(p.Name) == false));

            messages.AddRange(new string[] { methodName, httpMethod });

            foreach (var property in modelProperties)
            {
                var value = property.GetValue(request);

                messages.Add("|@{property.Name}: {value?.ToString()}");
            }

            Logger.info(archivo, null, messages.ToArray());
        }

        public static void info(Log archivo, Exception ex)
        {
            Logger.info(archivo, ex, null);
        }

        public static void info(Log archivo, Exception ex, params string[] message)
        {
            Logger.doLog(archivo, LogLevel.Info, ex, message);
        }

        #endregion Log Level Info

        #region Log Level debug

        public static void debug(Log archivo, params string[] message)
        {
            Logger.debug(archivo, null, message);
        }
        public static void debug(Log archivo, Exception ex)
        {
            Logger.debug(archivo, ex, null);
        }

        public static void debug(Log archivo, Exception ex, params string[] message)
        {
            Logger.doLog(archivo, LogLevel.Debug, ex, message);
        }

        #endregion Log Level debug

        #region Log Level warning

        public static void warning(Log archivo, params string[] message)
        {
            Logger.warning(archivo, null, message);
        }
        public static void warning(Log archivo, Exception ex)
        {
            Logger.warning(archivo, ex, null);
        }

        public static void warning(Log archivo, Exception ex, params string[] message)
        {
            Logger.doLog(archivo, LogLevel.Warning, ex, message);
        }

        #endregion Log Level warning

        #region Log Level fatal

        public static void fatal(Log archivo, params string[] message)
        {
            Logger.fatal(archivo, null, message);
        }
        public static void fatal(Log archivo, Exception ex)
        {
            Logger.fatal(archivo, ex, null);
        }

        public static void fatal(Log archivo, Exception ex, params string[] message)
        {
            Logger.doLog(archivo, LogLevel.Fatal, ex, message);
        }

        #endregion Log Level fatal

        #region Log Level error

        public static void error(Log archivo, params string[] message)
        {
            Logger.error(archivo, null, message);
        }
        public static void error(Log archivo, Exception ex)
        {
            Logger.error(archivo, ex, null);
        }

        public static void error(Log archivo, Exception ex, params string[] message)
        {
            Logger.doLog(archivo, LogLevel.Error, ex, message);
        }

        #endregion Log Level error

        private static void doLog(Log archivo, LogLevel level, Exception ex, params string[] message)
        {
            ILog logger = Logger.getLogger(archivo);

            if (Logger.levelEnabled(logger, level))
            {
                string allMessage = null;
                if (message != null)
                {
                    allMessage = string.Empty;
                    for (int i = message.GetLowerBound(0); i <= message.GetUpperBound(0); i++)
                    {
                        allMessage += message[i];
                    }
                }
                string completeMessage = getCompleteMessage(allMessage, ex);

                switch (level)
                {
                    case LogLevel.Info:
                        logger.Info(completeMessage);
                        break;
                    case LogLevel.Debug:
                        logger.Debug(completeMessage);
                        break;
                    case LogLevel.Warning:
                        logger.Warn(completeMessage);
                        break;
                    case LogLevel.Error:
                        logger.Error(completeMessage);
                        break;
                    case LogLevel.Fatal:
                        logger.Fatal(completeMessage);
                        break;
                    default:
                        logger.Info(completeMessage);
                        break;
                }

                string varOperLog = "varOperLog";
                string varLog = "varLog";

                if (message != null && message.Count() > 0)
                {
                    if (message[0].Length > 249)
                        varOperLog = message[0].Substring(0, 249);
                    else
                        varOperLog = message[0];
                }

                if (message != null)
                {
                    if (allMessage.Length < 3999)
                    {
                        ws_InsertLog(varOperLog, allMessage);
                    }
                    else
                    {
                        int index = 0;

                        while (index < allMessage.Length)
                        {
                            if ((allMessage.Length - index) > 3998)
                                varLog = allMessage.Substring(index, 3998);
                            else
                                varLog = allMessage.Substring(index, (allMessage.Length - index));

                            try
                            {
                                ws_InsertLog(varOperLog, varLog);
                            }
                            catch (Exception X)
                            {
                                ws_InsertLog(varOperLog, "Error doLog :-" + X.Message);
                            }

                            index = index + 3998;
                        }
                    }
                }
            }
        }

        private static ILog getLogger(Log logger)
        {
            ILog result;

            switch (logger)
            {
                case Log.Error:
                case Log.Info:
                    result = LogManager.GetLogger(cLogger);
                    break;
                default:
                    result = LogManager.GetLogger(cLogger);
                    break;
            }

            return result;
        }

        private static bool levelEnabled(ILog logger, LogLevel level)
        {
            bool result = false;

            switch (level)
            {
                case LogLevel.Info:
                    result = logger.IsInfoEnabled;
                    break;
                case LogLevel.Debug:
                    result = logger.IsDebugEnabled;
                    break;
                case LogLevel.Warning:
                    result = logger.IsWarnEnabled;
                    break;
                case LogLevel.Error:
                    result = logger.IsErrorEnabled;
                    break;
                case LogLevel.Fatal:
                    result = logger.IsFatalEnabled;
                    break;
                default:
                    result = logger.IsInfoEnabled;
                    break;
            }

            return result;
        }

        private static string getCompleteMessage(string message, Exception ex)
        {
            Exception innerException = ex;
            StringBuilder completeMessage = new StringBuilder(string.Empty);

            if (!string.IsNullOrEmpty(message) && (ex != null))
            {
                completeMessage.AppendLine(message);
            }
            else
            {
                completeMessage.Append(message);
            }

            while (innerException != null)
            {
                completeMessage.AppendLine(ex.ToString());
                innerException = innerException.InnerException;
            }

            return completeMessage.ToString();
        }

        /// <summary>
        /// metodo que logea en la bd los pedidos de a los ws
        /// </summary>
        /// <param name="varOperLog"></param>
        /// <param name="varLog"></param>
        private static void ws_InsertLog(string varOperLog, string varLog)
        {
            
            StringBuilder pstrProc = new StringBuilder();

            //@varOperLog nvarchar(250) ,@varLog nvarchar(1000) 

            pstrProc.Append("exec p_CON_Finnegans_InsertAsientosLog ");
            pstrProc.Append("@varOperLog=");
            if (varOperLog != null && varOperLog.Trim().Length > 0) { pstrProc.Append(DataBase.darFormatoSQl(varOperLog)); } else { pstrProc.Append("null"); }
            pstrProc.Append(",@varLog=");
            if (varLog != null && varLog.Trim().Length > 0) { pstrProc.Append(DataBase.darFormatoSQl(varLog)); } else { pstrProc.Append("null"); }

            try
            {
                DataSet ds = DataBase.gExecuteQuery(conStr, pstrProc.ToString(), 9999);
            }
            catch (Exception ex)
            {
            }
        }
    }
}
