﻿using Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace MailHelper
{
    /// <summary>
    /// Útiles de la aplicación
    /// </summary>
    public class MailManager
    {
        private string _SmtpServidor { get; set; }
        private string _SmtpUsuario { get; set; }
        private string _SmtpPassword { get; set; }
        public int smtpPuerto { get; set; }
        public bool EnableSsl { get; set; }
        public string VarEmail { get; set; }
        public string VarEmailCC { get; set; }
        public string VarEmailCCO { get; set; }
        private string _CuentaOrigen { get; set; }
        public bool isBodyHtml { get; set; }
        public string varDisplayName { get; set; }


        public MailManager()
        {
            this._SmtpServidor = ConfigurationManager.AppSettings["smtpServidor"];
            this._SmtpUsuario = ConfigurationManager.AppSettings["smtpUsuario"];
            this._SmtpPassword = ConfigurationManager.AppSettings["smtpPassword"];
            this._CuentaOrigen = ConfigurationManager.AppSettings["cuentaOrigen"];
            this.smtpPuerto = Convert.ToInt16(ConfigurationManager.AppSettings["smtpPuerto"]);
            this.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
        }

        /// <summary>
        /// Envío de un correo electrónico
        /// </summary>
        /// <param name="subject">Asunto del mensaje</param>
        /// <param name="body"></param>
        public void EnviaMensaje(string subject, string body)
        {
            this.EnviaMensaje(subject, body, null);
        }

        /// <summary>
        /// Envío de un correo electrónico con adjuntos
        /// </summary>
        /// <param name="subject">Asunto del mensaje</param>
        /// <param name="body"></param>
        public void EnviaMensaje(string subject, string body, List<Attachment> attachments)
        {
            try
            {
                string smtpServidor = this._SmtpServidor; //"192.168.123.7";
                string smtpUsuario = this._SmtpUsuario; //"procesos";
                string smtpPassword = this._SmtpPassword; //"procesos";
                string cuentaDestino = this.VarEmail;
                string cuentaDestinoCC = this.VarEmailCC;
                string cuentaDestinoCCO = this.VarEmailCCO;

                string cuentaOrigen = this._CuentaOrigen; //"procesos@sra.org.ar";

                string _displayname = (varDisplayName.Trim().Length == 0) ? "Sociedad Rural Argentina. Envío" : varDisplayName;


                // Permite a las aplicaciones enviar mensajes de correo electrónico mediante el protocolo SMTP 
                SmtpClient cliente = new SmtpClient(smtpServidor, smtpPuerto);
                //cliente.Port = smtpPuerto;
                // Credenciales de autenticación
                NetworkCredential credenciales = new NetworkCredential(smtpUsuario, smtpPassword);

                // Credenciales de autenticación
                cliente.Credentials = credenciales;
                // Se descartan las credenciales por defecto
                cliente.UseDefaultCredentials = false;

                // Los mensajes de correo electrónico se envían a un servidor SMTP a través de la red
                //cliente.DeliveryMethod = SmtpDeliveryMethod.Network;
                cliente.EnableSsl = EnableSsl;

                // Mensaje
                MailMessage mensaje = new MailMessage();

                // Dirección de origen
                MailAddress direccionOrigen = new MailAddress(cuentaOrigen, _displayname);
                mensaje.From = direccionOrigen;

                // Dirección de destino
                if (cuentaDestino != null)
                {
                    string[] cuentas = cuentaDestino.Split(';');
                    foreach (string cuenta in cuentas)
                    {
                        if (cuenta.Trim().Length > 0)
                        {
                            MailAddress direccionDestino = new MailAddress(cuenta);
                            mensaje.To.Add(direccionDestino);
                            direccionDestino = null;
                        }
                    }
                }

                // Dirección de destino
                if (cuentaDestinoCC != null && cuentaDestinoCC.Trim().Length > 0)
                {
                    string[] cuentas = cuentaDestinoCC.Split(';');
                    foreach (string cuenta in cuentas)
                    {
                        if (cuenta.Trim().Length > 0)
                        {
                            MailAddress direccionDestino = new MailAddress(cuenta);
                            mensaje.CC.Add(direccionDestino);
                            direccionDestino = null;
                        }
                    }
                }

                // Dirección de destinoCCO
                if (cuentaDestinoCCO != null && cuentaDestinoCCO.Trim().Length > 0)
                {
                    string[] cuentasCCO = cuentaDestinoCCO.Split(';');
                    foreach (string cuentaCCO in cuentasCCO)
                    {
                        if (cuentaCCO.Trim().Length > 0)
                        {
                            MailAddress direccionDestinoCCO = new MailAddress(cuentaCCO);
                            mensaje.Bcc.Add(direccionDestinoCCO);
                            direccionDestinoCCO = null;
                        }
                    }
                }

                mensaje.Subject = subject + " " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                if (attachments != null)
                {
                    foreach (Attachment itemAttach in attachments)
                    {
                        mensaje.Attachments.Add(itemAttach);
                    }
                }

                mensaje.BodyEncoding = Encoding.UTF8;
                mensaje.Body = body;
                mensaje.IsBodyHtml = isBodyHtml;
                /*
                ServicePointManager.ServerCertificateValidationCallback
                         = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                */
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(delegate { return true; });
                ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                // Envío del mensaje
                cliente.Send(mensaje);

                // Limpieza
                mensaje = null;
                direccionOrigen = null;

                credenciales = null;
                cliente = null;
            }
            catch (Exception ex)
            {
                string Msg = "MailManager.EnviaMensaje: registro el siguiente error: " + ex.Message;
                Logger.info(Logger.Log.Info, new string[] { "EnviaMensaje: ", Msg });
                throw new Exception(Msg);
            }
        }
    }

    public class TiposEmail
    {
        public const int Error = 1;
        public const int Informe = 2;
    }

    public class EmailSenderBusiness
    {
        // variable en la que guardo el mail de tecnologia que se usara en las alertas del servicio
        private string emailTecnologia = ConfigurationManager.AppSettings["emailTecnologia"].ToString();
        // variable para el poner el mail del dstinatario uno de test y que no salgan los mails a los clientes 
        private string emailDestinatarioTest = ConfigurationManager.AppSettings["emailDestinatarioTest"].ToString();
        // variable para el poner el mail del destinatario usuario rrggenvios
        private string emailDtoRRGG = ConfigurationManager.AppSettings["emailDtoRRGG"].ToString();
        // variable para el poner el mail del destinatario usuario facturacion para control    
        private string emailCtrlFactAuto = ConfigurationManager.AppSettings["emailCtrlFactAuto"].ToString();
        // variable que sirve apra indicarle al servicio si debe de enviar los mails a los clientes o a un usuario de test
        private bool bitEnviaAEmailDestinatarioTest = Convert.ToBoolean(ConfigurationManager.AppSettings["enviaAEmailDestinatarioTest"]);
        // variable que utilizo para hacer logs en modo debug
        private bool bitDebug = Convert.ToBoolean(ConfigurationManager.AppSettings["Debug"]);
        // variable que contiene la url que se envia en el mail a los clientes para hacer el acuse de recibo del mismo
        //private string UrlAcuse = ConfigurationManager.AppSettings["UrlAcuse"].ToString();
        private int intCantAdjuntosMails = Convert.ToInt16(ConfigurationManager.AppSettings["CantAdjuntosMails"]);

        public void EnviarEmailATecnologia(int TipoEmail, string comando, string msgError, string varfuncion)
        {
            MailManager email = new MailManager();
            email.varDisplayName = "Sociedad Rural Argentina. Envío";
            switch (TipoEmail)
            {
                case TiposEmail.Error:
                    email.VarEmail = emailTecnologia;
                    email.isBodyHtml = false;
                    email.EnviaMensaje(((bitEnviaAEmailDestinatarioTest == true) ? "TEST ALERTA del Servicio de Envios de Mail de la SRA" : "ALERTA del Servicio de Envios de Mail de la SRA")
                                       , "EL servicio se ejecuto con error:- " + varfuncion
                                       + "\r\n"
                                       + "verifique los logs del servicio o el evenview del servidor, en donde encontrara mayor detalle de los sucedido."
                                       + "\r\n"
                                       + " **************************** Command:-"
                                       + ((comando.Trim().Length > 0) ? comando : "comando vacio")
                                       + " **************************** ERROR:-"
                                       + ((msgError.Trim().Length > 0) ? msgError : "error vacio")
                                       + "\r\n"
                                       + "Saludos"
                                       );
                    break;
                case TiposEmail.Informe:
                    email.VarEmail = emailTecnologia;
                    email.isBodyHtml = false;
                    email.EnviaMensaje(((bitEnviaAEmailDestinatarioTest == true) ? "TEST INFORME del Servicio de Envios de Mail de la SRA" : "INFORME del Servicio de Envios de Mail de la SRA")
                                                          , "EL servicio se ejecuto con error:- " + varfuncion
                                                          + "\r\n"
                                                          + "verifique los logs del servicio o el evenview del servidor, en donde encontrara mayor detalle de los sucedido."
                                                          + "\r\n"
                                                          + " **************************** Command:-"
                                                          + ((comando.Trim().Length > 0) ? comando : "comando vacio")
                                                          + "**************************** ERROR:-"
                                                          + ((msgError.Trim().Length > 0) ? msgError : "error vacio")
                                                          + "\r\n"
                                                          + "Saludos"
                                                          );
                    break;

                default:
                    break;
            }

        }

        public void EnviarEmail(string destinatario, string subject, string body)
        {
            MailManager email = new MailManager
            {
                varDisplayName = "Sociedad Rural Argentina. Envío",
                VarEmail = destinatario,
                isBodyHtml = false
            };

            email.EnviaMensaje(subject, body);
        }

        ///// <summary>
        ///// metodo que envia el parte ingresado de nacimientos al cliente 
        ///// </summary>
        ///// <param name="objImpoWGRespuesta"></param>
        ///// <returns>bool</returns>
        //public static bool EnviarParteIngresadoWG(ImpoWGRespuestaEntity objImpoWGRespuesta, string FuncionEnvia, bool archiRetroactivo)
        //{
        //    bool respuesta = true;



        //    string NombreArchivo = objImpoWGRespuesta.varNomArchi;

        //    try
        //    {
        //        // creo el objeto attachments donde agregare todos los
        //        MailManager email = new MailManager();
        //        email.isBodyHtml = true;

        //        email.varDisplayName = "Sociedad Rural Argentina. Envío";

        //        // controlo que el servicio este seteado pata enviar mails al los clientes o
        //        // solo al usuario de tests
        //        if (bitEnviaAEmailDestinatarioTest)
        //        {
        //            email.VarEmail = emailDestinatarioTest;
        //            email.VarEmailCCO = emailDtoRRGG;
        //        }
        //        else
        //        {
        //            if (criador.cria_email.Trim().Length > 0) { email.VarEmail = criador.cria_email; };
        //            email.VarEmailCCO = emailDtoRRGG + ((archiRetroactivo) ? ";" + emailCtrlFactAuto : string.Empty);
        //        }

        //        string bodyHTML = this.GetBodyEnviarParteIngresadoWG(raza, criador, objImpoWGRespuesta);

        //        if (bitEnviaAEmailDestinatarioTest)
        //        {
        //            email.EnviaMensaje(("MAIL DE TEST - Envío Porceso Archivo " + NombreArchivo + " RRGG S.R.A. :- " + raza.varRazaDesc.Trim() + " - " + criador.cria_nume.ToString() + " ")
        //                               , bodyHTML
        //                               , null
        //                               );
        //        }
        //        else
        //        {
        //            email.EnviaMensaje(("Envío Porceso Archivo " + NombreArchivo + " RRGG S.R.A. :- " + raza.varRazaDesc.Trim() + " - " + criador.cria_nume.ToString() + " ")
        //                                   , bodyHTML
        //                                   , null
        //                                   );
        //        }


        //        EmailSenderData.RegistroEnvioRetenidasMasivasEmail(raza.varCodiRaza, criador.cria_nume.ToString(), criador.cria_desc, criador.cria_email
        //                                                          , "No Reporte", "-", NombreArchivo, FuncionEnvia
        //                                                          , CachingHelper.getInstace().VersionApp(string.Empty));

        //    }
        //    catch (Exception ex)
        //    {
        //        respuesta = false;
        //        string msg = "ERROR en el envio del criador " + raza.varCodiRaza.PadLeft(3, ' ') + " - " + criador.cria_nume.ToString().PadLeft(5, ' ') + " "
        //                    + criador.cria_desc
        //                    + " email: " + criador.cria_email
        //                    + " :- EmailSenderBusiness.EnviarParteIngresadoWG " + ex.Message;
        //        clsUtiles.clsUtiles.gManejarError(new Exception(msg));
        //    }
        //    // retorno el termino el proceso
        //    return respuesta;
        //}

        //private string GetBodyEnviarParteIngresadoWG(RazasEntity raza, CriadorEntity criador, ImpoWGRespuestaEntity objImpoWGRespuesta)
        //{
        //    string respuesta = string.Empty;
        //    // armo presentacion del mail
        //    respuesta += "<html><body>";
        //    respuesta += "<br><font color='#666666' face='arial' size='3'><b>EMAIL ENVIADO AUTOMATICAMENTE, POR FAVOR NO RESPONDA ESTE EMAIL</b></font>";
        //    respuesta += "<br><br><font color='#666666' face='arial' size='3'>Estimado/a&nbsp;<b>" + criador.cria_desc + "</b></font>";
        //    respuesta += "<br><br><font color='#666666' face='arial' size='2'>Es grato comunicarnos con usted, en esta ocasión le informamos que le dimos curso al archivo " + objImpoWGRespuesta.varNomArchi
        //               + " enviado por usted" + ((objImpoWGRespuesta.intNroLote > 0) ? " del parte: " + objImpoWGRespuesta.intNroLote : ".")
        //               + ((objImpoWGRespuesta.varNroFactura.Trim().Length > 0) ? " .Se generó la factura " + objImpoWGRespuesta.varNroFactura + " la misma le estara llegando a la brevedad." : " La factura sera confeccionada y enviada a la brevedad")
        //               + "</font><br>";
        //    respuesta += "<br><font color='#666666' face='arial' size='2'>Total de Crías declaradas en el archivo:" + objImpoWGRespuesta.lstImpoWGRespuestaDetEntity.Count.ToString() + "</font><br>";

        //    /*seccion de datos de no iscriben o muertor informados por el criador*/
        //    var queryNoFact = (from dato in objImpoWGRespuesta.lstImpoWGRespuestaDetEntity
        //                       where dato.varIsncribe.ToUpper() == "NO".ToUpper()
        //                       select dato).ToList<ImpoWGRespuestaDetEntity>();

        //    if (queryNoFact.Count > 0)
        //    {
        //        respuesta += "<br><font color='#666666' face='arial' size='2'>Crías declaradas por usted como muertas/no inscriben: "
        //                  + queryNoFact.Count.ToString() + " :- Detalle: ";
        //        string Machos = "M:- (";
        //        string Hembras = "H:- (";
        //        foreach (ImpoWGRespuestaDetEntity item in queryNoFact)
        //        {
        //            if (item.varSexo.ToUpper() == "Macho".ToUpper())
        //                Machos += "<font color='#666666' face='arial' size='2'>" + item.varRP + " - " + "</font>";
        //            else
        //                Hembras += "<font color='#666666' face='arial' size='2'>" + item.varRP + " - " + "</font>";
        //        }

        //        if (Machos.Trim().Count() > 5)
        //            respuesta += Machos + ") ";

        //        if (Hembras.Trim().Count() > 5)
        //            respuesta += Hembras + ")";

        //        respuesta += "</font>";
        //    }

        //    /*seccion de animales duplicados ya informados por el criador estos no se facturan*/
        //    var queryDuplicado = (from dato in objImpoWGRespuesta.lstImpoWGRespuestaDetEntity
        //                          where dato.varDuplicado.ToUpper() == "Duplicado".ToUpper()
        //                          select dato).ToList<ImpoWGRespuestaDetEntity>();

        //    if (queryDuplicado.Count > 0)
        //    {
        //        respuesta += "<br><font color='#666666' face='arial' size='2'>Crías declaradas anteriormente por usted, estas no seran facturadas: "
        //                  + queryDuplicado.Count.ToString() + " :- Detalle: ";
        //        string Machos = "M:- (";
        //        string Hembras = "H:- (";
        //        foreach (ImpoWGRespuestaDetEntity item in queryDuplicado)
        //        {
        //            if (item.varSexo.ToUpper() == "Macho".ToUpper())
        //                Machos += "<font color='#666666' face='arial' size='2'>" + item.varRP + " - " + "</font>";
        //            else
        //                Hembras += "<font color='#666666' face='arial' size='2'>" + item.varRP + " - " + "</font>";
        //        }

        //        if (Machos.Trim().Count() > 5)
        //            respuesta += Machos + ") ";

        //        if (Hembras.Trim().Count() > 5)
        //            respuesta += Hembras + ")";

        //        respuesta += "</font>";
        //    }

        //    /*seccion de animales que se facturan*/
        //    var queryFact = (from dato in objImpoWGRespuesta.lstImpoWGRespuestaDetEntity
        //                     where dato.varDuplicado.Trim().Count() == 0 && dato.varIsncribe.Trim().Count() == 0
        //                     select dato).ToList<ImpoWGRespuestaDetEntity>();

        //    if (queryFact.Count > 0)
        //    {
        //        respuesta += "<br><font color='#666666' face='arial' size='2'>Crías declaradas que son facturadas: "
        //                  + queryFact.Count.ToString() + " :- Detalle: ";
        //        string Machos = "M:- (";
        //        string Hembras = "H:- (";
        //        foreach (ImpoWGRespuestaDetEntity item in queryFact)
        //        {
        //            if (item.varSexo.ToUpper() == "Macho".ToUpper())
        //                Machos += "<font color='#666666' face='arial' size='2'>" + item.varRP + " - " + "</font>";
        //            else
        //                Hembras += "<font color='#666666' face='arial' size='2'>" + item.varRP + " - " + "</font>";
        //        }

        //        if (Machos.Trim().Count() > 5)
        //            respuesta += Machos + ") ";

        //        if (Hembras.Trim().Count() > 5)
        //            respuesta += Hembras + ")";

        //        respuesta += "</font>";
        //    }
        //    //respuesta += "<br>";
        //    // contacto 
        //    respuesta += getContactos(raza.varCodiRaza);
        //    respuesta += "<br><br><font color='#666666' face='arial' size='2'>Cordialmente</font>";
        //    respuesta += "<br><br><br><font color='#666666' face='arial' size='2'>Servicios Interactivos Sociedad Rural Argentina</font>";
        //    respuesta += "</body></html>";

        //    // si estamos en modo debug registra en el log el body del mail a enviar
        //    if (bitDebug)
        //        clsUtiles.clsUtiles.gManejarErrorSinMsgBox(new Exception(respuesta));

        //    return respuesta;
        //}






    }

}


