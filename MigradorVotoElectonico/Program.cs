﻿using Data;
using Helper;
using MailHelper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigradorVotoElectonico
{
    class Program
    {
        public static string conStr = ConfigurationManager.ConnectionStrings["SRAConn"].ConnectionString;
        public static string origenPathReader = ConfigurationManager.AppSettings ["path"].ToString();
        public static char splitSeparator = Convert.ToChar(ConfigurationManager.AppSettings["SplitSeparator"]);

        public static int cantidadregistros = 0;

        static void Main(string[] args)
        {   
            Console.WriteLine("INICIO ----------------------------------");
            Logger.info(Logger.Log.Info, new string[] { "INICIO", " ------------------------ " });
            Console.WriteLine("BD:- " + conStr);
            Logger.info(Logger.Log.Info, new string[] { "DB:", "- ", conStr });
            Console.WriteLine("origenPathReader:- " + origenPathReader);
            Logger.info(Logger.Log.Info, new string[] { "Path:", "- ", origenPathReader });

            Console.WriteLine("Ingresa al paso para ingresar los votos emitidos");
            
            LeerArchivo();

            Console.WriteLine("Cant Proc:-");
            Logger.info(Logger.Log.Info, new string[] { "Cant Proc:", "- ", cantidadregistros.ToString() });
            Console.WriteLine("FIN ----------------------------------");
            Logger.info(Logger.Log.Info, new string[] { "FIN", " ------------------------ " });

            Console.ReadLine();
              
            /*
              MailManager mail = new MailManager();
                    mail.VarEmail = "dvasqueznigro@sra.org.ar";
                    mail.varDisplayName = "ERROR integracion Finnegas";
                    mail.EnviaMensaje("ERROR integracion Finnegans se ejecuto con error:-"
                                     , "la integracion Finnegans se ejecuto con error:- " + "status:" + token.status + ",error:" + token.error);
             */
        }

        public static void LeerArchivo()
        {
            cantidadregistros = 0;
            StreamReader objReader = new StreamReader(origenPathReader);

            var sLineaTitulos = objReader.ReadLine().Split(splitSeparator);

            //string[] sLineaLeida = objReader.ReadLine().Split(splitSeparator);

            while (!objReader.EndOfStream) // && sLineaLeida.Length > 5)
            {
                // lee siguiente linea
                string[] sLineaLeida = objReader.ReadLine().Split(splitSeparator);
                cantidadregistros++;
                StringBuilder pstrProc = new StringBuilder();
                try
                {
                    Int32 orvo_id = (sLineaLeida[Convert.ToInt16(colcsv.orvo_id)].Trim().Length > 0) ? Convert.ToInt32("0" + sLineaLeida[Convert.ToInt16(colcsv.orvo_id)]) : 0;
                    string Orvo_Texto_Voto = (sLineaLeida[Convert.ToInt16(colcsv.Orvo_Texto_Voto)].Trim().Length > 0) ? sLineaLeida[Convert.ToInt16(colcsv.Orvo_Texto_Voto)] : string.Empty;

                    if (orvo_id == 0)
                        throw new Exception("orvo_id No valido " + sLineaLeida[Convert.ToInt16(colcsv.orvo_id)]);

                    pstrProc = new StringBuilder();

                    pstrProc.Append("exec p_SOC_RegistraVotoElectronico ");
                    pstrProc.Append(" @orvo_id=");
                    if (orvo_id > 0) { pstrProc.Append(orvo_id); } else { pstrProc.Append("null"); }
                    pstrProc.Append(" ,@Orvo_Texto_Voto=");
                    pstrProc.Append(DataBase.darFormatoSQl(Orvo_Texto_Voto));

                    //string[] Msg = { "MigradorVotoElectonico.LeerArchivo;", pstrProc.ToString() };
                    //Logger.info(Logger.Log.Info, Msg);

                    DataSet ds = DataBase.gExecuteQuery(conStr, pstrProc.ToString(), 9999);
                    //Console.Clear();
                    Console.WriteLine("Cant:-" + cantidadregistros.ToString());
                }
                catch (Exception ex)
                {
                    string[] MsgError = { "MigradorVotoElectonico.LeerArchivo:  ", pstrProc.ToString(), " registro el siguiente error: " + ex.Message };
                    Logger.error(Logger.Log.Error, MsgError);
                    Console.WriteLine("Error: linea:-" + cantidadregistros.ToString() + MsgError[2]);
                }
                // lee siguiente linea
                //sLineaLeida = objReader.ReadLine().Split(splitSeparator);
                /*
                if (objReader.EndOfStream)
                    break;
                */
            }
            objReader.Close();

            string antes = origenPathReader;
            string despues = origenPathReader.Replace('.','_') + ".proc";
            File.Move(antes, despues);
        }
    }

    public enum colcsv
    {
        orvo_id = 0,
        Orvo_Texto_Voto = 1,
    }
}
